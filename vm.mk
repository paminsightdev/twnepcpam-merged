$(eval $(call defw,REGISTRY_HOST,docker.io))
$(eval $(call defw,VAGRANT,vagrant))
$(eval $(call defw,GIT,git))

.PHONY: up
up:: ##@VM Starting up VM
	$(VAGRANT) up

.PHONY: down
down:: ##@VM Stopping up VM
	$(VAGRANT) halt

.PHONY: ssh	
ssh:: ##@VM SSH into the VM
	$(VAGRANT) ssh

.PHONY: putty	
putty:: ##@VM SSH using Putty into the VM
	$(VAGRANT) putty

.PHONY: status	
status:: ##@VM Display status of the VM
	$(VAGRANT) status
				
.PHONY: monitor				
monitor:: ##@VM Start a monitor background process
	$(VAGRANT) fsnotify &