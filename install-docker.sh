#!/usr/bin/env bash

echo "running as $(whoami)..."

function install_docker {
		echo "installing docker"
		apt-get update -y
		apt-get dist-upgrade -y
		apt-get install -y curl apt-transport-https ca-certificates software-properties-common

		#install docker
		echo -n "installing docker ce..."
		curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
		add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
		apt-get update -y
		apt-get install -y docker-ce

		groupadd docker
		usermod -aG docker vagrant
		usermod -aG docker root
		su -c 'newgrp docker' vagrant
		
		curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose		
		chmod +x /usr/local/bin/docker-compose
		echo "done!"
}

which docker

if [ $? -eq 0 ]
then
    docker --version | grep "Docker version"
    if [ $? -eq 0 ]
    then
        echo "docker existing - nothing to do"
    else
		install_docker
    fi
else
    echo "install docker" >&2
	install_docker
fi

echo "user vagrant belongs to $(groups vagrant)"