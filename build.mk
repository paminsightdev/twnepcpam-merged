$(eval $(call defw,MAKE,make))
$(eval $(call defw,DOCKER,docker))

USERNAME=$(shell whoami)
$(eval $(call defw,USERNAME,$(USERNAME)))

VERSION=$(shell cat .version | sed 's/[",]//g' | tr -d '[[:space:]]')
BUILD_DATE := $(shell date -R)

PACKAGE_VERSION=$(shell cat config.yaml | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | tr -d '[[:space:]]')
ver=version:
verionremoved=$(subst $(ver),,$(PACKAGE_VERSION))
VERSION=$(strip $(verionremoved))
$(eval $(call defw,VERSION,$(VERSION)))

PACKAGE_NAME=$(shell cat config.yaml | grep name | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | sed "s/'//g" | sed 's/&project_name//g' | tr -d '[[:space:]]')
name=name:
nameemoved=$(subst $(name),,$(PACKAGE_NAME))
NAME=$(strip $(nameemoved))
$(eval $(call defw,NAME,$(NAME)))

.PHONY: history
history:: ##@Build Display image history
	@echo "Image history"
	$(DOCKER) history $(NAME)

.PHONY: build
build: prebuild run-build postbuild ##@Build Build the docker image

prebuild:: 
	@echo "-+-+-+-+-+-+- Building $(NAME) -+-+-+-+-+-+-"

postbuild:: 	
	@echo "-+-+-+-+-+-+- Successfully built $(NAME) -+-+-+-+-+-+-"

run-build: 
	@echo "-+-+-+-+-+-+- Building $(NAME) -+-+-+-+-+-+-"
	$(DOCKER) build -t $(NAME) \
	--build-arg APP_NAME="${NAME}" \
	--build-arg VERSION="${VERSION}" \
	--build-arg VENDOR="${USERNAME}" .


.PHONY: tag
tag: ##@Build Tagging the build
tag: tag-latest tag-version 

tag-latest: 
	@echo '-+-+-+-+-+-+- Create tag $(NAME) $(REGISTRY_HOST)/$(NAME):latest -+-+-+-+-+-+-'
	$(DOCKER) tag $(NAME) $(REGISTRY_HOST)/$(NAME):latest

tag-version: 
	@echo '-+-+-+-+-+-+- Create tag $(NAME) $(REGISTRY_HOST)/$(NAME):$(VERSION) -+-+-+-+-+-+-'
	$(DOCKER) tag $(NAME) $(REGISTRY_HOST)/$(NAME):$(VERSION) 	