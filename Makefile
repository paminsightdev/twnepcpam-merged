#using https://github.com/25th-floor/docker-make-stub
# ------------------------------ HELPER Scripts BEGIN ----------------------------
BLUE      := $(shell tput -Txterm setaf 4)
GREEN     := $(shell tput -Txterm setaf 2)
TURQUOISE := $(shell tput -Txterm setaf 6)
WHITE     := $(shell tput -Txterm setaf 7)
YELLOW    := $(shell tput -Txterm setaf 3)
GREY      := $(shell tput -Txterm setaf 1)
RESET     := $(shell tput -Txterm sgr0)

SMUL      := $(shell tput smul)
RMUL      := $(shell tput rmul)

# Add the following 'help' target to your Makefile
# And add help text after each target name starting with '\#\#'
# A category can be added with @category
HELP_FUN = \
	%help; \
	use Data::Dumper; \
	while(<>) { \
		if (/^([_a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-_\s]+))?\t(.*)$$/ \
			|| /^([_a-zA-Z\-]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/) { \
			$$c = $$2; $$t = $$1; $$d = $$3; \
			push @{$$help{$$c}}, [$$t, $$d, $$ARGV] unless grep { grep { grep /^$$t$$/, $$_->[0] } @{$$help{$$_}} } keys %help; \
		} \
	}; \
	for (sort keys %help) { \
		printf("${WHITE}%24s:${RESET}\n\n", $$_); \
		for (@{$$help{$$_}}) { \
			printf("%s%25s${RESET}%s  %s${RESET}\n", \
				( $$_->[2] eq "Makefile" || $$_->[0] eq "help" ? "${YELLOW}" : "${GREY}"), \
				$$_->[0], \
				( $$_->[2] eq "Makefile" || $$_->[0] eq "help" ? "${GREEN}" : "${GREY}"), \
				$$_->[1] \
			); \
		} \
		print "\n"; \
	} 

# make
.DEFAULT_GOAL := help

# Variable wrapper
define defw
	custom_vars += $(1)
	$(1) ?= $(2)
	export $(1)
	shell_env += $(1)="$$($(1))"
endef

# Variable wrapper for hidden variables
define defw_h
	$(1) := $(2)
	shell_env += $(1)="$$($(1))"
endef

.PHONY: help
help:: ##@Other Show this help.
	@echo ""
	@printf "%30s " "${BLUE}VARIABLES"
	@echo "${RESET}"
	@echo ""
	@printf "${BLUE}%25s${RESET}${TURQUOISE}  ${SMUL}%s${RESET}\n" $(foreach v, $(custom_vars), $v $(if $($(v)),$($(v)), ''))
	@echo ""
	@echo ""
	@echo ""
	@printf "%30s " "${YELLOW}TARGETS"
	@echo "${RESET}"
	@echo ""
	@perl -e '$(HELP_FUN)' $(MAKEFILE_LIST)

# ------------------------------ HELPER Scripts END ----------------------------	
	
BASENAME=$(shell basename $(PWD))
$(eval $(call defw,MACHINE,${BASENAME}))
	
include ./*.mk

.PHONY: all
all: ##@Other Build all the containers and deploy them to kubernetes
all: build deploy

.PHONY: example-se
example-se: ##@Examples How to use $(shell_env)
	@echo ""
	@echo 'example: how to to use $$(shell_env)'
	@printf "=%.0s" {1..80}
	@echo ""
	@echo '$$(shell_env) my_command'
	$(shell_env) echo "echoing I_AM_A_VARIABLE=$${I_AM_A_VARIABLE}"

.PHONY: example-interfaces
example-interface:: ##@Examples Abuse double-colon rules
	@echo ""
	@echo "example: using double-colon rules to create and implement interface-ish targets"
	@printf "=%.0s" {1..80}
	@echo ""
	@echo "define 'target:: ##@Category not implemement' in .mk"
	@echo "implement 'target:: ##@Category this is nice' in Makefile"

.PHONY: du-overlay
du-overlay:: ##@System Display disk usage of overlay folder
	@sudo su -c 'du -hs /var/lib/docker/overlay2/* | sort -rh | head -20'