ALTER TABLE `article_category` ADD INDEX `article_category_idx_id_id` (`category_id`,`article_id`);
ALTER TABLE `article_region` ADD INDEX `article_region_idx_id_id` (`region_id`,`article_id`);
ALTER TABLE `twnepc_news_articles` ADD INDEX `twnepc_news_articles_idx_id_date_title_slug` (`id`,`published_date`,`title` (255),`slug`);
ALTER TABLE `twnepc_news_articles` ADD INDEX `twnepc_news_articles_idx_date` (`published_date`);