#!/bin/bash

# dump-tables-mysql.sh
# Descr: Dump MySQL table data into separate SQL files for a specified database.
# Usage: Run without args for usage info.
# Author: @Trutane
# Ref: http://stackoverflow.com/q/3669121/138325
# Notes:
#  * Script will prompt for password for db access.
#  * Output files are compressed and saved in the current working dir, unless DIR is
#    specified on command-line.

[ $# -lt 2 ] && echo "Usage: $(basename $0) <DB_USER> <DB_NAME> [<DIR>]" && exit 1

#DB=twnepc
#DB_host='127.0.0.1'
#DB_user=root
#DB_pass='P@minsight2021123!!'
#DB_pass='mysql'

DB_host='localhost'
DB='twnepc_live'
DB_port=3306
DB_user=charles
db_pass=jkgsw2459Kihwh4345lk;;ljHGUoLOIlout?jiu!gjs


DIR='data_dump'


leave_tables=(article_user backed_access_log user_visits sendgrid_reports rainlab_blog_posts rainlab_blog_posts_categories) 

[ -n "$DIR" ] || DIR=.
test -d $DIR || mkdir -p $DIR


echo -n "DB password: "
read -s DB_pass
echo
echo "Dumping tables into separate SQL command files for database '$DB' into dir=$DIR"
echo "Database Backup with these options"
echo "Host $host"
echo "User $user"
echo "Backup in $backup_folder"
echo '----------------------------------------'
echo "Tables to skip:"
printf "%s\n" "${SKIP_TABLES[@]}"
echo '----------------------------------------'

tbl_count=0

function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}

containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}

for t in $(mysql -NBA -h $DB_host -u $DB_user -p$DB_pass -D $DB -e 'show tables') 
do 
    #echo "Table is  $t  and it contains= "  $(contains "${leave_tables[@]}" "$t")
    #echo

    #echo "==Examining  TABLE:" $t

    if [[ $(containsElement "$t" "${leave_tables[@]}") == "y" ]]; then    
        echo " Skipping Table $t as requested "
    else
        echo "DUMPING TABLE: $t"
        # mysqldump -h $DB_host -u $DB_user -p$DB_pass --no-create-info --skip-triggers --extended-insert --lock-tables --quick $DB $t | gzip > $DIR/$DB.$t.sql.gz
        mysqldump -h $DB_host -u $DB_user -p$DB_pass --no-create-info --skip-triggers --extended-insert --lock-tables --quick $DB $t > $DIR/$DB.$t.sql
        tbl_count=$(( tbl_count + 1 ))
    fi
done

echo '----------------------------------------------------------'
echo "$tbl_count tables dumped from database '$DB' into dir=$DIR"