# dump_tables.sh
#!/bin/bash
# this file
# a) gets all databases from mysql
# b) gets all tables from all databases in a)
# c) creates subfolders for every database in a)
# d) dumps every table from b) in a single file

    # this is a mixture of scripts from Trutane (http://stackoverflow.com/q/3669121/138325) 
    # and Elias Torres Arroyo (https://stackoverflow.com/a/14711298/8398149)

# usage: 
# sk-db.bash parameters
# where pararmeters are:

# t " tables to leave"
# u "user who connects to database"
# h "db host"
# f "/backup/folder"


host='127.0.0.1'
user=$1
db='twnepc'
DIR=$3
DB_pass='P@minsight2021123!!'

backup_folder='dumped_data'
leave_tables=(article_user backed_access_log user_visits sendgrid_reports rainlab_blog_posts rainlab_blog_posts_categories)
while getopts ":t:u:h:f:" opt; do
  case $opt in
    t) leave_tables=( $OPTARG )
    ;;
    u) user=$OPTARG
    ;;
    h) host=$OPTARG
    ;;
    f) backup_folder=$OPTARG
    ;;

    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

##echo -n "DB password: "
## read -s DB_pass
echo
echo '****************************************'
echo "Database Backup with these options"
echo "Host $host"
echo "User $user"
echo "Backup in $backup_folder"
echo '----------------------------------------'
echo "Tables to omit:"
printf "%s\n" "${leave_tables[@]}"
echo '----------------------------------------'


BACKUP_DIR=$backup_folder/$(date +%Y-%m-%dT%H_%M_%S);

function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}


test -d "$BACKUP_DIR" || mkdir -p "$BACKUP_DIR"
# Get the database list, exclude information_schema
database_count=0
tbl_count=0

DIR=$BACKUP_DIR/$db
[ -n "$DIR" ] || DIR=.

test -d $DIR || mkdir -p $DIR

echo "Dumping tables into separate SQL command files for database '$db' into dir=$DIR"

for t in $(mysql -NBA -h $host -u $user -p$DB_pass -D $db -e 'show tables') 
# for t in $(mysql -NBA -h $host -u $user -D $db -e 'show tables')
do
    echo $(contains "${leave_tables[@]}" "$t")
    
    if [ $(contains "${leave_tables[@]}" "$t") == 0 ]; then
        echo "leave table $t as requested"
    else
        echo "DUMPING TABLE: $db.$t"
#        echo "SET autocommit=0;SET unique_checks=0;SET foreign_key_checks=0;" > $DIR/$db.$t.sql        
#       mysqldump --defaults-extra-file=$CONFIG_FILE -h $host -u $user $db $t  >> $DIR/$db.$t.sql
#        echo "SET autocommit=1;SET unique_checks=1;SET foreign_key_checks=1;commit;" >> $DIR/$db.$t.sql
        tbl_count=$(( tbl_count + 1 ))
    fi
done

echo "Database $db is finished"
echo '----------------------------------------'


echo '----------------------------------------'
echo "Backup completed"
echo '**********************************************'