-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamtbl_companyfeetypes`
--

LOCK TABLES `pamtbl_companyfeetypes` WRITE;
/*!40000 ALTER TABLE `pamtbl_companyfeetypes` DISABLE KEYS */;
INSERT INTO `pamtbl_companyfeetypes` VALUES (1,'disc','s','Discretionary Portfolio','2016-11-10 23:18:39','2016-11-10 22:27:54',0),(2,'advi','s','Advisory Portfolio','2016-11-10 23:18:39','2016-11-10 22:28:03',0),(3,'duks','t','Discretionary UK Stocks','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(4,'duke','t','Discretionary UK Equities & Convertibles','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(5,'doss','t','Discretionary Overseas Fixed Interest','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(6,'dose','t','Discretionary Overseas Equities','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(7,'auks','t','Advisory UK Stocks','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(8,'auke','t','Advisory UK Equities & Convertibles','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(9,'aoss','t','Advisory Overseas Fixed Interest','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(10,'aose','t','Advisory Overseas Equities','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(13,'aukcb','t','Advisory UK Corporate Bonds','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(14,'dukcb','t','Discretionary UK Corporate Bonds','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(15,'aukcd','t','Advisory UK Contracts for Difference','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(16,'dukcd','t','Discretionary UK Contracts for Difference','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(17,'docd','t','Discretionary Overseas Contracts for Difference','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(18,'aocd','t','Advisory Overseas Contracts for Difference','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(19,'auksp','t','Advisory UK Structured Products','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(20,'duksps','t','Discretionary UK Structured Products','2016-11-10 23:18:39','2016-11-10 22:38:49',0),(21,'aosp','t','Advisory Overseas Structured Products','2016-11-10 23:18:39','2016-11-10 22:31:05',0),(22,'dosp','t','Discretionary Overseas Structured Products','2016-11-10 23:18:39','2016-08-08 18:06:00',0),(23,'duksp','t','Discretionary UK Structured Product','2016-11-10 23:18:39','2016-08-08 18:06:00',0);
/*!40000 ALTER TABLE `pamtbl_companyfeetypes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:52
