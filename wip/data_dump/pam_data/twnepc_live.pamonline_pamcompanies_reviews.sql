-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamonline_pamcompanies_reviews`
--

LOCK TABLES `pamonline_pamcompanies_reviews` WRITE;
/*!40000 ALTER TABLE `pamonline_pamcompanies_reviews` DISABLE KEYS */;
INSERT INTO `pamonline_pamcompanies_reviews` VALUES (52,725,61,'2016-07-07 05:38:30','testing user','testing user','{\"service\":{\"rate\":\"60\",\"description\":\"testing user\",\"title\":\"Client Service Quality\"},\"quality\":{\"rate\":\"60\",\"description\":\"testing user\",\"title\":\"Quality & Clarity of Reporting\"},\"performance\":{\"rate\":\"60\",\"description\":\"testing user\",\"title\":\"Performance\"},\"innovation\":{\"rate\":\"60\",\"description\":\"testing user\",\"title\":\"Innovation\"},\"relationship\":{\"rate\":\"60\",\"description\":\"testing user\",\"title\":\"Relationship Management\"},\"transparency\":{\"rate\":\"60\",\"description\":\"testing user\",\"title\":\"Transparency and Trust\"},\"delivery\":{\"rate\":\"60\",\"description\":\"testing user\",\"title\":\"Product & Service Delivery\"},\"access\":{\"rate\":\"40\",\"description\":\"testing user\",\"title\":\"Online Access\"},\"communication\":{\"rate\":\"60\",\"description\":\"testing user\",\"title\":\"Communication\"},\"staff\":{\"rate\":\"80\",\"description\":\"testing user\",\"title\":\"Staff Turnover Rates\"}}',60,0),(53,718,0,'2016-07-07 06:24:20','testalice','testalice','{\"service\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Client Service Quality\"},\"quality\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Quality & Clarity of Reporting\"},\"performance\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Performance\"},\"innovation\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Innovation\"},\"relationship\":{\"rate\":\"80\",\"description\":\"shit test\",\"title\":\"Relationship Management\"},\"transparency\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Transparency and Trust\"},\"delivery\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Product & Service Delivery\"},\"access\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Online Access\"},\"communication\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Communication\"},\"staff\":{\"rate\":\"80\",\"description\":\"testalice\",\"title\":\"Staff Turnover Rates\"}}',80,0),(54,862,13721,'2016-09-29 14:15:28','Alice testing prof adviser','cftyikft8po','{\"service\":{\"rate\":\"100\",\"description\":\"fyjcftki\",\"title\":\"Client Service Quality\"},\"quality\":{\"rate\":\"100\",\"description\":\"ctygtkiy\",\"title\":\"Quality & Clarity of Reporting\"},\"performance\":{\"rate\":\"100\",\"description\":\"xjycgtkyi\",\"title\":\"Performance\"},\"innovation\":{\"rate\":\"100\",\"description\":\"ctykc ,k\",\"title\":\"Innovation\"},\"relationship\":{\"rate\":\"100\",\"description\":\"cgtykc gtuk\",\"title\":\"Relationship Management\"},\"transparency\":{\"rate\":\"100\",\"description\":\"ctkyucvtyuofv\",\"title\":\"Transparency and Trust\"},\"delivery\":{\"rate\":\"100\",\"description\":\"ctukc\",\"title\":\"Product & Service Delivery\"},\"access\":{\"rate\":\"100\",\"description\":\"cyukl\",\"title\":\"Online Access\"},\"communication\":{\"rate\":\"100\",\"description\":\"ctul\",\"title\":\"Communication\"},\"staff\":{\"rate\":\"100\",\"description\":\"c tgukl\",\"title\":\"Staff Turnover Rates\"}}',100,0),(55,1196,13869,'2016-09-29 14:25:36','Alice testing private investor','testing','{\"service\":{\"rate\":\"100\",\"description\":\"dctrju\",\"title\":\"Client Service Quality\"},\"quality\":{\"rate\":\"100\",\"description\":\"xut\",\"title\":\"Quality & Clarity of Reporting\"},\"performance\":{\"rate\":\"100\",\"description\":\"xrtu\",\"title\":\"Performance\"},\"innovation\":{\"rate\":\"100\",\"description\":\"xutij\",\"title\":\"Innovation\"},\"relationship\":{\"rate\":\"100\",\"description\":\"xritk\",\"title\":\"Relationship Management\"},\"transparency\":{\"rate\":\"100\",\"description\":\"xiryk\",\"title\":\"Transparency and Trust\"},\"delivery\":{\"rate\":\"100\",\"description\":\"xik\",\"title\":\"Product & Service Delivery\"},\"access\":{\"rate\":\"100\",\"description\":\"xrityk\",\"title\":\"Online Access\"},\"communication\":{\"rate\":\"100\",\"description\":\"xyiok\",\"title\":\"Communication\"},\"staff\":{\"rate\":\"100\",\"description\":\"xrik\",\"title\":\"Staff Turnover Rates\"}}',100,0),(57,731,13721,'2016-11-29 14:27:32','TESTY TESTY','VGHVJKGTUL','{\"service\":{\"rate\":\"100\",\"description\":\"j\",\"title\":\"Client Service Quality\"},\"quality\":{\"rate\":\"100\",\"description\":\"j\",\"title\":\"Quality & Clarity of Reporting\"},\"performance\":{\"rate\":\"100\",\"description\":\"j\",\"title\":\"Performance\"},\"innovation\":{\"rate\":\"80\",\"description\":\"j\",\"title\":\"Innovation\"},\"relationship\":{\"rate\":\"80\",\"description\":\"j\",\"title\":\"Relationship Management\"},\"transparency\":{\"rate\":\"100\",\"description\":\"j\",\"title\":\"Transparency and Trust\"},\"delivery\":{\"rate\":\"100\",\"description\":\"j\",\"title\":\"Product & Service Delivery\"},\"access\":{\"rate\":\"100\",\"description\":\"j\",\"title\":\"Online Access\"},\"communication\":{\"rate\":\"100\",\"description\":\"j\",\"title\":\"Communication\"},\"staff\":{\"rate\":\"100\",\"description\":\"j\",\"title\":\"Staff Turnover Rates\"}}',96,0);
/*!40000 ALTER TABLE `pamonline_pamcompanies_reviews` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:50
