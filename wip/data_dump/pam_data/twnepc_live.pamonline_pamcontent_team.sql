-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamonline_pamcontent_team`
--

LOCK TABLES `pamonline_pamcontent_team` WRITE;
/*!40000 ALTER TABLE `pamonline_pamcontent_team` DISABLE KEYS */;
INSERT INTO `pamonline_pamcontent_team` VALUES (1,'Ed Hicks','Managing Director','+44 (0)20 7967 1601','ehicks@paminsight.com','Having worked for the IoD for 17 years, Ed is now running this thriving SME in the UK. Publishing online news services, reports, magazines & a directory and running events & initiatives are his areas of expertise. Ed is also focused on developing commercial & marketing relationships and managing and empowering the employees.','https://www.linkedin.com/in/ed-hicks-7937385?authType=NAME_SEARCH&authToken=_IWT&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A16748895%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1455715593242%2Ctas%3Aed%20hicks','/bcards/Ed Hicks.vcf'),(2,'Alice Grant','Project and Business Development Director','+44 (0)7860 958141','agrant@paminsight.com','Alice joined PAM Insight in 2015 having having been in financial services events since 2011.  Alice is primarily responsible for running the operations of PAM Insight, but is also responsible for the development of new and existing projects, with a focus on working with leaders in the private asset management community to create unique solutions to the challenges facing both their businesses and the industry.','https://www.linkedin.com/in/directorbankingfinanceresearch?authType=NAME_SEARCH&authToken=RBIW&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A135879661%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1455715533698%2Ctas%3Aa','/bcards/Alice Mungall.vcf'),(3,'Tristan Blythe','Group Editor','+44 (0)20 7967 1602','tblythe@paminsight.com','Tristan has been with PAM Insight since 2006 and Group Editor since 2010. He began his career as a reporter, and senior reporter, for thewealthnet. He went on to edit eprivateclient, and its predecessor Private Client Practitioner, for almost two years before taking on his group wide role.','https://www.linkedin.com/in/tristan-blythe-323a4314?authType=NAME_SEARCH&authToken=SGP5&locale=en_US&trk=tyah&trkInfo=clickedVertical%3Amynetwork%2CclickedEntityId%3A50352651%2CauthType%3ANAME_SEARCH%2Cidx%3A1-1-1%2CtarId%3A1455715666983%2Ctas%3Atristan','/bcards/Tristan Blythe.vcf'),(5,'Wassyl Abdessemed','Research Analyst','+44 (0)7525 832 730','wabdessemed@paminsight.com','','',''),(7,'Kathryn Stokes','Event Manager','+44 (0)20 7967 1609','kstokes@paminsight.com','Kathryn joined PAM Insight in 2018 to run the events team.','https://uk.linkedin.com/in/kathryn-stokes',''),(8,'Adam Middleton-Pink','Finance Manager','+44 (0)20 7967 1601','amiddleton-pink@paminsight.com','Adam has responsibility to oversee the financial day to day activity and ensure the companies finance function is well organised and efficient.','','/bcards/Adam  Middleton-Pink.vcf'),(9,'James Anderson','Founder & Editor in Chief','+41 (0)22 731 1270','janderson@paminsight.com','James is the founder of PAM Insight and has specialised in wealth management since 1990, launching our unique range of products and developing publications for numerous professional bodies, including STEP, AIMA, CIOT and the Actuarial Profession. James has chaired the PAM Awards Judging Panel since 2000. He lives in Geneva.','https://www.linkedin.com/in/james-anderson-435361?authType=NAME_SEARCH&authToken=R40f&locale=en_US&srchid=1358796611455715611894&srchindex=2&srchtotal=6746&trk=vsrp_people_res_name&trkInfo=VSRPsearchId%3A1358796611455715611894%2CVSRPtargetId%3A873257%2CVS','/bcards/James  Anderson.vcf'),(10,'Bianca Anechite','Research Associate','0207 974 4444','banechite@paminsight.com','Professional Administrator','',''),(11,'Paul Otchere','IT Developper','07407457518','potchere@paminsight.com','solves problems and builds software solutions','linkedin.com/paulotchere','');
/*!40000 ALTER TABLE `pamonline_pamcontent_team` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:50
