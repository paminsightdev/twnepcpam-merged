-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamtbl_registeredbody`
--

LOCK TABLES `pamtbl_registeredbody` WRITE;
/*!40000 ALTER TABLE `pamtbl_registeredbody` DISABLE KEYS */;
INSERT INTO `pamtbl_registeredbody` VALUES (4,'Law Society of England and Wales','LSE','2016-08-08 18:05:51','2016-08-08 18:05:51',0),(7,'US Securities and Exchange Commission','SEC','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(10,'Jersey Financial Services Commission','JFSC','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(11,'International Security Management Association','ISMA','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(12,'Bank of England','BE','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(13,'Dutch Central Bank/Dutch Securities Board','DCB','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(14,'Guernsey Financial Services Commission','FFSC','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(16,'Central Bank of Bahamas','CBB','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(17,'London Stock Exchange','LSE','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(21,'ICE Futures Europe','ICEFE','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(22,'Cayman Monetary Authority','CMA','2016-08-08 18:05:52','2016-08-08 18:05:52',0),(23,'Bahamas Financial Services Commission','BFSC','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(24,'Gibraltar Financial Services Commission','GFSC','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(25,'Swiss Federal Banking Commission','SFBC','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(26,'Isle of Man Financial Services Commission','IFSC','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(28,'Swiss Financial Market Supervisory Authority','SFM','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(29,'French L\' Autorita des Marches Financiers and Canadian L\'Autorita des Marchs Financiers','LMF','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(30,'US National Futures Association','NFA','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(31,'Swedish Financial Supervisory Authority (FI)','SFSA','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(32,'Spanish Financial Supervisory Authority (CNMV)','SFSA','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(33,'Swiss Association Romande des Intermadiaires Financiers','ARIF','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(34,'German Federal Financial Supervisory Authority (BaFin)','GFF','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(35,'Irish Financial Services Regulatory Authority','IFSRA','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(37,'The Pensions Regulator','TPR','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(38,'South African Financial Services Board','FSB','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(39,'Financial Conduct Authority','FCA','2016-08-08 18:05:53','2016-08-08 18:05:53',0),(40,'Prudential Regulation Authority','PRA','2016-08-08 18:05:54','2016-08-08 18:05:54',0),(42,'Commission de Surveillance du Secteur Financier','CSSF','2016-10-12 13:54:39',NULL,0);
/*!40000 ALTER TABLE `pamtbl_registeredbody` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:52
