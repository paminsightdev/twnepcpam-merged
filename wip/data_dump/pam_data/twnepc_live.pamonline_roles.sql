-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamonline_roles`
--

LOCK TABLES `pamonline_roles` WRITE;
/*!40000 ALTER TABLE `pamonline_roles` DISABLE KEYS */;
INSERT INTO `pamonline_roles` VALUES (8,'Judge','2016-03-29 13:11:00','2021-07-05 20:19:55','Judge - Registered users who are able to judge on the PAM awards, can access personal profile, judging.',0),(9,'Private Investor','2016-03-29 13:12:04','2021-07-05 20:19:55','Private Investor - A normal website user who has registered on the site and is trying to find a PAM, can access personal profile.',0),(10,'Professional Advisors','2016-03-29 13:12:48','2021-07-05 20:19:55','Professional Advisors - Advisors who help Personal Investors find a PAM, can access personal profile.',0),(11,'Private Asset Manager','2016-03-29 13:13:57','2021-07-05 20:19:55','Private Asset Managers - Users who have companies in the PAM directory, can access personal profile, Company details.',0),(12,'Private Asset Manager Admin','2016-03-29 13:14:24','2021-07-05 20:19:55','Private Asset Manager Admin - Users who have companies in the PAM directory, can access personal profile, Company details, ability to verify other team members',0);
/*!40000 ALTER TABLE `pamonline_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:50
