-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamonline_pamcontent_question_options`
--

LOCK TABLES `pamonline_pamcontent_question_options` WRITE;
/*!40000 ALTER TABLE `pamonline_pamcontent_question_options` DISABLE KEYS */;
INSERT INTO `pamonline_pamcontent_question_options` VALUES (5,'None','none',2),(6,'Basic','basic',2),(7,'Average','average',2),(8,'Good','good',2),(9,'Advanced','advanced',2),(18,'I am a <br> <strong>Private Investor</strong><br>','ind',4),(19,'I am a <br> <strong>Professional Advisor</strong><br>','pro',4),(533,'I am a<br>                                <strong>Business Owner / Entrepreneur</strong>','0',7),(534,'I am a<br>                                <strong>Company Executive</strong>','1',7),(535,'I am a<br>                                <strong>Land Owner</strong>','2',7),(536,'I am<br>                                <strong>Retired</strong>','3',7),(537,'<br><strong>Other</strong><br><br>','4',7),(538,'Investment of cash','1',8),(539,'Sale of a business, or shares','2',8),(540,'Inherited a pre-existing portfolio','3',8),(541,'Unhappy with existing provider','4',8),(542,'Retirement planning','5',8),(543,'Just reviewing my options','6',8),(544,'Other','7',8),(545,'less than&nbsp;<br>£100,000','2',9),(546,'£100,001-<br>£500,000','3',9),(547,'£500,001-<br>£1,000,000','4',9),(548,'£1,000,001-<br>£2,500,000','5',9),(549,'£2,500,001-<br>£5,000,000','6',9),(550,'£5,000,001-<br>£10,000,000','7',9),(551,'£10,000,001-<br>£25,000,000','8',9),(552,'more than <br>£25,000,001','9',9),(553,'less than&nbsp;<br>£100,000','2',10),(554,'£100,001&nbsp;-<br>£500,000','3',10),(555,'£500,001&nbsp;-&nbsp;<br>£1,000,000','4',10),(556,'£1,000,001&nbsp;-<br>£2,500,000','5',10),(557,'£2,500,001-<br>£5,000,000','6',10),(558,'£5,000,001-<br>£10,000,000','7',10),(559,'£10,000,001-<br>£25,000,000','8',10),(560,'more than <br>£25,000,001','9',10),(561,'A rise in<br> interest rates','0',11),(562,'A decline in real<br> estate prices','2',11),(563,'A fall in<br> equity markets','2',11),(564,'An increase<br> in tax rates','3',11),(565,'Local to me','1',12),(566,'In the same time zone','2',12),(567,'Anywhere, I don\'t mind','3',12),(568,'Private Bank','Private Bank',13),(569,'Private Investment Bank','Private Investment Bank',13),(570,'Independent Investment Manager','Independent investment manager',13),(571,'Stockbroker','Stockbroker',13),(572,'Fund Manager','Fund Manager',13),(573,'Multi-Family Office','Multi-Family Office',13),(574,'Independent Financial Adviser','Independent Financial Adviser',13),(575,'I don\'t mind<br><br>','',13),(576,'Advisory','a',14),(577,'Discretionary','d',14),(578,'I don\'t mind','n',14),(580,'Choose one...','0',15),(581,'Segregated only','s',15),(582,'Pooled funds only','p',15),(583,'Both','b',15),(584,'I\'m not sure','r',15),(585,'This is not relevant to my selection','n',15),(586,'Choose one...','0',16),(587,'By a portfolio manager','s',16),(588,'By a client relationship manager','p',16),(589,'Both','b',16),(590,'I\'m not sure','r',16),(591,'This is not relevant to my selection','n',16),(592,'Choose one...','0',17),(593,'Income','i',17),(594,'Capital growth','g',17),(595,'Combination of both','b',17),(596,'I\'m not sure','r',17),(597,'This is not relevant to my selection','n',17),(598,'Choose one...','0',18),(599,'Flat annual management fee','c',18),(600,'Transaction commissions','t',18),(601,'Combination of both','b',18),(602,'I\'m not sure','r',18),(603,'This is not relevant to my selection','n',18),(621,'I\'m not sure','r',19),(622,'This is not relevant to my selection','n',19),(623,'Choose one...','0',20),(624,'Minimum investment threshold','m',20),(625,'Average client portfolio size','a',20),(626,'I\'m not sure','r',20),(627,'This is not relevant to my selection','n',20),(722,'This is not relevant to my selection','n',21),(723,'Choose one...','0',22),(724,'I don\'t want to put any my investment capital risk','b',22),(725,'I recognise that there is a trade off between risk and reward but i am still cautious','d',22),(726,'I am a confident investor, lokking to maximise my returns and I undestand the risk of capital loss','a',22),(727,'I\'m not sure','r',22),(728,'Hard copy','hard_copy',24),(729,'Online','online',24),(730,'Via email','via_email',24),(731,'Via an app','via_app',24),(732,'Hard copy','hard_copy',25),(733,'Online','online',25),(734,'Via email','via_email',25),(735,'Via an app','via_app',25),(736,'Hard copy','hard_copy',26),(737,'Online','online',26),(738,'Via email','via_email',26),(739,'Via an app','via_app',26),(740,'​A no-deal Brexit','5',11);
/*!40000 ALTER TABLE `pamonline_pamcontent_question_options` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:50
