-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamtbl_awardfeedback`
--

LOCK TABLES `pamtbl_awardfeedback` WRITE;
/*!40000 ALTER TABLE `pamtbl_awardfeedback` DISABLE KEYS */;
INSERT INTO `pamtbl_awardfeedback` VALUES (5,861,91,'A very clear and well presented entry, addressing the criteria. The report is easy to follow and user friendly. The first page of the report is great – clear explanation and they spell out the objectives early on. They have illustrated the extent to which they have updated their reporting, in order to come back to the forefront in this category. \r\n\r\nStrengths:\r\n•	Objectives clearly outlined\r\n•	Graphics and colour are used well and subtly.\r\n•	Good on fee transparency which could be improved by translating the % fees on underlying funds into amounts\r\n•	Online good with full, clear information that is incredibly accessible\r\n\r\nAreas for improvement:\r\n•	Poor contact details provided\r\n•	Mobile compatible but not a dedicated app\r\n•	Could have done with more attribution analysis?\r\n•	No comment on tax reports\r\n\r\nAn excellent reporting structure overall.','2016-12-16 11:32:20',NULL,0),(6,816,91,'A very basic submission. It is recommended that a standard report be submitted as well as a bespoke report to show the extent of capability. Also access to online should be provided, where applicable. Samples of any other relevant documentation, like accompanying letters, tax reports, consolidated reporting, etc, should also be submitted. \r\n\r\nStrengths:\r\n•	UK and US tax reporting\r\nAreas to improve:\r\n•	No confirmation of objectives or classification/measurement of risk\r\n•	Level of income not granular\r\n•	Calculation basis on performance not included\r\n•	No contact details on valuation\r\n•	Commentary specific to portfolio only available to sophisticated clients – no sample provided','2016-12-16 11:46:24',NULL,0),(7,1152,93,'this is the feedback','2017-10-09 13:47:22',NULL,0),(8,1152,92,'this is the feedback','2017-10-09 13:49:08',NULL,0);
/*!40000 ALTER TABLE `pamtbl_awardfeedback` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:51
