-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamtbl_companyquestion`
--

LOCK TABLES `pamtbl_companyquestion` WRITE;
/*!40000 ALTER TABLE `pamtbl_companyquestion` DISABLE KEYS */;
INSERT INTO `pamtbl_companyquestion` VALUES (1,'How do you feel your business is differentiated from competitors?',_binary '',1,'2016-08-08 18:06:06','2016-08-08 18:06:06',0),(2,'From which channel do you see most of the growth coming for your business over the next 12 months?',_binary '',1,'2016-08-08 18:06:06','2016-08-08 18:06:06',0),(3,'What is the greatest challenge that you think the wealth management community faces?',_binary '',1,'2016-08-08 18:06:06','2016-08-08 18:06:06',0),(4,'What do you feel is the most important measurement of success for your business?',_binary '',1,'2016-08-08 18:06:06','2016-08-08 18:06:06',0),(5,'What can a client expect from your firm in terms of communication and reporting?',_binary '',1,'2016-08-08 18:06:07','2016-08-08 18:06:07',0),(6,'How would you describe the distinctive features of your firm\'s investment philosophy? ',_binary '',2,'2016-08-08 18:06:07','2016-08-08 18:06:07',0),(7,'What asset classes, sectors, themes and/or regions do you favour in the year ahead?',_binary '',2,'2016-08-08 18:06:07','2016-08-08 18:06:07',0),(8,'What, in your view, is the most dangerous threat facing your client\'s portfolios in the next 12 months?',_binary '',2,'2016-08-08 18:06:07','2016-08-08 18:06:07',0);
/*!40000 ALTER TABLE `pamtbl_companyquestion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:52
