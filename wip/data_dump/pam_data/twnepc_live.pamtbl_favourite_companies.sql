-- MySQL dump 10.13  Distrib 5.7.37, for Linux (x86_64)
--
-- Host: localhost    Database: twnepc_live
-- ------------------------------------------------------
-- Server version	5.7.37

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `pamtbl_favourite_companies`
--

LOCK TABLES `pamtbl_favourite_companies` WRITE;
/*!40000 ALTER TABLE `pamtbl_favourite_companies` DISABLE KEYS */;
INSERT INTO `pamtbl_favourite_companies` VALUES (2141,57,'Rothschild Trust Corporation Ltd',1157,0,0,'weekly'),(2144,56,'C Hoare ',718,0,0,'weekly'),(2109,43,'test 123 company',123,0,0,'weekly'),(2145,56,'Close Brothers Asset Management',725,0,0,'weekly'),(2143,56,'',0,0,0,'weekly'),(2135,45,'C Hoare',718,0,0,'weekly'),(2134,45,'Artorius Wealth',1406,0,0,'weekly'),(2146,76,'C Hoare ',718,0,0,'daily'),(2147,76,'RBC Wealth Management',855,0,0,'daily'),(2148,77,'Close Brothers Asset Management',725,0,0,'daily'),(2155,56,'Mourant Private Wealth',1151,0,0,'daily'),(2157,82,'Close Brothers Asset Management',725,0,0,'daily'),(2158,82,'C Hoare ',718,0,0,'daily'),(2159,82,'RBC Wealth Management',855,0,0,'daily'),(2160,82,'UBS Wealth Management',882,0,0,'daily'),(2164,13834,'Close Brothers Asset Management',725,0,0,'daily'),(2163,21985,'Barclays',690,0,0,'daily'),(2166,13834,'Cazenove Capital Management',862,0,0,'weekly'),(2167,13721,'Cazenove Capital Management',862,0,0,'daily'),(2168,13721,'UBS Wealth Management',882,0,0,'daily'),(2169,13869,'LGT Vestra',1196,0,0,'daily'),(2170,13721,'LGT Vestra',1196,0,0,'daily'),(2171,92,'Ruffer LLP',857,0,0,'weekly'),(2172,200,'UBS Wealth Management',882,0,0,'never'),(2173,200,'Coutts',731,0,0,'never'),(2174,200,'Maseco Private Wealth',1229,0,0,'never'),(2175,22410,'Citi Private Bank',720,0,0,'never'),(2176,22410,'UBS Wealth Management',882,0,0,'never'),(2177,22410,'Cazenove Capital Management',862,0,0,'never'),(2179,22411,'Coutts',731,0,0,'weekly'),(2180,22411,'Brewin Dolphin Ltd',701,0,0,'weekly'),(2181,22659,'N W Brown Investment Management',835,0,0,'weekly'),(2182,22659,'Albert E Sharp',1314,0,0,'daily'),(2183,22729,'Sarasin ',861,0,0,'weekly'),(2184,22729,'Charles Stanley ',715,0,0,'weekly'),(2186,22729,'Ruffer LLP',857,0,0,'weekly');
/*!40000 ALTER TABLE `pamtbl_favourite_companies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-03 10:27:52
