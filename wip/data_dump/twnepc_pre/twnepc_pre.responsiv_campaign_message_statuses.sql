-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: twnepc_pre
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `responsiv_campaign_message_statuses`
--

LOCK TABLES `responsiv_campaign_message_statuses` WRITE;
/*!40000 ALTER TABLE `responsiv_campaign_message_statuses` DISABLE KEYS */;
INSERT INTO `responsiv_campaign_message_statuses` VALUES (1,'Draft','draft','2018-07-30 22:59:43','2018-07-30 22:59:43'),(2,'Sent','sent','2018-07-30 22:59:43','2018-07-30 22:59:43'),(3,'Pending','pending','2018-07-30 22:59:43','2018-07-30 22:59:43'),(4,'Active','active','2018-07-30 22:59:44','2018-07-30 22:59:44'),(5,'Cancelled','cancelled','2018-07-30 22:59:44','2018-07-30 22:59:44'),(6,'Archived','archived','2018-07-30 22:59:44','2018-07-30 22:59:44'),(7,'Processing','processing','2018-07-30 22:59:46','2018-07-30 22:59:46');
/*!40000 ALTER TABLE `responsiv_campaign_message_statuses` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-02 11:34:34
