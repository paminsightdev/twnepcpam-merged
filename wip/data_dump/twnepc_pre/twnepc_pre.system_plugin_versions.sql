-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: twnepc_pre
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `system_plugin_versions`
--

LOCK TABLES `system_plugin_versions` WRITE;
/*!40000 ALTER TABLE `system_plugin_versions` DISABLE KEYS */;
INSERT INTO `system_plugin_versions` VALUES (2,'Keios.Multisite','1.1.4','2018-12-03 18:12:03',0,1),(3,'RainLab.Builder','1.0.22','2018-03-26 10:46:40',0,1),(4,'RainLab.User','1.4.6','2018-07-23 17:47:49',0,1),(5,'RainLab.UserPlus','1.1.0','2018-02-06 10:10:51',0,1),(6,'RainLab.Location','1.0.7','2018-02-06 10:12:07',0,1),(8,'RainLab.Notify','1.0.2','2018-02-06 10:13:21',0,1),(9,'RainLab.Sitemap','1.0.8','2018-02-06 10:17:54',0,1),(10,'Mja.Mail','1.2.2','2018-02-06 10:19:11',0,1),(11,'October.Drivers','1.1.1','2018-02-06 10:22:29',0,1),(12,'VojtaSvoboda.TwigExtensions','1.2.2','2018-07-23 17:47:43',0,1),(13,'Castiron.WebpackAssets','1.0.1','2018-02-09 08:48:08',0,1),(33,'AnandPatel.WysiwygEditors','1.2.8','2018-03-29 07:31:34',0,1),(35,'OFFLINE.SiteSearch','1.3.8','2018-06-04 06:01:01',0,1),(36,'Twnepc.Banners','1.0.6','2018-07-31 08:19:57',0,1),(37,'Twnepc.Author','1.0.9','2018-06-28 05:04:55',0,1),(38,'Twnepc.News','1.0.73','2020-10-14 16:31:26',0,1),(39,'Twnepc.Regions','1.0.5','2018-07-16 08:13:44',0,1),(40,'TWnepc.User','1.0.27','2018-09-25 12:15:53',0,1),(41,'Twnepc.Membership','1.1.7','2018-08-28 10:45:24',0,1),(42,'Twnepc.Company','1.0.16','2018-09-25 12:15:53',0,1),(43,'Renatio.FormBuilder','1.4.9','2018-07-23 17:47:49',0,1),(44,'Twnepc.Logs','1.0.6','2019-02-07 11:53:01',0,1),(45,'JorgeAndrade.Subscribe','1.1.0','2018-07-30 07:28:20',1,1),(46,'Twnepc.Newsalerts','1.0.5','2018-07-30 07:28:21',1,1),(47,'Responsiv.Campaign','1.1.30','2018-11-18 20:12:49',0,1),(48,'Twnepc.Backenduser','1.0.2','2018-09-03 08:49:00',0,1),(49,'October.Demo','1.0.1','2018-08-10 18:01:39',0,1),(50,'RainLab.Pages','1.2.18','2018-08-25 16:59:54',0,1),(51,'ShahiemSeymor.Online','1.0.1','2018-08-25 16:59:54',0,1),(52,'Flynsarmy.Mfa','1.0.6','2018-08-29 17:59:19',0,1),(53,'Vdomah.TwoFactorAuth','1.0.3','2018-08-29 17:59:19',0,1),(54,'Twnepc.Fund','1.0.6','2018-09-26 11:40:22',0,1),(55,'Carlosrgzm.CookieConsent','1.0.2','2018-11-01 11:19:39',0,1),(56,'Twnepc.Report','1.0.1','2018-11-14 20:23:25',0,1),(57,'Twnepc.Cron','1.0.1','2019-02-26 20:57:17',0,1),(58,'twnepc.xmlsitemap','1.0.1','2022-01-04 13:47:10',0,0);
/*!40000 ALTER TABLE `system_plugin_versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-02 11:35:19
