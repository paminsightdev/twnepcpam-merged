-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: twnepc_pre
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `responsiv_campaign_lists`
--

LOCK TABLES `responsiv_campaign_lists` WRITE;
/*!40000 ALTER TABLE `responsiv_campaign_lists` DISABLE KEYS */;
INSERT INTO `responsiv_campaign_lists` VALUES (1,'Followers','followers','People who are interested in hearing about news.','2018-07-30 22:59:44','2018-07-30 22:59:44'),(2,'TEST TWN Subscribers','test-twn-subscribers','','2018-07-30 23:08:01','2018-11-10 15:38:52'),(3,'TEST EPC Subscribers','test-epc-subscribers','','2018-07-30 23:08:13','2018-11-10 15:39:42'),(4,'TEST Fundeye Subscribers','test-fundeye-subscribers','','2018-07-30 23:08:26','2018-11-10 15:39:25'),(7,'TWN Subscribers','twn-subscribers','TWN-CORE (do not remove these words Charles 14/04/19)','2018-11-10 15:40:19','2019-04-14 08:48:38'),(8,'OLD FundEye Subscribers','old-fundeye-subscribers','','2018-11-10 15:40:38','2018-11-21 19:44:26'),(9,'EPC Subscribers','epc-subscribers','EPC-CORE (do not remove these words Charles 14/04/19)','2018-11-10 15:41:35','2019-04-14 08:48:51'),(10,'Test List for development','developer-list','','2018-11-14 20:58:39','2020-05-08 13:45:38'),(11,'tWN Unsub Test List','twn-unsub-test-list','','2018-11-19 14:57:34','2018-11-19 14:57:34'),(12,'FundEye Subscribers','fundeye-subscribers','Created 21st November based on tWN list less any users who unsubscribed during the first week. FUNDEYE-CORE (do not remove these words Charles 14/04/19)','2018-11-21 19:45:38','2019-04-14 08:49:17');
/*!40000 ALTER TABLE `responsiv_campaign_lists` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-02 11:34:34
