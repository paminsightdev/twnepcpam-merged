-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: twnepc_pre
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `backend_user_roles`
--

LOCK TABLES `backend_user_roles` WRITE;
/*!40000 ALTER TABLE `backend_user_roles` DISABLE KEYS */;
INSERT INTO `backend_user_roles` VALUES (1,'Publisher','publisher','Site editor with access to publishing tools.','',1,'2018-02-06 09:33:42','2018-02-06 09:33:42'),(2,'Developer','developer','Site administrator with access to developer tools.','',1,'2018-02-06 09:33:43','2018-02-06 09:33:43'),(3,'Reviewer','reviewer','','{\"view_review\":\"1\",\"view_articles\":\"1\"}',0,'2018-04-26 09:13:05','2018-04-26 09:58:29'),(4,'Site Admin','siteadmin','','{\"view_fund\":\"1\",\"edit_fund\":\"1\",\"backend.access_dashboard\":\"1\",\"media.manage_media\":\"1\",\"system.manage_mail_templates\":\"1\",\"edit_companies\":\"1\",\"view_companies\":\"1\",\"view_subscriptions\":\"1\",\"responsiv.campaign.manage_lists\":\"1\",\"responsiv.campaign.manage_messages\":\"1\",\"responsiv.campaign.manage_subscribers\":\"1\",\"view_categories\":\"1\",\"view_articles\":\"1\",\"view_review\":\"1\",\"view_all\":\"1\",\"view_drafts\":\"1\",\"view_denied\":\"1\",\"view_published\":\"1\",\"view_story\":\"1\",\"view_delay\":\"1\",\"view_memberships\":\"1\",\"rainlab.users.access_users\":\"1\",\"rainlab.users.access_settings\":\"1\",\"twnepc.report.view_reports\":\"1\"}',0,'2018-07-19 15:13:01','2020-03-05 10:24:34'),(5,'Super Admin','super_admin','','{\"cms.manage_partials\":\"1\",\"cms.manage_theme_options\":\"1\",\"cms.manage_content\":\"1\",\"cms.manage_assets\":\"1\",\"cms.manage_pages\":\"1\",\"cms.manage_layouts\":\"1\",\"cms.manage_themes\":\"1\",\"rainlab.pages.manage_pages\":\"1\",\"rainlab.pages.manage_menus\":\"1\",\"rainlab.pages.manage_content\":\"1\",\"rainlab.pages.access_snippets\":\"1\",\"view_fund\":\"1\",\"edit_fund\":\"1\",\"backend.access_dashboard\":\"1\",\"backend.manage_preferences\":\"1\",\"backend.manage_editor\":\"1\",\"backend.manage_branding\":\"1\",\"media.manage_media\":\"1\",\"system.manage_updates\":\"1\",\"system.access_logs\":\"1\",\"system.manage_mail_settings\":\"1\",\"system.manage_mail_templates\":\"1\",\"backend.manage_users\":\"1\",\"edit_companies\":\"1\",\"view_companies\":\"1\",\"edit_banners\":\"1\",\"view_banners\":\"1\",\"responsiv.campaign.manage_messages\":\"1\",\"responsiv.campaign.manage_subscribers\":\"1\",\"view_categories\":\"1\",\"view_articles\":\"1\",\"view_review\":\"1\",\"view_all\":\"1\",\"view_drafts\":\"1\",\"view_denied\":\"1\",\"view_published\":\"1\",\"view_story\":\"1\",\"view_delay\":\"1\",\"view_region\":\"1\",\"edit_region\":\"1\",\"rainlab.users.access_groups\":\"1\",\"rainlab.users.access_users\":\"1\",\"rainlab.users.access_settings\":\"1\",\"rainlab.users.impersonate_user\":\"1\",\"renatio.formbuilder.access_fieldtypes\":\"1\",\"renatio.formbuilder.access_formlogs\":\"1\",\"renatio.formbuilder.access_settings\":\"1\",\"renatio.formbuilder.access_forms\":\"1\",\"mja.mail.template\":\"1\",\"mja.mail.mail\":\"1\",\"keios.multisite.access_settings\":\"1\",\"offline.sitesearch.manage_settings\":\"1\",\"rainlab.builder.manage_plugins\":\"1\",\"rainlab.location.access_settings\":\"1\",\"rainlab.sitemap.access_definitions\":\"1\"}',0,'2018-08-27 11:04:07','2019-03-26 13:56:34'),(6,'Editorial One','','Full access: Articles, Email Alerts, Reports, Gifts, Media. \r\nRead Only: Memberships, Companies. \r\nNo Acccess: Users.','{\"backend.access_dashboard\":\"1\",\"media.manage_media\":\"1\",\"system.manage_mail_templates\":\"1\",\"view_companies\":\"1\",\"view_subscriptions\":\"1\",\"responsiv.campaign.manage_messages\":\"1\",\"responsiv.campaign.manage_subscribers\":\"1\",\"view_categories\":\"1\",\"view_articles\":\"1\",\"view_review\":\"1\",\"view_all\":\"1\",\"view_drafts\":\"1\",\"view_denied\":\"1\",\"view_published\":\"1\",\"view_story\":\"1\",\"view_delay\":\"1\",\"view_memberships\":\"1\",\"twnepc.report.view_reports\":\"1\"}',0,'2019-03-26 13:51:50','2019-03-26 13:53:16'),(7,'Editorial Three','','View Articles ONLY.','{\"backend.access_dashboard\":\"1\",\"view_articles\":\"1\",\"view_review\":\"1\",\"view_all\":\"1\",\"view_published\":\"1\"}',0,'2019-03-26 13:53:00','2019-06-04 12:15:22');
/*!40000 ALTER TABLE `backend_user_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-02 11:34:28
