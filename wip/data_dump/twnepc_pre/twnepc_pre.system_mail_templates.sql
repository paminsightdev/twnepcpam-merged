-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: twnepc_pre
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `system_mail_templates`
--

LOCK TABLES `system_mail_templates` WRITE;
/*!40000 ALTER TABLE `system_mail_templates` DISABLE KEYS */;
INSERT INTO `system_mail_templates` VALUES (1,'rainlab.user::mail.activate','Confirm your account','Activate a new user','<h1>Hello {{ name }}</h1>\r\n\r\n<p>We need to verify that this is your email address.</p>\r\n\r\n<p>Please click the link below to confirm your account:</p>\r\n\r\n{% partial \'button\' url=link type=\'positive\' body %}\r\nConfirm account\r\n{% endpartial %}','',1,1,'2018-07-23 17:47:44','2018-08-31 07:38:21'),(2,'rainlab.user::mail.welcome','Your account has been confirmed!','User confirmed their account','<h1>Hello {{ name }}</h1>\r\n\r\n<p>This is a message to inform you that your account has been activated successfully.</p>','',1,1,'2018-07-23 17:47:44','2018-08-31 07:34:54'),(3,'rainlab.user::mail.restore','Requested Password Reset','User requests a password reset','<h1>Hello {{ name }}</h1>\r\n\r\n<p>Somebody has requested a password reset for your account,<br/>if this was not you, please ignore this email.</p>\r\n\r\n<p>Use this activation code to restore your password:</p>\r\n\r\n{% partial \'promotion\' body %}\r\n{{ code }}\r\n{% endpartial %}\r\n\r\n<p>You can use the following link:</p>\r\n\r\n{% partial \'button\' url=link type=\'positive\' body %}\r\nRestore password\r\n{% endpartial %}\r\n\r\n{% partial \'subcopy\' body %}\r\n    **This is an automatic message. Please do not reply to it.**\r\n{% endpartial %}','',1,1,'2018-07-23 17:47:44','2018-11-15 15:34:20'),(4,'rainlab.user::mail.new_user',NULL,'Notify admins of a new sign up',NULL,NULL,2,0,'2018-07-23 17:47:44','2018-07-23 17:47:44'),(5,'rainlab.user::mail.reactivate','Welcome back! Your account has been reactivated','User has reactivated their account','<h1>Welcome back {{ name }}</h1>\r\n\r\n<p>This is a message to inform you that you have successfully signed in and reactivated your account.</p>\r\n\r\n<p>If you did not prompt this, we suggest changing your password before deactivating your account again.</p>','',1,1,'2018-07-23 17:47:44','2018-08-31 07:37:34'),(6,'rainlab.user::mail.invite','An account has been created for you','Invite a new user to the website','<h1>Hello {{ name }}</h1>\r\n\r\n<p>A user account has been created for you. Please use the following login and password to sign in:</p>\r\n\r\n{% partial \'panel\' body %}\r\n- Login: `{{ login }}`\r\n- Password: `{{ password }}`\r\n{% endpartial %}\r\n\r\n<p>After signing in you should change your password as soon as possible.</p>','',1,1,'2018-07-23 17:47:44','2018-08-31 07:38:05'),(7,'renatio.formbuilder::mail.contact','{{ subject }}','Contact form','<h1>Contact request</h1>\r\n\r\n<p>**Name:** {{ name }}</p>\r\n\r\n<p>**Subject:** {{ subject }}</p>\r\n\r\n<p>**E-mail:** {{ email }}</p>\r\n\r\n<p>**Phone:** {{ phone }}</p>\r\n\r\n<p>**Message:** {{ content_message }}</p>','',1,1,'2018-07-23 17:47:44','2018-08-31 07:34:36'),(8,'renatio.formbuilder::mail.default','New User Submission to PAM Insight','New User Submission to PAM Insight','{% for field in form.fields %}\r\n    {% if field.isForView %}\r\n        **{{ field.label }}:** {{ field.postValue }}\r\n\r\n    {% endif %}\r\n{% endfor %}','',1,1,'2018-07-23 17:47:44','2018-09-17 15:59:30'),(9,'backend::mail.invite',NULL,'Invite new admin to the site',NULL,NULL,2,0,'2018-07-23 17:47:44','2018-07-23 17:47:44'),(10,'backend::mail.restore','Password Reset','Reset an admin password','Hello {{ name }}\r\n\r\nSomebody has requested a password reset for your account,<br/>if this was not you, please ignore this email.\r\n\r\nYou can use the following link to restore your password:\r\n\r\n{% partial \'button\' url=link type=\'positive\' body %}\r\nRestore password\r\n{% endpartial %}','',2,1,'2018-07-23 17:47:44','2018-11-15 15:35:19'),(11,'responsiv.campaign::mail.confirm_subscriber','Please confirm subscription','Please confirm subscription','<h1>Please confirm your newsletter subscription using the link below</h1>\r\n\r\n<p><a href=\"{{ confirmUrl }}\">{{ confirmUrl }}</a></p>\r\n\r\n<p>If you received this email by mistake, simply delete it. You won\'t be subscribed if you don\'t click the confirmation link above.</p>','Please confirm your newsletter subscription using the link below:\r\n\r\n{{ confirmUrl }}\r\n\r\nIf you received this email by mistake, simply delete it. You won\'t be subscribed if you don\'t click the confirmation link above.',1,1,'2018-08-02 07:58:28','2018-09-05 13:51:58'),(14,'renatio.formbuilder::mail.article.submission','Article submission','Article submission','{% for field in form.fields %}\r\n    {% if field.isForView %}\r\n        **{{ field.label }}:** {{ field.postValue }}\r\n\r\n    {% endif %}\r\n{% endfor %}','',1,1,'2018-12-06 07:37:15','2018-12-06 07:37:15'),(15,'renatio.formbuilder::mail.press.release.submission','Press release submission','Press release submission','{% for field in form.fields %}\r\n    {% if field.isForView %}\r\n        **{{ field.label }}:** {{ field.postValue }}\r\n\r\n    {% endif %}\r\n{% endfor %}','',1,1,'2018-12-06 07:40:42','2018-12-06 07:40:42');
/*!40000 ALTER TABLE `system_mail_templates` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-02 11:35:19
