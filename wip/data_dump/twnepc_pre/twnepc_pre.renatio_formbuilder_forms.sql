-- MySQL dump 10.17  Distrib 10.3.15-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: twnepc_pre
-- ------------------------------------------------------
-- Server version	10.3.15-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `renatio_formbuilder_forms`
--

LOCK TABLES `renatio_formbuilder_forms` WRITE;
/*!40000 ALTER TABLE `renatio_formbuilder_forms` DISABLE KEYS */;
INSERT INTO `renatio_formbuilder_forms` VALUES (1,7,'Contact Form','contact-form','Renders a contact form.',NULL,NULL,'admin@domain.tld','Admin Person',NULL,NULL,NULL,NULL,'2018-07-23 17:47:44','2018-07-23 17:47:44',NULL,NULL,NULL,NULL,NULL,NULL),(2,8,'Default Form','default-form','Renders a form with all available system fields.',NULL,NULL,'admin@domain.tld','Admin Person',NULL,NULL,NULL,NULL,'2018-07-23 17:47:44','2018-07-23 17:47:44',NULL,NULL,NULL,NULL,NULL,NULL),(3,14,'Contribute a story (TWN)','contribute-a-story-twn','','','','news@thewealthnet.com','TWN','','','','','2018-07-24 11:08:04','2018-12-06 07:41:27','','','','',NULL,NULL),(4,15,'Submit Press Release (TWN)','submit-press-release-twn','','','','news@thewealthnet.com','TWN','','','','','2018-07-25 07:10:08','2018-12-06 07:41:57','','','','',NULL,NULL),(5,15,'Submit Press Release (EPC)','submit-press-release-epc','','','','news@eprivateclient.com','EPC','','','','','2018-07-30 13:15:01','2018-12-06 07:41:49','','','','',NULL,NULL),(6,8,'Subscription (TWN)','subscription-twn','','','','subs@thewealthnet.com','TWN','','','','','2018-09-25 10:30:14','2019-03-04 13:24:49','','','','',NULL,NULL),(7,8,'Subscription (EPC)','subscription-epc','','','','subs@eprivateclient.com','EPC','','','','','2018-09-25 10:46:10','2019-03-04 13:25:16','','','','',NULL,NULL);
/*!40000 ALTER TABLE `renatio_formbuilder_forms` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-02 11:34:34
