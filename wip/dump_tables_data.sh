# dump_tables_data.sh
#!/bin/bash
# this file
# a) gets all databases from mysql
# b) gets all tables from all databases in a)
# c) creates subfolders for every database in a)
# d) dumps every table from b) in a single file

    # this is a mixture of scripts from Trutane (http://stackoverflow.com/q/3669121/138325) 
    # and Elias Torres Arroyo (https://stackoverflow.com/a/14711298/8398149)

# usage: 
# sk-db.bash parameters
# where pararmeters are:

# d "dbs to leave"
# t " tables to leave"
# u "user who connects to database"
# h "db host"
# f "/backup/folder"


user='root'
host='127.0.0.1'
db='twnepc'
backup_folder='data_dump_03_02'
DB_pass='mysql'
leave_dbs=(information_schema mysql twnepc twnepc_pre)
leave_tables=(backend_access_logs system_request_logs system_event_logs renatio_formbuilder_logs page_views)
while getopts ":t:u:h:f:" opt; do
  case $opt in
    t) leave_tables=( $OPTARG )
    ;;
    u) user=$OPTARG
    ;;
    h) host=$OPTARG
    ;;
    f) backup_folder=$OPTARG
    ;;

    \?) echo "Invalid option -$OPTARG" >&2
    ;;
  esac
done

#echo -n "DB password: "
#read -s DB_pass
#echo

echo '****************************************'
echo "Database Backup with these options"
echo "Host $host"
echo "User $user"
echo "Backup in $backup_folder"
echo '----------------------------------------'
echo "Databases to omit:"
printf "%s\n" "${leave_dbs[@]}"
echo '----------------------------------------'
echo "Tables to emit:"
printf "%s\n" "${leave_tables[@]}"
echo '----------------------------------------'

BACKUP_DIR=$backup_folder/$(date +%Y-%m-%dT%H_%M_%S);

function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}


test -d "$backup_folder" || mkdir -p "$backup_folder"
# Get the database list, exclude information_schema
tbl_count=0


       DIR=$backup_folder/$db
       [ -n "$DIR" ] || DIR=.

       test -d $DIR || mkdir -p $DIR

       echo
       echo "Dumping tables into separate SQL command files for database '$db' into dir=$DIR"

       for t in $(mysql  -h $host -u $user -p$DB_pass  -D $db -e 'show tables')
       do
           if [ $(contains "${leave_tables[@]}" "$db.$t") == "y" ]; then
               echo "leave table $db.$t as requested"
           else
               echo "DUMPING TABLE: $db.$t"
               mysqldump --no-create-info  -h $host -u $user -p$DB_pass $db $t  > $DIR/$db.$t.sql
               tbl_count=$(( tbl_count + 1 ))
           fi
       done

       echo "Database $db is finished"
       echo '----------------------------------------'

echo '----------------------------------------'
echo "Backup completed"
echo '**********************************************'
