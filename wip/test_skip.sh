#!/bin/bash


# test_skip.sh

T="backend_access_log"

SKIP_TABLES=(information_schema mysql article_user backend_access_log user_visits sendgrid_reports rainlab_blog_posts rainlab_blog_posts_categories)

containsElement () {
  local e match="$1"
  shift
  for e; do [[ "$e" == "$match" ]] && return 0; done
  return 1
}


function contains() {
    local n=$#
    local value=${!n}
    for ((i=1;i < $#;i++)) {
        if [ "${!i}" == "${value}" ]; then
            echo "y"
            return 0
        fi
    }
    echo "n"
    return 1
}

echo "contains = " $(contains "${SKIP_TABLES[@]}" "$T")

echo $(containsElement "$T" "${SKIP_TABLES[@]}")

if [[ $(contains "${SKIP_TABLES[@]}" "$T") == "y" ]]; then

    echo "---------------------------------------------------"
    echo " -------  Skipping Table $T as requested ------"
    echo "---------------------------------------------------"
else 
    echo " -NOT SKIPPING  ------"
fi
