
--
-- Structure for view `unsubscribes`
--
DROP TABLE IF EXISTS `unsubscribes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `unsubscribes`  AS SELECT `unsubscribers`.`subscriber_id` AS `subscriber_id`, `responsiv_campaign_lists`.`name` AS `name`, `unsubscribers`.`stop_at` AS `stop_at` FROM ((`responsiv_campaign_messages_subscribers` `unsubscribers` left join `responsiv_campaign_messages_lists` on(`responsiv_campaign_messages_lists`.`message_id` = `unsubscribers`.`message_id`)) left join `responsiv_campaign_lists` on(`responsiv_campaign_lists`.`id` = `responsiv_campaign_messages_lists`.`list_id`)) WHERE `unsubscribers`.`stop_at` is not null ;

-- --------------------------------------------------------

--
-- Structure for view `view_article_titles`
--
DROP TABLE IF EXISTS `view_article_titles`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_article_titles`  AS SELECT `twnepc_news_articles`.`title` AS `title` FROM `twnepc_news_articles` ORDER BY `twnepc_news_articles`.`published_date` DESC ;

-- --------------------------------------------------------

--
-- Structure for view `view_article_visits`
--
DROP TABLE IF EXISTS `view_article_visits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `view_article_visits`  AS SELECT `article`.`id` AS `article_id`, `article`.`title` AS `article_title`, `article`.`created_at` AS `article_created_at`, `article`.`published_date` AS `article_published_at`, `article`.`is_free` AS `article_is_free`, `article`.`author_id` AS `article_author_id`, `article_author`.`author_name` AS `article_author_name`, `article`.`admin_author_id` AS `article_admin_author_id`, concat(`article_admin_author`.`first_name`,' ',`article_admin_author`.`last_name`) AS `article_admin_author_name`, `article`.`site_id` AS `article_site_id`, `site`.`theme` AS `article_site_name`, `article`.`status_id` AS `article_status_id`, `article_status`.`name` AS `article_status_name`, `visit`.`membership_id` AS `membership_id`, `visit`.`created_at` AS `visit_created_at`, if(`membership`.`user_id` is not null,'user','company') AS `membership_type`, `membership`.`company_id` AS `membership_company_id`, `membership`.`branch_id` AS `membership_branch_id`, `membership`.`user_id` AS `membership_user_id`, `user`.`classification` AS `user_classification`, `company`.`name` AS `user_company_name`, `industry`.`industry_name` AS `user_company_industry_name`, `user_country`.`name` AS `user_country_name`, `branch_country`.`name` AS `user_branch_country_name`, `branch`.`name` AS `user_branch_name`, `membership`.`subscription_id` AS `subscription_id`, `subscription`.`name` AS `subscription_type`, `subscription`.`interval` AS `subscription_interval`, `subscription`.`limit` AS `subscription_limit` FROM (((((((((((((`twnepc_news_articles` `article` left join `keios_multisite_settings` `site` on(`site`.`id` = `article`.`site_id`)) left join `twnepc_news_statuses` `article_status` on(`article_status`.`id` = `article`.`status_id`)) left join `twnepc_author_author` `article_author` on(`article_author`.`id` = `article`.`author_id`)) left join `backend_users` `article_admin_author` on(`article_admin_author`.`id` = `article`.`admin_author_id`)) left join `article_user` `visit` on(`article`.`id` = `visit`.`article_id`)) left join `twnepc_membership_memberships` `membership` on(`membership`.`id` = `visit`.`membership_id`)) left join `users` `user` on(`user`.`id` = `visit`.`user_id`)) left join `twnepc_company_companies` `company` on(`company`.`id` = `user`.`company_id`)) left join `twnepc_company_branches` `branch` on(`branch`.`id` = `user`.`branch_id`)) left join `twnepc_users_industry` `industry` on(`industry`.`id` = `company`.`industry_id`)) left join `rainlab_location_countries` `branch_country` on(`branch_country`.`id` = `branch`.`country_id`)) left join `rainlab_location_countries` `user_country` on(`user_country`.`id` = `user`.`country_id`)) left join `twnepc_membership_subscriptions` `subscription` on(`subscription`.`id` = `membership`.`subscription_id`)) ORDER BY `article`.`id` ASC ;
