FROM node:6.14 as builder

ENV DEPLOYMENT_ENV development

WORKDIR  /var/www/html
ADD themes /var/www/html/themes
COPY bootstrap.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/bootstrap.sh && bootstrap.sh app:init-themes

FROM aspendigital/octobercms:build.436-php7.2-apache

WORKDIR /var/www/html
COPY --from=builder /var/www/html/themes /var/www/html/themes
COPY ./plugins /var/www/html/plugins
COPY ./modules /var/www/html/modules
COPY bootstrap.sh /usr/local/bin/
COPY .htaccess /var/www/html
COPY ./config/cms.php /var/www/html/config/
COPY ./config/session.php /var/www/html/config/
RUN apt-get update && apt-get install -y \
    unzip  \
    && rm -rf /var/lib/apt/lists/*
RUN chmod +x /usr/local/bin/bootstrap.sh && bootstrap.sh app:init-plugins
COPY cronjobs.sh /var/www/html/
RUN chmod +x /var/www/html/cronjobs.sh 
RUN echo "* * * * * /var/www/html/cronjobs.sh > /proc/1/fd/1 2>/proc/1/fd/2" > /etc/cron.d/october-cron && \
  crontab /etc/cron.d/october-cron
RUN sed -i -e 's/memory_limit=128M/memory_limit=256M/g' /usr/local/etc/php/conf.d/docker-oc-php.ini
RUN sed -i '/exec "$@"/i /usr/bin/env >/var/www/html/.cron.env' /usr/local/bin/docker-oc-entrypoint

RUN  mkdir twn && mkdir epc && mkdir fundeye && mkdir pam
RUN  ln -sfn /var/www/html/boostrap /var/www/html/twn/ && \
     ln -sfn /var/www/html/config /var/www/html/twn/ && \ 
     ln -sfn /var/www/html/env /var/www/html/twn/ && \
     ln -sfn /var/www/html/modules /var/www/html/twn/ && \
     ln -sfn /var/www/html/plugins /var/www/html/twn/ && \
     ln -sfn /var/www/html/storage /var/www/html/twn/ && \
     ln -sfn /var/www/html/themes /var/www/html/twn/ && \
     ln -sfn /var/www/html/vendor /var/www/html/twn/ && \
     ln -sfn /var/www/html/.env  /var/www/html/twn/ && \
     ln -sfn /var/www/html/.htaccess /var/www/html/twn/ && \
     ln -sfn /var/www/html/index.php /var/www/html/twn/ && \
     ln -sfn /var/www/html/server.php /var/www/html/twn/ 


RUN  ln -sfn /var/www/html/boostrap /var/www/html/epc/ && \
     ln -sfn /var/www/html/config /var/www/html/epc/ && \ 
     ln -sfn /var/www/html/env /var/www/html/epc/ && \
     ln -sfn /var/www/html/modules /var/www/html/epc/ && \
     ln -sfn /var/www/html/plugins /var/www/html/epc/ && \
     ln -sfn /var/www/html/storage /var/www/html/epc/ && \
     ln -sfn /var/www/html/themes /var/www/html/epc/ && \
     ln -sfn /var/www/html/vendor /var/www/html/epc/ && \
     ln -sfn /var/www/html/.env  /var/www/html/epc/ && \
     ln -sfn /var/www/html/.htaccess /var/www/html/epc/ && \
     ln -sfn /var/www/html/index.php /var/www/html/epc/ && \
     ln -sfn /var/www/html/server.php /var/www/html/epc/ 



RUN  ln -sfn /var/www/html/boostrap /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/config /var/www/html/fundeye/ && \ 
     ln -sfn /var/www/html/env /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/modules /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/plugins /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/storage /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/themes /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/vendor /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/.env  /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/.htaccess /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/index.php /var/www/html/fundeye/ && \
     ln -sfn /var/www/html/server.php /var/www/html/fundeye/ 



RUN  ln -sfn /var/www/html/boostrap /var/www/html/pam/ && \
     ln -sfn /var/www/html/config /var/www/html/pam/ && \ 
     ln -sfn /var/www/html/env /var/www/html/pam/ && \
     ln -sfn /var/www/html/modules /var/www/html/pam/ && \
     ln -sfn /var/www/html/plugins /var/www/html/pam/ && \
     ln -sfn /var/www/html/storage /var/www/html/pam/ && \
     ln -sfn /var/www/html/themes /var/www/html/pam/ && \
     ln -sfn /var/www/html/vendor /var/www/html/pam/ && \
     ln -sfn /var/www/html/.env  /var/www/html/pam/ && \
     ln -sfn /var/www/html/.htaccess /var/www/html/pam/ && \
     ln -sfn /var/www/html/index.php /var/www/html/pam/ && \
     ln -sfn /var/www/html/server.php /var/www/html/pam/ 



