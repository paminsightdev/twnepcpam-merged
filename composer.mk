$(eval $(call defw,COMPOSE,docker-compose))

.PHONY: start
start:: ##@Dev Start environment
	$(COMPOSE) up -d

.PHONY: stop
stop:: ##@Dev Stop environment
	$(COMPOSE) down --remove-orphans

.PHONY: env
env:: ##@Dev Display artisan env info
	$(COMPOSE) exec web php artisan env

.PHONY: compose-info
compose-info:: ##@Dev Run composer info
	$(COMPOSE) exec web composer info

.PHONY: cms-up
october-up:: ##@Dev October up
	$(COMPOSE) exec web october up

.PHONY: cms-ssh
ssh-cms:: ##@Dev Remove volume
	$(COMPOSE) exec web bash

.PHONY: cron-ssh
ssh-cron:: ##@Dev Remove volume
	$(COMPOSE) exec cron bash

.PHONY: cms-init
init-cms:: ##@Dev Remove volume
	$(COMPOSE) exec web bash -c "php artisan october:up && php artisan cache:clear &&  php artisan view:clear"	

.PHONY: tinker
october-up:: ##@Dev Run Artisan tinker
	$(COMPOSE) exec web tinker

.PHONY: remove-volumes
remove-volumes:: ##@Dev Remove volume
	$(DOCKER) volume prune

.PHONY: campaign
campaign:: ##@Prod Execute campaign crond
	$(COMPOSE) -f docker-compose-prod.yml exec cron bash -c "/usr/local/bin/php /var/www/html/artisan campaign:run"

.PHONY: rediscli
rediscli:: ##@Prod Redis CLI
	$(DOCKER) run -it --network=twnproxy --link twn-epc_redis_1:redis --rm redis redis-cli -h redis -p 6379
