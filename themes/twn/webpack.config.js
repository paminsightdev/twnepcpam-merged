const webpack               = require('webpack');
const path                  = require('path');
const inProduction          = (process.env.NODE_ENV === 'production');
const ExtractTextPlugin     = require('extract-text-webpack-plugin');
const PhpManifestPlugin     = require('webpack-php-manifest');
const CopyWebpackPlugin     = require('copy-webpack-plugin');

module.exports = {

    entry: [
        './src/js/app.js',
        './src/scss/main.scss',
    ],
    output: {
        path: path.resolve(__dirname, './assets'),
        filename: 'app.js'
    },
    devtool: 'inline-source-map',
    devServer: {
      contentBase: path.resolve(__dirname, 'assets'),
      publicPath: '/',
      hot: true,
      headers: { 'Access-Control-Allow-Origin': '*' }
    },
    module: { 
        rules: [
            {
                test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                // Limiting the size of the woff fonts breaks font-awesome ONLY for the extract text plugin
                // loader: "url?limit=10000"
                use: "url-loader"
            },
            {
                test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                use: 'file-loader'
            },
            {
                test: /\.s[ac]ss$/,
                use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader'],
                    fallback: 'style-loader'
                }))
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
              test: /\.(jpg|png|svg)$/,
              loader: 'file-loader'
            },
        ],
    },
    plugins: [
        new CopyWebpackPlugin([

            { from: './src/images', to: 'images' },

        ],{
            copyUnmodified: true
        }),
        new PhpManifestPlugin({
            path: (!inProduction) ? '' : path.relative(path.resolve(__dirname,'../../'),path.resolve('./assets')),
            devServer: (!inProduction),
            pathPrefix: (!inProduction) ? 'http://localhost:8080' : null

        }),
        new ExtractTextPlugin('app.css'),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            Tether: 'tether',
        }),
    ]
};


if (inProduction) {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false },
            output: {comments: false}
        })
    );
}