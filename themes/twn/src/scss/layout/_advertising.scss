// Fix width of banners col
//$bannerRightWidth: 150px;   // 120
$bannerSquareWidth: 330px;  // 300

.main-content {

	> .container {

		// Top banners
		&.banner-container {
			padding-top: 30px;
			@media (max-width: 991px) {
				padding-top: 15px;
			}
		}

		// Home and catergory pages
		> .row {
			> .col-main {
				flex: 0 0 calc(100% - #{$bannerSquareWidth});
				max-width: calc(100% - #{$bannerSquareWidth});
				@media (max-width: 991px) {
					flex: 0 0 100%;
					max-width: 100%;
				}

				.news-box-row {
					.news-box-wrapper {
						@media (max-width: 991px) {
							flex: 0 0 calc(100% - #{$bannerSquareWidth});
							max-width: calc(100% - #{$bannerSquareWidth});
						}
						@media (max-width: 767px) {
							flex: 0 0 100%;
							max-width: 100%;
						}
					}

					.col-secondary {
						@media (max-width: 991px) {
							padding-left: 15px;
							flex: 0 0 $bannerSquareWidth;
							max-width: $bannerSquareWidth;
						}
						@media (max-width: 767px) {
							flex: 0 0 100%;
							max-width: 100%;
						}
					}
				}
			}
			> .col-secondary {
				flex: 0 0 $bannerSquareWidth;
				max-width: $bannerSquareWidth;
				@media (max-width: 991px) {
					flex: 0 0 100%;
					max-width: 100%;
				}
			}
		}
	}
	
	// CMS pages
	> .container.cms-wrapper {
		> .row {
			> .col-md-12 {
				flex: 0 0 calc(100%);
				max-width: calc(100%);
				@media (max-width: 1199px) {
					flex: 0 0 calc(100%);
					max-width: calc(100%);
				}
				@media (max-width: 767px) {
					flex: 0 0 100%;
					max-width: 100%;
				}
			}
		}
	}
}

// Banners
.banners-wrapper {
	text-align: center;
	display: flex;
	justify-content: center;
	> div {
		margin: 0 4px;
		@media (max-width: 991px) {
			margin: 0;
		}
	}
	img {
		display: block;
	}
	&:after {
		content: '';
		display: table;
		clear: both;
	}
	@media (max-width: 1199px) {
		.small-banner {
			display: none;
		}
	}
	@media (max-width: 991px) {
		.large-banner {
			margin: 0 -19px;
			img {
				width: 100%;
				height: auto;
				margin: 0;
			}
		}
	}
	@media (max-width: 767px) {
		.small-banner {
			display: inline-block;
		}
		.large-banner {
			display: none;
		}
	}
}

// Banners on tablet
.tablet-banner-wrapper {
	.col-banners-wrapper {
		display: flex;
		margin-bottom: 2%;
		.small-banner {
			float: left;
			width: 32%;

			& + .small-banner {
				margin-left: 2%;
			}
		}
		&.banners-2 {
			.small-banner {
				width: 49%;
			}
		}

		&.banners-1 {
			.small-banner {
				width: 100%;
			}
		}
	}
	
	&:after {
		clear: both;
		display: table;
		content: '';
	}
}

// Banners on mobile
@media (max-width: 767px) {
	.col-banners-wrapper {
		margin-top: 20px;
		padding-left: 15px;
		padding-right: 15px;
		.small-banner {
			& + .small-banner {
				margin-top: 15px;
			}
		}
	}
}

.banner-unit {
	&.banner-square {
		margin-bottom: 20px;
		min-height: 0;
		height: auto;
		padding: 0;
		background-color: transparent; 
		@media (max-width: 767px) {
			text-align: center;
			padding: 20px 0 15px 0;
			background-color: #EEE;
		}
		img {
			display: block;
			width: 100%;
			height: auto;
		}
	}
}