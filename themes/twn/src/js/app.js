import $ from 'jquery'
import 'bootstrap'

if(module.hot) {

    module.hot.accept(
       
    )
}

// Breakpoint.js
(function($) {

	var lastSize = 0;
	var interval = null;

	$.fn.resetBreakpoints = function() {
		$(window).unbind('resize');
		if (interval) {
			clearInterval(interval);
		}
		lastSize = 0;
	};
	
	$.fn.setBreakpoints = function(settings) {
		var options = jQuery.extend({
							distinct: true,
							breakpoints: new Array(320,480,768,1024)
				    	},settings);


		interval = setInterval(function() {
	
			var w = $(window).width();
			var done = false;
			
			for (var bp in options.breakpoints.sort(function(a,b) { return (b-a) })) {
			
				// fire onEnter when a browser expands into a new breakpoint
				// if in distinct mode, remove all other breakpoints first.
				if (!done && w >= options.breakpoints[bp] && lastSize < options.breakpoints[bp]) {
					if (options.distinct) {
						for (var x in options.breakpoints.sort(function(a,b) { return (b-a) })) {
							if ($('body').hasClass('breakpoint-' + options.breakpoints[x])) {
								$('body').removeClass('breakpoint-' + options.breakpoints[x]);
								$(window).trigger('exitBreakpoint' + options.breakpoints[x]);
							}
						}
						done = true;
					}
					$('body').addClass('breakpoint-' + options.breakpoints[bp]);
					$(window).trigger('enterBreakpoint' + options.breakpoints[bp]);

				}				

				// fire onExit when browser contracts out of a larger breakpoint
				if (w < options.breakpoints[bp] && lastSize >= options.breakpoints[bp]) {
					$('body').removeClass('breakpoint-' + options.breakpoints[bp]);
					$(window).trigger('exitBreakpoint' + options.breakpoints[bp]);

				}
				
				// if in distinct mode, fire onEnter when browser contracts into a smaller breakpoint
				if (
					options.distinct && // only one breakpoint at a time
					w >= options.breakpoints[bp] && // and we are in this one
					w < options.breakpoints[bp-1] && // and smaller than the bigger one
					lastSize > w && // and we contracted
					lastSize >0 &&  // and this is not the first time
					!$('body').hasClass('breakpoint-' + options.breakpoints[bp]) // and we aren't already in this breakpoint
					) {					
					$('body').addClass('breakpoint-' + options.breakpoints[bp]);
					$(window).trigger('enterBreakpoint' + options.breakpoints[bp]);

				}						
			}
			
			// set up for next call
			if (lastSize != w) {
				lastSize = w;
			}
		}, 250);
	};
	
})(jQuery);

// TWN - EPC
$(function() {

	// Trending sidebar opener
	$('.trending-sidebar .title').on('click', function() {
		$('.trending-sidebar').toggleClass('active');
	});

	$('.all-subjects').find('input').on('change',function(){
		var siteClass = $(this).parent().attr('data-site');

		if($(this).prop('checked')) {
			
			$('.interests.site-'+siteClass).find('input').each(function(index,item) {
				$(this).prop('checked',false);
			})
		}
	});

	$('.interests input').on('change',function(){
		var siteClass= $(this).parent().attr('data-site');
		$('.all-subjects.site-'+siteClass).find('input').prop('checked',false);

		if($(this).prop('checked')){
            $(this).parents('.interest.custom-control.custom-checkbox').children('input.custom-control-input').prop('checked',false);
            $(this).parent().find('.interest.custom-control.custom-checkbox > input.custom-control-input').prop('checked',false);
            $(this).prop('checked',true);
		}


	})

	$(document).mouseup(function(e) {
    	var container = $(".search-popup");

    	if ($(e.target).parents('.search-wrapper').length > 0) {
    		$('.search-popup').toggleClass('active');
    	} else if (!container.is(e.target) && container.has(e.target).length === 0)  {
	        $('.search-popup').removeClass('active');
	    }
	});

    // Breakpoints - move elements
    $(window).setBreakpoints({
        distinct: true,
        breakpoints: [
        	200,
            768,
            992
        ]
    });


    if ($('.page-home').length) {
    
	    var banners = $('.col-banners-wrapper');
	    var secCol  = $('.col-secondary');
	    
	    $(window).bind('enterBreakpoint200',function() {
	    	secCol.insertAfter($('.news-box-row .news-box-wrapper'));
	    	banners.insertAfter($('.news-box-wrapper'));
	    });

	    $(window).bind('enterBreakpoint768',function() {
	        $('.tablet-banner-wrapper').prepend(banners);
	        secCol.insertAfter($('.news-box-row .news-box-wrapper'));
	    });

	    $(window).bind('enterBreakpoint992',function() {
	        $('.col-secondary').prepend(banners);
	        secCol.insertAfter($('.col-main'));
	    });
	}

	$(window).bind('enterBreakpoint768',function() {
        $('.navbar .container .navbar-collapse').prepend($('.region-selector'));
    });

	$(window).bind('enterBreakpoint992',function() {
        $('.header-top .col-lg-5').prepend($('.region-selector'));
    });


	// Change gender according to salutation - Registration
    $("#registerSalutation").change(function () {
        var val = $(this).val();
        if (val == "Mrs.") {
            $("#female").addClass('active');
            $("#male").removeClass('active');
        }
        if (val == "Ms.") {
            $("#female").addClass('active');
            $("#male").removeClass('active');
        }
        if (val == "Mr.") {
            $("#male").addClass('active');
            $("#female").removeClass('active');
        }
    });


    // Select all function for interest - Registration
   // $('.register-wrapper .interests-wrapper .all-subjects').find('.custom-checkbox :checkbox').attr('checked', true);
   // $('.page-preferences .interests-wrapper .all-subjects').find('.custom-checkbox :checkbox').attr('checked', true);


    // Add class to custom banners
    if ( $('.col-banners-wrapper').length ) {
	    var bannersCount = $(".col-banners-wrapper > div").length;
	    $('.col-banners-wrapper').addClass('banners-' + bannersCount);
	}


	// Open my account tab (Favourites) from url
	$('body.page-account .user-nav a:first-child').on('click', function() {
		location.reload();
	});

	var hash = window.location.hash;
	window.location.hash = "";
	hash && $('.account-wrapper .left-nav a.nav-link[href="' + hash + '"]').tab('show');

	// Request forms
	$('.find-form-group select').on('change', function (e) {
    	var optionSelected = $("option:selected", this);
    	var valueSelected = this.value;
    	if (valueSelected == "word_of_mouth") {
    		$('.referred-form-group').fadeIn();
    		$('.referred-form-group input').prop("disabled", false);
    	} else {
    		$('.referred-form-group').fadeOut();
    		$('.referred-form-group input').prop("disabled", true);
    	}
	});

	if ($('.free-trial-form').length) {
		$('.request-form-group select').val("free_trial");
	}

	if ($('.extend-form').length) {
		$('.request-form-group select').val("extend");
	}

	if ($('.new-form').length) {
		$('.request-form-group select').val("new");
	}
	
});
