#!/bin/bash

set -e
[[ $DEBUG == true ]] && set -x

case ${1} in
  app:init-themes|app:init-plugins|app:help)
    case ${1} in
      app:init-themes)	  
        # Initialize vendor plugins where vendor folder isn't detected
        echo 'Initializing themes folders...'
          for i in $(echo /var/www/html/themes/*); do
            if [ -f "$i/package.json" ]; then
                cd "$i"
                npm install
                npm run prod
                chown -R www-data:www-data $i
            fi
          done    
        ;;	
      app:init-plugins)	  
        # Initialize vendor plugins where vendor folder isn't detected
        echo 'Initializing plugin vendor folders...'
          for i in $(echo /var/www/html/plugins/*/*); do
            if [ -f "$i/composer.json" ]; then
                echo "*** Installing compose.json dependencies in $i ***"
                composer --working-dir="$i" --no-interaction install
                chown -R www-data:www-data $i
		echo "*** Finished installing dependencies in $i ***"
            fi
          done    
        ;;	
	  app:help)
		echo "Available options:"
		echo " app:init-themes    - Create indexes from /docker-entrypoint-ini.d"
		echo " app:init-plugins   - Displays the help"
		echo " [command]          - Execute the specified command, eg. bash."	
		;;
    esac
    ;;
  *)
  
	# KEEP original docker-entrypoint to do its house keeping
	# if command starts with an option, prepend php
	if [ "${1:0:1}" = '-' ]; then
			set -- php artisan "$@"
	fi

	# As argument is not related
	# then assume that user wants to run his own process,
	# for example a `bash` shell to explore this image  
  
    exec "$@"
    ;;
esac
