-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2022 at 12:52 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `twnepc`
--

-- --------------------------------------------------------

--
-- Table structure for table `article_article`
--

CREATE TABLE `article_article` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `relation_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE `article_category` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_fav_user`
--

CREATE TABLE `article_fav_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `site_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_file`
--

CREATE TABLE `article_file` (
  `article_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_fund`
--

CREATE TABLE `article_fund` (
  `fund_id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_region`
--

CREATE TABLE `article_region` (
  `id` int(10) UNSIGNED NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `region_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `article_user`
--

CREATE TABLE `article_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `sessionid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `membership_id` int(10) UNSIGNED DEFAULT NULL,
  `message_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Triggers `article_user`
--
DELIMITER $$
CREATE TRIGGER `insert_guest_user_article_user` BEFORE INSERT ON `article_user` FOR EACH ROW BEGIN
	IF ( NEW.user_id IS NULL ) THEN 
		SET NEW.user_id = 66;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `backend_access_log`
--

CREATE TABLE `backend_access_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_users`
--

CREATE TABLE `backend_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL DEFAULT 0,
  `site_id` int(11) DEFAULT NULL,
  `mfa_enabled` tinyint(1) DEFAULT 0,
  `mfa_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `mfa_persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `mfa_question_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `mfa_answer_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `mfa_question_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `mfa_answer_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_users_groups`
--

CREATE TABLE `backend_users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_groups`
--

CREATE TABLE `backend_user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_new_user_default` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_preferences`
--

CREATE TABLE `backend_user_preferences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_roles`
--

CREATE TABLE `backend_user_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_system` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `backend_user_throttle`
--

CREATE TABLE `backend_user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT 0,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT 0,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT 0,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bedard_blogtags_post_tag`
--

CREATE TABLE `bedard_blogtags_post_tag` (
  `tag_id` int(10) UNSIGNED DEFAULT NULL,
  `post_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bedard_blogtags_tags`
--

CREATE TABLE `bedard_blogtags_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `benfreke_menumanager_menus`
--

CREATE TABLE `benfreke_menumanager_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nest_left` int(10) UNSIGNED DEFAULT NULL,
  `nest_right` int(10) UNSIGNED DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_external` tinyint(1) NOT NULL DEFAULT 0,
  `link_target` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '_self',
  `enabled` int(11) NOT NULL DEFAULT 1,
  `parameters` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `query_string` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_subscription`
--

CREATE TABLE `category_subscription` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `subscription_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `category_user`
--

CREATE TABLE `category_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_data`
--

CREATE TABLE `cms_theme_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_theme_logs`
--

CREATE TABLE `cms_theme_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_content` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `company_sections_updates`
--

CREATE TABLE `company_sections_updates` (
  `company_id` int(11) NOT NULL DEFAULT 0,
  `investment_commentary` int(11) DEFAULT NULL,
  `publications` int(11) DEFAULT NULL,
  `news` int(11) DEFAULT NULL,
  `articles` int(11) DEFAULT NULL,
  `other` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_sections_users`
--

CREATE TABLE `company_sections_users` (
  `user_id` int(11) NOT NULL DEFAULT 0,
  `company_id` int(11) NOT NULL DEFAULT 0,
  `investment_commentary` int(11) DEFAULT NULL,
  `publications` int(11) DEFAULT NULL,
  `news` int(11) DEFAULT NULL,
  `articles` int(11) DEFAULT NULL,
  `other` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deferred_bindings`
--

CREATE TABLE `deferred_bindings` (
  `id` int(10) UNSIGNED NOT NULL,
  `master_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `master_field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slave_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slave_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `session_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_bind` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `failed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flynsarmy_menu_menuitems`
--

CREATE TABLE `flynsarmy_menu_menuitems` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `enabled` tinyint(1) NOT NULL DEFAULT 0,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title_attrib` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_attrib` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class_attrib` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `selected_item_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `master_object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `master_object_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `nest_left` int(11) NOT NULL DEFAULT 0,
  `nest_right` int(11) NOT NULL DEFAULT 0,
  `nest_depth` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `target_attrib` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `data` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flynsarmy_menu_menus`
--

CREATE TABLE `flynsarmy_menu_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_attrib` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `class_attrib` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `short_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `keios_multisite_settings`
--

CREATE TABLE `keios_multisite_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `domain` text COLLATE utf8_unicode_ci NOT NULL,
  `theme` text COLLATE utf8_unicode_ci NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `default_subscription_id` int(10) UNSIGNED DEFAULT NULL,
  `sender_address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender_name` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mja_mail_email_log`
--

CREATE TABLE `mja_mail_email_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `to` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `bcc` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `sender` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reply_to` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sent` tinyint(1) NOT NULL DEFAULT 0,
  `hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `response` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mja_mail_email_opens`
--

CREATE TABLE `mja_mail_email_opens` (
  `id` int(10) UNSIGNED NOT NULL,
  `email_id` int(11) NOT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offline_csp_logs`
--

CREATE TABLE `offline_csp_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `blocked_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referrer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disposition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `violated_directive` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `effective_directive` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_policy` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `script_sample` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_views`
--

CREATE TABLE `page_views` (
  `id` int(11) NOT NULL,
  `page_url` text DEFAULT NULL,
  `server` text DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `session_id` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_flexicontact_logs`
--

CREATE TABLE `pamonline_flexicontact_logs` (
  `id` int(11) NOT NULL,
  `data` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_mailchimp_logs`
--

CREATE TABLE `pamonline_mailchimp_logs` (
  `id` int(11) NOT NULL,
  `data` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcompanies_admins`
--

CREATE TABLE `pamonline_pamcompanies_admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activated` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcompanies_companies`
--

CREATE TABLE `pamonline_pamcompanies_companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `intro` text COLLATE utf8_unicode_ci NOT NULL,
  `history` text COLLATE utf8_unicode_ci NOT NULL,
  `disclaimer` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcompanies_members`
--

CREATE TABLE `pamonline_pamcompanies_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `activated` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcompanies_rate_questions`
--

CREATE TABLE `pamonline_pamcompanies_rate_questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcompanies_reviews`
--

CREATE TABLE `pamonline_pamcompanies_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `rate` text COLLATE utf8_unicode_ci NOT NULL,
  `general` int(3) NOT NULL DEFAULT 0,
  `activated` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcompanies_users`
--

CREATE TABLE `pamonline_pamcompanies_users` (
  `user_id` int(11) NOT NULL,
  `pam_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_faq_posts`
--

CREATE TABLE `pamonline_pamcontent_faq_posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `section_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_faq_post_section`
--

CREATE TABLE `pamonline_pamcontent_faq_post_section` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_faq_section`
--

CREATE TABLE `pamonline_pamcontent_faq_section` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_galleries`
--

CREATE TABLE `pamonline_pamcontent_galleries` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `dissabled` tinyint(1) DEFAULT NULL,
  `descr` tinytext DEFAULT NULL,
  `group` varchar(60) DEFAULT NULL,
  `option_title` varchar(150) DEFAULT NULL,
  `option_order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_judges`
--

CREATE TABLE `pamonline_pamcontent_judges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` text COLLATE utf8_unicode_ci NOT NULL,
  `year` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `regionid` int(11) NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chair` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_pictures`
--

CREATE TABLE `pamonline_pamcontent_pictures` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `picture_url` varchar(355) DEFAULT NULL,
  `descr` mediumtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_questions`
--

CREATE TABLE `pamonline_pamcontent_questions` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `field_name` varchar(100) NOT NULL,
  `quest_type` varchar(100) NOT NULL DEFAULT 'slider',
  `hint_text` text DEFAULT NULL,
  `background` varchar(80) DEFAULT NULL,
  `cols_per_row` int(11) DEFAULT NULL,
  `hint_title` text DEFAULT NULL,
  `box_height_px` int(11) DEFAULT 110,
  `options_from_table` varchar(150) DEFAULT NULL,
  `from_table_values` varchar(150) DEFAULT NULL,
  `from_table_texts` varchar(150) DEFAULT NULL,
  `sql_where` text DEFAULT NULL,
  `default_option_value` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_question_options`
--

CREATE TABLE `pamonline_pamcontent_question_options` (
  `id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `id_quest` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_pamcontent_team`
--

CREATE TABLE `pamonline_pamcontent_team` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` text COLLATE utf8_unicode_ci NOT NULL,
  `virtual_business_card` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_roles`
--

CREATE TABLE `pamonline_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `default_group` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_roles_assigned_roles`
--

CREATE TABLE `pamonline_roles_assigned_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_roles_permissions`
--

CREATE TABLE `pamonline_roles_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_roles_permission_role`
--

CREATE TABLE `pamonline_roles_permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamonline_salutations`
--

CREATE TABLE `pamonline_salutations` (
  `salutation_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_assetclass`
--

CREATE TABLE `pamtbl_assetclass` (
  `assetclass_id` int(11) NOT NULL,
  `ac_name` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `ac_date` datetime DEFAULT NULL,
  `ac_description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_assetclass`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_assetclass` BEFORE INSERT ON `pamtbl_assetclass` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_assetclass` BEFORE UPDATE ON `pamtbl_assetclass` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_assetcompany`
--

CREATE TABLE `pamtbl_assetcompany` (
  `assetcompany_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `assetclass_id` int(11) NOT NULL,
  `ac_description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `ac_orderno` int(11) DEFAULT NULL,
  `ac_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ac_contactnote` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_assetcompany`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_assetcompany` BEFORE INSERT ON `pamtbl_assetcompany` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_assetcompany` BEFORE UPDATE ON `pamtbl_assetcompany` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_award`
--

CREATE TABLE `pamtbl_award` (
  `award_id` int(11) NOT NULL,
  `awardyear_id` int(11) NOT NULL,
  `award_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `award_rules` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `award_ordernumber` int(11) DEFAULT NULL,
  `award_criterion` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `award_canjudgevote` int(1) DEFAULT 0,
  `award_investment` int(1) DEFAULT 0,
  `award_emprequired` int(1) DEFAULT 0,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardapplication`
--

CREATE TABLE `pamtbl_awardapplication` (
  `awardapplication_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `award_id` int(11) NOT NULL,
  `aa_applied` int(11) DEFAULT 0,
  `aa_reason` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aa_date` datetime DEFAULT NULL,
  `aa_result` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aa_uploadedfiles` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aa_submitted` int(1) DEFAULT 0,
  `aa_showtojudge` int(1) DEFAULT 0,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardchanges`
--

CREATE TABLE `pamtbl_awardchanges` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `award_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `before_save` text NOT NULL,
  `after_save` text NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardemp1`
--

CREATE TABLE `pamtbl_awardemp1` (
  `awardemp1_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `lockedby_user_id` int(11) DEFAULT NULL,
  `lockedby_aepl_id` int(11) DEFAULT NULL,
  `aemp1_award` int(11) DEFAULT NULL,
  `aemp1_date` datetime DEFAULT NULL,
  `awardyear_id` int(11) NOT NULL,
  `aemp1_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_ceo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_hopc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_cio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_custodians` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_banks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_history` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_per1` int(11) DEFAULT NULL,
  `aemp1_pex1` int(11) DEFAULT NULL,
  `aemp1_per2` int(11) DEFAULT NULL,
  `aemp1_pex2` int(11) DEFAULT NULL,
  `aemp1_per3` int(11) DEFAULT NULL,
  `aemp1_pex3` int(11) DEFAULT NULL,
  `aemp1_per4` int(11) DEFAULT NULL,
  `aemp1_pex4` int(11) DEFAULT NULL,
  `aemp1_per5` int(11) DEFAULT NULL,
  `aemp1_pex5` int(11) DEFAULT NULL,
  `aemp1_per6` int(11) DEFAULT NULL,
  `aemp1_pex6` int(11) DEFAULT NULL,
  `aemp1_per7` int(11) DEFAULT NULL,
  `aemp1_pex7` int(11) DEFAULT NULL,
  `aemp1_per8` int(11) DEFAULT NULL,
  `aemp1_pex8` int(11) DEFAULT NULL,
  `aemp1_to1` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_to2` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_activity` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_add_notes` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp1_add_notes2` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardemp2`
--

CREATE TABLE `pamtbl_awardemp2` (
  `awardemp2_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `lockedby_user_id` int(11) DEFAULT NULL,
  `lockedby_aepl_id` int(11) DEFAULT NULL,
  `aemp2_award` int(11) DEFAULT NULL,
  `aemp2_date` datetime DEFAULT NULL,
  `awardyear_id` int(11) NOT NULL,
  `aemp2_discretionary1` int(11) DEFAULT NULL,
  `aemp2_advisory1` int(11) DEFAULT NULL,
  `aemp2_philosophy` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_client_gained1` int(14) DEFAULT NULL,
  `aemp2_client_lost1` int(14) DEFAULT NULL,
  `aemp2_client_net1` int(14) DEFAULT NULL,
  `aemp2_client_gained2` int(14) DEFAULT NULL,
  `aemp2_client_lost2` int(14) DEFAULT NULL,
  `aemp2_client_net2` int(14) DEFAULT NULL,
  `aemp2_asset_gained1` decimal(14,1) DEFAULT NULL,
  `aemp2_asset_lost1` decimal(14,1) DEFAULT NULL,
  `aemp2_asset_net1` decimal(14,1) DEFAULT NULL,
  `aemp2_asset_gained2` decimal(14,1) DEFAULT NULL,
  `aemp2_asset_lost2` decimal(14,1) DEFAULT NULL,
  `aemp2_asset_net2` decimal(14,1) DEFAULT NULL,
  `aemp2_portfolio1` decimal(14,1) DEFAULT NULL,
  `aemp2_portfolio2` decimal(14,1) DEFAULT NULL,
  `aemp2_avg_client` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_avg_family` int(14) DEFAULT NULL,
  `aemp2_process_change` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_derivatives` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_foreign_currency_risk` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_discretionary2` int(14) DEFAULT NULL,
  `aemp2_advisory2` int(14) DEFAULT NULL,
  `aemp2_notes1` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_total1` decimal(14,1) DEFAULT NULL,
  `aemp2_pc1` decimal(14,1) DEFAULT NULL,
  `aemp2_c1` decimal(14,1) DEFAULT NULL,
  `aemp2_pf1` decimal(14,1) DEFAULT NULL,
  `aemp2_i1` decimal(14,1) DEFAULT NULL,
  `aemp2_total2` decimal(14,1) DEFAULT NULL,
  `aemp2_pc2` decimal(14,1) DEFAULT NULL,
  `aemp2_c2` decimal(14,1) DEFAULT NULL,
  `aemp2_pf2` decimal(14,1) DEFAULT NULL,
  `aemp2_i2` decimal(14,1) DEFAULT NULL,
  `aemp2_curr11` decimal(14,1) DEFAULT NULL,
  `aemp2_curr12` decimal(14,1) DEFAULT NULL,
  `aemp2_curr13` decimal(14,1) DEFAULT NULL,
  `aemp2_curr14` decimal(14,1) DEFAULT NULL,
  `aemp2_curr15` decimal(14,1) DEFAULT NULL,
  `aemp2_curr21` decimal(14,1) DEFAULT NULL,
  `aemp2_curr22` decimal(14,1) DEFAULT NULL,
  `aemp2_curr23` decimal(14,1) DEFAULT NULL,
  `aemp2_curr24` decimal(14,1) DEFAULT NULL,
  `aemp2_curr25` decimal(14,1) DEFAULT NULL,
  `aemp2_def1` decimal(14,1) DEFAULT NULL,
  `aemp2_bal1` decimal(14,1) DEFAULT NULL,
  `aemp2_grow1` decimal(14,1) DEFAULT NULL,
  `aemp2_hgrow1` decimal(14,1) DEFAULT NULL,
  `aemp2_other1` decimal(14,1) DEFAULT NULL,
  `aemp2_def2` decimal(14,1) DEFAULT NULL,
  `aemp2_bal2` decimal(14,1) DEFAULT NULL,
  `aemp2_grow2` decimal(14,1) DEFAULT NULL,
  `aemp2_hgrow2` decimal(14,1) DEFAULT NULL,
  `aemp2_other2` decimal(14,1) DEFAULT NULL,
  `aemp2_m11` decimal(14,1) DEFAULT NULL,
  `aemp2_m12` decimal(14,1) DEFAULT NULL,
  `aemp2_m13` decimal(14,1) DEFAULT NULL,
  `aemp2_m14` decimal(14,1) DEFAULT NULL,
  `aemp2_m15` decimal(14,1) DEFAULT NULL,
  `aemp2_m21` decimal(14,1) DEFAULT NULL,
  `aemp2_m22` decimal(14,1) DEFAULT NULL,
  `aemp2_m23` decimal(14,1) DEFAULT NULL,
  `aemp2_m24` decimal(14,1) DEFAULT NULL,
  `aemp2_m25` decimal(14,1) DEFAULT NULL,
  `aemp2_approach` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_policy` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_model` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_consistency` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_research` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_electronic` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_performance` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_gips` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp2_add_notes` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardemp3`
--

CREATE TABLE `pamtbl_awardemp3` (
  `awardemp3_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `lockedby_user_id` int(11) DEFAULT NULL,
  `lockedby_aepl_id` int(11) DEFAULT NULL,
  `aemp3_award` int(11) DEFAULT NULL,
  `aemp3_date` datetime DEFAULT NULL,
  `awardyear_id` int(11) NOT NULL,
  `aemp3_approach` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp3_pooled` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp3_research` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp3_invest` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp3_add_notes` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardemp4`
--

CREATE TABLE `pamtbl_awardemp4` (
  `awardemp4_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `lockedby_user_id` int(11) DEFAULT NULL,
  `lockedby_aepl_id` int(11) DEFAULT NULL,
  `aemp4_award` int(11) DEFAULT NULL,
  `aemp4_date` datetime DEFAULT NULL,
  `awardyear_id` int(11) NOT NULL,
  `aemp4_regulators` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp4_history` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp4_add_notes` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardemp5`
--

CREATE TABLE `pamtbl_awardemp5` (
  `awardemp5_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `lockedby_user_id` int(11) DEFAULT NULL,
  `lockedby_aepl_id` int(11) DEFAULT NULL,
  `aemp5_award` int(11) DEFAULT NULL,
  `aemp5_date` datetime DEFAULT NULL,
  `awardyear_id` int(11) NOT NULL,
  `aemp5_clean` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp5_performance` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp5_adjustment` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp5_commissions` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp5_rebate` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `aemp5_add_notes` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardemp6`
--

CREATE TABLE `pamtbl_awardemp6` (
  `awardemp6_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `lockedby_user_id` int(11) NOT NULL,
  `lockedby_aepl_id` int(11) NOT NULL,
  `aemp6_award` int(11) NOT NULL,
  `aemp6_date` datetime DEFAULT NULL,
  `awardyear_id` int(11) NOT NULL,
  `aemp6_staff_avg1` decimal(10,2) DEFAULT NULL,
  `aemp6_staff_avg2` decimal(10,2) DEFAULT NULL,
  `aemp6_client_gain1` int(11) DEFAULT NULL,
  `aemp6_client_lost1` int(11) DEFAULT NULL,
  `aemp6_client_net1` int(11) DEFAULT NULL,
  `aemp6_asset_gain1` decimal(10,2) DEFAULT NULL,
  `aemp6_asset_lost1` decimal(10,2) DEFAULT NULL,
  `aemp6_asset_net1` decimal(10,2) DEFAULT NULL,
  `aemp6_client_gain2` int(11) DEFAULT NULL,
  `aemp6_client_lost2` int(11) DEFAULT NULL,
  `aemp6_client_net2` int(11) DEFAULT NULL,
  `aemp6_asset_gain2` decimal(10,2) DEFAULT NULL,
  `aemp6_asset_lost2` decimal(10,2) DEFAULT NULL,
  `aemp6_asset_net2` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio1` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio2` decimal(10,2) DEFAULT NULL,
  `aemp6_total1` decimal(10,2) DEFAULT NULL,
  `aemp6_pc1` decimal(10,2) DEFAULT NULL,
  `aemp6_c1` decimal(10,2) DEFAULT NULL,
  `aemp6_pf1` decimal(10,2) DEFAULT NULL,
  `aemp6_i1` decimal(10,2) DEFAULT NULL,
  `aemp6_total2` decimal(10,2) DEFAULT NULL,
  `aemp6_pc2` decimal(10,2) DEFAULT NULL,
  `aemp6_c2` decimal(10,2) DEFAULT NULL,
  `aemp6_pf2` decimal(10,2) DEFAULT NULL,
  `aemp6_i2` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_pound1` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_doll1` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_eur1` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_chf1` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_other1` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_total1` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_pound2` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_doll2` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_eur2` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_chf2` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_other2` decimal(10,2) DEFAULT NULL,
  `aemp6_curr_total2` decimal(10,2) DEFAULT NULL,
  `aemp6_def1` decimal(10,2) DEFAULT NULL,
  `aemp6_bal1` decimal(10,2) DEFAULT NULL,
  `aemp6_grow1` decimal(10,2) DEFAULT NULL,
  `aemp6_oth1` decimal(10,2) DEFAULT NULL,
  `aemp6_tot1` decimal(10,2) DEFAULT NULL,
  `aemp6_def2` decimal(10,2) DEFAULT NULL,
  `aemp6_bal2` decimal(10,2) DEFAULT NULL,
  `aemp6_grow2` decimal(10,2) DEFAULT NULL,
  `aemp6_oth2` decimal(10,2) DEFAULT NULL,
  `aemp6_tot2` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size1_1` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size2_1` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size3_1` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size4_1` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size5_1` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_total_1` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size1_2` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size2_2` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size3_2` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size4_2` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_size5_2` decimal(10,2) DEFAULT NULL,
  `aemp6_portfolio_total_2` decimal(10,2) DEFAULT NULL,
  `pam_created` datetime NOT NULL,
  `pam_modified` datetime NOT NULL,
  `pam_userid` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardemplogin`
--

CREATE TABLE `pamtbl_awardemplogin` (
  `awardemplogin_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `ael_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ael_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ael_expirydate` datetime DEFAULT NULL,
  `ael_readonly` bit(1) DEFAULT NULL,
  `ael_sectionlist` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardfeedback`
--

CREATE TABLE `pamtbl_awardfeedback` (
  `awardfeed_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `awardyear_id` int(11) NOT NULL,
  `af_note` text DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardlink`
--

CREATE TABLE `pamtbl_awardlink` (
  `awardlink_id` int(11) NOT NULL,
  `award_id` int(11) NOT NULL,
  `to_award_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardnote`
--

CREATE TABLE `pamtbl_awardnote` (
  `awardnote_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `award_id` int(11) NOT NULL,
  `aw_note` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardregion`
--

CREATE TABLE `pamtbl_awardregion` (
  `awardregion_id` int(11) NOT NULL,
  `awardregion_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardswon`
--

CREATE TABLE `pamtbl_awardswon` (
  `award_id` int(11) NOT NULL,
  `award_pamid` int(11) DEFAULT NULL,
  `award_awardno` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardvote`
--

CREATE TABLE `pamtbl_awardvote` (
  `awardvote_id` int(11) NOT NULL,
  `awardapplication_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `av_rank` int(11) DEFAULT NULL,
  `av_reason` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_awardyear`
--

CREATE TABLE `pamtbl_awardyear` (
  `awardyear_id` int(11) NOT NULL,
  `ay_year` int(11) NOT NULL,
  `awardregion_id` int(11) NOT NULL DEFAULT 1,
  `ay_entrysoftdeadline` datetime DEFAULT NULL,
  `ay_entryharddeadline` datetime DEFAULT NULL,
  `ay_shortlistdate` datetime DEFAULT NULL,
  `ay_winnerdate` datetime DEFAULT NULL,
  `ay_judgingdate` datetime DEFAULT NULL,
  `ay_judgingends` datetime DEFAULT NULL,
  `ay_entrystarts` datetime DEFAULT NULL,
  `ay_entryends` datetime DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_company`
--

CREATE TABLE `pamtbl_company` (
  `company_id` int(11) NOT NULL,
  `c_companyname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companytype_id` int(11) NOT NULL,
  `c_active` int(11) DEFAULT NULL,
  `c_discmin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_advmin` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_thresholdnotes` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_managedtype` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_segpool` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_indproinv` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_awardname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_numsearches` int(11) DEFAULT NULL,
  `c_totalofposition` int(11) DEFAULT NULL,
  `c_financialyear` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_auditor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_averageportsize` int(11) DEFAULT NULL,
  `c_maxstaff` int(11) DEFAULT NULL,
  `c_maxoffices` int(11) DEFAULT NULL,
  `c_maxproducts` int(11) DEFAULT NULL,
  `c_rankbonus` int(11) DEFAULT NULL,
  `c_fulloffices` int(11) DEFAULT NULL,
  `c_typeback` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_typefront` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_pinsecurity` int(1) DEFAULT 0,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0,
  `c_bankinglicense` tinyint(1) DEFAULT 1,
  `c_roboadvice` tinyint(1) DEFAULT 1,
  `c_custodian` tinyint(4) DEFAULT 0,
  `c_financialplanning` tinyint(1) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_company`
--
DELIMITER $$
CREATE TRIGGER `trg_pamtbl_company` BEFORE UPDATE ON `pamtbl_company` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_company_up` BEFORE INSERT ON `pamtbl_company` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyanswer`
--

CREATE TABLE `pamtbl_companyanswer` (
  `companyanswer_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `companyquestion_id` int(11) NOT NULL,
  `companypersonnel_id` int(11) NOT NULL,
  `ca_text` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companyanswer`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companyanswer` BEFORE INSERT ON `pamtbl_companyanswer` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companyanswer` BEFORE UPDATE ON `pamtbl_companyanswer` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companycommentary`
--

CREATE TABLE `pamtbl_companycommentary` (
  `companycomment_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cc_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_text` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_date` datetime DEFAULT NULL,
  `cc_url` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_fileurl` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_contenttype` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companycommentary`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companycommentary` BEFORE INSERT ON `pamtbl_companycommentary` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companycommentary` BEFORE UPDATE ON `pamtbl_companycommentary` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyconfig`
--

CREATE TABLE `pamtbl_companyconfig` (
  `companyconfig_id` int(11) NOT NULL,
  `ccf_kcddatelock` datetime DEFAULT NULL,
  `ccf_pamlockdown` bit(1) DEFAULT NULL,
  `ccf_pamsignoffdate_basic` datetime DEFAULT NULL,
  `ccf_pamsignoffdate_enhanced` datetime DEFAULT NULL,
  `ccf_pamsignoffdate_premier` datetime DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyfees`
--

CREATE TABLE `pamtbl_companyfees` (
  `companyfee_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `cf_position` int(11) DEFAULT NULL,
  `cf_charge` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cfeetype_id` int(11) DEFAULT NULL,
  `cf_text` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `cf_file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cf_filename` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cf_otherfees` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companyfees`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companyfees` BEFORE INSERT ON `pamtbl_companyfees` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companyfees` BEFORE UPDATE ON `pamtbl_companyfees` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyfeetypes`
--

CREATE TABLE `pamtbl_companyfeetypes` (
  `cfeetype_id` int(11) NOT NULL,
  `cft_type` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cf_charge` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 't',
  `cft_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companyfeetypes`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companyfeetypes` BEFORE INSERT ON `pamtbl_companyfeetypes` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companyfeetypes` BEFORE UPDATE ON `pamtbl_companyfeetypes` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companykcd`
--

CREATE TABLE `pamtbl_companykcd` (
  `companykcd_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `ckcd_year` int(11) DEFAULT NULL,
  `ckcd_clientassets` int(11) DEFAULT NULL,
  `ckcd_clientadvice` int(11) DEFAULT NULL,
  `ckcd_staffemployed` int(11) DEFAULT NULL,
  `ckcd_numstaff` int(11) DEFAULT NULL,
  `ckcd_numclients` int(11) DEFAULT NULL,
  `ckcd_discsplit` decimal(14,1) DEFAULT NULL,
  `ckcd_revenue` int(11) DEFAULT NULL,
  `ckcd_profit` int(11) DEFAULT NULL,
  `ckcd_clientassets_prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_clientadvice_prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_staffemployed_prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_numstaff_prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_numclients_prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_discsplit_prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_revenue_prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_profit_prefix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_clientassets_suffix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_clientadvice_suffix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_staffemployed_suffix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_numstaff_suffix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_numclients_suffix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_discsplit_suffix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_revenue_suffix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcd_profit_suffix` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT current_timestamp(),
  `pam_modified` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companykcd`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companykcd` BEFORE INSERT ON `pamtbl_companykcd` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companykcd` BEFORE UPDATE ON `pamtbl_companykcd` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companykcdnotes`
--

CREATE TABLE `pamtbl_companykcdnotes` (
  `companykcdn_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `ckcdn_explanatorynote` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcdn_marker` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ckcdn_orderno` int(11) DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) NOT NULL DEFAULT 0,
  `ckcdn_superscript` int(11) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companykcdnotes`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companykcdnotes` BEFORE INSERT ON `pamtbl_companykcdnotes` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companykcdnotes` BEFORE UPDATE ON `pamtbl_companykcdnotes` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companylogs`
--

CREATE TABLE `pamtbl_companylogs` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `before_save` text NOT NULL,
  `after_save` text NOT NULL,
  `pam_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companypersonnel`
--

CREATE TABLE `pamtbl_companypersonnel` (
  `companypersonnel_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `cp_order` int(11) DEFAULT NULL,
  `cpsection_id` int(11) NOT NULL,
  `cp_firstname` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_lastname` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_jobtitle` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_salutation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cp_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companypersonnel`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companypersonnel` BEFORE INSERT ON `pamtbl_companypersonnel` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companypersonnel` BEFORE UPDATE ON `pamtbl_companypersonnel` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companypersonnelsection`
--

CREATE TABLE `pamtbl_companypersonnelsection` (
  `cpsection_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `cps_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cps_order` int(11) DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companypersonnelsection`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companypersonnelsection` BEFORE INSERT ON `pamtbl_companypersonnelsection` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companypersonnelsection` BEFORE UPDATE ON `pamtbl_companypersonnelsection` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companypinsusers`
--

CREATE TABLE `pamtbl_companypinsusers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_activated` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyplatform`
--

CREATE TABLE `pamtbl_companyplatform` (
  `companyplatform_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `companyplatform_order` int(11) DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companyplatform`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companyplatform` BEFORE INSERT ON `pamtbl_companyplatform` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companyplatform` BEFORE UPDATE ON `pamtbl_companyplatform` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyquestion`
--

CREATE TABLE `pamtbl_companyquestion` (
  `companyquestion_id` int(11) NOT NULL,
  `cq_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cq_active` bit(1) DEFAULT NULL,
  `companyqsection_id` int(11) DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companyquestion`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companyquestion` BEFORE INSERT ON `pamtbl_companyquestion` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companyquestion` BEFORE UPDATE ON `pamtbl_companyquestion` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyquestionsection`
--

CREATE TABLE `pamtbl_companyquestionsection` (
  `companyqsection_id` int(11) NOT NULL,
  `cqs_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cqs_maxcharsforsection` int(11) DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companyquestionsection`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companyquestionsection` BEFORE INSERT ON `pamtbl_companyquestionsection` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companyquestionsection` BEFORE UPDATE ON `pamtbl_companyquestionsection` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyshareholders`
--

CREATE TABLE `pamtbl_companyshareholders` (
  `companysh_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `csh_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `csh_share` decimal(5,2) DEFAULT NULL,
  `csh_orderno` int(11) DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companyshareholders`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companyshareholders` BEFORE INSERT ON `pamtbl_companyshareholders` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companyshareholders` BEFORE UPDATE ON `pamtbl_companyshareholders` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companysignoff`
--

CREATE TABLE `pamtbl_companysignoff` (
  `companysignoff_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `cs_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cs_comment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cs_date` datetime DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0,
  `c_general_signoff` tinyint(1) DEFAULT NULL,
  `c_about_us_signoff` tinyint(1) DEFAULT NULL,
  `c_history_signoff` tinyint(1) DEFAULT NULL,
  `c_investment_philosophy_signoff` tinyint(1) DEFAULT NULL,
  `c_disclaimer_signoff` tinyint(1) DEFAULT NULL,
  `c_kcd_signoff` tinyint(1) DEFAULT NULL,
  `c_investment_platforms_signoff` tinyint(1) DEFAULT NULL,
  `c_products_and_services_signoff` tinyint(1) DEFAULT NULL,
  `c_offices_signoff` tinyint(1) DEFAULT NULL,
  `c_main_shareholders_signoff` tinyint(1) DEFAULT NULL,
  `c_asset_classes_signoff` tinyint(1) DEFAULT NULL,
  `c_key_personnel_signoff` tinyint(1) DEFAULT NULL,
  `c_publications_signoff` tinyint(1) DEFAULT NULL,
  `c_fees_and_charges_signoff` tinyint(1) DEFAULT NULL,
  `c_news_signoff` tinyint(1) DEFAULT NULL,
  `c_articles_and_press_cuttings_signoff` tinyint(1) DEFAULT NULL,
  `c_investment_commentary_signoff` tinyint(1) DEFAULT NULL,
  `c_minimum_thresholds_signoff` tinyint(1) DEFAULT NULL,
  `c_questions_and_answers_signoff` tinyint(1) DEFAULT NULL,
  `archive` tinyint(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companytext`
--

CREATE TABLE `pamtbl_companytext` (
  `companytext_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `ct_text` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `companytexttype_id` int(11) DEFAULT NULL,
  `pam_created` datetime DEFAULT current_timestamp(),
  `pam_modified` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companytext`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companytext` BEFORE INSERT ON `pamtbl_companytext` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companytext` BEFORE UPDATE ON `pamtbl_companytext` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companytexttypes`
--

CREATE TABLE `pamtbl_companytexttypes` (
  `companytexttype_id` int(11) NOT NULL,
  `ctt_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companytrackevents`
--

CREATE TABLE `pamtbl_companytrackevents` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `session_id` varchar(40) DEFAULT NULL,
  `url` text NOT NULL,
  `action` text NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companytype`
--

CREATE TABLE `pamtbl_companytype` (
  `companytype_id` int(11) NOT NULL,
  `ct_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_companytype`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_companytype` BEFORE INSERT ON `pamtbl_companytype` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_companytype` BEFORE UPDATE ON `pamtbl_companytype` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_companyviews`
--

CREATE TABLE `pamtbl_companyviews` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `url` text NOT NULL,
  `pam_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `server` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_country`
--

CREATE TABLE `pamtbl_country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_country`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_country` BEFORE INSERT ON `pamtbl_country` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_country` BEFORE UPDATE ON `pamtbl_country` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_favourite_companies`
--

CREATE TABLE `pamtbl_favourite_companies` (
  `favouriteid` int(10) NOT NULL,
  `REGUSER_ID` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `PamID` int(10) NOT NULL DEFAULT 0,
  `SrvID` int(10) NOT NULL DEFAULT 0,
  `judge` tinyint(1) NOT NULL DEFAULT 0,
  `subscription` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_office`
--

CREATE TABLE `pamtbl_office` (
  `office_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `o_address1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_address2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_address3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_town` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_county` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_postcode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `o_telephone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_fax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_emailaddress` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_contactname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_contactjobtitle` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o_headoffice` int(11) DEFAULT NULL,
  `o_otheroffice` int(11) DEFAULT NULL,
  `o_ordernouk` int(11) DEFAULT NULL,
  `o_point` point DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0,
  `o_point_x` float DEFAULT NULL,
  `o_point_y` float DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utc_offset` int(2) DEFAULT NULL,
  `o_directory` tinyint(1) DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_office`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_office` BEFORE INSERT ON `pamtbl_office` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_office` BEFORE UPDATE ON `pamtbl_office` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_platform`
--

CREATE TABLE `pamtbl_platform` (
  `platform_id` int(11) NOT NULL,
  `platform_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_platform`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_platform` BEFORE INSERT ON `pamtbl_platform` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_platform` BEFORE UPDATE ON `pamtbl_platform` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_portfolio_search`
--

CREATE TABLE `pamtbl_portfolio_search` (
  `PS_ID` int(11) NOT NULL,
  `PS_Type` char(1) DEFAULT NULL,
  `PS_Name` varchar(50) DEFAULT NULL,
  `PS_ValueL` int(11) DEFAULT NULL,
  `PS_ValueM` int(11) DEFAULT NULL,
  `PS_ValueU` int(11) DEFAULT NULL,
  `PS_BoundL` int(11) DEFAULT NULL,
  `PS_BoundU` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_product`
--

CREATE TABLE `pamtbl_product` (
  `product_id` int(11) NOT NULL,
  `p_name` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `p_date` datetime DEFAULT NULL,
  `p_description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_product`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_product` BEFORE INSERT ON `pamtbl_product` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_product` BEFORE UPDATE ON `pamtbl_product` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_productcompany`
--

CREATE TABLE `pamtbl_productcompany` (
  `productcompany_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `pc_description` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `pc_orderno` int(11) DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_productcompany`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_productcompany` BEFORE INSERT ON `pamtbl_productcompany` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_productcompany` BEFORE UPDATE ON `pamtbl_productcompany` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_registeredbody`
--

CREATE TABLE `pamtbl_registeredbody` (
  `rbody_id` int(11) NOT NULL,
  `rb_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rb_abbrev` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `pamtbl_registeredbody`
--
DELIMITER $$
CREATE TRIGGER `trgset_pamtbl_registeredbody` BEFORE INSERT ON `pamtbl_registeredbody` FOR EACH ROW BEGIN
SET NEW.pam_created = NOW();
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trgup_pamtbl_registeredbody` BEFORE UPDATE ON `pamtbl_registeredbody` FOR EACH ROW BEGIN
SET NEW.pam_modified = CURRENT_TIMESTAMP();
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_registeredbodyhash`
--

CREATE TABLE `pamtbl_registeredbodyhash` (
  `rbh_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `rbody_id` int(11) NOT NULL,
  `pam_created` datetime DEFAULT NULL,
  `pam_modified` datetime DEFAULT NULL,
  `pam_userid` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_search_log`
--

CREATE TABLE `pamtbl_search_log` (
  `SL_ID` int(11) NOT NULL,
  `SL_UserID` int(11) DEFAULT NULL,
  `SL_Date` datetime DEFAULT NULL,
  `SL_Min` varchar(50) DEFAULT NULL,
  `SL_Area` varchar(50) DEFAULT NULL,
  `SL_AdvDisc` varchar(50) DEFAULT NULL,
  `SL_Managed` varchar(50) DEFAULT NULL,
  `SL_SegPool` varchar(50) DEFAULT NULL,
  `SL_ManagerBased` varchar(50) DEFAULT NULL,
  `SL_PnS` mediumtext DEFAULT NULL,
  `SL_Ass` varchar(50) DEFAULT NULL,
  `SL_Risk` varchar(50) DEFAULT NULL,
  `SL_Fundamental` varchar(50) DEFAULT NULL,
  `SL_Charge` varchar(50) DEFAULT NULL,
  `SL_Type` varchar(4) DEFAULT NULL,
  `SL_CapRisk` varchar(50) DEFAULT NULL,
  `SL_Tax` varchar(50) DEFAULT NULL,
  `SL_Nat` varchar(50) DEFAULT NULL,
  `SL_SessionID` varchar(40) DEFAULT NULL,
  `SL_SearchFormType` char(1) DEFAULT NULL,
  `SL_ManagerType` varchar(50) DEFAULT NULL,
  `SL_MinCap` varchar(50) DEFAULT NULL,
  `SL_describes` varchar(255) DEFAULT NULL,
  `SL_knowledge` varchar(255) DEFAULT NULL,
  `SL_reason` varchar(255) DEFAULT NULL,
  `SL_wealth` varchar(255) DEFAULT NULL,
  `SL_block` varchar(255) DEFAULT NULL,
  `SL_viewportfolio` varchar(255) DEFAULT NULL,
  `SL_buyselportfolio` varchar(255) DEFAULT NULL,
  `SL_accesstoinvestment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pamtbl_search_queries`
--

CREATE TABLE `pamtbl_search_queries` (
  `searchid` int(10) NOT NULL,
  `REGUSER_ID` int(10) NOT NULL DEFAULT 0,
  `name` varchar(255) NOT NULL DEFAULT '',
  `search_string` text NOT NULL,
  `date` date NOT NULL DEFAULT '0000-00-00',
  `found` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_blog_categories`
--

CREATE TABLE `rainlab_blog_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_blog_posts`
--

CREATE TABLE `rainlab_blog_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_blog_posts_categories`
--

CREATE TABLE `rainlab_blog_posts_categories` (
  `post_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_location_countries`
--

CREATE TABLE `rainlab_location_countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_pinned` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_location_states`
--

CREATE TABLE `rainlab_location_states` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_notify_notifications`
--

CREATE TABLE `rainlab_notify_notifications` (
  `id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `event_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_notify_notification_rules`
--

CREATE TABLE `rainlab_notify_notification_rules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `config_data` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `condition_data` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_custom` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_notify_rule_actions`
--

CREATE TABLE `rainlab_notify_rule_actions` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `config_data` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_host_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_notify_rule_conditions`
--

CREATE TABLE `rainlab_notify_rule_conditions` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `config_data` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `condition_control_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_host_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rule_host_id` int(10) UNSIGNED DEFAULT NULL,
  `rule_parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_sitemap_definitions`
--

CREATE TABLE `rainlab_sitemap_definitions` (
  `id` int(10) UNSIGNED NOT NULL,
  `theme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `rainlab_user_mail_blockers`
--

CREATE TABLE `rainlab_user_mail_blockers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `template` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `region_setting`
--

CREATE TABLE `region_setting` (
  `setting_id` int(10) UNSIGNED NOT NULL,
  `region_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `region_subscription`
--

CREATE TABLE `region_subscription` (
  `region_id` int(10) UNSIGNED NOT NULL,
  `subscription_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_fields`
--

CREATE TABLE `renatio_formbuilder_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` int(10) UNSIGNED DEFAULT NULL,
  `field_type_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `placeholder` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_attributes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `options` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT 1,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `nest_left` int(11) DEFAULT NULL,
  `nest_right` int(11) DEFAULT NULL,
  `nest_depth` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `wrapper_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `validation_messages` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_field_types`
--

CREATE TABLE `renatio_formbuilder_field_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `markup` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_forms`
--

CREATE TABLE `renatio_formbuilder_forms` (
  `id` int(10) UNSIGNED NOT NULL,
  `template_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `to_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `success_message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error_message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `css_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `response_email_field` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reply_email_field_id` int(10) UNSIGNED DEFAULT NULL,
  `reply_name_field_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_form_logs`
--

CREATE TABLE `renatio_formbuilder_form_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` int(10) UNSIGNED NOT NULL,
  `form_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `renatio_formbuilder_sections`
--

CREATE TABLE `renatio_formbuilder_sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` int(10) UNSIGNED DEFAULT NULL,
  `sort_order` int(10) UNSIGNED DEFAULT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wrapper_begin` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wrapper_end` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_visible` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `report_keywords`
--

CREATE TABLE `report_keywords` (
  `keyword_ubs` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keyword_yn` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responsiv_campaign_lists`
--

CREATE TABLE `responsiv_campaign_lists` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responsiv_campaign_lists_subscribers`
--

CREATE TABLE `responsiv_campaign_lists_subscribers` (
  `list_id` int(10) UNSIGNED NOT NULL,
  `subscriber_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responsiv_campaign_messages`
--

CREATE TABLE `responsiv_campaign_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_id` int(10) UNSIGNED DEFAULT NULL,
  `page` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `syntax_data` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `syntax_fields` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count_subscriber` int(11) DEFAULT 0,
  `count_sent` int(11) DEFAULT 0,
  `count_read` int(11) DEFAULT 0,
  `count_stop` int(11) DEFAULT 0,
  `count_repeat` int(11) DEFAULT 1,
  `is_delayed` tinyint(1) NOT NULL DEFAULT 0,
  `launch_at` timestamp NULL DEFAULT NULL,
  `is_staggered` tinyint(1) NOT NULL DEFAULT 0,
  `stagger_time` int(11) DEFAULT NULL,
  `is_repeating` tinyint(1) NOT NULL DEFAULT 0,
  `repeat_frequency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `groups` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `stagger_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `stagger_count` int(11) DEFAULT NULL,
  `processed_at` timestamp NULL DEFAULT NULL,
  `content_html` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `article_ids` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `site_id` int(10) NOT NULL DEFAULT 1,
  `list_codes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responsiv_campaign_messages_lists`
--

CREATE TABLE `responsiv_campaign_messages_lists` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `list_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responsiv_campaign_messages_subscribers`
--

CREATE TABLE `responsiv_campaign_messages_subscribers` (
  `sent_id` int(10) UNSIGNED NOT NULL,
  `message_id` int(10) UNSIGNED NOT NULL,
  `subscriber_id` int(10) UNSIGNED NOT NULL,
  `processing_time` timestamp NULL DEFAULT NULL,
  `sent_at` timestamp NULL DEFAULT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `stop_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responsiv_campaign_message_statuses`
--

CREATE TABLE `responsiv_campaign_message_statuses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `responsiv_campaign_subscribers`
--

CREATE TABLE `responsiv_campaign_subscribers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unsubscribed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed_ip_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed_at` timestamp NULL DEFAULT NULL,
  `message_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'html',
  `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sendgrid_reports`
--

CREATE TABLE `sendgrid_reports` (
  `id` int(10) UNSIGNED NOT NULL,
  `created` int(11) NOT NULL,
  `reason` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_activity` int(11) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting_subscription`
--

CREATE TABLE `setting_subscription` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_id` int(10) UNSIGNED NOT NULL,
  `subscription_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `setting_user`
--

CREATE TABLE `setting_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `setting_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shahiemseymor_online`
--

CREATE TABLE `shahiemseymor_online` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `last_page` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_event_logs`
--

CREATE TABLE `system_event_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `details` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_files`
--

CREATE TABLE `system_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `disk_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `content_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attachment_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_layouts`
--

CREATE TABLE `system_mail_layouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_css` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_locked` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_partials`
--

CREATE TABLE `system_mail_partials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_mail_templates`
--

CREATE TABLE `system_mail_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_html` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_text` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `layout_id` int(11) DEFAULT NULL,
  `is_custom` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_parameters`
--

CREATE TABLE `system_parameters` (
  `id` int(10) UNSIGNED NOT NULL,
  `namespace` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `group` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_history`
--

CREATE TABLE `system_plugin_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `detail` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_plugin_versions`
--

CREATE TABLE `system_plugin_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `is_disabled` tinyint(1) NOT NULL DEFAULT 0,
  `is_frozen` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_request_logs`
--

CREATE TABLE `system_request_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `status_code` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referer` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_revisions`
--

CREATE TABLE `system_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cast` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `old_value` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_value` longtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `revisionable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisionable_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_settings`
--

CREATE TABLE `system_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `item` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_articlelinkswitch`
--

CREATE TABLE `temp_articlelinkswitch` (
  `id` int(10) UNSIGNED NOT NULL,
  `articleid` int(11) NOT NULL,
  `siteid` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `old_body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `new_body` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `numlinks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_articleshift`
--

CREATE TABLE `temp_articleshift` (
  `id` int(10) UNSIGNED NOT NULL,
  `articleid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_countryconvert`
--

CREATE TABLE `temp_countryconvert` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_id` int(11) NOT NULL,
  `new_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_passwordhash`
--

CREATE TABLE `temp_passwordhash` (
  `id` int(10) UNSIGNED NOT NULL,
  `userid` int(11) NOT NULL,
  `passwordhash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temp_usermigration`
--

CREATE TABLE `temp_usermigration` (
  `id` int(10) UNSIGNED NOT NULL,
  `salutation` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `companyname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branchid` int(11) DEFAULT NULL,
  `companyid` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_author_author`
--

CREATE TABLE `twnepc_author_author` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `biography` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `backend_user_id` int(11) DEFAULT -1,
  `oldtwn_id` int(11) DEFAULT -1,
  `oldepc_id` int(11) DEFAULT -1,
  `site_id` int(11) DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_banners_space`
--

CREATE TABLE `twnepc_banners_space` (
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `banners` text COLLATE utf8_unicode_ci NOT NULL,
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_company_branches`
--

CREATE TABLE `twnepc_company_branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) UNSIGNED NOT NULL,
  `branch_sagecode` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_address1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_address2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_address3` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_city` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_region` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_zip` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `branch_tel` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_fax` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `branch_inactive` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_company_companies`
--

CREATE TABLE `twnepc_company_companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `industry_id` int(10) UNSIGNED DEFAULT NULL,
  `old_companygroup_id` int(11) DEFAULT NULL,
  `company_parentname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_info` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_website` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_inactive` tinyint(4) DEFAULT NULL,
  `company_branches` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_users` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_domain` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `employees_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_fund_fund`
--

CREATE TABLE `twnepc_fund_fund` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_gift`
--

CREATE TABLE `twnepc_gift` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hash` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `viewcount` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `article_id` int(10) UNSIGNED DEFAULT 0,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_membership_memberships`
--

CREATE TABLE `twnepc_membership_memberships` (
  `id` int(10) UNSIGNED NOT NULL,
  `subscription_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `related_users` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(8,2) NOT NULL DEFAULT 0.00,
  `comments` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_membership_subscriptions`
--

CREATE TABLE `twnepc_membership_subscriptions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `interval` int(11) NOT NULL DEFAULT 0,
  `limit` int(11) DEFAULT -1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_news_articles`
--

CREATE TABLE `twnepc_news_articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_id` int(10) UNSIGNED DEFAULT NULL,
  `is_free` tinyint(1) NOT NULL DEFAULT -1,
  `status_id` int(11) DEFAULT -1,
  `summary` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `breaking` tinyint(1) NOT NULL DEFAULT -1,
  `newsfeed` tinyint(1) NOT NULL DEFAULT -1,
  `newsupdate` tinyint(1) NOT NULL DEFAULT -1,
  `published_date` date NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sponsored` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_id` int(10) UNSIGNED DEFAULT NULL,
  `oldtwn_article_id` int(11) DEFAULT -1,
  `oldepc_article_id` int(11) DEFAULT -1,
  `body` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `premium` tinyint(1) NOT NULL DEFAULT 0,
  `exclusive` tinyint(1) NOT NULL DEFAULT 0,
  `front` tinyint(1) NOT NULL DEFAULT 0,
  `order` int(11) DEFAULT NULL,
  `contributor` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_author_id` int(10) UNSIGNED NOT NULL,
  `author_lockedby` int(10) UNSIGNED DEFAULT NULL,
  `author_checkedby` int(11) DEFAULT NULL,
  `subeditor` varchar(191) COLLATE utf8_unicode_ci DEFAULT '0',
  `is_sent` tinyint(1) NOT NULL DEFAULT 0,
  `not_include_in_email` tinyint(1) NOT NULL,
  `files` text COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_news_article_ratings`
--

CREATE TABLE `twnepc_news_article_ratings` (
  `id` int(10) UNSIGNED NOT NULL,
  `rating` int(11) NOT NULL,
  `article_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `site_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Triggers `twnepc_news_article_ratings`
--
DELIMITER $$
CREATE TRIGGER `insert_guest_user_article_rating` BEFORE INSERT ON `twnepc_news_article_ratings` FOR EACH ROW BEGIN
	IF ( NEW.user_id IS NULL ) THEN 
		SET NEW.user_id = 66;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_news_categories`
--

CREATE TABLE `twnepc_news_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NULL',
  `identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NULL',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `site_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `nest_left` int(11) DEFAULT -1,
  `nest_right` int(11) DEFAULT -1,
  `nest_depth` int(11) DEFAULT -1,
  `sort_order` int(11) DEFAULT -1,
  `old_category_id` int(11) DEFAULT 0,
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_news_groups`
--

CREATE TABLE `twnepc_news_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `identifier` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `site_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_news_statuses`
--

CREATE TABLE `twnepc_news_statuses` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `identifier` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_regions_region`
--

CREATE TABLE `twnepc_regions_region` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NULL',
  `identifier` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NULL',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_users_friendarticle`
--

CREATE TABLE `twnepc_users_friendarticle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `article_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `comment` mediumtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_users_industry`
--

CREATE TABLE `twnepc_users_industry` (
  `id` int(10) UNSIGNED NOT NULL,
  `industry_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_users_salesrep`
--

CREATE TABLE `twnepc_users_salesrep` (
  `id` int(10) UNSIGNED NOT NULL,
  `salesrep_oldtwn_id` int(11) DEFAULT NULL,
  `salesrep_oldepc_id` int(11) DEFAULT NULL,
  `salesrep_name` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salesrep_longname` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salesrep_active` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `twnepc_user_accept_settings`
--

CREATE TABLE `twnepc_user_accept_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `site_id` int(10) UNSIGNED NOT NULL,
  `is_accepted` tinyint(1) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `unsubscribes`
-- (See below for the actual view)
--
CREATE TABLE `unsubscribes` (
`subscriber_id` int(10) unsigned
,`name` varchar(191)
,`stop_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `permissions` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_activated` tinyint(1) NOT NULL DEFAULT 0,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'NULL',
  `surname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `middlename` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `is_guest` tinyint(1) NOT NULL DEFAULT 0,
  `is_superuser` tinyint(1) NOT NULL DEFAULT 0,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classification` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iu_compid` int(11) DEFAULT NULL,
  `street_addr` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `mobile` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salutation` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salesrep_id` int(10) UNSIGNED DEFAULT NULL,
  `branch` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `street_addr_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `street_addr_3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_ext` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_updates` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `branch_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(10) UNSIGNED DEFAULT NULL,
  `old_twn_member_id` int(11) DEFAULT NULL,
  `old_epc_member_id` int(11) DEFAULT NULL,
  `old_contact_id` int(11) DEFAULT NULL,
  `old_company_id` int(11) DEFAULT NULL,
  `old_type` int(11) DEFAULT NULL,
  `old_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contactby_phone` tinyint(4) DEFAULT NULL,
  `contactby_fax` tinyint(4) DEFAULT NULL,
  `contactby_email` tinyint(4) DEFAULT NULL,
  `contactby_pam` tinyint(4) DEFAULT 1,
  `contactby_other` tinyint(4) DEFAULT NULL,
  `contact_never` tinyint(4) DEFAULT 0,
  `nomagazine` tinyint(4) DEFAULT NULL,
  `comments` mediumtext COLLATE utf8_unicode_ci DEFAULT NULL,
  `google2fa_token` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google2fa_recovery_codes` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_creation_date` timestamp NULL DEFAULT NULL,
  `primary_usergroup` int(11) DEFAULT NULL,
  `iu_about` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iu_webpage` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iu_blog` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iu_facebook` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iu_twitter` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iu_skype` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iu_icq` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `iu_contactbusinesses` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `backenduser_id` int(11) DEFAULT NULL,
  `location_lat` float DEFAULT NULL,
  `location_lon` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_throttle`
--

CREATE TABLE `user_throttle` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT 0,
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `is_suspended` tinyint(1) NOT NULL DEFAULT 0,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `is_banned` tinyint(1) NOT NULL DEFAULT 0,
  `banned_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_visits`
--

CREATE TABLE `user_visits` (
  `id` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `session_id` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `query_string` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `source` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_article_titles`
-- (See below for the actual view)
--
CREATE TABLE `view_article_titles` (
`title` text
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_article_visits`
-- (See below for the actual view)
--
CREATE TABLE `view_article_visits` (
`article_id` int(10) unsigned
,`article_title` text
,`article_created_at` timestamp
,`article_published_at` date
,`article_is_free` tinyint(1)
,`article_author_id` int(10) unsigned
,`article_author_name` varchar(191)
,`article_admin_author_id` int(10) unsigned
,`article_admin_author_name` varchar(511)
,`article_site_id` int(10) unsigned
,`article_site_name` text
,`article_status_id` int(11)
,`article_status_name` text
,`membership_id` int(10) unsigned
,`visit_created_at` timestamp
,`membership_type` varchar(7)
,`membership_company_id` int(10) unsigned
,`membership_branch_id` int(10) unsigned
,`membership_user_id` int(10) unsigned
,`user_classification` varchar(30)
,`user_company_name` varchar(191)
,`user_company_industry_name` varchar(100)
,`user_country_name` varchar(255)
,`user_branch_country_name` varchar(255)
,`user_branch_name` varchar(191)
,`subscription_id` int(10) unsigned
,`subscription_type` text
,`subscription_interval` int(11)
,`subscription_limit` int(11)
);

-- --------------------------------------------------------

--
-- Structure for view `unsubscribes`
--
DROP TABLE IF EXISTS `unsubscribes`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `unsubscribes`  AS SELECT `unsubscribers`.`subscriber_id` AS `subscriber_id`, `responsiv_campaign_lists`.`name` AS `name`, `unsubscribers`.`stop_at` AS `stop_at` FROM ((`responsiv_campaign_messages_subscribers` `unsubscribers` left join `responsiv_campaign_messages_lists` on(`responsiv_campaign_messages_lists`.`message_id` = `unsubscribers`.`message_id`)) left join `responsiv_campaign_lists` on(`responsiv_campaign_lists`.`id` = `responsiv_campaign_messages_lists`.`list_id`)) WHERE `unsubscribers`.`stop_at` is not null ;

-- --------------------------------------------------------

--
-- Structure for view `view_article_titles`
--
DROP TABLE IF EXISTS `view_article_titles`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_article_titles`  AS SELECT `twnepc_news_articles`.`title` AS `title` FROM `twnepc_news_articles` ORDER BY `twnepc_news_articles`.`published_date` DESC ;

-- --------------------------------------------------------

--
-- Structure for view `view_article_visits`
--
DROP TABLE IF EXISTS `view_article_visits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_article_visits`  AS SELECT `article`.`id` AS `article_id`, `article`.`title` AS `article_title`, `article`.`created_at` AS `article_created_at`, `article`.`published_date` AS `article_published_at`, `article`.`is_free` AS `article_is_free`, `article`.`author_id` AS `article_author_id`, `article_author`.`author_name` AS `article_author_name`, `article`.`admin_author_id` AS `article_admin_author_id`, concat(`article_admin_author`.`first_name`,' ',`article_admin_author`.`last_name`) AS `article_admin_author_name`, `article`.`site_id` AS `article_site_id`, `site`.`theme` AS `article_site_name`, `article`.`status_id` AS `article_status_id`, `article_status`.`name` AS `article_status_name`, `visit`.`membership_id` AS `membership_id`, `visit`.`created_at` AS `visit_created_at`, if(`membership`.`user_id` is not null,'user','company') AS `membership_type`, `membership`.`company_id` AS `membership_company_id`, `membership`.`branch_id` AS `membership_branch_id`, `membership`.`user_id` AS `membership_user_id`, `user`.`classification` AS `user_classification`, `company`.`name` AS `user_company_name`, `industry`.`industry_name` AS `user_company_industry_name`, `user_country`.`name` AS `user_country_name`, `branch_country`.`name` AS `user_branch_country_name`, `branch`.`name` AS `user_branch_name`, `membership`.`subscription_id` AS `subscription_id`, `subscription`.`name` AS `subscription_type`, `subscription`.`interval` AS `subscription_interval`, `subscription`.`limit` AS `subscription_limit` FROM (((((((((((((`twnepc_news_articles` `article` left join `keios_multisite_settings` `site` on(`site`.`id` = `article`.`site_id`)) left join `twnepc_news_statuses` `article_status` on(`article_status`.`id` = `article`.`status_id`)) left join `twnepc_author_author` `article_author` on(`article_author`.`id` = `article`.`author_id`)) left join `backend_users` `article_admin_author` on(`article_admin_author`.`id` = `article`.`admin_author_id`)) left join `article_user` `visit` on(`article`.`id` = `visit`.`article_id`)) left join `twnepc_membership_memberships` `membership` on(`membership`.`id` = `visit`.`membership_id`)) left join `users` `user` on(`user`.`id` = `visit`.`user_id`)) left join `twnepc_company_companies` `company` on(`company`.`id` = `user`.`company_id`)) left join `twnepc_company_branches` `branch` on(`branch`.`id` = `user`.`branch_id`)) left join `twnepc_users_industry` `industry` on(`industry`.`id` = `company`.`industry_id`)) left join `rainlab_location_countries` `branch_country` on(`branch_country`.`id` = `branch`.`country_id`)) left join `rainlab_location_countries` `user_country` on(`user_country`.`id` = `user`.`country_id`)) left join `twnepc_membership_subscriptions` `subscription` on(`subscription`.`id` = `membership`.`subscription_id`)) ORDER BY `article`.`id` ASC ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article_article`
--
ALTER TABLE `article_article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_article_article_id_foreign` (`article_id`),
  ADD KEY `article_article_relation_id_foreign` (`relation_id`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_category_idx_id_id` (`category_id`,`article_id`),
  ADD KEY `article_category_article_id_foreign` (`article_id`);

--
-- Indexes for table `article_fav_user`
--
ALTER TABLE `article_fav_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_fav_user_article_id_foreign` (`article_id`),
  ADD KEY `article_fav_user_user_id_foreign` (`user_id`),
  ADD KEY `article_fav_user_site_id_foreign` (`site_id`);

--
-- Indexes for table `article_fund`
--
ALTER TABLE `article_fund`
  ADD KEY `article_fund_article_id_foreign` (`article_id`),
  ADD KEY `article_fund_fund_id_foreign` (`fund_id`);

--
-- Indexes for table `article_region`
--
ALTER TABLE `article_region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_region_idx_id_id` (`region_id`,`article_id`),
  ADD KEY `article_region_article_id_foreign` (`article_id`);

--
-- Indexes for table `article_user`
--
ALTER TABLE `article_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `article_user_idx_article_id` (`article_id`),
  ADD KEY `article_user_user_id_foreign` (`user_id`),
  ADD KEY `article_user_membership_id_foreign` (`membership_id`),
  ADD KEY `article_user_idx_created_at` (`created_at`);

--
-- Indexes for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backend_users`
--
ALTER TABLE `backend_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_unique` (`login`),
  ADD UNIQUE KEY `email_unique` (`email`),
  ADD KEY `act_code_index` (`activation_code`),
  ADD KEY `reset_code_index` (`reset_password_code`),
  ADD KEY `admin_role_index` (`role_id`);

--
-- Indexes for table `backend_users_groups`
--
ALTER TABLE `backend_users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_unique` (`name`),
  ADD KEY `code_index` (`code`);

--
-- Indexes for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_item_index` (`user_id`,`namespace`,`group`,`item`);

--
-- Indexes for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_unique` (`name`),
  ADD KEY `role_code_index` (`code`);

--
-- Indexes for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `backend_user_throttle_user_id_index` (`user_id`),
  ADD KEY `backend_user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `bedard_blogtags_post_tag`
--
ALTER TABLE `bedard_blogtags_post_tag`
  ADD KEY `bedard_blogtags_post_tag_tag_id_post_id_index` (`tag_id`,`post_id`),
  ADD KEY `bedard_blogtags_post_tag_post_id_foreign` (`post_id`);

--
-- Indexes for table `bedard_blogtags_tags`
--
ALTER TABLE `bedard_blogtags_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bedard_blogtags_tags_name_unique` (`name`);

--
-- Indexes for table `benfreke_menumanager_menus`
--
ALTER TABLE `benfreke_menumanager_menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `benfreke_menumanager_menus_parent_id_index` (`parent_id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `category_subscription`
--
ALTER TABLE `category_subscription`
  ADD KEY `category_subscription_category_id_foreign` (`category_id`),
  ADD KEY `category_subscription_subscription_id_foreign` (`subscription_id`);

--
-- Indexes for table `category_user`
--
ALTER TABLE `category_user`
  ADD KEY `category_user_user_id_foreign` (`user_id`),
  ADD KEY `category_user_category_id_foreign` (`category_id`);

--
-- Indexes for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_data_theme_index` (`theme`);

--
-- Indexes for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cms_theme_logs_type_index` (`type`),
  ADD KEY `cms_theme_logs_theme_index` (`theme`),
  ADD KEY `cms_theme_logs_user_id_index` (`user_id`);

--
-- Indexes for table `company_sections_updates`
--
ALTER TABLE `company_sections_updates`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `company_sections_users`
--
ALTER TABLE `company_sections_users`
  ADD PRIMARY KEY (`user_id`,`company_id`);

--
-- Indexes for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deferred_bindings_master_type_index` (`master_type`),
  ADD KEY `deferred_bindings_master_field_index` (`master_field`),
  ADD KEY `deferred_bindings_slave_type_index` (`slave_type`),
  ADD KEY `deferred_bindings_slave_id_index` (`slave_id`),
  ADD KEY `deferred_bindings_session_key_index` (`session_key`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flynsarmy_menu_menuitems`
--
ALTER TABLE `flynsarmy_menu_menuitems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `flynsarmy_menu_menuitems_menu_id_index` (`menu_id`),
  ADD KEY `flynsarmy_menu_menuitems_enabled_index` (`enabled`),
  ADD KEY `flynsarmy_menu_menuitems_parent_id_index` (`parent_id`);

--
-- Indexes for table `flynsarmy_menu_menus`
--
ALTER TABLE `flynsarmy_menu_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_reserved_at_index` (`queue`,`reserved_at`);

--
-- Indexes for table `keios_multisite_settings`
--
ALTER TABLE `keios_multisite_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `keios_multisite_settings_default_subscription_id_foreign` (`default_subscription_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mja_mail_email_log`
--
ALTER TABLE `mja_mail_email_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mja_mail_email_opens`
--
ALTER TABLE `mja_mail_email_opens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offline_csp_logs`
--
ALTER TABLE `offline_csp_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_views`
--
ALTER TABLE `page_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_flexicontact_logs`
--
ALTER TABLE `pamonline_flexicontact_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_mailchimp_logs`
--
ALTER TABLE `pamonline_mailchimp_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcompanies_admins`
--
ALTER TABLE `pamonline_pamcompanies_admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcompanies_companies`
--
ALTER TABLE `pamonline_pamcompanies_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcompanies_members`
--
ALTER TABLE `pamonline_pamcompanies_members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcompanies_rate_questions`
--
ALTER TABLE `pamonline_pamcompanies_rate_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcompanies_reviews`
--
ALTER TABLE `pamonline_pamcompanies_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_faq_posts`
--
ALTER TABLE `pamonline_pamcontent_faq_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_faq_post_section`
--
ALTER TABLE `pamonline_pamcontent_faq_post_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_faq_section`
--
ALTER TABLE `pamonline_pamcontent_faq_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_galleries`
--
ALTER TABLE `pamonline_pamcontent_galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_judges`
--
ALTER TABLE `pamonline_pamcontent_judges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_pictures`
--
ALTER TABLE `pamonline_pamcontent_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_questions`
--
ALTER TABLE `pamonline_pamcontent_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_question_options`
--
ALTER TABLE `pamonline_pamcontent_question_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_pamcontent_team`
--
ALTER TABLE `pamonline_pamcontent_team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_roles`
--
ALTER TABLE `pamonline_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shahiemseymor_roles_name_unique` (`name`);

--
-- Indexes for table `pamonline_roles_assigned_roles`
--
ALTER TABLE `pamonline_roles_assigned_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_roles_permissions`
--
ALTER TABLE `pamonline_roles_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_roles_permission_role`
--
ALTER TABLE `pamonline_roles_permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamonline_salutations`
--
ALTER TABLE `pamonline_salutations`
  ADD PRIMARY KEY (`salutation_id`);

--
-- Indexes for table `pamtbl_assetclass`
--
ALTER TABLE `pamtbl_assetclass`
  ADD PRIMARY KEY (`assetclass_id`);

--
-- Indexes for table `pamtbl_assetcompany`
--
ALTER TABLE `pamtbl_assetcompany`
  ADD PRIMARY KEY (`assetcompany_id`);

--
-- Indexes for table `pamtbl_award`
--
ALTER TABLE `pamtbl_award`
  ADD PRIMARY KEY (`award_id`);

--
-- Indexes for table `pamtbl_awardapplication`
--
ALTER TABLE `pamtbl_awardapplication`
  ADD PRIMARY KEY (`awardapplication_id`);

--
-- Indexes for table `pamtbl_awardchanges`
--
ALTER TABLE `pamtbl_awardchanges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamtbl_awardemp1`
--
ALTER TABLE `pamtbl_awardemp1`
  ADD PRIMARY KEY (`awardemp1_id`);

--
-- Indexes for table `pamtbl_awardemp2`
--
ALTER TABLE `pamtbl_awardemp2`
  ADD PRIMARY KEY (`awardemp2_id`);

--
-- Indexes for table `pamtbl_awardemp3`
--
ALTER TABLE `pamtbl_awardemp3`
  ADD PRIMARY KEY (`awardemp3_id`);

--
-- Indexes for table `pamtbl_awardemp4`
--
ALTER TABLE `pamtbl_awardemp4`
  ADD PRIMARY KEY (`awardemp4_id`);

--
-- Indexes for table `pamtbl_awardemp5`
--
ALTER TABLE `pamtbl_awardemp5`
  ADD PRIMARY KEY (`awardemp5_id`);

--
-- Indexes for table `pamtbl_awardemp6`
--
ALTER TABLE `pamtbl_awardemp6`
  ADD PRIMARY KEY (`awardemp6_id`);

--
-- Indexes for table `pamtbl_awardemplogin`
--
ALTER TABLE `pamtbl_awardemplogin`
  ADD PRIMARY KEY (`awardemplogin_id`);

--
-- Indexes for table `pamtbl_awardfeedback`
--
ALTER TABLE `pamtbl_awardfeedback`
  ADD PRIMARY KEY (`awardfeed_id`);

--
-- Indexes for table `pamtbl_awardlink`
--
ALTER TABLE `pamtbl_awardlink`
  ADD PRIMARY KEY (`awardlink_id`);

--
-- Indexes for table `pamtbl_awardnote`
--
ALTER TABLE `pamtbl_awardnote`
  ADD PRIMARY KEY (`awardnote_id`);

--
-- Indexes for table `pamtbl_awardregion`
--
ALTER TABLE `pamtbl_awardregion`
  ADD PRIMARY KEY (`awardregion_id`);

--
-- Indexes for table `pamtbl_awardswon`
--
ALTER TABLE `pamtbl_awardswon`
  ADD PRIMARY KEY (`award_id`);

--
-- Indexes for table `pamtbl_awardvote`
--
ALTER TABLE `pamtbl_awardvote`
  ADD PRIMARY KEY (`awardvote_id`);

--
-- Indexes for table `pamtbl_awardyear`
--
ALTER TABLE `pamtbl_awardyear`
  ADD PRIMARY KEY (`awardyear_id`);

--
-- Indexes for table `pamtbl_company`
--
ALTER TABLE `pamtbl_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `pamtbl_companyanswer`
--
ALTER TABLE `pamtbl_companyanswer`
  ADD PRIMARY KEY (`companyanswer_id`);

--
-- Indexes for table `pamtbl_companycommentary`
--
ALTER TABLE `pamtbl_companycommentary`
  ADD PRIMARY KEY (`companycomment_id`);

--
-- Indexes for table `pamtbl_companyconfig`
--
ALTER TABLE `pamtbl_companyconfig`
  ADD PRIMARY KEY (`companyconfig_id`);

--
-- Indexes for table `pamtbl_companyfees`
--
ALTER TABLE `pamtbl_companyfees`
  ADD PRIMARY KEY (`companyfee_id`);

--
-- Indexes for table `pamtbl_companyfeetypes`
--
ALTER TABLE `pamtbl_companyfeetypes`
  ADD PRIMARY KEY (`cfeetype_id`);

--
-- Indexes for table `pamtbl_companykcd`
--
ALTER TABLE `pamtbl_companykcd`
  ADD PRIMARY KEY (`companykcd_id`);

--
-- Indexes for table `pamtbl_companykcdnotes`
--
ALTER TABLE `pamtbl_companykcdnotes`
  ADD PRIMARY KEY (`companykcdn_id`);

--
-- Indexes for table `pamtbl_companylogs`
--
ALTER TABLE `pamtbl_companylogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamtbl_companypersonnel`
--
ALTER TABLE `pamtbl_companypersonnel`
  ADD PRIMARY KEY (`companypersonnel_id`);

--
-- Indexes for table `pamtbl_companypersonnelsection`
--
ALTER TABLE `pamtbl_companypersonnelsection`
  ADD PRIMARY KEY (`cpsection_id`);

--
-- Indexes for table `pamtbl_companypinsusers`
--
ALTER TABLE `pamtbl_companypinsusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamtbl_companyplatform`
--
ALTER TABLE `pamtbl_companyplatform`
  ADD PRIMARY KEY (`companyplatform_id`);

--
-- Indexes for table `pamtbl_companyquestion`
--
ALTER TABLE `pamtbl_companyquestion`
  ADD PRIMARY KEY (`companyquestion_id`);

--
-- Indexes for table `pamtbl_companyquestionsection`
--
ALTER TABLE `pamtbl_companyquestionsection`
  ADD PRIMARY KEY (`companyqsection_id`);

--
-- Indexes for table `pamtbl_companyshareholders`
--
ALTER TABLE `pamtbl_companyshareholders`
  ADD PRIMARY KEY (`companysh_id`);

--
-- Indexes for table `pamtbl_companysignoff`
--
ALTER TABLE `pamtbl_companysignoff`
  ADD PRIMARY KEY (`companysignoff_id`);

--
-- Indexes for table `pamtbl_companytext`
--
ALTER TABLE `pamtbl_companytext`
  ADD PRIMARY KEY (`companytext_id`);

--
-- Indexes for table `pamtbl_companytexttypes`
--
ALTER TABLE `pamtbl_companytexttypes`
  ADD PRIMARY KEY (`companytexttype_id`);

--
-- Indexes for table `pamtbl_companytrackevents`
--
ALTER TABLE `pamtbl_companytrackevents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamtbl_companytype`
--
ALTER TABLE `pamtbl_companytype`
  ADD PRIMARY KEY (`companytype_id`);

--
-- Indexes for table `pamtbl_companyviews`
--
ALTER TABLE `pamtbl_companyviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pamtbl_country`
--
ALTER TABLE `pamtbl_country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `pamtbl_favourite_companies`
--
ALTER TABLE `pamtbl_favourite_companies`
  ADD PRIMARY KEY (`favouriteid`),
  ADD UNIQUE KEY `REGUSER_ID` (`REGUSER_ID`,`PamID`,`SrvID`,`judge`);

--
-- Indexes for table `pamtbl_office`
--
ALTER TABLE `pamtbl_office`
  ADD PRIMARY KEY (`office_id`);

--
-- Indexes for table `pamtbl_platform`
--
ALTER TABLE `pamtbl_platform`
  ADD PRIMARY KEY (`platform_id`);

--
-- Indexes for table `pamtbl_portfolio_search`
--
ALTER TABLE `pamtbl_portfolio_search`
  ADD PRIMARY KEY (`PS_ID`),
  ADD KEY `PS_Type` (`PS_Type`,`PS_ValueL`,`PS_ValueM`,`PS_ValueU`,`PS_BoundL`,`PS_BoundU`);

--
-- Indexes for table `pamtbl_product`
--
ALTER TABLE `pamtbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `pamtbl_productcompany`
--
ALTER TABLE `pamtbl_productcompany`
  ADD PRIMARY KEY (`productcompany_id`);

--
-- Indexes for table `pamtbl_registeredbody`
--
ALTER TABLE `pamtbl_registeredbody`
  ADD PRIMARY KEY (`rbody_id`);

--
-- Indexes for table `pamtbl_registeredbodyhash`
--
ALTER TABLE `pamtbl_registeredbodyhash`
  ADD PRIMARY KEY (`rbh_id`);

--
-- Indexes for table `pamtbl_search_log`
--
ALTER TABLE `pamtbl_search_log`
  ADD PRIMARY KEY (`SL_ID`);

--
-- Indexes for table `pamtbl_search_queries`
--
ALTER TABLE `pamtbl_search_queries`
  ADD PRIMARY KEY (`searchid`);

--
-- Indexes for table `rainlab_blog_categories`
--
ALTER TABLE `rainlab_blog_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_blog_categories_slug_index` (`slug`),
  ADD KEY `rainlab_blog_categories_parent_id_index` (`parent_id`);

--
-- Indexes for table `rainlab_blog_posts`
--
ALTER TABLE `rainlab_blog_posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_blog_posts_user_id_index` (`user_id`),
  ADD KEY `rainlab_blog_posts_slug_index` (`slug`);

--
-- Indexes for table `rainlab_blog_posts_categories`
--
ALTER TABLE `rainlab_blog_posts_categories`
  ADD PRIMARY KEY (`post_id`,`category_id`);

--
-- Indexes for table `rainlab_location_countries`
--
ALTER TABLE `rainlab_location_countries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_location_countries_name_index` (`name`);

--
-- Indexes for table `rainlab_location_states`
--
ALTER TABLE `rainlab_location_states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_location_states_country_id_index` (`country_id`),
  ADD KEY `rainlab_location_states_name_index` (`name`);

--
-- Indexes for table `rainlab_notify_notifications`
--
ALTER TABLE `rainlab_notify_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_notify_notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indexes for table `rainlab_notify_notification_rules`
--
ALTER TABLE `rainlab_notify_notification_rules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_notify_notification_rules_code_index` (`code`);

--
-- Indexes for table `rainlab_notify_rule_actions`
--
ALTER TABLE `rainlab_notify_rule_actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_notify_rule_actions_rule_host_id_index` (`rule_host_id`);

--
-- Indexes for table `rainlab_notify_rule_conditions`
--
ALTER TABLE `rainlab_notify_rule_conditions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_rule_id_type` (`rule_host_id`,`rule_host_type`),
  ADD KEY `rainlab_notify_rule_conditions_rule_host_id_index` (`rule_host_id`),
  ADD KEY `rainlab_notify_rule_conditions_rule_parent_id_index` (`rule_parent_id`);

--
-- Indexes for table `rainlab_sitemap_definitions`
--
ALTER TABLE `rainlab_sitemap_definitions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_sitemap_definitions_theme_index` (`theme`);

--
-- Indexes for table `rainlab_user_mail_blockers`
--
ALTER TABLE `rainlab_user_mail_blockers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rainlab_user_mail_blockers_email_index` (`email`),
  ADD KEY `rainlab_user_mail_blockers_template_index` (`template`),
  ADD KEY `rainlab_user_mail_blockers_user_id_index` (`user_id`);

--
-- Indexes for table `region_setting`
--
ALTER TABLE `region_setting`
  ADD KEY `region_setting_setting_id_foreign` (`setting_id`),
  ADD KEY `region_setting_region_id_foreign` (`region_id`);

--
-- Indexes for table `region_subscription`
--
ALTER TABLE `region_subscription`
  ADD KEY `region_subscription_subscription_id_foreign` (`subscription_id`),
  ADD KEY `region_subscription_region_id_foreign` (`region_id`);

--
-- Indexes for table `renatio_formbuilder_fields`
--
ALTER TABLE `renatio_formbuilder_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `renatio_formbuilder_fields_form_id_index` (`form_id`),
  ADD KEY `renatio_formbuilder_fields_field_type_id_index` (`field_type_id`),
  ADD KEY `renatio_formbuilder_fields_parent_id_index` (`parent_id`),
  ADD KEY `renatio_formbuilder_fields_section_id_index` (`section_id`);

--
-- Indexes for table `renatio_formbuilder_field_types`
--
ALTER TABLE `renatio_formbuilder_field_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `renatio_formbuilder_field_types_code_unique` (`code`);

--
-- Indexes for table `renatio_formbuilder_forms`
--
ALTER TABLE `renatio_formbuilder_forms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `renatio_formbuilder_forms_code_unique` (`code`),
  ADD KEY `renatio_formbuilder_forms_template_id_index` (`template_id`);

--
-- Indexes for table `renatio_formbuilder_form_logs`
--
ALTER TABLE `renatio_formbuilder_form_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `renatio_formbuilder_form_logs_form_id_index` (`form_id`);

--
-- Indexes for table `renatio_formbuilder_sections`
--
ALTER TABLE `renatio_formbuilder_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `renatio_formbuilder_sections_form_id_index` (`form_id`),
  ADD KEY `renatio_formbuilder_sections_sort_order_index` (`sort_order`);

--
-- Indexes for table `responsiv_campaign_lists`
--
ALTER TABLE `responsiv_campaign_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `responsiv_campaign_lists_code_index` (`code`);

--
-- Indexes for table `responsiv_campaign_lists_subscribers`
--
ALTER TABLE `responsiv_campaign_lists_subscribers`
  ADD PRIMARY KEY (`list_id`,`subscriber_id`);

--
-- Indexes for table `responsiv_campaign_messages`
--
ALTER TABLE `responsiv_campaign_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `responsiv_campaign_messages_status_id_index` (`status_id`),
  ADD KEY `responsiv_campaign_messages_page_index` (`page`);

--
-- Indexes for table `responsiv_campaign_messages_lists`
--
ALTER TABLE `responsiv_campaign_messages_lists`
  ADD PRIMARY KEY (`message_id`,`list_id`);

--
-- Indexes for table `responsiv_campaign_messages_subscribers`
--
ALTER TABLE `responsiv_campaign_messages_subscribers`
  ADD PRIMARY KEY (`sent_id`),
  ADD KEY `responsiv_campaign_messages_subscribers_sent_index` (`message_id`,`processing_time`,`sent_at`),
  ADD KEY `responsiv_campaign_messages_subscribers_subscriber_index` (`subscriber_id`);

--
-- Indexes for table `responsiv_campaign_message_statuses`
--
ALTER TABLE `responsiv_campaign_message_statuses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `responsiv_campaign_message_statuses_code_index` (`code`);

--
-- Indexes for table `responsiv_campaign_subscribers`
--
ALTER TABLE `responsiv_campaign_subscribers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `responsiv_campaign_subscribers_email_index` (`email`),
  ADD KEY `responsiv_campaign_subscribers_user_id_foreign` (`user_id`);

--
-- Indexes for table `sendgrid_reports`
--
ALTER TABLE `sendgrid_reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `setting_subscription`
--
ALTER TABLE `setting_subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `setting_subscription_subscription_id_foreign` (`subscription_id`),
  ADD KEY `setting_subscription_setting_id_foreign` (`setting_id`);

--
-- Indexes for table `setting_user`
--
ALTER TABLE `setting_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `setting_user_user_id_foreign` (`user_id`),
  ADD KEY `setting_user_setting_id_foreign` (`setting_id`);

--
-- Indexes for table `shahiemseymor_online`
--
ALTER TABLE `shahiemseymor_online`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shahiemseymor_online_idx_sessionid` (`session_id`),
  ADD KEY `shahiemseymor_online_user_id_index` (`user_id`);

--
-- Indexes for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_event_logs_level_index` (`level`);

--
-- Indexes for table `system_files`
--
ALTER TABLE `system_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_files_field_index` (`field`),
  ADD KEY `system_files_attachment_id_index` (`attachment_id`),
  ADD KEY `system_files_attachment_type_index` (`attachment_type`);

--
-- Indexes for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_mail_templates_layout_id_index` (`layout_id`);

--
-- Indexes for table `system_parameters`
--
ALTER TABLE `system_parameters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_index` (`namespace`,`group`,`item`);

--
-- Indexes for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_history_code_index` (`code`),
  ADD KEY `system_plugin_history_type_index` (`type`);

--
-- Indexes for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_plugin_versions_code_index` (`code`);

--
-- Indexes for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_revisions`
--
ALTER TABLE `system_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`),
  ADD KEY `system_revisions_user_id_index` (`user_id`),
  ADD KEY `system_revisions_field_index` (`field`);

--
-- Indexes for table `system_settings`
--
ALTER TABLE `system_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `system_settings_item_index` (`item`);

--
-- Indexes for table `temp_articlelinkswitch`
--
ALTER TABLE `temp_articlelinkswitch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_articleshift`
--
ALTER TABLE `temp_articleshift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_countryconvert`
--
ALTER TABLE `temp_countryconvert`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_passwordhash`
--
ALTER TABLE `temp_passwordhash`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_usermigration`
--
ALTER TABLE `temp_usermigration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_author_author`
--
ALTER TABLE `twnepc_author_author`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_banners_space`
--
ALTER TABLE `twnepc_banners_space`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twnepc_banners_space_site_id_foreign` (`site_id`);

--
-- Indexes for table `twnepc_company_branches`
--
ALTER TABLE `twnepc_company_branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_company_companies`
--
ALTER TABLE `twnepc_company_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twnepc_company_companies_industry_id_foreign` (`industry_id`);

--
-- Indexes for table `twnepc_fund_fund`
--
ALTER TABLE `twnepc_fund_fund`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_gift`
--
ALTER TABLE `twnepc_gift`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twnepc_gift_user_id_foreign` (`user_id`),
  ADD KEY `twnepc_gift_article_id_foreign` (`article_id`);

--
-- Indexes for table `twnepc_membership_memberships`
--
ALTER TABLE `twnepc_membership_memberships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twnepc_membership_memberships_user_id_foreign` (`user_id`),
  ADD KEY `twnepc_membership_memberships_branch_id_foreign` (`branch_id`),
  ADD KEY `twnepc_membership_memberships_company_id_foreign` (`company_id`),
  ADD KEY `twnepc_membership_memberships_subscription_id_foreign` (`subscription_id`);

--
-- Indexes for table `twnepc_membership_subscriptions`
--
ALTER TABLE `twnepc_membership_subscriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_news_articles`
--
ALTER TABLE `twnepc_news_articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twnepc_news_articles_idx_id_date_title_slug` (`id`,`published_date`,`title`(255),`slug`),
  ADD KEY `twnepc_news_articles_idx_date` (`published_date`),
  ADD KEY `i_twnepc_news_articles_createdat` (`created_at`),
  ADD KEY `twnepc_news_articles_status_id_foreign` (`status_id`),
  ADD KEY `twnepc_news_articles_author_id_foreign` (`author_id`),
  ADD KEY `twnepc_news_articles_admin_author_id_foreign` (`admin_author_id`),
  ADD KEY `twnepc_news_articles_author_lockedby_foreign` (`author_lockedby`),
  ADD KEY `twnepc_news_articles_idx_site_id` (`site_id`),
  ADD KEY `twnepc_news_articles_idx_site_published` (`site_id`,`status_id`,`published_date`,`order`),
  ADD KEY `twnepc_news_articles_idx_slug_site_id` (`slug`,`site_id`),
  ADD KEY `twnepc_news_articles_slug_index` (`slug`),
  ADD KEY `twnepc_news_articles_idx_status_id_site_id_newsfeed` (`status_id`,`site_id`,`newsfeed`);
ALTER TABLE `twnepc_news_articles` ADD FULLTEXT KEY `news_articles_search` (`title`,`summary`,`body`);

--
-- Indexes for table `twnepc_news_article_ratings`
--
ALTER TABLE `twnepc_news_article_ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `i_twnepc_news_article_ratings_articleid` (`article_id`),
  ADD KEY `twnepc_news_article_ratings_user_id_foreign` (`user_id`),
  ADD KEY `twnepc_news_article_ratings_site_id_foreign` (`site_id`);

--
-- Indexes for table `twnepc_news_categories`
--
ALTER TABLE `twnepc_news_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twnepc_news_categories_site_id_foreign` (`site_id`),
  ADD KEY `twnepc_news_categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `twnepc_news_groups`
--
ALTER TABLE `twnepc_news_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_news_statuses`
--
ALTER TABLE `twnepc_news_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_regions_region`
--
ALTER TABLE `twnepc_regions_region`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_users_friendarticle`
--
ALTER TABLE `twnepc_users_friendarticle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twnepc_users_friendarticle_article_id_foreign` (`article_id`),
  ADD KEY `twnepc_users_friendarticle_user_id_foreign` (`user_id`);

--
-- Indexes for table `twnepc_users_industry`
--
ALTER TABLE `twnepc_users_industry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_users_salesrep`
--
ALTER TABLE `twnepc_users_salesrep`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `twnepc_user_accept_settings`
--
ALTER TABLE `twnepc_user_accept_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `twnepc_user_accept_settings_user_id_foreign` (`user_id`),
  ADD KEY `twnepc_user_accept_settings_site_id_foreign` (`site_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_login_unique` (`username`),
  ADD KEY `users_activation_code_index` (`activation_code`),
  ADD KEY `users_reset_password_code_index` (`reset_password_code`),
  ADD KEY `users_login_index` (`username`),
  ADD KEY `users_state_id_index` (`state_id`),
  ADD KEY `users_country_id_index` (`country_id`),
  ADD KEY `users_salesrep_id_foreign` (`salesrep_id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`user_id`,`user_group_id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_groups_code_index` (`code`);

--
-- Indexes for table `user_throttle`
--
ALTER TABLE `user_throttle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_throttle_user_id_index` (`user_id`),
  ADD KEY `user_throttle_ip_address_index` (`ip_address`);

--
-- Indexes for table `user_visits`
--
ALTER TABLE `user_visits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_visits_user_id_index` (`user_id`),
  ADD KEY `user_visits_site_id_index` (`site_id`),
  ADD KEY `user_visits_created_at_index` (`created_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article_article`
--
ALTER TABLE `article_article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_fav_user`
--
ALTER TABLE `article_fav_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_region`
--
ALTER TABLE `article_region`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `article_user`
--
ALTER TABLE `article_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_access_log`
--
ALTER TABLE `backend_access_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_users`
--
ALTER TABLE `backend_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_groups`
--
ALTER TABLE `backend_user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_preferences`
--
ALTER TABLE `backend_user_preferences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_roles`
--
ALTER TABLE `backend_user_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `backend_user_throttle`
--
ALTER TABLE `backend_user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bedard_blogtags_tags`
--
ALTER TABLE `bedard_blogtags_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `benfreke_menumanager_menus`
--
ALTER TABLE `benfreke_menumanager_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_data`
--
ALTER TABLE `cms_theme_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_theme_logs`
--
ALTER TABLE `cms_theme_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deferred_bindings`
--
ALTER TABLE `deferred_bindings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flynsarmy_menu_menuitems`
--
ALTER TABLE `flynsarmy_menu_menuitems`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `flynsarmy_menu_menus`
--
ALTER TABLE `flynsarmy_menu_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `keios_multisite_settings`
--
ALTER TABLE `keios_multisite_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mja_mail_email_log`
--
ALTER TABLE `mja_mail_email_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mja_mail_email_opens`
--
ALTER TABLE `mja_mail_email_opens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offline_csp_logs`
--
ALTER TABLE `offline_csp_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page_views`
--
ALTER TABLE `page_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_flexicontact_logs`
--
ALTER TABLE `pamonline_flexicontact_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_mailchimp_logs`
--
ALTER TABLE `pamonline_mailchimp_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcompanies_admins`
--
ALTER TABLE `pamonline_pamcompanies_admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcompanies_companies`
--
ALTER TABLE `pamonline_pamcompanies_companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcompanies_members`
--
ALTER TABLE `pamonline_pamcompanies_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcompanies_rate_questions`
--
ALTER TABLE `pamonline_pamcompanies_rate_questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcompanies_reviews`
--
ALTER TABLE `pamonline_pamcompanies_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_faq_posts`
--
ALTER TABLE `pamonline_pamcontent_faq_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_faq_post_section`
--
ALTER TABLE `pamonline_pamcontent_faq_post_section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_faq_section`
--
ALTER TABLE `pamonline_pamcontent_faq_section`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_galleries`
--
ALTER TABLE `pamonline_pamcontent_galleries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_judges`
--
ALTER TABLE `pamonline_pamcontent_judges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_pictures`
--
ALTER TABLE `pamonline_pamcontent_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_questions`
--
ALTER TABLE `pamonline_pamcontent_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_question_options`
--
ALTER TABLE `pamonline_pamcontent_question_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_pamcontent_team`
--
ALTER TABLE `pamonline_pamcontent_team`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_roles`
--
ALTER TABLE `pamonline_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_roles_assigned_roles`
--
ALTER TABLE `pamonline_roles_assigned_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_roles_permissions`
--
ALTER TABLE `pamonline_roles_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_roles_permission_role`
--
ALTER TABLE `pamonline_roles_permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamonline_salutations`
--
ALTER TABLE `pamonline_salutations`
  MODIFY `salutation_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_assetclass`
--
ALTER TABLE `pamtbl_assetclass`
  MODIFY `assetclass_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_assetcompany`
--
ALTER TABLE `pamtbl_assetcompany`
  MODIFY `assetcompany_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_award`
--
ALTER TABLE `pamtbl_award`
  MODIFY `award_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardapplication`
--
ALTER TABLE `pamtbl_awardapplication`
  MODIFY `awardapplication_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardchanges`
--
ALTER TABLE `pamtbl_awardchanges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardemp1`
--
ALTER TABLE `pamtbl_awardemp1`
  MODIFY `awardemp1_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardemp2`
--
ALTER TABLE `pamtbl_awardemp2`
  MODIFY `awardemp2_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardemp3`
--
ALTER TABLE `pamtbl_awardemp3`
  MODIFY `awardemp3_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardemp4`
--
ALTER TABLE `pamtbl_awardemp4`
  MODIFY `awardemp4_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardemp5`
--
ALTER TABLE `pamtbl_awardemp5`
  MODIFY `awardemp5_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardemp6`
--
ALTER TABLE `pamtbl_awardemp6`
  MODIFY `awardemp6_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardemplogin`
--
ALTER TABLE `pamtbl_awardemplogin`
  MODIFY `awardemplogin_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardfeedback`
--
ALTER TABLE `pamtbl_awardfeedback`
  MODIFY `awardfeed_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardlink`
--
ALTER TABLE `pamtbl_awardlink`
  MODIFY `awardlink_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardnote`
--
ALTER TABLE `pamtbl_awardnote`
  MODIFY `awardnote_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardregion`
--
ALTER TABLE `pamtbl_awardregion`
  MODIFY `awardregion_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardswon`
--
ALTER TABLE `pamtbl_awardswon`
  MODIFY `award_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardvote`
--
ALTER TABLE `pamtbl_awardvote`
  MODIFY `awardvote_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_awardyear`
--
ALTER TABLE `pamtbl_awardyear`
  MODIFY `awardyear_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_company`
--
ALTER TABLE `pamtbl_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyanswer`
--
ALTER TABLE `pamtbl_companyanswer`
  MODIFY `companyanswer_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companycommentary`
--
ALTER TABLE `pamtbl_companycommentary`
  MODIFY `companycomment_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyconfig`
--
ALTER TABLE `pamtbl_companyconfig`
  MODIFY `companyconfig_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyfees`
--
ALTER TABLE `pamtbl_companyfees`
  MODIFY `companyfee_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyfeetypes`
--
ALTER TABLE `pamtbl_companyfeetypes`
  MODIFY `cfeetype_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companykcd`
--
ALTER TABLE `pamtbl_companykcd`
  MODIFY `companykcd_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companykcdnotes`
--
ALTER TABLE `pamtbl_companykcdnotes`
  MODIFY `companykcdn_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companylogs`
--
ALTER TABLE `pamtbl_companylogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companypersonnel`
--
ALTER TABLE `pamtbl_companypersonnel`
  MODIFY `companypersonnel_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companypersonnelsection`
--
ALTER TABLE `pamtbl_companypersonnelsection`
  MODIFY `cpsection_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companypinsusers`
--
ALTER TABLE `pamtbl_companypinsusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyplatform`
--
ALTER TABLE `pamtbl_companyplatform`
  MODIFY `companyplatform_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyquestion`
--
ALTER TABLE `pamtbl_companyquestion`
  MODIFY `companyquestion_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyquestionsection`
--
ALTER TABLE `pamtbl_companyquestionsection`
  MODIFY `companyqsection_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyshareholders`
--
ALTER TABLE `pamtbl_companyshareholders`
  MODIFY `companysh_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companysignoff`
--
ALTER TABLE `pamtbl_companysignoff`
  MODIFY `companysignoff_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companytext`
--
ALTER TABLE `pamtbl_companytext`
  MODIFY `companytext_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companytexttypes`
--
ALTER TABLE `pamtbl_companytexttypes`
  MODIFY `companytexttype_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companytrackevents`
--
ALTER TABLE `pamtbl_companytrackevents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companytype`
--
ALTER TABLE `pamtbl_companytype`
  MODIFY `companytype_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_companyviews`
--
ALTER TABLE `pamtbl_companyviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_country`
--
ALTER TABLE `pamtbl_country`
  MODIFY `country_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_favourite_companies`
--
ALTER TABLE `pamtbl_favourite_companies`
  MODIFY `favouriteid` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_office`
--
ALTER TABLE `pamtbl_office`
  MODIFY `office_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_platform`
--
ALTER TABLE `pamtbl_platform`
  MODIFY `platform_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_portfolio_search`
--
ALTER TABLE `pamtbl_portfolio_search`
  MODIFY `PS_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_product`
--
ALTER TABLE `pamtbl_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_productcompany`
--
ALTER TABLE `pamtbl_productcompany`
  MODIFY `productcompany_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_registeredbody`
--
ALTER TABLE `pamtbl_registeredbody`
  MODIFY `rbody_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_registeredbodyhash`
--
ALTER TABLE `pamtbl_registeredbodyhash`
  MODIFY `rbh_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_search_log`
--
ALTER TABLE `pamtbl_search_log`
  MODIFY `SL_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pamtbl_search_queries`
--
ALTER TABLE `pamtbl_search_queries`
  MODIFY `searchid` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_blog_categories`
--
ALTER TABLE `rainlab_blog_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_blog_posts`
--
ALTER TABLE `rainlab_blog_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_location_countries`
--
ALTER TABLE `rainlab_location_countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_location_states`
--
ALTER TABLE `rainlab_location_states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_notify_notification_rules`
--
ALTER TABLE `rainlab_notify_notification_rules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_notify_rule_actions`
--
ALTER TABLE `rainlab_notify_rule_actions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_notify_rule_conditions`
--
ALTER TABLE `rainlab_notify_rule_conditions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_sitemap_definitions`
--
ALTER TABLE `rainlab_sitemap_definitions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rainlab_user_mail_blockers`
--
ALTER TABLE `rainlab_user_mail_blockers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_fields`
--
ALTER TABLE `renatio_formbuilder_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_field_types`
--
ALTER TABLE `renatio_formbuilder_field_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_forms`
--
ALTER TABLE `renatio_formbuilder_forms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_form_logs`
--
ALTER TABLE `renatio_formbuilder_form_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `renatio_formbuilder_sections`
--
ALTER TABLE `renatio_formbuilder_sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `responsiv_campaign_lists`
--
ALTER TABLE `responsiv_campaign_lists`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `responsiv_campaign_messages`
--
ALTER TABLE `responsiv_campaign_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `responsiv_campaign_messages_subscribers`
--
ALTER TABLE `responsiv_campaign_messages_subscribers`
  MODIFY `sent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `responsiv_campaign_message_statuses`
--
ALTER TABLE `responsiv_campaign_message_statuses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `responsiv_campaign_subscribers`
--
ALTER TABLE `responsiv_campaign_subscribers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sendgrid_reports`
--
ALTER TABLE `sendgrid_reports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting_subscription`
--
ALTER TABLE `setting_subscription`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `setting_user`
--
ALTER TABLE `setting_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shahiemseymor_online`
--
ALTER TABLE `shahiemseymor_online`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_event_logs`
--
ALTER TABLE `system_event_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_files`
--
ALTER TABLE `system_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_mail_layouts`
--
ALTER TABLE `system_mail_layouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_mail_partials`
--
ALTER TABLE `system_mail_partials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_mail_templates`
--
ALTER TABLE `system_mail_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_parameters`
--
ALTER TABLE `system_parameters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_plugin_history`
--
ALTER TABLE `system_plugin_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_plugin_versions`
--
ALTER TABLE `system_plugin_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_request_logs`
--
ALTER TABLE `system_request_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_revisions`
--
ALTER TABLE `system_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_settings`
--
ALTER TABLE `system_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_articlelinkswitch`
--
ALTER TABLE `temp_articlelinkswitch`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_articleshift`
--
ALTER TABLE `temp_articleshift`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_countryconvert`
--
ALTER TABLE `temp_countryconvert`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_passwordhash`
--
ALTER TABLE `temp_passwordhash`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_usermigration`
--
ALTER TABLE `temp_usermigration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_author_author`
--
ALTER TABLE `twnepc_author_author`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_banners_space`
--
ALTER TABLE `twnepc_banners_space`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_company_branches`
--
ALTER TABLE `twnepc_company_branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_company_companies`
--
ALTER TABLE `twnepc_company_companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_fund_fund`
--
ALTER TABLE `twnepc_fund_fund`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_gift`
--
ALTER TABLE `twnepc_gift`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_membership_memberships`
--
ALTER TABLE `twnepc_membership_memberships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_membership_subscriptions`
--
ALTER TABLE `twnepc_membership_subscriptions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_news_articles`
--
ALTER TABLE `twnepc_news_articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_news_article_ratings`
--
ALTER TABLE `twnepc_news_article_ratings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_news_categories`
--
ALTER TABLE `twnepc_news_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_news_groups`
--
ALTER TABLE `twnepc_news_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_news_statuses`
--
ALTER TABLE `twnepc_news_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_regions_region`
--
ALTER TABLE `twnepc_regions_region`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_users_friendarticle`
--
ALTER TABLE `twnepc_users_friendarticle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_users_industry`
--
ALTER TABLE `twnepc_users_industry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_users_salesrep`
--
ALTER TABLE `twnepc_users_salesrep`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `twnepc_user_accept_settings`
--
ALTER TABLE `twnepc_user_accept_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_throttle`
--
ALTER TABLE `user_throttle`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_visits`
--
ALTER TABLE `user_visits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article_article`
--
ALTER TABLE `article_article`
  ADD CONSTRAINT `article_article_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_article_relation_id_foreign` FOREIGN KEY (`relation_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article_category`
--
ALTER TABLE `article_category`
  ADD CONSTRAINT `article_category_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `twnepc_news_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article_fav_user`
--
ALTER TABLE `article_fav_user`
  ADD CONSTRAINT `article_fav_user_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fav_user_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `keios_multisite_settings` (`id`),
  ADD CONSTRAINT `article_fav_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article_fund`
--
ALTER TABLE `article_fund`
  ADD CONSTRAINT `article_fund_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_fund_fund_id_foreign` FOREIGN KEY (`fund_id`) REFERENCES `twnepc_fund_fund` (`id`);

--
-- Constraints for table `article_region`
--
ALTER TABLE `article_region`
  ADD CONSTRAINT `article_region_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_region_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `twnepc_regions_region` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article_user`
--
ALTER TABLE `article_user`
  ADD CONSTRAINT `article_user_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_user_membership_id_foreign` FOREIGN KEY (`membership_id`) REFERENCES `twnepc_membership_memberships` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `article_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `bedard_blogtags_post_tag`
--
ALTER TABLE `bedard_blogtags_post_tag`
  ADD CONSTRAINT `bedard_blogtags_post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `rainlab_blog_posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bedard_blogtags_post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `bedard_blogtags_tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_subscription`
--
ALTER TABLE `category_subscription`
  ADD CONSTRAINT `category_subscription_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `twnepc_news_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_subscription_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `twnepc_membership_subscriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category_user`
--
ALTER TABLE `category_user`
  ADD CONSTRAINT `category_user_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `twnepc_news_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keios_multisite_settings`
--
ALTER TABLE `keios_multisite_settings`
  ADD CONSTRAINT `keios_multisite_settings_default_subscription_id_foreign` FOREIGN KEY (`default_subscription_id`) REFERENCES `twnepc_membership_subscriptions` (`id`);

--
-- Constraints for table `region_setting`
--
ALTER TABLE `region_setting`
  ADD CONSTRAINT `region_setting_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `twnepc_regions_region` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `region_setting_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `keios_multisite_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `region_subscription`
--
ALTER TABLE `region_subscription`
  ADD CONSTRAINT `region_subscription_region_id_foreign` FOREIGN KEY (`region_id`) REFERENCES `twnepc_regions_region` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `region_subscription_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `twnepc_membership_subscriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `responsiv_campaign_subscribers`
--
ALTER TABLE `responsiv_campaign_subscribers`
  ADD CONSTRAINT `responsiv_campaign_subscribers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `setting_subscription`
--
ALTER TABLE `setting_subscription`
  ADD CONSTRAINT `setting_subscription_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `keios_multisite_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `setting_subscription_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `twnepc_membership_subscriptions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `setting_user`
--
ALTER TABLE `setting_user`
  ADD CONSTRAINT `setting_user_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `keios_multisite_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `setting_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `twnepc_banners_space`
--
ALTER TABLE `twnepc_banners_space`
  ADD CONSTRAINT `twnepc_banners_space_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `keios_multisite_settings` (`id`);

--
-- Constraints for table `twnepc_company_companies`
--
ALTER TABLE `twnepc_company_companies`
  ADD CONSTRAINT `twnepc_company_companies_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `twnepc_users_industry` (`id`);

--
-- Constraints for table `twnepc_gift`
--
ALTER TABLE `twnepc_gift`
  ADD CONSTRAINT `twnepc_gift_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `twnepc_gift_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `twnepc_membership_memberships`
--
ALTER TABLE `twnepc_membership_memberships`
  ADD CONSTRAINT `twnepc_membership_memberships_branch_id_foreign` FOREIGN KEY (`branch_id`) REFERENCES `twnepc_company_branches` (`id`),
  ADD CONSTRAINT `twnepc_membership_memberships_company_id_foreign` FOREIGN KEY (`company_id`) REFERENCES `twnepc_company_companies` (`id`),
  ADD CONSTRAINT `twnepc_membership_memberships_subscription_id_foreign` FOREIGN KEY (`subscription_id`) REFERENCES `twnepc_membership_subscriptions` (`id`),
  ADD CONSTRAINT `twnepc_membership_memberships_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `twnepc_news_articles`
--
ALTER TABLE `twnepc_news_articles`
  ADD CONSTRAINT `twnepc_news_articles_admin_author_id_foreign` FOREIGN KEY (`admin_author_id`) REFERENCES `backend_users` (`id`),
  ADD CONSTRAINT `twnepc_news_articles_author_id_foreign` FOREIGN KEY (`author_id`) REFERENCES `twnepc_author_author` (`id`),
  ADD CONSTRAINT `twnepc_news_articles_author_lockedby_foreign` FOREIGN KEY (`author_lockedby`) REFERENCES `backend_users` (`id`),
  ADD CONSTRAINT `twnepc_news_articles_status_id_foreign` FOREIGN KEY (`status_id`) REFERENCES `twnepc_news_statuses` (`id`);

--
-- Constraints for table `twnepc_news_article_ratings`
--
ALTER TABLE `twnepc_news_article_ratings`
  ADD CONSTRAINT `twnepc_news_article_ratings_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `keios_multisite_settings` (`id`),
  ADD CONSTRAINT `twnepc_news_article_ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `twnepc_news_categories`
--
ALTER TABLE `twnepc_news_categories`
  ADD CONSTRAINT `twnepc_news_categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `twnepc_news_categories` (`id`),
  ADD CONSTRAINT `twnepc_news_categories_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `keios_multisite_settings` (`id`);

--
-- Constraints for table `twnepc_users_friendarticle`
--
ALTER TABLE `twnepc_users_friendarticle`
  ADD CONSTRAINT `twnepc_users_friendarticle_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `twnepc_news_articles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `twnepc_users_friendarticle_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `twnepc_user_accept_settings`
--
ALTER TABLE `twnepc_user_accept_settings`
  ADD CONSTRAINT `twnepc_user_accept_settings_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `keios_multisite_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `twnepc_user_accept_settings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_salesrep_id_foreign` FOREIGN KEY (`salesrep_id`) REFERENCES `twnepc_users_salesrep` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
