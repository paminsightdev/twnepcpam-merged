#!/bin/bash
# running campaign
echo "CRON Starts"
source <(sed -e /^$/d -e /^#/d -e 's/.*/declare -x "&"/g' /var/www/html/.cron.env)
SCRIPT_PATH=${0%/*}
if [ "$0" != "$SCRIPT_PATH" ] && [ "$SCRIPT_PATH" != "" ]; then 
    cd $SCRIPT_PATH
fi
echo "Executing CRON tasks: $SCRIPT_PATH"
/usr/local/bin/php /var/www/html/artisan schedule:run
echo "Executing CAMPAIGN  schedule"
/usr/local/bin/php /var/www/html/artisan campaign:run
echo "Cron finished"
