This plugin extends RainLab.User with possibility to add 2-factor google authentication for user account.

### You may like my other plugins
- [Translate Tabs](http://octobercms.com/plugin/vdomah-translatetabs) - Edit translatable backend form fields and theme options grouped into tabs by locales
- [Front-end hierarchic roles](http://octobercms.com/plugin/vdomah-roles) - Allows to manage access rights based on roles hierarchy
- [Excel](http://octobercms.com/plugin/vdomah-excel) - excel import-export tools
- [JWT Auth API](https://octobercms.com/plugin/vdomah-jwtauth) - Token Authentication for API integrated with RainLab.User
- [Blog Views](https://octobercms.com/plugin/vdomah-blogviews) - Enables blog posts views tracking and displaying popular articles.