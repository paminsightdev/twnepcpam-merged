<?php namespace Vdomah\TwoFactorAuth\Components;

use Cms\Classes\ComponentBase;
use Redirect;
use Session;
use Auth;
use October\Rain\Support\Facades\Flash;
use PragmaRX\Google2FA\Google2FA;
use RainLab\User\Models\User;
use Cms\Classes\Page;

class OneTimePasswordForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'vdomah.twofactorauth::lang.onetimepasswordform.onetimepasswordform',
            'description' => 'vdomah.twofactorauth::lang.onetimepasswordform.onetimepasswordform_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'redirect' => [
                'title'       => 'vdomah.twofactorauth::lang.onetimepasswordform.redirect_title',
                'description' => 'vdomah.twofactorauth::lang.onetimepasswordform.redirect_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'redirect_logged' => [
                'title'       => 'vdomah.twofactorauth::lang.onetimepasswordform.redirect_logged_title',
                'description' => 'vdomah.twofactorauth::lang.onetimepasswordform.redirect_logged_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'msg_code_mismatch' => [
                'title'       => 'vdomah.twofactorauth::lang.onetimepasswordform.msg_code_mismatch_title',
                'description' => 'vdomah.twofactorauth::lang.onetimepasswordform.msg_code_mismatch_desc',
                'default'     => e(trans('vdomah.twofactorauth::lang.onetimepasswordform.msg_code_mismatch')),
            ],
        ];
    }

    public function getRedirectOptions()
    {
        return [''=>e(trans('vdomah.twofactorauth::lang.none'))] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getRedirect_loggedOptions()
    {
        return [''=>e(trans('vdomah.twofactorauth::lang.none'))] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $user_id = Session::get('g2fa-user-id');

        if (!$user_id || !Auth::check() || !Auth::getUser()->google2fa_token) {
            $redirectUrl = $this->controller->pageUrl($this->property('redirect'));
            return Redirect::guest($redirectUrl);
        }

        Auth::logout();
    }

    public function onVerifyPass()
    {
        $user = User::findOrFail(Session::get('g2fa-user-id'));

        if ($this->checkRecoveryCodes(post('otp'), $user)) {
            $verified = true;
        } else {
            $g2fa = new Google2FA;

            if ($g2fa->verifyKey($user->google2fa_token, post('otp'))) {
                $verified = true;
            } else {
                $verified = false;
            }
        }

        if ($verified) {
            Auth::login($user);
            Session::forget('afterLogin', 0);
            Session::forget('g2fa-user-id');

            $redirectUrl = $this->controller->pageUrl($this->property('redirect_logged'));
            return redirect()->intended($redirectUrl);
        } else {
            Flash::error($this->property('msg_code_mismatch'));
            return Redirect::to('/login');
        }
    }

    public function checkRecoveryCodes($otp, $user)
    {
        $codes = $user->google2fa_recovery_codes;

        if (in_array($otp, $codes)) {
            if (($key = array_search($otp, $codes)) !== false) {
                unset($codes[$key]);

                $user->google2fa_recovery_codes = $codes;
                $user->save();
            }

            return true;
        }

        return false;
    }

}