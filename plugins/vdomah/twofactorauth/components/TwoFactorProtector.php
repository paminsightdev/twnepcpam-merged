<?php namespace Vdomah\TwoFactorAuth\Components;

use Redirect;
use Request;
use Session;
use Auth;
use Cms\Classes\ComponentBase;
use Cms\Classes\Page;

class TwoFactorProtector extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'vdomah.twofactorauth::lang.twofactorprotector.twofactorprotector',
            'description' => 'vdomah.twofactorauth::lang.twofactorprotector.twofactorprotector_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'redirect' => [
                'title'       => 'vdomah.twofactorauth::lang.twofactorprotector.redirect_title',
                'description' => 'vdomah.twofactorauth::lang.twofactorprotector.redirect_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'except' => [
                'title'       => 'vdomah.twofactorauth::lang.twofactorprotector.except_title',
                'description' => 'vdomah.twofactorauth::lang.twofactorprotector.except_desc',
                'type'        => 'set',
            ],
        ];
    }

    public function getRedirectOptions()
    {
        return [''=>e(trans('vdomah.twofactorauth::lang.none'))] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getExceptOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Component is initialized.
     */
    public function init()
    {
        if (Request::ajax() && !$this->checkUser2Factor()) {
            abort(403, 'Access denied');
        }
    }

    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun()
    {
        if (!$this->checkUser2Factor()) {
            $redirectUrl = $this->controller->pageUrl($this->property('redirect'));
            return Redirect::guest($redirectUrl);
        }
    }

    /**
     * Checks if the user got valid 2-factor authentication
     * @return bool
     */
    protected function checkUser2Factor()
    {
        $out = true;

        $page_current = str_replace('.htm', '', $this->controller->getPage()->fileName);

        $is_exception_page = in_array($page_current, $this->property('except'));

        if (!$is_exception_page && Session::get('afterLogin') == 1 && Auth::check() && (bool)Auth::getUser()->google2fa_token) {
            Session::put('g2fa-user-id', Auth::getUser()->id);

            $out = false;
        }

        return $out;
    }
}