<?php namespace Vdomah\TwoFactorAuth\Components;

use Lang;
use Flash;
use Auth;
use Session;
use Cms\Classes\ComponentBase;
use PragmaRX\Google2FA\Google2FA;

class TwoFactorActivate extends ComponentBase
{
    public $key;

    public $qrCode;

    public $user_check;

    public function componentDetails()
    {
        return [
            'name'        => 'vdomah.twofactorauth::lang.twofactoractivate.twofactoractivate',
            'description' => 'vdomah.twofactorauth::lang.twofactoractivate.twofactoractivate_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'app_name' => [
                'title'       => 'vdomah.twofactorauth::lang.twofactoractivate.app_name_title',
                'description' => 'vdomah.twofactorauth::lang.twofactoractivate.app_name_desc',
                'default'     => config('app.name'),
            ],
            'msg_enabled' => [
                'title'       => 'vdomah.twofactorauth::lang.twofactoractivate.msg_enabled_title',
                'description' => 'vdomah.twofactorauth::lang.twofactoractivate.msg_enabled_desc',
                'default'     => e(trans('vdomah.twofactorauth::lang.twofactoractivate.msg_enabled')),
            ],
            'msg_disabled' => [
                'title'       => 'vdomah.twofactorauth::lang.twofactoractivate.msg_disabled_title',
                'description' => 'vdomah.twofactorauth::lang.twofactoractivate.msg_disabled_desc',
                'default'     => e(trans('vdomah.twofactorauth::lang.twofactoractivate.msg_disabled')),
            ],
            'msg_code_mismatch' => [
                'title'       => 'vdomah.twofactorauth::lang.twofactoractivate.msg_code_mismatch_title',
                'description' => 'vdomah.twofactorauth::lang.twofactoractivate.msg_code_mismatch_desc',
                'default'     => e(trans('vdomah.twofactorauth::lang.twofactoractivate.msg_code_mismatch')),
            ],
        ];
    }

    public function prepareVars()
    {
        $this->user_check = Auth::check();
        Session::forget('google2fa_recovery_codes');

        $user = Auth::getUser();
//        $user->google2fa_token = null;
//        $user->save();

        if ($user && !(bool)$user->google2fa_token) {
            $g2fa = new Google2FA;
            $g2fa->setAllowInsecureCallToGoogleApis(true);
            $key = $g2fa->generateSecretKey(64);

            $google2fa_url = $g2fa->getQRCodeGoogleUrl(
                $this->property('app_name'),
                $user->email,
                $key
            );

            $this->key = $key;
            $this->qrCode = $google2fa_url;
            $this->page['otp'] = $g2fa->getCurrentOtp($key);
            Session::put('2fa-key', $key);
        }
    }

    public function onRun()
    {
        $this->prepareVars();
    }

    public function onActivateToken()
    {
        $user = Auth::getUser();
        $otp = post('otp');

        if (is_null($otp)) {
            return $this->onDisableToken();
        }

        $secretKey = Session::get('2fa-key');
        if (!$secretKey) {
            return $this->onDisableToken();
        }

        $g2fa = new Google2FA;

        if ($g2fa->verifyKey($secretKey, $otp)) {
            Session::forget('2fa-key');
            $user->google2fa_token = $secretKey;
            $user->generateRecoveryCodes();
            $user->save();

            Session::put('google2fa_recovery_codes', 1);

            Flash::success($this->property('msg_enabled'));

            return response()->json([
                'msg' => $this->property('msg_enabled'),
                'result' => 1,
                '#two-factor-activate' => $this->renderPartial('twoFactorActivate::_form', [
                    'google2fa_recovery_codes' => $user->google2fa_recovery_codes,
                ]),
            ]);
        }

        return response()->json([
            'result' => 0,
            'msg' => $this->property('msg_code_mismatch'),
        ]);
    }

    public function onDisableToken()
    {
        $user = Auth::getUser();
        $user->google2fa_token = null;
        $user->save();

        $this->prepareVars();

        Flash::success($this->property('msg_disabled'));

        return response()->json([
            'msg' => $this->property('msg_disabled'),
            '#two-factor-activate' => $this->renderPartial('twoFactorActivate::_form'),
        ]);
    }
}