<?php namespace Vdomah\TwoFactorAuth;

use RainLab\User\Models\User as UserModel;
use RainLab\User\Controllers\Users as UsersController;
use System\Classes\PluginBase;
use Event;
use Session;
use Auth;

class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    public function registerComponents()
    {
        return [
            'Vdomah\TwoFactorAuth\Components\TwoFactorActivate' => 'twoFactorActivate',
            'Vdomah\TwoFactorAuth\Components\OneTimePasswordForm' => 'oneTimePasswordForm',
            'Vdomah\TwoFactorAuth\Components\TwoFactorProtector' => 'twoFactorProtector',
        ];
    }

    public function registerSettings()
    {
    }

    public function boot()
    {
        Event::listen('rainlab.user.login', function () {
            if (Auth::getUser()->google2fa_token)
                Session::put('afterLogin', 1);
        });

        UserModel::extend(function ($model) {
            $model->addJsonable('google2fa_recovery_codes');

            $model->addDynamicMethod('generateRecoveryCodes', function () use ($model) {
                $codes = [];
                for ($i=0; $i < 10; $i++) {
                    $str = md5(time() . rand(10000, 99999));
                    $codes[] = substr($str, 0, 16);
                }
                $model->google2fa_recovery_codes = $codes;
            });
        });

        UserModel::saving(function($model) {
            if ($this->app->runningInBackend()) {
                if (post('User._2fa_disable')) {
                    $model->google2fa_token = null;
                }
            }
        });

        UsersController::extendFormFields(function($form, $model, $context){

            if (!$model instanceof UserModel)
                return;

            $form->addTabfields([
                '_2fa_disable' => [
                    'label'     => 'vdomah.twofactorauth::lang.user.2fa_label',
                    'tab'       => 'vdomah.twofactorauth::lang.user.2fa_tab',
                    'type'      => 'checkbox',
                    'disabled'  => $model->google2fa_token ? false : true,
                    'comment'   => $model->google2fa_token ? 'vdomah.twofactorauth::lang.user.2fa_comment_enabled' : 'vdomah.twofactorauth::lang.user.2fa_comment_disabled',
                ],
            ]);
        });
    }
}
