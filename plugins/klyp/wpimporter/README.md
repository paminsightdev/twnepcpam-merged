# Wordpress Importer for October CMS
 
## Tech
This plugin will take exported XML file from wordpress and import the entries into OctoberCMS Blog or ProBlog plugin
  * Only allow XML format
  * Blog or ProBlog plugin is required (either)
  * http://octobercms.com/plugin/rainlab-blog
  * http://octobercms.com/plugin/radiantweb-problog
 
## Installation - Manual
Copy all files and folders into /plugins/klyp/wpimporter folder
 
## Notes
  * Blog or ProBlog plugin IS REQUIRED for this plugin to work