<?php namespace Keios\Multisite\Classes;

use Illuminate\Support\Facades\App;
use October\Rain\Support\Traits\Singleton;
use Keios\Multisite\Models\Setting;
use Request;

/**
 * Provides helper methods for Builder CMS components.
 *
 * @package Keios\Multisite
 * @author Balint Gaspar, Bence Dragsits
 */
class SettingHelper
{
    use Singleton;

    static function getCurrentSite(){
        $url = preg_replace("(^https?://)", "", Request::url() );

        $sites = Setting::all();
        foreach($sites as $site){
            $domain = preg_replace("(^https?://)", "", $site->domain );
            if(preg_match('/'.preg_quote($domain, '/').'/i', $url)){
                return $site;
            }
        }
        
        return $sites->first();
    }
    
    static function getSenderAddress($siteId) {
        $site = Setting::find($siteId);
        return $site ? $site->sender_address : '';
    }

    static function getSenderName($siteId) {
        $site = Setting::find($siteId);
        return $site ? $site->sender_name : '';
    }
}
