<?php namespace Keios\Multisite\Models;

use Cache;
use Model;
use Config;
use Request;
use DirectoryIterator;
use Backend\Facades\BackendAuth;

/**
 * Setting Model
 */
class Setting extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array
     */
    public $rules = [
        'domain' => 'required|url',
        'theme'  => 'required',
    ];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'keios_multisite_settings';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['domain', 'theme', 'sender_address', 'sender_name', 'is_protected'];

    public $belongsTo = [
        'subscription' => ['Twnepc\Membership\Models\Subscription', 'key' => 'default_subscription_id'],
    ];

    /*
     * Get all currently available themes, return them to form widget for selection
     */
    /**
     * @return array
     */
    public function getThemeOptions()
    {
        $path = base_path().Config::get('cms.themesPath');
        $themeDirs = [];

        foreach (new DirectoryIterator($path) as $file) {
            if ($file->isDot()) {
                continue;
            }
            if ($file->isDir()) {
                $name = $file->getBasename();
                $themeDirs[$name] = $name;
            }
        }

        return $themeDirs;
    }

    /**
     * @return bool
     * @throws \UnexpectedValueException
     */
    public function beforeSave()
    {
        if ($this->is_protected && preg_match('/'.Request::getHost().'/', $this->domain)) {
            return false;
        }
    }

    public function scopeFilterByBackendUser($query) {

        $user = BackendAuth::getUser();
        if($user && $user->role->code == 'siteadmin') {
            if($user->site) {
                $query->whereIn('id', $user->site->pluck('id'));
            }
        }

        return $query;
    }
    

    /*
     * Update cache after saving new domain - theme set
     */
    public function afterSave()
    {
        // forget current data
        Cache::forget('keios_multisite_settings');

        // get all records available now
        $cacheableRecords = Setting::generateCacheableRecords();

        //save them in cache
        Cache::forever('keios_multisite_settings', $cacheableRecords);
    }

    /**
     * @return array
     */
    public static function generateCacheableRecords()
    {
        $allRecords = Setting::all()->toArray();
        $cacheableRecords = [];

        foreach ($allRecords as $record) {
            $cacheableRecords[$record['domain']] = [
                'theme'        => $record['theme'],
                'is_protected' => $record['is_protected'],
            ];
        }

        return $cacheableRecords;
    }
}
