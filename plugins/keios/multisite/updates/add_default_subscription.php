<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddDefaultSubscription extends Migration
{
    public function up()
    {
        Schema::table('keios_multisite_settings', function ($table) {
            $table->integer('default_subscription_id')->unsigned()->nullable();
        });
    }

    public function down()
    {
        Schema::table('keios_multisite_settings', function ($table) {
            $table->dropColumn('default_subscription_id');
        });
    }

}
