<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddSenderAddress extends Migration
{
    public function up()
    {
        Schema::table('keios_multisite_settings', function ($table) {
            $table->text('sender_address')->nullable();
        });
    }

    public function down()
    {
        Schema::table('keios_multisite_settings', function ($table) {
            $table->dropColumn('sender_address');
        });
    }

}
