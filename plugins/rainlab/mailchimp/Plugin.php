<?php namespace RainLab\MailChimp;

use System\Classes\PluginBase;
use Backend\Facades\BackendAuth;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'MailChimp',
            'description' => 'Provides MailChimp integration services.',
            'author'      => 'Alexey Bobkov, Samuel Georges',
            'icon'        => 'icon-envelope',
            'homepage'    => 'https://github.com/rainlab/mailchimp-plugin'
        ];
    }

    public function registerComponents()
    {
        return [
            'RainLab\MailChimp\Components\Signup' => 'mailSignup'
        ];
    }

    public function registerSettings()
    {
        $backendUser = BackendAuth::getUser();
        if($backendUser->is_superuser == 1) {
            
            // display configuration setting for the mailchimp 
            // only super user can change API key for mail chimp
            return [
                'settings' => [
                    'label'       => 'MailChimp',
                    'icon'        => 'icon-envelope',
                    'description' => 'Configure MailChimp API access.',
                    'class'       => 'RainLab\MailChimp\Models\Settings',
                    'order'       => 600
                ]
            ];
            
        } else {
            
            // hide configuration setting for the mailchimp 
            // when other role based users look at the backend settings
            return [];
            
        }
    }
}
