<?php namespace RainLab\User\Updates;

use RainLab\User\Models\UserGroup;
use October\Rain\Database\Updates\Seeder;

class SeedUserGroupsTable extends Seeder
{
    public function run()
    {
        // check table exists
        if(Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(UserGroup::all()->count() == 0) {
                UserGroup::create([
                    'name' => 'Guest',
                    'code' => 'guest',
                    'description' => 'Default group for guest users.'
                ]);
            }
        }
        
        // check table exists
        if(Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(UserGroup::all()->count() == 0) {
                UserGroup::create([
                    'name' => 'Registered',
                    'code' => 'registered',
                    'description' => 'Default group for registered users.'
                ]);
            }
        }
    }
}
