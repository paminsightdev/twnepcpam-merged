<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use RainLab\User\Models\User;

class UsersRenameLoginToUsername extends Migration
{

    public function up()
    {
        // check table exists
        if(Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(User::all()->count() == 0) {
                
                Schema::table('users', function($table)
                {
                    $table->renameColumn('login', 'username');
                });
            }
        }
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->renameColumn('username', 'login');
        });
    }

}
