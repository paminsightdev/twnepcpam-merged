<?php namespace RainLab\User\Models;

use Str;
use Auth;
use Mail;
use Event;
use October\Rain\Auth\AuthException;
use Illuminate\Support\Facades\Validator;
use October\Rain\Auth\Models\User as UserBase;
use RainLab\User\Models\Settings as UserSettings;
use Illuminate\Support\Facades\DB;

// Pamonline added classes from integration
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use Session;
use ValidationException;
use ApplicationException;
use Flash;
use Twnepc\PamCompanies\Models\Country;
use Twnepc\PamCompanies\Models\Companies;
use Twnepc\Models\User as BackendUser;
use Keios\Multisite\Classes\SettingHelper;
use Twnepc\User\Models\Salutation;
use Twnepc\User\Models\Role;

class User extends UserBase
{
    use \October\Rain\Database\Traits\SoftDelete;

    /**
     * @var string The database table used by the model.
     */
    protected $table = 'users';

    /**
     * Validation rules
     */
    public $rules = [
        'email'    => 'required|between:6,255|email|unique:users',
        'avatar'   => 'nullable|image|max:4000',
        'username' => 'required|between:2,255|unique:users',
        'password' => 'required:create|between:8,255|regex:/^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/|confirmed',
        'password_confirmation' => 'required_with:password|between:8,255|regex:/^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/',
        //'phone' => 'required|regex:/^\+*\d( ?\d){10,14}$/'
    ];
    
    
    public $customMessages = [
        'banned_email' => 'You cannot register with that type of email provider',
        //'phone.regex' => 'The telephone format is invalid.',
        //'password.regex' => 'Password must contain at least one uppercase letter and one number.',
    ];
    
    
    public $attributeNames = [
        'phone' => 'Telephone',
        'company' => 'Company'
    ];
    
    
    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'groups' => [UserGroup::class, 'table' => 'users_groups'],
        'roles' => ['Twnepc\Roles\Models\UserGroup', 'table' => 'pamonline_roles_assigned_roles', 'key'=>'user_id', 'otherKey' => 'role_id'],
        'userRole' => ['Twnepc\User\Models\Role', 'table' => 'pamonline_roles_assigned_roles', 'key'=>'user_id', 'otherKey' => 'role_id'],
        // legacy code, could be refactored...
        'frontend_groups' => ['Twnepc\Roles\Models\Group', 'table' => 'pamonline_roles_assigned_roles', 'otherKey' => 'role_id'],
        'permissions' => ['Twnepc\Roles\Models\Group', 'table' => 'pamonline_roles_assigned_roles', 'otherKey' => 'role_id']
    ];
    
    
    public $belongsTo = [
        'backenduser' => ['Backend\Models\User', 'key' => 'backenduser_id'],
        'thecompany' => ['Twnepc\Company\Models\Company', 'key' => 'company_id'],
        'thebranch' => ['Twnepc\Company\Models\Branch', 'key' => 'branch_id'],
        'industry' => ['Twnepc\User\Models\Industry']
    ];
    
    
    public $hasOne = [
        'member' => ['Twnepc\PamCompanies\Models\Members', 'key' => 'user_id'],
        'pamadmin' => ['Twnepc\PamCompanies\Models\Admins', 'table' => 'pamonline_pamcompanies_admins'],
        'membership' => ['Twnepc\Membership\Models\Membership']
    ];
    
    
    public $attachOne = [
        'avatar' => \System\Models\File::class
    ];
    
    /**
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = [
        'name',
        'surname',
        'login',
        'username',
        'email',
        'password',
        'password_confirmation',
        'gender',
        // PAMOnline fields that are to be mass persisted
        //'salutation',
        //'phone',
        //'zip',
        //'country_id',
        //'contactby_other',
        //'contactby_pam',
        //'iu_contactbusinesses',
        //'company',
        'company_id',
        'branch_id'
        //'location_lat',
        //'location_lon'
    ];
    
    /**
     * Purge attributes from data set.
     */
    protected $purgeable = ['password_confirmation', 'send_invite'];
    
    protected $dates = [
        'last_seen',
        'deleted_at',
        'created_at',
        'updated_at',
        'activated_at',
        'last_login'
    ];
    
    public static $loginAttribute = null;
    
    /**
     * Sends the confirmation email to a user, after activating.
     * @param  string $code
     * @return bool
     */
    public function attemptActivation($code) {
        
        $result = parent::attemptActivation($code);
        if ($result === false) {
            return false;
        }
        
        // when PAMonline do this...
        if(SettingHelper::getCurrentSite()->id == 5) {
            
            if ($mailTemplate = UserSettings::get('welcome_template')) {
                Mail::sendTo($this, $mailTemplate, [
                    'name'  => $this->name,
                    'email' => $this->email
                ]);
            }
            
        } else {
            // when PamInsight do this...
            Event::fire('rainlab.user.activate', [$this]);
        }
        
        return true;
    }

    /**
     * Converts a guest user to a registered one and sends an invitation notification.
     * @return void
     */
    public function convertToRegistered($sendNotification = true)
    {
        // Already a registered user
        if (!$this->is_guest) {
            return;
        }

        if ($sendNotification) {
            $this->generatePassword();
        }

        $this->is_guest = false;
        $this->save();

        if ($sendNotification) {
            $this->sendInvitation();
        }
    }

    //
    // Constructors
    //

    /**
     * Looks up a user by their email address.
     * @return self
     */
    public static function findByEmail($email)
    {
        if (!$email) {
            return;
        }

        return self::where('email', $email)->first();
    }

    //
    // Getters
    //

    /**
     * Gets a code for when the user is persisted to a cookie or session which identifies the user.
     * @return string
     */
    public function getPersistCode()
    {
        $block = UserSettings::get('block_persistence', false);

        if ($block || !$this->persist_code) {
            return parent::getPersistCode();
        }

        return $this->persist_code;
    }

    /**
     * Returns the public image file path to this user's avatar.
     */
    public function getAvatarThumb($size = 25, $options = null)
    {
        if (is_string($options)) {
            $options = ['default' => $options];
        }
        elseif (!is_array($options)) {
            $options = [];
        }

        // Default is "mm" (Mystery man)
        $default = array_get($options, 'default', 'mm');

        if ($this->avatar) {
            return $this->avatar->getThumb($size, $size, $options);
        }
        else {
            return '//www.gravatar.com/avatar/'.
                md5(strtolower(trim($this->email))).
                '?s='.$size.
                '&d='.urlencode($default);
        }
    }

    /**
     * Returns the name for the user's login.
     * @return string
     */
    public function getLoginName()
    {
        if (static::$loginAttribute !== null) {
            return static::$loginAttribute;
        }
        
        return static::$loginAttribute = UserSettings::get('login_attribute', UserSettings::LOGIN_EMAIL);
    }

    public function getCompanystringAttribute() {
        $company = $this->getCompany();
        if ($this->thecompany){
        // NOTE: being $this the object of your current model
              $company = $this->thecompany->name;
        }
        return $company;
    }

    public function getCompany() {
        return $this->company;
    }
    
    
    //
    // Scopes
    //

    public function scopeIsActivated($query)
    {
        return $query->where('is_activated', 1);
    }
    
    
    
    /**
     * @TODO pamonline - changed from 'groups' to 'roles' and 'id' to 'role_id'
     */
    public function scopeFilterByGroup($query, $filter) {
        return $query->whereHas('groups', function($group) use ($filter) {
            $group->whereIn('id', $filter);
        });
    }

    //
    // Events
    //

    /**
     * Before validation event
     * @return void
     */
    public function beforeValidate() {
        
        // PAMOnline only...
        if($this->company != "") {
            
            $company = Companies::where('c_companyname',$this->company)->first();
            if($company){
                $this->company_id = $company->company_id;
                
            } else {
                $this->company_id = 0;
                if($this->isPAM()){
                    $this->rules['company'] = 'exists:pamtbl_company,c_companyname';
                }
            }

        } else {

            $this->company_id = 0;
            if($this->isPAM()) {
                $this->rules['company'] = 'required';
            }
        }
        
        /*
         * Guests are special
         */
        if ($this->is_guest && !$this->password) {
            $this->generatePassword();
        }
        
        /*
         * When the username is not used, the email is substituted.
         */
        if (
            (!$this->username) ||
            ($this->isDirty('email') && $this->getOriginal('email') == $this->username)
        ) {
            $this->username = $this->email;
        }
    }
    
    
    /**
     * After create event
     * @return void
     */
    public function afterCreate()
    {
        $this->restorePurgedValues();

        if ($this->send_invite) {
            $this->sendInvitation();
        }
    }

    /**
     * Before login event
     * @return void
     */
    public function beforeLogin()
    {
        if ($this->is_guest) {
            $login = $this->getLogin();
            throw new AuthException(sprintf(
                'Cannot login user "%s" as they are not registered.', $login
            ));
        }

        parent::beforeLogin();
    }

    /**
     * After login event
     * @return void
     */
    public function afterLogin()
    {
        $this->last_login = $this->freshTimestamp();

        if ($this->trashed()) {
            $this->restore();

            Mail::sendTo($this, 'rainlab.user::mail.reactivate', [
                'name' => $this->name
            ]);

            Event::fire('rainlab.user.reactivate', [$this]);
        }
        else {
            parent::afterLogin();
        }

        Event::fire('rainlab.user.login', [$this]);
    }

    /**
     * After delete event
     * @return void
     */
    public function afterDelete()
    {
        if ($this->isSoftDelete()) {
            Event::fire('twnepc.user.deactivate', [$this]);
            return;
        }

        $this->avatar && $this->avatar->delete();

        parent::afterDelete();
    }

    //
    // Banning
    //

    /**
     * Ban this user, preventing them from signing in.
     * @return void
     */
    public function ban()
    {
        Auth::findThrottleByUserId($this->id)->ban();
    }

    /**
     * Remove the ban on this user.
     * @return void
     */
    public function unban()
    {
        Auth::findThrottleByUserId($this->id)->unban();
    }

    /**
     * Check if the user is banned.
     * @return bool
     */
    public function isBanned()
    {
        $throttle = Auth::createThrottleModel()->where('user_id', $this->id)->first();
        return $throttle ? $throttle->is_banned : false;
    }

    //
    // Last Seen
    //

    /**
     * Checks if the user has been seen in the last 5 minutes, and if not,
     * updates the last_seen timestamp to reflect their online status.
     * @return void
     */
    public function touchLastSeen()
    {
        if ($this->isOnline()) {
            //return;
        }

        $oldTimestamps = $this->timestamps;
        $this->timestamps = false;

        $this
            ->newQuery()
            ->where('id', $this->id)
            ->update(['last_seen' => $this->freshTimestamp()])
        ;

        $this->last_seen = $this->freshTimestamp();
        $this->timestamps = $oldTimestamps;
    }

    /**
     * Returns true if the user has been active within the last 5 minutes.
     * @return bool
     */
    public function isOnline()
    {
        return $this->getLastSeen() > $this->freshTimestamp()->subMinutes(5);
    }

    /**
     * Returns the date this user was last seen.
     * @return Carbon\Carbon
     */
    public function getLastSeen()
    {
        return $this->last_seen ?: $this->created_at;
    }

    //
    // Utils
    //

    /**
     * Returns the variables available when sending a user notification.
     * @return array
     */
    public function getNotificationVars()
    {
        $vars = [
            'name'     => $this->name,
            'email'    => $this->email,
            'username' => $this->username,
            'login'    => $this->getLogin(),
            'password' => $this->getOriginalHashValue('password')
        ];

        /*
         * Extensibility
         */
        $result = Event::fire('rainlab.user.getNotificationVars', [$this]);
        if ($result && is_array($result)) {
            $vars = call_user_func_array('array_merge', $result) + $vars;
        }

        return $vars;
    }

    /**
     * Sends an invitation to the user using template "rainlab.user::mail.invite".
     * @return void
     */
    protected function sendInvitation()
    {
        Mail::sendTo($this, 'rainlab.user::mail.invite', $this->getNotificationVars());
    }

    /**
     * Assigns this user with a random password.
     * @return void
     */
    protected function generatePassword()
    {
        $this->password = $this->password_confirmation = Str::random(6);
    }
    
    
    /**
     * get the membership details from the user record
     * @param mixed|array $emails list of emails in a single dimensional array 
     * @return collection|array results from a query
     */
    public static function getUserIdsForMemberships($emails) {
        
        return User::select('id', 'email', 'name', 'surname', 'branch_id', 'company_id')
                ->whereIn('email', $emails)
                ->get();    
    }
    
    
    /**
     * retrieve the email addresses of all the users
     */
    public static function getEmailsForSubscriptionAutoSwitch() {
        
        // get the users list
        $usersList = DB::table('users')->pluck('email')->toArray();
        
        return $usersList;
    }
    
    /**
     * get the company name according to ID
     * @param integer $companyId
     * @return string $companyName
     */
    public static function getCompanyName($companyId) {
        $companyName = DB::table('twnepc_company_companies')->where('id', $companyId)->pluck('name')[0];
        return $companyName;
    }
    
    /**
     * get the branch name according to ID
     * @param integer $branchId
     * @return string $branchName which is really a location e.g. city
     */
    public static function getBranchName($branchId) {
        $branchName = DB::table('twnepc_company_branches')->where('id', $branchId)->pluck('name')[0];
        return $branchName;
    }
    
    
    
    ///////////////////////////////////////////////////////////////////
    //
    //  PAMONLINE FUNCTIONS PULLED UP INTO THE MAIN USER CLASS
    //
    ///////////////////////////////////////////////////////////////////
    
    
    
    public function getSalutationOptions() {

        $options = [];
        $salutations = Salutation::orderBy('name','ASC')->get();

        foreach($salutations as $salutation){
            $options[$salutation->name] = $salutation->name;
        }

        return $options;
    }
    
    
    
    
    /**
     * get the pamonline country list
     * @todo refactor - use the paminsight country list instead
     */
    public function getCountryIdOptions() {

        $options = [];
        $countries = Country::orderBy('country_name','ASC')->get();

        foreach($countries as $country){
            $options[$country->country_id] = $country->country_name;
        }

        return $options;
    }
    
    
    /**
     * get the pamonline country list
     * @todo refactor - use the paminsight country list instead
     */
    public function getCompanyIdOptions() {

        $options = [];
        $items = Companies::all();
        $options[] = "Select company";

        foreach($items as $item){
            $options[$item->company_id] = $item->c_companyname;
        }

        return $options;
    }
    
    
    public function getUsersOptions() {

        $options = [];
        $users = $this->get();

        foreach($users as $user){
            $options[$user->id] = $user->name." ".$user->surname;
        }

        return $options;
    }
    
    
    public function enterPIN() {

        $pin = post('pin_code','');
        $rules = [
            'pin_code' => ['required','digits:6']
        ];

        $validation = Validator::make(['pin_code' => $pin], $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $validPin = Pins::where('user_id',$this->id)->first();
        if(!$validPin) {
            throw new ValidationException(['PIN' => 'PIN is not valid. Please request new PIN.']);
        }

        if($pin != $validPin->code){
            throw new ValidationException(['PIN' => 'PIN is not valid']);
        }

        Session::put('pin_code', $pin);
        Flash::success("Valid PIN entered");
    }
    
    
    public function isValidPIN() {

        try {
            $this->checkPIN();
            return true;
        } catch (ValidationException $e) {
            return false;
        }
    }
    
    
    public function checkPIN() {

        if($this->pamadmin && $this->pamadmin->company->c_pinsecurity == 0)
            return;

        if($this->member && $this->member->company->c_pinsecurity == 0)
            return;
        
        $pin = Session::get('pin_code',false);

        if(!$pin) {
            throw new ValidationException(['PIN' => 'Please enter your PIN to make changes to your profile.']);
        }

        $validPin = Pins::where('user_id',$this->id)->first();

        if(!$validPin) {
            throw new ValidationException(['PIN' => 'PIN is not valid. Please request new PIN.']);
        }

        if($pin != $validPin->code){
            throw new ValidationException(['PIN' => 'PIN is not valid. Please enter new PIN.']);
        }

        $now = time();

        if($validPin->pamCreated != NULL) {
            
            $pinCreated = strtotime($validPin->pam_created);
            $endOfActivePIN = strtotime('+1 day', $pinCreated);

            if($now > $endOfActivePIN) {
                throw new ValidationException(['PIN' => 'PIN is expired.']);
            }
        }
        
        return true;
    }
    
    
    public function isJudge() {

        if(UserSettings::get('judge_frontend_group',false) == false) {
            throw new ApplicationException("Settings not set.");
        }

        $isJudge = false;
        foreach($this->roles as $role) {

            if($role->id == UserSettings::get('judge_frontend_group')) {
                $isJudge = true;
                break;
            }
        }

        return $isJudge;

    }
    
    
    public function isPAM() {

        $pamsAdminBackendGroup  = CompanySettings::get('pams_admin_frontend_groups');
        $pamsBackendGroup       = CompanySettings::get('pams_frontend_groups');

        if(!$pamsAdminBackendGroup || !$pamsBackendGroup)
            throw new ApplicationException('Must set Company settings.');

        foreach($this->roles as $group) {
            
            if(($group->id == $pamsAdminBackendGroup) || ($group->id == $pamsBackendGroup))
                return true;
            
        }

        return false;

    }
    
    
    /**
     * overriding inherited function
     */
    public function beforeSave() {

	$postCodeCoords = User::checkPostcode($this->zip, $this->country_id);
        $this->location_lon = $postCodeCoords['long'];
        
        $this->location_lat = $postCodeCoords['lat'];
        $this->isUserGroupChanged();

    }
    
    
    public function afterUpdate() {
        $this->isUserCompanyChanged();
    }
    
    
    private function isUserGroupChanged() {

        $newGroups = $this->frontend_groups;
        $newGroup  = null;

        if($newGroups) {
            foreach($newGroups as $key => $val) {

                $newGroup = $val->id;
                break;

            }
        }

        if($newGroup == null)
            return;
        
        $roles = DB::table('pamonline_roles_assigned_roles')->where('user_id', $this->id)->get();

        $oldGroup = 0;

        foreach($roles as $role) {
            $oldGroup = $role->role_id;
            break;
        }

        if($newGroup != $oldGroup) {

            try {
                Event::fire('twnepc.user.group.changed', [$this,$oldGroup,$newGroup]);
            } catch(ValidationException $e) {
                throw $e;
            }
        }
    }
    
    
    private function isUserCompanyChanged() {
        
        if((int) $this->original['company_id'] !== (int) $this->attributes['company_id']) {
            Event::fire('twnepc.user.company.changed', [$this]);
        }
    }
    
    
    public static function checkPostcode($postcode,$country="UK") {

        $key = 'FA22-EE34-HK89-BN33';
        $country = $country;

        $countryAbr = self::IsoCodeOfCountry($country);
        if (empty($countryAbr)) {
            $country = 'gb';
        } else {
            $country = $countryAbr;
        }

        $location = $postcode;
        
        if (empty($location)) return false;

        if (empty($country)) $country = 'UK';

        //$location = '';      
        $url = "http://services.postcodeanywhere.co.uk/Geocoding/International/Geocode/v1.10/xmla.ws?";
        $url .= "&Key=" . urlencode($key);
        $url .= "&Country=" . urlencode($country);
        $url .= "&Location=" . urlencode($location);
        $xml = file_get_contents($url);

        $latitude = self::textBetween($xml, 'Latitude="', '"');
        $longitude = self::textBetween($xml, 'Longitude="', '"');


        if ((empty($latitude) or empty($longitude))) {
            // Check for NON UK - if so allow.
            if ($country == 'United Kingdom' OR $country == 'UK' OR $country == 'gb') {
                return false;
            } else {
                return ["lat" => "51.51423", "long" => "-0.09357"]; // Long and Lat for PAM offices. EC2V 6DN.
            }
        }

        return ["lat" => $latitude, "long" => $longitude];      
    }
    
    
    public static function textBetween($text, $tag1, $tag2) {

        $strStart = strpos($text, $tag1);
        if ($strStart === false) {
            return false;
        }

        $strStart += strlen($tag1);
        $strEnd = strpos($text, $tag2, $strStart);

        if ($strEnd === false) {
            return false;
        }

        return substr($text, $strStart, $strEnd - $strStart);
    }
    
    
    
    public static function IsoCodeOfCountry($country) {

        $countriesIso = ['afghanistan' => 'af',

            'albania' => 'al',

            'algeria' => 'dz',

            'american samoa' => 'as',

            'andorra' => 'ad',

            'angola' => 'ao',

            'anguilla' => 'ai',

            'antarctica' => 'aq',

            'antigua and barbuda' => 'ag',

            'antigua & barbuda' => 'ag',

            'argentina' => 'ar',

            'armenia' => 'am',

            'aruba' => 'aw',

            'australia' => 'au',

            'austria' => 'at',

            'azerbaijan' => 'az',

            'bahamas' => 'bs',

            'bahrain' => 'bh',

            'bangladesh' => 'bd',

            'barbados' => 'bb',

            'belarus' => 'by',

            'belgium' => 'be',

            'belize' => 'bz',

            'benin' => 'bj',

            'bermuda' => 'bm',

            'bhutan' => 'bt',

            'bolivia, plurinational state of' => 'bo',

            'bonaire, sint eustatius and saba' => 'bq',

            'bosnia and herzegovina' => 'ba',

            'botswana' => 'bw',

            'bouvet island' => 'bv',

            'brazil' => 'br',

            'british indian ocean territory' => 'io',

            'brunei darussalam' => 'bn',

            'bulgaria' => 'bg',

            'burkina faso' => 'bf',

            'burundi' => 'bi',

            'cambodia' => 'kh',

            'cameroon' => 'cm',

            'canada' => 'ca',

            'cape verde' => 'cv',

            'cayman islands' => 'ky',

            'central african republic' => 'cf',

            'chad' => 'td',

            'chile' => 'cl',

            'china' => 'cn',

            'christmas island' => 'cx',

            'cocos (keeling) islands' => 'cc',

            'colombia' => 'co',

            'comoros' => 'km',

            'congo' => 'cg',

            'congo, the democratic republic of the' => 'cd',

            'cook islands' => 'ck',

            'costa rica' => 'cr',

            'country name' => 'code',

            'croatia' => 'hr',

            'cuba' => 'cu',

            'curaçao' => 'cw',

            'cyprus' => 'cy',

            'czech republic' => 'cz',

            "côte d'ivoire" => 'ci',

            'denmark' => 'dk',

            'djibouti' => 'dj',

            'dominica' => 'dm',

            'dominican republic' => 'do',

            'ecuador' => 'ec',

            'egypt' => 'eg',

            'el salvador' => 'sv',

            'equatorial guinea' => 'gq',

            'eritrea' => 'er',

            'estonia' => 'ee',

            'ethiopia' => 'et',

            'falkland islands (malvinas)' => 'fk',

            'faroe islands' => 'fo',

            'fiji' => 'fj',

            'finland' => 'fi',

            'france' => 'fr',

            'french guiana' => 'gf',

            'french polynesia' => 'pf',

            'french southern territories' => 'tf',

            'gabon' => 'ga',

            'gambia' => 'gm',

            'georgia' => 'ge',

            'germany' => 'de',

            'ghana' => 'gh',

            'gibraltar' => 'gi',

            'greece' => 'gr',

            'greenland' => 'gl',

            'grenada' => 'gd',

            'guadeloupe' => 'gp',

            'guam' => 'gu',

            'guatemala' => 'gt',

            'guernsey' => 'gg',

            'guernsey, channel islands' => 'gg',

            'guinea' => 'gn',

            'guinea-bissau' => 'gw',

            'guyana' => 'gy',

            'haiti' => 'ht',

            'heard island and mcdonald islands' => 'hm',

            'holy see (vatican city state)' => 'va',

            'honduras' => 'hn',

            'hong kong' => 'hk',

            'hungary' => 'hu',

            'iso 3166-2=>gb' => '(.uk)',

            'iceland' => 'is',

            'india' => 'in',

            'indonesia' => 'id',

            'iran, islamic republic of' => 'ir',

            'iraq' => 'iq',

            'ireland' => 'ie',

            'isle of man' => 'im',

            'israel' => 'il',

            'italy' => 'it',

            'jamaica' => 'jm',

            'japan' => 'jp',

            'jersey' => 'je',

            'jersey, channel islands' => 'je',

            'jordan' => 'jo',

            'kazakhstan' => 'kz',

            'kenya' => 'ke',

            'kiribati' => 'ki',

            "korea, democratic people's republic of" => 'kp',

            'korea, republic of' => 'kr',

            'korea (south)' => 'kr',

            'kuwait' => 'kw',

            'kyrgyzstan' => 'kg',

            "lao people's democratic republic" => 'la',

            'latvia' => 'lv',

            'lebanon' => 'lb',

            'lesotho' => 'ls',

            'liberia' => 'lr',

            'libya' => 'ly',

            'liechtenstein' => 'li',

            'lithuania' => 'lt',

            'luxembourg' => 'lu',

            'macao' => 'mo',

            'macedonia, the former yugoslav republic of' => 'mk',

            'madagascar' => 'mg',

            'malawi' => 'mw',

            'malaysia' => 'my',

            'maldives' => 'mv',

            'mali' => 'ml',

            'malta' => 'mt',

            'marshall islands' => 'mh',

            'martinique' => 'mq',

            'mauritania' => 'mr',

            'mauritius' => 'mu',

            'mayotte' => 'yt',

            'mexico' => 'mx',

            'micronesia, federated states of' => 'fm',

            'moldova, republic of' => 'md',

            'monaco' => 'mc',

            'mongolia' => 'mn',

            'montenegro' => 'me',

            'montserrat' => 'ms',

            'morocco' => 'ma',

            'mozambique' => 'mz',

            'myanmar' => 'mm',

            'namibia' => 'na',

            'nauru' => 'nr',

            'nepal' => 'np',

            'netherlands' => 'nl',

            'new caledonia' => 'nc',

            'new zealand' => 'nz',

            'nicaragua' => 'ni',

            'niger' => 'ne',

            'nigeria' => 'ng',

            'niue' => 'nu',

            'norfolk island' => 'nf',

            'northern mariana islands' => 'mp',

            'norway' => 'no',

            'oman' => 'om',

            'pakistan' => 'pk',

            'palau' => 'pw',

            'palestine, state of' => 'ps',

            'panama' => 'pa',

            'papua new guinea' => 'pg',

            'paraguay' => 'py',

            'peru' => 'pe',

            'philippines' => 'ph',

            'pitcairn' => 'pn',

            'poland' => 'pl',

            'portugal' => 'pt',

            'puerto rico' => 'pr',

            'qatar' => 'qa',

            'romania' => 'ro',

            'russian federation' => 'ru',

            'rwanda' => 'rw',

            'réunion' => 're',

            'saint barthélemy' => 'bl',

            'saint helena, ascension and tristan da cunha' => 'sh',

            'saint kitts and nevis' => 'kn',

            'saint lucia' => 'lc',

            'saint martin (french part)' => 'mf',

            'saint pierre and miquelon' => 'pm',

            'saint vincent and the grenadines' => 'vc',

            'samoa' => 'ws',

            'san marino' => 'sm',

            'sao tome and principe' => 'st',

            'saudi arabia' => 'sa',

            'senegal' => 'sn',

            'serbia' => 'rs',

            'seychelles' => 'sc',

            'sierra leone' => 'sl',

            'singapore' => 'sg',

            'sint maarten (dutch part)' => 'sx',

            'slovakia' => 'sk',

            'slovenia' => 'si',

            'solomon islands' => 'sb',

            'somalia' => 'so',

            'south africa' => 'za',

            'south georgia and the south sandwich islands' => 'gs',

            'south sudan' => 'ss',

            'spain' => 'es',

            'sri lanka' => 'lk',

            'sudan' => 'sd',

            'suriname' => 'sr',

            'svalbard and jan mayen' => 'sj',

            'swaziland' => 'sz',

            'sweden' => 'se',

            'switzerland' => 'ch',

            'syrian arab republic' => 'sy',

            'taiwan, province of china' => 'tw',

            'tajikistan' => 'tj',

            'tanzania, united republic of' => 'tz',

            'thailand' => 'th',

            'timor-leste' => 'tl',

            'togo' => 'tg',

            'tokelau' => 'tk',

            'tonga' => 'to',

            'trinidad and tobago' => 'tt',

            'tunisia' => 'tn',

            'turkey' => 'tr',

            'turkmenistan' => 'tm',

            'turks and caicos islands' => 'tc',

            'turks & caicos islands' => 'tc',

            'tuvalu' => 'tv',

            'uganda' => 'ug',

            'ukraine' => 'ua',

            'united arab emirates' => 'ae',

            'united kingdom' => 'gb',

            'united states' => 'us',

            'united states minor outlying islands' => 'um',

            'uruguay' => 'uy',

            'uzbekistan' => 'uz',

            'vanuatu' => 'vu',

            'venezuela, bolivarian republic of' => 've',

            'viet nam' => 'vn',

            'virgin islands, british' => 'vg',

            'british virgin islands' => 'vg',

            'virgin islands, u.s.' => 'vi',

            'wallis and futuna' => 'wf',

            'western sahara' => 'eh',

            'yemen' => 'ye',

            'zambia' => 'zm',

            'zimbabwe' => 'zw',

            'åland islands' => 'ax',

            'united kingdom - london' => 'gb',

            'united kingdom - scotland' => 'gb',

            'united kingdom - south west' => 'gb',

            'united kingdom - west midlands' => 'gb',

            'united kingdom - south east' => 'gb',

            'united kingdom - north west' => 'gb',

            'united kingdom - north east' => 'gb',

            'united kingdom - central southern' => 'gb',

            'united kingdom - east midlands' => 'gb',

            'united kingdom - wales' => 'gb',

            'united kingdom - northern ireland' => 'gb',

            'usa' => 'us'

        ];

        $country = strtolower($country);
        if (isset($countriesIso["{$country}"])) {
           return $countriesIso["{$country}"];
       }
    }
    
}
