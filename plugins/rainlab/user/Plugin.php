<?php namespace RainLab\User;

use App;
use Auth;
use Backend\Controllers\Auth as BackendAuth;
use Event;
use Backend;
use System\Classes\PluginBase;
use Backend\Facades\BackendMenu;
use System\Classes\SettingsManager;
use RainLab\Notify\Classes\Notifier;
use RainLab\User\Models\MailBlocker;
use Illuminate\Foundation\AliasLoader;
use RainLab\User\Classes\UserRedirector;

// after pamonline integration...
use Illuminate\Support\Facades\Log;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use Twnepc\PamCompanies\Models\CompanySignoff;
use Session;
use Twnepc\PamCompanies\Models\Companies as Company;


class Plugin extends PluginBase
{
    /**
     * @var boolean Determine if this plugin should have elevated privileges.
     */
    public $elevated = true;

    public function pluginDetails()
    {
        return [
            'name'        => 'rainlab.user::lang.plugin.name',
            'description' => 'rainlab.user::lang.plugin.description',
            'author'      => 'Alexey Bobkov, Samuel Georges',
            'icon'        => 'icon-user',
            'homepage'    => 'https://github.com/rainlab/user-plugin'
        ];
    }

    public function register()
    { 
        $alias = AliasLoader::getInstance();
        $alias->alias('Auth', 'RainLab\User\Facades\Auth');

        App::singleton('user.auth', function() {
            return \RainLab\User\Classes\AuthManager::instance();
        });

        App::singleton('redirect', function ($app) {
            // overrides with our own extended version of Redirector to support 
            // seperate url.intended session variable for frontend
            $redirector = new UserRedirector($app['url']);

            // If the session is set on the application instance, we'll inject it into
            // the redirector instance. This allows the redirect responses to allow
            // for the quite convenient "with" methods that flash to the session.
            if (isset($app['session.store'])) {
                $redirector->setSession($app['session.store']);
            }

            
            
            return $redirector;
        });

        
        \Backend\Models\UserGroup::extend(function($model) {
            // Adds fillable or jsonable fields
            $model->addFillable('permissions');
            $model->addJsonable('permissions');
        });
        
        
        
        /*
         * Apply user-based mail blocking
         */
        Event::listen('mailer.prepareSend', function($mailer, $view, $message) {
            return MailBlocker::filterMessage($view, $message);
        });

        BackendMenu::registerContextSidenavPartial('RainLab.User', 'user', '$/rainlab/user/partials/_sidebar.htm');

        /*
         * Compatability with RainLab.Notify
         */
        $this->bindNotificationEvents();
    }

    public function boot() {

        Event::listen('backend.menu.extendItems', function ($manager) {
            $manager->addSideMenuItems('RainLab.User', 'user', [
                'company-side-menu'=> [
                    'label'=> 'twnepc.company::lang.companies',
                    'url'=> Backend::url('twnepc/company/companies'),
                    'icon'=> 'icon-sitemap',
                    'code' => 'companies',
                    'owner' => 'Twnepc.Company',
                    'group'=> 'companies',
                ],
                'branch-company-side' => [
                    'label' => 'twnepc.company::lang.branches',
                    'url' => Backend::url('twnepc/company/branches'),
                    'icon'=> 'icon-sitemap',
                    'group'=>'companies',
                    'code' => 'branches',
                    'owner' => 'Twnepc.Company',
                ],
                'users' => [
                    'label' => 'Users',
                    'icon' => 'icon-users',
                    'code' => 'users',
                    'owner' => 'RainLab.User',
                    'group' => 'users',
                    'url' => Backend::url('rainlab/user/users')
                ],
                'salesreps' => [
                    'label' => 'Salesreps',
                    'icon' => 'icon-users',
                    'code' => 'salesreps',
                    'owner' => 'Twnepc.User',
                    'group' => 'users',
                    'url' => Backend::url('twnepc/user/salesreps')
                ],
                'industries' => [
                    'label' => 'Industries',
                    'icon' => 'icon-building',
                    'code' => 'industries',
                    'owner' => 'Twnepc.User',
                    'group' => 'users',
                    'url' => Backend::url('twnepc/user/industries')
                ],
            ]);
        });
        
        // PAMOnline... 
        BackendAuth::extend(function ($controller) {
            $controller->bindEvent('page.beforeDisplay', function ($action, $params) use ($controller) {
                if ($action === 'signout') { 
                    Auth::logout();
                    //Session::flush();
                }
            });
        });
        
        // PAMOnline 
        Event::listen('backend.signin', function($backenduser) {
            if($backenduser->frontenduser){
                Auth::login($backenduser->frontenduser,true);
            
            $companyID = null;
            $type = null;
            if($backenduser->frontenduser->pamadmin){
                $companyID = $backenduser->frontenduser->pamadmin->company->company_id;
                $model = $backenduser->frontenduser->pamadmin->company;
            }
            else if($backenduser->frontenduser->member){
                $companyID = $backenduser->frontenduser->member->company->company_id;
                $model = $backenduser->frontenduser->member->company;
            }

            
            $disabled = array();

            if(isset($model)){
                switch($model->c_typeback){
                    case "p":
                        $disabled = array();
                        break;
                    case "e":
                        $disabled = array('history','questions_and_answers','articles_and_press_cuttings','publications','news');
                        break;
                    default:
                        $disabled = array('about_us','history','investment_philosophy','questions_and_answers','products_and_services','main_shareholders','articles_and_press_cuttings','publications','news','investment_commentary');
                }
            
                $now = date('U');
                $message = CompanySettings::get('progress_message');
                $deadline = CompanySettings::get('progress_deadline');
                $alert = CompanySettings::get('progress_alert_after');
                $alert = date_create($alert);
                $alert = date_format($alert, 'U');

               
                $signoff = CompanySignoff::where(array('company_id'=>$companyID,'archive'=>0))->orderBy('companysignoff_id','DESC')->first();
                $notCompleted = array();
                $showPopup = false;
                
                if($signoff){
                    foreach ($signoff->attributes as $key => $value) {
                        
                        if(strpos($key, '_signoff'))
                        {
                            $name = str_replace('c_','', $key);
                            $name2 = str_replace('_signoff','',$name);
                            $name3 = ucwords($name2);
                            
                            if($value == 0 && !in_array($name2,$disabled)){
                                $showPopup = true;
                                array_push($notCompleted, $name3);
                            }
                        }
                    }
                    
                }else{
                    array_push($notCompleted, 'No data available. Please check your Company Profile!');
                    $showPopup = true;
                }
                
                if($now >= $alert && $showPopup)
                 {
                    Log::info('Session set!');
                    Session::put('show_progress_popup',true);
                    Session::put('progress_message',$message);
                    Session::put('progress_deadline',$deadline);
                    Session::put('progress_not_completed',$notCompleted);

                    
                 }
             }
            }
            return true;
        });
        
    }

    public function registerComponents()
    {
        return [
            \RainLab\User\Components\Session::class       => 'session',
            \RainLab\User\Components\Account::class       => 'account',
            \RainLab\User\Components\ResetPassword::class => 'resetPassword'
        ];
    }

    public function registerPermissions()
    {
        return [
            'rainlab.users.access_users' => [
                'tab'   => 'rainlab.user::lang.plugin.tab',
                'label' => 'rainlab.user::lang.plugin.access_users'
            ],
            'rainlab.users.access_groups' => [
                'tab'   => 'rainlab.user::lang.plugin.tab',
                'label' => 'rainlab.user::lang.plugin.access_groups'
            ],
            'rainlab.users.access_settings' => [
                'tab'   => 'rainlab.user::lang.plugin.tab',
                'label' => 'rainlab.user::lang.plugin.access_settings'
            ],
            'rainlab.users.impersonate_user' => [
                'tab'   => 'rainlab.user::lang.plugin.tab',
                'label' => 'rainlab.user::lang.plugin.impersonate_user'
            ],
        ];
    }

    public function registerNavigation()
    {
        return [
            'user' => [
                'label'       => 'rainlab.user::lang.users.menu_label',
                'url'         => Backend::url('rainlab/user/users'),
                'icon'        => 'icon-user',
                'hidden'       => true,
                'iconSvg'     => 'plugins/rainlab/user/assets/images/user-icon.svg',
                'permissions' => ['rainlab.users.*'],
                'order'       => 50,
            ]
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'rainlab.user::lang.settings.menu_label',
                'description' => 'rainlab.user::lang.settings.menu_description',
                'category'    => SettingsManager::CATEGORY_USERS,
                'icon'        => 'icon-cog',
                'class'       => 'RainLab\User\Models\Settings',
                'order'       => 500,
                'permissions' => ['rainlab.users.access_settings']
            ]
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'rainlab.user::mail.activate',
            'rainlab.user::mail.welcome',
            'rainlab.user::mail.restore',
            'rainlab.user::mail.new_user',
            'rainlab.user::mail.reactivate',
            'rainlab.user::mail.invite',
        ];
    }

    public function registerNotificationRules()
    {
        return [
            'groups' => [
                'user' => [
                    'label' => 'User',
                    'icon' => 'icon-user'
                ],
            ],
            'events' => [
                \RainLab\User\NotifyRules\UserActivatedEvent::class,
                \RainLab\User\NotifyRules\UserRegisteredEvent::class,
            ],
            'actions' => [],
            'conditions' => [
                \RainLab\User\NotifyRules\UserAttributeCondition::class
            ],
        ];
    }

    protected function bindNotificationEvents()
    {
        if (!class_exists(Notifier::class)) {
            return;
        }

        Notifier::bindEvents([
            'rainlab.user.activate' => \RainLab\User\NotifyRules\UserActivatedEvent::class,
            'rainlab.user.register' => \RainLab\User\NotifyRules\UserRegisteredEvent::class
        ]);

        Notifier::instance()->registerCallback(function($manager) {
            $manager->registerGlobalParams([
                'user' => Auth::getUser()
            ]);
        });
    }
}
