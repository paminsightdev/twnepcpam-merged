<?php namespace RainLab\User\Components;

use DB;
use Auth;
use Lang;
use Event;
use Flash;
use Request;
use Redirect;
use Response;
use Cms\Classes\Page;
use ValidationException;
use Cms\Classes\ComponentBase;
use RainLab\User\Models\UserGroup;
use Illuminate\Support\Facades\Session as SessionFacade;
use Keios\Multisite\Classes\SettingHelper;
use Twnepc\User\Models\UserAcceptSettings;

// needed for PAMOnline...
use Backend\Facades\BackendAuth;
use Session as AppSession;
use Twnepc\User\Models\PageView;
use Twnepc\PamCompanies\Models\CompanyTrackEvent;

// needed for session mananagement between pam insight + online
use Twnepc\Membership\Models\Membership;
use Twnepc\Membership\Models\Subscription;
use Keios\Multisite\Models\Setting as multisiteSetting;
use Cookie;
use RainLab\User\Models\User;

use Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse;

/**
 * User session
 *
 * This will inject the user object to every page and provide the ability for
 * the user to sign out. This can also be used to restrict access to pages.
 */
class Session extends ComponentBase
{
    const ALLOW_ALL = 'all';
    const ALLOW_GUEST = 'guest';
    const ALLOW_USER = 'user';

    private $needs_setup = false;
    private $redirected = false;
    
    private static $sitesSubscribed = [];
    

    public function componentDetails()
    {
        return [
            'name'        => 'rainlab.user::lang.session.session',
            'description' => 'rainlab.user::lang.session.session_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'security' => [
                'title'       => 'rainlab.user::lang.session.security_title',
                'description' => 'rainlab.user::lang.session.security_desc',
                'type'        => 'dropdown',
                'default'     => 'all',
                'options'     => [
                    'all'   => 'rainlab.user::lang.session.all',
                    'user'  => 'rainlab.user::lang.session.users',
                    'guest' => 'rainlab.user::lang.session.guests'
                ]
            ],
            'allowedUserGroups' => [
                'title'       => 'rainlab.user::lang.session.allowed_groups_title',
                'description' => 'rainlab.user::lang.session.allowed_groups_description',
                'placeholder' => '*',
                'type'        => 'set',
                'default'     => []
            ],
            'redirect' => [
                'title'       => 'rainlab.user::lang.session.redirect_title',
                'description' => 'rainlab.user::lang.session.redirect_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ]
        ];
    }

    public function getRedirectOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getAllowedUserGroupsOptions()
    {
        return UserGroup::lists('name','code');
    }

    /**
     * Component is initialized.
     */
    public function init()
    {
        if (Request::ajax() && !$this->checkUserSecurity()) {
            abort(403, 'Access denied');
        }
    }

    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun()
    {
        if (!$this->checkUserSecurity()) {
            if (empty($this->property('redirect'))) {
                throw new \InvalidArgumentException('Redirect property is empty');
            }

            $redirectUrl = $this->controller->pageUrl($this->property('redirect'));
            SessionFacade::put('before_login_url',$this->controller->currentPageUrl().'?'.$_SERVER["QUERY_STRING"]);
            return Redirect::guest($redirectUrl.'?'.$_SERVER["QUERY_STRING"]);

        } else  {
            
            if($this->needs_setup && $this->page->baseFileName != "preferences" && $this->page->baseFileName != "2fa-login") {
                return Redirect::guest('preferences');
            }
        }
        
        $this->page['user'] = $user = $this->user();
        
        // PAMOnline code to save pages viewed by users...
        if(SettingHelper::getCurrentSite()->id == 5 && $user) {
            $viewedPage = AppSession::get('viewed_page',array());
            $reqUri = Request::url();
            if(!isset($viewedPage[$reqUri])){
                $user = Auth::getUser();
                if($user){
                    $pageViewModel = new PageView;
                    $pageViewModel->page_url = $reqUri;
                    $pageViewModel->session_id = AppSession::getId();
                    $pageViewModel->server = '';
                    $pageViewModel->user_id = $user->id;
                    $pageViewModel->save();
                    $viewedPage[$reqUri] = array(
                        'user' => $user->id,
                        'timestamp' => time()
                    );
                    AppSession::put('viewed_page',$viewedPage);
                }
            }
        }
    }

    
    public function onTrackCompany()
    {
        $companyId = post('company_id',false);
        $action = post('action',false);
        $url = post('url',false);
        $user = Auth::getUser();
        if($companyId && $action && $url && $user){
            $companyTrackModel = new CompanyTrackEvent;
            $companyTrackModel->user_id = $user->id;
            $companyTrackModel->session_id = AppSession::getId();
            $companyTrackModel->url = $url;
            $companyTrackModel->action = $action;
            $companyTrackModel->company_id = $companyId;
            $companyTrackModel->save();
        }
        return true;
    }
    
    
    /**
     * Returns the logged in user, if available, and touches
     * the last seen timestamp.
     * @return RainLab\User\Models\User
     */
    public function user()
    {
        if (!$user = Auth::getUser()) {
            return null;
        }

        if (!Auth::isImpersonator()) {
            $user->touchLastSeen();
        }

        return $user;
    }

    /**
     * Returns the previously signed in user when impersonating.
     */
    public function impersonator()
    {
        return Auth::getImpersonator();
    }

    /**
     * Log out the user
     *
     * Usage:
     *   <a data-request="onLogout">Sign out</a>
     *
     * With the optional redirect parameter:
     *   <a data-request="onLogout" data-request-data="redirect: '/good-bye'">Sign out</a>
     *
     */
    public function onLogout()
    {
//        if(!Auth::check()) {
//            return Redirect::guest("/");
//            die();
//        }
        
        $user = Auth::getUser();
        
        Auth::logout();
        
        if ($user) {
            Event::fire('rainlab.user.logout', [$user]);
        }
        
        // PAMOnline, put here to logout the backend user object as well..
        if(BackendAuth::check()) {
            BackendAuth::logout();
        }
        
        $url = post('redirect', Request::fullUrl());
        AppSession::flush();
        Flash::success(Lang::get('rainlab.user::lang.session.logout'));
        
        return Redirect::to($url);
    }

    /**
     * If impersonating, revert back to the previously signed in user.
     * @return Redirect
     */
    public function onStopImpersonating()
    {
        if (!Auth::isImpersonator()) {
            return $this->onLogout();
        }

        Auth::stopImpersonate();

        $url = post('redirect', Request::fullUrl());

        Flash::success(Lang::get('rainlab.user::lang.session.stop_impersonate_success'));

        return Redirect::to($url);
    }

    /**
     * Checks if the user can access this page based on the security rules
     * @return bool
     */
    protected function checkUserSecurity()
    {

        $allowedGroup = $this->property('security', self::ALLOW_ALL);
        $allowedUserGroups = $this->property('allowedUserGroups', []);
        $isAuthenticated = Auth::check();
        $site = SettingHelper::getCurrentSite();
        
        if ($isAuthenticated) {
            $user = Auth::getUser();
            if($user) {
                
                $accepted_settings = UserAcceptSettings::where('user_id',$user->id)->where('site_id',$site->id)->where('is_accepted',1)->count();
                
                if($accepted_settings < 1) {
                    $this->needs_setup = true;
                }
            }
            
            // find out what this does...
            if ($allowedGroup == self::ALLOW_GUEST) {
                return false;
            }
            
            
            
            // find out what this does...
            if (!empty($allowedUserGroups)) {
                $userGroups = Auth::getUser()->groups->lists('code');
                if (!count(array_intersect($allowedUserGroups, $userGroups))) {
                    return false;
                }
            }
            
            
            // is the user subscribed to this website...
            if(!static::isUserSubscribedToWebsite($user)) {
                
                // PAMOnline is different at the moment but will be updated soon
                if($site->id == 5) {

                    // don't show prefernces as the user will be logged out...
                    $this->needs_setup = false;
                    
                    // bypass the redirect stuff as we have already been evaluted up until now, returns true
                    return static::addLoggedInUserToSession();
                    
                } else {
                    // use this else block here 
                    // for websites other than PAMOnline
                    // where users are not subscribed to them...
                }
                
            }
            
            
            
            
        // when the user is not logged in...
        } else {
            
            if(SessionFacade::has('siteIn.memberRefTo')) {
                // should return 'true' if the user is logged in or 'false' if not 
                if(static::logReturningUserIn()) {
                    return true;
                }
            }
            
            if ($allowedGroup == self::ALLOW_USER) {
                return false;
            }
            
            // when we are n the PAM Account page after backend logout, then click the logout btn...
            //if(!$isAuthenticated && ($site->id == 5) && (stripos($this->currentPageUrl(), '/my-account') !== false)) {
            //    return $this->onLogout();
            //    die();
            //}
        }
        
        return true;
    }
    
    
    /** 
     * log user in using cookie if they are on a site they have a subscription for.
     * @return boolean
     */
    public static function logReturningUserIn() {
        
        if(SessionFacade::has('siteIn.memberRefTo')) {
            
            // check if user was here before using the cookie placed in there browser for 2 hours
            $memberRefTo = Cookie::get('siteout');
            
            // then check the session for the random number
            if(SessionFacade::get('siteIn.memberRefTo') == $memberRefTo) {
                
                if(in_array(SettingHelper::getCurrentSite()->id, SessionFacade::get('siteIn.subscribedTo') )) {
                    
                    // if the number matches then pull out the user id, then get the user 
                    $user = User::find(SessionFacade::get('siteIn.member'));

                    // with that user object log them in.
                    Auth::login($user, false);
                    
                    if(SessionFacade::has('siteIn')) {
                        // remove automatic user session var
                        SessionFacade::forget('siteIn');
                        // remove cookie siteout cookie for automatic user session too
                        setcookie('siteout', '', time() - 3600);
                    }
                    // success user should be logged in.
                    return true;
                }
                
            }
        }
        
        // not the right site, or session vars did not match with cookie
        return false;
    }
    
    
    /**
     * when leaving subscribed site, mainly to PAMOnline the user will have their details saved/paused 
     * and a cookie placed in their browser so they can be automatically logged in on return.
     * @return boolean
     */
    public static function addLoggedInUserToSession() {
        
        // generate random number
        $memberRefTo = 'siteout'.rand(1, 20).''.rand(1, 20);
        
        // create session var
        $siteIn = [];
        // add array of site ids to [subscribedTo]
        $siteIn['subscribedTo'] = static::$sitesSubscribed;
        // add random number to [memberRefTo]
        $siteIn['memberRefTo'] = $memberRefTo;
        // add user_id to [member]
        $siteIn['member'] = Auth::getUser()->id;
        // put these in the session for later
        SessionFacade::put('siteIn', $siteIn);
        
        //create cookie name = siteout / value = 'random number' / 2 days (min * hours * days)
        Cookie::queue('siteout', $memberRefTo, (60*24*2));
        
        // unset/remove user details from session without having to fwd them to predefined location
        Auth::logout();
        
        // user not authenticated (logged out) and does not need to pass checkUserSecurity() conditions
        return true;
    }
    
    
    
    /**
     * match the current site against the sites the user has subscription access to.
     * this is a public function so we can use this around buttons and layout components 
     * @param object $user 
     * @return boolean
     */
    public static function isUserSubscribedToWebsite($user) {
        
        // initialise vars...
        $site = SettingHelper::getCurrentSite();
        $now = strtotime(date('Y-m-d',strtotime('now')));
        $isUserSubscribedToWebsite = false;
        
        // get memberships for this user, branch and company associated with their record individually or collectively...
        $memberships = Membership::where('user_id', $user->id);
        if($user->branch_id) {
            $memberships->orWhere('branch_id', $user->branch_id);
        }
        if($user->company_id) {
            $memberships->orWhere('company_id', $user->company_id);
        }
        $memberships = $memberships->get();
        
        
        // check if the logged in member has membership(s)
        if($memberships) {
            
            // loop through all memberships this user has...
            foreach($memberships as $membership) {

                // check active the membership is still active...
                if($membership->status != 'Active') {
                    continue;
                }
                
                // check dates: not started and finished...
                if(strtotime(date('Y-m-d', strtotime($membership->start_date))) > $now) {
                    continue;
                }
                if(strtotime(date('Y-m-d', strtotime($membership->end_date))) < $now) {
                    continue;
                }

                // retrieve websites that are associated with a subscription, for this user, through this membership...
                $subscribedSites = DB::table('setting_subscription')->select('setting_id')->where('subscription_id', $membership->subscription_id)->get();
                
                // loop through sites included in this membership, which is linked to the subscription package...
                foreach($subscribedSites as $siteIncluded) {
                    
                    // add all active subscriptions to this array...
                    array_push(static::$sitesSubscribed, $siteIncluded->setting_id);
                    
                    // check this website id against the ones in the subscription package...
                    if($site->id == $siteIncluded->setting_id) {
                        $isUserSubscribedToWebsite = true;
                    }
                }

                // when they are subscribed to this site return yes
                if($isUserSubscribedToWebsite) { 
                    return true;
                }
            }
            
            // user is not subscribed to this website...
            return false;
        }
    }
    
    /*
    public function checkSubscriptionOnlyPages() {
        
        // get current site's multisite details...
        $currentSite = SettingHelper::getCurrentSite();
        // create multi to hold data array [site][url]...
        $pagesForPermissions = [];
        // get all the micro sites defined in multisite table...
        $websites = multisiteSetting::all();

        // correct doman name and URL to my-account
        foreach($websites as $site) {
            
            if($site->id == 5) {
                // there are more URLs for PAM than any other domain...
                $pagesForPermissions[$site->id] = [$site->domain.'/my-account', $site->domain.'/admin-panel', $site->domain.'/backend']; //, $site->domain.'/', $site->domain.''];
                
            } else {
                // add the other URLs to each array...
                $pagesForPermissions[$site->id] = [$site->domain.'/account'];
            }
        }

        // for this particular site, match pages that are protected by using the $pagesForPermissions[site][pages]
        foreach($pagesForPermissions[$currentSite->id] as $page) {
            // match the website url against the restricted one in the list
            if(stripos($this->currentPageUrl(), $page) !== false) {
                // match is found...
                return true;
            }
        }
        
        // no match found...
        return false;
    }
     
     */
}
