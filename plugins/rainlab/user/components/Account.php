<?php namespace RainLab\User\Components;

use RainLab\User\Facades\Auth;
use Lang;
use Mail;
use Event;
use Flash;
use Input;
use Request;
use Redirect;
use Exception;
use System\Models\EventLog;
use Twnepc\News\Models\Category;
use Validator;
use Cms\Classes\Page;
use ValidationException;
use ApplicationException;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Keios\Multisite\Classes\SettingHelper;
use Twnepc\User\Models\UserAcceptSettings;
use RainLab\User\Models\Settings as UserSettings;
use RainLab\User\Models\User as userModel;


// Pamonline added classes from integration
use Twnepc\Roles\Models\Group;
use Backend\Facades\BackendAuth;
use Session as AppSession;
use October\Rain\Auth\AuthException;
use Twnepc\User\Models\Salutation;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use Twnepc\PamCompanies\Models\Companies;
use Twnepc\User\Models\Role;
use Backend\Models\User as BackendUser;
use Backend;
use Twnepc\PamCompanies\Models\Country;
use Twnepc\Awards\Models\Award;


use Twnepc\Logs\Models\PamSplunkTroubleShooting;
use Illuminate\Support\Facades\Queue;


/**
 * Account component
 *
 * Allows users to register, sign in and update their account. They can also
 * deactivate their account and resend the account verification email.
 */
class Account extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => /*Account*/'rainlab.user::lang.account.account',
            'description' => /*User management form.*/'rainlab.user::lang.account.account_desc'
        ];
    }

    public function defineProperties()
    {
        return [
            'redirect' => [
                'title'       => /*Redirect to*/'rainlab.user::lang.account.redirect_to',
                'description' => /*Page name to redirect to after update, sign in or registration.*/'rainlab.user::lang.account.redirect_to_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'paramCode' => [
                'title'       => /*Activation Code Param*/'rainlab.user::lang.account.code_param',
                'description' => /*The page URL parameter used for the registration activation code*/ 'rainlab.user::lang.account.code_param_desc',
                'type'        => 'string',
                'default'     => 'code'
            ],
            'forceSecure' => [
                'title'       => /*Force secure protocol*/'rainlab.user::lang.account.force_secure',
                'description' => /*Always redirect the URL with the HTTPS schema.*/'rainlab.user::lang.account.force_secure_desc',
                'type'        => 'checkbox',
                'default'     => 0
            ],
        ];
    }

    public function getRedirectOptions()
    {
        // pamonline...
        //return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
        //paminsight
        return [''=>'- refresh page -', '0' => '- no redirect -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Executed when this component is initialized
     * @todo we may need to use default values if there is no retrieve value
     */
    public function prepareVars() {
        
        // for all web accounts...
        $this->page['user'] = $this->user();
        $this->page['canRegister'] = $this->canRegister();
        $this->page['loginAttribute'] = $this->loginAttribute();
        $this->page['loginAttributeLabel'] = $this->loginAttributeLabel();
        $this->page['site'] = $site = SettingHelper::getCurrentSite();
        $this->page['block'] = $this;
        $this->page['roles'] = $this->roles();
    }
    
    
    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun() {
        
        /*
         * Redirect to HTTPS checker
         */
        if ($redirect = $this->redirectForceSecure()) {
            return $redirect;
        }
        
        
        /*
         * Activation code supplied
         */
        if ($code = $this->activationCode()) {
            $this->onActivate($code);
        }
                
        $this->prepareVars();
        
        // for pamonline users...
        if(SettingHelper::getCurrentSite()->id == 5) {
            if(isset( $this->page['user'] ) && $this->page['user']->backenduser != null) {
                // when the user has backend access relation, log them in if they are not...
                if(!BackendAuth::getUser()) {                
                    BackendAuth::login($this->page['user']->backenduser);
                }

            } else {
                // make sure the backend user is logged out
                if(BackendAuth::getUser()) {
                    BackendAuth::logout();
                    AppSession::flush();
                }
            }
        }
        
        
        if(SettingHelper::getCurrentSite()->id == 5) {
            
            if(get('redirect_after_login', false)) {
                AppSession::put('redirect_after_login', get('redirect_after_login'));
            }
        
            if(AppSession::has('redirect_after_login')) {

                if(null != $this->page['user']) {
                    $redirect = AppSession::get('redirect_after_login');
                    AppSession::forget('redirect_after_login');
                    return redirect($redirect);
                }
            }
        }
        
    }
    
    
    
    public function getSubInterests($site, $category, $level, $selections = []){
        $html = '';

        $children = $category->children()->get();
        foreach($children as $child){
            if(!$child->is_hidden){
                $html .= '<div data-site="'.$site->id.'" class="interest custom-control custom-checkbox level-'.$level.'">';
                $html .= '<input type="checkbox" '.(in_array($child->id,$selections) ? 'checked' : '').' class="custom-control-input" name="category[]" value="'.$child->id.'" id="iterests'.$child->id.'">';
                $html .= '<label class="custom-control-label" for="iterests'.$child->id.'">'.$child->name.'</label>';
                $html .= '<div class="subInterests level-'.$level.'">'.$this->getSubInterests($site, $child, $level+1, $selections).'</div>';
                $html .= '</div>';

            }
        }

        return $html;
    }

    public function getAllInterests($site, $selections = []){
        $topLevelCategories = Category::where('site_id',$site->id)->where('is_hidden',0)->where('parent_id',NULL)->get();
        $level = 1;
        $html = '';
        foreach($topLevelCategories as $category){
            if($category->children->count()){
                $html .= '<strong>'.$category->name.'</strong>';
                $html .= '<div class="interests site-'.$site->id.' level-'.$level.'" data-site="'.$site->id.'">';
                $html .= $this->getSubInterests($site, $category, $level+1, $selections);
                $html .= '</div>';
            }
        }

        return $html;
    }

    //
    // Properties
    //

    /**
     * Returns the logged in user, if available
     */
    public function user()
    {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }

    /**
     * Flag for allowing registration, pulled from UserSettings
     */
    public function canRegister()
    {
        return UserSettings::get('allow_registration', true);
    }

    /**
     * Returns the login model attribute.
     */
    public function loginAttribute()
    {
        return UserSettings::get('login_attribute', UserSettings::LOGIN_EMAIL);
    }

    /**
     * Returns the login label as a word.
     */
    public function loginAttributeLabel()
    {
        return Lang::get($this->loginAttribute() == UserSettings::LOGIN_EMAIL
            ? /*Email*/'rainlab.user::lang.login.attribute_email'
            : /*Username*/'rainlab.user::lang.login.attribute_username'
        );
    }

    /**
     * Looks for the activation code from the URL parameter. If nothing
     * is found, the GET parameter 'activate' is used instead.
     * @return string
     */
    public function activationCode()
    {
        $routeParameter = $this->property('paramCode');

        if ($code = $this->param($routeParameter)) {
            return $code;
        }

        return get('activate');
    }

    //
    // AJAX
    //

    /**
     * Sign in the user
     */
    public function onSignin() {

        try {
            /*
             * Validate input
             */
            $data = post();            
            $rules = [];
            $email = post('email');

            $rules['login'] = $this->loginAttribute() == UserSettings::LOGIN_USERNAME
                ? 'required|between:2,255'
                : 'required|email|between:6,255';

            $rules['password'] = 'required|between:4,255';

            if (!array_key_exists('login', $data)) {
                $data['login'] = post('username', post('email'));
            }

            $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                EventLog::add('Login Error: Validation error', 'error', $email);
                throw new ValidationException($validation);
            }
            
            
            /*
             * Authenticate user
             */
            $credentials = [
                'login'    => array_get($data, 'login'),
                'password' => array_get($data, 'password')
            ];
            //dd($credentials);
            // not implemented yet
            Event::fire('rainlab.user.beforeAuthenticate', [$this, $credentials]);
            
            // PAMonline...
            $keep = array_get($data, 'keep') == "on" ? true : false;
            
            
            // check this function, could be related to the encryption function
            $user = (SettingHelper::getCurrentSite()->id == 5) ? Auth::authenticate($credentials, $keep) : Auth::authenticate($credentials, false);
            
            //dd($user);
            
            if ($user->isBanned()) {
                EventLog::add('Login Error: User Banned', 'error', $email);
                Auth::logout();
                throw new AuthException(/*Sorry, this user is currently not activated. Please contact us for further assistance.*/'rainlab.user::lang.account.banned');
            }
            
            
            // PAMOnline...
            if(!$user) {
                EventLog::add('User Tried to login with:'.array_get($data,'login')."but failed to login",'info',$user);
            }
            
            // PAMOnline...
            if($user->backenduser) {
                $backenduser = BackendAuth::login($user->backenduser, $keep);
            }
            
            
            
//            if(SettingHelper::getCurrentSite()->id == 5) {
//                // what is this returning and to where?
//                if(post('redirect',null)) {
//                    return ['X_OCTOBER_REDIRECT' => post('redirect')];
//
//                } elseif(AppSession::has('redirect_after_login')) {
//                    return ['X_OCTOBER_REDIRECT' => AppSession::get('redirect_after_login')];
//
//                } else {
//                    // it gets to this bit here...
//                    return [];
//                }
//            }
            
            
            /*
             * Redirect
             */
            if ($redirect = $this->makeRedirection(true)) {

                try {
                    // check site
                    if(SettingHelper::getCurrentSite()->id == 5) {
                    
                        // reload the last page viewed...
                        $lastPageUrl = request()->header('referer');
                        // does the referer have the domain in the URL, if so let use it, if not lets go straight to my account...
                        $redirect = (stripos($lastPageUrl, SettingHelper::getCurrentSite()->domain) !== false) ? Redirect::intended($lastPageUrl) : Redirect::intended(SettingHelper::getCurrentSite()->domain.'/my-account');
                        
                    } else {

                        // check the last article viewed is in the same site domain as the signed in account
                        $articleUrl = Session::get('article_url');
                        Session::forget('article_url');

                        $beforeLoginUrl = Session::get('before_login_url');
                        Session::forget('before_login_url');
                        if($beforeLoginUrl){
                            $articleUrl = $beforeLoginUrl;
                        }
                        
                        $redirect = (stripos($articleUrl, SettingHelper::getCurrentSite()->domain) !== false) ? Redirect::intended($articleUrl) : Redirect::intended(SettingHelper::getCurrentSite()->domain.'/account'); //$redirect;
                    }
                    
                } catch (Exception $ex) {
                    // return the default page defined - when error
                    return Redirect::intended($redirect);
                }
                
                //dd($redirect);
                
                // return the default page defined - when no article has been looked at
                return $redirect;
            }
            
            
        } catch (Exception $ex) {
            
            $email = post('login');
            EventLog::add('Login Error: '.$ex->getMessage(), 'error', $email);
            
            if (Request::ajax()) { 
                throw $ex;
            } else {
                Flash::error($ex->getMessage());
            }
        }
    }
    
    
    /**
     * Register the user
     */
    public function onRegister()
    {
        try {
            
            if (!$this->canRegister()) {
                throw new ApplicationException(Lang::get(/*Registrations are currently disabled.*/'rainlab.user::lang.account.registration_disabled'));
            }

            /*
             * Validate input
             */
            $data = post();
            
            if (!array_key_exists('password_confirmation', $data)) {
                $data['password_confirmation'] = post('password');
            }
            
            if (!array_key_exists('country_id', $data)) {
                $data['country'] = post('country_id');
            }
            
            
            //////////////////////////////////////////////////////////////
            // common validation rules for both sites...
            //////////////////////////////////////////////////////////////
            
            $rules = [
                'email'    => 'required|email|between:6,255',
                'password' => 'required:create|between:8,255|regex:/^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/|confirmed',
                'password_confirmation' => 'required_with:password|between:8,255|regex:/^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/',
                'name'     => 'required',
                'surname'  => 'required',
                'country_id' => 'required',
                'zip' => 'required',
                'phone' => 'required',
            ];
            
            // on the PAMOnlne site add the following rules...
            if(SettingHelper::getCurrentSite()->id == 5) {
                $rules['role'] = 'required';
                //$rules['salutation'] = 'required|between:2,10'; 
            
            // on the PAMInsight site add the following rules...
            } else {
                $rules['position'] = 'required';
                $rules['classification'] = 'required';
                $rules['company'] = 'required';
                $rules['street_addr'] = 'required';
                $rules['city'] = 'required';
                $rules['gender'] = 'required';
                $rules['email_updates'] = 'required';
            }
            
            if ($this->loginAttribute() == UserSettings::LOGIN_USERNAME) {
                $rules['username'] = 'required|between:2,255';
            }
            
            // this is from the PAMOnline website, but will now work with PAMInsight...
            // get longitude and latitude from postcode
            $zip = $data['zip'];
            $countryId = $data['country_id'];
            $postCodeCoords = $this->getZipLongLat($zip, $countryId);
            if($postCodeCoords !== false) {
                // assign data to array used for populating user object later...
                $data['location_lon'] = $postCodeCoords['long'];
                $data['location_lat'] = $postCodeCoords['lat']; 
            }
            
            // PAMOnline specific code
            if(SettingHelper::getCurrentSite()->id == 5) {
                $pamsGroup = CompanySettings::get('pams_frontend_groups');
                $pamsAdminGroup = CompanySettings::get('pams_admin_frontend_groups');

                if($data['role'] == $pamsGroup || $data['role'] == $pamsAdminGroup){
                    $rules['company'] = 'required|exists:pamtbl_company,c_companyname';
                }

                // @TODO pamonline - test if this statement is needed test with and test without.
                // same as above.
                if($data['role'] == $pamsGroup || $data['role'] == $pamsAdminGroup){
                    if($data['company'] != ""){
                        $rules['company'] = 'required|exists:pamtbl_company,c_companyname';
                    }
                }
            }
            
            $validation = Validator::make($data, $rules);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            }
            
            // PAMOnline specific code...
            // @TODO pamonline investigate "why do we associate registrant with default company
            // when the user does not have a company added associate them to the default - 4 Shires...
            // when the user adds a company then use that one...
            if(SettingHelper::getCurrentSite()->id == 5) {
                if($data['role'] == $pamsGroup || $data['role'] == $pamsAdminGroup) {
                    $userCompanyByDefault = UserSettings::get('user_company_by_default',false);
                    if($userCompanyByDefault && ($data['company'] == "")) {
                        $data['company'] = $userCompanyByDefault;
                    }

                    if($data['company'] != "") {
                        $company = Companies::where('c_companyname',$data['company'])->first();
                        $data['company_id'] = $company->company_id;
                    }
                }
            }
            
            
            /*
             * Register user event - is not implemented yet!
             */
            //Event::fire('rainlab.user.beforeRegister', [&$data]);
            
            $requireActivation = UserSettings::get('require_activation', true);
            $automaticActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_AUTO;
            $userActivation = UserSettings::get('activate_mode') == UserSettings::ACTIVATE_USER;
            $user = Auth::register($data, $automaticActivation);
            
            
            
            /*
             * Add PAM role to user
             */
            if(SettingHelper::getCurrentSite()->id == 5) {
                $userRole = new Role();
                $userRole->user_id = $user->id;
                $userRole->role_id = $data['role'];
                $userRole->save();
            }
            
            $site = SettingHelper::getCurrentSite();
            $settingData = [
                'site_id' => $site->id,
                'user_id' => $user->id,
                'is_accepted' => 1
            ];
            $setting = UserAcceptSettings::firstOrCreate($settingData);
            
            
            /*
             * Registered user event
             * adds the user to a company if they have an email from that company
             * setup for 
             */
            if(SettingHelper::getCurrentSite()->id != 5) {
                Event::fire('rainlab.user.register', [$user, $data]);
            }
            
            
            /*
             * Activation is by the user, send the email
             */
            if ($userActivation) {
                $this->sendActivationEmail($user);

                Flash::success(Lang::get(/*An activation email has been sent to your email address.*/'rainlab.user::lang.account.activation_email_sent'));
            }

            /*
             * Automatically activated or not required, log the user in
             */
            if ($automaticActivation || !$requireActivation) {
                Auth::login($user);
            }
            
            
            /*
             * Redirect to the intended page after successful sign in
             */
            if(SettingHelper::getCurrentSite()->id == 5) {
                // when the user has registered on PAMOnline execute this code block...
                $redirectUrl = $this->pageUrl($this->property('redirect')) ?: $this->property('redirect');
                if ($redirectUrl = post('redirect', $redirectUrl)) {
                    return Redirect::intended($redirectUrl);
                }
                
            } else {
                // when the user has registered on paminsight execute this block...
                $article_url = Session::get('article_url');
                $site = SettingHelper::getCurrentSite();
                $domain = $site->domain;
                $is_same_site = (strpos($article_url,$domain) !== false);

                if($article_url != NULL && $is_same_site) {
                    Session::forget('article_url');
                    return Redirect::to($article_url);
                }
                
            }
            
            /*
             * Redirect to the intended page after successful sign in
             */
            if ($redirect = $this->makeRedirection(true)) {
                return $redirect;
            }
            
        } catch (Exception $ex) {
            
            if (Request::ajax()) {
                throw $ex;
            } else {
                Flash::error($ex->getMessage());
            }
            
        }
    }

    /**
     * Activate the user
     * @param  string $code Activation code
     */
    public function onActivate($code = null) {
        
        try {
            
            $code = post('code', $code);
            
            $errorFields = ['code' => Lang::get(/*Invalid activation code supplied.*/'rainlab.user::lang.account.invalid_activation_code')];

            /*
             * Break up the code parts
             */
            $parts = explode('!', $code);
            if (count($parts) != 2) {
                throw new ValidationException($errorFields);
            }

            list($userId, $code) = $parts;

            if (!strlen(trim($userId)) || !strlen(trim($code))) {
                throw new ValidationException($errorFields);
            }

            if (!$user = Auth::findUserById($userId)) {
                throw new ValidationException($errorFields);
            }

            if (!$user->attemptActivation($code)) {
                throw new ValidationException($errorFields);
            }

            Flash::success(Lang::get(/*Successfully activated your account.*/'rainlab.user::lang.account.success_activation'));

            /*
             * Sign in the user
             */
            Auth::login($user);

        }
        catch (Exception $ex) {
            
            if (Request::ajax()) {
                throw $ex;
            } else {
                Flash::error($ex->getMessage());
            }
        }
    }

    
    /**
     * Update the user
     */
    public function onUpdate()
    {
        if (!$user = $this->user()) {
            return;
        }
        
        if (Input::hasFile('avatar')) {
            $user->avatar = Input::file('avatar');
        }
        
        // put post in data array
        $data = post();
        
        // this is from the PAMOnline website, but will now work with PAMInsight...
        // get longitude and latitude from postcode
        $zip = $data['zip'];
        $countryId = $data['country_id'];
        $postCodeCoords = $this->getZipLongLat($zip, $countryId);
        if($postCodeCoords !== false) {
            // assign data to array used for populating user object later...
            $data['location_lon'] = $postCodeCoords['long'];
            $data['location_lat'] = $postCodeCoords['lat']; 
        }
        
        $user->fill($data);
        $user->save();
        
        
        /*
         * Password has changed, reauthenticate the user
         */
        if (strlen(post('password'))) {
            Auth::login($user->reload(), true);
        }
        
        Flash::success(post('flash', Lang::get(/*Settings successfully saved!*/'rainlab.user::lang.account.success_saved')));
        
        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
        
        $this->prepareVars();
    }


    public function onUpdatePersonalInformation(){
        if (!$user = $this->user()) {
            return;
        }

        $data = post();

        /*Validate partial fields*/
        $rules = [
            'name'    => 'required',
            'surname' => 'required',
            'company' => 'required',
            'position' => 'required',
            'phone' => 'required',
            'gender' => 'required'
        ];


        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        /*End validation*/

        /* Save if validation passes*/
        $user->salutation = $data['salutation'];
        $user->name = $data['name'];
        $user->surname = $data['surname'];
        $user->company = $data['company'];
        $user->position = $data['position'];
        $user->phone = $data['phone'];
        $user->phone_ext = $data['phone_ext'];
        $user->fax = $data['fax'];
        $user->gender = $data['gender'];
        $user->contactby_pam = isset($data['contacttruest']) ? 0 : 1;
        $user->contact_never = isset($data['contactbusinesses']) ? 1 : 0;
        $user->save();

        Flash::success(post('flash', Lang::get(/*Settings successfully saved!*/'rainlab.user::lang.account.success_saved')));
    }

    public function onUpdateMyAddress(){
        if (!$user = $this->user()) {
            return;
        }

        $data = post();

        /*Validate partial fields*/
        $rules = [
            'street_addr'    => 'required',
            'city' => 'required',
            'country_id' => 'required'
        ];


        $messages = [
            'country_id.required' => 'Please select yout country'
        ];


        $validation = Validator::make($data, $rules,$messages);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        $user->street_addr = $data['street_addr'];
        $user->street_addr_2 = $data['street_addr_2'];
        $user->street_addr_3 = $data['street_addr_3'];
        $user->city = $data['city'];
        $user->country_id = $data['country_id'];
        $user->save();
        Flash::success(post('flash', Lang::get(/*Settings successfully saved!*/'rainlab.user::lang.account.success_saved')));
    }
    
    
    public function onUpdateMyInterests(){
        if (!$user = $this->user()) {
            return;
        }

        $data = post();


        $user->category = $data['category'];

        $user->save();

        /* Troubleshooting log */
        // (null !== $user->category && !empty($user->category)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/rainlab/user/components/Account.php', 'onUpdateMyInterests', 'data[category]', $user->category) : '';

        Flash::success(post('flash', Lang::get(/*Settings successfully saved!*/'rainlab.user::lang.account.success_saved')));

    }
    
    
    public function onUpdateNewsletter(){
        if (!$user = $this->user()) {
            return;
        }
    }

    public function onUnFav() {
        
        $article = Request::get('article');
        $user = Request::get('user');

        $delete = DB::table('article_fav_user')->where('user_id',$user)->where('article_id',$article)->delete();

        if($delete) {
            $this->page['fav'] = DB::table('article_fav_user')->where('user_id',$user)->leftJoin('twnepc_news_articles as article','article.id','=','article_fav_user.article_id')->get();
            $this->page['user'] = Auth::getUser();
            return [
                '#favs' => $this->renderPartial('account::favourite')
            ];
        }
    }

    
    public function onUpdatePasswordAndMail(){
        if (!$user = $this->user()) {
            return;
        }

        $data = post();

        $rules = [
            'password' => 'required:create|between:8,255|regex:/^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/|confirmed',
            'password_confirmation' => 'required_with:password|between:8,255|regex:/^.*(?=.{8,})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/',
        ];

        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }

        if($data['password']){
            $user->password = $data['password'];
            $user->password_confirmation = $data['password_confirmation'];
        }


        $user->save();

        Flash::success(post('flash', Lang::get(/*Settings successfully saved!*/'rainlab.user::lang.account.success_saved')));


        /*
         * Password has changed, reauthenticate the user
         */
        if (strlen(post('password'))) {
            Auth::login($user->reload(), true);
        }


    }

    /**
     * Deactivate user
     */
    public function onDeactivate()
    {
        if (!$user = $this->user()) {
            return;
        }

        if (!$user->checkHashValue('password', post('password'))) {
            throw new ValidationException(['password' => Lang::get('rainlab.user::lang.account.invalid_deactivation_pass')]);
        }

        Auth::logout();
        $user->delete();

        Flash::success(post('flash', Lang::get(/*Successfully deactivated your account. Sorry to see you go!*/'rainlab.user::lang.account.success_deactivation')));

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }

    /**
     * Trigger a subsequent activation email
     */
    public function onSendActivationEmail()
    {
        try {
            if (!$user = $this->user()) {
                throw new ApplicationException(Lang::get(/*You must be logged in first!*/'rainlab.user::lang.account.login_first'));
            }

            if ($user->is_activated) {
                throw new ApplicationException(Lang::get(/*Your account is already activated!*/'rainlab.user::lang.account.already_active'));
            }

            Flash::success(Lang::get(/*An activation email has been sent to your email address.*/'rainlab.user::lang.account.activation_email_sent'));

            $this->sendActivationEmail($user);

        }
        catch (Exception $ex) {
            
            if (Request::ajax()) {
                throw $ex;
            } else {
                Flash::error($ex->getMessage());
            }
        }

        /*
         * Redirect
         */
        if ($redirect = $this->makeRedirection()) {
            return $redirect;
        }
    }

    //
    // Helpers
    //

    /**
     * Returns a link used to activate the user account.
     * @return string
     */
    protected function makeActivationUrl($code)
    {
        $params = [
            $this->property('paramCode') => $code
        ];

        if ($pageName = $this->property('activationPage')) {
            $url = $this->pageUrl($pageName, $params);
        }
        else {
            $url = $this->currentPageUrl($params);
        }

        if (strpos($url, $code) === false) {
            $url .= '?activate=' . $code;
        }

        return $url;
    }

    /**
     * Sends the activation email to a user
     * @param  User $user
     * @return void
     */
    protected function sendActivationEmail($user)
    {
        $code = implode('!', [$user->id, $user->getActivationCode()]);

        $link = $this->makeActivationUrl($code);

        $data = [
            'name' => $user->name,
            'link' => $link,
            'code' => $code
        ];

        Mail::send('rainlab.user::mail.activate', $data, function($message) use ($user) {
            $message->to($user->email, $user->name);
        });
    }

    /**
     * Redirect to the intended page after successful update, sign in or registration.
     * The URL can come from the "redirect" property or the "redirect" postback value.
     * @return mixed
     */
    protected function makeRedirection($intended = false)
    {
        $method = $intended ? 'intended' : 'to';

        $property = $this->property('redirect');

        if (strlen($property) && !$property) {
            return;
        }

        $redirectUrl = $this->pageUrl($property) ?: $property;

        if ($redirectUrl = post('redirect', $redirectUrl)) {
            return Redirect::$method($redirectUrl);
        }
    }

    /**
     * Checks if the force secure property is enabled and if so
     * returns a redirect object.
     * @return mixed
     */
    protected function redirectForceSecure()
    {
        if (
            Request::secure() ||
            Request::ajax() ||
            !$this->property('forceSecure')
        ) {
            return;
        }

        return Redirect::secure(Request::path());
    }
    
    
    
    ///////////////////////////////////////////////////////////////////
    //
    //  PAMONLINE FUNCTIONS PULLED UP INTO THE MAIN USER CLASS
    //
    ///////////////////////////////////////////////////////////////////
    
    
    
    /**
     * Get roles
     * @return mixed
     */
    public function roles() {

        $return = [];
        $groups = Group::all();

        foreach($groups as $group) {
            $return[$group->id] = $group->name;
        }
        return $return;
    }
    
    
    public function getSalutations() {

        $return = [];
        $salutations = Salutation::all();

        foreach($salutations as $salutation){
            $return[$salutation->name] = $salutation->name;
        }
        return $return;
    }
    
    
    /**
     * Get allowed user roles
     */
    public function allowedRoles() {
        return UserSettings::get('allowed_registration_types');
    }
    
    
    public function onGetNotifications() {

        $user = $this->user();
        $notifications = [];

        if(isset($user) && ($user->pamadmin || $user->member)) {
            
            $award = Award::getActiveAward();
            if($award && !AppSession::get('openaward'))
                $notifications[] = ["session_id" => 'openaward', 'data'=> array(),'callback'=>'account::onCloseNotifications',"type" => "info", "message" => "PAM Awards {$award->year->ay_year} are now open for entries. Click here to enter.", "url" => url('backend/twnepc/awards/pamscategories')];
        }

        if(isset($user) && $user->isJudge()) {
//        if(Award::getActiveAward() && !AppSession::get('openaward'))
//            $notifications[] = ["type" => "info", "message" => "Check it out! Click here to view active PAM Awards", "url" => url('admin-panel/twnepc/awards/judgeawards')];
        }

        if(isset($user)) {

            $allNotifications = Companies::getAllUserCompaniesSubscriptions($user->attributes['id']);
            $maxNotifications = 2;

            $i = 0;
            foreach ($allNotifications as $notification) {
                
                if (++$i > $maxNotifications) {
                    break;
                }
                $notifications[] = ["session_id" => 'companyNotificator', 'data'=>$notification, 'callback'=>'account::onCloseCompanyNotification',"type" => "info", "message" => $notification["text"], "url" => '/'.$notification["url"]];
            }

//            $notifications[] = ["type" => "error", "message" => "This is test error", "url" => false];
//            $notifications[] = ["type" => "info", "message" => "This is test success", "url" => false];
//            $notifications[] = ["type" => "warning", "message" => "This is test warning", "url" => false];
        }

        return $notifications;
    }
    
    
    public function onCloseCompanyNotification() {
        
        $post = Input::all();
        $user = $this->user();
        Companies::userSectionUpdate($user->id, $post['data']['company_id'], $post['data']['type']);

        return;
    }
    
    
    public function onCloseNotifications() {
        
        if(post('key',false) != false && post('value',false) != false){
            AppSession::put(post('key'), post('value'));
        }
        return;
    }
    
    
    public function getCompanyNameByCompanyId($compid,$default = "") {

        $company = Companies::where('company_id',$compid)->first();
        if($company) {
            return $company->c_companyname;
        }
        
        return $default;
    }
    
    
    public function onCompanySelect() {

        $results = [];
        $term = post('term');

        $companies = Db::table('pamtbl_company')->where('c_active', 1)->where('c_companyname', 'like', "%{$term}%")->take(10)->get();
        foreach($companies as $company) {
            $results[] = $company->c_companyname;
        }

        return $results;
    }
    
    
    /**
     * get longitude and latitude from postcode
     * @param string $zip the postcode
     * @param string $countryId the id on the country table
     * @return array $postCodeCoords the coordinates
     */
    public function getZipLongLat($zip, $countryId) {
        
        // this is from the PAMOnline website, but will now work with PAMInsight...
        if(empty($zip) || empty($countryId)) {
            return false;
        }
        
        $country = "";
        $site = SettingHelper::getCurrentSite();
        
        // check which site we are using and execute the according query
        if(in_array($site->id, [1, 2, 3, 4])) { 
            // on the PAMInsight site...
            $country = \Db::table('rainlab_location_countries')->where(["id" => intval($countryId)])->first();
            $country = ($country) ? $country->name : 'UK';
            
        } else if($site->id === 5) {
            // on the PAMOnlne site...
            $country = \Db::table('pamtbl_country')->where(["country_id" => intval($countryId)])->first();
            $country = ($country) ? $country->country_name : 'UK';
        }
        
        
        // call function to return array of long and lat...
        $postCodeCoords = userModel::checkPostcode($zip, $country);
        if($postCodeCoords === false) {
            return false;
        }
        
        return $postCodeCoords;
    }
    
    
    
    public function onResendEmail() {

        try {
            $rules = [
                'email' => 'required|email|between:6,255'
            ];

            $validation = Validator::make(post(), $rules);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            }

            if (!$user = userModel::where('email',post('email'))->where('is_activated',0)->first()) {
                throw new ApplicationException(trans('rainlab.user::lang.account.invalid_user'));
            }

            $this->sendActivationEmail($user);

        } catch(ValidationException $e) {
            throw $e;
        } catch(ApplicationException $e) {
            throw new ValidationException(array(
                'email' => 'Email incorrect.',
            ));
        }
    }
    
    
    public function backendUrl($id) {
        return Backend::url('twnepc/pamcompanies/company/update/'.$id);
    }
    
    
    public function backendAwardUrl() {
        return Backend::url('twnepc/awards/pamsawards');
    }
    
    
    public function backendAwardJudgeUrl() {
        return Backend::url('twnepc/awards/judgeawards');
    }
    
    
    public function getCountries() {
        return Country::orderBy('country_name','asc')->get();
    }
    
    
    public function companiesList() {
        $companies = Db::table('pamonline_pamcompanies_companies')->lists('name');
        return json_encode($companies);
    }
    
    
    public function onCompaniesList() {
        $companies = Db::table('pamonline_pamcompanies_companies')->lists('name');
        return $companies;
    }
    
    
    public function getActiveAward() {
        $activeAward = null;
        $activeAward = Award::getActiveAward();
        return $activeAward;
    }
    
    
    
    
    
}
