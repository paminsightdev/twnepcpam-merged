<?php namespace RainLab\User\Classes;

use October\Rain\Auth\Manager as RainAuthManager;
use RainLab\User\Models\Settings as UserSettings;
use RainLab\User\Models\UserGroup as UserGroupModel;
use Keios\Multisite\Classes\SettingHelper;

class AuthManager extends RainAuthManager
{
    protected static $instance;

    protected $sessionKey = 'user_auth';

    protected $userModel = 'RainLab\User\Models\User';

    protected $groupModel = 'RainLab\User\Models\UserGroup';

    protected $throttleModel = 'RainLab\User\Models\Throttle';

    public function init()
    {
        $this->useThrottle = UserSettings::get('use_throttle', $this->useThrottle);
        $this->requireActivation = UserSettings::get('require_activation', $this->requireActivation);
        parent::init();
    }

    
    /**
     * 
     * @param array $credentials
     * @param boolean $remember
     * @return type
     */
    public function authenticate(array $credentials, $remember = true) {
//        echo "authenticate";
//        dd($credentials);
        
        switch(SettingHelper::getCurrentSite()->id) {
            // when this is for PAMOnline
            case 5 : return $this->authenticatePAM($credentials, $remember); break;
            // for everything else at the moment...
            default : return parent::authenticate($credentials,$remember); break;
        }
        
    }
    
    
    /**
     * 
     * @param array $credentials
     * @param boolean $remember
     * @return type
     * 
     * @Todo - check the PAM Users have all migrated to TWNEPC tables and related database tables 
     * along with backend_users, etc. This function should not be needed and the call to it from 
     * the authenticate function should be removed too.
     */
    public function authenticatePAM(array $credentials, $remember = true)
    {   
        $roles = UserSettings::get('user_roles_throttle_ignore',array());
        $login = array_get($credentials, 'login', null);
        $user = $this->findUserByLogin($login);
        if($user && is_array($roles)){
            foreach($user->roles as $role){
                if(in_array($role->id,$roles)){
                    $this->useThrottle = false;
                    break;
                }
            }
        }
        return parent::authenticate($credentials,$remember);
    }
    
    
    /**
     * {@inheritDoc}
     */
    public function extendUserQuery($query)
    {
        $query->withTrashed();
    }

    /**
     * {@inheritDoc}
     */
    public function register(array $credentials, $activate = false, $autoLogin = true)
    {
        if ($guest = $this->findGuestUserByCredentials($credentials)) {
            return $this->convertGuestToUser($guest, $credentials, $activate);
        }

        return parent::register($credentials, $activate, $autoLogin);
    }

    //
    // Guest users
    //

    public function findGuestUserByCredentials(array $credentials)
    {
        if ($email = array_get($credentials, 'email')) {
            return $this->findGuestUser($email);
        }

        return null;
    }

    public function findGuestUser($email)
    {
        $query = $this->createUserModelQuery();

        return $user = $query
            ->where('email', $email)
            ->where('is_guest', 1)
            ->first();
    }

    /**
     * Registers a guest user by giving the required credentials.
     *
     * @param array $credentials
     * @return Models\User
     */
    public function registerGuest(array $credentials)
    {
        $user = $this->findGuestUserByCredentials($credentials);
        $newUser = false;

        if (!$user) {
            $user = $this->createUserModel();
            $newUser = true;
        }

        $user->fill($credentials);
        $user->is_guest = true;
        $user->save();

        // Add user to guest group
        if ($newUser && $group = UserGroupModel::getGuestGroup()) {
            $user->groups()->add($group);
        }

        // Prevents revalidation of the password field
        // on subsequent saves to this model object
        $user->password = null;

        return $this->user = $user;
    }

    /**
     * Converts a guest user to a registered user.
     *
     * @param Models\User $user
     * @param array $credentials
     * @param bool $activate
     * @return Models\User
     */
    public function convertGuestToUser($user, $credentials, $activate = false)
    {
        $user->fill($credentials);
        $user->convertToRegistered(false);

        // Remove user from guest group
        if ($group = UserGroupModel::getGuestGroup()) {
            $user->groups()->remove($group);
        }

        if ($activate) {
            $user->attemptActivation($user->getActivationCode());
        }

        // Prevents revalidation of the password field
        // on subsequent saves to this model object
        $user->password = null;

        return $this->user = $user;
    }
 
    
}
