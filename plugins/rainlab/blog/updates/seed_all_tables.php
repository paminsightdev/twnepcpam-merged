<?php namespace RainLab\Blog\Updates;

use Schema;
use Carbon\Carbon;
use RainLab\Blog\Models\Post;
use RainLab\Blog\Models\Category;
use October\Rain\Database\Updates\Seeder;

class SeedAllTables extends Seeder
{

    public function run()
    {
        // check table exists
        if(Schema::hasTable('rainlab_blog_posts')) {
            // when there is no data put dummy one in
            if(Post::all()->count() == 0) {


                Post::create([
                    'title' => 'First blog post',
                    'slug' => 'first-blog-post',
                    'content' => '
        This is your first ever **blog post**! It might be a good idea to update this post with some more relevant content.

        You can edit this content by selecting **Blog** from the administration back-end menu.

        *Enjoy the good times!*
                    ',
                    'excerpt' => 'The first ever blog post is here. It might be a good idea to update this post with some more relevant content.',
                    'published_at' => Carbon::now(),
                    'published' => true
                ]);
            }
        }
        
        
        // check table exists
        if (Schema::hasTable('rainlab_blog_categories')) {
            // when there is no data put dummy one in
            if(Category::all()->count() == 0) {

                Category::create([
                    'name' => trans('rainlab.blog::lang.categories.uncategorized'),
                    'slug' => 'uncategorized',
                ]);
            }
        }
    }

}
