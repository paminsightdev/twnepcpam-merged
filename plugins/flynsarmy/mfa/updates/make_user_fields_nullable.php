<?php namespace Flynsarmy\Mfa\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class MakeUserFieldsNullable extends Migration
{
    public function up()
    {
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_enabled` TINYINT(1) DEFAULT 0;');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_secret` VARCHAR(255) DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_persist_code` VARCHAR(255) DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_question_1` VARCHAR(255) DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_answer_1` VARCHAR(255) DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_question_2` VARCHAR(255) DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_answer_2` VARCHAR(255) DEFAULT "";');
    }

    public function down()
    {
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_enabled` tinyint(1) NOT NULL DEFAULT 0;');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_secret` VARCHAR(255) NOT NULL DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_persist_code` VARCHAR(255) NOT NULL DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_question_1` VARCHAR(255) NOT NULL DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_answer_1` VARCHAR(255) NOT NULL DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_question_2` VARCHAR(255) NOT NULL DEFAULT "";');
        DB::statement('ALTER TABLE `backend_users` MODIFY `mfa_answer_2` VARCHAR(255) NOT NULL DEFAULT "";');
    }
}
