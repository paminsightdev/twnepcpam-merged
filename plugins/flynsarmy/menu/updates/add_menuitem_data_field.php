<?php namespace Flynsarmy\Menu\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Flynsarmy\Menu\Models\Menuitem;

class AddMenuitemDataField extends Migration
{

	public function up()
	{
            if(Schema::hasTable('flynsarmy_menu_menuitems')) {
                // when there is no data put dummy one in
                if(Menuitem::all()->count() == 0) {
                    Schema::table('flynsarmy_menu_menuitems', function($table)
                    {
                            $table->text('data')->nullable();
                    });
                }
            }
	}

	public function down()
	{
		Schema::table('flynsarmy_menu_menuitems', function($table)
		{
			$table->dropColumn('data');
		});
	}

}
