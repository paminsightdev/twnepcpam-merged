<?php

namespace Flynsarmy\Menu\Components;

use Validator;
use Cms\Classes\ComponentBase;
use October\Rain\Support\ValidationException;
use Flynsarmy\Menu\Models\Menu as MenuModel;
use Flynsarmy\Menu\Models\Menuitem as MenuModelItem;
use System\Classes\ApplicationException;

class UsefulLinks extends ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'PAM Usefull links',
            'description' => 'Displays a given menu.'
        ];
    }

    public function defineProperties() {
        return [
            // Menu selection
            'menu_id' => [
                'title' => 'Menu',
                'type' => 'dropdown',
            ],
        ];
    }
   
    public function getMenu_idOptions()
    {
	return MenuModel::select('id', 'name')->orderBy('name')->get()->lists('name', 'id');
    }    

    public function onRun() {
        $this->page['test'] = 'This is test message.';
    }

    public function onRender() {
        $menu = MenuModel::where("id",$this->property('menu_id', 0))->get();
        $records = MenuModelItem::where("menu_id",$this->property('menu_id', 0))->orderBy('created_at', 'desc')->get();
        $posts =  [];
        foreach ($records as $record) {
            $posts[] = $record->attributes;
        }
        
        if ($menu = current(current($menu))) {
            $this->page['menu'] =  $menu->attributes;
        }
        
        $this->page['posts'] = $posts;
    }

}
