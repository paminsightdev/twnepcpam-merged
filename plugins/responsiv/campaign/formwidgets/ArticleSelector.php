<?php namespace Responsiv\Campaign\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Config;

use Twnepc\News\Models\Article;


class ArticleSelector extends FormWidgetBase {

    public function widgetDetails() {
        
        return NULL;


    }

    public function prepareVars(){
        $this->vars['id'] = $this->model->id;

        $this->vars['name'] = $this->formField->getName();

        $selected = $this->model->article()->pluck('title','id');
        $this->vars['selected']= $selected;


    }

    public function render() {

        $this->prepareVars();
        return $this->makePartial('widget');
    }

    public function loadAssets() {
        //$this->addCss('css/select2.css');
        $this->addJs('js/select2.js');
    }

}

