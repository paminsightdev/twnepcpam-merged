/*
 * Template Selector plugin
 * 
 * Data attributes:
 * - data-control="campaign-templateselector" - enables the plugin on an element
 * - data-option="value" - an option with a value
 *
 * JavaScript API:
 * $('a#someElement').templateSelector({ option: 'value' })
 *
 * Dependences: 
 * - Some other plugin (filename.js)
 */

+function ($) { "use strict";

    // TEMPLATE SELECTOR CLASS DEFINITION
    // ============================

    var TemplateSelector = function(element, options) {
        this.options   = options
        this.$el       = $(element)

        // Init
        this.init()
    }

    TemplateSelector.DEFAULTS = {
        dataLocker: null
    }

    TemplateSelector.prototype.init = function() {
        var self = this
        this.$dataLocker  = $(this.options.dataLocker, this.$el);
        this.$siteLocker  = $('#Form-field-Message-site_id-group');
        this.$el.on('click', '>ul>li a', function(){
            self.selectTemplate(this)
        })
    }

    TemplateSelector.prototype.selectTemplate = function(el) {
        var $item = $(el).closest('li')

        $item.addClass('selected')
            .siblings().removeClass('selected')

        this.$dataLocker.val($item.data('value'))

        var siteId = 0;
        switch($item.data('value')){
            case 'campaign/twn-breaking-template':
            case 'campaign/twn-news-alert-template':
                siteId = 1;
                break;
            case 'campaign/epc-breaking-template':
            case 'campaign/epc-news-alert-template':
                siteId = 2;
                break;
            case 'campaign/fundeye-breaking-template':
            case 'campaign/fundeye-news-alert-template':
                siteId = 4;
                break;
            default:
                siteId = 1;
                break;
        }

        this.$siteLocker.val(siteId);
    }

    // TEMPLATE SELECTOR PLUGIN DEFINITION
    // ============================

    var old = $.fn.templateSelector

    $.fn.templateSelector = function (option) {
        var args = Array.prototype.slice.call(arguments, 1), result
        this.each(function () {
            var $this   = $(this)
            var data    = $this.data('oc.campaignTemplateSelector')
            var options = $.extend({}, TemplateSelector.DEFAULTS, $this.data(), typeof option == 'object' && option)
            if (!data) $this.data('oc.campaignTemplateSelector', (data = new TemplateSelector(this, options)))
            if (typeof option == 'string') result = data[option].apply(data, args)
            if (typeof result != 'undefined') return false
        })
        
        return result ? result : this
    }

    $.fn.templateSelector.Constructor = TemplateSelector

    // TEMPLATE SELECTOR NO CONFLICT
    // =================

    $.fn.templateSelector.noConflict = function () {
        $.fn.templateSelector = old
        return this
    }

    // TEMPLATE SELECTOR DATA-API
    // ===============

    $(document).render(function() {
        $('[data-control="campaign-templateselector"]').templateSelector()
    });

}(window.jQuery);