<?php namespace Responsiv\Campaign\Controllers;

use Flash;
use Request;
use Redirect;
use BackendMenu;
use Backend\Classes\Controller;
use Responsiv\Campaign\Helpers\RecipientGroup;
use Responsiv\Campaign\Classes\CampaignManager;
use ApplicationException;
use Responsiv\Campaign\Models\Subscriber;
use ValidationException;
use Rainlab\User\Models\User;

/**
 * Lists Back-end Controller
 */
class Lists extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = ['responsiv.campaign.manage_subscribers'];

    public function __construct()
    {
        parent::__construct();
        $this->addCss('/plugins/responsiv/campaign/assets/css/mod.css','Responsiv.Campaign');
        BackendMenu::setContext('Responsiv.Campaign', 'campaign', 'lists');
    }

    public function preview($recordId = null, $context = null)
    {
        $this->bodyClass = 'slim-container';

        

        $result = $this->asExtension('FormController')->preview($recordId, $context);

        if ($model = $this->formGetModel()) {
            $this->pageTitle = $model->name;
        }

        return $result;
    }

    public function preview_onDelete($recordId = null)
    {
        return $this->asExtension('FormController')->update_onDelete($recordId);
    }

    public function onLoadAddRecipientGroup()
    {
        $groups = RecipientGroup::listRecipientGroups();

        $this->vars['groups'] = $groups;

        return $this->makePartial('add_recipient_group_form');
    }

    public function onLoadAddSubscriber()
    {
        $users = User::all();
        $this->vars['users'] = $users;
        return $this->makePartial('add_subscriber_form');
    }

    public function onAddRecipientGroup($recordId = null)
    {
        if (!$type = post('type')) {
            throw new ValidationException(['type' => 'Select a type to add to the list.']);
        }

        if (!$list = $this->formFindModelObject($recordId)) {
            throw new ApplicationException('Fatal error: unable to find list');
        }

        $ids = CampaignManager::instance()->getSubscribersFromRecipientTypes($type);

        if (count($ids) > 0) {
            $list->subscribers()->sync($ids, false);
        }

        Flash::success('Added recipients to the list');

        return Redirect::refresh();
    }

    public function onAddSubscriber($recordId = null)
    {
        $userId = post('user');
        $user = User::find($userId);

        if($user){
            if (!$list = $this->formFindModelObject($recordId)) {
                throw new ApplicationException('Fatal error: unable to find list');
            }
            $subscriber = Subscriber::signup([
                'email' => $user->email,
                'first_name' => $user->name,
                'last_name' => $user->surname,
                'created_ip_address' => Request::ip()
            ], $list->code, 0, $user->id);
        }

        Flash::success('Added Users to the list');

        return Redirect::refresh();
    }
}
