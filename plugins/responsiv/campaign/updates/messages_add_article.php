<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class MessagesAddArticle extends Migration
{
    public function up()
    {
        Schema::table('responsiv_campaign_messages', function($table)
        {
            $table->integer('article_id')->unsigned()->nullable();
        });
    }

    public function down()
    {
        Schema::table('responsiv_campaign_messages', function($table)
        {
            $table->dropColumn('article_id');
        });
    }

}
