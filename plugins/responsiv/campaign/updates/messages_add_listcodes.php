<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class MessagesAddListcodes extends Migration
{
    public function up()
    {
        Schema::table('responsiv_campaign_messages', function($table)
        {
            $table->string('list_codes')->nullable();
        });
    }

    public function down()
    {
        Schema::table('responsiv_campaign_messages', function($table)
        {
            $table->dropColumn('list_codes');
        });
    }

}
