<?php namespace RainLab\User\Updates;

use Carbon\Carbon;
use Schema;
use October\Rain\Database\Updates\Migration;
use Responsiv\Campaign\Models\Subscriber as SubscriberModel;

class SubscribersAddUserId extends Migration
{
    public function up()
    {
        Schema::table('responsiv_campaign_subscribers', function($table)
        {
            $table->integer('user_id')->unsigned()->nullable();
        });
    }

    public function down()
    {
        Schema::table('responsiv_campaign_subscribers', function($table)
        {
            $table->dropColumn('user_id');
        });
    }

}
