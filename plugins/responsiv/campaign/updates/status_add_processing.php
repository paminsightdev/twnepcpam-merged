<?php namespace Responsiv\Campaign\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Responsiv\Campaign\Models\MessageStatus;

class StatusAddProcessing extends Migration
{

    public function up()
    {
        MessageStatus::create(['name' => 'Processing', 'code' => 'processing']);
    }

    public function down()
    {
    }

}
