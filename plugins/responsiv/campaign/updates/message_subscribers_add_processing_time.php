<?php namespace RainLab\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Responsiv\Campaign\Models\Message as MessageModel;

class MessageSubscribersAddProcessingTime extends Migration
{
    public function up()
    {
        Schema::table('responsiv_campaign_messages_subscribers', function($table)
        {
            $table->timestamp('processing_time')->nullable()->after('subscriber_id');
        });
    }

    public function down()
    {
        Schema::table('responsiv_campaign_messages_subscribers', function($table)
        {
            $table->dropColumn('processing_time');
        });
    }

}
