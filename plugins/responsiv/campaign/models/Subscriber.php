<?php namespace Responsiv\Campaign\Models;

use Model;
use Carbon\Carbon;
use ApplicationException;

use Twnepc\Logs\Models\PamSplunkTroubleShooting;

/**
 * Subscriber Model
 */
class Subscriber extends Model
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'responsiv_campaign_subscribers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['email', 'first_name', 'last_name'];

    /**
     * @var array Date fields
     */
    public $dates = ['confirmed_at', 'unsubscribed_at'];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'subscriber_lists' => ['Responsiv\Campaign\Models\SubscriberList', 'table' => 'responsiv_campaign_lists_subscribers', 'otherKey' => 'list_id'],
        'messages' => ['Responsiv\Campaign\Models\Message', 'table' => 'responsiv_campaign_messages_subscribers', 'otherKey' => 'message_id'],
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'email' => 'required|between:3,64|email|unique:responsiv_campaign_subscribers',
    ];

    /**
     * @var bool Set this to true to automatically confirm the new subscriber.
     */
    public $autoConfirm;

    public function afterDelete()
    {
        $this->subscriber_lists()->detach();
        $this->messages()->detach();
    }

    public function beforeCreate()
    {
        if ($this->autoConfirm) {
            $this->confirmed_at = $this->freshTimestamp();
        }
    }

    /**
     * Signs up a user as a subscriber, adds them to supplied list code.
     */
    public static function signup($details, $addToList = null, $isConfirmed = true, $userId = null)
    {
        if (is_string($details)) {
            $details = ['email' => $details];
        }

        if (!$email = array_get($details, 'email')) {
            throw new ApplicationException('Missing email for subscriber!');
        }

        $subscriber = self::firstOrNew(['email' => $email]);
        $subscriber->first_name = array_get($details, 'first_name');
        $subscriber->last_name = array_get($details, 'last_name');
        $subscriber->created_ip_address = array_get($details, 'created_ip_address');
        
        /* troubleshooting log */
        // (null !== $email && !empty($email))? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','signup','email', $email) : '';
        // (null !== $subscriber->first_name && !empty($subscriber->first_name)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','signup','subscriber->first_name', $subscriber->first_name) : '';
        // (null !== $subscriber->last_name && !empty($subscriber->last_name)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','signup','subscriber->last_name', $subscriber->last_name) : '';
        // (null !== $subscriber->created_ip_address && !empty($subscriber->created_ip_address)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','signup','subscriber->created_ip_address', $subscriber->created_ip_address) : '';

        if($userId){
            $subscriber->user_id = $userId;
        }

        if ($isConfirmed) {
            $subscriber->confirmed_ip_address = array_get($details, 'created_ip_address');
            $subscriber->confirmed_at = $subscriber->freshTimestamp();
            $subscriber->unsubscribed_at = null;
            /* troubleshooting log */
            // (null !== $subscriber->confirmed_ip_address && !empty($subscriber->confirmed_ip_address)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','signup','subscriber->confirmed_ip_address', $subscriber->confirmed_ip_address) : '';
            // (null !== $subscriber->confirmed_at && !empty($subscriber->confirmed_at)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','signup','subscriber->confirmed_at', $subscriber->confirmed_at) : '';
            // (null !== $subscriber->unsubscribed_at && !empty($subscriber->unsubscribed_at)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','signup','subscriber->unsubscribed_at', $subscriber->unsubscribed_at) : '';
        }

        // Already subscribed and opted-in
        if ($subscriber->exists && !$subscriber->unsubscribed_at) {
            $subscriber->unsubscribed_at = null;
        }
        
        $subscriber->save();

        if ($addToList) {

            if (!$list = SubscriberList::where('code', $addToList)->first()) {
                throw new ApplicationException('Unable to find a list with code: ' . $addToList);
            }

            if (!$list->subscribers()->where('id', $subscriber->id)->count()) {
                $list->subscribers()->add($subscriber);
                /* troubleshooting log */
                // (null !== $subscriber->id && !empty($subscriber->id)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','signup','list->subscribers()->add($subscriber)', $subscriber->id) : '';
            }

        }

        return $subscriber;
    }

    public function attemptVerification($ipAddress = null)
    {
        // Already verified!
        if ($this->confirmed_at) {
            return false;
        }

        if ($ipAddress) {
            $this->confirmed_ip_address = $ipAddress;
        }

        $this->confirmed_at = $this->freshTimestamp();
        $this->unsubscribed_at = null;
        $this->forceSave();

        return true;
    }

    public function attemptUnsubscribe($removeFromList = null)
    {
        if($removeFromList){
            if (!$list = SubscriberList::where('code', $removeFromList)->first()) {
                throw new ApplicationException('Unable to find a list with code: ' . $removeFromList);
            }

            if ($subscriber = $list->subscribers()->where('id', $this->id)->first()) {
                $list->subscribers()->remove($subscriber);
                /* troubleshooting log */
                // (null !== $this->id && !empty($this->id)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/responsiv/campaign/models/Subscriber.php','attemptUnsubscribe','list->subscribers()->remove($subscriber)', $this->id) : '';
            }
        }

        // Already unsubscribed!

        $this->forceSave();

        return true;
    }

    public function getUniqueCode()
    {
        $hash = md5($this->id . '!' . $this->email);
        return base64_encode($this->id.'!'.$hash);
    }

    /**
     * Returns true if IP address is throttled and cannot subscribe
     * again. Maximum 3 subscription every 15 minutes.
     * @return bool
     */
    public static function checkThrottle($ip)
    {
        if (!$ip) {
            return false;
        }

        $timeLimit = Carbon::now()->subMinutes(15);
        $count = static::make()
            ->where('created_ip_address', $ip)
            ->where('created_at', '>', $timeLimit)
            ->count()
        ;

        return $count > 2;
    }

}