<?php namespace Responsiv\Campaign\Models;

use Illuminate\Support\Facades\Input;
use Keios\Multisite\Classes\SettingHelper;
use Illuminate\Support\Facades\Log;
use Keios\Multisite\Models\Setting;
use Lang;
use Model;
use Event;
use Config;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Cms\Classes\Controller as CmsController;
use October\Rain\Parse\Bracket as TextParser;
use Responsiv\Campaign\Helpers\RecipientGroup;
use Twnepc\News\Models\Article;
use System\Classes\MailManager;
use ApplicationException;

/**
 * Message Model
 */
class Message extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Parse\Syntax\SyntaxModelTrait;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'responsiv_campaign_messages';

    /**
     * @var array Date fields
     */
    public $dates = ['launch_at', 'processed_at'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array List of attribute names which are json encoded and decoded from the database.
     */
    protected $jsonable = ['syntax_data', 'syntax_fields', 'groups'];

    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required|between:2,128',
        'page' => 'required'
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'status' => ['Responsiv\Campaign\Models\MessageStatus'],
        'article' => ['Twnepc\News\Models\Article'],
    ];

    public $belongsToMany = [
        'subscriber_lists' => [
            'Responsiv\Campaign\Models\SubscriberList',
            'table' => 'responsiv_campaign_messages_lists',
            'otherKey' => 'list_id'
        ],
        'subscribers' => [
            'Responsiv\Campaign\Models\Subscriber',
            'table' => 'responsiv_campaign_messages_subscribers',
            'pivot' => ['sent_id','sent_at', 'read_at', 'stop_at','processing_time']
        ],
    ];


    public $attachMany = [
        'attachments' => ['System\Models\File']
    ];

    public function beforeCreate()
    {
        if (empty($this->subject)) {
            $this->subject = $this->name;
        }

        if (empty($this->status_id)) {
            $this->status_id = MessageStatus::getDraftStatus()->id;
        }

        if (empty($this->content)) {
            $this->rebuildContent();
        }

        if (Input::has('Message')) {
            $message = Input::get('Message');
            $this->site_id = $message['site_id'];
        }
    }

    public function beforeSave()
    {
        $this->content_html = $this->renderTemplate();
        if(!$this->article_ids && $this->status_id == MessageStatus::getPendingStatus()->id){
            $articles = Article::where('is_sent',0)
                ->where('status_id',3)
                ->where('not_include_in_email',0)
                ->where('breaking',0)
                ->where('site_id',$this->site_id)
                ->where('published_date', date('Y-m-d'))
                ->orderBy('published_date', 'desc')
                ->orderBy('order', 'asc')
                ->limit(10)
                ->get();

            $ids = $articles->lists('id');
            if($articles->count()){
                $this->article_ids = implode(',',$ids);
            }

        }

        if(!$this->list_codes && $this->status_id == MessageStatus::getPendingStatus()->id){
            $message = Input::get('Message');
            $listIds = $message['subscriber_lists'];
            $listCodes = [];
            foreach($listIds as $listId){
                if($list = SubscriberList::find($listId)){
                    $listCodes[] = $list->code;
                }

            }
            $this->list_codes =  implode(',',$listCodes);

        }

        if($this->status_id == MessageStatus::getSentStatus()->id){
            $ids = explode(',',$this->article_ids);
            $articles = Article::whereIn('id',$ids)->update(['is_sent' => 1]);
        }

    }

    public function afterDelete()
    {
        $this->subscriber_lists()->detach();
        $this->subscribers()->detach();
    }

    public function getUniqueCode($subscriber)
    {
        $value = $this->id.'!'.$subscriber->id;
        $hash = md5($value . '!' . $subscriber->email);
        return base64_encode($value.'!'.$hash);
    }

    public function getBrowserUrl($subscriber)
    {
        $code = $this->getUniqueCode($subscriber);
        $site = Setting::find($this->site_id);
        $domain = $site->domain;

        if(strpos($this->page,'breaking') === false){
            $path = '/campaign/news/'.$code;
        }else{
            $path = '/campaign/breaking/'.$code;
        }

        return $domain . $path;
        //return Page::url($this->page, ['code' => $code]);
    }

    public function getMyAccountUrl($subscriber)
    {
        $site = Setting::find($this->site_id);
        $domain = $site->domain;

        $path = '/account#account-newsletter';

        return $domain . $path;
    }

    public function getLatestNews($live, $subscriber)
    {
        // TODO: update articles if they are sent
       $ids = explode(',',$this->article_ids);
       if($live){
           $articles = Article::whereIn('id',$ids)
               ->where('site_id',$this->site_id)
               ->get();
       }else{
           $articles = Article::where('is_sent',0)
               ->where('status_id',3)
               ->where('not_include_in_email',0)
               ->where('breaking',0)
               ->where('site_id',$this->site_id)
               ->where('published_date', date('Y-m-d'))
               ->orderBy('published_date', 'desc')
               ->orderBy('order', 'asc')
               ->limit(10)
               ->get();
       }


       $news = '';
       foreach($articles as $article){
           $baseUrl = Setting::find($article->site_id)->domain;
           $news .= '<div style="padding-bottom: 10px; padding-top: 10px; border-bottom: 1px solid #F1F1F1;">';
           $news .= '<p style="margin: 0; font-size: 14px; line-height: 18px; color: #333; font-family: Helvetica, Arial, sans-serif; padding: 2px 0 0 0;"><a href="'.$baseUrl.'/article/'.$article->slug.'?message='.$this->id.'" style=" font-size: 14px; color: #222222; font-family: Helvetica, Arial, sans-serif; text-decoration: none;">'.$article->title.'<br>';
           if($article->author && $article->author->author_name != 'News Team' && !$article->contributor){
               $news .= '<span style="font-size: 12px; color: #333;">by '.$article->author->author_name.'</span>';
           }else if($article->contributor){
               $news .= '<span style="font-size: 12px; color: #333;">by '.$article->contributor.'</span>';
           }
           $news .= '</a></p></div>';
       }

       if(!$news){
           if($live){
               Log::error('-----------EMPTY EMAIL--------------');
               Log::error('--------------messageId:'.$this->id.'-----------');
               Log::error('--------------pageId:'.$this->page.'-----------');
               Log::error('--------------articleIds:'.implode("|",$ids).'-----------');
               Log::error('--------------subscriberEmail:'.($subscriber ? $subscriber->email : 'false').'-----------');
               Log::error('-----------EMPTY EMAIL END----------');
           }
           $news = 'No Latest News';
       }

        return $news;
    }

    public function getBreakingNews()
    {
        $ids = explode(',',$this->article_ids);
        $articleId = isset($ids[0]) ? $ids[0] : 0;
        $news = '';
        if($articleId){
            $article = Article::find($articleId);
            $baseUrl = Setting::find($article->site_id)->domain;
            if($article){
                $news = '<p style="margin: 0; font-size: 17px; line-height: 20px; color: #000; font-family: Helvetica, Arial, sans-serif; padding: 2px 0 5px 0;">'.$article->title.'</p>';
                //$news .= '<div style="font-size: 14px; color: #222; line-height: 18px; padding-bottom: 10px; font-family: Helvetica, Arial, sans-serif;">'.$article->summary.'</div>';
                $news .= '<a href="'.$baseUrl.'/article/'.$article->slug.'?message='.$this->id.'" style=" font-size: 14px; color: #222222; font-family: Helvetica, Arial, sans-serif; text-decoration: none;"><strong>Read more »</strong></a>';
                if($article->author && $article->author->author_name != 'News Team' && !$article->contributor){
                    $news .= '<span style="font-size: 12px; color: #333;">by '.$article->author->author_name.'</span>';
                }else if($article->contributor){
                    $news .= '<span style="font-size: 12px; color: #333;">by '.$article->contributor.'</span>';
                }
            }
        }

        if(!$news){
            $news = 'No Breaking News';
        }

        return $news;
    }

    /**
     * Rebuild and sync fields and data
     */
    public function rebuildContent()
    {
        $this->content = $this->getPageContent($this->page);
        $this->makeSyntaxFields($this->content);
        return $this;
    }

    /**
     * Rebuilds the statistics for the campaign
     * @return void
     */
    public function rebuildStats()
    {
        $this->count_subscriber = $this->subscribers()->count();
        $this->count_sent = $this->subscribers()->whereNotNull('sent_at')->count();
        $this->count_read = $this->subscribers()->whereNotNull('read_at')->count();
        $this->count_stop = $this->subscribers()->whereNotNull('stop_at')->count();
        return $this;
    }

    public function getExtendedStats()
    {
        return (object) [
            'open_rate' => $this->count_read && $this->count_sent ? round(($this->count_read / $this->count_sent) * 100) : 0,
            'stop_rate' => $this->count_stop && $this->count_sent ? round(($this->count_stop / $this->count_sent) * 100) : 0,
            'count_unread' => $this->count_sent - $this->count_read,
            'count_happy' => $this->count_sent - $this->count_stop
        ];
    }

    /**
     * Determines how many messages to send each hour, if staggered option is enabled.
     */
    public function getStaggerCount()
    {
        if (!$this->is_staggered) return -1;

        /*
         * Stagger by time
         */
        if ($this->stagger_type == 'time') {
            $spread = max(1, (int) $this->stagger_time);
            return ceil($this->count_subscriber / $spread);
        }

        /*
         * Stagger by count
         */
        return max(1, (int) $this->stagger_count);
    }

    /**
     * Determines if a message can be processed. For staggered messages,
     * it can be processed again after 15 minutes. Otherwise, 1 hour.
     * @return bool
     */
    public function canBeProcessed()
    {
        if (!$this->processed_at) {
            return true;
        }

        if($this->status_id == MessageStatus::getActiveStatus()->id){
            return true;
        }

        $hourAgo = $this
            ->freshTimestamp()
            ->subMinutes($this->is_staggered ? 15 : 60)
        ;

        return $this->processed_at <= $hourAgo;
    }

    /**
     * Marks this message as being processed.
     */
    public function markProcessed()
    {
        self::where('id', $this->id)
            ->update(['processed_at' => $this->freshTimestamp()]);
    }

    public function duplicateCampaign()
    {
        $model = new self([
            'syntax_data'   => $this->syntax_data,
            'syntax_fields' => $this->syntax_fields,
            'groups'        => $this->groups,
            'page'          => $this->page,
            'content'       => $this->content,
            'name'          => $this->name,
            'subject'       => $this->subject,
            'is_staggered'  => $this->is_staggered,
            'stagger_type'  => $this->stagger_type,
            'stagger_time'  => $this->stagger_time,
            'stagger_count' => $this->stagger_count,
            'is_repeating'  => $this->is_repeating,
            'count_repeat'  => $this->count_repeat,
            'repeat_frequency' => $this->repeat_frequency,
        ]);

        $model->subscriber_lists = $this->subscriber_lists;
        return $model;
    }

    public function relatedArticles() {
        return $this->article();
    }

    public function getIterativeNameAttribute()
    {
        if ($this->is_repeating) {
            return $this->name . ' (#'.$this->count_repeat.')';
        }

        return $this->name;
    }

    //
    // Tag processing
    //

    /**
     * Returns all available tags for parsing and their descriptions.
     */
    public static function getAvailableTags()
    {
        return [
            'first_name'      => "Subscriber's first name",
            'last_name'       => "Subscriber's last name",
            'email'           => "Subscriber's email address",
            'unsubscribe_url' => "Link to unsubscribe from emails",
            'latest_news'     => "List of latest news",
            'breaking_news'   => "One breaking news",
            'browser_url'     => "Link to open web-based version",
            'banner'          => "Banner AD"
        ];
    }

    /**
     * Returns an array of tag data for this message and the subscriber.
     */
    public function buildTagData($subscriber, $live)
    {   if($subscriber) {
            $data = [];
            $listCodes = $this->list_codes;
            $data['first_name'] = $subscriber->first_name;
            $data['last_name'] = $subscriber->last_name;
            $data['email'] = $subscriber->email;
            //$data['unsubscribe_url'] = $this->getBrowserUrl($subscriber).'?lists='.urlencode($listCodes).'&unsubscribe=1';
            $data['unsubscribe_url'] = $this->getMyAccountUrl($subscriber);
            try{
                $data['latest_news'] = $this->getLatestNews($live, $subscriber);
            }catch(Exception $e){
                Log::error($e->getMessage());
            }
            $data['breaking_news'] = $this->getBreakingNews();
            $data['browser_url'] = $this->getBrowserUrl($subscriber);
            $data['tracking_pixel'] = $this->getTrackingPixelImage($subscriber);
            $data['tracking_url'] = $this->getBrowserUrl($subscriber).'.png';
            $data['banner'] = $this->getBanner($subscriber);
        } else {
            $data = [];
            try{
                $data['latest_news'] = $this->getLatestNews($live, $subscriber);
            }catch(Exception $e){
                Log::error($e->getMessage());
            }
            $data['breaking_news'] = $this->getBreakingNews();
            $data['banner'] = $this->getBanner();
        }
        return $data;

    }

    public function getBanner()
    {
        $siteId = $this->site_id;

        $site = Setting::where('id',$siteId)->first();

        return MailManager::instance()->renderPartial('banner-'.$site->theme);

    }

    public function getTrackingPixelImage($subscriber)
    {
        $src = $this->getBrowserUrl($subscriber).'.png';
        return '<img src="'.$src.'" alt="" />';
    }

    //
    // Group management
    //

    public function getGroupsOptions()
    {
        return RecipientGroup::listRecipientGroups();
    }

    //
    // Page management
    //

    /**
     * Returns a list of pages available in the theme.
     * @return array Returns an array of strings.
     */
    public function getPageOptions()
    {
        $result = [];

        $pages = $this->listPagesWithCampaignComponent();
        foreach ($pages as $baseName => $page) {
            $result[$baseName] = strlen($page->name) ? $page->name : $baseName;
        }

        if (!$result) {
            $result[null] = 'No pages found';
        }

        return $result;
    }

    /**
     * Returns a collection of page objects that use the
     * Campaign Component provided by this plugin.
     * @return array
     */
    public function listPagesWithCampaignComponent()
    {
        $result = [];
        $pages = Page::withComponent('campaignTemplate')->sortBy('baseFileName')->all();

        foreach ($pages as $page) {
            $baseName = $page->getBaseFileName();
            $result[$baseName] = $page;
        }

        return $result;
    }

    public function getPageObject()
    {
        return Page::find($this->page);
    }

    public function getPageName()
    {
        return ($page = $this->getPageObject()) ? $page->title : $this->page;
    }

    protected function getPageContent($page)
    {
        $theme = Theme::getEditTheme();
        $result = CmsController::render($page, ['code' => LARAVEL_START], $theme);
        return $result;
    }

    public function renderTemplate()
    {
        if (!$this->content) return null;

        $parser = $this->getSyntaxParser($this->content);
        $data = $this->getSyntaxData();
        $template = $parser->render($data);
        return $template;
    }

    public function renderForPreview()
    {
        $content = $this->content_html ?: $this->renderTemplate();
        $parser = new TextParser;

        $data = [];
        $data['first_name'] = 'Test';
        $data['last_name'] = 'Person';
        $data['email'] = 'test@email.tld';
        $data['unsubscribe_url'] = 'javascript:;';
        $data['browser_url'] = 'javascript:;';
        $data['tracking_pixel'] = '';
        $data['tracking_url'] = 'javascript:;';
        $data['banner'] = '<tr><td class="banner-container" width="700" bgcolor="#FFFFFF" style="padding: 0 20px;"><table cellspacing="0" cellpadding="0" border="0" align="center" width="100%"><tr><td width="100%" valign="bottom" align="center"><div style="text-align: center; padding: 28px 0 25px 0; line-height: 100%; background-color: #F3F3F3; margin-bottom: 15px; width: 100%; font-family: Helvetica, Arial, sans-serif; font-size: 16px; color: #7a7a7a;">Banner will come here</div></td></tr></table></td><tr>';
        $data['latest_news'] = $this->getLatestNews(0, false);
        $data['breaking_news'] = $this->getBreakingNews();

        $result = $parser->parseString($content, $data);

        // Inject base target
        $result = str_replace(
            '</head>',
            '<base target="_blank" />' . PHP_EOL . '</head>',
            $result
        );

        return $result;
    }

    public function renderForSubscriber($subscriber, $live = 0)
    {
        $parser = new TextParser;
        $data = $this->buildTagData($subscriber, $live);
        $result = $parser->parseString($this->content_html, $data);
        //var_dump($data);
        //var_dump($result);
        /*
         * Inject tracking pixel
         */
        /*$result = str_replace(
            '</body>',
            $this->getTrackingPixelImage($subscriber) . PHP_EOL . '</body>',
            $result
        );*/

        return $result;
    }

    //
    // Scopes
    //

    public function scopeIsArchived($query)
    {
        return $query->where('status_id', '!=', MessageStatus::getArchivedStatus()->id);
    }

}
