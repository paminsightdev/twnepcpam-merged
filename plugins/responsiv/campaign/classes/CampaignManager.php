<?php namespace Responsiv\Campaign\Classes;

use Db;
use Illuminate\Support\Facades\Session;
use Keios\Multisite\Classes\SettingHelper;
use Mail;
use Event;
use Responsiv\Campaign\Models\Message;
use Responsiv\Campaign\Models\Subscriber;
use Responsiv\Campaign\Models\MessageStatus;
use Responsiv\Campaign\Helpers\RecipientGroup;
use Carbon\Carbon;
use ApplicationException;
use Exception;

class CampaignManager
{
    use \October\Rain\Support\Traits\Singleton;

    //
    // State
    //

    /**
     * Sets the status to pending, ready to be picked up by
     * the worker process.
     */
    public function confirmReady($campaign)
    {
        if (!$campaign->is_delayed) {
            $campaign->launch_at = $campaign->freshTimestamp();
        }

        $campaign->status = MessageStatus::getPendingStatus();
        $campaign->save();
    }

    /**
     * Sets the status to active, binds all the subscribers to the message,
     * recompiles the stats and repeats the campaign.
     */
    public function launchCampaign($campaign)
    {
        $campaign->status = MessageStatus::getProcessingStatus();
        $campaign->save();

        $this->bindListSubscribers($campaign);
        $this->bindGroupSubscribers($campaign);

        $campaign->rebuildStats();
        $campaign->status = MessageStatus::getActiveStatus();
        $campaign->save();

        if ($campaign->is_repeating) {
            $this->repeatCampaign($campaign);
        }
    }

    /**
     * When a campaign has no subscribers
     */
    public function recreateCampaign($campaign)
    {
        if ($campaign->count_subscriber > 0) {
            throw new ApplicationException('Sorry, you cannot recreate this campaign because it has subscribers belonging to it.');
        }

        $campaign->status = MessageStatus::getDraftStatus();
        $campaign->save();
    }

    public function archiveCampaign($campaign)
    {
        // Delete all subscriber info in the pivot table
        $campaign->subscribers()->detach();

        $campaign->status = MessageStatus::getArchivedStatus();
        $campaign->save();
    }

    public function cancelCampaign($campaign)
    {
        $campaign->status = MessageStatus::getCancelledStatus();
        $campaign->save();
    }

    public function repeatCampaign($campaign)
    {
        $duplicate = $campaign->duplicateCampaign();
        $duplicate->is_delayed = true;

        $now = $campaign->freshTimestamp();
        switch ($campaign->repeat_frequency) {
            case 'daily': $now = $now->addDay(); break;
            case 'weekly': $now = $now->addWeek(); break;
            case 'monthly': $now = $now->addMonth(); break;
            case 'yearly': $now = $now->addYear(); break;
            default: $now = $now->addYears(5); break;
        }

        $duplicate->launch_at = $now;
        $duplicate->status = MessageStatus::getPendingStatus();
        $duplicate->count_repeat++;
        $duplicate->save();

        return $duplicate;
    }

    //
    // Sending
    //

    public function sendToSubscriber($campaign, $subscriber, $live = 1)
    {
        $html = $campaign->renderForSubscriber($subscriber, $live);

        $text = $this->getTextMessage($campaign->getBrowserUrl($subscriber));

        Session::put('campaign.message.site.id', $campaign->site_id);
        Mail::rawTo($subscriber, [
            'html' => $html,
            'text' => $text
        ], function($message) use ($campaign) {
            $message->subject($campaign->subject);
        });
        Session::forget('campaign.message.site.id');
    }

    public function getTextMessage($browserUrl)
    {
        $lines = [];
        $lines[] = '---------------------------------------------';
        $lines[] = '------- Graphical email content';
        $lines[] = '---------------------------------------------';
        $lines[] = 'This email contains graphical content, you may view it in your browser using the address located below.';
        $lines[] = '<%browserurl%>';
        $lines[] = '---------------------------------------------';
        return implode(PHP_EOL.PHP_EOL, $lines);
    }

    //
    // Helpers
    //

    /**
     * Binds all subscribers from the campaign lists to the message.
     */
    protected function bindListSubscribers($campaign)
    {
        if (!$campaign->subscriber_lists()->count()) {
            return;
        }

        foreach ($campaign->subscriber_lists as $list) {
            $ids = $list->subscribers()->lists('id');

            if ($ids && count($ids) > 0) {
                $campaign->subscribers()->sync($ids, false);
            }
        }
    }

    /**
     * Binds all subscribers from the campaign groups to the message.
     */
    public function bindGroupSubscribers($campaign)
    {
        $groups = $campaign->groups;
        if (!is_array($groups)) {
            return;
        }

        /*
         * Pair them to existing subscribers, or create them
         */
        $ids = $this->getSubscribersFromRecipientTypes($groups);

        /*
         * Sync to the campaign
         */
        if (count($ids) > 0) {
            $campaign->subscribers()->sync($ids, false);
        }
    }

    /**
     * Creates new subscribers from a recipient data groups,
     * returns an array of the new subscriber IDs.
     * @return array
     */
    public function getSubscribersFromRecipientTypes($types)
    {
        $ids = [];
        $data = [];

        if (!is_array($types)) {
            $types = [$types];
        }

        foreach ($types as $type) {
            $data += RecipientGroup::getRecipientsData($type);
        }

        /*
         * Looking for existing subscribers and pruning them from the dataset
         */
        Db::table('responsiv_campaign_subscribers')
            ->select('id', 'email')
            ->orderBy('id')
            ->chunk(250, function($subscribers) use (&$data, &$ids) {
                foreach ($subscribers as $subscriber) {
                    if (!isset($data[$subscriber->email])) {
                        continue;
                    }

                    $ids[] = $subscriber->id;
                    unset($data[$subscriber->email]);
                }
            })
        ;

        /*
         * Creating subscribers in the remaining dataset
         */
        foreach ($data as $email => $info) {
            try {
                $info['email'] = $email;
                $info['confirmed_at'] = Carbon::now();
                $info['created_at'] = Carbon::now();
                $info['updated_at'] = Carbon::now();
                $ids[] = Subscriber::insertGetId($info);
            }
            catch (Exception $ex) {
                traceLog('Unable to add group recipient: '.$email, $ex);
            }
        }

        return $ids;
    }
}
