<?php namespace Responsiv\Campaign\Classes;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Keios\Multisite\Classes\SettingHelper;
use Mail;
use Event;
use Responsiv\Campaign\Models\Message;
use Responsiv\Campaign\Models\Subscriber;
use Responsiv\Campaign\Models\MessageStatus;
use Carbon\Carbon;
use ApplicationException;
use SendGrid;

/**
 * Worker class, engaged by the automated worker
 */
class CampaignWorker
{
    use \October\Rain\Support\Traits\Singleton;

    const BATCH_SIZE = 1000;

    /**
     * @var Responsiv\Campaign\Classes\CampaignManager
     */
    protected $campaignManager;

    /**
     * @var bool There should be only one task performed per execution.
     */
    protected $isReady = true;

    /**
     * @var string Processing message
     */
    protected $logMessage = 'There are no outstanding activities to perform.';

    /**
     * Initialize this singleton.
     */
    protected function init()
    {
        $this->campaignManager = CampaignManager::instance();
    }

    /*
     * Process all tasks
     */
    public function process()
    {
        $this->isReady && $this->processPendingMessages();
        $this->isReady && $this->processActiveMessages();

        // @todo Move this action so the user can do it manually
        // $this->isReady && $this->processUnsubscribedSubscribers();

        return $this->logMessage;
    }

    /**
     * This will launch pending campaigns if there launch date has
     * passed.
     */
    public function processPendingMessages()
    {
        $now = new Carbon;
        $pendingId = MessageStatus::getPendingStatus()->id;

        $campaign = Message::where('status_id', $pendingId)
            ->get()
            ->filter(function($message) use ($now) {
                return $message->launch_at <= $now;
            })
            ->shift()
        ;

        if ($campaign) {
            $this->campaignManager->launchCampaign($campaign);

            $this->logActivity(sprintf(
                'Launched campaign "%s" with %s subscriber(s) queued.',
                $campaign->name,
                $campaign->count_subscriber
            ));
        }
    }

    /**
     * This will send messages subscribers of active campaigns.
     */
    public function processActiveMessages()
    {
        $activeId = MessageStatus::getActiveStatus()->id;

        $campaign = Message::where('status_id', $activeId)
            ->get()
            ->filter(function($message) { return $message->canBeProcessed(); })
            ->shift()
        ;

        if ($campaign) {
            $senderAddress = SettingHelper::getSenderAddress($campaign->site_id);
            $senderName = SettingHelper::getSenderName($campaign->site_id);

            /*
             * Immediately mark as processed to prevent multiple threads
             */
            $campaign->markProcessed();

            $subscribers = $campaign->subscribers()
                ->whereNull('sent_at')
                ->whereNull('processing_time')
                ->limit(self::BATCH_SIZE);
            $ids = $subscribers->pluck('sent_id');

            $subscribers = $campaign->subscribers()
                ->whereNull('sent_at')
                ->whereNull('processing_time')
                ->limit(self::BATCH_SIZE);
            $subscribers = $subscribers->get();

            $now = Carbon::now()->toDateTimeString();

            DB::table('responsiv_campaign_messages_subscribers')->whereIn('sent_id', $ids)->update(['processing_time' => $now]);
            $countSent = 0;
            // create sendgrid instance
            $sendgrid = new SendGrid(getenv('SENDGRID_API_KEY'));
            $mail_settings = new SendGrid\Mail\MailSettings();
            $sandbox_mode = new SendGrid\Mail\SandBoxMode();
            $sandbox_mode->setEnable(true);
            $mail_settings->setSandboxMode($sandbox_mode);

            $email = new SendGrid\Mail\Mail(new SendGrid\Mail\From($senderAddress,$senderName));
//            $email->setMailSettings($mail_settings);
            $html = $campaign->renderForSubscriber(null, 1);
            $text = $this->campaignManager->getTextMessage(null);

            foreach ($subscribers as $subscriber) {
                if (!$subscriber->confirmed_at || $subscriber->unsubscribed_at) {
                    $campaign->subscribers()->remove($subscriber);
                    Log::info('--------------removing user:'.$subscriber->email.'-----------');
                    continue;
                }

                $listCodes = $campaign->list_codes;

		try {
                	$personalization = new SendGrid\Mail\Personalization();
                	$personalization->addTo(new SendGrid\Mail\To($subscriber->email));
                	//$personalization->addSubstitution(new SendGrid\Mail\Substitution('%unsubscribeurl%',$campaign->getBrowserUrl($subscriber).'?lists='.urlencode($listCodes).'&unsubscribe=1'));
                	$personalization->addSubstitution(new SendGrid\Mail\Substitution('%unsubscribeurl%',$campaign->getMyAccountUrl($subscriber)));
                    $personalization->addSubstitution(new SendGrid\Mail\Substitution('%trackingpixel_url%',$campaign->getBrowserUrl($subscriber).'.png'));
                	$personalization->addSubstitution(new SendGrid\Mail\Substitution('%trackingpixel%',$campaign->getTrackingPixelImage($subscriber)));
                	$personalization->addSubstitution(new SendGrid\Mail\Substitution('%browserurl%',$campaign->getBrowserUrl($subscriber)));

                	$email->addPersonalization($personalization);
		} catch (SendGrid\Mail\TypeException $e) {
			Log::error($e->getMessage());
		}
               /* // create email
                try {
                    $email = new SendGrid\Mail\Mail();
                    $email->addTo($subscriber->email);
                    $email->setFrom($senderAddress, $senderName);
                    $email->setSubject($campaign->subject);
                    $email->addContent("text/plain", $text);
                    $email->addContent("text/html", $html);
                    //$email->setMailSettings($mail_settings);
                } catch (SendGrid\Mail\TypeException $e) {
                    Log::error($e->getMessage());
                    $subscriber->pivot->sent_at = $subscriber->freshTimestamp();
                    $subscriber->pivot->save();
                    $campaign->count_sent++;
                    $countSent++;
                    continue;
                }

                try {
                    $sendgrid->send($email);
                } catch (Exception $e) {
                    Log::error($e->getMessage());
                }
                $subscriber->pivot->sent_at = $subscriber->freshTimestamp();
                $subscriber->pivot->save();
                */
                $campaign->count_sent++;
                $countSent++;
            }

            //echo $campaign->count_sent;


            //SENDDATA gatehered all subscriber

            //$email->setFrom($senderAddress, $senderName);
	    try {
                $subject = $campaign->subject;
                $email->setSubject('=?utf-8?Q?'.$subject.'?=');
            	//$email->setSubject(utf8_encode($campaign->subject));
            	$email->addContent("text/plain", $text);
            	$email->addContent("text/html", $html);
            	$result = $sendgrid->send($email);
	    } catch (Exception $e) {
                Log::error($e->getMessage());
            }

            $now = Carbon::now()->toDateTimeString();
            DB::table('responsiv_campaign_messages_subscribers')->whereIn('sent_id', $ids)->update(['sent_at' => $now]);

            if (
                $campaign->count_sent >= $campaign->count_subscriber
            ) {
                $campaign->status = MessageStatus::getSentStatus();
            }

            $campaign->rebuildStats();
            $campaign->save();

            $this->logActivity(sprintf(
                'Sent campaign "%s" to %s subscriber(s).',
                $campaign->name,
                $countSent
            ));
        }
    }

    /**
     * This will find subscribers who are unsubscribed for longer
     * than 14 days and delete their account.
     */
    public function processUnsubscribedSubscribers()
    {
        $endDate = new Carbon;
        $endDate = $endDate->subDays(14);

        $subscriber = Subscriber::whereNotNull('unsubscribed_at')
            ->get()
            ->filter(function($subscriber) use ($endDate) {
                return $subscriber->unsubscribed_at <= $endDate;
            })
            ->shift()
        ;

        if ($subscriber) {
            $subscriber->delete();

            $this->logActivity(sprintf(
                'Deleted subscriber "%s"  who opted out 14 days ago.',
                $subscriber->email
            ));
        }
    }

    /**
     * Called when activity has been performed.
     */
    protected function logActivity($message)
    {
        $this->logMessage = $message;
        $this->isReady = false;
    }
}
