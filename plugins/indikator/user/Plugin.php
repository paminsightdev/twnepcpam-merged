<?php namespace Indikator\User;

use System\Classes\PluginBase;
use RainLab\User\Controllers\Users as UsersController;
use RainLab\User\Models\User as UserModel;
use Illuminate\Support\Facades\URL;
use Lang;

class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    public function pluginDetails()
    {
        return [
            'name'        => 'indikator.user::lang.plugin.name',
            'description' => 'indikator.user::lang.plugin.description',
            'author'      => 'indikator.user::lang.plugin.author',
            'icon'        => 'icon-user-plus',
            'homepage'    => 'https://github.com/gergo85/oc-user-plus'
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Indikator\User\ReportWidgets\Users' => [
                'label'   => 'indikator.user::lang.widget.title',
                'context' => 'dashboard'
            ]
        ];
    }

    public function boot()
    {
        UserModel::extend(function($model) {
            $model->addFillable([
                'gender',
                'phone',
                'position',
                'company',
                'iu_about',
                'iu_webpage',
                'iu_blog',
                'iu_facebook',
                'iu_twitter',
                'iu_skype',
                'iu_icq',
                'comments',
            ]);
        });
      
        UsersController::extendFormFields(function($form, $model, $context) {
            if (!$model instanceof UserModel) {
                return;
            }
            
            if(stripos(URL::current(), 'backend/rainlab/user/users')) {
                return;
            }
            
            $form->addTabFields([
                'gender' => [
                    'label'   => 'indikator.user::lang.personal.gender',
                    'tab'     => 'indikator.user::lang.personal.tab',
                    'type'    => 'dropdown',
                    'options' => [
                        'unknown' => Lang::get('indikator.user::lang.gender.unknown'),
                        'female'  => Lang::get('indikator.user::lang.gender.female'),
                        'male'    => Lang::get('indikator.user::lang.gender.male')
                    ],
                    'span'    => 'auto'
                ],
                'phone' => [
                    'label' => 'indikator.user::lang.personal.telephone',
                    'tab'   => 'indikator.user::lang.personal.tab',
                    'span'  => 'auto'
                ],
                'position' => [
                    'label' => 'indikator.user::lang.personal.job',
                    'tab'   => 'indikator.user::lang.personal.tab',
                    'span'  => 'auto'
                ],
                'company' => [
                    'label' => 'indikator.user::lang.personal.company',
                    'tab'   => 'indikator.user::lang.personal.tab',
                    'span'  => 'auto'
                ],
                'iu_about' => [
                    'label' => 'indikator.user::lang.personal.about',
                    'tab'   => 'indikator.user::lang.personal.tab',
                    'type'  => 'textarea',
                    'size'  => 'small',
                    'span'  => 'full'
                ],
                'iu_webpage' => [
                    'label' => 'indikator.user::lang.internet.webpage',
                    'tab'   => 'indikator.user::lang.internet.tab',
                    'span'  => 'auto'
                ],
                'iu_blog' => [
                    'label' => 'indikator.user::lang.internet.blog',
                    'tab'   => 'indikator.user::lang.internet.tab',
                    'span'  => 'auto'
                ],
                'iu_facebook' => [
                    'label' => 'indikator.user::lang.internet.facebook',
                    'tab'   => 'indikator.user::lang.internet.tab',
                    'span'  => 'auto'
                ],
                'iu_twitter' => [
                    'label' => 'indikator.user::lang.internet.twitter',
                    'tab'   => 'indikator.user::lang.internet.tab',
                    'span'  => 'auto'
                ],
                'iu_skype' => [
                    'label' => 'indikator.user::lang.internet.skype',
                    'tab'   => 'indikator.user::lang.internet.tab',
                    'span'  => 'auto'
                ],
                'iu_icq' => [
                    'label' => 'indikator.user::lang.internet.icq',
                    'tab'   => 'indikator.user::lang.internet.tab',
                    'span'  => 'auto'
                ]
            ]);

            $form->addSecondaryTabFields([
                'comments' => [
                    'label' => 'indikator.user::lang.comment',
                    'type'  => 'textarea',
                    'size'  => 'small'
                ]
            ]);
        });
    }
}
