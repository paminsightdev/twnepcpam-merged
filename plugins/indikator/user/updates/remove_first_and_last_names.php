<?php namespace Indikator\User\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;
use RainLab\User\Models\User;

class RemoveFirstAndLastNames extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(User::all()->count() == 0) {
        
                Schema::table('users', function($table)
                {
                    $table->dropColumn('iu_first_name');
                    $table->dropColumn('iu_last_name');
                });
            }
        }
    }

    public function down()
    {
        Schema::table('users', function($table)
        {
            $table->string('iu_first_name', 100)->nullable();
            $table->string('iu_last_name', 100)->nullable();
        });
    }
}
