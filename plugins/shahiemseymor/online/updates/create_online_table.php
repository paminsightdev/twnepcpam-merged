<?php namespace ShahiemSeymor\Online\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateOnlineTable extends Migration
{

    public function up()
    {
        Schema::create('shahiemseymor_online', function($table)
        {
            $table->increments('id')->unsigned();
            $table->string('ip');
            $table->integer('user_id')->nullable();
            $table->string('last_page');
            $table->string('session_id');
            $table->integer('last_activity');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('shahiemseymor_online');
    }

}
