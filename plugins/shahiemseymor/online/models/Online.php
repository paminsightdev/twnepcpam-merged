<?php namespace ShahiemSeymor\Online\Models;

use Auth;
use DB;
use Model;
use Session;
use Request;

class Online extends Model
{

    public $table = 'shahiemseymor_online';

    public static function isOnline($user)
    {
        $onlineQuery = DB::select(DB::raw('SELECT id
                                           FROM shahiemseymor_online 
                                           WHERE DATE_ADD(FROM_UNIXTIME(last_activity), INTERVAL 60 SECOND) > NOW() 
                                           AND user_id = '.$user));
        return count($onlineQuery);
    }

    public static function getOnlineUsers($currentPage = null)
    {
   
        if ($currentPage)
        {
            $onlineQuery = DB::select(DB::raw('SELECT * 
                                               FROM shahiemseymor_online 
                                               WHERE DATE_ADD(FROM_UNIXTIME(last_activity), INTERVAL 60 SECOND) > NOW() 
                                               AND user_id >= 1
                                               AND last_page = :path'), array(
                                                   'path' => Request::path()
                                              ));
        }
        else
        {
            $onlineQuery = DB::select(DB::raw('SELECT * 
                                               FROM shahiemseymor_online 
                                               WHERE DATE_ADD(FROM_UNIXTIME(last_activity), INTERVAL 60 SECOND) > NOW() 
                                               AND user_id >= 1'));
        }

        return $onlineQuery;
    }  

    public function scopeUpdateCurrent()
    {
       
        $onlineRecord = $this->where('session_id', Session::getId());

        if(Auth::check())
        {
            $onlineRecords = $this->where('user_id', Auth::getUser()->id); // Check if the user already exists

            if($onlineRecords->count() >= 1)
            {
                $onlineRecords->delete();
            }
        }

        if($onlineRecord->count() == 1)
            $onlineRecord->delete();

        $date = date_create();
        $this->ip            = Request::getClientIp();
        $this->user_id       = Auth::check() ? Auth::getUser()->id : null;
        $this->last_page     = Request::path();
        $this->session_id    = Session::getId();
        $this->last_activity = date_timestamp_get($date);

        $this->save();
    }

}