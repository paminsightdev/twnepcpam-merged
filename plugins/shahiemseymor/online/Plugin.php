<?php namespace ShahiemSeymor\Online;
use DB;
use System\Classes\PluginBase;
use ShahiemSeymor\Online\Models\Online as OnlineModel;

class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Online',
            'description' => 'Enable you to display how many users and guests are online on your website.',
            'author'      => 'ShahiemSeymor',
            'icon'        => 'icon-circle'
        ];
    }

    public function registerComponents()
    {
        return [
            'ShahiemSeymor\Online\Components\Online'  => 'onlineList',
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'functions'   => [
                'isOnline'   => function($user) { return OnlineModel::isOnline($user); }
            ]
        ];
    }

    public function registerReportWidgets()
{
    return [
        'ShahiemSeymor\Online\ReportWidgets\Onlineusers' => [
            'label'   => 'Online User listing',
            'context' => 'dashboard'
        ],
    ];
}

}
