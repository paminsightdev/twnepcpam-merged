<?php namespace ShahiemSeymor\Online\Components;

use DB;
use Cms\Classes\ComponentBase;
use RainLab\Forum\Models\Member;
use Request;
use ShahiemSeymor\Online\Models\Online as OnlineModel;

class Online extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Online list',
            'description' => 'Display the users and guests on your website or the current page.',
            'icon'        => 'icon-circle'
        ];
    }

    public function defineProperties()
    {
         return [
            'current_page' => [
                 'title'             => 'Users on current page',
                 'description'       => 'Select users by current page',
                 'type'              => 'checkbox'
            ]
        ];
    }

    public function onRun()
    {
        $this->page['userInfo']    = new Member;
        $this->page['onlineUsers'] = $this->getOnlineList();
        $this->page['currentPage'] = $this->property('current_page');
    }

    public function getOnlineList()
    {
        $current_page = $this->property('current_page');

        $onlineList = OnlineModel::getOnlineUsers($this->property('current_page'));

        $this->page['guestsOnline']            = DB::select(DB::raw('SELECT * 
                                                                     FROM shahiemseymor_online 
                                                                     WHERE DATE_ADD(FROM_UNIXTIME(last_activity), INTERVAL 300 SECOND) > NOW()
                                                                     AND user_id is null'));

        $this->page['usersOnline']             = DB::select(DB::raw('SELECT * 
                                                                     FROM shahiemseymor_online 
                                                                     WHERE DATE_ADD(FROM_UNIXTIME(last_activity), INTERVAL 300 SECOND) > NOW()
                                                                     AND user_id >= 1'));

        $this->page['usersOnlineCurrentPage']  = DB::select(DB::raw('SELECT * 
                                                                     FROM shahiemseymor_online 
                                                                     WHERE DATE_ADD(FROM_UNIXTIME(last_activity), INTERVAL 300 SECOND) > NOW() 
                                                                     AND user_id >= 1
                                                                     AND last_page = :path'), array(
                                                                           'path' => Request::path()
                                                                      ));

        return $onlineList;
    }

}