<?php
namespace Shahiemseymor\Online\ReportWidgets;

use Backend\Classes\ReportWidgetBase;

use ShahiemSeymor\Online\Models\Online as OnlineModel;
use Rainlab\User\Models\User;



class Onlineusers extends ReportWidgetBase
{
    public $users;

    public function render()
    {
        $this->vars['users'] = $users = new OnlineModel;

        $this->vars['onlineusers'] = $onlineusers = $users->getOnlineUsers();

        $userids = Array();
        foreach($onlineusers as $o) {
            array_push($userids,$o->user_id);
        }

        $this->vars['online'] = $online = User::whereIn('id',$userids)->get();


        return $this->makePartial('widget');
    }
}