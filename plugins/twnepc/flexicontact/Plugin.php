<?php

namespace Twnepc\Flexicontact;

use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Backend\Facades\Backend;

class Plugin extends PluginBase{

    /**
     * Returns information about this plugin, including plugin name and developer name.
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Pam Contact',
            'description' => 'This is flexicontact plugin updated version',
            'author'      => 'Pam Online',
            'icon'        => 'icon-hand-peace-o'
        ];
    }

    public function registerComponents()
    {
        return [
            'Twnepc\Flexicontact\Components\ContactForm' => 'contactForm',
        ];
    }

    public function registerPermissions()
    {
        return [
            'pamonline.flexicontact.access_settings' => [
                'tab'   => 'laminsanneh.flexicontact::lang.permissions.tab',
                'label' => 'laminsanneh.flexicontact::lang.permissions.settings'
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'twnepc.flexicontact::lang.strings.settings_label',
                'description' => 'twnepc.flexicontact::lang.strings.settings_desc',
                'category'    => 'Pamonline',
                'icon'        => 'icon-hand-peace-o',
                'class'       => 'Twnepc\Flexicontact\Models\Settings',
                'permissions' => ['pamonline.flexicontact.access_settings'],
                'order'       => 100
            ]
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'twnepc.flexicontact::emails.message' => 'twnepc.flexicontact::lang.strings.email_desc',
        ];
    }
}
