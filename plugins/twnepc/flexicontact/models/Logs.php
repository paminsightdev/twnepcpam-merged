<?php
namespace Twnepc\Flexicontact\Models;

use Model;

class Logs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pamonline_flexicontact_logs';
    protected $jsonable = ['data'];
}