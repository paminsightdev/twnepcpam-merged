<?php

namespace Twnepc\FormBuilder;

use System\Classes\PluginBase;
use Backend;
use App;
use Event;

/**
 * Class Plugin
 * @package Renatio\FormBuilder
 */
class Plugin extends PluginBase
{

    public function pluginDetails()
    {
        return [
            'name'        => 'Pam form builder',
            'description' => 'Pamonline formbuilder',
            'author'      => 'Pamonline',
            'icon'        => 'icon-hand-peace-o'
        ];
    }

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Twnepc\FormBuilder\Components\RenderForm' => 'renderForm',
        ];
    }

    public function registerSettings()
    {
    }

}
