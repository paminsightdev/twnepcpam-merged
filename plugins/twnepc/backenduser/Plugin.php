<?php

namespace Twnepc\Backenduser;


use Backend\Models\User as BackendUserModel;
use Backend\Controllers\Users as BackendUsersController;


class Plugin extends \System\Classes\PluginBase
{

    public function boot()
    {

        BackendUserModel::extend(function($model){
            $model->belongsToMany['site'] = ['Keios\Multisite\Models\Setting'];
            $model->addDynamicMethod('getSiteOptions', function(){
                return \Keios\Multisite\Models\Setting::all()->pluck('theme','id');
            });
        });

        BackendUsersController::extendFormFields(function($form, $model, $context){

            if (!$model instanceof BackendUserModel)
                return;
           
            $form->addTabFields([
                'site' => [
                    'label' => 'Site',
                    'type'  => 'relation',
                    'span' => 'left',
                    'nameFrom' => 'theme'
                ],
            ]);
        });
        
    }
}