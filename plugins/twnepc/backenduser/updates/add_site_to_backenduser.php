<?php  namespace Twnepc\Backenduser\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddSiteToBackenduser extends Migration
{
    public function up()
    {

        Schema::table('backend_users', function($table)
        {
            $table->integer('site_id')->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('backend_users')) {
            Schema::table('backend_users', function ($table) {
                $table->dropColumn(['site_id']);
            });
        }
    }
}