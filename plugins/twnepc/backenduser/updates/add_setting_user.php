<?php namespace Twnepc\Backenduser\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddSettingUser extends Migration
{
    public function up()
    {
        Schema::create('setting_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('setting_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('setting_user');
    }
}
