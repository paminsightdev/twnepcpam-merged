<?php

namespace Twnepc\Menu;

use Event;
use Backend;
use Rainlab\Location\Models\Country;
use Rainlab\User\Models\User as UserModel;
use Twnepc\User\Components\Newslettersignup;
use Twnepc\User\Components\Preferences;
use Twnepc\User\Components\Subscriptioncheck;
use Rainlab\User\Controllers\Users as UsersController;

class Plugin extends \System\Classes\PluginBase
{
    public function boot() {
        Event::listen('backend.menu.extendItems', function ($manager) { 

            //$manager->removeMainMenuItem('Twnepc.Membership','memberships');
            $manager->addMainMenuItems('Twnepc.Company',[
                'crm' => [
                    'label' => 'CRM',
                    'icon' => 'icon-cubes',
                    'order' => 10,
                    'url' => Backend::url('twnepc/company/companies'),
                    'permissions' => ['view_articles', 'view_review']
                ]
            ]);

            $manager->addMainMenuItems('Twnepc.News',[
                'article_management' => [
                    'label' => 'Aritcle Management',
                    'order' => 20,
                    'url' => Backend::url('twnepc/news/gifts'),
                    'icon' => 'icon-newspaper-o',
                    'permissions' => ['view_articles', 'view_review']
                ]
            ]);

            $manager->addSideMenuItems('Twnepc.News', 'gifts-main', [
                'gifts' => [
                    'label' => 'Gifts',
                    'icon' => 'icon-gift',
                    'code' => 'gifts',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/gifts'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'ratings' => [
                    'label' => 'Ratings',
                    'icon' => 'icon-life-ring',
                    'code' => 'ratings',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/ratings'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'authors' => [
                    'label' => 'Authors',
                    'icon' => 'icon-life-ring',
                    'code' => 'authors',
                    'owner' => 'Twnepc.Author',
                    'url' => Backend::url('twnepc/author/authors'),
                    'permissions' => ['view_articles', 'view_review']
                ]
                ,
                'funds' => [
                    'label' => 'Funds',
                    'icon' => 'icon-life-ring',
                    'code' => 'funds',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/fund/funds'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'bannerspace' => [
                    'label' => 'Banner Spaces',
                    'icon' => 'icon-life-ring',
                    'code' => 'banner-spaces',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/banners/banners'),
                    'permissions' => ['view_articles', 'view_review']
                ]
            ]);

            $manager->addSideMenuItems('Twnepc.News', 'rating-menu-item', [
                'gifts' => [
                    'label' => 'Gifts',
                    'icon' => 'icon-gift',
                    'code' => 'gifts',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/gifts'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'ratings' => [
                    'label' => 'Ratings',
                    'icon' => 'icon-life-ring',
                    'code' => 'ratings',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/ratings'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'authors' => [
                    'label' => 'Authors',
                    'icon' => 'icon-life-ring',
                    'code' => 'authors',
                    'owner' => 'Twnepc.Author',
                    'url' => Backend::url('twnepc/author/authors'),
                    'permissions' => ['view_articles', 'view_review']
                ]
                ,
                'funds' => [
                    'label' => 'Funds',
                    'icon' => 'icon-life-ring',
                    'code' => 'funds',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/fund/funds'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'bannerspace' => [
                    'label' => 'Banner Spaces',
                    'icon' => 'icon-life-ring',
                    'code' => 'banner-spaces',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/banners/banners'),
                    'permissions' => ['view_articles', 'view_review']
                ]
            ]);

            $manager->addSideMenuItems('Twnepc.Author', 'author-menu-item', [
                'gifts' => [
                    'label' => 'Gifts',
                    'icon' => 'icon-gift',
                    'code' => 'gifts',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/gifts'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'ratings' => [
                    'label' => 'Ratings',
                    'icon' => 'icon-life-ring',
                    'code' => 'ratings',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/ratings'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'authors' => [
                    'label' => 'Authors',
                    'icon' => 'icon-life-ring',
                    'code' => 'authors',
                    'owner' => 'Twnepc.Author',
                    'url' => Backend::url('twnepc/author/authors'),
                    'permissions' => ['view_articles', 'view_review']
                ]
                ,
                'funds' => [
                    'label' => 'Funds',
                    'icon' => 'icon-life-ring',
                    'code' => 'funds',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/fund/funds'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'bannerspace' => [
                    'label' => 'Banner Spaces',
                    'icon' => 'icon-life-ring',
                    'code' => 'banner-spaces',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/banners/banners'),
                    'permissions' => ['view_articles', 'view_review']
                ]
            ]);

            $manager->addSideMenuItems('Twnepc.Fund', 'main-menu-fund', [
                'gifts' => [
                    'label' => 'Gifts',
                    'icon' => 'icon-gift',
                    'code' => 'gifts',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/gifts'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'ratings' => [
                    'label' => 'Ratings',
                    'icon' => 'icon-life-ring',
                    'code' => 'ratings',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/ratings'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'authors' => [
                    'label' => 'Authors',
                    'icon' => 'icon-life-ring',
                    'code' => 'authors',
                    'owner' => 'Twnepc.Author',
                    'url' => Backend::url('twnepc/author/authors'),
                    'permissions' => ['view_articles', 'view_review']
                ]
                ,
                'funds' => [
                    'label' => 'Funds',
                    'icon' => 'icon-life-ring',
                    'code' => 'funds',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/fund/funds'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'bannerspace' => [
                    'label' => 'Banner Spaces',
                    'icon' => 'icon-life-ring',
                    'code' => 'banner-spaces',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/banners/banners'),
                    'permissions' => ['view_articles', 'view_review']
                ]
            ]);

            $manager->addSideMenuItems('Twnepc.Banners', 'banner-menu-item', [
                'gifts' => [
                    'label' => 'Gifts',
                    'icon' => 'icon-gift',
                    'code' => 'gifts',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/gifts'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'ratings' => [
                    'label' => 'Ratings',
                    'icon' => 'icon-life-ring',
                    'code' => 'ratings',
                    'owner' => 'Twnepc.News',
                    'url' => Backend::url('twnepc/news/ratings'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'authors' => [
                    'label' => 'Authors',
                    'icon' => 'icon-life-ring',
                    'code' => 'authors',
                    'owner' => 'Twnepc.Author',
                    'url' => Backend::url('twnepc/author/authors'),
                    'permissions' => ['view_articles', 'view_review']
                ]
                ,
                'funds' => [
                    'label' => 'Funds',
                    'icon' => 'icon-life-ring',
                    'code' => 'funds',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/fund/funds'),
                    'permissions' => ['view_articles', 'view_review']
                ],
                'bannerspace' => [
                    'label' => 'Banner Spaces',
                    'icon' => 'icon-life-ring',
                    'code' => 'banner-spaces',
                    'owner' => 'Twnepc.Fund',
                    'url' => Backend::url('twnepc/banners/banners'),
                    'permissions' => ['view_articles', 'view_review']
                ]
            ]);
        });
    }
}