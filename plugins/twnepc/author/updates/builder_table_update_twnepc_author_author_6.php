<?php namespace Twnepc\Author\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcAuthorAuthor6 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->integer('site_id')->nullable()->default(-1);
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->dropColumn('site_id');
        });
    }
}
