<?php namespace Twnepc\Author\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcAuthorAuthor4 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->integer('bakend_user_id')->nullable()->default(-1);
            $table->integer('contributor')->default(-1)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->dropColumn('bakend_user_id');
            $table->integer('contributor')->default(NULL)->change();
        });
    }
}
