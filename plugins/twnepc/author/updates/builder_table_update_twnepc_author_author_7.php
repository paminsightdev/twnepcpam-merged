<?php namespace Twnepc\Author\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcAuthorAuthor7 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->renameColumn('bakend_user_id', 'backend_user_id');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->renameColumn('backend_user_id', 'bakend_user_id');
        });
    }
}
