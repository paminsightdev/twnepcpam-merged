<?php namespace Twnepc\Author\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcAuthorAuthor extends Migration
{
    public function up()
    {
        Schema::create('twnepc_author_author', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('author_name')->nullable();
            $table->boolean('status');
            $table->text('biography');
            $table->integer('contributor')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_author_author');
    }
}
