<?php namespace Twnepc\Author\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcAuthorAuthor5 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->integer('oldtwn_id')->nullable()->default(-1);
            $table->integer('oldepc_id')->nullable()->default(-1);
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->dropColumn('oldtwn_id');
            $table->dropColumn('oldepc_id');
        });
    }
}
