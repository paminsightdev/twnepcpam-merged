<?php namespace Twnepc\Author\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcAuthorAuthor2 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->string('author_name')->default(null)->change();
            $table->integer('contributor')->default(0)->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->string('author_name', 191)->default('NULL')->change();
            $table->integer('contributor')->default(NULL)->change();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
        });
    }
}
