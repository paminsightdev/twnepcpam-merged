<?php namespace Twnepc\Author;

use System\Classes\PluginBase;
use Twnepc\Author\Models\Author;
use ApplicationException;
use Lang;
class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function boot(){
        
        Author::extend(function($model){
            
            $model->bindEvent('model.beforeDelete', function () use ($model) {
                
                if (!$model->isAllowedToBeDeleted()) {
                    
                    throw new ApplicationException(Lang::get('twnepc.author::lang.cannot_delete'));
                }
            });
        });
    }
}
