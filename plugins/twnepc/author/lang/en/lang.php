<?php return [
    'plugin' => [
        'name' => 'Author',
        'description' => '',
        'status' => 'Active',
    ],
    'author_name' => 'Name',
    'status' => 'Active',
    'biography' => 'Biography',
    'cannot_delete' => 'You cannot delete, because the author has articles.'
];