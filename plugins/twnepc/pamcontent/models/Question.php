<?php
namespace Twnepc\PamContent\Models;

use Model;

/**
 * Model
 */
class Question extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];


    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamonline_pamcontent_questions';

    protected $fillable = [
        'question', 'field_name', 'quest_type'
    ];

    public $hasMany = [
        'options' => ['Twnepc\PamContent\Models\QuestionOption', 'table' => 'pamonline_pamcontent_question_options', 'key'=>'id_quest']
    ];


    public function afterSave()
    {
        $getParams = get();
        if (!isset($getParams['id'])) {
            $id = $this->attributes['id'];
            $OptionsModel = new QuestionOption;
            $OptionsModel->where(['id_quest' => 0])->update(['id_quest' => $id]);
        }
    }

    public function getQuestTypeOptions()
    {
        $options = ["slider" => "Slider", "options" => "Option box", "switches" => "Switch", "list" => "List", "multiple_list" => "Multiple select", "half_size_list" => "Half size list", "percentage" => "Percentage slider","company_lookup"=>'Company lookup'];
        return $options;
    }

    public function getBackgroundOptions()
    {
        $options = ["" => "none", "half blue-bar" => "half blue-bar", "full blue-bar" => "full blue-bar", "gray" => "gray", "white" => "white"];
        return $options;
    }

}