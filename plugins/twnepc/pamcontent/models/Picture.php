<?php namespace Twnepc\PamContent\Models;

use Model;

/**
 * Model
 */
class Picture extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];
    
    public $belongsTo = [
    	'gallery' => ['Twnepc\PamContent\Models\Gallery'],
    ];    

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamonline_pamcontent_pictures';
    
    protected $fillable = [
        'gallery_id','picture_url','descr'
    ];    
    
}