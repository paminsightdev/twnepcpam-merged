<?php namespace Twnepc\PamContent\Models;

use Model;

/**
 * Model
 */
class FaqPosts extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $belongsTo  = [
        'section' => [
            'Twnepc\Pamcontent\Models\FaqSection',
            'key' => 'section_id',
            'otherKey' => 'id'
        ]
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamonline_pamcontent_faq_posts';
}