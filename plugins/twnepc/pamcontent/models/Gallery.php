<?php

namespace Twnepc\PamContent\Models;

use Model;

/**
 * Model
 */
class Gallery extends Model {

    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */

    public $rules = [
    ];
    

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;
    public $hasMany = [
        'pictures' => ['Twnepc\PamContent\Models\Picture'],
    ]; 
    
    
    public static function boot() {
        //Call default functionality (required)
        parent::boot();
    }

    public function getOptionOrderOptions() {
        
        $options = ["1" => "1"];
        
        if (!empty($this->group)) {
            $record = $this->where(["group" => $this->group])->max("option_order");
            $allGroupGalleries = $this->where(["group" => $this->group])->get();
            $allExistingOrders = [];
            foreach ($allGroupGalleries as $gal) {
                $allExistingOrders[] = $gal->attributes["option_order"];
            }
            
            if ($record) {
                $maxOrder = intval($record);
                $options = [];
                if ($this->option_order > 0) {
                    $options = ["{$this->option_order}" => $this->option_order];
                }                
                for ($i=1; $i<=$maxOrder+1; $i++) {
                    if (!in_array($i, $allExistingOrders)) {
                        $options["{$i}"] = $i; 
                    }
                }
            }
        }
        
        return $options;
    }
    
    
    public function afterSave()
    {
        $getParams = get();
        if (!isset($getParams['gallery_id'])) {
            $galleryId = $this->attributes['id'];
            $PictureModel = new Picture;
            $PictureModel->where(['gallery_id'=>0])->update(['gallery_id' => $galleryId]);
        }
    }
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamonline_pamcontent_galleries';

}
