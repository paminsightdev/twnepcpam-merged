<?php namespace Twnepc\PamContent;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Twnepc\Pamcontent\Components\Team' => 'team',
            'Twnepc\Pamcontent\Components\Judges' => 'judges',
            'Twnepc\Pamcontent\Components\Gallery' => 'gallery',
            'Twnepc\Pamcontent\Components\Faq' => 'faq',
            'Twnepc\Pamcontent\Components\Question' => 'question',
        ];
    }  

    public function registerSettings()
    {
    }
    
    public function registerFormWidgets()
{
        return [
            'Twnepc\Pamcontent\FormWidgets\QuestOptions' => ['label' => 'Answers', 'code' => 'options']
        ];
}
}
