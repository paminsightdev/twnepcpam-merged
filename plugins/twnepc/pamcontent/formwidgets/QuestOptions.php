<?php

namespace Twnepc\PamContent\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamContent\Models\Question as Question;
use Twnepc\PamContent\Models\QuestionOption as Options;
use Exception;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class QuestOptions extends FormWidgetBase {

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';
    
    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $columnName = $this->valueFrom;
        //$this->vars['itemTypes'] = MenuManager::instance()->listItemTypes();
        $this->vars['name'] = $this->formField->getName();
        //$this->vars['value'] = $this->model->$columnName()->withDeferred($sessionKey)->getNested();
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        $id = get('id');
        $this->vars['records'] = Options::where('id_quest', $id)->orderBy('id', 'asc')->get();
        $this->vars['id_quest'] = $id;
        return $this->makePartial('options');
         /* 
         */
    }

    public function onLoadCreatePicture() {
        try {
            /*
             * Create a form widget to render the form
             */
            $config = $this->makeConfig('$/twnepc/pamcontent/models/questoption/fields.yaml');
            $config->model = new Question;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            /*
              $form->bindEvent('form.extendFields', function() use ($type, $form){
              $itemTypeObj = new $type;
              $itemTypeObj->extendItemForm($form);
              });
             */
            $this->vars['form'] = $form;
            $this->vars['id_quest'] = intval(post('id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }
    
    public function onLoadUpdateOption2() {
        try {
            /*
             * Create a form widget to render the form
             */
            $id = post('id');
            $config = $this->makeConfig('$/twnepc/pamcontent/models/questoption/fields.yaml');
            $config->model = new Question;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            /*
              $form->bindEvent('form.extendFields', function() use ($type, $form){
              $itemTypeObj = new $type;
              $itemTypeObj->extendItemForm($form);
              });
             */
            $optionModel = new Options;
            $record = $optionModel->where(["id"=>$id])->first();
            if ($record) {
                $this->vars['records'] = $record->attributes;
            } else {
                $this->vars['item'] = '';
            }
            
            $this->vars['form'] = $form;
            $this->vars['id_quest'] = intval(post('id_quest'));
            $this->vars['id'] = intval(post('id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }    
    
    
	public function onLoadUpdateOption() {
        try {
            $id = post('id', 0);
            if (!$item = Options::find($id))
                throw new Exception('Record not found.');

            $this->vars['id'] = $id;
            $this->vars['type'] = $type = $item->master_object_class;
            //$itemTypes = MenuManager::instance()->listItemTypes();
            //$this->vars['typeInfo'] = $itemTypes[$type];

            /*
             * Create a form widget to render the form
             */
            $config = $this->makeConfig('$/twnepc/pamcontent/models/questoption/fields.yaml');
            $config->model = new Question;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['record'] = $item->attributes;
            $this->vars['form'] = $form;
            $this->vars['id_quest'] = intval(post('id_quest'));
            $this->vars['id'] = intval(post('id'));            
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdateOption() { 
       try { 
        $post = post();
        if (!isset($post['id'])) {
               throw new Exception('ID must be specified.');
        }
        $id = intval($post['id']);
        
        $questModel = Question::where('id',$post['id_quest'])->first();
        if ($questModel and in_array($questModel->attributes['quest_type'],['switches','list','half_size_list'])) {
            $post['text'] = isset($post['text'])?strip_tags($post['text']):'';
        } else
        if (isset($post['text'])) {
            $post['text'] = preg_replace('(<p.*?>)is', '<br>', $post['text']);
            $post['text'] = preg_replace('(<\/p.*?>)is', '', $post['text']);
            $mas = explode('<br>',$post['text']);
            if (count($mas) > 1 and trim($mas[0]) == '') {
                array_shift($mas);
            }
            $post['text'] = join('<br>',$mas);
        }        
        
        if (!$model = Options::find($id))
                throw new Exception('Record not found.');
        $model->fill($post);
        $model->save();
        
        $this->prepareVars();
        $this->vars['id_quest'] = post('id_quest');
        $this->vars['records'] = Options::where('id_quest', post('id_quest'))->orderBy('id', 'asc')->get();
        return [
            '#reorderRecords' => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];        
        
       } catch (Exception $ex) {
           $this->vars['fatalError'] = $ex->getMessage();
       }
    }
    
    public function onCreatePicture() {
        $post = post();
        $questModel = Question::where('id',$post['id_quest'])->first();
        if ($questModel and in_array($questModel->attributes['quest_type'],['switches','list','half_size_list'])) {
            $post['text'] = isset($post['text'])?strip_tags($post['text']):'';
        } else
        if (isset($post['text'])) {
            $post['text'] = preg_replace('(<p.*?>)is', '<br>', $post['text']);
            $post['text'] = preg_replace('(<\/p.*?>)is', '', $post['text']);
            $mas = explode('<br>',$post['text']);
            if (count($mas) > 1 and trim($mas[0]) == '') {
                array_shift($mas);
            }
            $post['text'] = join('<br>',$mas);
        } 
        $item = new Options;
        $item->fill($post);
        $item->save();


        // \Log::info(print_r($_POST, true));
        $this->prepareVars();
        $this->vars['id_quest'] = post('id_quest');
        $this->vars['records'] = Options::where('id_quest', post('id_quest'))->orderBy('id', 'asc')->get();
        return [
            '#reorderRecords' => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemovePictures() {
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Options::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['id_quest'] = post('id');
        $this->vars['records'] = Options::where('id_quest', post('id'))->orderBy('id', 'asc')->get();
        return [
            '#reorderRecords' => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

}
