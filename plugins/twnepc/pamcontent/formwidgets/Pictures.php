<?php

namespace Twnepc\PamContent\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamContent\Models\Gallery;
use Twnepc\PamContent\Models\Picture;
use Exception;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Pictures extends FormWidgetBase {

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'pictures';

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        $id = get('gallery_id');
        $this->vars['records'] = Picture::where('gallery_id', $id)->orderBy('id', 'desc')->get();
        $this->vars['gallery_id'] = $id;
        return $this->makePartial('pictures');
    }

    public function onLoadCreatePicture() {
        try {
            /*
             * Create a form widget to render the form
             */
            $config = $this->makeConfig('$/twnepc/pamcontent/models/picture/fields.yaml');
            $config->model = new Gallery;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            /*
              $form->bindEvent('form.extendFields', function() use ($type, $form){
              $itemTypeObj = new $type;
              $itemTypeObj->extendItemForm($form);
              });
             */
            $this->vars['form'] = $form;
            $this->vars['gallery_id'] = intval(post('gallery_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

    public function onCreatePicture() {
        $item = new Picture;
        $item->fill(post());

        /*
          if ( class_exists($item->master_object_class) )
          {
          $itemTypeObj = new $item->master_object_class;
          $itemTypeObj->extendItemModel($item);
          if ( $item->validate() )
          $item->url = $itemTypeObj->getUrl($item);
          }
         * 
         */

        $item->save();


        // \Log::info(print_r($_POST, true));
        $this->prepareVars();
        $this->vars['records'] = Picture::where('gallery_id', post('gallery_id'))->orderBy('id', 'desc')->get();
        return [
            '#reorderRecords' => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemovePictures() {
        $ids = post('ids');
        $galleryId = post('gallery_id');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Picture::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['records'] = Picture::where('gallery_id', post('gallery_id'))->orderBy('id', 'desc')->get();
        return [
            '#reorderRecords' => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

}
