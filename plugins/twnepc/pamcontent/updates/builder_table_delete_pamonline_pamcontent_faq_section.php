<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\PamContent\Models\FaqSection;

class BuilderTableDeletePamonlinePamcontentFaqSection extends Migration
{
    public function up()
    {
        // when there is no data put dummy one in
        if(FaqSection::all()->count() == 0) {
            Schema::dropIfExists('pamonline_pamcontent_faq_section');
        }
    }
    
    public function down()
    {
        Schema::create('pamonline_pamcontent_faq_section', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->string('title', 255);
        });
    }
}
