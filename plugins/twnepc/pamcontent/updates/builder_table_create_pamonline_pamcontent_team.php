<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcontentTeam extends Migration
{
    public function up()
    {
        // check table exists
        if(!Schema::hasTable('pamonline_pamcontent_team')) {
            Schema::create('pamonline_pamcontent_team', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('name');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcontent_team');
    }
}
