<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\PamContent\Models\Team;

class BuilderTableUpdatePamonlinePamcontentTeam extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('pamonline_pamcontent_team')) {
            // when there is no data put dummy one in
            if(Team::all()->count() == 0) {
            
                Schema::table('pamonline_pamcontent_team', function($table)
                {
                    $table->string('role', 255);
                    $table->string('number', 255);
                    $table->string('email', 255);
                    $table->text('about');
                    $table->string('name', 255)->nullable(false)->unsigned(false)->default(null)->change();
                });
            }
        }
    }
    
    public function down()
    {
        Schema::table('pamonline_pamcontent_team', function($table)
        {
            $table->dropColumn('role');
            $table->dropColumn('number');
            $table->dropColumn('email');
            $table->dropColumn('about');
            $table->text('name')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
