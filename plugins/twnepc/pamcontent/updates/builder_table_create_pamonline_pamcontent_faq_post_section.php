<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcontentFaqPostSection extends Migration
{
    public function up()
    {
        // check table exists
        if(!Schema::hasTable('pamonline_pamcontent_faq_post_section')) {
            
            Schema::create('pamonline_pamcontent_faq_post_section', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('post_id');
                $table->integer('section_id');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcontent_faq_post_section');
    }
}
