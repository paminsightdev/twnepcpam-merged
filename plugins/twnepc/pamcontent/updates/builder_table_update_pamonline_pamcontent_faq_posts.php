<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\PamContent\Models\FaqPosts;

class BuilderTableUpdatePamonlinePamcontentFaqPosts extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('pamonline_pamcontent_faq_posts')) {
            // when there is no data put dummy one in
            if(FaqPosts::all()->count() == 0) {
                
                Schema::table('pamonline_pamcontent_faq_posts', function($table)
                {
                    $table->integer('section');
                    $table->increments('id')->unsigned(false)->change();
                });
            }
        }
    }
    
    public function down()
    {
        Schema::table('pamonline_pamcontent_faq_posts', function($table)
        {
            $table->dropColumn('section');
            $table->increments('id')->unsigned()->change();
        });
    }
}
