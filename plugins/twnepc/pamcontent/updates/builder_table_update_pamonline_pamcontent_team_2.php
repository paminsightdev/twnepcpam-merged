<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\PamContent\Models\Team;

class BuilderTableUpdatePamonlinePamcontentTeam2 extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('pamonline_pamcontent_team')) {
            // when there is no data put dummy one in
            if(Team::all()->count() == 0) {
                
                Schema::table('pamonline_pamcontent_team', function($table)
                {
                    $table->text('linkedin');
                    $table->text('virtual_business_card');
                });
            }
        }
    }
    
    public function down()
    {
        Schema::table('pamonline_pamcontent_team', function($table)
        {
            $table->dropColumn('linkedin');
            $table->dropColumn('virtual_business_card');
        });
    }
}
