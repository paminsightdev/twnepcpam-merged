<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcontentJudges extends Migration
{
    public function up()
    {
        // check table exists
        if(!Schema::hasTable('pamonline_pamcontent_judges')) {
            Schema::create('pamonline_pamcontent_judges', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name', 255);
                $table->text('about');
                $table->string('email', 255);
                $table->text('photo');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcontent_judges');
    }
}
