<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcontentFaqPosts extends Migration
{
    public function up()
    {
        // check table exists
        if(!Schema::hasTable('pamonline_pamcontent_faq_posts')) {
            
            Schema::create('pamonline_pamcontent_faq_posts', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title', 255);
                $table->text('text');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcontent_faq_posts');
    }
}
