<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\PamContent\Models\Judges;

class BuilderTableUpdatePamonlinePamcontentJudges extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('pamonline_pamcontent_judges')) {
            // when there is no data put dummy one in
            if(Judges::all()->count() == 0) {
                
                Schema::table('pamonline_pamcontent_judges', function($table)
                {
                    $table->string('year', 4);
                    $table->integer('regionid');
                    $table->string('position', 255);
                    $table->string('chair', 1);
                });
            }
        }
    }
    
    public function down()
    {
        Schema::table('pamonline_pamcontent_judges', function($table)
        {
            $table->dropColumn('year');
            $table->dropColumn('regionid');
            $table->dropColumn('position');
            $table->dropColumn('chair');
        });
    }
}
