<?php namespace Twnepc\PamContent\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcontentFaqSection extends Migration
{
    public function up()
    {
        // check table exists
        if(!Schema::hasTable('pamonline_pamcontent_faq_section')) {
            Schema::create('pamonline_pamcontent_faq_section', function($table)
            {
                $table->engine = 'InnoDB';
                $table->integer('id');
                $table->string('title');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcontent_faq_section');
    }
}
