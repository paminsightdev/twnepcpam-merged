<?php
namespace Twnepc\Pamcontent\components;


use Cms\Classes\ComponentBase;
use Twnepc\Pamcontent\Models\Judges as JudgesModel;

class Judges extends ComponentBase{

    public function componentDetails()
    {
        return [
            'name' => 'Judges posts',
            'description' => 'Display Pam Judges posts'
        ];
    }

    public function defineProperties()
    {
        return [
            'year' => [
                'title'             => 'Year',
                'description'       => 'Filtering judges',
                'default'           => 2017,
                'type'              => 'string'
            ]
        ];
    }

    public function onRun()
    {      
        $this->page['test'] = 'This is test message.';
    }
    
    public function years() {
       $years = \Db::select("select distinct(year) as year from pamonline_pamcontent_judges order by year desc");
       return $years; 
    }

    
    public function posts() {
        $year = ($this->param('year'))?$this->param('year'):$this->property('year');
        $query = "select SUBSTRING_INDEX(SUBSTRING_INDEX(name, ' ', 2), ' ', -1) AS lastname,judges.* from pamonline_pamcontent_judges as judges where year='".intval($year)."' order by lastname ASC";
        $posts = \Db::select($query);
        return $posts;
    }
}
?>