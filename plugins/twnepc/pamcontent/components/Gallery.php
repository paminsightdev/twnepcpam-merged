<?php

namespace Twnepc\Pamcontent\components;

use Cms\Classes\ComponentBase;
use Twnepc\Pamcontent\Models\Gallery as GalleriesModel;
use Twnepc\Pamcontent\Models\Picture as PicturesModel;

class Gallery extends ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'Galleries',
            'description' => 'Display Pam Galleries list'
        ];
    }

    public function defineProperties() {
        return [
            'gallery_id' => [
                'title' => 'Gallery ID',
                'description' => 'Filtering galleries',
                'type' => 'dropdown'
            ]
        ];
    }

    public function getGallery_idOptions() {
        return GalleriesModel::orderBy('id', 'desc')->get()->lists('name', 'id');
    }

    public function onRun() {
        $galIdFromGet = get('gallery_id');
        if ($galIdFromGet > 0) {
            $galleryId = intval($galIdFromGet);
        } else {
            $galleryId = 18; //intval($this->property('gallery_id'));
        }
        if ($galleryId > 0) {
            if ($gallery = GalleriesModel::where('id', $galleryId)->first()) {
                $this->page['record'] = $gallery->attributes;

                if (!empty($gallery->attributes['group'])) {
                    $new = [];
                    $otherGroupsGalleries = GalleriesModel::where(["group" => $gallery->attributes['group'],"dissabled" => false])->orderBy('option_order', 'asc')->get();
                    foreach ($otherGroupsGalleries as $otherGroupGallery) {
                        $new[] = $otherGroupGallery->attributes;
                    }

                    $this->page['otherGalleries'] = $new;
                }
            }
        }


        $this->page['test'] = 'This is test message.';
    }

    public function posts() {
        $galIdFromGet = get('gallery_id');
        if ($galIdFromGet > 0) {
            $galleryId = intval($galIdFromGet);
        } else {
            $galleryId = 18; //intval($this->property('gallery_id'));
        }
        $posts = PicturesModel::where('gallery_id', $galleryId)->orderBy('id', 'desc')->get();
        $newposts = [];
        $nr = 0;
        foreach ($posts as $post) {
            $post = $post->attributes;
            $post['nr'] = $nr++;
            $newposts[] = $post;
        }
        return $newposts;
    }

}

?>