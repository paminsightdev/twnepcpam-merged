<?php



namespace Twnepc\Pamcontent\components;



use Cms\Classes\ComponentBase;

use Twnepc\PamContent\Models\Question as QuestionModel;

use Twnepc\PamContent\Models\QuestionOption as OptionsModel;

use Request;



class Question extends ComponentBase {



    public function componentDetails() {

        return [

            'name' => 'Form question',

            'description' => 'Display question in form'

        ];

    }



    public function defineProperties() {

        return [

            'id_quest' => [

                'title' => 'Question ID',

                'description' => 'Filtering question',

                'type' => 'dropdown'],

             'only_one_option' => ['title' => 'Only option',

                 'description' => 'Only one option in the list',

                 'type' => 'dropdown'

            ]

        ];

    }



    public function getId_questOptions() {

        return QuestionModel::orderBy('id', 'desc')->get()->lists('question', 'id');

    }

    

    public function getOnly_one_optionOptions() {

        $options = [''=>'none'];

        return array_merge($options, OptionsModel::where('id_quest', \Request::input('id_quest',-1))->get()->lists('text','value'));

    }

    

    public function onRun() {



    }

    

    public function onRender() {

        $idQuest = intval($this->property('id_quest'));

        if ($idQuest > 0) {

            if ($quest = QuestionModel::where('id', $idQuest)->first()) {

                $this->page['record'] = $quest->attributes;

                $posts = OptionsModel::where('id_quest', $idQuest)->orderBy('id', 'asc')->get();

                $newposts = [];

                foreach ($posts as $post) {

                    $post = $post->attributes;

                    $newposts[] = $post;

                }



                if ($quest->attributes['options_from_table'] != '') {

		  if ($quest->attributes['sql_where'] == 'countryorder') {

			 

                    $wherePart = "";

                    $newposts = array_merge($newposts,\Db::select("SELECT {$quest->attributes['from_table_values']} as value, {$quest->attributes['from_table_texts']} as text FROM {$quest->attributes['options_from_table']} {$wherePart} order by (case when country_id=272 then 0 else 1 end), country_name ASC"));

		  } else {

                    $wherePart = empty($quest->attributes['sql_where'])?'':" WHERE {$quest->attributes['sql_where']}";

                    $newposts = array_merge($newposts,\Db::select("SELECT {$quest->attributes['from_table_values']} as value, {$quest->attributes['from_table_texts']} as text FROM {$quest->attributes['options_from_table']} {$wherePart}"));			  

		  }

                    $posts = $newposts;

                }                     

                

                if ($quest->attributes['quest_type'] == 'slider' and count($newposts) > 0) {

                    $minValue = $newposts[0]['value'];

                    $maxValue = $newposts[0]['value'];

                    foreach ($newposts as $post) {

                        if ($post['value'] < $minValue) {

                            $minValue = $post['value'];

                        }

                        if ($post['value'] > $maxValue) {

                            $maxValue = $post['value'];

                        }

                    }

                    $this->page['minValue'] = $minValue;

                    $this->page['maxValue'] = $maxValue;

                }

                

                

                

                $this->page['posts'] = $newposts;

                $this->page['only_one_option'] = $this->property('only_one_option','');

                $this->page['option_selected'] = $this->property('option_selected', '');



                $this->page['count_records_plus_one'] = count($posts)-1;

                $this->page['elm_id'] = 'slider' . rand(1000, 10000000);

                if ($quest->attributes['quest_type'] == 'slider') {

                    if (count($posts) > 0) {

                        $pWidth = round(100 / count($posts), 4);

                    } else {

                        $pWidth = 0;

                    }

                } else if ($quest->attributes['quest_type'] == 'options' or $quest->attributes['quest_type'] == 'switches') {

                    if (count($posts) > 0) {

                        $pWidth = round(100 / $quest->attributes['cols_per_row'], 4);

                    } else {

                        $pWidth = 0;

                    }

                } else {

                    $pWidth = 100;

                }

                $this->page['pWidth'] = $pWidth;

            }

        }

        $this->page['test'] = 'This is test message.';

        parent::onRender();

    }



}



?>