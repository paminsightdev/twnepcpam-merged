<?php
namespace Twnepc\Pamcontent\components;


use Cms\Classes\ComponentBase;
use Twnepc\Pamcontent\Models\Team as TeamModel;

class Team extends ComponentBase{

    public function componentDetails()
    {
        return [
            'name' => 'Team posts',
            'description' => 'Display Pam Team posts'
        ];
    }

    public function onRun()
    {
        $this->page['test'] = 'This is test message.';
    }

    public function posts(){
        $posts = TeamModel::all();
        return $posts;
    }
}
?>