<?php
namespace Twnepc\Pamcontent\components;

use Twnepc\Pamcontent\Models\FaqPosts;
use Twnepc\Pamcontent\Models\FaqSection;

use Cms\Classes\ComponentBase;

class Faq extends ComponentBase{

    public function componentDetails()
    {
        return [
            'name' => 'Faq posts',
            'description' => 'Display Pam Faq posts'
        ];
    }

    public function onRun()
    {
        $this->page['test'] = 'This is test message.';
    }

    public function sections(){
        $sections = FaqSection::all();
        return $sections;
    }

    public function getPostsBySection($sectionID){
        $posts = FaqPosts::where('section_id', $sectionID)
            ->orderBy('id', 'desc')
            ->get();
        return $posts;
    }
}
?>