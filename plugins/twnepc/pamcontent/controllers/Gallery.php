<?php namespace Twnepc\PamContent\Controllers;

use Request;
use Backend\Classes\FormWidgetBase;
use Backend\Classes\Controller;
use BackendMenu;
use Twnepc\Pamcontent\Models\Gallery as GalleriesModel;
use Twnepc\Pamcontent\Models\Picture as PicturesModel;

class Gallery extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController','Backend.Behaviors.RelationController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig = 'config_relation.yaml';
    
    
    public function onDelete() {
        $ids = post('checked');
        if (!empty($ids) and count($ids)>0) {
            foreach ($ids as $id) {
                PicturesModel::where("gallery_id","=",$id)->delete();
            }
            GalleriesModel::destroy($ids);
        }
        return ["#f1" => "<script>self.location.reload()</script>"];
    }
    
    public function __construct()
    {         
        parent::__construct();
        BackendMenu::setContext('Twnepc.PamContent', 'main-menu-item', 'side-menu-item5');
    }
    
}