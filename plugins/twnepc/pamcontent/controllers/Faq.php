<?php namespace Twnepc\PamContent\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Twnepc\Pamcontent\Models\FaqPosts as FaqPosts;

class Faq extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.PamContent', 'main-menu-item', 'side-menu-item4');
    }
    
    public function onDelete() {
        $ids = post('checked');
        if (!empty($ids) and count($ids)>0) {
            FaqPosts::destroy($ids);
        }
        return ["#f1" => "<script>self.location.reload()</script>"];        
        
    }
}