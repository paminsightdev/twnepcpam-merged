$(document).ready(function(){

    var checkForInputTimer;
    var oldVal = null;
    var checkForInput = function () {
        if($('#Form-field-Branch-country_id').val() != oldVal) {
            $('#Form-field-Branch-country_id').select2();

            oldVal = $('#Form-field-Branch-country_id').val();
        }
    }

    checkForInputTimer = window.setInterval(checkForInput, 300);

})