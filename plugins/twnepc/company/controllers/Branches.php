<?php namespace Twnepc\Company\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Branches extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        $this->addJs('/plugins/twnepc/company/assets/js/select.js');
        BackendMenu::setContext('Twnepc.Company', 'main-menu-item-companies', 'branch-company-side');
    }
}
