<?php namespace Twnepc\Company\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Twnepc\Company\Models\Company;
use Rainlab\User\Models\User;

class Companies extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Company', 'main-menu-item-companies','company-side-menu');
    }

    
}
