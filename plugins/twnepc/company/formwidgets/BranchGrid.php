<?php 

namespace Twnepc\Company\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use Backend\Widgets\Grid;
use Twnepc\Company\Models\Branch;

class BranchGrid extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'branchgrid';

    public function render() {

        $this->prepareVars();

        return $this->makePartial('branchgrid');
    }

    public function prepareVars()
    {
       $this->vars['records'] = Branch::where('company_id',$this->model->id)->get();
       $this->vars['columnTotal'] = 2;
       $this->vars['noRecordsMessage'] = 'No branch found.';
    }
}