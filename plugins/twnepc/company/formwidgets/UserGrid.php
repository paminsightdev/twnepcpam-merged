<?php 

namespace Twnepc\Company\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use Backend\Widgets\Grid;
use Twnepc\Company\Models\Company;
use Twnepc\Company\Models\Branch;
use Rainlab\User\Models\User;

use Redirect;

class UserGrid extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'usergrid';

    public function render() {

        $this->prepareVars();

        return $this->makePartial('usergrid');
    }

    public function prepareVars()
    {
        $companyusers = User::where('company_id',$this->model->id)->get();
        $this->vars['records'] = $companyusers;
        $this->vars['columnTotal'] = 4;
        $this->vars['noRecordsMessage'] = 'No users found.';

        $cusers = [];
        foreach($companyusers as $c) {
            array_push($cusers,$c->id);
        }

        $this->vars['branches'] = Branch::where('company_id',$this->model->id)->get();
        $this->vars['users'] = User::whereNotIn('id',$cusers)->get();

    }

    public function onSaveUser() {
        $data = post();
        
        $user = User::find($data['userid']);

        $company = Company::find($data['company']);
        if($company){
            $user->company_id = $company->id;
        }

        if($data['branch'] != 'no') {
            $branch = Branch::find($data['branch']);
            if($branch){
                $user->branch_id = $branch->id;
            }
        }
        $user->save();

        
        $this->vars['records'] = User::where('company_id',$company->id)->get();
        $this->vars['columnTotal'] = 4;
        $this->vars['noRecordsMessage'] = 'No users found.';

        return \Redirect::back();
    }
   
}