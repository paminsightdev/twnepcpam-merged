<?php namespace Twnepc\Company\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Model;
use Rainlab\Location\Models\Country;

/**
 * Model
 */
class Branch extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'company' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_company_branches';

    public $belongsTo = [
        'company' => 'Twnepc\Company\Models\Company',

    ];

    /*public $belongsToMany = [
        'users' => 'Rainlab\User\Models\User'
    ];*/

    public $hasOne = [
        'membership' => ['Twnepc\Membership\Models\Membership','id','branch']
    ];
    
    public function getCountryIdOptions() {

        return Country::getNameList();
    }

    

    public function scopeFilterByCompany($query, $data) {
        $company_id = Input::get('User')['thecompany'] ? : $data['company_id'];

        $query->where('company_id', $company_id ? $company_id : 0);

        return $query;
    }
}
