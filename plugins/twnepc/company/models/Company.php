<?php namespace Twnepc\Company\Models;

use Model;

/**
 * Model
 */
class Company extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_company_companies';
    
    public $belongsTo = [
        'industry' => 'Twnepc\User\Models\Industry',
    ];

    public $belongsToMany = [
        'branch' => ['Twnepc\Company\Models\Branch', 'table' => 'twnepc_company_branches'],
    ];

    public $hasOne = [
        'membership' => 'Twnepc\Membership\Models\Membership'
    ];

    public function scopeOrder($query) {
        return $query->orderBy('name','ASC');
    }

    public function beforeSave() {
        unset($this->company_branches);
        unset($this->company_users);
    }
}
