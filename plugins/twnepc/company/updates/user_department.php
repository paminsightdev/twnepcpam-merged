<?php namespace Twnepc\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UserDepartment extends Migration
{
    public function up()
    {
        Schema::create('user_department', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('department_id');
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('company_user');
    }

    
}