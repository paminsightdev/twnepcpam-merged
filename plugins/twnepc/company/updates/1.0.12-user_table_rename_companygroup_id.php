<?php  namespace Twnepc\Company\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class UserTableRenameCompanygroupId extends Migration
{
    public function up()
    {
        Schema::table('twnepc_company_companies', function($table)
        {
            $table->renameColumn('companygroup_id', 'old_companygroup_id');

        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_company_companies')) {
            Schema::table('twnepc_company_companies', function ($table) {
                $table->renameColumn('old_companygroup_id', 'companygroup_id');
            });
        }
    }
}