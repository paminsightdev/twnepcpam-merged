<?php  namespace Twnepc\Company\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddEmailDomain extends Migration
{
    public function up()
    {
        Schema::table('twnepc_company_companies', function($table) {
            $table->string('email_domain', 100)->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_company_companies')) {
            Schema::table('twnepc_company_companies', function ($table) {
                $table->dropColumn(['email_domain']);
            });
        }
    }
}