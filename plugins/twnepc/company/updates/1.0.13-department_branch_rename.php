<?php  namespace Twnepc\Company\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class DepartmentBranchRename extends Migration
{
    public function up()
    {
        Schema::table('user_department', function($table)
        {
            $table->renameColumn('department_id', 'branch_id');

        });
        Schema::table('company_department', function($table)
        {
            $table->renameColumn('department_id', 'branch_id');

        });
        Schema::rename('twnepc_company_departments', 'twnepc_company_branches');
        Schema::rename('user_department', 'user_branch');
        Schema::rename('company_department', 'company_branch');

    }

    public function down()
    {
        Schema::table('user_department', function($table)
        {
            $table->renameColumn('branch_id', 'department_id');

        });
        Schema::table('company_department', function($table)
        {
            $table->renameColumn('branch_id', 'department_id');

        });
        Schema::rename('twnepc_company_branches', 'twnepc_company_departments');
        Schema::rename('user_branch', 'user_department');
        Schema::rename('company_branch', 'company_department');
    }
}