<?php  namespace Twnepc\Company\Updates;
use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcCompanyCompaniesTableAddNewColumns extends Migration
{
    public function up()
    {

        Schema::table('twnepc_company_companies', function($table)
        {
            $table->integer('industry_id')->nullable();
            $table->integer('companygroup_id')->nullable();
            $table->string('company_parentname', 100)->nullable();
            $table->mediumText('company_info')->nullable();
            $table->string('company_website', 100)->nullable();
            $table->tinyInteger('company_inactive')->nullable();
            // $table->timestamp('created_at')->useCurrent()->change();
            // $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->change();
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_company_companies')) {
            Schema::table('twnepc_company_companies', function ($table) {
                $table->dropColumn(['industry_id']);
                $table->dropColumn(['companygroup_id']);
                $table->dropColumn(['company_parentname']);
                $table->dropColumn(['company_info']);
                $table->dropColumn(['company_website']);
                $table->dropColumn(['company_inactive']);
            });
        }
    }
}