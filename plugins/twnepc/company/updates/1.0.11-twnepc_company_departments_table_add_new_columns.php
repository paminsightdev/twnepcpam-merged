<?php  namespace Twnepc\Company\Updates;
use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcCompanyDepartmentsTableAddNewColumns extends Migration
{
    public function up()
    {
        
        Schema::table('twnepc_company_departments', function($table)
        {
            $table->string('branch_sagecode', 50)->nullable();
            $table->string('branch_address1', 100)->nullable();
            $table->string('branch_address2', 100)->nullable();
            $table->string('branch_address3', 100)->nullable();
            $table->string('branch_city', 50)->nullable();
            $table->string('branch_region', 50)->nullable();
            $table->string('branch_zip', 50)->nullable();
            $table->integer('country_id')->nullable();
            $table->string('branch_tel', 30)->nullable();
            $table->string('branch_fax', 30)->nullable();
            $table->tinyInteger('branch_inactive')->nullable();
            // $table->timestamp('created_at')->useCurrent()->change();
            // $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->change();
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_company_departments')) {
            Schema::table('twnepc_company_departments', function ($table) {
                $table->dropColumn(['branch_sagecode']);
                $table->dropColumn(['branch_address1']);
                $table->dropColumn(['branch_address2']);
                $table->dropColumn(['branch_address3']);
                $table->dropColumn(['branch_city']);
                $table->dropColumn(['branch_region']);
                $table->dropColumn(['branch_zip']);
                $table->dropColumn(['country_id']);
                $table->dropColumn(['branch_tel']);
                $table->dropColumn(['branch_fax']);
                $table->dropColumn(['branch_inactive']);
            });
        }
    }
}