<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class UserAddCompanyId extends Migration
{
    public function up()
    {
        Schema::dropIfExists('company_user');
        Schema::table('users', function($table)
        {
            $table->integer('company_id')->after('branch_id')->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['company_id']);
            });
        }
        Schema::create('company_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('user_id');
        });
    }
}