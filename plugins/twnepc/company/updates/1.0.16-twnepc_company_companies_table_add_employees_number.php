<?php  namespace Twnepc\Company\Updates;
use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcCompanyCompaniesTableAddEmployeesNumber extends Migration
{
    public function up()
    {
        Schema::table('twnepc_company_companies', function($table){
            $table->string('employees_number', 100)->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_company_companies')) {
            Schema::table('twnepc_company_companies', function ($table) {
                $table->dropColumn(['employees_number']);
            });
        }
    }
}