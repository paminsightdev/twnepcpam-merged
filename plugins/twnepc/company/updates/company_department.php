<?php namespace Twnepc\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CompanyDepartment extends Migration
{
    public function up()
    {
        Schema::create('company_department', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('department_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('company_department');
    }

    
}