<?php namespace Twnepc\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcCompanyCompanies extends Migration
{
    public function up()
    {
        
        Schema::create('twnepc_company_companies', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_company_companies');
    }


}
