<?php namespace Twnepc\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcCompanyDepartments2 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_company_departments', function($table)
        {
            $table->integer('company_id');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_company_departments', function($table)
        {
            $table->dropColumn('company_id');
        });
    }
}
