<?php namespace Twnepc\Company\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CompanyUser extends Migration
{
    public function up()
    {
        Schema::create('company_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('company_user');
    }

    
}