<?php namespace Twnepc\Company;

use Event;
use System\Classes\PluginBase;
use Backend\Facades\BackendMenu;
use Illuminate\Support\Facades\DB;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function register() {

        BackendMenu::registerContextSidenavPartial('Twnepc.Company', 'main-menu-item-companies', '$/twnepc/company/partials/_sidebar.htm');        
    }

    public function boot(){
        Event::listen('rainlab.user.register', function($user, $data) {
            // Automatically attach customer to company if email domain is matching
            $emailDomain = explode('@',$user->email)[1];
            $company = DB::table('twnepc_company_companies')
                ->whereRaw(sprintf('FIND_IN_SET(\'%s\',email_domain)',$emailDomain))->first();

            if($company){
                $companyId = $company->id;
            }else{
                $companyId = false;
            }

            if($companyId){
                $user->company_id = $companyId;
                $user->save();
            }
        });
    }

    public function registerFormWidgets()
    {
        return [
            'Twnepc\Company\FormWidgets\UserGrid' => 'usergrid',
            'Twnepc\Company\FormWidgets\BranchGrid' => 'branchgrid',
        ];
    }
}
