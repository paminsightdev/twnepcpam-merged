<?php namespace Twnepc\Awards;

use System\Classes\PluginBase;
use Backend;
use Event;
use BackendAuth;
use Twnepc\Awards\Models\AwardPersonnel;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Awards\Models\AwardSecurity;
use Twnepc\Awards\Models\AwardRegulation;
use Twnepc\Awards\Models\AwardInterest;
use Twnepc\Awards\Models\AwardData;

class Plugin extends PluginBase
{
    
    
    public function registerComponents()
    {
        return [
            'Twnepc\Awards\Components\Notes' => 'judgeNotes',
        ];
    }

    public function registerSettings()
    {
    }

    public function registerNavigation()
    {
        $backendUser = BackendAuth::getUser();
		
        if($backendUser->is_superuser == 1) {
            return [
                'awards' => [
                    'label' => 'Awards',
                    'url' => Backend::url('twnepc/awards/awardsyears'),
                    'icon' => 'icon-trophy',
                    'order' => 500,
                    'permissions' => ['pamonline.awards.access_main'],
                    'sideMenu' => [
                        'awardsyears' => [
                            'label' => 'Awards',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/awardsyears')
                        ],
                        'leagueyears' => [
                            'label' => 'Voting Results',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/leagueyears/'),
                        ],
//                        'regions' => [
//                            'label' => 'Award Regions',
//                            'icon' => 'icon-copy',
//                            'url' => Backend::url('twnepc/awards/regions')
//                        ],
                        'dates' => [
                            'label' => 'Award Dates',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/dates')
                        ],
                        'applications' => [
                            'label' => 'Awards Applications',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/applications')
                        ],
                        'votes' => [
                            'label' => 'Awards Votes',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/votes')
                        ],
                        'notes' => [
                            'label' => 'Awards Notes',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/notes')
                        ],
                        'personnels' => [
                            'label' => 'Group Structure And Personnel',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/personnels')
                        ],
                        'investments' => [
                            'label' => 'Investment Management Process',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/investments')
                        ],
                        'securities' => [
                            'label' => 'Pooled Funds/Unlisted Securities',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/securities')
                        ],
                        'regulations' => [
                            'label' => 'Regulation And Investor Protection',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/regulations')
                        ],
                        'interests' => [
                            'label' => 'Fees, Charges And Interest',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/interests')
                        ],
                        'data' => [
                            'label' => 'Data updates - Year end',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/data')
                        ],
                        'winnersyears' => [
                            'label' => 'Awards Winners',
                            'icon' => 'icon-trophy',
                            'url' => Backend::url('twnepc/awards/winnersyears'),
                        ],
                        'feedback' => [
                            'label' => 'Feedback',
                            'icon' => 'icon-trophy',
                            'url' => Backend::url('twnepc/awards/feedback')
                        ],
                    ]
                ]
            ];
        }else{
            return [
                'pamsawards' => [
                    'label'       => 'PAM Awards',
                    'url'         => Backend::url('twnepc/awards/pamsawards'),
                    'icon'        => 'icon-diamond',
                    'permissions' => ['pamonline.awards.access_pamsawards'],
                    'order'       => 500,
                    'sideMenu' => [
                        'pamscategories' => [
                            'label' => 'Awards Categories',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/pamscategories'),
                        ],						
                        'pamspersonnels' => [
                            'label' => 'Group Structure And Personnel',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/pamspersonnels/'.AwardPersonnel::getControllerContextForPams()),
//                            'permissions' => ['pamonline.awards.access_pamspersonnels']
                        ],
                        'pamsinvestments' => [
                            'label' => 'Investment Management Process',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/pamsinvestments/'.AwardInvestment::getControllerContextForPams()),
                        ],
                        'pamssecurities' => [
                            'label' => 'Pooled Funds/Unlisted Securities',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/pamssecurities/'.AwardSecurity::getControllerContextForPams()),
                        ],
                        'pamsregulations' => [
                            'label' => 'Regulation And Investor Protection',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/pamsregulations/'.AwardRegulation::getControllerContextForPams()),
                        ],
                        'pamsinterests' => [
                            'label' => 'Fees, Charges And Interest',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/pamsinterests/'.AwardInterest::getControllerContextForPams()),
                        ],
                        'pamsdata' => [
                            'label' => 'Data updates - Year end',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/pamsdata/'.AwardData::getControllerContextForPams()),
                        ],
                        'pamshistory' => [
                            'label' => 'Awards History',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/pamshistory'),
                        ],
                    ]
                ],
                'judgeawards' => [
                    'label'       => 'Judging area',
                    'url'         => Backend::url('twnepc/awards/judgeawards'),
                    'icon'        => 'icon-edit',
                    'permissions' => ['pamonline.awards.access_judgeawards'],
                    'order'       => 500,
                    'sideMenu' => [
                        'judgeawards' => [
                            'label' => 'PAM Awards',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/judgeawards/'),
                        ],
                        'judgenotes' => [
                            'label' => 'Notes',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/judgenotes/'),
                        ],
                        'judgeleagueyears' => [
                            'label' => 'Voting Results',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/judgeleagueyears/'),
                        ],
                        'judgeawardsyears' => [
                            'label' => 'Historical Award Entries',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/judgeawardsyears/'),
                        ],
                    ]
                ],
                'awards' => [
                    'label' => 'Awards',
                    'url' => Backend::url('twnepc/awards/awardsyears'),
                    'icon' => 'icon-diamond',
                    'order' => 500,
                    'permissions' => ['pamonline.awards.access_main'],
                    'sideMenu' => [
                        'awardsyears' => [
                            'label' => 'Awards',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/awardsyears')
                        ],
                        'leagueyears' => [
                            'label' => 'Voting Results',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/leagueyears/'),
                        ],
//                        'regions' => [
//                            'label' => 'Award Regions',
//                            'icon' => 'icon-copy',
//                            'url' => Backend::url('twnepc/awards/regions')
//                        ],
                        'dates' => [
                            'label' => 'Award Dates',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/dates')
                        ],
                        'applications' => [
                            'label' => 'Awards Applications',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/applications')
                        ],
                        'votes' => [
                            'label' => 'Awards Votes',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/votes')
                        ],
                        'notes' => [
                            'label' => 'Awards Notes',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/notes')
                        ],
                        'personnels' => [
                            'label' => 'Group Structure And Personnel',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/personnels')
                        ],
                        'investments' => [
                            'label' => 'Investment Management Process',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/investments')
                        ],
                        'securities' => [
                            'label' => 'Pooled Funds/Unlisted Securities',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/securities')
                        ],
                        'regulations' => [
                            'label' => 'Regulation And Investor Protection',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/regulations')
                        ],
                        'interests' => [
                            'label' => 'Fees, Charges And Interest',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/interests')
                        ],
                        'data' => [
                            'label' => 'Data updates - Year end',
                            'icon' => 'icon-copy',
                            'url' => Backend::url('twnepc/awards/data')
                        ],
                        'winners' => [
                            'label' => 'Awards Winners',
                            'icon' => 'icon-trophy',
                            'url' => Backend::url('twnepc/awards/winners'),
                        ],
                        'feedback' => [
                            'label' => 'Feedback',
                            'icon' => 'icon-trophy',
                            'url' => Backend::url('twnepc/awards/feedback')
                        ],
                    ]
                ]
            ];
        }
    }

    public function registerPermissions()
    {
        return [
            'pamonline.awards.access_main' => ['tab' => 'twnepc.awards::lang.plugin.name', 'label' => 'Access Main Awards section.'],
            'pamonline.awards.access_pamsawards' => ['tab' => 'twnepc.awards::lang.plugin.name', 'label' => 'Pams access to awards.'],
            'pamonline.awards.access_pamscategories' => ['tab' => 'twnepc.awards::lang.plugin.name', 'label' => 'Pams access to awards categories.'],
            'pamonline.awards.access_pamspersonnels' => ['tab' => 'twnepc.awards::lang.plugin.name', 'label' => 'Pams access to awards Group Structure And Personnel.'],
            'pamonline.awards.access_judgeawards' => ['tab' => 'twnepc.awards::lang.plugin.name', 'label' => 'Access to judging area.'],
        ];
    }
}
