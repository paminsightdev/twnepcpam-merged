<?php
namespace Twnepc\Awards\Components;

use Cms\Classes\ComponentBase;
use Twnepc\Awards\Models\AwardYear;
use Twnepc\Awards\Models\Award;
use Twnepc\Awards\Models\AwardApplication;
use Twnepc\Pamcompanies\Models\Companies;
use Twnepc\Awards\Models\AwardNote;
use Validator;
use ValidationException;
use Flash;
use Auth;
use Request;
use Backend;

class Notes extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Judge notes',
            'description' => ''
        ];
    }

    public function onRun()
    {
        $user = $this->user();
        $this->page['user'] = $user;
        $this->addCss('/plugins/twnepc/awards/assets/css/awards.css');
    }

    public function user()
    {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }


    public function onLoadSelectAward()
    {
        $user = $this->user();
        if(!$user)return;
        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $awards = Award::where('awardyear_id',$openedYear->awardyear_id)->get();

            $content = $this->renderPartial('@select_award',[
                'awards'=>$awards
            ]);

        }else{
            $content = $this->renderPartial('@message',[
                'message'=>"No awards detected"
            ]);
        }
        return ['html' => $content];
    }

    public function onLoadSelectPAM()
    {
        $user = $this->user();
        if(!$user)return;
        $awardId = post('award_id',false);
        if(!$awardId)
                return $this->onLoadSelectAward();

        if($awardId == 0)
                return $this->onLoadSelectAward();

        $segment1 = Request::segment(1);
        $segment2 = Request::segment(2);

        if($segment1 == 'company' && $segment2 != ''){
            $_POST['company_id'] = $segment2;
            return $this->onLoadNoteForm();
        }

        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $awards = Award::where('awardyear_id',$openedYear->awardyear_id)->get();
            $awardApplications = AwardApplication::where('award_id',$awardId)->get();

            $companyIds = [];
            foreach($awardApplications as $app){
                $companyIds[] = $app->company_id;
            }

            $companies = Companies::whereIn('company_id', $companyIds)->get();

            $content = $this->renderPartial('@select_pam',[
                'selected_award'=> $awardId,
                'awards'=>$awards,
                'companies' => $companies
            ]);

        }else{
            $content = $this->renderPartial('@message',[
                'message'=>"No awards detected"
            ]);
        }
        return ['html' => $content];
    }

    public function onLoadNoteForm()
    {
        $user = $this->user();
        if(!$user)return;
        $companyId = post('company_id',false);
        if(!$companyId)
            return $this->onLoadSelectPAM();

        if($companyId == 0)
            return $this->onLoadSelectPAM();

        $awardId = post('award_id',false);
        if(!$awardId)
            return $this->onLoadSelectAward();

        if($awardId == 0)
            return $this->onLoadSelectAward();

        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $awards = Award::where('awardyear_id',$openedYear->awardyear_id)->get();
            $awardApplications = AwardApplication::where('award_id',$awardId)->get();

            $companyIds = [];
            foreach($awardApplications as $app){
                $companyIds[] = $app->company_id;
            }

            $companies = Companies::whereIn('company_id', $companyIds)->get();

            $content = $this->renderPartial('@note_form',[
                'selected_award'    => $awardId,
                'selected_company'  => $companyId,
                'awards'            => $awards,
                'companies'         => $companies
            ]);

        }else{
            $content = $this->renderPartial('@message',[
                'message'=>"No awards detected"
            ]);
        }
        return ['html' => $content];
    }

    public function onAddNote()
    {
        $user = $this->user();
        if(!$user)return;
        $data = post();
        $rules = [
            'aw_note'          => 'required'
        ];
        $validation = Validator::make($data, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
        $user = $this->user();
        $data['user_id'] = $user->id;

        $note = new AwardNote;
        $note->fill($data);
        $note->save();

        $content =  $this->renderPartial('@message',[
            'title'=>'Note successfully added.',
        ]);
        return ['html' => $content];
    }

    public function backendAwardJudgeUrl()
    {
        return Backend::url('twnepc/awards/judgenotes');
    }
}
?>