<?php namespace Twnepc\Awards\Models;

use BackendAuth;

/**
 * Model
 */
class AwardSecurity extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardemp3_id';

    /*
     * Validation
     */
    public $rules = [
//        'aemp3_approach'    => 'required',
//        'aemp3_pooled'      => 'required',
//        'aemp3_research'    => 'required',
//        'aemp3_invest'      => 'required',
    ];
    public $attributeNames = [
        'aemp3_approach'    => 'Field 1',
        'aemp3_pooled'      => 'Field 2',
        'aemp3_research'    => 'Field 3',
        'aemp3_invest'      => 'Field 4',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardemp3';

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies'
        ],
        'year' => [
            'Twnepc\Awards\Models\AwardYear',
            'key' => 'awardyear_id',
            'order' => 'ay_year desc',
        ]
    ];

    public static function getControllerContextForPams()
    {
        $user = BackendAuth::getUser();
		
	if (!isset($user->frontenduser)) {
		 return "index";
	}		
		
        if($user->frontenduser->pamadmin)
            $companyID = $user->frontenduser->pamadmin->company->company_id;
        elseif($user->frontenduser->member)
            $companyID = $user->frontenduser->member->company->company_id;
        else
            return "index";

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($opened_year){
            $emp = self::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$companyID)->first();
            if($emp){
                return "update/{$emp->awardemp3_id}";
            }else{
                return "create";
            }
        }
        return "index";
    }
}