<?php namespace Twnepc\Awards\Models;

use ApplicationException;
use Rainlab\User\Models\User;
use Twnepc\Awards\Models\Award;
use Twnepc\PamCompanies\Models\Companies;
/**
 * Model
 */
class AwardVote extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardvote_id';

    /*
     * Validation
     */
    public $rules = [

    ];

    protected $fillable = [
        'av_rank','awardapplication_id','user_id','av_reason'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardvote';

    protected $guarded = [];

    public $belongsTo = [
        'application' => [
            'Twnepc\Awards\Models\AwardApplication',
            'key'=>'awardapplication_id'
        ],
    ];

    public function getUserOptions()
    {
        $options = [];
        $users = User::get();
        foreach($users as $user){
            $options[$user->id] = $user->name." ".$user->surname;
        }
        if(!array_key_exists($this->user_id,$options))
            $options[$this->user_id] = 'Unknown';

        return $options;
    }

    public function getJudgeOptions()
    {
        $options = [];
        $users = User::get();
        foreach($users as $user){
            if($user->isJudge())
                $options[$user->id] = $user->name." ".$user->surname;
        }
        if(!array_key_exists($this->user_id,$options))
            $options[$this->user_id] = 'Unknown';

        return $options;
    }

    public function getAdminRankOptions()
    {
        $application = $this->application;
        $options = [];
        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        $awardApps = AwardApplication::where('award_id',$application->award_id)->where('aa_applied',1)->where('aa_submitted',1)->get();
        $index = 1;
        foreach($awardApps as $key => $app){
            $options[$index] = $index;
            $index++;
        }
        return $options;
    }

    public function getRankOptions()
    {
        $options = [];
        $award_id = post('award_id',null);
        if($award_id == null)
            throw new ApplicationException("Missing post variable.");
        $openedYear = AwardYear::whereRaw("Date(ay_judgingdate) < DATE(NOW())")->whereRaw("Date(ay_judgingends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $awardApps = AwardApplication::where('award_id',$award_id)->where('aa_applied',1)->where('aa_submitted',1)->get();
            $index = 1;
            foreach($awardApps as $key => $app){
                $options[$index] = $index;
                $index++;
            }
        }
        return $options;
    }

    public function getOpenAwards()
    {
        $options = [];
        $options[0] = "Select Open Award";
        $openedYear = AwardYear::whereRaw("Date(ay_judgingdate) < DATE(NOW())")->whereRaw("Date(ay_judgingends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $awards = Award::where('awardyear_id',$openedYear->awardyear_id)->get();
            foreach($awards as $key => $award){
                $options[$award->award_id] = $award->award_name;
            }
        }else{
            $options[0] = "No opened awards";
        }

        return $options;
    }

    public function getAwardApplications()
    {
        $options = [];
        $options[''] = "Select application";
        if($this->award){
            $awardApplications = AwardApplication::where('award_id',$this->award)
                ->where('aa_applied',1)
                ->where('aa_submitted',1)
                ->get();
            foreach($awardApplications as $app){
                $company = Companies::where('company_id',$app->company_id)->first();
                $options[$app->awardapplication_id] = $company->c_companyname;
            }
        }

        return $options;
    }

    public function getAwardApplicationsUpdate()
    {
        $options = [];
        if($this->award){
            $awardApplications = AwardApplication::where('award_id',$this->award)->get();
            foreach($awardApplications as $app){
                $company = Companies::where('company_id',$app->company_id)->first();
                $options[$app->awardapplication_id] = $company->c_companyname;
            }
        }

        return $options;
    }

    public function beforeSave()
    {
        unset($this->award);
        unset($this->company);
    }
}