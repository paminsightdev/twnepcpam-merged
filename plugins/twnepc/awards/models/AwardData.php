<?php namespace Twnepc\Awards\Models;

use BackendAuth;

/**
 * Model
 */
class AwardData extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardemp6_id';

    public $rules = [

    ];
    public $attributeNames = [

    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardemp6';

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies'
        ],
        'year' => [
            'Twnepc\Awards\Models\AwardYear',
            'key' => 'awardyear_id',
            'order' => 'ay_year desc',
        ]
    ];

    public static function getControllerContextForPams()
    {
        $user = BackendAuth::getUser();
		
        if (!isset($user->frontenduser)) {
             return "index";
        }
		
        if($user->frontenduser->pamadmin)
            $companyID = $user->frontenduser->pamadmin->company->company_id;
        elseif($user->frontenduser->member)
            $companyID = $user->frontenduser->member->company->company_id;
        else
            return "index";

//        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
//        if($opened_year){
//            $emp = self::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$companyID)->first();
//            if($emp){
//                return "update/{$emp->awardemp6_id}";
//            }else{
//                return "create";
//            }
//        }
        return "index";
    }

    public function getCurrentOpenedYear()
    {
        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryharddeadline) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($opened_year){
            return $opened_year->ay_year;
        }
        return date("Y");
    }
}