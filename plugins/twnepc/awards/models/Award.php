<?php namespace Twnepc\Awards\Models;

use Twnepc\Awards\Models\AwardYear as Years;

/**
 * Model
 */
class Award extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'award_id';

    /*
     * Validation
     */
    public $rules = [
        'award_name'=>'required'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_award';

    protected $guarded = [];

    public $belongsTo = [
        'year' => [
            'Twnepc\Awards\Models\AwardYear',
            'key' => 'awardyear_id',
        ]
    ];
    
    public $hasOne = [
        'application' => [
            'Twnepc\Awards\Models\AwardApplication', 
            'key' => 'award_id',
            'otherKey' => 'award_id'
        ]
        
    ];
    

    public $belongsToMany = [
        'links' => [
            'Twnepc\Awards\Models\Award',
            'table' => 'pamtbl_awardlink',
            'key' => 'award_id',
            'otherKey' => 'to_award_id'
        ]
    ];

    public function getYearOptions($keyValue = null)
    {
        $options = [];
        $years = Years::orderBy('ay_year','desc')->get();

        foreach($years as $year){
            $options[$year->awardyear_id] = $year->ay_year;//.' / '.$year->region->awardregion_name;
        }

        return $options;
    }

    public function getLinksOptions($keyValue = null)
    {
        $options = [];
        $awards = Award::where('awardyear_id',$this->awardyear_id)->get();

        foreach($awards as $award){
            if($this->award_id == $award->award_id)continue;
            $options[$award->award_id] = $award->award_name." / year - ".$award->year->ay_year;
        }

        return $options;
    }

    public static function getActiveAward()
    {
        $activeAward = null;
        $openedYear = Years::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $activeAward = self::where('awardyear_id',$openedYear->awardyear_id)->first();
        }
        return $activeAward;
    }

    public function getInvestmentType()
    {
        if($this->award_investment == 1)
            return "Investment";
        else
            return "Non-investment";
    }

    public function getLinkedCategoryID()
    {
        foreach($this->links as $award){
            return $award->award_id;
        }
        return 0;
    }

    public function getLinkedCategoryTitle()
    {
        foreach($this->links as $award){
            return $award->award_name;
        }
        return "";
    }

}