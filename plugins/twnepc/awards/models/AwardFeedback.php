<?php namespace Twnepc\Awards\Models;

use BackendAuth;
use Twnepc\PamCompanies\Models\Companies;

/**
 * Model
 */
class AwardFeedback extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardfeed_id';

    /*
     * Validation
     */
    public $rules = [

    ];

    public $fillable = [
        'af_note','company_id','awardyear_id'
    ];

    public $attachMany = [
        'feedbackfiles' => ['System\Models\File']
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardfeedback';

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies'
        ],
        'year' => [
            'Twnepc\Awards\Models\AwardYear',
            'key' => 'awardyear_id'
        ]
    ];

    public function getYearOptions($keyValue = null)
    {
        $options = [];
        $years = AwardYear::orderBy('ay_year','desc')->get();

        foreach($years as $year){
            $options[$year->awardyear_id] = $year->ay_year.' / '.$year->region->awardregion_name;
        }

        return $options;
    }

    public function getCompanyOptions($keyValue = null)
    {
        $options = [];
        $companies = Companies::all();
        foreach($companies as $company){
            $options[$company->company_id] = $company->c_companyname;
        }
        return $options;
    }
}