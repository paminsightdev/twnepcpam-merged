<?php namespace Twnepc\Awards\Models;

/**
 * Model
 */
class AwardWinners extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardapplication_id';
    public $feedback;
    /*
     * Validation
     */
    public $rules = [
    ];

    protected $fillable = [
        'aa_result','company_id','award_id'
    ];


    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardapplication';

    public $belongsTo = [
        'company' => [
            'Twnepc\PamCompanies\Models\Companies'
        ],
        'award' => [
            'Twnepc\Awards\Models\Award'
        ]
    ];
	
    public function getAaResultOptions($keyValue = null)
    {
        $options = [];
       // $years = Years::orderBy('ay_year','desc')->get();

       // foreach($years as $year){	
        $options[""] = '---';
        $options["w"] = 'Winner';
        $options["s"] = 'Shortlisted';
        $options["c"] = 'Commended';
        $options['p'] = 'Not Progressed';
		 
       // }

        return $options;
    }		
}