<?php namespace Twnepc\Awards\Models;

use Twnepc\PamCompanies\Models\Companies;
use Rainlab\User\Models\User;

/**
 * Model
 */
class AwardNote extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardnote_id';

    /*
     * Validation
     */
    public $rules = [
        'aw_note'=>'required'
    ];

    protected $fillable = [
        'aw_note','company_id','user_id','award_id'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardnote';

    protected $guarded = [];

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies',
            'key' => 'company_id',
            'otherKey' => 'company_id'
        ],
        'award' => [
            'Twnepc\Awards\Models\Award'
        ],
        'user' => [
            'Rainlab\User\Models\User',
            'key' => 'user_id',
            'otherKey' => 'id'
        ]
    ];

    public function getUserOptions()
    {
        $options = [];
        $users = User::get();
        foreach($users as $user){
            $options[$user->id] = $user->name." ".$user->surname;
        }
        if(!array_key_exists($this->user_id,$options))
            $options[$this->user_id] = 'Unknown';

        return $options;
    }

    public function getJudgeOptions()
    {
        $options = [];
        $users = User::get();
        foreach($users as $user){
            if($user->isJudge())
                $options[$user->id] = $user->name." ".$user->surname;
        }
        if(!array_key_exists($this->user_id,$options))
            $options[$this->user_id] = 'Unknown';

        return $options;
    }

    public function getYearOptionsOnCreate()
    {
        $options = [];
        $options[''] = 'Choose year';
        $awardYears = AwardYear::orderBy('awardyear_id','DESC')->get();
        foreach($awardYears as $year){
            $options[$year->awardyear_id] = $year->ay_year;
        }
        return $options;
    }

    public function getAwardOptionsOnCreate()
    {
        $options = [];
        $options[''] = 'Choose Awards';
        if($this->year){
            $awards = Award::where('awardyear_id',$this->year)->get();
            foreach($awards as $award){
                $options[$award->award_id] = $award->award_name;
            }
        }
        return $options;
    }

    public function getCompanyOptionsOnCreate()
    {
        $options = [];
        $options[''] = 'Choose Company';
        if($this->award){
            $awardApplications = AwardApplication::where('award_id',$this->award->award_id)->get();
            foreach($awardApplications as $app){
                $company = Companies::where('company_id',$app->company_id)->first();
                if($company){
                    $options[$company->company_id] = $company->c_companyname;
                }
            }
        }

        return $options;
    }

    public function getOpenAwards()
    {
        $options = [];
        $options[0] = "Select Open Award";
        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $awards = Award::where('awardyear_id',$openedYear->awardyear_id)->get();
            foreach($awards as $key => $award){
                $options[$award->award_id] = $award->award_name;
            }
        }

        return $options;
    }

    public function getAwardCompanies()
    {
        $options = [];
        if($this->award){
            $awardApplications = AwardApplication::where('award_id',$this->award->award_id)->get();
            $companyIds = [];
            foreach($awardApplications as $app){
                $companyIds[] = $app->company_id;
            }
            $companies = Companies::whereIn('company_id', $companyIds)->get();
            foreach($companies as $company){
                $options[$company->company_id] = $company->c_companyname;
            }
        }

        return $options;
    }

    public function beforeSave()
    {
        unset($this->year);
    }
}