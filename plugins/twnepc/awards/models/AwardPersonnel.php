<?php namespace Twnepc\Awards\Models;

use BackendAuth;

/**
 * Model
 */
class AwardPersonnel extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardemp1_id';

    /*
     * Validation
     */
    public $rules = [
//        'aemp1_name'        => '',
//        'aemp1_ceo'         => 'required',
//        'aemp1_hopc'        => 'required',
//        'aemp1_cio'         => 'required',
//        'aemp1_custodians'  => 'required',
//        'aemp1_banks'       => 'required',
//        'aemp1_history'     => 'required',
//        'aemp1_per1'        => 'required',
//        'aemp1_pex1'        => 'required',
//        'aemp1_per2'        => 'required',
//        'aemp1_pex2'        => 'required',
//        'aemp1_per3'        => 'required',
//        'aemp1_pex3'        => 'required',
//        'aemp1_per4'        => 'required',
//        'aemp1_pex4'        => 'required',
//        'aemp1_per5'        => 'required',
//        'aemp1_pex5'        => 'required',
//        'aemp1_per6'        => 'required',
//        'aemp1_pex6'        => 'required',
//        'aemp1_per7'        => 'required',
//        'aemp1_pex7'        => 'required',
    ];
    public $attributeNames = [
        'aemp1_ceo'         => 'CEO',
        'aemp1_hopc'        => 'Head of Private Clients',
        'aemp1_cio'         => 'Chief Investment Officer',
        'aemp1_custodians'  => 'Custodians',
        'aemp1_banks'       => 'Banks',
        'aemp1_history'     => 'History',
        'aemp1_per1'        => 'Relationship Managers: Number',
        'aemp1_pex1'        => 'Relationship Managers: Average Years Experience',
        'aemp1_per2'        => 'Assistants: Number',
        'aemp1_pex2'        => 'Assistants: Average Years Experience',
        'aemp1_per3'        => 'Portfolio Managers: Number',
        'aemp1_pex3'        => 'Portfolio Managers: Average Years Experience',
        'aemp1_per4'        => 'Assistants: Number',
        'aemp1_pex4'        => 'Assistants: Average Years Experience',
        'aemp1_per5'        => 'Research: Number',
        'aemp1_pex5'        => 'Research: Average Years Experience',
        'aemp1_per6'        => 'Sales & Marketing: Number',
        'aemp1_pex6'        => 'Sales & Marketing: Average Years Experience',
        'aemp1_per7'        => 'Administration & Settlements: Number',
        'aemp1_pex7'        => 'Administration & Settlements: Average Years Experience',
        'aemp1_to1'         => "Staff turnover"
    ];

    protected $fillable = [
        'aemp1_to1',
        'aemp1_to2'
    ];

    public $attachMany = [
        'files' => ['System\Models\File','public'=>false]
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardemp1';

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies'
        ],
        'year' => [
            'Twnepc\Awards\Models\AwardYear',
            'key' => 'awardyear_id',
            'order' => 'ay_year desc',
        ]
    ];

    public static function getControllerContextForPams()
    {
        $user = BackendAuth::getUser();
	
	if (!isset($user->frontenduser)) {
		 return "index";
	}
		
        if($user->frontenduser->pamadmin)
            $companyID = $user->frontenduser->pamadmin->company->company_id;
        elseif($user->frontenduser->member)
            $companyID = $user->frontenduser->member->company->company_id;
        else
            return "index";

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($opened_year){
            $emp = self::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$companyID)->first();
            if($emp){
                return "update/{$emp->awardemp1_id}";
            }else{
                return "create";
            }
        }
        return "index";
    }
}