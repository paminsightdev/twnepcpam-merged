<?php namespace Twnepc\Awards\Models;

/**
 * Model
 */
class AwardRegion extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardregion_id';

    /*
     * Validation
     */
    public $rules = [
        'awardregion_name'=>'required'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardregion';

    protected $guarded = [];
}