<?php namespace Twnepc\Awards\Models;

/**
 * Model
 */
class AwardYear extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardyear_id';

    /*
     * Validation
     */
    public $rules = [
        'ay_year'=>'required'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardyear';

    protected $guarded = [];

    public $belongsTo = [
        'region' => [
            'Twnepc\Awards\Models\AwardRegion',
            'key' => 'awardregion_id',
        ]
    ];

}