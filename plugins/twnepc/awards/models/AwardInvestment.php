<?php namespace Twnepc\Awards\Models;

use BackendAuth;

/**
 * Model
 */
class AwardInvestment extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardemp2_id';

    public $rules = [
//        'aemp2_discretionary1'        => 'required',
//        'aemp2_advisory1'       => 'required',
//        'aemp2_philosophy'  => 'required',
//        'aemp2_client_gained1'  => 'required',
//        'aemp2_client_lost1'    => 'required',
//        'aemp2_client_net1' => 'required',
//        'aemp2_client_gained2'  => 'required',
//        'aemp2_client_lost2'    => 'required',
//        'aemp2_client_net2' => 'required',
//        'aemp2_asset_gained1' => 'required',
//        'aemp2_asset_lost1' => 'required',
//        'aemp2_asset_net1'  => 'required',
//        'aemp2_asset_gained2' => 'required',
//        'aemp2_asset_lost2' => 'required',
//        'aemp2_asset_net2'  => 'required',
//        'aemp2_portfolio1'  => 'required',
//        'aemp2_portfolio2'  => 'required',
//        'aemp2_avg_client'  => 'required',
//        'aemp2_avg_family'  => 'required',
//        'aemp2_process_change' => 'required',
    ];
    public $attributeNames = [
        'aemp2_discretionary1'         => 'Discretionary',
        'aemp2_advisory1'           => 'Advisory',
        'aemp2_philosophy'  => 'Philosophy',
        'aemp2_client_gained1' => 'Number of Clients Gained',
        'aemp2_client_lost1'    => 'Number of Clients Lost',
        'aemp2_client_net1' => 'Net Client Total',
        'aemp2_client_gained2' => 'Number of Clients Gained',
        'aemp2_client_lost2'    => 'Number of Clients Lost',
        'aemp2_client_net2' => 'Net Client Total',
        'aemp2_asset_gained1' => 'Assets gained',
        'aemp2_asset_lost1' => 'Assets lost',
        'aemp2_asset_net1'  => 'Assets net',
        'aemp2_asset_gained2' => 'Assets gained',
        'aemp2_asset_lost2' => 'Assets lost',
        'aemp2_asset_net2'  => 'Assets net',
        'aemp2_portfolio1'  => 'Portfolio size',
        'aemp2_portfolio2'  => 'Portfolio size',
        'aemp2_avg_client'  => 'Avg number of client accounts',
        'aemp2_avg_client'  => 'Avg number of family groups',
        'aemp2_process_change' => 'Investment process',
    ];

    protected $fillable = [
        'aemp2_client_gained1', 'aemp2_client_lost1', 'aemp2_client_net1', 'aemp2_asset_gained1', 'aemp2_asset_lost1',
        'aemp2_asset_net1', 'aemp2_client_gained2', 'aemp2_client_lost2', 'aemp2_client_net2', 'aemp2_asset_gained2',
        'aemp2_asset_lost2', 'aemp2_asset_net2', 'aemp2_portfolio1', 'aemp2_portfolio2', 'aemp2_total1', 'aemp2_pc1',
        'aemp2_c1', 'aemp2_pf1', 'aemp2_i1', 'aemp2_total2', 'aemp2_pc2', 'aemp2_c2', 'aemp2_pf2', 'aemp2_i2', 'aemp2_curr11',
        'aemp2_curr12', 'aemp2_curr13', 'aemp2_curr14', 'aemp2_curr15', 'aemp2_curr21', 'aemp2_curr22',
        'aemp2_curr23', 'aemp2_curr24', 'aemp2_curr25', 'aemp2_def1', 'aemp2_bal1', 'aemp2_grow1', 'aemp2_hgrow1',
        'aemp6_tot1', 'aemp2_def2', 'aemp2_bal2', 'aemp2_grow2', 'aemp2_hgrow2', 'aemp2_other2', 'aemp2_m11', 'aemp2_m12',
        'aemp2_m13', 'aemp2_m14', 'aemp2_m15', 'aemp2_m21', 'aemp2_m22', 'aemp2_m23', 'aemp2_m24', 'aemp2_m25'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardemp2';

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies',
            'key' => 'company_id',
            'otherKey' => 'company_id'
        ],
        'year' => [
            'Twnepc\Awards\Models\AwardYear',
            'key' => 'awardyear_id',
            'order' => 'ay_year desc',
        ]
    ];

    public static function getControllerContextForPams()
    {
        $user = BackendAuth::getUser();
		
        if (!isset($user->frontenduser)) {
             return "index";
        }
		
        if($user->frontenduser->pamadmin)
            $companyID = $user->frontenduser->pamadmin->company->company_id;
        elseif($user->frontenduser->member)
            $companyID = $user->frontenduser->member->company->company_id;
        else
            return "index";

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($opened_year){
            $emp = self::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$companyID)->first();
            if($emp){
                return "update/{$emp->awardemp2_id}";
            }else{
                return "create";
            }
        }
        return "index";
    }

    public function loadCompanyRelation()
    {
        $this->company = $this->company();
    }
}