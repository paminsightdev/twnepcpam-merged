<?php namespace Twnepc\Awards\Models;

use BackendAuth;

/**
 * Model
 */
class AwardInterest extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardemp5_id';

    /*
     * Validation
     */
    public $rules = [
//        'aemp5_clean'    => 'required',
//        'aemp5_performance'      => 'required',
//        'aemp5_adjustment'    => 'required',
//        'aemp5_commissions'      => 'required',
//        'aemp5_rebate'      => 'required',
    ];
    public $attributeNames = [
        'aemp5_clean'    => 'text',
        'aemp5_performance'      => 'text',
        'aemp5_adjustment'    => 'text',
        'aemp5_commissions'      => 'text',
        'aemp5_rebate'      => 'text',
    ];


    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardemp5';

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies'
        ],
        'year' => [
            'Twnepc\Awards\Models\AwardYear',
            'key' => 'awardyear_id',
            'order' => 'ay_year desc',
        ]
    ];

    public static function getControllerContextForPams()
    {
        $user = BackendAuth::getUser();

        if (!isset($user->frontenduser)) {
             return "index";
        }
		
        if($user->frontenduser->pamadmin)
            $companyID = $user->frontenduser->pamadmin->company->company_id;
        elseif($user->frontenduser->member)
            $companyID = $user->frontenduser->member->company->company_id;
        else
            return "index";

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($opened_year){
            $emp = self::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$companyID)->first();
            if($emp){
                return "update/{$emp->awardemp5_id}";
            }else{
                return "create";
            }
        }
        return "index";
    }
}