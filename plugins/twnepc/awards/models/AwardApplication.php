<?php namespace Twnepc\Awards\Models;

use Twnepc\Pamcompanies\Models\Companies;

/**
 * Model
 */
class AwardApplication extends AwardsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'awardapplication_id';
    public $feedback;

    /*
     * Validation
     */
    public $rules = [
        'aa_reason'=>'required'
    ];

    protected $fillable = [
        'aa_reason','company_id','award_id','aa_applied'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_awardapplication';

    public $belongsTo = [
        'company' => [
            'Twnepc\PamCompanies\Models\Companies'
        ],
        'award' => [
            'Twnepc\Awards\Models\Award', 
            'key' => 'award_id',
            'otherKey' => 'award_id'
        ]
    ];

    public $hasMany = [
        'votes' => [
            'Twnepc\Awards\Models\AwardVote',
            'key' => 'awardapplication_id',
            'otherKey' => 'awardapplication_id'
        ]
    ];

    public $attachMany = [
        'files' => ['System\Models\File','public'=>false]
    ];

    public function getYearOptionsOnCreate()
    {
        $options = [];
        $options[''] = '';
        $awardYears = AwardYear::orderBy('awardyear_id','DESC')->get();
        foreach($awardYears as $year){
            $options[$year->awardyear_id] = $year->ay_year;
        }
        return $options;
    }

    public function getAwardOptionsOnCreate()
    {
        $options = [];
        if($this->year){
            $awards = Award::where('awardyear_id',$this->year)->get();
            foreach($awards as $award){
                $options[$award->award_id] = $award->award_name;
            }
        }
        return $options;
    }

    public function getCompanyOptionsOnCreate()
    {
        $options = [];
        $companies = Companies::where('c_active',1)->get();
        foreach($companies as $app){

            $options[$app->company_id] = $app->c_companyname;
        }
        return $options;
    }

    public function beforeSave()
    {
	unset($this->feedback_list);
        unset($this->year);
    }
}
