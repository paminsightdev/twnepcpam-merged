<?php namespace Twnepc\Awards\Models;

use Model;
use BackendAuth;
/**
 * Model
 */
class AwardsModel extends Model
{
    public function beforeCreate()
    {
        $user = BackendAuth::getUser();
        $user->frontenduser;
        if($user->frontenduser)
            $this->pam_userid = $user->frontenduser->id;
    }

    public function beforeUpdate()
    {
        $user = BackendAuth::getUser();
        $user->frontenduser;
        if($user->frontenduser)
            $this->pam_userid = $user->frontenduser->id;
    }

    public function beforeDelete()
    {
        $this->logAwardChanges($this->original,array());
    }

    public function afterUpdate()
    {
        $this->logAwardChanges($this->original,$this->attributes);
    }

    private function logAwardChanges($beforeSave,$afterSave)
    {
        $user = BackendAuth::getUser();
        $diff = array_diff_assoc($beforeSave,$afterSave);
        if(count($diff) > 0){
            $awardUpdateModel = new AwardUpdate;
            $awardUpdateModel->user_id = $user->id;
            $awardUpdateModel->award_id = isset($this->award_id)?$this->award_id:0;
            $awardUpdateModel->company_id = isset($this->company_id)?$this->company_id:0;
            $awardUpdateModel->before_save = $beforeSave;
            $awardUpdateModel->after_save = $afterSave;
            $awardUpdateModel->save();
        }
    }

    public function getSectionYear()
    {
        return $this->year;
    }

    public function resetValidationRules()
    {
        $this->rules = [];
    }
}