<?php

namespace Twnepc\Awards\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 *
 *
 */
class Hidden extends FormWidgetBase
{
    protected $defaultAlias = 'hidden';
    protected $key = null;

    public function init()
    {

    }

    public function prepareVars()
    {
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['name'] = $this->formField->getName();
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial('field');
    }

}