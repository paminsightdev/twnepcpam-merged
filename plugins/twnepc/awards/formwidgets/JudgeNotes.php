<?php

namespace Twnepc\Awards\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Validator;
use ValidationException;
use Twnepc\Awards\Models\AwardNote;
use BackendAuth;
use Flash;
use Twnepc\Awards\Models\AwardApplication;
/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class JudgeNotes extends FormWidgetBase {

    public $alias = 'judgeNotes';

    public function init()
    {
        $this->user = BackendAuth::getUser();
    }

    public function prepareVars()
    {
        $this->vars['showList'] = isset($this->config->showList)?$this->config->showList:0;
        $this->vars['notes'] = [];
        $this->vars['canEdit'] = false;
        if(isset($this->model->{$this->config->key}))
                $this->vars['notes'] = $this->model->{$this->config->key};

        if(isset($this->config->edit))
                $this->vars['canEdit'] = $this->config->edit;
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    public function onLoadAddJudgeNote()
    {
        return $this->makePartial('add_note');
    }

    public function onLoadUpdateNote()
    {
        $id = post('note_id', 0);
        if (!$item = AwardNote::find($id))
            throw new Exception('Record not found.');
        $this->vars['note_id'] = $id;
        $config = $this->makeConfig('$/twnepc/awards/models/awardnote/judge_fields.yaml');
        $config->model = $item;
        $config->context = 'update';
        $form = $this->makeWidget('Backend\Widgets\Form', $config);
        $this->vars['form'] = $form;
        return $this->makePartial('update_note');
    }

    public function onUpdateNote()
    {
        $post = post();
        if (!isset($post['note_id'])) {
            throw new Exception('ID must be specified.');
        }
        $id = intval($post['note_id']);

        $note = AwardNote::find($id);

        if (!$note)
            throw new Exception('Record not found.');

        $config = $this->makeConfig('$/twnepc/awards/models/awardnote/judge_fields.yaml');
        $config->model = $note;
        $config->context = 'update';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $note->fill($post);
        $sessionKey = $formWidget->getSessionKey();
        if($note->validate())
            $note->save();
        $this->prepareVars();
        $this->vars['notes'] = AwardNote::where('company_id',post('company'))->where('award_id',post('award'))->where('user_id',$this->user->frontenduser->id)->orderBy('pam_modified','DESC')->get();
        return [
            '#judgeNotes' => $this->makePartial('options', ['notes' => $this->vars['notes']])
        ];
    }

    public function onAddNote()
    {
        $post = post();
        $rules = [
            'note'          => 'required'
        ];
        $validation = Validator::make($post, $rules);
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
        $data = [];
        $data['aw_note'] = $post['note'];
        $data['company_id'] = $this->model->company_id;
        $data['award_id'] = $this->model->award_id;
        $data['user_id'] = $this->user->frontenduser->id;

        $note = new AwardNote;
        $note->fill($data);
        $note->save();

        Flash::success("Note added.");
        $this->vars['notes'] = AwardNote::where('company_id',$this->model->company_id)
            ->where('award_id',$this->model->award_id)
            ->where('user_id',$this->user->frontenduser->id)
            ->orderBy('pam_modified','DESC')
            ->get();
        $this->vars['canEdit'] = true;
        $this->vars['showList'] = isset($this->config->showList)?$this->config->showList:0;
        return [
            '#judgeNotes' => $this->makePartial('options', $this->vars)
        ];

    }

    public function canJudgeVote($debug = false)
    {
        $appID = $this->model->awardapplication_id;

        $awardApp = AwardApplication::where('awardapplication_id',$appID)->first();

        if($awardApp->award->award_canjudgevote == 0)
            return false;

        $startDate = strtotime($awardApp->award->year->ay_judgingdate);
        $endDate = strtotime($awardApp->award->year->ay_judgingends);
        $now = time();
        if($debug) {
            dump(date('m/d/Y H:i:s', $startDate));
            dump(date('m/d/Y H:i:s', $endDate));
            dump(date('m/d/Y H:i:s', $now));
        }
        if($startDate < $now && $now < $endDate){
            return true;
        }
        return false;
    }
}
