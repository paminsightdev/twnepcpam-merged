<?php

namespace Twnepc\Awards\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Backend\FormWidgets\FileUpload;
use Exception;
use Input;
use Twnepc\Awards\Models\AwardFeedback;
use October\Rain\Exception\ApplicationException;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Feedback extends FormWidgetBase {

    public $alias = 'index';

    public function init()
    {
        if(Request::header('X-OCTOBER-FILEUPLOAD')){

            if($feedbackID = post('awardfeed_id',false)){
                $config = $this->makeConfig('$/twnepc/awards/models/awardfeedback/widget_fields.yaml');
                $config->model = AwardFeedback::find($feedbackID);
                $config->context = "update";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }else{
                $config = $this->makeConfig('$/twnepc/awards/models/awardfeedback/widget_fields.yaml');
                $config->model = new AwardFeedback();
                $config->context = "create";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
        }
        if(Request::header('X-OCTOBER-REQUEST-HANDLER')){
            if($articleID = post('awardfeed_id',false)){
                $config = $this->makeConfig('$/twnepc/awards/models/awardfeedback/widget_fields.yaml');
                $config->model = AwardFeedback::find($articleID);
                $config->context = "update";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
        }
    }

    public function prepareVars()
    {
        $this->vars['company'] = $this->model->company;
        if(!$this->vars['company'])
                throw new ApplicationException("Model has to be a relation 'company'");

        $this->vars['award'] = $this->model->award;
        if(!$this->vars['award'])
            throw new ApplicationException("Model has to be a relation 'award'");

        $this->vars['year'] = $this->model->award->year;
        $this->vars['feedback'] = AwardFeedback::where('company_id',$this->vars['company']->company_id)->where('awardyear_id',$this->vars['year']->awardyear_id)->first();


    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial($this->alias);
    }

    

    public function onLoadUpdateFeedback()
    {
        try {
            $this->prepareVars();
           
            $config = $this->makeConfig('$/twnepc/awards/models/awardfeedback/widget_fields.yaml');
            $config->model = $this->vars['feedback'];
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $files = $this->makeWidget('Backend\FormWidgets\FileUpload',$this);
            $files->bindToController();
            $this->vars['form'] = $form;

        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update');
    }

    public function onUpdateFeedback()
    {
        $post = post();
        $this->prepareVars();


        $config = $this->makeConfig('$/twnepc/awards/models/awardfeedback/widget_fields.yaml');
        $config->model = $this->vars['feedback'];
        $config->context = 'update';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
        $this->vars['feedback']->fill($post);
        $this->vars['feedback']->save(null, $formWidget->getSessionKey());


        $return_path = str_replace('admin-panel/', '', Request::path());
        return \Backend::redirect($return_path);

        /*return [
            '#feedback_action' => $this->makePartial($this->alias, $this->vars)
        ];*/

    }

    public function onLoadAddFeedback()
    {
        try {
            $this->prepareVars();

            $config = $this->makeConfig('$/twnepc/awards/models/awardfeedback/widget_fields.yaml');
            $config->model = new AwardFeedback;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            
            $this->vars['form'] = $form;
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create');
    }

    public function onAddFeedback()
    {
        $post = post();
        $model = new AwardFeedback;
       
        $config = $this->makeConfig('$/twnepc/awards/models/awardfeedback/widget_fields.yaml');
        $config->model = $model;
        $config->context = 'create';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
        $formWidget->bindToController();
        $model->fill($post);
        if($model->validate())
            $model->save(null, $formWidget->getSessionKey());

        $this->prepareVars();
        
        $return_path = str_replace('/backend/', '', $_SERVER['REQUEST_URI']);
        return \Backend::redirect($return_path);
        /*return [
            '#feedback_action' => $this->makePartial($this->alias, $this->vars)
        ];*/
    }

}
