<?php

namespace Twnepc\Awards\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Exception;
use Input;
use Twnepc\Awards\Models\Award;
use Twnepc\Awards\Models\AwardApplication;
use Twnepc\Awards\Models\AwardFeedback;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class OverallFeedbacks extends FormWidgetBase {

    public $alias = 'options';

    public function init()
    {

    }

    public function prepareVars()
    {

        if($this->model->feedbacks){
            $this->vars['feedbacks'] = $this->model->feedbacks;
        }else if($this->model->awardyear_id){
            $company_id = $this->model->company->company_id;
            $awardyear_id = $this->model->awardyear_id;
            $this->vars['feedbacks'] = AwardFeedback::where('company_id',$company_id)->where('awardyear_id',$awardyear_id)->get();

        }else{

            $this->vars['company'] = $this->model->company;
            if(!$this->vars['company'])
                    throw new ApplicationException("Model has to be a relation 'company'");

            $this->vars['award'] = $this->model->award;
            if(!$this->vars['award'])
                throw new ApplicationException("Model has to be a relation 'award'");

            $this->vars['year'] = $this->model->award->year;
            $this->vars['feedbacks'] = AwardFeedback::where('company_id',$this->vars['company']->company_id)->where('awardyear_id',$this->vars['year']->awardyear_id)->get();  
        }

    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial($this->alias);
    }

}
