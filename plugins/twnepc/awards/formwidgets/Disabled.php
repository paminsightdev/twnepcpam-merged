<?php

namespace Twnepc\Awards\FormWidgets;

use Backend\Classes\FormWidgetBase;
use BackendAuth;

/**
 *
 *
 */
class Disabled extends FormWidgetBase
{
    protected $defaultAlias = 'disabled';
    protected $key = null;

    public function init()
    {
        if (isset($this->config->select)) {
            $this->key = $this->config->select;
        }
    }

    public function prepareVars()
    {
        $this->vars['label'] = "";
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        if($this->key){
            $model = $this->model->{$this->formField->valueFrom};
                


            if(isset($model->{$this->key})){
                $this->vars['label'] = $model->{$this->key};
            }
        }
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial('field');
    }

}
