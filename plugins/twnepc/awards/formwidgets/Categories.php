<?php

namespace Twnepc\Awards\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Exception;
use Input;
use Twnepc\Awards\Models\Award;
use Twnepc\Awards\Models\AwardApplication;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Categories extends FormWidgetBase {

    public $alias = 'options';

    public function init()
    {

    }

    public function prepareVars()
    {
        $this->vars['company_id'] = $this->model->company_id;
        $this->vars['awards'] = [];
        $this->vars['applications'] = [];
        $openedYear = $this->model;
        if($openedYear){
            $this->vars['awards'] = Award::where('awardyear_id',$openedYear->awardyear_id)->get();
        }
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial($this->alias);
    }

    public function getAwardApplication($awardID,$companyID)
    {
        return AwardApplication::where('company_id',$companyID)->where('award_id',$awardID)->first();
    }

    public function onLoadUpdateApplication()
    {
        try {
            if (!$app = AwardApplication::find(post('application_id')))
                throw new Exception('Record not found.');

            $config = $this->makeConfig('$/twnepc/awards/models/awardapplication/history_fields.yaml');
            $config->model = $app;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
            $this->vars['application'] = $app;
            $this->vars['application_id'] = intval(post('application_id'));
            $this->vars['award'] = Award::where('award_id',intval(post('award_id')))->first();
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }
        return $this->makePartial('update_application');
    }

}
