(function($, window, document) {

    // The $ is now locally scoped

    // Listen for the jQuery ready event on the document
    $(function() {

        if($('#criteria').length > 0 ){
            $('#criteria').readmore({
                speed: 75,
                lessLink: '<a href="#">Read less</a>',
                collapsedHeight: 100
            });
        }

        jQuery('#hidershower').on('click', function(event) {        
             jQuery('#critcon').toggle();
        });

        initAwardApplicationVotesSum();

        $('#Lists').on('ajaxUpdate', function() {
            initAwardApplicationVotesSum();
        })

    });

    function initAwardApplicationVotesSum()
    {
        $("#Lists").find('.award_application_votes_sum').each(function(){
            if($(this).data('content_loaded') == 0){
                var elem = $(this);
                var appID = elem.data("application_id");
                $.request("onGetAwardApplicationVotesSum",{
                    data:{
                        'application_id':appID
                    },
                    success:function(data){
                        elem.html(data.sum);
                        elem.data('content_loaded',1);
                    },
                    complete:function(){
                        initAwardApplicationVotesSum();
                    }
                });
                return false;
            }
        });

    }


    // The rest of the code goes here!

}(window.jQuery, window, document));
// The global jQuery object is passed