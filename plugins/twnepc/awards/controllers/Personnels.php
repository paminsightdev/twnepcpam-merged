<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use Twnepc\Awards\Models\AwardPersonnel;
use BackendMenu;
use Event;
use Request;
use BackendAuth;

class Personnels extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['pamonline.awards.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'personnels');
        $this->addCss("/plugins/twnepc/pamcompanies/assets/css/custom.css", "1.0.1");
        $this->addjs("/plugins/twnepc/pamcompanies/assets/js/custom.js", "1.0.1");
    }

    public function preview($recordId = null, $context = null){
         $this->formConfig = 'config_preview.yaml';
         $model = AwardPersonnel::find($recordId);
         parent::update($recordId, $context);

         

         return $this->makePartial('print',['showHeader' => false]);
        
    }
}