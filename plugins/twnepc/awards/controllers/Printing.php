<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Twnepc\Awards\Models\AwardPersonnel;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Awards\Models\AwardSecurity;
use Twnepc\Awards\Models\AwardRegulation;
use Twnepc\Awards\Models\AwardInterest;
use Twnepc\Awards\Models\AwardData;
use Twnepc\Awards\Models\AwardApplication;
use Twnepc\Awards\Models\AwardVote;
use Twnepc\Awards\Models\AwardNote;
use Twnepc\Awards\Models\AwardFeedback;
use October\Rain\Exception\ApplicationException;
use Flash;


class Printing extends PamsController
{
    public $implement = ['Backend\Behaviors\FormController'];

    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        $this->addCss("/plugins/twnepc/awards/assets/css/print.css",['media'=>'print']);
    }

    public function preview($recordId = null, $context = null)
    {
        try {
            $this->pageTitle = "Page title";

            $this->vars['awardapplication'] = $model = $this->formFindModelObject($recordId);
            $model->emp1 = AwardPersonnel::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp2 = AwardInvestment::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp3 = AwardSecurity::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp4 = AwardRegulation::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp5 = AwardInterest::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp6 = AwardData::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->notes = AwardNote::where('company_id',$model->company_id)->where('award_id',$model->award_id)->where('user_id',$this->user->frontenduser->id)->get();
            $model->fullnotes = AwardNote::where('company_id',$model->company_id)->where('award_id',$model->award_id)->get();
            $model->feedbacks = AwardFeedback::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->get();
            $this->initForm($model);
        }
        catch (Exception $ex) {
            $this->handleError($ex);
        }
    }
}