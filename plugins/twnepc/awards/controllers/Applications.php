<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Twnepc\Awards\Models\AwardVote;

class Applications extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'applications');
        $this->addjs("/plugins/twnepc/awards/assets/js/awards.js", "1.0.0");
    }

    public function onGetAwardApplicationVotesSum()
    {
        $applicationId = post("application_id",null);
        if($applicationId){
            $sum = AwardVote::where('awardapplication_id',$applicationId)->sum('av_rank');
            return ['sum'=>$sum];
        }

    }

    public function listExtendQuery($query)
    {
        $query->orderBy('awardapplication_id', 'desc');
    }
}