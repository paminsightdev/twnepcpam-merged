<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use Backend\FormWidgets\FileUpload;
use BackendMenu;
use Event;
use Request;
use BackendAuth;

class Winners extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'winnersyears');
        
    }

    public function listExtendQuery($query)
    {
        $award_id = get('award_id',false);
        if($award_id){
            $query->where('award_id',$award_id);
        }
    }
}