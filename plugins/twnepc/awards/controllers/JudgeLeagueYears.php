<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Twnepc\Awards\Models\AwardYear;

class JudgeLeagueYears extends PamsController
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'judgeawards', 'judgeleagueyears');
    }

    public function listExtendQuery($query)
    {
        $openedYears = AwardYear::whereRaw("Date(ay_judgingends) > DATE(NOW())")->get();
        $yearsIds = [];
        foreach($openedYears as $year) {
            $yearsIds[]  = $year->awardyear_id;
        }
        $query->whereNotIn('awardyear_id', $yearsIds);
    }
}