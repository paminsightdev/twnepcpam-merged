<?php namespace Twnepc\Awards\Controllers;

use App;
use BackendMenu;
use Event;
use Illuminate\Support\Facades\Input;
use Request;
use BackendAuth;
use Backend;
use Lang;
use Twnepc\Awards\Models\AwardYear;
use Twnepc\Awards\Models\AwardData;
use Flash;
use ApplicationException;
use Session;
use Twnepc\Awards\Models\AwardPersonnel;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Awards\Models\AwardSecurity;
use Twnepc\Awards\Models\AwardRegulation;
use Twnepc\Awards\Models\AwardInterest;

class Pamsdata extends PamsController
{

    use \Twnepc\PamCompanies\Classes\PinSystemControllerTrait;

    public $implement = ['Backend\Behaviors\FormController'];

    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_pamsawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'pamsawards', 'pamsdata');
    }

    public function preview($recordId = null, $context = null){
         $this->formConfig = 'config_preview.yaml';
         if($this->user->frontenduser->pamadmin)
                $company_id = $this->user->frontenduser->pamadmin->company->company_id;
            elseif($this->user->frontenduser->member)
                $company_id = $this->user->frontenduser->member->company->company_id;
            else
                throw new ApplicationException("Hack!");
         $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->orderBy('ay_year','desc')->first();

            if($opened_year){
                $model = new AwardData();
                $model->company_id = $company_id;
                $model->awardyear_id = $opened_year->awardyear_id;
                $model->emp1 = AwardPersonnel::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
                $model->emp2 = AwardInvestment::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
//            $model->emp3 = AwardSecurity::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
//            $model->emp4 = AwardRegulation::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
//            $model->emp5 = AwardInterest::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
                $this->initForm($model);
            }else{
                return $this->makePartial('empty');
            }
         parent::update($recordId, $context);

         return $this->makePartial('print',['showHeader' => false]);
        
    }

    public function index()
    {
        try{
            $this->pageTitle = "Data updates - Year end";
            if($this->user->frontenduser->pamadmin)
                $company_id = $this->user->frontenduser->pamadmin->company->company_id;
            elseif($this->user->frontenduser->member)
                $company_id = $this->user->frontenduser->member->company->company_id;
            else
                throw new ApplicationException("Hack!");

            $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->orderBy('ay_year','desc')->first();

            if($opened_year){
                $model = new AwardData();
                $model->company_id = $company_id;
                $model->awardyear_id = $opened_year->awardyear_id;
                $model->emp1 = AwardPersonnel::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
                $model->emp2 = AwardInvestment::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
//            $model->emp3 = AwardSecurity::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
//            $model->emp4 = AwardRegulation::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
//            $model->emp5 = AwardInterest::where('company_id',$company_id)->where('awardyear_id',$opened_year->awardyear_id)->first();
                $this->initForm($model);
            }else{
                return $this->makePartial('empty');
            }
        }catch(ApplicationException $e){
            Flash::error($e->getMessage());
        }
    }

    public function index_onSave()
    {

        try{
            if(!Input::has('AwardData'))
                throw new ApplicationException("Not provided AwardData.");

            if(!Input::has('AwardData.company_id'))
                throw new ApplicationException("Not provided company.");

            if(!Input::has('AwardData.awardyear_id'))
                throw new ApplicationException("Not provided award id.");

            if(!Input::has('AwardData.emp1'))
                throw new ApplicationException("Personnel data not provided.");

            if(!Input::has('AwardData.emp2'))
                throw new ApplicationException("Investment data not provided.");

            $emp1 = AwardPersonnel::where('company_id',Input::get('AwardData.company_id'))->where('awardyear_id',Input::get('AwardData.awardyear_id'))->first();
            $emp1->fill(Input::get('AwardData.emp1'));
            if($emp1->validate())
                    $emp1->save();
            $emp2 = AwardInvestment::where('company_id',Input::get('AwardData.company_id'))->where('awardyear_id',Input::get('AwardData.awardyear_id'))->first();
            $emp2->fill(Input::get('AwardData.emp2'));
            if($emp2->validate())
                    $emp2->save();

            return [
                '#layout-flash-messages' => Flash::success("End year data successfully updated.")
            ];
        }catch(ApplicationException $e){
            Flash::error($e->getMessage());
        }
    }

    public function create($context = null)
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryharddeadline) > DATE(NOW())")->orderBy('ay_year','desc')->first();

        if(!$opened_year)
            return Backend::redirect("twnepc/awards/pamsdata/");

        $currentYear = (int) date("Y");

        $emp = AwardData::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$company_id)->first();

        if($emp)
            return Backend::redirect("twnepc/awards/pamsdata/update/{$emp->awardemp6_id}");

        return parent::create($context);
    }

    public function create_onSave($context = null)
    {
        //TODO ??? What this die() do ???
        die();
        $className = substr(strrchr(get_class($this->formCreateModelObject()), "\\"), 1);
        if($this->user->frontenduser->pamadmin)
            $_POST[$className]['company_id'] = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $_POST[$className]['company_id'] = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryharddeadline) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$opened_year)
            throw new ApplicationException("Hack!");

        $_POST[$className]['awardyear_id'] = $opened_year->awardyear_id;
        Flash::success('Saved successfully');
        $result = parent::create_onSave($context);
        return $result;
    }


    public function update($id)
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryharddeadline) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$opened_year)
            return Backend::redirect("twnepc/awards/pamsdata/");

        $emp = AwardData::where('awardemp6_id',$id)->where('company_id',$company_id)->first();
        if(!$emp)
            return Backend::redirect("twnepc/awards/pamsdata/");

        if($emp->aemp6_award == 1){
            $this->vars['submitted'] = true;
            //$this->config->form = "$/twnepc/awards/models/awardpersonnel/submitted_pams_fields.yaml";
        }

        return parent::update($id);
    }

    public function onSingleEmpSubmitToAwards()
    {
        try{
            parent::singleEmpSubmitToAwards(new AwardData);
            return [
                '#layout-flash-messages' => Flash::success("Applications successfully submitted.")
            ];
        }catch(ApplicationException $e){
            Flash::error($e->getMessage());
        }
    }

    public function isAwardOpen()
    {
        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear)
                return true;

        return false;
    }

    public function onSubmitToAwards()
    {
        try{
            parent::onSubmitToAwards();
            return [
                '#layout-flash-messages' => Flash::success("Applications successfully submitted.")
            ];
        }catch(ApplicationException $e){
            Flash::error($e->getMessage());
        }
    }
}