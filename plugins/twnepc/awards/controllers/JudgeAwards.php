<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Twnepc\Awards\Models\Award;
use Twnepc\Awards\Models\AwardYear;
use Session;

class JudgeAwards extends PamsController
{
    use \Twnepc\PamCompanies\Classes\Notificator;
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'judgeawards', 'judgeawards');
        $this->addjs("/plugins/twnepc/pamcompanies/assets/js/DisplaySize.js", "1.0.0");
    }

    public function index()
    {
        Session::put('openaward', true);
        $this->pageTitle = 'Judges section of the website';
        return parent::index();
    }

    public function listExtendQuery($query)
    {
        if(get('awardyear_id',false)){
            $openedYear = get('awardyear_id');
            $query->where('awardyear_id',$openedYear);
        }else{
            $openedYears = AwardYear::whereRaw("Date(ay_judgingdate) < DATE(NOW())")->whereRaw("Date(ay_judgingends) > DATE(NOW())")->get();
            $yearsIds = [];
            foreach($openedYears as $year) {
                $yearsIds[] = $year->awardyear_id;
            }
            $query->whereIn('awardyear_id', $yearsIds);
        }

        $query->orderBy('award_ordernumber',"ASC");
    }
}