<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use League\Flysystem\Exception;
use Request;
use BackendAuth;
use Twnepc\Awards\Models\AwardYear;
use Twnepc\Awards\Models\Award;
use Twnepc\Awards\Models\AwardApplication;
use Illuminate\Http\Exception as HTTPException;
use Flash;
use ApplicationException;
use Session;

class PamsCategories extends PamsController
{
    use \Twnepc\PamCompanies\Classes\PinSystemControllerTrait;
    public $requiredPermissions = ['pamonline.awards.access_pamsawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'pamsawards', 'pamscategories');
    }

    public function prepareVars()
    {
        if($this->user->frontenduser->pamadmin)
            $this->vars['company_id'] = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $this->vars['company_id'] = $this->user->frontenduser->member->company->company_id;
        else
            throw new HTTPException("Error");
        $this->vars['awards'] = [];
        $this->vars['applications'] = [];
        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $this->vars['awards'] = Award::where('awardyear_id',$openedYear->awardyear_id)->get();
        }
    }

    public function index()
    {
        try{
            $this->uploadHandlerForAwardApplication();
            $this->pageTitle = "Awards Categories";
            $this->prepareVars();
            Session::put('openaward', true);
        }catch(Exception $e){

        }
    }

    public function uploadHandlerForAwardApplication()
    {
        $config = $this->makeConfig('$/twnepc/awards/models/awardapplication/add_fields.yaml');
        $config->model = new AwardApplication;
        $config->context = "update";
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
        $formWidget->render();
    }

    public function onLoadUpdateApplication()
    {
        try {
            if (!$app = AwardApplication::find(post('application_id')))
                throw new Exception('Record not found.');

            $config = $this->makeConfig('$/twnepc/awards/models/awardapplication/update_fields.yaml');
            $config->model = $app;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
            $this->vars['application_id'] = intval(post('application_id'));
            $this->vars['award'] = Award::where('award_id',intval(post('award_id')))->first();
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }
        return $this->makePartial('update_application');
    }

    public function onUpdateApplication()
    {
        $post = post();
        if (!isset($post['application_id'])) {
            throw new Exception('ID must be specified.');
        }
        $id = intval($post['application_id']);

        $app = AwardApplication::where('awardapplication_id',$id)->first();

        if (!$app)
            throw new Exception('Record not found.');

        $config = $this->makeConfig('$/twnepc/awards/models/awardapplication/update_fields.yaml');
        $config->model = $app;
        $config->context = 'update';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $app->fill($post);
        $app->save(null, $formWidget->getSessionKey());

        $this->prepareVars();

        return [
            '#list-categories' => $this->makePartial('list_categories', $this->vars),
            'link' => $this->getLinkedAwardsCategoryResponseData(post('award_id'),post('company_id'))
        ];
    }

    public function onLoadAddApplication()
    {
        try {
            $config = $this->makeConfig('$/twnepc/awards/models/awardapplication/add_fields.yaml');
            $config->model = new AwardApplication();
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
            $this->vars['award_id'] = intval(post('award_id'));
            $this->vars['award'] = Award::where('award_id',intval(post('award_id')))->first();
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }
        return $this->makePartial('add_application');
    }

    public function onAddApplication()
    {
        $post = post();
        $app = new AwardApplication();
        $config = $this->makeConfig('$/twnepc/awards/models/awardapplication/add_fields.yaml');
        $config->model = $app;
        $config->context = 'create';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
        $post['aa_applied'] = 1;
        $app->fill($post);
        $app->save(null, $formWidget->getSessionKey());

        $this->prepareVars();
        return [
            '#list-categories' => $this->makePartial('list_categories', $this->vars),
            'link' => $this->getLinkedAwardsCategoryResponseData(post('award_id'),post('company_id'))
        ];
    }

    public function onSubmitToAwards()
    {
        try{
            parent::onSubmitToAwards();
            $this->prepareVars();
            return [
                '#list-categories' => $this->makePartial('list_categories', $this->vars),
                '#layout-flash-messages' => Flash::success("Applications successfully submitted.")
            ];
        }catch(ApplicationException $e){
            $this->prepareVars();
            return [
                '#list-categories' => $this->makePartial('list_categories', $this->vars),
                '#layout-flash-messages' => Flash::error($e->getMessage())
            ];
        }
    }

    private function getLinkedAwardsCategoryResponseData($parent_award_id, $company_id)
    {
        $award = Award::where('award_id',$parent_award_id)->first();
        $linkData = [];
        if(count($award->links) > 0){

            foreach($award->links as $link){
                $awardApp = AwardApplication::where('award_id',$link->award_id)->where('company_id',$company_id)->first();
                if(!$awardApp){
                    $linkData = [
                        'company_id' => $company_id,
                        'award_id' => $link->award_id,
                    ];
                    break;
                }
            }
        }
        return $linkData;
    }
}