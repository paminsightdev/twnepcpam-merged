<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use ApplicationException;
use Flash;
use Twnepc\Awards\Models\AwardPersonnel;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Awards\Models\AwardSecurity;
use Twnepc\Awards\Models\AwardRegulation;
use Twnepc\Awards\Models\AwardInterest;
use Twnepc\Awards\Models\AwardData;
use Twnepc\Awards\Models\AwardYear;
use Twnepc\Awards\Models\Award;
use Twnepc\Awards\Models\AwardApplication;
use Illuminate\Database\Eloquent\Collection;

class PamsController extends Controller
{
    use \Twnepc\PamCompanies\Classes\Notificator;
    public function __construct()
    {
        parent::__construct();
        $this->addjs("/plugins/twnepc/pamcompanies/assets/js/DisplaySize.js", "1.0.0");
        $this->addjs("/plugins/twnepc/awards/assets/js/awards.js", "1.0.0");
    }

    public function prepareVars()
    {
        if($this->user->frontenduser->pamadmin)
            $this->vars['company_id'] = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $this->vars['company_id'] = $this->user->frontenduser->member->company->company_id;
        else
            throw new HTTPException("Error");
        $this->vars['awards'] = [];
        $this->vars['applications'] = [];
        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if($openedYear){
            $this->vars['awards'] = Award::where('awardyear_id',$openedYear->awardyear_id)->get();
        }
    }

    public function onLoadSubmitToAwards()
    {
        $this->user->frontenduser->checkPIN();
        $this->prepareVars();
        return $this->makePartial('$/twnepc/awards/controllers/pamscategories/_submit_to_awards.htm',$this->vars);
    }

    public function singleEmpSubmitToAwards($emp)
    {
        $this->user->frontenduser->checkPIN();

        if($this->user->frontenduser->pamadmin)
            $companyID = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $companyID = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("You must be PAM user.");

        if(!$companyID)
            throw new ApplicationException("You must own company.");

        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryharddeadline) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$openedYear)
            throw new ApplicationException("There is no open awards.");

        $aemp6 = $emp::where('company_id',$companyID)->where('awardyear_id',$openedYear->awardyear_id)->first();
        $aemp6->aemp6_award = 1;
        $aemp6->save();
    }

    public function onSubmitToAwards()
    {
        $this->user->frontenduser->checkPIN();

        if($this->user->frontenduser->pamadmin)
            $companyID = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $companyID = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("You must be PAM user.");

        if(!$companyID)
            throw new ApplicationException("You must own company.");

        $openedYear = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$openedYear)
            throw new ApplicationException("There is no open awards.");

        $applicationIds = post('application_ids',array());

        if(count($applicationIds) < 1)
            throw new ApplicationException("You must select at least one application to submit.");

        $apps = AwardApplication::whereIn('awardapplication_id', $applicationIds)->get();
        //$apps = AwardApplication::where('company_id',$companyID)->where('aa_submitted',0)->get();

        foreach($apps as $app){
            $linkedAward = $this->getLinkedAwardsCategory($app->award_id,$companyID);
            if(count($linkedAward) > 0){
                throw new ApplicationException("You must entry '{$linkedAward->award_name}' award");
            }
        }

        //Submit application which not required EMP
        foreach($apps as $app){
            $award = Award::where('award_id',$app->award_id)->first();
            if($award->award_emprequired != 1){
                if($app->aa_reason == "")$app->aa_reason = "Empty!";
                $app->aa_submitted = 1;
                $app->aa_applied = 1;
                $app->save();
            }
        }

        $aemp1 = AwardPersonnel::where('company_id',$companyID)->where('awardyear_id',$openedYear->awardyear_id)->first();
        $aemp2 = AwardInvestment::where('company_id',$companyID)->where('awardyear_id',$openedYear->awardyear_id)->first();
        $aemp3 = AwardSecurity::where('company_id',$companyID)->where('awardyear_id',$openedYear->awardyear_id)->first();
        $aemp4 = AwardRegulation::where('company_id',$companyID)->where('awardyear_id',$openedYear->awardyear_id)->first();
        $aemp5 = AwardInterest::where('company_id',$companyID)->where('awardyear_id',$openedYear->awardyear_id)->first();
        $aemp6 = AwardData::where('company_id',$companyID)->where('awardyear_id',$openedYear->awardyear_id)->first();

        $isEMPrequired = false;
        //Try to Submit application which require EMP
        foreach($apps as $app){
            $award = Award::where('award_id',$app->award_id)->first();
            if($award->award_emprequired == 1){

                $exceptionMessage = "You are required to submit the Extended Manager Profile to enter these categories. Please complete and save";
                $throwException = false;


                $isEMPrequired = true;
                if(!$aemp1){
                    $throwException = true;
                    $exceptionMessage .= " 'Group Structure And Personnel',";
                }

                if(!$aemp2){
                    $throwException = true;
                    $exceptionMessage .= " 'Investment Management Process',";
                }

                if(!$aemp3){
                    $throwException = true;
                    $exceptionMessage .= " 'Pooled Funds/Unlisted Securities',";
                }

                if(!$aemp4){
                    $throwException = true;
                    $exceptionMessage .= " 'Regulation And Investor Protection',";
                }

                if(!$aemp5){
                    $throwException = true;
                    $exceptionMessage .= " 'Fees, Charges And Interest'";
                }

//                if(!$aemp6)
//                    throw new ApplicationException("You entry Award's which required Extended Manager Profile. You must entry 'New data updates - year end'.");

                $exceptionMessage = rtrim($exceptionMessage,",").".";

                if($throwException)
                    throw new ApplicationException($exceptionMessage);

                if($app->aa_reason == "")$app->aa_reason = "Empty!";
                $app->aa_submitted = 1;
                $app->aa_applied = 1;
                $app->save();
            }
        }

        if($isEMPrequired){
            $aemp1->aemp1_award = 1;
            $aemp1->save();

            $aemp2->aemp2_award = 1;
            $aemp2->save();

            $aemp3->aemp3_award = 1;
            $aemp3->save();

            $aemp4->aemp4_award = 1;
            $aemp4->save();

            $aemp5->aemp5_award = 1;
            $aemp5->save();

//            $aemp6->aemp6_award = 1;
//            $aemp6->save();
        }
    }

    public function getAwardApplication($awardID,$companyID)
    {
        return AwardApplication::where('company_id',$companyID)->where('award_id',$awardID)->first();
    }

    private function getLinkedAwardsCategory($parent_award_id, $company_id)
    {
        $award = Award::where('award_id',$parent_award_id)->first();
        $linkAward = null;
        if(count($award->links) > 0){

            foreach($award->links as $link){
                $awardApp = AwardApplication::where('award_id',$link->award_id)->where('company_id',$company_id)->first();
                if(!$awardApp){
                    $linkAward = $link;
                    break;
                }
            }
        }
        return $linkAward;
    }

    public function parse_html($html, $args)
    {
        foreach($args as $key => $val) $html = str_replace("%[$key]", $val, $html);
        return $html;
    }

    protected static function countVotes(Collection $votes)
    {
        $counter = 0;
        foreach($votes as $vote){
            if($vote->av_rank > 0)
                $counter++;
        }
        return $counter;
    }
}