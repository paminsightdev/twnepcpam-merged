<?php namespace Twnepc\Awards\Controllers;

use App;
use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Backend;
use Lang;
use Twnepc\Awards\Models\AwardYear;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Pamcompanies\Models\CompanyKcd;
use Flash;
use ApplicationException;
use Session;

class PamsInvestments extends PamsController
{
    use \Twnepc\PamCompanies\Classes\PinSystemControllerTrait;

    public $implement = ['Backend\Behaviors\FormController'];

    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_pamsawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'pamsawards', 'pamsinvestments');
    }

    public function preview($recordId = null, $context = null){
         $this->formConfig = 'config_preview.yaml';
         $model = AwardInvestment::find($recordId);
         parent::update($recordId, $context);

         

         return $this->makePartial('print',['showHeader' => false]);
        
    }

    public function index()
    {
        $this->pageTitle = "Investment Management Process";
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");
        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();

        if($opened_year){
            $emp = AwardInvestment::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$company_id)->first();
            if($emp){
                return Backend::redirect("twnepc/awards/pamsinvestments/update/{$emp->awardemp2_id}");
            }else{
                return Backend::redirect("twnepc/awards/pamsinvestments/create");
            }
        }
    }

    public function create($context = null)
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();

        if(!$opened_year)
            return Backend::redirect("twnepc/awards/pamsinvestments/");

        $emp = AwardInvestment::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$company_id)->first();

        if($emp)
            return Backend::redirect("twnepc/awards/pamsinvestments/update/{$emp->awardemp2_id}");

        $prevYearEmp = AwardInvestment::where('company_id',$company_id)->first();

        if($prevYearEmp)
            $model = $prevYearEmp;
        else
            $model = $this->formCreateModelObject();

        $model->company_id = $company_id;
        $model->company();

        $kcdModel = CompanyKcd::where('company_id',$company_id)->orderBy('companykcd_id', 'desc')->first();

        if($kcdModel){
            $model->aemp2_discretionary1 = $kcdModel->ckcd_discsplit;
            if($kcdModel->ckcd_discsplit < 100){
                $model->aemp2_advisory1 = 100-$kcdModel->ckcd_discsplit;
            }elseif($kcdModel->ckcd_discsplit > 100){
                $model->aemp2_advisory1 = ($kcdModel->ckcd_discsplit-100)*-1;
            }else{
                $model->aemp2_advisory1 = 0;
            }
        }
        $this->pageTitle = "Investment Management Process";
        $this->initForm($model,'create');
    }

    public function create_onSave($context = null)
    {
        $className = substr(strrchr(get_class($this->formCreateModelObject()), "\\"), 1);
        if($this->user->frontenduser->pamadmin)
            $_POST[$className]['company_id'] = $companyId = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $_POST[$className]['company_id'] = $companyId = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$opened_year)
            throw new ApplicationException("Hack!");

        $_POST[$className]['awardyear_id'] = $opened_year->awardyear_id;

        Flash::success('Saved successfully');
        $result = parent::create_onSave($context);

        $discretionary = $_POST[$className]['aemp2_discretionary1'];
        $advisory = $_POST[$className]['aemp2_advisory1'];

        $kcdModel = CompanyKcd::where('company_id',$companyId)->orderBy('companykcd_id', 'desc')->first();
        if($kcdModel && $kcdModel->getAvailableYear() == 0){
            $kcdModel->resetValidationRules();
            $kcdModel->ckcd_discsplit = $discretionary;
            $kcdModel->save();
        }else{
            $kcdModel = new CompanyKcd;
            $kcdModel->company_id = $companyId;
            $kcdModel->ckcd_year = $kcdModel->getAvailableYear();
            $kcdModel->ckcd_discsplit = $discretionary;
            $kcdModel->ckcd_clientassets = 0;
            $kcdModel->ckcd_clientadvice = 0;
            $kcdModel->ckcd_staffemployed= 0;
            $kcdModel->ckcd_numstaff     = 0;
            $kcdModel->ckcd_revenue      = 0;
            $kcdModel->ckcd_numclients   = 0;
            $kcdModel->ckcd_profit       = 0;
            $kcdModel->save();
        }
        return $result;
    }

    public function formBeforeCreate($model)
    {
        $model->loadCompanyRelation();
    }

    public function update($id)
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $this->pageTitle = "Investment Management Process";
        $model = $this->formFindModelObject($id);

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$opened_year)
            return Backend::redirect("twnepc/awards/pamsinvestments/");

        $emp = AwardInvestment::where('awardemp2_id',$id)->where('company_id',$company_id)->first();
        if(!$emp)
            return Backend::redirect("twnepc/awards/pamsinvestments/");

        $kcdModel = CompanyKcd::where('company_id',$model->company_id)->orderBy('companykcd_id', 'desc')->first();

        if($kcdModel){
            $model->aemp2_discretionary1 = $kcdModel->ckcd_discsplit;
            if($kcdModel->ckcd_discsplit < 100){
                $model->aemp2_advisory1 = 100-$kcdModel->ckcd_discsplit;
            }elseif($kcdModel->ckcd_discsplit > 100){
                $model->aemp2_advisory1 = ($kcdModel->ckcd_discsplit-100)*-1;
            }else{
                $model->aemp2_advisory1 = 0;
            }
        }

        if($emp->aemp2_award == 1){
            $this->vars['submitted'] = true;
            //$this->config->form = "$/twnepc/awards/models/awardpersonnel/submitted_pams_fields.yaml";
        }
        $this->initForm($model,'update');
        //return parent::update($id);
    }

    public function update_onSave($recordId = null, $context = null)
    {
        $this->user->frontenduser->checkPIN();

        $model = $this->formFindModelObject($recordId);

        $result = parent::update_onSave($recordId,$context);

        $className = substr(strrchr(get_class($this->formCreateModelObject()), "\\"), 1);

        $discretionary = $_POST[$className]['aemp2_discretionary1'];
        $advisory = $_POST[$className]['aemp2_advisory1'];

        $kcdModel = CompanyKcd::where('company_id',$model->company_id)->orderBy('companykcd_id', 'desc')->first();
        if($kcdModel && $kcdModel->getAvailableYear() == 0){
            $kcdModel->resetValidationRules();
            $kcdModel->ckcd_discsplit = $discretionary;
            $kcdModel->save();
        }else{
            $kcdModel = new CompanyKcd;
            $kcdModel->company_id = $model->company_id;
            $kcdModel->ckcd_year = $kcdModel->getAvailableYear();
            $kcdModel->ckcd_discsplit = $discretionary;
            $kcdModel->ckcd_clientassets = 0;
            $kcdModel->ckcd_clientadvice = 0;
            $kcdModel->ckcd_staffemployed= 0;
            $kcdModel->ckcd_numstaff     = 0;
            $kcdModel->ckcd_revenue      = 0;
            $kcdModel->ckcd_numclients   = 0;
            $kcdModel->ckcd_profit       = 0;
            $kcdModel->save();
        }

        return $result;
    }

    public function onSubmitToAwards()
    {
        try{
            parent::onSubmitToAwards();
            return [
                '#layout-flash-messages' => Flash::success("Applications successfully submitted.")
            ];
        }catch(ApplicationException $e){
            Flash::error($e->getMessage());
        }
    }
}