<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Twnepc\Awards\Models\Award;
use Twnepc\Awards\Models\AwardYear;
use Redirect;

class JudgeLeague extends PamsController
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'judgeawards', 'judgeleagueyears');
    }

    public function index()
    {
        if(get('awardyear_id',false) == false)
                return Redirect::to("backend/twnepc/awards/judgeleagueyears");
        return parent::index();
    }

    public function listExtendQuery($query)
    {
        $openedYear = AwardYear::whereRaw("Date(ay_judgingends) > DATE(NOW())")->first();
        if(get('awardyear_id',false)){
            $year = get('awardyear_id');
            if(isset($openedYear->awardyear_id) && $openedYear->awardyear_id == $year)
                    die();
            $query->where('awardyear_id',$year);
        }
        $query->orderBy('award_ordernumber',"ASC");
    }
}