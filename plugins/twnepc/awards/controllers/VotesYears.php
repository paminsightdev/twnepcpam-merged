<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;

class VotesYears extends PamsController
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'leagueyears');
    }
}