<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Twnepc\Awards\Models\Award;
use Twnepc\Awards\Models\AwardYear;

class League extends PamsController
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'leagueyears');
    }

    public function listExtendQuery($query)
    {
        $openedYear = AwardYear::whereRaw("Date(ay_winnerdate) > DATE(NOW())")->first();
        if(get('awardyear_id',false)){
            $year = get('awardyear_id');
            $query->where('awardyear_id',$year);
        }else{
            $query->where('awardyear_id',$openedYear->awardyear_id);
        }
        $query->orderBy('award_ordernumber',"ASC");
    }
}