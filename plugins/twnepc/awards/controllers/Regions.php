<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Event;
use Request;
use BackendAuth;

class Regions extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['pamonline.awards.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards','regions');
    }
}