<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Backend\Classes\Controller;
use Twnepc\Awards\Models\AwardYear;

class WinnersAwards extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'winnersyears');
        
         
        
    }

    public function listExtendQuery($query)
    {
        $openedYear = AwardYear::whereRaw("Date(ay_winnerdate) > DATE(NOW())")->first();
        if(get('awardyear_id',false)){
            $year = get('awardyear_id');
            $query->where('awardyear_id',$year);
        }
        $query->orderBy('award_ordernumber',"ASC");
    }
}