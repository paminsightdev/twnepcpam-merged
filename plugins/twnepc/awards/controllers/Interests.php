<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Twnepc\Awards\Models\AwardInterest;

class Interests extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['pamonline.awards.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'interests');
    }

    public function preview($recordId = null, $context = null){
         $this->formConfig = 'config_preview.yaml';
         $model = AwardInterest::find($recordId);
         parent::update($recordId, $context);

         

         return $this->makePartial('print',['showHeader' => false]);
        
    }
}