<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Twnepc\Awards\Models\AwardYear;

class JudgeAwardsYears extends Controller
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'judgeawards','judgeawardsyears');
    }

    public function listExtendQuery($query)
    {
        $openedYears = AwardYear::whereRaw("Date(ay_winnerdate) > DATE(NOW())")->get();
        $yearsIds = [];
        foreach($openedYears as $year) {
            $yearsIds[]  = $year->awardyear_id;
        }
        $query->whereNotIn('awardyear_id', $yearsIds);
    }
}