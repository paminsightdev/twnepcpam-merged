<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Twnepc\PamCompanies\Models\Companies;
use Twnepc\Awards\Models\AwardPersonnel;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Awards\Models\AwardSecurity;
use Twnepc\Awards\Models\AwardRegulation;
use Twnepc\Awards\Models\AwardInterest;
use Twnepc\Awards\Models\AwardData;
use Twnepc\Awards\Models\AwardApplication;
use Twnepc\Awards\Models\AwardVote;
use Twnepc\Awards\Models\AwardNote;
use Twnepc\Awards\Models\AwardFeedback;
use October\Rain\Exception\ApplicationException;
use Flash;

class JudgeApplications extends PamsController
{

    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'judgeawards', 'judgeawards');
        $this->addjs("/plugins/twnepc/awards/assets/js/jquery.printPage.js", "1.0.0");
        $this->addjs("/plugins/twnepc/awards/assets/js/readmore.min.js", "1.0.0");
        $this->addjs("/plugins/twnepc/awards/assets/js/awards.js", "1.0.0");
        //$this->addCss("/plugins/twnepc/awards/assets/css/awards.css");
    }

    public function canJudgeVote($appID,$debug = false)
    {
        $awardApp = AwardApplication::where('awardapplication_id',$appID)->first();

        if($awardApp->award->award_canjudgevote == 0)
                return false;

        $startDate = strtotime($awardApp->award->year->ay_judgingdate);
        $endDate = strtotime($awardApp->award->year->ay_judgingends);
        $now = time();
        if($debug) {
            dump(date('m/d/Y H:i:s', $startDate));
            dump(date('m/d/Y H:i:s', $endDate));
            dump(date('m/d/Y H:i:s', $now));
        }
        if($startDate < $now && $now < $endDate){
            return true;
        }
        return false;
    }

    public function isVoted($appID)
    {
        $judgeID = $this->user->frontenduser->id;
        $vote = AwardVote::where('awardapplication_id',$appID)->where('user_id',$judgeID)->first();
        if($vote)
            return true;
        return false;
    }

    public function onLoadVoteForm()
    {
        $voteTpl = 'vote_application';
        try {
            if (!$app = AwardApplication::find(post('application_id')))
                throw new Exception('Record not found.');

            $judgeID = $this->user->frontenduser->id;
            $vote = AwardVote::where('awardapplication_id',$app->awardapplication_id)->where('user_id',$judgeID)->first();
            if($vote)
                $voteTpl = 'preview_vote_application';

            $config = $this->makeConfig('$/twnepc/awards/models/awardvote/vote_fields.yaml');
            $config->model = $vote ? $vote : new AwardVote;
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['application_id'] = intval(post('application_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }
        return $this->makePartial($voteTpl);
    }

    public function onVoteApplication()
    {
        $post = post();

        if (!isset($post['awardapplication_id'])) {
            throw new ApplicationException('ID must be specified.');
        }
        $id = intval($post['awardapplication_id']);

        $app = AwardApplication::where('awardapplication_id',$id)->first();

        if (!$app)
            throw new ApplicationException('Record not found.');

        $judgeID = $this->user->frontenduser->id;
        $vote = new AwardVote;
        $post['user_id'] = $judgeID;

        $config = $this->makeConfig('$/twnepc/awards/models/awardvote/vote_fields.yaml');
        $config->model = $vote;
        $config->context = 'create';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $vote->fill($post);
        $vote->save(null, $formWidget->getSessionKey());

        Flash::success("Applications Successfully voted.");
        return [
            'voteValue' => $post['av_rank'],
            'awardapplicationId' => $post['awardapplication_id']
        ];
//        $this->prepareVars();
//
//        return [
//            '#list-categories' => $this->makePartial('list_categories', $this->vars),
//            'link' => $this->getLinkedAwardsCategoryResponseData(post('award_id'),post('company_id'))
//        ];
    }

    public function preview($recordId = null, $context = null)
    {
        try {
            $model = $this->formFindModelObject($recordId);



            $this->vars['awardapplication'] = $model;
            $this->vars['company'] = Companies::where('company_id',$model->company_id)->first();

            $this->vars['awardapplications'] = [];
            $awardApplications = AwardApplication::where('award_id',$model->award_id)
                ->where('aa_showtojudge',1)
                ->where('aa_submitted',1)
                ->where('aa_applied',1)
                ->get();

            foreach($awardApplications as $app){
                $this->vars['awardapplications'][$app->company->c_companyname] = $app->awardapplication_id;
            }

            ksort($this->vars['awardapplications']);

            $model->company     = $this->vars['company'];
            $model->emp1        = AwardPersonnel::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp2        = AwardInvestment::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp3        = AwardSecurity::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp4        = AwardRegulation::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp5        = AwardInterest::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->emp6        = AwardData::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->first();
            $model->notes       = AwardNote::where('company_id',$model->company_id)->where('award_id',$model->award_id)->where('user_id',$this->user->frontenduser->id)->orderBy('pam_modified','DESC')->get();
            $model->fullnotes   = AwardNote::where('company_id',$model->company_id)->where('award_id',$model->award_id)->get();
            $model->feedbacks   = AwardFeedback::where('company_id',$model->company_id)->where('awardyear_id',$model->award->awardyear_id)->get();

            if(!$model->emp1) $model->emp1 = new AwardPersonnel();
            if(!$model->emp2) $model->emp2 = new AwardInvestment();
            if(!$model->emp3) $model->emp3 = new AwardSecurity();
            if(!$model->emp4) $model->emp4 = new AwardRegulation();
            if(!$model->emp5) $model->emp5 = new AwardInterest();
            if(!$model->emp6) $model->emp6 = new AwardData();

            $model->emp4->setCompanyRelation($this->vars['company']);

            $this->initForm($model);

            $this->pageTitle = $model->award->award_name;
        }
        catch (Exception $ex) {
            $this->handleError($ex);
        }
    }

    public function formExtendQuery($query)
    {
        $query->where('aa_showtojudge',1);
        $query->where('aa_submitted',1);
        $query->where('aa_applied',1);
    }

    public function listExtendQuery($query)
    {
        $award_id = get('award_id',false);
        if($award_id){
            $query->where('award_id',$award_id);
        }
        $query->where('aa_showtojudge',1);
        $query->where('aa_submitted',1);
        $query->where('aa_applied',1);
    }
}