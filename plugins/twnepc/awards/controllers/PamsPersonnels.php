<?php namespace Twnepc\Awards\Controllers;

use App;
use Backend\Classes\Controller;
use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Backend;
use Lang;
use Twnepc\Awards\Models\AwardYear;
use Twnepc\Awards\Models\AwardPersonnel;
use Flash;
use ApplicationException;
use Session;

class PamsPersonnels extends PamsController
{

    use \Twnepc\PamCompanies\Classes\PinSystemControllerTrait;

    public $implement = ['Backend\Behaviors\FormController'];

    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_pamsawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'pamsawards', 'pamspersonnels');
    }


    public function preview($recordId = null, $context = null){
         $this->formConfig = 'config_preview.yaml';
         $model = AwardPersonnel::find($recordId);
         parent::update($recordId, $context);

         

         return $this->makePartial('print',['showHeader' => false]);
        
    }

    public function index()
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");
        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        $this->pageTitle = "Group structure and personnel";
        if($opened_year){
            $award_personnel = AwardPersonnel::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$company_id)->first();
            if($award_personnel){
                return Backend::redirect("twnepc/awards/pamspersonnels/update/{$award_personnel->awardemp1_id}");
            }else{
                return Backend::redirect("twnepc/awards/pamspersonnels/create");
            }
        }
    }

    public function create($context = null)
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");
        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();

        if(!$opened_year)
            return Backend::redirect("twnepc/awards/pamspersonnels/");

        $award_personnel = AwardPersonnel::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$company_id)->first();

        if($award_personnel)
            return Backend::redirect("twnepc/awards/pamspersonnels/update/{$award_personnel->awardemp1_id}");

        $prevYearEmp = AwardPersonnel::where('company_id',$company_id)->first();

        if($prevYearEmp)
            $model = $prevYearEmp;
        else
            $model = $this->formCreateModelObject();
        $this->pageTitle = "Group structure and personnel";
        $this->initForm($model,'create');
    }

    public function create_onSave($context = null)
    {
        $className = substr(strrchr(get_class($this->formCreateModelObject()), "\\"), 1);
        if($this->user->frontenduser->pamadmin)
            $_POST[$className]['company_id'] = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $_POST[$className]['company_id'] = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$opened_year)
            throw new ApplicationException("Hack!");

        $_POST[$className]['awardyear_id'] = $opened_year->awardyear_id;
        Flash::success('Saved successfully');
        $result = parent::create_onSave($context);
        return $result;
    }


    public function update($id)
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();

        if(!$opened_year)
            return Backend::redirect("twnepc/awards/pamspersonnels");

        $emp = AwardPersonnel::where('awardemp1_id',$id)->where('company_id',$company_id)->first();
        if(!$emp)
            return Backend::redirect("twnepc/awards/pamspersonnels");

        if($emp->aemp1_award == 1){
            $this->vars['submitted'] = true;
            //$this->config->form = "$/twnepc/awards/models/awardpersonnel/submitted_pams_fields.yaml";
        }

        return parent::update($id);
    }

    public function onSubmitToAwards()
    {
        try{
            parent::onSubmitToAwards();
            return [
                '#layout-flash-messages' => Flash::success("Applications successfully submitted.")
            ];
        }catch(ApplicationException $e){
            Flash::error($e->getMessage());
        }
    }
}