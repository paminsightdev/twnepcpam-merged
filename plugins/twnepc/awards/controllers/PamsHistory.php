<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Twnepc\Awards\Models\AwardPersonnel;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Awards\Models\AwardSecurity;
use Twnepc\Awards\Models\AwardRegulation;
use Twnepc\Awards\Models\AwardInterest;
use Twnepc\Awards\Models\AwardData;
use Twnepc\Awards\Models\AwardYear;

class PamsHistory extends PamsController
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $requiredPermissions = ['pamonline.awards.access_pamsawards'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'pamsawards', 'pamshistory');
    }

    public function preview($recordId = null, $context = null)
    {
        $company = null;
        if($this->user->frontenduser->pamadmin)
            $company = $this->user->frontenduser->pamadmin->company;
        elseif($this->user->frontenduser->member)
            $company = $this->user->frontenduser->member->company;
        else
            throw new ApplicationException("Hack!");

        $model = $this->formFindModelObject($recordId);

        $model->company = $company;
        $model->company_id = $company->company_id;
        $model->emp1 = AwardPersonnel::where('company_id',$company->company_id)->where('awardyear_id',$recordId)->first();
        $model->emp2 = AwardInvestment::where('company_id',$company->company_id)->where('awardyear_id',$recordId)->first();
        $model->emp3 = AwardSecurity::where('company_id',$company->company_id)->where('awardyear_id',$recordId)->first();
        $model->emp4 = AwardRegulation::where('company_id',$company->company_id)->where('awardyear_id',$recordId)->first();
        $model->emp5 = AwardInterest::where('company_id',$company->company_id)->where('awardyear_id',$recordId)->first();
        $model->emp6 = AwardData::where('company_id',$company->company_id)->where('awardyear_id',$recordId)->first();

        if(!$model->emp1)
            $model->emp1 = new AwardPersonnel;
        if(!$model->emp2)
            $model->emp2 = new AwardInvestment;
        if(!$model->emp3)
            $model->emp3 = new AwardSecurity;
        if(!$model->emp4)
            $model->emp4 = new AwardRegulation;
        if(!$model->emp5)
            $model->emp5 = new AwardInterest;
        if(!$model->emp6)
            $model->emp6 = new AwardData;

        $this->pageTitle = $model->ay_year;

        $this->initForm($model);
    }

    public function listExtendQuery($query)
    {
        $openedYears = AwardYear::whereRaw("Date(ay_judgingends) > DATE(NOW())")->get();
        $yearsIds = [];
        foreach($openedYears as $year) {
            $yearsIds[]  = $year->awardyear_id;
        }
        $query->whereNotIn('awardyear_id', $yearsIds);
    }
}