<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;

class PamsAwards extends PamsController
{

    public $requiredPermissions = ['pamonline.awards.access_pamsawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'pamsawards');
    }

    public function index()
    {
        $this->pageTitle = "PAM Awards";
        $this->bodyClass = 'slim-container';
    }
}