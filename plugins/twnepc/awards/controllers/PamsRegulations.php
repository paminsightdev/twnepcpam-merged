<?php namespace Twnepc\Awards\Controllers;

use App;
use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Backend;
use Lang;
use Twnepc\Awards\Models\AwardYear;
use Twnepc\Awards\Models\AwardRegulation;
use Flash;
use ApplicationException;
use Session;

class PamsRegulations extends PamsController
{

    use \Twnepc\PamCompanies\Classes\PinSystemControllerTrait;

    public $implement = ['Backend\Behaviors\FormController'];

    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_pamsawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'pamsawards', 'pamsregulations');
    }

    public function preview($recordId = null, $context = null){
         $this->formConfig = 'config_preview.yaml';
         $model = AwardRegulation::find($recordId);
         parent::update($recordId, $context);

         

         return $this->makePartial('print',['showHeader' => false]);
        
    }

    public function index()
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");
        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        $this->pageTitle = "Regulation and investor protection";
        if($opened_year){
            $emp = AwardRegulation::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$company_id)->first();
            if($emp){
                return Backend::redirect("twnepc/awards/pamsregulations/update/{$emp->awardemp4_id}");
            }else{
                return Backend::redirect("twnepc/awards/pamsregulations/create");
            }
        }
    }

    public function create($context = null)
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");
        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();

        if(!$opened_year)
            return Backend::redirect("twnepc/awards/pamsregulations/");

        $emp = AwardRegulation::where('awardyear_id',$opened_year->awardyear_id)->where('company_id',$company_id)->first();

        if($emp)
            return Backend::redirect("twnepc/awards/pamsregulations/update/{$emp->awardemp4_id}");

        $prevYearEmp = AwardRegulation::where('company_id',$company_id)->first();

        if($prevYearEmp)
            $model = $prevYearEmp;
        else
            $model = $this->formCreateModelObject();
        $this->pageTitle = "Regulation and investor protection";
        $this->initForm($model,'create');
    }

    public function create_onSave($context = null)
    {
        $className = substr(strrchr(get_class($this->formCreateModelObject()), "\\"), 1);
        if($this->user->frontenduser->pamadmin)
            $_POST[$className]['company_id'] = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $_POST[$className]['company_id'] = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$opened_year)
            throw new ApplicationException("Hack!");

        $_POST[$className]['awardyear_id'] = $opened_year->awardyear_id;
        Flash::success('Saved successfully');
        $result = parent::create_onSave($context);
        return $result;
    }


    public function update($id)
    {
        if($this->user->frontenduser->pamadmin)
            $company_id = $this->user->frontenduser->pamadmin->company->company_id;
        elseif($this->user->frontenduser->member)
            $company_id = $this->user->frontenduser->member->company->company_id;
        else
            throw new ApplicationException("Hack!");

        $opened_year = AwardYear::whereRaw("Date(ay_entrystarts) < DATE(NOW())")->whereRaw("Date(ay_entryends) > DATE(NOW())")->orderBy('ay_year','desc')->first();
        if(!$opened_year)
            return Backend::redirect("twnepc/awards/pamsregulations/");

        $emp = AwardRegulation::where('awardemp4_id',$id)->where('company_id',$company_id)->first();
        if(!$emp)
            return Backend::redirect("twnepc/awards/pamsregulations/");

        if($emp->aemp4_award == 1){
            $this->vars['submitted'] = true;
            //$this->config->form = "$/twnepc/awards/models/awardpersonnel/submitted_pams_fields.yaml";
        }

        return parent::update($id);
    }

    public function onSubmitToAwards()
    {
        try{
            parent::onSubmitToAwards();
            return [
                '#layout-flash-messages' => Flash::success("Applications successfully submitted.")
            ];
        }catch(ApplicationException $e){
            Flash::error($e->getMessage());
        }
    }
}