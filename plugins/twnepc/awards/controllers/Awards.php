<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Event;
use Request;
use BackendAuth;

class Awards extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['pamonline.awards.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'awardsyears');
        $this->addjs("/plugins/twnepc/awards/assets/js/awards.js", "1.0.0");
    }

    public function create($context = null)
    {
        return parent::create($context);
    }

    public function update($recordId = null, $context = null)
    {
        $model = $this->formFindModelObject($recordId);
        $this->vars['awardyear_id'] = $model->year->awardyear_id;
        return parent::update($recordId,$context);
    }

    public function listExtendQuery($query)
    {
        if(get('awardyear_id',false)){
            $year = get('awardyear_id');
            $query->where('awardyear_id',$year);
        }
        $query->orderBy('award_ordernumber',"ASC");
    }
}