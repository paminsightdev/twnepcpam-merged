<?php namespace Twnepc\Awards\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Twnepc\Awards\Models\AwardYear;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Pamcompanies\Models\CompanyKcd;

class Investments extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['pamonline.awards.access_main'];

    public function __construct()
    {
        
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'awards', 'investments');
    }

     public function preview($recordId = null, $context = null){
         $this->formConfig = 'config_preview.yaml';
         $model = AwardInvestment::find($recordId);
         parent::update($recordId, $context);

         

         return $this->makePartial('print',['showHeader' => false]);
        
    }

    public function update_onSave($recordId = null, $context = null)
    {
        $result = parent::update_onSave($recordId,$context);

        $className = substr(strrchr(get_class($this->formCreateModelObject()), "\\"), 1);
        $model = $this->formFindModelObject($recordId);
        $discretionary = $_POST[$className]['aemp2_discretionary1'];
        $advisory = $_POST[$className]['aemp2_advisory1'];

        $opened_year = AwardYear::where("awardyear_id",$model->awardyear_id)->first();
        $prevYear = $opened_year->ay_year-1;
        $kcdModel = CompanyKcd::where('ckcd_year',$prevYear)->where('company_id',$model->company_id)->first();
        if($kcdModel){
            $kcdModel->ckcd_discsplit = $discretionary + $advisory;
            $kcdModel->save();
        }else{
            $kcdModel = new CompanyKcd;
            $kcdModel->ckcd_year = $prevYear;
            $kcdModel->ckcd_discsplit = $discretionary + $advisory;
            $kcdModel->company_id = $model->company_id;
            $kcdModel->ckcd_clientassets = 0;
            $kcdModel->ckcd_clientadvice = 0;
            $kcdModel->ckcd_staffemployed= 0;
            $kcdModel->ckcd_numstaff     = 0;
            $kcdModel->ckcd_revenue      = 0;
            $kcdModel->ckcd_numclients   = 0;
            $kcdModel->ckcd_profit       = 0;
            $kcdModel->save();
        }

        return $result;
    }
}