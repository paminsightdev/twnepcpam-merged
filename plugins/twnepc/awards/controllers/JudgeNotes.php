<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Twnepc\Awards\Models\AwardNote;

class JudgeNotes extends PamsController
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards','judgeawards','judgenotes');
    }

    public function listExtendQuery($query)
    {
        //not working
        $query->where('user_id',$this->user->frontenduser->id);
    }

    public function update_onSave($recordId = null, $context = null)
    {
        return parent::update_onSave($recordId,$context);
    }

    public function formBeforeSave($model)
    {
        $model->user_id = $this->user->frontenduser->id;
    }

    public function formBeforeUpdate($model)
    {
        $model->user_id = $this->user->frontenduser->id;
    }
}