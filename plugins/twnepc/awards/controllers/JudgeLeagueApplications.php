<?php namespace Twnepc\Awards\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Lang;
use Twnepc\Awards\Models\AwardPersonnel;
use Twnepc\Awards\Models\AwardInvestment;
use Twnepc\Awards\Models\AwardSecurity;
use Twnepc\Awards\Models\AwardRegulation;
use Twnepc\Awards\Models\AwardInterest;
use Twnepc\Awards\Models\AwardApplication;
use Twnepc\Awards\Models\AwardVote;
use Twnepc\Awards\Models\AwardNote;
use October\Rain\Exception\ApplicationException;
use Flash;
use DB;

class JudgeLeagueApplications extends PamsController
{

    public $implement = ['Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.awards.access_judgeawards'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Awards', 'judgeawards', 'judgeleagueyears');
    }

    public function index()
    {

        $this->pageTitle = "League table";
        $this->bodyClass = 'slim-container';

        $award_id = get('award_id',false);
        $awardApplications = AwardApplication::where('award_id',$award_id)
            ->where('aa_showtojudge',1)
            ->where('aa_submitted',1)
            ->where('aa_applied',1)
            ->get();

        $leagueData = [];
        $maximumVoters = 0;
        foreach($awardApplications as $app){
            $votes = 0;
            foreach($app->votes as $vote){
                if($vote->av_rank > 0)
                    $votes += $vote->av_rank;
            }
            $leagueData[$app->awardapplication_id] = [
                'app'           => $app,
                'company'       => $app->company,
                'votes'         => $votes,
                'voters'        => parent::countVotes($app->votes),
                'rank'          => $votes
            ];
            if($maximumVoters < parent::countVotes($app->votes)){
                $maximumVoters = parent::countVotes($app->votes);
            }
        }

        foreach($leagueData as $key => $app){
            if($app['voters'] < $maximumVoters){
                $difference  = $maximumVoters - $app['voters'];
                $leagueData[$key]['rank'] = $app['rank']+(count($awardApplications)*$difference);
            }
        }

        $sortedLeagueData = [];
        foreach($leagueData as $key => $app){
            $sortedLeagueData[$app['rank']][$key] = $app;
            if($maximumVoters > 0)
                $sortedLeagueData[$app['rank']][$key]['rank'] = number_format((float)$app['rank']/$maximumVoters, 2, '.', '');
            else
                $sortedLeagueData[$app['rank']][$key]['rank'] = 0;
        }
        ksort($sortedLeagueData);
        //var_dump($sortedLeagueData);
        $this->vars['leagueTableData'] = $sortedLeagueData;
    }
}