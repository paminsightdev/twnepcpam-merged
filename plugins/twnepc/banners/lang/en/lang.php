<?php return [
    'plugin' => [
        'name' => 'Banners',
        'description' => 'Banner manager',
    ],
    'components' => [
        'space' => [
            'name' => 'Space',
            'description' => 'Inserts predefined Banner space'
        ]
    ],
    'spacename' => 'Sapce name',
    'sapceidentifier' => 'Identifier',
    'Banners' => 'Banners',
    'banner' => 'Banner',
    'banner_image' => 'Banner Image File',
    'banner_url' => 'Banner Url',
    'banner_space_menu' => 'Banner Spaces',
];