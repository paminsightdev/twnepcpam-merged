<?php namespace Twnepc\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcBannersSpace2 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_banners_space', function($table)
        {
            $table->dropColumn('id');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_banners_space', function($table)
        {
            $table->smallInteger('id');
        });
    }
}
