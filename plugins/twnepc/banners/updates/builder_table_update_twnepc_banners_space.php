<?php namespace Twnepc\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcBannersSpace extends Migration
{
    public function up()
    {
        Schema::table('twnepc_banners_space', function($table)
        {
            $table->string('name')->change();
            $table->string('identifier')->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_banners_space', function($table)
        {
            $table->string('name', 191)->change();
            $table->string('identifier', 191)->change();
        });
    }
}
