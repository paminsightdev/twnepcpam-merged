<?php namespace Twnepc\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcBannersSpace extends Migration
{
    public function up()
    {
        Schema::create('twnepc_banners_space', function($table)
        {
            $table->engine = 'InnoDB';
            $table->smallInteger('id');
            $table->string('name');
            $table->string('identifier');
            $table->text('banners');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_banners_space');
    }
}
