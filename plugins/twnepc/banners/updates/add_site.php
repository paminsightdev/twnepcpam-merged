<?php namespace Twnepc\Banners\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddSite extends Migration
{
    public function up()
    {
        Schema::table('twnepc_banners_space', function($table)
        {
            $table->integer('site_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_banners_space', function($table)
        {
            $table->dropColumn('site_id');
        });
    }
}