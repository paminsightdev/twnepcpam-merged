<?php namespace Twnepc\Banners;

use System\Classes\PluginBase;

use Twnepc\Banners\Components\Space;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            Space::class => 'space',
        ];
    }

    public function registerSettings()
    {
    }
}
