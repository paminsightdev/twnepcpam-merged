<?php namespace Twnepc\Banners\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use BackendAuth;

class Banners extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'view_banners', 
        'edit_banners' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Banners', 'banner-menu-item','banners');
    }

    public function listExtendQuery($query) {
        $user = BackendAuth::getUser();
        if($user->role->code == 'siteadmin') {
            if($user->site) {
                $query->whereIn('site_id', $user->site->pluck('id'));
            }
        }
    }

    public function formExtendFields($form) {

    }

    public function formAfterSave($model)
    {
        
    } 
}
