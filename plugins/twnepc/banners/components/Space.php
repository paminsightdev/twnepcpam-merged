<?php namespace Twnepc\Banners\Components;

use Lang;
use Cms\Classes\ComponentBase;
use Twnepc\Banners\Models\Space as SpaceModel;
use Auth;

class Space extends ComponentBase {


    public $space;

    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.banners::lang.components.space.name',
            'description' => 'twnepc.banners::lang.components.space.description'
        ];
    }

    public function onRun() {
        $this->addCss('/plugins/twnepc/banners/assets/css/mod.css');
        if($this->property('space')) {
            $this->space = $this->page['space'] = SpaceModel::find($this->property('space'));
        }
    }

    public function defineProperties(){
        
        return [
            'space' => [
                'title'       => 'Space',
                'description' => 'Space',
                'type'        => 'dropdown'
            ]
        ];
    }

    public function getSpaceOptions()
    {

        $spaces = SpaceModel::all();
        $result = [];
        $result['noselect'] = '--Select Space--';
        foreach($spaces as $space) {
            $result[$space->id] = $space->name;
        }
        return $result;
    }


}