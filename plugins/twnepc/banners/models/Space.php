<?php namespace Twnepc\Banners\Models;

use Backend\Facades\BackendAuth;
use Model;

/**
 * Model
 */
class Space extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable = ['site_id'];

    protected $jsonable = ['banners'];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_banners_space';

    public $belongsTo = [
        'site' => ['Keios\Multisite\Models\Setting']
    ];

    public function getSiteOptions() {
        $user = BackendAuth::getUser();
        if($user->role->code == 'siteadmin') {
            if($user->site) {
                return \Keios\Multisite\Models\Setting::whereIn('id', $user->site->pluck('id'))->get()->pluck('theme','id');
            }
        }

        return \Keios\Multisite\Models\Setting::all()->pluck('theme','id');
    }
}
