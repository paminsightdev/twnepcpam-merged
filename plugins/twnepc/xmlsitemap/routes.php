<?php 

Route::get('/sitemap_index.xml', function(){ 
    /* 
    Creates a list of sitemap.xml URLs using theme and year as virtual folder names, 
    for every themes (epc,twn and fundtruffle) for every year since 1997.
    e.g. 
    /twn/1998/sitemap.xml
    /epc/1998/sitemap.xml
    /fundtruffle/1998/sitemap.xml
    /twn/1999/sitemap.xml
    /epc/1999/sitemap.xml
    /fundtruffle/1999/sitemap.xml
    .
    .
    . 
    /twn/2021/sitemap.xml
    /epc/2021/sitemap.xml
    /fundtruffle/2021/sitemap.xml    

    */

    $siteNames = array('twn','epc','fundtruffle') ;
    $currentYear = date('Y') ;

    $years = range($currentYear,1997) ;

    return Response::view('twnepc.xmlsitemap::sitemap_index',['sites' => $siteNames, 'years' => $years])->header('Content-Type','application/xhtml+xml') ;
});

#.*\/(epc|twn|fundtruffle|fundeye)\/(1997|1998|1999|200\d|201\d|202\d)

Route::get('{site}/{year}/sitemap.xml', function($site, $year) {
    
    $siteNames = array('twn'=>1,'epc'=>2,'fundeye'=>4,'fundtruffle'=>4) ;

    if (array_key_exists($site,$siteNames)) {
        $siteID = $siteNames[$site] ;
    }
    else {
        return Response::make( View::make( 'cms::404' ), 404 );        
    }
    $dateFrom = $year . '-01-01' ;
    $dateTo = $year . '-12-31' ;

    $sql = "SELECT * from twnepc_news_articles WHERE site_id =  $siteID  and published_date between '$dateFrom' and '$dateTo' order by published_date desc";
    //return $sql ;
    $allArticles = DB::select($sql);

    return Response::view('twnepc.xmlsitemap::sitemap',['articles' => $allArticles ,'site' => $site, 'year'=>$year])->header('Content-Type','application/xhtml+xml') ;
})->where([ 'site'=>'^(epc|twn|fundtruffle|fundeye)$' , 'year'=>'^(1997|1998|1999|200\d|201\d|202\d)$']);
