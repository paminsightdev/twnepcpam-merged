<?php return [
    'plugin' => [
        'name' => 'XML Sitemap',
        'description' => 'Automatically creates a sitemap.xml from all the TWN, EPC and FundTruffle articles in the whole database, and displays it in correct the XML format for submission to Google for search engine indexing.'
    ]
];