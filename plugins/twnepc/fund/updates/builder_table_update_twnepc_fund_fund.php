<?php namespace Twnepc\Fund\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcFundFund extends Migration
{
    public function up()
    {
        Schema::rename('twnepc_fund_items', 'twnepc_fund_fund');
        Schema::table('twnepc_fund_fund', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->string('name')->change();
        });
    }
    
    public function down()
    {
        Schema::rename('twnepc_fund_fund', 'twnepc_fund_items');
        Schema::table('twnepc_fund_items', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->string('name', 191)->change();
        });
    }
}
