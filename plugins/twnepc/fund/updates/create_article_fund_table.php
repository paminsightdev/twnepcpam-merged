<?php namespace Twnepc\Fund\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateArticleFundTable extends Migration
{
    public function up()
    {
        Schema::create('article_fund', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('fund_id');
            $table->integer('article_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('article_fund');
    }
}
