<?php namespace Twnepc\Fund\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcFundItems extends Migration
{
    public function up()
    {
        Schema::create('twnepc_fund_items', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_fund_items');
    }
}
