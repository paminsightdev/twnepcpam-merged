<?php namespace Twnepc\Fund\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcFundFund3 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_fund_fund', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_fund_fund', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
