<?php return [
    'plugin' => [
        'name' => 'Fund',
        'description' => 'Add/Edit/Manage funds',
    ],
    'name' => 'Fund Name',
    'funds_settings' => 'Funds',
    'view_fund' => 'View Funds',
    'edit_funds' => 'Edit Funds',
    'main_menu_fund' => 'Funds',
];