<?php namespace Twnepc\Fund\Models;

use Model;

/**
 * Model
 */
class Fund extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_fund_fund';


    public $belongsToMany = [
        'articles' => 'Twnepc\News\Models\Article'
    ];
}
