<?php namespace Twnepc\Fund\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Funds extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'view_fund', 
        'edit_fund' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Fund', 'main-menu-fund','funds');
    }
}
