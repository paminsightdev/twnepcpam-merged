<?php namespace Twnepc\Fund\Controllers;

use Request;
use BackendMenu;
use GuzzleHttp\Psr7\Response;
use Twnepc\News\Models\Article;
use Illuminate\Routing\Controller;
use Keios\Multisite\Models\Setting;
use Illuminate\Support\Facades\Input;

class FundApiController extends Controller
{
    public function index()
    {
        $parameters = Input::all();


        // Replace with logic to return the model data
        $articles = Article::whereHas('fund',function($query) use ($parameters){
                $query->where('slug',$parameters['fund']);
            }
        )->where('status_id',3)->where('site_id',4);

        //$articles = Article::with('fund')->where('site_id',4);

        //var_dump($articles->toSql());

        if(isset($parameters['limit'])){
            $articles = $articles->limit($parameters['limit']);
        }
        

        $articles = $articles->get();

        $result = [];
        foreach($articles as $article) {
            //"isnew": "1",
            //"title": "Latest UK tax gap figures result in £71 billion for UK public services",
            //"newsurl": "http://www.eprivateclient.com/page_fullstory.php?articleid=34322",
            //"newsdate": "18.06.2018"
            $item = [];
            //$item['isnew'] = 1;
            $item['title'] = $article->title;
            $site = Setting::where('id',$article->site_id)->first();
            $item['newsurl'] = $site->domain.'/article/'.$article->slug;
            $item['newsdate'] = date('d.m.Y',strtotime($article->published_date));
            array_push($result,$item);
        }

        return response()->json($result, 200);
    }
}
