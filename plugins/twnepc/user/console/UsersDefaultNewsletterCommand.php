<?php
namespace Twnepc\User\Console;

use RainLab\User\Models\User;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Twnepc\Logs\Models\PamSplunkTroubleShooting; 

use Twnepc\Membership\Models\Membership;

use Twnepc\Membership\Models\ListsSubscribers;
use Twnepc\Membership\Models\Subscribers;
use Twnepc\Membership\Models\CategoryUser;
use Twnepc\Membership\Models\UserAcceptSettings;


/**
 * Description of UsersDefaultNewsletterCommand
 * The class checks if the user has preferences setup and if not registers them for a default newsletter if they have not. 
 * @author paulie
 */
class UsersDefaultNewsletterCommand extends Command {
    /**
     * @var string The console command name.
     */
    protected $name = 'user:defaultnewsletter';
    /**
     * @var string The console command description.
     */
    protected $description = 'registers a user for a default newsletter based on membership(s)';
    /**
     * @var file pointer to a .csv file used to hold user/membership/subscriber data records for admin corroboration
     */
    public $fpCsv;
    /**
     * @var array a predefined header used at the to of a file
     */
    public $csvHeader;
    /**
     * @var string the path location and filename of the file that will hold user/membership/subscriber data records
     */
    private $autoSwitchDataCapture;
    /**
     * @var boolean flag that acts as a execution switch/mode indicating update/modify tables or not
     */    
    private $executeChanges = false;
    
    
    
    /**
     * Execute the console command.
     * @return void
     */
    public function handle() { 
        
        // mark the start time before executing
        $startTime = microtime(true);
        $this->output->writeln( "#process start time: ". date("H:i:s [d.m.Y]", $startTime) );
        // use this var to define the path and location of the file...
        $this->autoSwitchDataCapture = base_path().'/tests/audit/auto-switch-data-capture.csv';
        
        
        /////////////////////////////////////////////////////////////////
        // THIS PART GENERATES THE LIST OF USERS WITHOUT SUBSCRIPTIONS
        /////////////////////////////////////////////////////////////////
        
        
        $userAndMembershipArray = [];
        // there will be similar functions here later that retrieve only a subset of emails depending on the feature being run
        $emails = User::getEmailsForSubscriptionAutoSwitch();
        
        // write out step 1 finished generate unsubscribed list
        $this->output->writeln("step 1/4: generated a list of all users that will be processed, FINISHED... ");
        
        // get the user records associated with these emails: id, company_id and branch_id...
        $userAndMembershipData = User::getUserIdsForMemberships($emails);
        
        $count = 0;
        // add user membership record data to an array...
        foreach($userAndMembershipData as $usrData) {
            $count++;
            // add array of data to array element...
            $userAndMembershipArray[] = $usrData->attributes;
        }
        // write out step 1 finished got users
        $this->output->writeln("step 2/4: got $count users, FINISHED... ");
        
        
        /////////////////////////////////////////////////////////////////
        // THIS PART GETS MEMBERSHIPS FOR EACH USER IN THE LIST
        /////////////////////////////////////////////////////////////////
        
        
        $userMembershipSubscript = [];
        $count = 0;
        // check the membership table for each user; return an array[email] of arrays[mId, sub]
        foreach($userAndMembershipArray as $memData) {
                
            $membershipData = [];
            
            // call function to get membership daaataaa!
            $objectsInList = $this->getUsersMembershipData($memData);
            
            // where there are no memberships (based on active and expired) for a user, do not add them to the $userMembershipSubscript list for processed thought this script
            // @todo this could lead to a seperate clean up task where the related data in the database could be removed that's associated with the user, requirements needed.
            if(count($objectsInList) > 0) {
            
                // here we add the valid memberships for a user whose subscription will be processed later...
                foreach($objectsInList as $userMembership) {
                    $count++;
                    // attach the user_id to the object, when not one in the membership_membership table, the form that inserts the record uses users_id or company_id
                    if(empty($userMembership->attributes['user_id'])) { 
                        $userMembership->attributes['user_id'] = $memData['id']; 
                    }
                    $membershipData[] = $userMembership->attributes;
                }
            
                // add any returned membership objects to the user records array
                $memData['membershipData'] = $membershipData;
                // add the user array to a list of users
                $userMembershipSubscript[] = $memData;
            }
        }
        // write out step 1 finished got users
        $this->output->writeln("step 3/4: got $count memberships, from filtered down ".count($userMembershipSubscript)." users, FINISHED... ");
        
        
        /////////////////////////////////////////////////////////////////
        // THIS PART LOOPS THROUGH EACH USER AND CHECKS/UPDATES RECORDS
        /////////////////////////////////////////////////////////////////
        
        
        // before we start writing out these records we clear the file of any content
        file_put_contents($this->autoSwitchDataCapture, "");
        // open the .csv file for writing user/membership/subscriber data here...
        $this->fpCsv = fopen($this->autoSwitchDataCapture, 'a+');
        // flag to indicate header should be added when file is generated
        $this->createHeader = true;
        
        // the counter
        $counter = 0;
        // go through each user with membership and: 
        // (1). check validity of subscriber according to membership. (2). process verified subscriptions accordingly
        foreach($userMembershipSubscript as $membership) {
            $counter++;
            //if($counter > 2000) {
                //break;
            //}
            // check a user and their memberships, to go through the subscription model
            $memMetaData = $this->updateNewUserSubscriptionPrepareAndExecute($membership);
        }
        // close the file for .csv 
        fclose($this->fpCsv);
        
        
        // write out step 1 finished got users
        $this->output->writeln("step 4/4: updated $counter subscriptions, FINISHED... ");
        
        
        // mark the end time after execution
        $endTime = microtime(true);
        $timeTaken = date( "H:i:s", ($endTime - $startTime));
        
        // out this for the user
        $this->output->writeln( "process end time: ".date("d.m.Y H:i:s", ($endTime)) );
        $this->output->writeln( "execution time: ".$timeTaken );
    }
    
    
    /**
     * go through the array of arrays and retrieve associated memberships records
     * @param type $memData
     * @return mixed|array a list of membership objects in an array
     */
    public function getUsersMembershipData($memData) {
        
        $membershipList = [];
        
        if(!empty($memData['company_id']) && !empty($memData['branch_id'])) {
            $membershipForUser = Membership::select('id', 'subscription_id', 'user_id', 'branch_id', 'company_id', 'start_date', 'end_date')
                ->where('status', 'Active')
                ->whereDate('end_date', '>=', date('Y-m-d H:i:s', strtotime('now')))
                ->whereNull('user_id')
                ->where('company_id', $memData['company_id'])->where('branch_id', $memData['branch_id'])->get();
            
            foreach($membershipForUser as $obj) {
                $membershipList[] = $obj;
            }
        }
        
        if(!empty($memData['company_id'])) {
            $membershipForUser = null;
            $membershipForUser = Membership::select('id', 'subscription_id', 'user_id', 'branch_id', 'company_id', 'start_date', 'end_date')
                ->where('status', 'Active')
                ->whereDate('end_date', '>=', date('Y-m-d H:i:s', strtotime('now')))
                ->whereNull('user_id')
                ->where('company_id', $memData['company_id'])->get();
            
            foreach($membershipForUser as $obj) {
                $membershipList[] = $obj;
            }
        }
        
        if(!empty($memData['id'])) {
            $membershipForUser = null;
            $membershipForUser = Membership::select('id', 'subscription_id', 'user_id', 'branch_id', 'company_id', 'start_date', 'end_date')
                ->where('status', 'Active')
                ->whereDate('end_date', '>=', date('Y-m-d H:i:s', strtotime('now')))
                ->where('user_id', $memData['id'])->get();
            
            foreach($membershipForUser as $obj) {
                $membershipList[] = $obj;
            }
        }
                
        ///////////////////////////////////////////////////////////////////////
        // THIS CODE BELOW DOES THE SAME AS ABOVE BUT NEEDS TO BE TESTED THOROUGHLY
        ///////////////////////////////////////////////////////////////////////
        
        // SELECT id, subscription_id, user_id, branch_id, company_id, start_date, end_date FROM twnepc.twnepc_membership_memberships 
        //$membershipForUser = Membership::select('id', 'subscription_id', 'user_id', 'branch_id', 'company_id', 'start_date', 'end_date')
            // WHERE `status` = 'Active' 
            //->where('status', 'Active')
            // AND `end_date` >= curdate()
            //->whereDate('end_date', '>=', date('Y-m-d H:i:s', strtotime('now')))
            // AND ((`company_id` = 8792 AND `branch_id` = 37704) OR (`company_id` = 8792) OR (`user_id` = 3822));
            //->where(function ($query) use ($memData) {
               //$query->where('company_id', $memData['company_id'])->where('branch_id', $memData['branch_id'])->whereNull('user_id');
            //})->orWhere(function($query) use ($memData) {
                //$query->where('company_id', $memData['company_id'])->whereNull('user_id');
            //})->orWhere(function($query) use ($memData) {
                //$query->where('user_id', $memData['id']);
            //})->get();
        
        return $membershipList;
    }
    
    
    /**
     * process to check if user has can be subscribed.
     * @param array|mixed $memSubData
     * @return array|mixed $verified status of simulated update process
     */
    public function updateNewUserSubscriptionPrepareAndExecute($memSubData) {

        // this array is used to store associative values...
        $recordValueStates = [];
        $subId = false;
        $subsCheck;
        
        
        ////////////////////////////////////////////////////////////////////////
        // Check if the user has a subscriber or create one...
        ////////////////////////////////////////////////////////////////////////
        
        
        $subsCheck = Subscribers::getSubscriberViaUserId($memSubData['id']);
        
        // update the database tables or not
        if($this->executeChanges) {
            // where there is no subscription for this user, create one...
            if(empty($subsCheck) || count($subsCheck) == 0) {
                $subId = Subscribers::setSubscriber($memSubData);
                // now retrieve the subscriber 
                $subsCheck = Subscribers::getSubscriberViaUserId($memSubData['user_id']);
                //@todo log to pam_troubleshooting table if you had to create a subscriber for this user
            }
        }
        // add the subscriber id to the processing object
        $memSubData['subscriber_id'] = (count($subsCheck) > 0) ? $subsCheck[0]->id : NULL;
        
        
        ///////////////////////////////////////////////////////////////////////
        // Check which subscriber list ids are missing
        ///////////////////////////////////////////////////////////////////////
        
        
        $memSubData['requiredListIds'] = [];
        $memSubData['actualListIds'] = [];
        
        foreach($memSubData['membershipData'] as $membership) {
            // get the associated list ids in an array for the given subscription label...
            $memSubData['requiredListIds'] = array_merge($memSubData['requiredListIds'], $this->subscriptionIdToListIds($membership['subscription_id']));
        }
        
        // get the actual list ids in an array for the given user...
        $memSubData['actualListIds'] = (!empty($memSubData['subscriber_id'])) ? ListsSubscribers::getListIdsViaSubscriberId($memSubData['subscriber_id']) : []; 
        
        //@todo log the changes to the pam_troubleshooting database and write a query
        
        // are there any in param 1 that are not in param 2
        $memSubData['anyListIdsRequired'] = array_diff($memSubData['requiredListIds'], $memSubData['actualListIds']);
        $memSubData['overSubscribedListIds'] = array_diff($memSubData['actualListIds'], $memSubData['requiredListIds']);
        
        // update the database tables or not
        if($this->executeChanges) {
            if(count($memSubData['anyListIdsRequired']) == 0 && count($memSubData['overSubscribedListIds']) == 0) {
                echo "Log in file: subscription list id's correct";

            } else {
                //echo "Log in file: subscription list id's is not correct";
                // the subscriber's list ids will be removed...
                ListsSubscribers::deleteListIdsViaSubscriberId($memSubData['subscriber_id']);
                // the subscriber's required list ids are to be added...
                ListsSubscribers::setListIdsViaSubscriberId($memSubData['subscriber_id'], $memSubData['requiredListIds']);
            }
        }
        
        
        ///////////////////////////////////////////////////////////////////////
        // check category interests or preferences should be based on subscription
        ///////////////////////////////////////////////////////////////////////
        
        
        //@todo add listId 8 to case
        
        $userCategoryAdd = [];
        foreach($memSubData['requiredListIds'] as $siteRef) {
            switch($siteRef) {
                case 7  : $userCategoryAdd[] = 1; break;
                case 9  : $userCategoryAdd[] = 2; break;
                case 12 : $userCategoryAdd[] = 4; break;
                default : break;
            } 
        }
        
        // get the site id that are relative to the interests (later we may set a default value 'All')
        $userCategoryList = CategoryUser::getSiteIdsViaUserId($memSubData['id']);  //12331);
        
        
        // 1=7twn 2=9epc 4=12fundeye
        if(count($userCategoryList) == 0) {
            $memSubData['anyCategorysExisting'] = $userCategoryList;
            $memSubData['anyCategorysRequired'] = $userCategoryAdd;
            
            // update the database tables or not
            if($this->executeChanges) {
                CategoryUser::setCategorys($memSubData['id'], $userCategoryAdd);
            }
        }
        
        if(count($userCategoryList) > 0) {
            // are there any in param 1 that are not in param 2
            $memSubData['anyCategorysExisting'] = $userCategoryList;
            $memSubData['anyCategorysRequired'] = array_diff($userCategoryAdd, $userCategoryList);
            
            // update the database tables or not
            if($this->executeChanges) {
                CategoryUser::setCategorys($memSubData['id'], $memSubData['anyCategorysRequired']);
            }
            
        }
        
        
        ///////////////////////////////////////////////////////////////////////
        // check user accept settings are all present in the database
        ///////////////////////////////////////////////////////////////////////
        
        
        // get the user's accept settings for each site
        $sitesAcceptedExistingArray = UserAcceptSettings::getUserAcceptViaUserId($memSubData['id']);
        $memSubData['sitesAcceptedRequired'] = $userCategoryAdd;
        $memSubData['sitesAcceptedExisting'] = $sitesAcceptedExistingArray;
        // after check what sites the user should be added to are needed to be accepted
        $memSubData['sitesAcceptedToBeAdded'] = array_diff($userCategoryAdd, $sitesAcceptedExistingArray);
        $memSubData['sitesAcceptedToBeDeleted'] = array_diff($sitesAcceptedExistingArray, $userCategoryAdd);
        
        
        // update the database table with these settings...
        if($this->executeChanges) {
            foreach($memSubData['sitesAcceptedToBeAdded'] as $siteId) {
                // each required site id that has not been added will be added
                UserAcceptSettings::setUserAccept($siteId, 1, $memSubData['id']);
            }
        }
        
        
        $rows = [];
        // some subscriptions have more than one membership, when one just pass the data object when more loop through changing the membership part...
        if(count($memSubData['membershipData']) == 0) {
            // call a function to format the data array into array to be placed in a csv file...
            $line = $this->formatUserMembershipSubscriberDataForCsv($memSubData);
            $rows[] = $line;
                
        } elseif(count($memSubData['membershipData']) > 0) {
            
            foreach($memSubData['membershipData'] as $membershipData) {
                // call a function to format the data array into array to be placed in a csv file...
                $line = $this->formatUserMembershipSubscriberDataForCsv($memSubData, $membershipData);
                $rows[] = $line;
            }
        }
        
        if($this->createHeader) {
            fputcsv($this->fpCsv, $this->csvHeader);
            // set addition of header to false for the next iteration of this function when called
            $this->createHeader = false;
        }
        
        // go through the array of lines
        foreach($rows as $fields) {
            // add line consisting of array of fields
            fputcsv($this->fpCsv, $fields);
        }
        
        return $recordValueStates;
    }
    
    
    /**
     * an array lookup list is used here to refer to data
     * @todo create a column in the twnepc.twnepc_membership_subscriptions table called 'type' and generate list/map below.
     */
    public function subscriptionIdToListIds($subId) {
        
        switch($subId) {
            // these are the normal subscriptions
            case 10 : return [7];
            case 11 : return [9];    
            case 12 : return [12];
            case 13 : return [7];
            case 14 : return [9];
            case 15 : return [9];
            case 16 : return [7];    
            case 17 : return [9];
            case 18 : return [9];
            case 19 : return [7];
            case 20 : return [7];
            case 21 : return [7];    
            case 22 : return [7];
            case 23 : return [9];
            // these are the dual and all 4 subscriptions over 24
            case 24 : return [7, 9, 12];
            case 25 : return [7, 9];
            case 26 : return [7, 9];    
            case 27 : return [7, 9];
            case 28 : return [7, 9, 12];
            case 29 : return [7, 9];
        }
    }    
    
    
    /**
     * format a user's membership and subscriber data for a .csv file input line.
     * @param array|mixed $memSubData array with arrays representing a user membership subscriber
     * @return array|mixed $field formatted array to be placed in the file.
     * @todo finish this function company name and other functions needed
     */
    public function formatUserMembershipSubscriberDataForCsv($memSubData, $membershipData = null) {
        
        //var_dump($memSubData);
        
        // on the first iteration of this function assign some values to this property
        // add header fields and row fields here, for the csv file, in this function...
        if(empty($this->csvHeader)) {
            $this->csvHeader = ['user id','first name','last name','email','company id','company name','branch id','branch/location','membership id','subscription id','start date (membership)',
                'end date (membership)','subscriber id','required mail lists','actual mail lists','mail lists to add','over subscribed mail lists','categorys/interests that exist','categorys/interests to add',
                'sites accepted-settings required','sites accepted-settings existing ','sites accepted-settings to add','sites accepted-settings deleted/oversubscribed'];
        }
        
        // 
        $fields = [];
        // user id
        $fields[] = $memSubData['id'];
        // name
        $fields[] = $memSubData['name'];
        // lastname
        $fields[] = $memSubData['surname'];
        // email
        $fields[] = $memSubData['email'];
        // company id
        $fields[] = (!empty($memSubData['company_id'])) ? $memSubData['company_id'] : '';
        // company name
        $fields[] = (!empty($memSubData['company_id'])) ? User::getCompanyName($memSubData['company_id']) : '';
        // branch id
        $fields[] = ($memSubData['branch_id']) ? $memSubData['branch_id'] : '';
        // branch name
        $fields[] = (!empty($memSubData['branch_id'])) ? User::getBranchName($memSubData['branch_id']) : '';
        // membership id
        $fields[] = ($membershipData != null) ? $membershipData['id'] : 'No membership created';
        // subscription id
        $fields[] = ($membershipData != null) ? $membershipData['subscription_id'] : 'No membership created';
        // subscription start date
        $fields[] = ($membershipData != null) ? $membershipData['start_date'] : 'No membership created';
        // subscription end date
        $fields[] = ($membershipData != null) ? $membershipData['end_date'] : 'No membership created';
        // subscriber id
        $fields[] = (isset($memSubData['subscriber_id'])) ? $memSubData['subscriber_id'] : 'there is no subscriber created';
        // required lists
        $fields[] = (count($memSubData['requiredListIds'])) ? implode("+", $memSubData['requiredListIds']) : 'Subscription not found for twn-epc-fundeye';
        // required lists
        $fields[] = (count($memSubData['actualListIds'])) ? implode("+", $memSubData['actualListIds']) : 'None Exist';
        // actual lists to be added
        $fields[] = (count($memSubData['anyListIdsRequired'])) ? implode("+", $memSubData['anyListIdsRequired']) : 'None other required';
        // over subscribed lists present
        $fields[] = (count($memSubData['overSubscribedListIds'])) ? implode("+", $memSubData['overSubscribedListIds']) : 'No extra access';
        // any existing category interests belonging to list group
        $fields[] = (count($memSubData['anyCategorysExisting'])) ? implode("+", $memSubData['anyCategorysExisting']) : 'No interests selected';
        // any required category interests belonging to list group
        $fields[] = (count($memSubData['anyCategorysRequired'])) ? implode("+", $memSubData['anyCategorysRequired']) : 'None to add';
        // what sites the user should have accepted according to their membership
        $fields[] = (count($memSubData['sitesAcceptedRequired'])) ? implode("+", $memSubData['sitesAcceptedRequired']) : 'Membership/Subscription admin issue' ;
        // what sites the user has accepted already and currently exist in the database
        $fields[] = (count($memSubData['sitesAcceptedExisting'])) ? implode("+", $memSubData['sitesAcceptedExisting']) : 'None created';
        // what sites the user should be added to or are needed to be accepted
        $fields[] = (count($memSubData['sitesAcceptedToBeAdded'])) ? implode("+", $memSubData['sitesAcceptedToBeAdded']) : 'Already added';
        // over accepted sites most probably from legacy memberships that have expired or over subscribed by admin
        $fields[] = (count($memSubData['sitesAcceptedToBeDeleted'])) ? implode("+", $memSubData['sitesAcceptedToBeDeleted']) : 'None over added';
        
        return $fields;
    }
    
    
    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }
    
    
    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
    
}
