<?php namespace Twnepc\User\Console;

use RainLab\User\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RecreateUserPassword extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'user:recreateuserpassword';

    /**
     * @var string The console command description.
     */
    protected $description = 'No description provided yet...';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $this->output->writeln('Gathering Users...');
        $users = User::all();
        foreach($users as $u) {
            if($u->old_password){
                $u->password = $u->password_confirmation = $u->old_password;

                $this->output->writeln($u->old_password.' - '.$u->password);

                if(Hash::check($u->old_password, $u->password))
                {
                    $u->save();
                }
                else {
                    $this->output->writeln('Password Hashcheck failed');
                }
            }
        }
    }   

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
