<?php namespace Twnepc\User\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Twnepc\User\Models\Salesrep;
use Rainlab\User\Models\User;

class Salesreps extends Controller
{
    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.User', 'salesreps-main','salesreps');
    }

    
}