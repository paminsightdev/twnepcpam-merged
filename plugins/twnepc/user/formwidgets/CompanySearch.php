<?php
namespace Twnepc\User\FormWidgets;

use Twnepc\PamCompanies\Models\Companies;
use Request;
use Backend\Classes\FormWidgetBase;
use Input;
use ApplicationException;
use Hash;
use BackendAuth;
use Flash;
use DB;
/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class CompanySearch extends FormWidgetBase {

    public $alias = 'field';

    public function init()
    {

    }

    public function prepareVars()
    {
        $this->vars['fieldName']    = $this->formField->getName();
        $this->vars['fieldValue']   = $this->getLoadValue();

        if($this->model->company == "" && ($this->model->company_id != '' || $this->model->company_id != null)){
            $company = Companies::where('company_id',$this->model->company_id)->first();
            $this->vars['fieldValue'] = $company->c_companyname;
        }

        $this->vars['companies']    = [];
        $companies    = Companies::all();
        foreach($companies as $company){
            $this->vars['companies'][] = $company->c_companyname;
        }
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial($this->alias);
    }

    protected function loadAssets()
    {
        $this->addCss('//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css', 'jquery-css');
        $this->addJs('https://code.jquery.com/ui/1.12.0/jquery-ui.js', 'jquery-js');
    }
}