<?php 

namespace Twnepc\User\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use Backend\Widgets\Grid;
use Twnepc\User\Models\Industry;

class IndustryGrid extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'industrygrid';

    public function render() {

        $this->prepareVars();

        return $this->makePartial('industrygrid');
    }

    public function prepareVars()
    {
       $this->vars['records'] = Industry::where('industry_id',$this->model->id)->get();
       $this->vars['columnTotal'] = 2;
       $this->vars['noRecordsMessage'] = 'No industry found.';
    }
}