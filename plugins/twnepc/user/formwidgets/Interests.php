<?php 

namespace Twnepc\User\FormWidgets;

use Twnepc\News\Models\Category;
use Illuminate\Support\Facades\DB;
use Backend\Classes\FormWidgetBase;
use Keios\Multisite\Models\Setting;



class Interests extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'interests';

    public function render() {

        $this->prepareVars();

        return $this->makePartial('interests');
    }

    public function getSubInterests($site, $category, $level, $selections = []){
        $html = '';

        $children = $category->children()->get();
        foreach($children as $child){
            if(!$child->is_hidden){
                $html .= '<div data-site="'.$site->site_id.'" class="interest custom-control custom-checkbox level-'.$level.'">';
                $html .= '<input type="checkbox" '.(in_array($child->id,$selections) ? 'checked' : '').' class="custom-control-input" name="User[category][]" value="'.$child->id.'"  id="interests'.$child->id.'">';
                $html .= '<label class="custom-control-label" for="interests'.$child->id.'">'.$child->name.'</label>';
                $html .= '<div class="subInterests level-'.$level.'">'.$this->getSubInterests($site, $child, $level+1, $selections).'</div>';
                $html .= '</div>';

            }
        }

        return $html;
    }

    public function getAllInterests($site, $selections = []){
        $topLevelCategories = Category::where('site_id',$site->site_id)->where('is_hidden',0)->where('parent_id',NULL)->get();
        $level = 1;
        $html = '';
        foreach($topLevelCategories as $category){
            if($category->children->count()){
                $html .= '<strong>'.$category->name.'</strong>';
                $html .= '<div class="interests site-'.$site->site_id.' level-'.$level.'" data-site="'.$site->site_id.'">';
                $html .= $this->getSubInterests($site, $category, $level+1, $selections);
                $html .= '</div>';
            }
        }

        return $html;
    }

    public function prepareVars()
    {
       $this->vars['sites'] = Setting::all();
       $this->vars['interests'] = Category::where('is_hidden',0)->where('parent_id',NULL)->get();
    
       $userinterests = $this->model->category;
       $catids = [];

       foreach($userinterests as $c) {
        array_push($catids,$c->id);
        }


        $this->vars['siteswithinterests'] = DB::table('keios_multisite_settings as site')
        ->select('interests.site_id','interests.parent_id','interests.is_hidden','site.id as siteid','site.theme','interests.id as categoryid','interests.name')
        ->where('interests.is_hidden',0)
        ->where('interests.parent_id',NULL)
        ->groupBy('site.id')
        ->leftJoin('twnepc_news_categories as interests','site.id','interests.site_id')
        ->get();

        $this->vars['catids'] = $catids;
    }
}