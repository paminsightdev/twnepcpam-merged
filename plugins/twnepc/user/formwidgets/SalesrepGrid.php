<?php 

namespace Twnepc\User\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use Backend\Widgets\Grid;
use Twnepc\User\Models\Salesrep;

class SalesrepGrid extends FormWidgetBase
{
    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'salesrepgrid';

    public function render() {

        $this->prepareVars();

        return $this->makePartial('salesrepgrid');
    }

    public function prepareVars()
    {
       $this->vars['records'] = Salesrep::where('salesrep_id',$this->model->id)->get();
       $this->vars['columnTotal'] = 2;
       $this->vars['noRecordsMessage'] = 'No salesrep found.';
    }
}