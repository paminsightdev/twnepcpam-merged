<?php
namespace Twnepc\User\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Exception;
use Input;
use ApplicationException;
use Hash;
use BackendAuth;
use Flash;
use Rainlab\User\Models\Settings as UserSettings;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use DB;
/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class BackendAccess extends FormWidgetBase {

    public $alias = 'index';

    public function init()
    {

    }

    public function prepareVars()
    {
        if(UserSettings::get('judge_frontend_group',false) == false){
            throw new ApplicationException("Settings not set.");
        }

        $pamsFrontendGroups = CompanySettings::get('pams_frontend_groups');
        $pamsAdminFrontendGroups = CompanySettings::get('pams_admin_frontend_groups');

        $this->vars['isJudge'] = false;
        $this->vars['hasBackendAccess'] = false;
        foreach($this->model->roles as $role){
            if($role->id == UserSettings::get('judge_frontend_group')){
                $this->vars['isJudge'] = true;
                break;
            }
        }

        $this->vars['backenduser'] = $this->model->backenduser?$this->model->backenduser->id:null;

        if($this->vars['isJudge']){
            if($this->model->backenduser){
                $this->vars['hasBackendAccess'] = true;
            }
        }
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial($this->alias);
    }

    public function onActivateJudgeBackend()
    {
        if(UserSettings::get('judge_administrator_group',false) == false){
            throw new ApplicationException("Settings not set.");
        }

        $user = $this->model;
        $password = Hash::make($user->email);
        $backendUser = BackendAuth::register(array(
            'login' => $user->email,
            'email' => $user->email,
            'password' => $password,
            'password_confirmation' => $password
        ),true);

        $user->backenduser_id = $backendUser->id;
        $user->save();
        $backendGroupId = UserSettings::get('judge_administrator_group');
        Db::table('backend_users_groups')->insert(
            ['user_id' => $backendUser->id, 'user_group_id' => $backendGroupId]
        );
        Flash::success("Update successful");
        $this->prepareVars();
        return [
            '#judgeBackendAccess' => $this->makePartial($this->alias, $this->vars)
        ];
    }

    public function onDisableJudgeBackend()
    {
        $user = $this->model;
        $backenduser = BackendAuth::findUserById($user->backenduser->id);
        $backenduser->delete();
        $backenduser->afterDelete();
        $user->backenduser = null;
        Flash::success("Update successful");
        $this->prepareVars();
        return [
            '#judgeBackendAccess' => $this->makePartial($this->alias, $this->vars)
        ];
    }
}