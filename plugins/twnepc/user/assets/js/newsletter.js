$('input.email_updates').parent().on('click',function(){
    var event = $(this).find('input').val();
    var parent = $(this).parent();
    $.request('onSignup', {
        data: {
            listcode: parent.find('.listcode').val(),
            useremail: parent.find('.listmail').val(),
            firstname: parent.find('.listname').val(),
            lastname: parent.find('.listsurname').val(),
            subscribe: $(this).find('input').val() == 1 ? true : false
        },
        success: function() {
            var e =  event == 1 ? true : false;
            if(e) {
                $('#subscriberesult_'+parent.find('.listcode').val()).html('You have been added to the newsletter subscription list.');
            }else {
                $('#subscriberesult_'+parent.find('.listcode').val()).html('You have been removed from the newsletter subscription list.');
            }
        }
    });
});