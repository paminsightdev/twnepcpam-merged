<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;
use RainLab\User\Models\User;

class PasswordResetCreationDate extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(User::all()->count() == 0) {
                
                Schema::table('users', function($table)
                {
                    $table->timestamp('password_reset_creation_date')->nullable();
                });
            }
        }
    }

    public function down()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['password_reset_creation_date']);
            });
        }
    }
}
