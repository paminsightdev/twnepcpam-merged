<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;
use RainLab\User\Models\User;

class UsersTableRelatedTwnepcUsersIndustryTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(User::all()->count() == 0) {
                
                Schema::table('users', function ($table) {
                    $table->integer('industry_id')->unsigned()->nullable()->change();
                    $table->foreign('industry_id')->references('id')->on('twnepc_users_industry');
                });
            }
        }
    }
    
    public function down()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function ($table) {
                $table->dropForeign('users_industry_id_foreign');
                $table->dropColumn(['industry_id']);
            });
        }
    }
}