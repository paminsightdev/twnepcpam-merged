<?php namespace Twnepc\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\User\Models\UserAcceptSettings;

class BuilderTableUpdateTwnepcUserAcceptSettings extends Migration
{
    public function up()
    {
        if(Schema::hasTable('twnepc_user_accept_settings')) {
            // when there is no data put dummy one in
            if(UserAcceptSettings::all()->count() == 0) {
                
                Schema::table('twnepc_user_accept_settings', function($table)
                {
                    $table->timestamp('created_at')->nullable();
                    $table->timestamp('updated_at')->nullable();
                });
            }
        }
    }
    
    public function down()
    {
        Schema::table('twnepc_user_accept_settings', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
