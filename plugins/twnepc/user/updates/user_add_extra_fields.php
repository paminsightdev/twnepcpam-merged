<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;
use RainLab\User\Models\User;

class UserAddExtraFields extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(User::all()->count() == 0) {
                
                Schema::table('users', function($table)
                {
                    $table->string('industry', 100)->nullable();
                    $table->string('employees', 100)->nullable();
                    $table->string('department', 100)->nullable();
                    $table->string('street_addr_2', 100)->nullable();
                    $table->string('position', 100)->nullable();
                    $table->string('phone_ext', 100)->nullable();
                });
            }
        }
    }

    public function down()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['salutation']);
            });
        }
    }
}