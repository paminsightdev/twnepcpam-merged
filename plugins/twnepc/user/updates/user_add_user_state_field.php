<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;
use RainLab\User\Models\User;

class UserAddUserStateField extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(User::all()->count() == 0) {
                
                Schema::table('users', function($table)
                {
                    $table->string('user_state', 100)->nullable();
                });
            }
        }
    }
    
    
    public function down()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['user_state']);
            });
        }
    }
}
