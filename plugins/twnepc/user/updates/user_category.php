<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class UserCategory extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('category_user')) {
            
            Schema::create('category_user', function($table)
            {
                $table->engine = 'InnoDB';
                $table->integer('user_id')->nullable();
                $table->integer('category_id')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::drop('category_user');
    }
}