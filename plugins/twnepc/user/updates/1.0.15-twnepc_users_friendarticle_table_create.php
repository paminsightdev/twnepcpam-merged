<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcUsersFriendarticleTableCreate extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('twnepc_users_friendarticle')) {

            Schema::create('twnepc_users_friendarticle', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('user_id')->nullable();
                $table->integer('article_id')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->mediumText('comment')->nullable();
                $table->string('name', 100)->nullable();
                $table->string('company', 100)->nullable();
                $table->string('email', 100)->nullable();
            });
        }
    }

    public function down()
    {
        Schema::drop('twnepc_users_friendarticle');
    }
}
