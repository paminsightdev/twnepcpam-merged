<?php  namespace Twnepc\User\Updates;
use DB;
use Schema;
use October\Rain\Database\Updates\Migration;
use RainLab\User\Models\User;

class UserTableAddNewColumns extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(User::all()->count() == 0) {
                
                Schema::table('users', function($table)
                {
                    $table->integer('old_twn_member_id')->nullable();
                    $table->integer('old_epc_member_id')->nullable();
                    $table->integer('old_contact_id')->nullable();
                    $table->integer('old_company_id')->nullable();
                    $table->string('middlename', 100)->nullable()->after('surname');
                    $table->string('street_addr_3', 100)->nullable()->after('street_addr_2');
                    $table->string('fax', 30)->nullable()->after('phone');
                    $table->string('email2', 100)->nullable()->after('email');
                    $table->integer('old_type')->nullable();
                    $table->tinyInteger('contactby_phone')->nullable();
                    $table->tinyInteger('contactby_fax')->nullable();
                    $table->tinyInteger('contactby_email')->nullable();
                    $table->tinyInteger('contactby_pam')->nullable();
                    $table->tinyInteger('contactby_other')->nullable();
                    $table->tinyInteger('contact_never')->nullable();
                    $table->tinyInteger('nomagazine')->nullable();
                    $table->mediumText('comments')->nullable();
                    $table->renameColumn('industry', 'industry_id');
                    // $table->timestamp('created_at')->useCurrent()->change();
                    // $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))->change();
                });
            }
        }
    }
    
    
    public function down()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function ($table) {
                $table->dropColumn(['old_twn_member_id']);
                $table->dropColumn(['old_epc_member_id']);
                $table->dropColumn(['old_contact_id']);
                $table->dropColumn(['old_company_id']);
                $table->dropColumn(['middlename']);
                $table->dropColumn(['street_addr_3']);
                $table->dropColumn(['fax']);
                $table->dropColumn(['email']);
                $table->dropColumn(['old_type']);
                $table->dropColumn(['contactby_phone']);
                $table->dropColumn(['contactby_fax']);
                $table->dropColumn(['contactby_email']);
                $table->dropColumn(['contactby_pam']);
                $table->dropColumn(['contactby_other']);
                $table->dropColumn(['contact_never']);
                $table->dropColumn(['nomagazine']);
                $table->dropColumn(['comments']);
                $table->dropColumn(['industry_id']);
            });
        }
    }
}