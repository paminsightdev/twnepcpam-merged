<?php  namespace Twnepc\User\Updates;
use DB;
use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\User\Models\Industry;

class TwnepcUsersIndustryTableAddNewColumns extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('twnepc_users_industry')) {
            // when there is no data put dummy one in
            if(Industry::all()->count() == 0) {
            
                Schema::table('twnepc_users_industry', function($table)
                {
                    $table->timestamp('created_at')->useCurrent();
                    $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
                });
            }
        }
    }
    
    
    public function down()
    {
        if (Schema::hasTable('twnepc_users_industry')) {
            Schema::table('twnepc_users_industry', function ($table) {
                $table->dropColumn(['created_at']);
                $table->dropColumn(['updated_at']);
            });
        }
    }
}