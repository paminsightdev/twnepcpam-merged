<?php  namespace Twnepc\User\Updates;
use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcUsersSalesrepTableCreate extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('twnepc_users_salesrep')) {
            Schema::create('twnepc_users_salesrep', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('salesrep_oldtwn_id')->nullable();
                $table->integer('salesrep_oldepc_id')->nullable();
                $table->string('salesrep_name', 10)->nullable();
                $table->string('salesrep_longname', 100)->nullable();
                $table->tinyInteger('salesrep_active')->nullable();
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            });
        }
    }

    public function down()
    {
        Schema::drop('twnepc_users_salesrep');
    }
}