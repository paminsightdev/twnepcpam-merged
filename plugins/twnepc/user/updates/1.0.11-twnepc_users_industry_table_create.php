<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcUsersIndustryTableCreate extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('twnepc_users_industry')) {
            Schema::create('twnepc_users_industry', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('industry_name', 100)->nullable();
            });
        }
    }

    public function down()
    {
        Schema::drop('twnepc_users_industry');
    }
}