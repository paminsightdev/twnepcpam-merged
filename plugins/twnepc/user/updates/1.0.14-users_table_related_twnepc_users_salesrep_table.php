<?php  namespace Twnepc\User\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;
use RainLab\User\Models\User;

class UsersTableRelatedTwnepcUsersSalesrepTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('users')) {
            // when there is no data put dummy one in
            if(User::all()->count() == 0) {
                
                Schema::table('users', function ($table) {
                    $table->integer('salesrep_id')->unsigned()->nullable()->after('industry_id');
                    $table->foreign('salesrep_id')->references('id')->on('twnepc_users_salesrep');
                });
            }
        }
    }
    
    public function down()
    {
        if (Schema::hasTable('users')) {
            Schema::table('users', function ($table) {
                $table->dropForeign('users_salesrep_id_foreign');
                $table->dropColumn(['salesrep_id']);
            });
        }
    }
}