<?php  namespace Twnepc\User\Updates;
use DB;
use Schema;
use October\Rain\Database\Updates\Migration;


class ArticleFavUserTableAddNewColumns extends Migration
{
    public function up()
    {
        // check table exists
        if(Schema::hasTable('article_fav_user')) {
            // when there is no data put dummy one in
            if(DB::table('article_fav_user')->count() == 0) {
            
                Schema::table('article_fav_user', function($table)
                {
                    $table->increments('id')->first();
                    $table->timestamp('created_at')->useCurrent();
                });
            }
        }
    }

    public function down()
    {
        if (Schema::hasTable('article_fav_user')) {
            Schema::table('article_fav_user', function ($table) {
                $table->dropColumn(['id']);
                $table->dropColumn(['created_at']);
            });
        }
    }
}