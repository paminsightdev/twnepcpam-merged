<?php namespace Twnepc\User\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcUserAcceptSettings extends Migration
{
    public function up()
    {
        // check table exists
        if(!Schema::hasTable('twnepc_user_accept_settings')) {
            Schema::create('twnepc_user_accept_settings', function($table)
            {
                $table->engine = 'InnoDB';
                $table->integer('id');
                $table->integer('site_id');
                $table->boolean('is_accepted');
                $table->integer('user_id');
                $table->primary(['id']);
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_user_accept_settings');
    }
}
