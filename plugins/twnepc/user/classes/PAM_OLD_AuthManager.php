<?php namespace Twnepc\User\Classes;

use October\Rain\Auth\Manager as RainAuthManager;
use Rainlab\User\Models\Settings as UserSettings;

class AuthManager extends RainAuthManager
{
    protected static $instance;

    protected $sessionKey = 'user_auth';

    protected $userModel = 'Rainlab\User\Models\User';    //'Twnepc\User\Models\User';

    protected $groupModel = 'Rainlab\User\Models\UserGroup';    //'Twnepc\User\Models\UserGroup';

    protected $throttleModel = 'Rainlab\User\Models\Throttle';    //'Twnepc\User\Models\Throttle';

    public function init()
    {
        $this->useThrottle = UserSettings::get('use_throttle', $this->useThrottle);
        $this->requireActivation = UserSettings::get('require_activation', $this->requireActivation);
        parent::init();
    }

    public function authenticate(array $credentials, $remember = true)
    {
        $roles = UserSettings::get('user_roles_throttle_ignore',array());
        $login = array_get($credentials, 'login', null);
        $user = $this->findUserByLogin($login);
        if($user && is_array($roles)){
            foreach($user->roles as $role){
                if(in_array($role->id,$roles)){
                    $this->useThrottle = false;
                    break;
                }
            }
        }
        return parent::authenticate($credentials,$remember);
    }

    public function extendUserQuery($query)
    {
        $query->withTrashed();
    }
}