<?php

namespace Twnepc\User;

use Event;
use Backend;
use Backend\Facades\BackendMenu;
use Rainlab\Location\Models\Country;
use Twnepc\User\Components\Preferences;
use Rainlab\User\Models\User as UserModel;
use Twnepc\User\Components\Newslettersignup;
use Twnepc\User\Components\Subscriptioncheck;
use Rainlab\User\Controllers\Users as UsersController;

class Plugin extends \System\Classes\PluginBase
{
    public $require = ['RainLab.User'];
    

    public function pluginDetails()
    {
        return [
            'name' => 'twnepc.user::lang.plugin.name',
            'description' => 'twnepc.user::lang.plugin.name',
            'author' => 'Twnepc',
            'icon' => 'icon-leaf'
        ];
    }

    public function boot(){
        
        \Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            $controller->addJs('/plugins/twnepc/user/assets/js/setNo.js');

            //$controller->addCss('/plugins/twnepc/news/assets/css/spell_checker.min.css');
            //$controller->addJs('/plugins/twnepc/news/assets/js/spell_checker.min.js');
        });

        // Extend the navigation
        Event::listen('backend.menu.extendItems', function ($manager) {
            
            

            $manager->addSideMenuItems('Twnepc.User', 'industries-main', [
                'company-side-menu'=> [
                    'label'=> 'twnepc.company::lang.companies',
                    'url'=> Backend::url('twnepc/company/companies'),
                    'icon'=> 'icon-sitemap',
                    'code' => 'companies',
                    'owner' => 'Twnepc.Company',
                    'group'=> 'companies',
                ],
                'branch-company-side' => [
                    'label' => 'twnepc.company::lang.branches',
                    'url' => Backend::url('twnepc/company/branches'),
                    'icon'=> 'icon-sitemap',
                    'group'=>'companies',
                    'code' => 'branches',
                    'owner' => 'Twnepc.Company',
                ],
                'users' => [
                    'label' => 'Users',
                    'icon' => 'icon-users',
                    'code' => 'users',
                    'owner' => 'RainLab.User',
                    'group' => 'users',
                    'url' => Backend::url('rainlab/user/users')
                ],
                'salesreps' => [
                    'label' => 'Salesreps',
                    'icon' => 'icon-users',
                    'code' => 'salesreps',
                    'owner' => 'Twnepc.User',
                    'group' => 'users',
                    'url' => Backend::url('twnepc/user/salesreps')
                ],
                'industries' => [
                    'label' => 'Industries',
                    'icon' => 'icon-building',
                    'code' => 'industries',
                    'owner' => 'Twnepc.User',
                    'group' => 'users',
                    'url' => Backend::url('twnepc/user/industries')
                ],
            ]);

            $manager->addSideMenuItems('Twnepc.User', 'salesreps-main', [
                'company-side-menu'=> [
                    'label'=> 'twnepc.company::lang.companies',
                    'url'=> Backend::url('twnepc/company/companies'),
                    'icon'=> 'icon-sitemap',
                    'code' => 'companies',
                    'owner' => 'Twnepc.Company',
                    'group'=> 'companies',
                ],
                'branch-company-side' => [
                    'label' => 'twnepc.company::lang.branches',
                    'url' => Backend::url('twnepc/company/branches'),
                    'icon'=> 'icon-sitemap',
                    'group'=>'companies',
                    'code' => 'branches',
                    'owner' => 'Twnepc.Company',
                ],
                'users' => [
                    'label' => 'Users',
                    'icon' => 'icon-users',
                    'code' => 'users',
                    'owner' => 'RainLab.User',
                    'group' => 'users',
                    'url' => Backend::url('rainlab/user/users')
                ],
                'salesreps' => [
                    'label' => 'Salesreps',
                    'icon' => 'icon-users',
                    'code' => 'salesreps',
                    'owner' => 'Twnepc.User',
                    'group' => 'users',
                    'url' => Backend::url('twnepc/user/salesreps')
                ],
                'industries' => [
                    'label' => 'Industries',
                    'icon' => 'icon-building',
                    'code' => 'industries',
                    'owner' => 'Twnepc.User',
                    'group' => 'users',
                    'url' => Backend::url('twnepc/user/industries')
                ],
            ]);

          
        });
        
        UserModel::extend(function($model){
        
            $model->addJsonable( [
                'user_accept_settings'
            ]);
            $model->addFillable([

                'country_id',
                'salutation',
                'middlename',
                'employees',
                'street_addr_2',
                'street_addr_3',
                'gender',
                'not_paminsights',
                'no_products',
                'email2',
                'phone_ext',
                'fax',
                'user_state',
                'position',
                'classification',
                'comments',
                'contact_never',
                'contactby_phone',
                'contactby_fax',
                'contactby_email',
                'contactby_pam',
                'contactby_other',
                'nomagazine',
                'category',
                'company',
                'branch_id',
                'company_id',
                'user_accept_settings'

            ]);

            $model->belongsTo['thecompany'] = ['Twnepc\Company\Models\Company', 'key' => 'company_id'];
            $model->belongsTo['thebranch'] = ['Twnepc\Company\Models\Branch', 'key' => 'branch_id'];
            $model->hasOne['membership'] = ['Twnepc\Membership\Models\Membership'];
            $model->belongsTo['industry'] = ['Twnepc\User\Models\Industry'];
            $model->belongsToMany['category'] = ['Twnepc\News\Models\Category'];

            /*$model->bindEvent('model.beforeSave',function() use ($model) {
                $validator = \Validator::make($model->attributes, [
                    'industry' => 'required',
                    'position' => 'required',
                    'country' => 'required',
                    'phone' => 'required',

                ]);
    
                if ($validator->fails()) {
                    throw new \ValidationException([
                        'industry' => 'Industry is required.',
                        'position' => 'position is required',

                    ]);
                }
    
            });*/
            

        });
    
        UsersController::extendFormFields(function($form,$model,$context){
            
            $form->removeField('name');
            $form->removeField('surname');
            $form->removeField('company');
            $form->removeField('street_addr');
            $form->removeField('city');
            $form->removeField('country');
            $form->removeField('state');
            $form->removeField('zip');
            $form->removeField('phone');
            $form->removeField('mobile');
            $form->removeField('email');
            $form->removeField('groups');

            $form->addFields([
                'salutation' => [
 
                    'label' => 'Salutation',
                    'type'  => 'dropdown',
                    'options' => [
                        'Mr.' => 'Mr.',
                        'Mrs.' => 'Mrs.',
                        'Ms.' => 'Ms.',
                        'Dr.' => 'Dr.',
                        'Other' => 'Other',
                    ],
                    'span' => 'left'
                ],
                'name' => [
                    'label' => 'twnepc.user::lang.user.name',
                    'type' => 'text',
                    'span' => 'right',
                ],
                'middlename' => [
                    'label' => 'Middle name',
                    'type' => 'text',
                    'span' => 'left'
                ],
                'surname' => [
                    'label' => 'twnepc.user::lang.user.surname',
                    'type' => 'text',
                    'span' => 'right'
                ],
                'email' => [
                    'label' => 'twnepc.user::lang.user.email',
                    'type' => 'text',
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'email2' => [
                    'label' => 'twnepc.user::lang.user.email2',
                    'type' => 'text',
                    'span' => 'right',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                
                /*
                'gender' => [
                    'label' => 'twnepc.user::lang.user.gender',
                    'type'  => 'radio',
                    'options' => [
                        'male' => 'Male',
                        'female' => 'Female'
                    ],
                    'span' => 'right'
                ]
                */
                
                'gender' => [
                    'label'   => 'twnepc.user::lang.user.gender',
                    //'tab'     => 'indikator.user::lang.personal.tab',
                    'type'    => 'dropdown',
                    'options' => [
                        'unknown' => 'Unknown',
                        'female'  => 'Female',
                        'male'    => 'Male'
                    ],
                    'span'    => 'auto'
                ]
                
                
                
            ]);






            $form->addTabFields([
                
                'company' => [
                    'label' => 'rainlab.userplus::lang.user.company',
                    'type' => 'text',
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'position' => [
                    'label' => 'twnepc.user::lang.user.position',
                    'type' => 'text',
                    'tab' => 'twnepc.user::lang.user.personal_information',
                    'span' => 'left',
                ],
                'classification' => [
                    'label' => 'twnepc.user::lang.user.classification',
                    'type' => 'dropdown',
                    'options' => [
                        'institutional' => 'Institutional',
                        'professional' => 'Professional',
                        'retail' => 'Retail'
                    ],
                    'placeholder' => '-- select classification --',
                    'tab' => 'twnepc.user::lang.user.personal_information',
                    'span' => 'right',
                ],
                'street_addr' => [
                    'label' => 'rainlab.userplus::lang.user.street_addr',
                    'type' => 'text',
                    'span' => 'left',
                    'required' => true,
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'street_addr_2' => [
                    'label' => 'twnepc.user::lang.user.street_addr_2',
                    'type' => 'text',
                    'span' => 'right',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'street_addr_3' => [
                    'label' => 'twnepc.user::lang.user.street_addr_3',
                    'type' => 'text',
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'city' => [
                    'label' => 'rainlab.userplus::lang.user.city',
                    'type' => 'text',
                    'span' => 'right',
                    'required' => true,
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],

                'user_state' => [
                    'label' => 'twnepc.user::lang.user.state',
                    'type' => 'text',
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'country' => [
                    'label' =>'rainlab.location::lang.country.label',
                    'type' => 'dropdown',
                    'required' => true,
                    'placeholder' => 'rainlab.location::lang.country.select',
                    'tab' => 'twnepc.user::lang.user.personal_information',
                    'span' => 'right'
                ],
                'phone' => [
                    'label' => 'rainlab.userplus::lang.user.phone',
                    'type' => 'number',
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'phone_ext' => [
                    'label' => 'twnepc.user::lang.user.phone_ext',
                    'type' => 'number',
                    'span' => 'right',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'fax' => [
                    'label' => 'twnepc.user::lang.user.fax',
                    'type' => 'number',
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'comments' => [
                    'label' => 'twnepc.user::lang.user.comments',
                    'type' => 'textarea',
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'contactby_phone' => [
                    'label' => 'twnepc.user::lang.user.contactby_phone',
                    'type' => 'switch',
                    'options' => [
                        '0' => 'No',
                        '1' => 'Yes'
                    ],
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'contactby_fax' => [
                    'label' => 'twnepc.user::lang.user.contactby_fax',
                    'type' => 'switch',
                    'options' => [
                        '0' => 'No',
                        '1' => 'Yes'
                    ],
                    'span' => 'right',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'contactby_email' => [
                    'label' => 'twnepc.user::lang.user.contactby_email',
                    'type' => 'switch',
                    'options' => [
                        '0' => 'No',
                        '1' => 'Yes'
                    ],
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'contactby_pam' => [
                    'label' => 'twnepc.user::lang.user.contactby_pam',
                    'type' => 'switch',
                    'options' => [
                        '0' => 'No',
                        '1' => 'Yes'
                    ],
                    'span' => 'right',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'contactby_other' => [
                    'label' => 'twnepc.user::lang.user.contactby_other',
                    'type' => 'switch',
                    'options' => [
                        '0' => 'No',
                        '1' => 'Yes'
                    ],
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'contact_never' => [
                    'label' => 'twnepc.user::lang.user.contact_never',
                    'type' => 'switch',
                    'options' => [
                        '0' => 'No',
                        '1' => 'Yes'
                    ],
                    'span' => 'right',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'nomagazine' => [
                    'label' => 'twnepc.user::lang.user.nomagazine',
                    'type' => 'switch',
                    'options' => [
                        '0' => 'No',
                        '1' => 'Yes'
                    ],
                    'span' => 'left',
                    'tab' => 'twnepc.user::lang.user.personal_information'
                ],
                'thecompany' => [
                    'label' => 'twnepc.user::lang.user.company',
                    'nameFrom' => 'name',
                    'span' => 'left',
                    'type' => 'relation',
                    //'placeholder' => '-- select company --',
                    'emptyOption' => '-- no company --',
                    'tab' => 'twnepc.user::lang.user.company_information',
                    'scope' => 'order'
                ],
                'thebranch' => [
                    'label' => 'twnepc.user::lang.user.branch',
                    'nameFrom' => 'name',
                    'span' => 'left',
                    'dependsOn' => 'thecompany',
                    'type' => 'relation',
                    'scope' => 'FilterByCompany',
                    //'placeholder' => '-- select branch --',
                    'emptyOption' => '-- no branch --',
                    'tab' => 'twnepc.user::lang.user.company_information',
                ],
                'branch_address' => [
                    'label' => 'twnepc.user::lang.user.address_information',
                    'type' => 'partial',
                    'path' => '$/twnepc/user/models/user/_content_branch_address.htm',
                    'tab' => 'twnepc.user::lang.user.address_information'
                ],
                /*'category' => [
                    'label' => 'Interests',
                    'type' => 'relation',
                    'nameFrom' => 'name',
                    'tab' => 'twnepc.user::lang.user.interests'
                ],*/

                'category' => [
                    'label' => 'Interests',
                    'type'  => 'interests',
                    'tab'   => 'twnepc.user::lang.user.interests'
                ]
                
                /*
                'gender' => [
                    'label' => 'twnepc.user::lang.user.gender',
                    'type'  => 'radio',
                    'options' => [
                        'male' => 'Male',
                        'female' => 'Female'
                    ],
                    'span' => 'right'
                ]
                */
            ]);

           
            
         });
    }

    public function getCategoryOptions(){

    }
    
    public function register()
    {
        $this->registerConsoleCommand('twnepc.userpassword', 'Twnepc\User\Console\RecreateUserPassword');
        BackendMenu::registerContextSidenavPartial('Twnepc.User', 'salesreps-main', '$/twnepc/user/partials/_sidebar.htm');
        BackendMenu::registerContextSidenavPartial('Twnepc.User', 'industries-main', '$/twnepc/user/partials/_sidebar.htm');
        $this->registerConsoleCommand('user:defaultnewsletter', 'Twnepc\User\Console\UsersDefaultNewsletterCommand');
    }
    
    public function registerSchedule($schedule)
    {
        //$schedule->command('user:defaultnewsletter')->hourly();
    }

    public function registerFormWidgets()
    {
        return [
            'Twnepc\User\FormWidgets\SalesrepGrid' => 'salesrepgrid',
            'Twnepc\User\FormWidgets\IndustryGrid' => 'industrygrid',
            'Twnepc\User\FormWidgets\Interests' => 'interests',
        ];
    }

    public function registerNavigation()
    {
        return [
            'salesreps-main' => [
                'label' => 'Sales Represenatives',
                'permissions' => ['view_salesreps'],
                'hidden' => true,
                'url' => Backend::url('twnepc/user/salesreps'),
                'order' => 1000,
            ],
            'industries-main' => [
                'label' => 'Industries',
                'permissions' => ['view_industries'],
                'hidden' => true,
                'url' => Backend::url('twnepc/user/industries'),
                'order' => 1001,
            ],
            // pamonline menu item...
            'user' => [
                'label'       => 'PAM Users',
                'url'         => Backend::url('twnepc/user/users'),
                'icon'        => 'icon-users',
                'permissions' => ['pamonline.users.*'],
                'order'       => 500,
            ]
            
        ];
    }
    public function getCountryList(){

        return Country::all();

    }


    public function registerSettings()
    {
        return [
            
            'location' => [
                'label'       => 'Extended User Settings',
                'description' => 'Other settings for users',
                'category'    => 'Users',
                'icon'        => 'icon-globe',
                'class'       => 'Twnepc\User\Models\TwnepcUserSettings',
                'order'       => 500,
                'permissions' => ['view_salesreps','view_industries']
            ],
            
            // pamonline settings for users...
            'settings' => [
                'label'       => 'twnepc.user::lang.settings.menu_label',
                'description' => 'twnepc.user::lang.settings.menu_description',
                'category'    => 'Pamonline',
                'icon'        => 'icon-users',
                'class'       => 'Twnepc\User\Models\Settings',
                'order'       => 500,
                'permissions' => ['pamonline.users.settings']
            ]
        ];
    }
    

    public function registerComponents()
    {
        return [
            Subscriptioncheck::class => 'subscriptioncheck',
            Newslettersignup::class => 'newslettersignup',
            Preferences::class => 'preferences',
            
            //'Rainlab\User\Components\Session' => 'session',
            'Rainlab\User\Components\Account' => 'account',
            //'RainLab\User\Components\ResetPassword' => 'resetPassword'
            
        ];
    }
    
    
    public function registerPermissions()
    {
        return [
            'pamonline.users.access_users' => ['tab' => 'twnepc.user::lang.plugin.tab', 'label' => 'twnepc.user::lang.plugin.access_users'],
            'pamonline.users.access_groups' => ['tab' => 'twnepc.user::lang.plugin.tab', 'label' => 'twnepc.user::lang.plugin.access_groups'],
            'pamonline.users.access_settings' => ['tab' => 'twnepc.user::lang.plugin.tab', 'label' => 'twnepc.user::lang.plugin.access_settings']
        ];
    }
    
    
    public function registerReportWidgets()
    {
        return [
            'Twnepc\User\ReportWidgets\Users' => [
                'label'   => 'Users to activate',
                'context' => 'dashboard'
            ]
        ];
    }
    
    
    public function registerMailTemplates()
    {
        return [
            'twnepc.user::mail.activate'   => 'Activation email sent to new users.',
            'twnepc.user::mail.welcome'    => 'Welcome email sent when a user is activated.',
            'twnepc.user::mail.restore'    => 'Password reset instructions for front-end users.',
            'twnepc.user::mail.new_user'   => 'Sent to administrators when a new user joins.',
            'twnepc.user::mail.reactivate' => 'Notification for users who reactivate their account.',
        ];
    }
    

}
