<?php

namespace Twnepc\User\Components;

use DB;
use Auth;
use Keios\Multisite\Classes\SettingHelper;
use Lang;
use Event;
use Flash;
use Request;
use Redirect;
use Response;
use Cms\Classes\Page;
use ValidationException;
use Twnepc\News\Models\Gift;
use Cms\Classes\ComponentBase;
use Twnepc\News\Models\Article;
use RainLab\User\Models\UserGroup;

use Twnepc\Company\Models\Company;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Session;
use Twnepc\Membership\Models\Membership;
use Twnepc\Membership\Models\Subscription;
use Twnepc\Logs\Models\Article as ArticleLog;
/**
 * User session
 *
 * This will inject the user object to every page and provide the ability for
 * the user to sign out. This can also be used to restrict access to pages.
 */
class Subscriptioncheck extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Subscription Session Check',
            'description' => 'Subscription Session Check'
        ];
    }

    public function defineProperties()
    {
        return [
            'redirect' => [
                'title'       => 'rainlab.user::lang.session.redirect_title',
                'description' => 'rainlab.user::lang.session.redirect_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'redirectnosub' => [
                'title'       => 'twnepc.user::lang.user.redirect_no_sub_title',
                'description' => 'rainlab.user::lang.session.redirect_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ],
            'redirectnoactive' => [
                'title'       => 'twnepc.user::lang.user.redirect_no_active_title',
                'description' => 'rainlab.user::lang.session.redirect_desc',
                'type'        => 'dropdown',
                'default'     => ''
            ]
        ];
    }

    public function getRedirectOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getRedirectnosubOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function getRedirectnoactiveOptions()
    {
        return [''=>'- none -'] + Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Component is initialized.
     */
    public function init()
    {
    }

    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun()
    {

        if (!$this->checkSubscription()) {
            if (empty($this->property('redirect'))) {
                throw new \InvalidArgumentException('Redirect property is empty');
            }
            
            $redirectUrl = $this->controller->pageUrl($this->property('redirect'));
            $user = Auth::getUser();
            //if(null === $user) {
                 //return Redirect::guest($redirectUrl);
            //}
            

            $activesubscription = true;

            $subscription = $this->hasSubscription($user);
            if($subscription) {
                $membership = $this->getMembership($user);
                if($membership) {
                    $activesubscription = $this->hasActiveSubscription($membership);
                }
                
            }

            if(!$subscription) {
                $redirectUrl = $this->property('redirectnosub');
            }

            if(!$activesubscription) {
                $redirectUrl = $this->property('redirectnoactive');
            }
            
            // based on evaluated conditions redirect the user to the calculated default page, however...
            // do not redirect if the user wants to view an article, as there now is a requirement to display part of the article,
            // here we skip the redirect when the url is pointing to an article:
            if(stripos($this->currentPageUrl(), "/article/") === false) {
                return Redirect::guest($redirectUrl);
            }
        }

        $this->page['user'] = $this->user();
    }
    
    
    /**
     * Returns the logged in user, if available, and touches
     * the last seen timestamp.
     * @return RainLab\User\Models\User
     */
    public function user()
    {
        
        if (!$user = Auth::getUser()) {
            return null;
        }

        if (!Auth::isImpersonator()) {
            $user->touchLastSeen();
        }

        return $user;
    }
    
    
    private function getMembership($user) {
        $company = Company::find($user->company_id);
        if($company) {
            $branch = $user->thebranch;
            if($branch) {
                if(count($branch->membership) > 0) {
                    return $branch->membership;
                }
            }

            // check against company membership
            //if($company->membership->exists()) {
            if(count($company->membership) > 0) {
                return $company->membership;
            }
        }

        // check against user membership
        $user = Auth::getUser();
        //if($user->membership->exists()) {
        if(count($user->membership) > 0) {
            return $user->membership;
        }

        return false;
    }
    
    
    private function hasSubscription($user) {
        
        if(null === $user || empty($user)) {
            return false;
        }
        
        $company = Company::find($user->company_id);
        if($company) {
            $branch = $user->thebranch;
            if($branch) {
                if(count($branch->membership) > 0) {
                    return true;
                }
            }

            // check against company membership
            //if($company->membership->exists()) {
            if(count($company->membership) > 0) {
                return true;
            }
        }

        // check against user membership
        $user = Auth::getUser();
        //if($user->membership->exists()) {
        if(count($user->membership) > 0) {
            return true;
        }

        return false;
    }
    
    
    private function hasActiveSubscription($membership) {
        
        if($membership->status != 'Active') {
            return false;
        }
       
        // check start date
        if(strtotime(date('Y-m-d',strtotime($membership->start_date))) > strtotime(date('Y-m-d',strtotime('now')))) {
            return false;
        }
        
        // check end date
        if(strtotime(date('Y-m-d',strtotime($membership->end_date))) < strtotime(date('Y-m-d',strtotime('now')))) {
            return false;
        }

        return true;
    }
    
    
    /**
     * this is used for articles
     * @return boolean
     */
    public function checkSubscription() {
        // find article by slug
        $site = SettingHelper::getCurrentSite();
        $article = Article::where('slug',$this->param('slug'))
            ->where('site_id', $site->id)
            ->first();
        
        if(!$article){
           return true; // if no article nothing to restrict
        }
        
        
        // if it is a gifted article access is allowed
        if(static::isArticleAGift($article)) {
            return true;
        }

        
        // if user is logged in but it is a free article access is allowed
        if($article->is_free) {
            return true;
        }
        
        // if user is not logged access is denied
        $user = Auth::getUser();
        if(!$user) {
            return false;
        }

        $access = false;
        // as agreed company is stronger than user level
        $company = Company::find($user->company_id);


        if($company) {
            $branch = $user->thebranch;
            if($branch) {

                $memberships = Membership::where('branch_id',$branch->id)->get();
                if(count($memberships) > 0) {
                    foreach($memberships as $membership){
                        $subscription = Subscription::find($membership->subscription_id);
                        // check against branch membership
                        $access = $this->checkSubscriptionAgainstArticle($subscription,$article,$membership);

                        if($access){
                            Session::put('article_membership',$membership->id);
                            return true;
                        }
                    }

                }
            }

            $memberships = Membership::where('company_id',$company->id)->get();
            // check against company membership
            if(count($memberships) > 0) {
                foreach($memberships as $membership){
                    $subscription = Subscription::find($membership->subscription_id);
                    $access = $this->checkSubscriptionAgainstArticle($subscription,$article,$membership);

                    if($access){
                        Session::put('article_membership',$membership->id);
                        return true;
                    }
                }
            }
        }

        // check against user membership
        $user = Auth::getUser();

        $memberships = Membership::where('user_id',$user->id)->get();
        if(count($memberships) > 0)
        {
            foreach($memberships as $membership){
                  
                $subscription = Subscription::find($membership->subscription_id);
                $access = $this->checkSubscriptionAgainstArticle($subscription,$article,$membership);

                if($access){
                    Session::put('article_membership',$membership->id);
                    return true;
                }
            }
        }
        
        return false;
    }
    
    
    private function checkSubscriptionAgainstArticle($subscription,$article,$membership) {
        // check subscription status
        if($membership->status != 'Active') {
            return false;
        }
       
        // check start date
        if(strtotime(date('Y-m-d',strtotime($membership->start_date))) > strtotime(date('Y-m-d',strtotime('now')))) {
            return false;
        }
        
        // check end date
        if(strtotime(date('Y-m-d',strtotime($membership->end_date))) < strtotime(date('Y-m-d',strtotime('now')))) {
            return false;
        }

        
        // load regions data for both article and subscription
        $article_regions = $article->regionsToArray();
        $subscription_regions = $subscription->regionsToArray();

        // load site data for both article and subscription
        $article_site = $article->site;
        $subscription_sites = $subscription->sitesToArray();

        // load category data for both article and subscription
        $article_main_categories = $article->getMainCategories();
        $subscription_categories = $subscription->categoriesToArray();

        // check site access
        if(!in_array($article_site->id,$subscription_sites)) {
            return false;
        }
        
      
        
        // check region access
        if((count($subscription_regions) && count($article_regions)) && empty(array_intersect($article_regions, $subscription_regions))) {
            return false;
        }

        

        // check category access
        if((count($subscription_categories) && count($article_main_categories)) && empty(array_intersect($article_main_categories, $subscription_categories))) {
           return false;
        }


        // get visit count for user and check if this visit is allowed
        $user = Auth::getUser();
        $visits = ArticleLog::groupBy('user_id')->where('user_id',$user->id)->count();
        if($subscription->limit > 0) {
            if($subscription->limit < $visits+1 ) {
                return false;
            }
        }
        
        return true;
    }
    
    
    /**
     *  if it is a gifted article access is allowed
     */
    public static function isArticleAGift($article) {

        // if it is a gifted article access is allowed
        $gift = Request::get('gift');
        if($gift) {
            $gift = Crypt::decrypt($gift);
            $isGift = Gift::where('hash',$gift)->where('article_id',$article->id);
            if($isGift) {
                return true;
            }
        }

        return false;
    }
    
    
}
