<?php namespace Twnepc\User\Components;

use Mail;
use Request;
use Exception;
use Validator;
use Cms\Classes\Page;
use ValidationException;
use ApplicationException;
use Cms\Classes\ComponentBase;
use RainLab\User\Facades\Auth;
use Responsiv\Campaign\Models\Subscriber;
use Responsiv\Campaign\Models\SubscriberList;

use Twnepc\Logs\Models\PamSplunkTroubleShooting;

class Newslettersignup extends ComponentBase
{
    public $listCode = '';
    public $isSubscribed = false;
    public $loadData = true;

    public function componentDetails()
    {
        return [
            'name'        => 'Newsletter signup',
            'description' => 'Sign up a new person to a campaign mailing list.'
        ];
    }
    public function defineProperties()
    {
        return [
            'list' => [
                'title'       => 'Add to list',
                'description' => 'The campaign list code to subscribe the person to.',
                'type'        => 'dropdown'
            ],
            'confirm' => [
                'title'       => 'Require confirmation',
                'description' => 'Subscribers must confirm their email address.',
                'type'        => 'checkbox',
                'default'     => 0,
                'showExternalParam' => false
            ],
            'loadData' => [
                'title'       => 'Load data',
                'description' => 'Loads data from database and sets value',
                'type'        => 'checkbox',
                'default'     => 1,
                'showExternalParam' => false
            ],
            'templatePage' => [
                'title'       => 'Confirmation page',
                'description' => 'If confirmation is required, select any mail template used for generating a confirmation URL link.',
                'type'        => 'dropdown',
                'showExternalParam' => false
            ],
        ];
    }

    public function onRun() {
        $this->addJs('assets/js/newsletter.js');
        $this->listCode = $listCode = $this->property('list');
        $this->loadData = $loadData = $this->property('loadData');
        $this->page['user'] = $user = Auth::getUser();
        if($this->loadData){
            $list = SubscriberList::where('code',$listCode)->first();
            if($user && $list)  {
                
                $this->isSubscribed = $isSubscribed = $list->subscribers()->where('responsiv_campaign_subscribers.email',$user->email)->count() > 0;
            } else {

                $this->isSubscribed = $isSubscribed = false;
            }
        }
        
        /* troubleshooting log - test on test server first */
        // (null !== $this->listCode && !empty($this->listCode)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/twnepc/user/components/Newslettersignup.php','onRun','listCode', $this->listCode) : '';
        // (null !== $user->email && !empty($user->email)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/twnepc/user/components/Newslettersignup.php','onRun','email', $user->email) : '';
        // (null !== $this->loadData && !empty($this->loadData)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/twnepc/user/components/Newslettersignup.php','onRun','loadData', $this->loadData) : '';
        // (null !== $this->$this->isSubscribed && !empty($this->$this->isSubscribed)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/twnepc/user/components/Newslettersignup.php','onRun','isSubscribed', $this->isSubscribed) : '';
    }

    public function getListOptions()
    {
        return SubscriberList::orderBy('name')->lists('name', 'code');
    }

    public function onSignup()
    {
        $data = post();

        $d = [
            'email' => $data['useremail'],
            'first_name' => $data['firstname'],
            'last_name' => $data['lastname'],
            'listcode' => $data['listcode'],
            'subscribe' => $data['subscribe']
        ];

        $this->listSubscribe($d);

    }

    protected function listSubscribe(array $data)
    {
        $listCode = array_get($data,'listcode');

        $requireConfirmation = $this->property('confirm', false);
        $user = Auth::getUser();
        $subscriber = Subscriber::signup([
            'email' => array_get($data, 'email'),
            'first_name' => array_get($data, 'first_name'),
            'last_name' => array_get($data, 'last_name'),
            'created_ip_address' => Request::ip()
        ], $listCode, !$requireConfirmation, $user->id);

        

        if(array_get($data,'subscribe') == 'false') {
            $attempt = $subscriber->attemptUnsubscribe($listCode);
            // paulo - put back, temporary adjustment, will remove soon
            // this command is no longer required as the feature does not persist to this field
            // email_updates is no longer used instead subscriber newsletter should be used
            //$user->email_updates = 0;
        }
        else{
            // paulo - put back, temporary adjustment, will remove soon
            // this command is no longer required as the feature does not persist to this field
            // email_updates is no longer used instead subscriber newsletter should be used
            //$user->email_updates = 1;
        }
        // paulo - put back, temporary adjustment, will remove soon
        // this command is no longer required as the feature does not persist to this field
        // email_updates is no longer used instead subscriber newsletter should be used
        //$user->save();
        
        /*
         * Send confirmation email
         */
        if (!$subscriber->confirmed_at) {
            $params = [
                'confirmUrl' => $this->getConfirmationUrl($subscriber)
            ];

            Mail::sendTo($subscriber->email, 'responsiv.campaign::mail.confirm_subscriber', $params);
        }

        return $subscriber;
    }

    protected function getConfirmationUrl($subscriber)
    {
        $pageName = $this->property('templatePage');

        return Page::url($pageName, ['code' => $subscriber->getUniqueCode()]) . '?verify=1';
    }

    /**
     * Returns true if user is throttled.
     */
    protected function checkThrottle()
    {
        return Subscriber::checkThrottle(Request::ip());
    }


}