<?php namespace Twnepc\User\Components;

use Event;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use RainLab\User\Facades\Auth;
use Twnepc\News\Models\Category;
use Illuminate\Support\Facades\Lang;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Redirect;
use Keios\Multisite\Classes\SettingHelper;
use Twnepc\User\Models\UserAcceptSettings;

class Preferences extends ComponentBase
{
    public $user;
    public $interests;
    public $catids;
    public $sitesallsubject;
    public $usercategory;
    public $site;
    public $newsletter_title;

    public function componentDetails()
    {
        return [
            'name'        => 'User Prefrences',
            'description' => 'User Prefernces'
        ];
    }

    public function defineProperties()
    {
        return [
            'newsletter_title' => [
                'title'       => 'Newsletter Title',
                'type'        => 'text',
                'default'     => ''
            ]
        ];
    }

    /**
     * Component is initialized.
     */
    public function init()
    {

    }

    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun()
    {
        $user = $this->user = Auth::getUser();
        if($user){
            $site = $this->site = SettingHelper::getCurrentSite();
            $interests = $this->interests = $this->getAllInterests($site);
            $usercategory = $this->usercategory =  $user->category;
            $this->newsletter_title = $this->property('newsletter_title');

            $catids = [];
            foreach($usercategory as $c) {
                array_push($catids,$c->id);
            }
            $this->catids = $catids;

            $this->sitesallsubject =  DB::table('keios_multisite_settings as site')
                ->select('interests.site_id','interests.parent_id','interests.is_hidden','site.id as id','site.theme','interests.id as categoryid','interests.name','interests.identifier')
                ->where('interests.identifier','all-subjects')
                ->leftJoin('twnepc_news_categories as interests','site.id','interests.site_id')->get();
        }

    }

    public function getSubInterests($site, $category, $level, $selections = []){
        $html = '';

        $children = $category->children()->get();
        foreach($children as $child){
            if(!$child->is_hidden){
                $html .= '<div data-site="'.$site->id.'" class="interest custom-control custom-checkbox level-'.$level.'">';
                $html .= '<input type="checkbox" '.(in_array($child->id,$selections) ? 'checked' : '').' class="custom-control-input" name="category[]" value="'.$child->id.'" id="iterests'.$child->id.'">';
                $html .= '<label class="custom-control-label" for="iterests'.$child->id.'">'.$child->name.'</label>';
                $html .= '<div class="subInterests level-'.$level.'">'.$this->getSubInterests($site, $child, $level+1, $selections).'</div>';
                $html .= '</div>';

            }
        }

        return $html;
    }

    public function getAllInterests($site, $selections = []){
        $topLevelCategories = Category::where('site_id',$site->id)->where('is_hidden',0)->where('parent_id',NULL)->get();
        $level = 1;
        $html = '';
        foreach($topLevelCategories as $category){
            if($category->children->count()){
                $html .= '<strong>'.$category->name.'</strong>';
                $html .= '<div class="interests site-'.$site->id.' level-'.$level.'" data-site="'.$site->id.'">';
                $html .= $this->getSubInterests($site, $category, $level+1, $selections);
                $html .= '</div>';
            }
        }

        return $html;
    }

    public function onUpdateMyInterests(){
        $user = Auth::getUser();
        
        $data = post();
        $site = SettingHelper::getCurrentSite();

        $getCurrentUserCatgories = $user->category;

        if(!isset($data['category'])){
            $data['category'] = [];
        }

        foreach($getCurrentUserCatgories as $c) {
            if(!in_array($c->id,$data['category'])) {
                array_push($data['category'],$c->id);
            }
        }
        

        $user->category = $data['category'];

        $user->save();
        
        $settingData = [
            'site_id' => $site->id,
            'user_id' => $user->id,
            'is_accepted' => 1
        ]; 

        $setting = UserAcceptSettings::firstOrCreate($settingData);

        Event::fire('twnepc.user.preference', [$user, $site, $data]);

        Flash::success(post('flash', Lang::get('rainlab.user::lang.account.success_saved')));
        
        $article_url = Session::get('article_url');


        $site = SettingHelper::getCurrentSite();

        $domain = $site->domain;

        $is_same_site = (strpos($article_url,$domain) !== false);

        if($article_url && $is_same_site) {
            Session::forget('article_url');
            return Redirect::to($article_url);

        } else {
            return Redirect::to($site->domain);
        }
    }
}