<?php namespace Twnepc\User\Models;

use Model;

/**
 * Model
 */
class Salesrep extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_users_salesrep';


    // public $belongsToMany = [
    //     'users' => 'Rainlab\User\Models\User',
    //     'branches' => 'Twnepc\Company\Models\Branch'
    // ];
    // 
    // public $hasOne = [
    //     'membership' => 'Twnepc\Membership\Models\Membership'
    // ];
}
