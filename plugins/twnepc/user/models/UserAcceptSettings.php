<?php namespace Twnepc\User\Models;

use Model;

/**
 * Model
 */
class UserAcceptSettings extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $fillable = [
        'site_id',
        'user_id',
        'is_accepted'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_user_accept_settings';
}
