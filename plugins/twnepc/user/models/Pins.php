<?php namespace Twnepc\user\Models;

use Model;

/**
 * Model
 */
class Pins extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companypinsusers';

    public function generatePIN()
    {
        $sixNumberPin = mt_rand(100000, 999999);
        return $sixNumberPin;
    }
}
