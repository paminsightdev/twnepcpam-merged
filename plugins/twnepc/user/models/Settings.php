<?php namespace Twnepc\User\Models;

use Lang;
use Model;
use System\Models\MailTemplate;
use Rainlab\User\Models\User as UserModel;
use Twnepc\Roles\Models\Group as FrontendGroup;
use Backend\Models\UserGroup as BackendGroup;
use Twnepc\Pamcompanies\Models\Companies;


class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'user_settings';
    public $settingsFields = 'fields.yaml';

    const ACTIVATE_AUTO = 'auto';
    const ACTIVATE_USER = 'user';
    const ACTIVATE_ADMIN = 'admin';

    const LOGIN_EMAIL = 'email';
    const LOGIN_USERNAME = 'username';

    public function initSettingsData()
    {
        $this->require_activation = true;
        $this->activate_mode = self::ACTIVATE_AUTO;
        $this->use_throttle = true;
        $this->allow_registration = true;
        $this->welcome_template = 'twnepc.user::mail.welcome';
        $this->login_attribute = self::LOGIN_EMAIL;
    }

    public function getActivateModeOptions()
    {
        return [
            self::ACTIVATE_AUTO => ['twnepc.user::lang.settings.activate_mode_auto', 'twnepc.user::lang.settings.activate_mode_auto_comment'],
            self::ACTIVATE_USER => ['twnepc.user::lang.settings.activate_mode_user', 'twnepc.user::lang.settings.activate_mode_user_comment'],
            self::ACTIVATE_ADMIN => ['twnepc.user::lang.settings.activate_mode_admin', 'twnepc.user::lang.settings.activate_mode_admin_comment'],
        ];
    }

    public function getLoginAttributeOptions()
    {
        return [
            self::LOGIN_EMAIL => ['twnepc.user::lang.login.attribute_email'],
            self::LOGIN_USERNAME => ['twnepc.user::lang.login.attribute_username'],
        ];
    }

    public function getAllowedRegistrationTypesOptions()
    {
        $settings = [];
        $roles = FrontendGroup::all();
        foreach($roles as $role){
            $settings[$role->id] = $role->name;
        }
        return $settings;
    }

    public function getUserRolesThrottleIgnoreOptions()
    {
        $settings = [];
        $roles = FrontendGroup::all();
        foreach($roles as $role){
            $settings[$role->id] = $role->name;
        }
        return $settings;
    }

    public function getActivateModeAttribute($value)
    {
        if (!$value) {
            return self::ACTIVATE_AUTO;
        }

        return $value;
    }

    public function getWelcomeTemplateOptions()
    {
        $codes = array_keys(MailTemplate::listAllTemplates());
        $result = [''=>'- '.Lang::get('twnepc.user::lang.settings.no_mail_template').' -'];
        $result += array_combine($codes, $codes);
        return $result;
    }

    public function getJudgeFrontendGroupOptions()
    {
        $settings = [];
        $roles = FrontendGroup::all();
        foreach($roles as $role){
            $settings[$role->id] = $role->name;
        }
        return $settings;
    }

    public function getJudgeAdministratorGroupOptions()
    {
        $options = [];
        $groups = BackendGroup::all();
        foreach($groups as $group){
            $options[$group->id] = $group->name;
        }
        return $options;
    }

    public function getUserCompanyByDefaultOptions()
    {
        $options = [];
        $companies = Companies::get();
        foreach($companies as $company){
            $options[$company->c_companyname] = $company->c_companyname;
        }
        return $options;
    }

}