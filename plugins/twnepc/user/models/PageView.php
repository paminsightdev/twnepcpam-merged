<?php namespace Twnepc\user\Models;

use Model;

/**
 * Model
 */
class PageView extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $jsonable = ['server'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'page_views';

    public $belongsTo = [
        'user' => [
            'Rainlab\User\Models\User',
            'key' => 'user_id',
            'otherKey' => 'id'
        ],
        'user_name' => [
            'Rainlab\User\Models\User',
            'key' => 'user_id',
            'otherKey' => 'id'
        ]
    ];

    public $belongsToMany = [
        'roles' => [
            'Twnepc\Roles\Models\UserGroup',
            'table' => 'pamonline_roles_assigned_roles',
            'key'=>'user_id', 'otherKey' => 'role_id'
        ]
    ];

    public function scopeFilterByGroup($query, $filter)
    {
        return $query->whereHas('roles', function($group) use ($filter) {
            $group->whereIn('role_id', $filter);
        });
    }

    public function scopeFilterByUsers($query, $filter)
    {
        var_dump($filter);die();
//        return $query->whereHas('user', function($user) use ($filter) {
//            $user->whereIn('id', $filter);
//        });
    }
}
