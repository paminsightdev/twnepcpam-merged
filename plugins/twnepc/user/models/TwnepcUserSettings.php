<?php namespace Twnepc\User\Models;

use Model;

class TwnepcUserSettings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'twnepc_user_settings';

    // Reference to field configuration
    public $settingsFields = 'twnepc_user_settings_fields.yaml';
}