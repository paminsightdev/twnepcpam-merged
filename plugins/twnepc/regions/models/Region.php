<?php namespace Twnepc\Regions\Models;

use Model;

/**
 * Model
 */
class Region extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_regions_region';

    public $belongsToMany = [
        'sites' => 'Keios\Multisite\Models\Setting'
    ];

}
