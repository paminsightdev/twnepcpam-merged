<?php namespace Twnepc\Regions;

use System\Classes\PluginBase;
use Twnepc\Regions\Components\Regions as RegionsComponent;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            RegionsComponent::class => 'regions',
        ];
    }

    public function registerSettings()
    {
        
    }
}
