<?php return [
    'plugin' => [
        'name' => 'Site Regions',
        'description' => 'Setting up Regions for website',
        'list_name' => 'Name',
        'list_identifier' => 'Identifier',
        'list_created_at' => 'Created at',
        'list_updated_at' => 'Updated at',
        'deleted_at' => 'Deleted at',
        'main_menu_name' => 'Regions',
        'region_main_menu' => 'Regions',
    ],
];