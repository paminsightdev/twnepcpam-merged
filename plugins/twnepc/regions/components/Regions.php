<?php namespace Twnepc\Regions\Components;

use Lang;
use Response;
use Exception;
use SystemException;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Twnepc\Widgets\Classes\ComponentHelper;

class Regions extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.regions::lang.components.regions.name',
            'description' => 'twnepc.regions::lang.components.regions.description'
        ];
    }

    public function defineProperties() {
        return [
           
            'site' => [
                'title'             => 'Site',
                'type'              => 'dropdown',
                'showExternalParam' => false
            ]
        ];
    }

    public function getSiteOptions()
    {
        return ComponentHelper::instance()->listSites();
    }

    public function onRun() {

        
    }

}