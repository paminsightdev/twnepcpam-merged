<?php namespace Twnepc\Regions\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddRegionSetting extends Migration
{
    public function up()
    {
        Schema::create('region_setting', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('setting_id');
            $table->integer('region_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('region_setting');
    }
}
