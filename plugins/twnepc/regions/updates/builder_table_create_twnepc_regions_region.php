<?php namespace Twnepc\Regions\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcRegionsRegion extends Migration
{
    public function up()
    {
        Schema::create('twnepc_regions_region', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255)->nullable()->default('NULL');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_regions_region');
    }
}
