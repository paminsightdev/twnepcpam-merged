<?php namespace Twnepc\Regions\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcRegionsRegion2 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_regions_region', function($table)
        {
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->string('name', 255)->default('NULL')->change();
            $table->string('identifier', 255)->default('NULL')->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_regions_region', function($table)
        {
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
            $table->dropColumn('deleted_at');
            $table->string('name', 255)->default('\'NULL\'')->change();
            $table->string('identifier', 255)->default('\'NULL\'')->change();
        });
    }
}
