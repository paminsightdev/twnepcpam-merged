<?php namespace Twnepc\Regions\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcRegionsRegion extends Migration
{
    public function up()
    {
        Schema::table('twnepc_regions_region', function($table)
        {
            $table->string('identifier', 255)->nullable()->default('NULL');
            $table->increments('id')->unsigned(false)->change();
            $table->string('name', 255)->default('NULL')->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_regions_region', function($table)
        {
            $table->dropColumn('identifier');
            $table->increments('id')->unsigned()->change();
            $table->string('name', 255)->default('\'NULL\'')->change();
        });
    }
}
