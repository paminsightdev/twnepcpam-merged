<?php namespace Twnepc\Paywithstripe;

use BackendAuth;
use System\Classes\PluginBase;
//use Backend\Facades\BackendAuth;

/**
 * paywithstripe Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'paywithstripe',
            'description' => 'Provides API linking PAM to Stripe Credit Card payment gateway',
            'author'      => 'twnepc',
            'icon'        => 'icon-cc-stripe'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Twnepc\Paywithstripe\Components\CreateSession' => 'createSession',
            'Twnepc\Paywithstripe\Components\Webhook' => 'webhook',
            'Twnepc\Paywithstripe\Components\ListWebhooks' => 'listwebhooks',            
            'Twnepc\Paywithstripe\Components\ListPaymentLinks' => 'listpaymentlinks',      
        ];
    }

    
    public function registerSettings() {
        $backendUser = BackendAuth::getUser();
        if($backendUser->is_superuser == 1) {
            
            // display configuration setting for Stripe 
            // only super user can change API keys for Stripe
            return [
                'settings' => [
                    'label'       => 'PayWithStripe',
                    'icon'        => 'icon-cc-stripe',
                    'description' => 'Configure Stripe API access.',
                    'class'       => 'Twnepc\PayWithStripe\Models\Settings',
                    'order'       => 600
                ]
            ];
            
        } else {
            // hide configuration setting for Stripe Settings from ordinary users
            return [];
        }

    }
}
