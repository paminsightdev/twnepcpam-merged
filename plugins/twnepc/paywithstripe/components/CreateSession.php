<?php namespace Twnepc\Paywithstripe\Components;

// require_once('vendor/autoload.php');
// not using vendor/autoload, because stripe\stripe-php has not been installed using composer.
// The Stripe files have been downloaded and 

require_once(plugins_path() . '/twnepc/paywithstripe/vendor/init.php');

use Cms\Classes\ComponentBase;
use Twnepc\Paywithstripe\Models\Settings;

use Twnepc\Roles\Models\Group;
use Twnepc\User\Models\Role;

class CreateSession extends ComponentBase
{
    /** 
    * The Public and Secret keys from Stripe 
    * @var string
    */
    public $public_key ;
    public $acct_id ;
    public $success_url ;
    public $cancel_url ;
    public $sessionId ;
    public $products;
    public $product_prices;
    public $images;
    public $cards;
    public $session ;
    protected $stripe;

    public function componentDetails() {
        return [
            'name'        => 'CreateSession Component',
            'description' => 'Creates an API session that remember the user as they migrate from PAM, to Stripe, and then back to PAM after successful payment.'
        ];
    }

    public function defineProperties() {
        return [];
    }

    public function init() {
        // this code is run when the layout/template/page that contains this Component, 
        // is loaded via Ajax 
    }

    public function sendThanksEmail($email) {

        $mail = new PHPMailer();

        // Settings
        $mail->IsSMTP();
        $mail->CharSet = 'UTF-8';
        
        $mail->Host       = "smtp.example.com";    // SMTP server example
        $mail->SMTPDebug  = 0;                     // enables SMTP debug information (for testing)
        $mail->SMTPAuth   = true;                  // enable SMTP authentication
        $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
        $mail->Username   = "username";            // SMTP account username example
        $mail->Password   = "password";            // SMTP account password example
        
        // Content
        $mail->isHTML(true);                       // Set email format to HTML
        $mail->Subject = 'Here is the subject';
        $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
        $mail->send();
    }


    protected function createStripeSession() {
        // retrieve pamonline_paywithstripe_settings field (defined in models/settings.php) 
        // from Settings table in DB Database 
        
        $settings = Settings::instance();

        // check that all the required elements are in
        // the pamonline_paywithstripe_settings field.

        if (!$settings->public_key) {
            throw new ApplicationException('Stripe Public API key is not configured.');
        }
        if (!$settings->secret_key) {
            throw new ApplicationException('Stripe Secret API key is not configured.');
        }
        if (!$settings->acct_id) {
            throw new ApplicationException('Stripe Acct ID is not configured.');
        }
        if (!$settings->success_url) {
            throw new ApplicationException('Stripe Success URL is not configured.');
        }
        if (!$settings->cancel_url) {
            throw new ApplicationException('Stripe Cancel URL is not configured.');
        }
       
        $this->public_key   = $settings->public_key ;
        $this->success_url  = $settings->success_url ;
        $this->cancel_url   = $settings->cancel_url ;

        /* Creates the Stripe object */
        $this->stripe = new \Stripe\StripeClient($settings->secret_key);

    }


    public function onRun() {
        // this code is run when the layout/template/page that contains this Component, loads
        // in a non-Ajax environment 

        $this->createStripeSession();
        $this->listAllProducts();

        $this->session = $this->stripe->checkout->sessions->create([
            'success_url'           => $this->success_url ,
            'cancel_url'            => $this->cancel_url ,
            'payment_method_types'  => ['card'],
            'mode'                  => 'subscription',
            'line_items'            => [
                [
                'price'             => 'price_1KGPC8Lyvna2Btk7TYRxR6t6',
                'quantity'         => 1
                ],
                [
                'price'             => 'price_1KF11sLyvna2Btk7KohHDxiI',
                'quantity'         => 1
                ]]
        ]);
    
        $this->sessionId = $this->session->id ;

        echo json_encode($this->session);

        /* prod_KwHlB3E4gAt0Ie */

    }

    public function listAllProducts() {
        $this->products         =   $this->stripe->products->all(['active' => true]);
        $this->product_prices   =   $this->products->price;
        $this->images =     $this->products->images;

    }



    public function onSubscribe() {
            // do nothing
    }


}

