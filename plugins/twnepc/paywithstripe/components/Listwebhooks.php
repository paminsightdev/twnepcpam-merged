<?php namespace Twnepc\Paywithstripe\Components;

require_once(plugins_path() . '/twnepc/paywithstripe/vendor/init.php');

use Cms\Classes\ComponentBase;
use Twnepc\Paywithstripe\Models\Settings;

class ListWebhooks extends ComponentBase
{
    /** 
    * The Public and Secret keys from Stripe 
    * @var string
    */
    public $endpoints;
    protected $stripe;

    public function componentDetails() {
        return [
            'name'        => 'ListWebhooks Component',
            'description' => 'Creates an API session that interogates the Stripe account, and lists the webhooks'
        ];
    }

    public function defineProperties() {
        return [];
    }

    protected function createStripeSession() {
        // retrieve pamonline_paywithstripe_settings field (defined in models/settings.php) 
        // from Settings table in DB Database 
        
        $settings = Settings::instance();
  
        // check that all the required elements are in
        // the pamonline_paywithstripe_settings field.
  
        if (!$settings->public_key) {
            throw new ApplicationException('Stripe Public API key is not configured.');
        }
        if (!$settings->secret_key) {
            throw new ApplicationException('Stripe Secret API key is not configured.');
        }
        if (!$settings->acct_id) {
            throw new ApplicationException('Stripe Acct ID is not configured.');
        }
       
        /* Creates the Stripe object */
        $this->stripe = new \Stripe\StripeClient($settings->secret_key);

        $this->endpoints = $this->stripe->webhookEndpoints->all();
  
    }    

    public function init() {
        // this code is run when the layout/template/page that contains this Component, 
        // is loaded via Ajax 
    }


    public function onRun() {
        // this code is run when the layout/template/page that contains this Component, loads
        // in a non-Ajax environment 

        $this->createStripeSession();

    }

}
