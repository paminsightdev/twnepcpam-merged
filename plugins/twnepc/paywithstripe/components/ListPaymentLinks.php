<?php namespace Twnepc\Paywithstripe\Components;

use Cms\Classes\ComponentBase;

class ListPaymentLinks extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'ListPaymentLinks Component',
            'description' => 'Connects to Stripe via APi to list the available Payment Links that ca be used to quickly and easily link to Payment for PAM products/subscriptions.'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
