<?php namespace Twnepc\Paywithstripe\Components;

// require_once('vendor/autoload.php');
// not using vendor/autoload, because stripe\stripe-php has not been installed using composer.
// The Stripe files have been downloaded and 

require_once(plugins_path() . '/twnepc/paywithstripe/vendor/init.php');

use Cms\Classes\ComponentBase;
use Twnepc\Paywithstripe\Models\Settings;
use RainLab\User\Models\Settings as UserSettings;
use RainLab\User\Models\User as userModel;


class Webhook extends ComponentBase
{
    public $session ;
    public $customer ;
    public $source ;
    public $subscription ;
    public $event;
    public $firstName;
    public $lastName;


    public function componentDetails()
    {
        return [
            'name'        => 'Webhook Component',
            'description' => 'Captures data sent from Stripe, when asynchronous events happen on the Stripe accocunt. e.g. Someone pays. These events can happen asynchronously, and not necessarily as a result of clicking a Subscribe Now" button on the PAM website, although the Stripe events that this Webhook catpures, will mostly be because of that.'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    protected function createStripeSession() {
      // retrieve pamonline_paywithstripe_settings field (defined in models/settings.php) 
      // from Settings table in DB Database 
      $settings = Settings::instance();

      // check that all the required elements are in
      // the pamonline_paywithstripe_settings field.

      if (!$settings->secret_key) {
          throw new ApplicationException('Stripe Secret API key is not configured.');

      /* Creates the Stripe object from the secret_key stored in the settings 
      fields in the database
      */
      $this->stripe = new \Stripe\StripeClient($settings->secret_key);
      }
    }

    // populate the password field with a random password.
    public function rand_string($length) {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      return substr(str_shuffle($chars),0,$length);
    }


    public function splitName($namestring,$ForL) {
        $parts      = explode (" ", $namestring) ;
        $lastname   = array_pop($parts);
        $firstname  = implode(" ", $parts); 
        if ($ForL == 'f') {
          return $firstname ;
        } 
        else {
          return $lastname ;
        }
    }


    public function onRun() {
      // webhook.php
      // copied from:  https://dashboard.stripe.com/test/webhooks/create
      // Use this sample code to handle webhook events in your integration.
      
      // Secret Code, which points to a particular Webpage which contains a Webhook.
      // Different endpoint_secrets for different Webhooks
      // defined in https://dashboard.stripe.com/test/webhooks  and https://dashboard.stripe.com/webhooks
      
      // For page https://test.dev.paminsight.com/pam/webhook from Stripe account in TEST mode
      $endpoint_id      = 'we_1KH64BLyvna2Btk71meWRYYI' ;
      $endpoint_secret  = 'whsec_pJqi4NVaBrdguvJdW3Mw03n9rrCj9aFL'; 

      // For page https://test.dev.paminsight.com/pam/webhook from Stripe LIVE Mode
      // $endpoint_id     = 'we_1KJbRCLyvna2Btk7GGR3nV4e';  
      // $endpoint_secret ='whsec_e0R9eHGcLWFhoFTFBV8naTdhhF925dLI'

      $this->createStripeSession(); 

      $payload = @file_get_contents('php://input');
      $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];

      $event = null;
      
      try {
        /*  this is replaced by JSONised string */
        $event = \Stripe\Webhook::constructEvent($payload, $sig_header, $endpoint_secret);

        //$event = \Stripe\Webhook::constructEvent(json_decode($payload, true));

        
      } catch(\UnexpectedValueException $e) {
        // Invalid payload
        http_response_code(400);
        exit();
      } catch(\Stripe\Exception\SignatureVerificationException $e) {
        // Invalid signature
        http_response_code(400);
        exit();
      }


      switch ($event->type) {
      // Handle the event  - 
        case 'checkout.session.completed':
          $this->session = $event->data->object;  

          echo "The amount = " . $event->data->object->amount_total . "\n" ;

        case 'checkout.session.async_payment_failed':
          $this->session = $event->data->object;
        
          case 'checkout.session.async_payment_succeeded':
          $this->session = $event->data->object;
        
        case 'checkout.session.expired':
          $this->session = $event->data->object;
        
        case 'customer.created':
          $this->customer = $event->data->object;

          echo "The customer email = " . $this->customer->email . "\n" ;
          echo "The customer phone = " . $this->customer->phone . "\n" ;   
          echo "The customer firstname = "  . $this->splitName($this->customer->name,'f') . "\n" ;   
          echo "The customer lastname = "   . $this->splitName($this->customer->name,'l') . "\n" ;
        
          echo "new Password is " . $this->rand_string(12);        
        
        case 'customer.deleted':
          $this->customer = $event->data->object;

        case 'customer.updated':
          $this->customer = $event->data->object;

        case 'customer.source.created':
          $this->source = $event->data->object;

        case 'customer.source.expiring':
          $this->source = $event->data->object;

        case 'customer.subscription.created':
          $this->subscription = $event->data->object;

        case 'customer.subscription.deleted':
          $this->subscription = $event->data->object;
          echo $this->subscription->customer ;

        case 'customer.subscription.pending_update_applied':
          $this->subscription = $event->data->object;

        case 'customer.subscription.pending_update_expired':
          $this->subscription = $event->data->object;

        case 'customer.subscription.trial_will_end':
          $this->subscription = $event->data->object;

        case 'customer.subscription.updated':
          $this->subscription = $event->data->object;

      // ... handle other event types
          break ;
        default:
          echo '\n Received unknown event type ' . $event->type;
              http_response_code(200);        
      }

    }
}

