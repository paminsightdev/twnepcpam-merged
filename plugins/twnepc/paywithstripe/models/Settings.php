<?php namespace Twnepc\Paywithstripe\Models;

use Model;

/**
 *  settings model
 *
 * @package system
 * @author Matt Walker-Wilson
 *
 */
class Settings extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'pamonline_paywithstripe_settings';

    public $settingsFields = 'fields.yaml';

    /**
     * Validation rules
     */
    public $rules = [
        'acct_id'       => 'required',
        'public_key'    => 'required',
        'secret_key'    => 'required',
        'success_url'   => 'required',
        'cancel_url'    => 'required',
        'webhook_id' => '',
        'webhook_signing_secret' => '',
        'webhook_target_url' => '' ,
        'payment_link'  => '',
        'access_level'  => ''
    ];

    /**
     * Initialize the seed data for this model. This only executes when the
     * model is first created or reset to default.
     * @return void
     */
    public function initSettingsData()
    {
        $this->acct_id      = 'acct_1KEysULyvna2Btk7';
        $this->public_key   = 'pk_test_51KEysULyvna2Btk7G4LkgyXrQBds0TRBbO9CQ0Sa6burA4pwnpAa5MTxF8CHytJwxmDyQhBO2eeMNaDHW31jKWX400Ei8wLP5T';
        $this->secret_key   = 'sk_test_51KEysULyvna2Btk7DSyGHPX4wXUgTAP0YHVFkT1TzpjIr4sbQG63DJ47CDu5bIDIQN8L4ImU6DeAUihx1fpnZevn008iIso56V';
        $this->success_url  = 'https://test.dev.paminsight.com/pam/pam-subscription-confirmed';
        $this->cancel_url   = 'https://test.dev.paminsight.com/pam/pam-transaction-cancelled';

    }

    /*** 
     * Update cache after saving new stripe settings
     */
    public function afterSave()
    {
        // forget current data
        Cache::forget('twnepc_paywithstripe_settings');

        // get all records available now
        $cacheableRecords = Setting::generateCacheableRecords();

        //save them in cache
        Cache::forever('twnepc_paywithstripe_settings', $cacheableRecords);
    }
}
