<?php namespace Twnepc\Blog\Components;

use Db;
use App;
use Request;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Twnepc\Blog\Models\Category as BlogCategory;

class Categories extends ComponentBase
{
    /**
     * @var Collection A collection of categories to display
     */
    public $categories;

    /**
     * @var string Reference to the page name for linking to categories.
     */
    public $categoryPage;

    /**
     * @var string Reference to the current category slug.
     */
    public $currentCategorySlug;

    public function componentDetails()
    {
        return [
            'name'        => 'rainlab.blog::lang.settings.category_title',
            'description' => 'rainlab.blog::lang.settings.category_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'rainlab.blog::lang.settings.category_slug',
                'description' => 'rainlab.blog::lang.settings.category_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
            'displayEmpty' => [
                'title'       => 'rainlab.blog::lang.settings.category_display_empty',
                'description' => 'rainlab.blog::lang.settings.category_display_empty_description',
                'type'        => 'checkbox',
                'default'     => 0
            ],
            'categoryPage' => [
                'title'       => 'rainlab.blog::lang.settings.category_page',
                'description' => 'rainlab.blog::lang.settings.category_page_description',
                'type'        => 'dropdown',
                'default'     => 'blog/category',
                'group'       => 'Links',
            ],
            'parentCategory' => [
                'title'       => 'Parent category',
                'description' => 'Parent category',
                'type'        => 'dropdown',
                'default'     => '',
                'type'        => 'string'
            ],
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        if($this->property('slug') == false){
            if($this->property('parentCategory') != ""){
                $parentCategory = BlogCategory::where('rainlab_blog_categories.slug',$this->property('parentCategory'))->first();
                $firstCategory = BlogCategory::where('rainlab_blog_categories.parent_id',$parentCategory->id)->first();
            }else{
                $firstCategory = BlogCategory::first();
            }
            $this->currentCategorySlug = $this->page['currentCategorySlug'] = $firstCategory->slug;
        }else{
            $this->currentCategorySlug = $this->page['currentCategorySlug'] = $this->property('slug');
        }
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
        $this->categories = $this->page['categories'] = $this->loadCategories();
    }

    protected function loadCategories()
    {
        $data = array();
        if($this->property('parentCategory') != ""){
            $parentCategory = BlogCategory::orderBy('name')
                ->where('rainlab_blog_categories.slug',$this->property('parentCategory'))->first();

            $categories = BlogCategory::where('rainlab_blog_categories.parent_id',$parentCategory->id);
        }else{
            $categories = BlogCategory::orderBy('name');
        }

        foreach($categories->get() as $category){
            $this->removeCategoryPost($category);
            if(count($category->posts)<2){
                $category->hidePosts = true;
            }
            $data[] = $category;
        }
        return $data;
    }

    private function removeCategoryPost($category)
    {
        foreach($category->posts as $post){
            if($category->slug == $post->slug){
                $post->hide = true;
            }
        }
        return $category;
    }
}
