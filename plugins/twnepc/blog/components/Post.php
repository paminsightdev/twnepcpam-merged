<?php namespace Twnepc\Blog\Components;

use Illuminate\Support\Facades\DB;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use RainLab\Blog\Models\Post as BlogPost;
use RainLab\Blog\Models\Category as BlogCategory;

class Post extends ComponentBase
{
    /**
     * @var RainLab\Blog\Models\Post The post model used for display.
     */
    public $post;

    /**
     * @var string Reference to the page name for linking to categories.
     */
    public $categoryPage;

    public function componentDetails()
    {
        return [
            'name'        => 'rainlab.blog::lang.settings.post_title',
            'description' => 'rainlab.blog::lang.settings.post_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'rainlab.blog::lang.settings.post_slug',
                'description' => 'rainlab.blog::lang.settings.post_slug_description',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
            'categoryPage' => [
                'title'       => 'rainlab.blog::lang.settings.post_category',
                'description' => 'rainlab.blog::lang.settings.post_category_description',
                'type'        => 'dropdown',
                'default'     => 'blog/category',
            ],
        ];
    }

    public function getCategoryPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->categoryPage = $this->page['categoryPage'] = $this->property('categoryPage');
        $this->post = $this->page['post'] = $this->loadPost();
    }

    protected function loadPost()
    {
        if($this->property('slug') == false){
            $post = Db::table('rainlab_blog_categories')
                ->join('rainlab_blog_posts_categories','rainlab_blog_posts_categories.category_id','=','rainlab_blog_categories.id')
                ->join('rainlab_blog_posts','rainlab_blog_posts.id','=','rainlab_blog_posts_categories.post_id')
                ->where('rainlab_blog_categories.slug',$this->page['currentCategorySlug'])
                ->first();
            return $post;
        }else{
            $slug = $this->property('slug');
            $post = BlogPost::isPublished()->where('slug', $slug)->first();

            /*
             * Add a "url" helper attribute for linking to each category
             */
            if ($post && $post->categories->count()) {
                $post->categories->each(function($category){
                    $category->setUrl($this->categoryPage, $this->controller);
                });
            }

            return $post;
        }
    }
}
