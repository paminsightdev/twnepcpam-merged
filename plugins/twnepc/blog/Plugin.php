<?php namespace Twnepc\Blog;

use Backend;
use Controller;
use System\Classes\PluginBase;
use RainLab\Blog\Classes\TagProcessor;
use RainLab\Blog\Models\Category;
use Event;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Pam online Front-end blog',
            'description' => 'Modified version of components of blog plugin ',
            'author'      => 'Pamonline',
            'icon'        => 'icon-hand-peace-o'
        ];
    }

    public function registerComponents()
    {
        return [
            'Twnepc\Blog\Components\Post'       => 'blogPost',
            'Twnepc\Blog\Components\Posts'      => 'blogPosts',
            'Twnepc\Blog\Components\Categories' => 'blogCategories'
        ];
    }

    public function registerPermissions()
    {
    }

    public function registerNavigation()
    {
    }

    public function registerFormWidgets()
    {
    }

    public function register()
    {
    }

    public function boot()
    {
    }
}
