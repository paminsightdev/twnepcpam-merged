<?php
namespace Twnepc\Roles;

use App;
use Event;
use Backend;
use BackendMenu;
use Illuminate\Foundation\AliasLoader;
use Rainlab\User\Models\User;
use Rainlab\User\Components\Account;
use Twnepc\Roles\Models\Custom;
use Twnepc\Roles\Models\Group;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public $require = ['Twnepc.User'];

    public function pluginDetails()
    {
        return [
            'name'        => 'Frontend User Roles Manager',
            'description' => 'The plugin lets you manage frontend user roles and permissions.',
            'author'      => 'Pam Online',
            'icon'        => 'icon-hand-peace-o'
        ];
    }

    public function boot()
    {
        /*
         * this has been pulled up into the Rainlab\User\Models\User class, 
         * because this User class does not obtain these relations when logged back in after the user logs into the backend to edit company , awards or members.
         * PLEASE REMOVE THIS COMMENTED OUT CODE BLOCK AFTER 12/2021
         
        User::extend(function($model) 
        {
            $model->belongsToMany['frontend_groups']      = ['Twnepc\Roles\Models\Group', 'table' => 'pamonline_roles_assigned_roles', 'otherKey' => 'role_id'];
            $model->belongsToMany['permissions'] = ['Twnepc\Roles\Models\Group', 'table' => 'pamonline_roles_assigned_roles', 'otherKey' => 'role_id'];
        });  
        */
        
        $userGroup = new Group;
        $userGroup->newUserAddToDefaultGroup();

        Event::listen('backend.menu.extendItems', function($manager)
        {
           $manager->addSideMenuItems('Twnepc.User', 'user', [
                'roles' => [
                    'label'       => 'Roles',
                    'icon'        => 'icon-users',
                    'code'        => 'roles',
                    'owner'       => 'Twnepc.User',
                    'url'         => Backend::url('twnepc/roles/groups')
                ],
                'perms' => [
                    'label'       => 'Permissions',
                    'icon'        => 'icon-key',
                    'code'        => 'perms',
                    'owner'       => 'Twnepc.User',
                    'url'         => Backend::url('twnepc/roles/permissions')
                ]
            ]);
           
           //dd($manager);
           
        });

        Event::listen('backend.form.extendFields', function($widget) 
        {
            if (!$widget->getController() instanceof \Twnepc\User\Controllers\Users) return;
            if (!$widget->model instanceof \Rainlab\User\Models\User) return;
            
             $widget->addFields([
                'frontend_groups@update'              => [
                    'label'           => 'Roles',
                    'commentAbove'    => 'Specify which Role this person is assigned to.',
                    'tab'             => 'Roles',
                    'type'            => 'relation'
                ]
            ], 'primary');
        });
    }

    public function registerMarkupTags()
    {
        return [
            'functions'   => [
                //'can'         => function($can) { return Group::can($can); },
                //'hasRole'     => function($role, $user=null) { return Group::hasRole($role,$user); }
                'can' => ['\Twnepc\Roles\Models\Group', 'can'], 
                'hasRole' => ['\Twnepc\Roles\Models\Group', 'hasRole']
            ]
        ];
    }

}
