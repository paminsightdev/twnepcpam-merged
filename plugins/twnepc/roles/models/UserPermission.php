<?php 
namespace Twnepc\Roles\Models;

use Model;
use Rainlab\User\Models\User;

class UserPermission extends Model
{

	use \October\Rain\Database\Traits\Validation;
	
    protected $table      = 'pamonline_roles_permissions';

    public $rules         = [
        'name'    => 'required|unique:pamonline_roles_permissions',
    ];

 	public $belongsToMany = [
        'frontend_groups' => ['Twnepc\Roles\Models\UserGroup', 'table' => 'pamonline_roles_permission_role']
    ];

}