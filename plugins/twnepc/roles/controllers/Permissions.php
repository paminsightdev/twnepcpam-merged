<?php namespace Twnepc\Roles\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Twnepc\Roles\Models\Permission;

class Permissions extends Controller
{

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.User', 'user', 'perms');
    }

    public function index_onDelete()
    {
        if(($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) 
        {
            foreach ($checkedIds as $permissionId) {
                if (!$permission = Permission::find($permissionId))
                    continue;

                $permission->delete();
            }

            Flash::success('Update successful');
        }

        return $this->listRefresh();
    }

}