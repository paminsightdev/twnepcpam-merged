<?php namespace Twnepc\Roles\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Twnepc\Roles\Models\Group;

class Groups extends Controller
{

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.User', 'user', 'roles');
    }

    public function index_onDelete()
    {
        if(($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) 
        {
            foreach ($checkedIds as $roleId) 
            {
                if (!$role = Group::find($roleId))
                    continue;

                $role->delete();
            }

            Flash::success('Update successful');
        }

        return $this->listRefresh();
    }
    
}