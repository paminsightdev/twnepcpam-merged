<?php namespace Twnepc\Reports\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use DB;

/**
 * TODO: Search research question report
 * 1. REGUSER_Describes or "Which of the following best describes you?" exist into search form but not stored
 * 2. REGUSER_Knowledge or "How would you rate your personal level of financial and investment knowledge?" exist into search form but not stored
 * 3. REGUSER_Reason or "What is the reason for your PAM search?" exist into searchform, not stored
 * 4. REGUSER_Wealth or "What are you most concerned about at the moment in relation to your personal wealth?" exist into searchform, not stored
 * 5. "Which of the following best describes your attitude to risk?" exist in advanced search form, not stored
 * 6. REGUSER_ViewPortfolio or "How would you like to able to view information about your portfolio?" exists in advanced search form but not stored
 * 7. SL_Min "What is the approximate value of your total investible assets?" value is saved but not linked to textual value,  currently can show only index
 *
 * Updated -
 * How would you rate your personal level of financial and investment knowledge? Can't be linked to option, because of field value is number, but value should be textual.
 */

class Search extends Reports
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['pamonline.reports.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Reports','reports','search');
    }

    public function listExtendQuery($query,$scope)
    {

    }

    public function listFilterExtendQuery($query, $scope)
    {

    }
}