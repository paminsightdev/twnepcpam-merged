<?php namespace Twnepc\Reports\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Twnepc\Pamcompanies\Models\Companies;
use Rainlab\User\Models\User;
use Twnepc\Roles\Models\Group;
use Twnepc\Pamcompanies\Models\SearchLog;

class Overview extends Reports
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = [
        'changes'=>'changes_config_list.yaml',
        'logins'=>'logins_config_list.yaml'
    ];

    public $requiredPermissions = ['pamonline.reports.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Reports','reports','overview');
    }

    public function index()
    {
        $this->vars['table1'] = $this->getPamsTableStatistic();
        $this->vars['table2'] = $this->getUsersTableStatistic();
        return parent::index();
    }

    public function listExtendQuery($query,$list)
    {
        if($list == 'logins'){
            $query->where('company_id','>',0)->whereNotNull('last_login')->orderBy('last_login','DESC');
        }

        if($list == 'changes'){
            $query->orderBy('pam_created','DESC');
        }
    }

    protected function getUsersTableStatistic()
    {
        $table = [];

        $columns = [
            'registered'       => 0,
            'registered_30'    => 0,
            'loggedin_30'      => 0,
            'searches'         => 0
        ];

        $table['rows'] = [
            'private_investor'      => $columns,
            'professional_advisor'  => $columns,
            'pams'                  => $columns,
            'totals'                => $columns,
        ];
        $monthAgo = date("Y-m-d H:m:s", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "-1 month" ) );
        
        $table['rows']['private_investor']['registered'] = Group::where('id',9)->first()->users()->count();
        $table['rows']['private_investor']['registered_30'] = Group::where('id',9)->first()->users()->whereRaw("users.created_at > '".$monthAgo."'")->count();
        $table['rows']['private_investor']['loggedin_30'] = Group::where('id',9)->first()->users()->whereRaw("users.last_login > '".$monthAgo."'")->count();
        $table['rows']['private_investor']['searches'] = SearchLog::count();

        $table['rows']['professional_advisor']['registered'] = Group::where('id',10)->first()->users()->count();
        $table['rows']['professional_advisor']['registered_30'] = Group::where('id',10)->first()->users()->whereRaw("users.created_at > '".$monthAgo."'")->count();
        $table['rows']['professional_advisor']['loggedin_30'] = Group::where('id',10)->first()->users()->whereRaw("users.last_login > '".$monthAgo."'")->count();
        $table['rows']['professional_advisor']['searches'] = SearchLog::count();

        Group::whereIn('id', [11,12])->get()->each(function($model) use (&$table){
            $table['rows']['pams']['registered'] += $model->users()->count();
        });

        Group::whereIn('id', [11,12])->get()->each(function($model) use (&$table,$monthAgo){
            $table['rows']['pams']['registered_30'] += $model->users()->whereRaw("users.created_at > '".$monthAgo."'")->count();
        });

        Group::whereIn('id', [11,12])->get()->each(function($model) use (&$table,$monthAgo){
            $table['rows']['pams']['loggedin_30'] += $model->users()->whereRaw("users.last_login > '".$monthAgo."'")->count();
        });

        $table['rows']['pams']['searches'] = SearchLog::count();

        Group::whereIn('id', [9,10,11,12])->get()->each(function($model) use (&$table){
            $table['rows']['totals']['registered'] += $model->users()->count();
        });

        Group::whereIn('id', [9,10,11,12])->get()->each(function($model) use (&$table,$monthAgo){
            $table['rows']['totals']['registered_30'] += $model->users()->whereRaw("users.created_at > '".$monthAgo."'")->count();
        });

        Group::whereIn('id', [9,10,11,12])->get()->each(function($model) use (&$table,$monthAgo){
            $table['rows']['totals']['loggedin_30'] += $model->users()->whereRaw("users.last_login > '".$monthAgo."'")->count();
        });

        $table['rows']['totals']['searches'] = SearchLog::count();

        return $table;
    }

    protected function getPamsTableStatistic()
    {
        $table = [];

        $columns = [
            'active'        => 0,
            'not_active'    => 0,
            'total'         => 0
        ];

        $table['rows'] = [
            'standard'              => $columns,
            'premier'               => $columns,
            'enhanced'              => $columns,
            'totals'                => $columns,
        ];

        $table['rows']['standard']['active']        = Companies::where('c_active',1)->where('c_typeback','b')->count();
        $table['rows']['standard']['not_active']    = Companies::where('c_active',0)->where('c_typeback','b')->count();
        $table['rows']['standard']['total']         = Companies::where('c_typeback','b')->count();

        $table['rows']['premier']['active']        = Companies::where('c_active',1)->where('c_typeback','p')->count();
        $table['rows']['premier']['not_active']    = Companies::where('c_active',0)->where('c_typeback','p')->count();
        $table['rows']['premier']['total']         = Companies::where('c_typeback','p')->count();

        $table['rows']['enhanced']['active']        = Companies::where('c_active',1)->where('c_typeback','e')->count();
        $table['rows']['enhanced']['not_active']    = Companies::where('c_active',0)->where('c_typeback','e')->count();
        $table['rows']['enhanced']['total']         = Companies::where('c_typeback','e')->count();

        $table['rows']['totals']['active']        = Companies::where('c_active',1)->count();
        $table['rows']['totals']['not_active']    = Companies::where('c_active',0)->count();
        $table['rows']['totals']['total']         = Companies::count();

        return $table;
    }
}