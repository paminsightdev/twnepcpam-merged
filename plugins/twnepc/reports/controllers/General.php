<?php namespace Twnepc\Reports\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Twnepc\User\Models\PageView;
use Twnepc\Pamcompanies\Models\SearchLog;
use Twnepc\Pamcompanies\Models\Reviews;
use Twnepc\Pamcompanies\Models\CompanyTrackEvent;
use Twnepc\Pamcompanies\Models\CompanyViews;
/**
 * TODO: General stat
 */

class General extends Reports
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['pamonline.reports.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Reports','reports','general');
    }

    public function index()
    {
        $this->bodyClass = 'slim-container';
        $this->pageTitle = 'General stats';
        $this->vars['data'] = $this->prepareTableData();
    }

    public function listExtendQuery($query)
    {

    }

    public function onFilterGetOptions()
    {

    }

    public function onFilterUpdate()
    {
        $data = $this->prepareTableData();

        return [
            '#Lists' => $this->makePartial('list',['data' => $data])
        ];
    }

    private function prepareTableData()
    {
        $data = [];
        $data['total_site_hits_1'] = 0;
        $data['total_site_hits_2'] = 0;
        $data['basic_search_1'] = 0;
        $data['basic_search_2'] = 0;
        $data['basic_search_3'] = 0;
        $data['advanced_search_1'] = 0;
        $data['advanced_search_2'] = 0;
        $data['advanced_search_3'] = 0;
        $data['total_search_1'] = 0;
        $data['total_search_2'] = 0;
        $data['total_search_3'] = 0;
        $data['reviews_ratings'] = 0;
        $data['follow'] = 0;
        $data['unfollow'] = 0;
        $data['unique_pam_views'] = 0;

        $dates = $this->prepareDateOptions();
        $data['total_site_hits_1']  = PageView::whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'")->count();
        $data['total_site_hits_2']  = PageView::whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'")->whereNotIn('page_url', ['http://pamonline.com'])->count();
        $data['basic_search_1']     = SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->whereNotIn('SL_SearchFormType',['a'])->where('SL_Type','')->count();
        $data['basic_search_2']     = SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->whereNotIn('SL_SearchFormType',['a'])->where('SL_Type','ind')->count();
        $data['basic_search_3']     = SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->whereNotIn('SL_SearchFormType',['a'])->where('SL_Type','pro')->count();
        $data['advanced_search_1']  = SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->where('SL_SearchFormType','a')->where('SL_Type','')->count();
        $data['advanced_search_2']  = SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->where('SL_SearchFormType','a')->where('SL_Type','ind')->count();
        $data['advanced_search_3']  = SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->where('SL_SearchFormType','a')->where('SL_Type','pro')->count();
        $data['total_search_1']     = $data['basic_search_1'] + $data['advanced_search_1'];//SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->where('SL_Type','')->count();
        $data['total_search_2']     = $data['basic_search_2'] + $data['advanced_search_2'];//SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->where('SL_Type','ind')->count();
        $data['total_search_3']     = $data['basic_search_3'] + $data['advanced_search_3'];//SearchLog::whereRaw("SL_Date < '".$dates['to']."'")->whereRaw("SL_Date > '".$dates['from']."'")->where('SL_Type','pro')->count();
        $data['reviews_ratings']    = Reviews::whereRaw("date < '".$dates['to']."'")->whereRaw("date > '".$dates['from']."'")->count();
        $data['follow']             = CompanyTrackEvent::whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'")->where('action','follow')->count();
        $data['unfollow']           = CompanyTrackEvent::whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'")->where('action','unfollow')->count();
        $data['unique_pam_views']   = count(PageView::whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'")->groupBy('user_id')->get()->toArray());

        return $data;
    }

}