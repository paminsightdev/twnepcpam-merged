<?php namespace Twnepc\Reports\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use DB;
use Session;
use Twnepc\Roles\Models\Group;

/**
 * TODO: Portfolio range statistics
 * 1. "What is the approximate value of the account you want to select a Private Asset Manager for?" not stored into log table
 * that's way this report cant be done. It is a main field for search by. SL_MinCap
 *
 * //Missing extra option fields
 * 2."What kind of Private Asset Manager are you trying to select?" - MT_ManagerType exist in form, not logged.
 */

class Portfolio extends Reports
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['pamonline.reports.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Reports','reports','portfolio');
    }

    public function index()
    {
        $this->bodyClass = 'slim-container';
        $this->pageTitle = 'Portfolio Range Statistics';
        $this->vars['minDate'] = '2000-01-01';
        $this->vars['maxDate'] = '2099-12-31';
        try{
            Session::forget('filters');
            $this->vars['data'] = [
                'portfolio' => $this->preparePortfolioTableData(),
            ];
        }catch(Exception $e){

        }
    }


    public function onFilterOptions()
    {
        $scopeName = post('scopeName',false);
        $search = post('search',null);
        $options = [
            'available' => [],
            'active'    => []
        ];
        try{
            if(!$scopeName)
                throw new Exception("Filter exception");


            switch($scopeName){
                case "pams":
                    $options['available'] = $this->preparePamsAsOptions($search);
                    break;

                case "types":
                    $options['available'] = $this->prepareTypesAsOptions($search);
                    break;

                case "options":
                    $options['available'] = $this->prepareFiltersAsOptions($search);
                    break;

            }

            return [
                'options' => $options,
                'scopeName' => $scopeName
            ];

        }catch(Exception $e){

        }
    }

    public function onFilterOptionsUpdate()
    {
        try{
            $this->setFilterSession();
            $data = [
                'portfolio'     => $this->preparePortfolioTableData()
            ];
            $filterDates = $this->prepareDateSession();
            $minDate = $filterDates['from'];
            $maxDate = $filterDates['to'];

            $filters = $this->getFilterSession();

            if(isset($filters['filters'])){
                $data['filter'] = $filters['filters'][0]['name'];
            }

            return [
                '#Lists' => $this->makePartial('list',['data' => $data,'minDate' => $minDate,'maxDate'=>$maxDate])
            ];
        }catch(Exception $e){

        }

    }

    protected function preparePamsAsOptions($searchTerm = null)
    {
        $options = [];
        $companiesModel = Db::table('pamtbl_company');

        if($searchTerm != null){
            $companiesModel->whereRaw("c_companyname LIKE '%".$searchTerm."%'");
        }

        $companies = $companiesModel->get();
        foreach($companies as $company){
            $options[] = [
                'id' => $company->company_id,
                'name' => $company->c_companyname
            ];
        }
        return $options;
    }

    protected function prepareFiltersAsOptions($searchTerm = null)
    {
        $options = [];
        $options[] = array(
            'id' => 'SL_AdvDisc',
            'id_quest' => 14,
            'name' => 'What type of portfolio management service would you prefer?',
        );
        $options[] = array(
            'id' => 'SL_ManagerType',
            'id_quest' => 13,
            'name' => 'What kind of Private Asset Manager are you trying to select?',
        );
        $options[] = array(
            'id' => 'SL_SegPool',
            'id_quest' => 15,
            'name' => 'Do you want your portfolio to be managed on a segregated or pooled basis?',
        );
        $options[] = array(
            'id' => 'SL_Managed',
            'id_quest' => 16,
            'name' => 'How do you want your manager-client relationship to be managed?',
        );
        $options[] = array(
            'id' => 'SL_Fundamental',
            'id_quest' => 17,
            'name' => 'What are your fundamental investment objectives?',
        );
        $options[] = array(
            'id' => 'SL_Risk',
            'id_quest' => 22,
            'name' => 'Which of the following best describes your attitude to risk?',
        );
        $options[] = array(
            'id' => 'SL_Charge',
            'id_quest' => 18,
            'name' => 'What is your preferred charging structure?',
        );
        $options[] = array(
            'id' => 'SL_Area',
            'id_quest' => 12,
            'name' => 'Where would you like your Private Asset Manager to be located?',
        );

        if($searchTerm != null){

        }

        return $options;
    }

    protected function prepareTypesAsOptions()
    {
        $options = [];
        $options[] = array('id' => 'ind', 'name' => 'Private Investor');
        $options[] = array('id' => 'pro', 'name' => 'Professional Advisor');
//        $types = Group::all();
//        foreach($types as $type){
//            $options[] = [
//                'id' => $type->id,
//                'name' => $type->name
//            ];
//        }
        return $options;
    }

    private function preparePortfolioTableData()
    {
        $filters = $this->getFilterSession();

        $typesFilters = [];
        $optionFilters = [];
        $datesFilters = $this->prepareDateOptions();

        foreach($filters as $filterName => $options){
            switch($filterName){

                case "types":
                    foreach($options as $opt)
                        $typesFilters[] = $opt['id'];
                    break;

                case "options":
                    foreach($options as $opt)
                        $optionFilters[] = $opt;
                    break;
            }
        }

        $logsGroupedByOptionValue = [];

        foreach($optionFilters as $opt){
            $optionValues = DB::table('pamonline_pamcontent_question_options')->where("id_quest",$opt['id_quest'])->get();
            foreach($optionValues as $val){
                $query = DB::table('pamtbl_search_log');
                $query->whereRaw("SL_Date < '".$datesFilters['to']."'");
                $query->whereRaw("SL_Date > '".$datesFilters['from']."'");
                $query->where($opt['id'],$val->value);
                if(!empty($typesFilters))
                    $query->whereIn('SL_Type', $typesFilters);

                $logsGroupedByOptionValue[$val->text] = $query->whereNotIn('SL_MinCap',[''])->get();
            }
        }

        $data = [];
        foreach($logsGroupedByOptionValue as $key => $array){
            $data[$key] = [
                '2' => 0,
                '3' => 0,
                '4' => 0,
                '5' => 0,
                '6' => 0,
                '7' => 0,
                '8' => 0,
                '9' => 0,
            ];
            foreach($array as $log){
                if(isset($data[$key][$log->SL_MinCap])){
                    $data[$key][$log->SL_MinCap]++;
                }
            }
        }

        foreach($data as $key => $val){
            $data[$key]['t'] = $val['2']+$val['3']+$val['4']+$val['5']+$val['6']+$val['7']+$val['8']+$val['9'];
        }

        return $data;
    }

}