<?php namespace Twnepc\Reports\Controllers;

use Backend\Classes\Controller;
use Session;

class Reports extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->addjs("/modules/system/assets/ui/vendor/moment/moment.js", "1.0.0");
        $this->addjs("/modules/system/assets/ui/vendor/moment/moment-timezone-with-data.js", "1.0.0");
    }

    protected function prepareDateOptions()
    {
        $dates = array(
            'from' => '2000-01-01',
            'to' => '2099-12-31'
        );
        $datesProvidedByFilter = post('options[dates]',false);
        if($datesProvidedByFilter){
            $dates['from'] = $datesProvidedByFilter[0] == ''?$dates['from']:$datesProvidedByFilter[0];
            $dates['to'] = $datesProvidedByFilter[1] == ''?$dates['to']:$datesProvidedByFilter[1];
            return $dates;
        }
        return $dates;
    }

    protected function setFilterSession()
    {
        $options = post('options',false);
        $scopeName = post('scopeName',false);

//        if(!$options)
//            throw new Exception("Filter options error!");

        if(!$scopeName)
            throw new Exception("Filter scopeName error!");


        $filters = Session::get('filters',[]);

        if(!isset($options['active']) && !isset($options['dates'])){
            $this->resetFilterSession($scopeName);
            return;
        }

        if(isset($options['active']))
            $filters[$scopeName] = $options['active'];

        if(isset($options['dates']))
            $filters[$scopeName] = $options['dates'];

        Session::put('filters',$filters);
    }

    protected  function getFilterSession()
    {
        return Session::get('filters',[]);
    }

    protected function resetFilterSession($key)
    {
        $filters = Session::get('filters',[]);

        if(isset($filters[$key])){
            unset($filters[$key]);
        }else{
            return;
        }

        Session::put('filters',$filters);
    }

    protected function prepareDateSession()
    {
        $dates = array(
            'from' => '2000-01-01',
            'to' => '2099-12-31'
        );
        $filters = Session::get('filters',[]);
        if(isset($filters['dates'])){
            $datesProvidedByFilter = $filters['dates'];
            $dates['from'] = $datesProvidedByFilter[0] == ''?$dates['from']:$datesProvidedByFilter[0];
            $dates['to'] = $datesProvidedByFilter[1] == ''?$dates['to']:$datesProvidedByFilter[1];
            return $dates;
        }
        return $dates;
    }

}