<?php namespace Twnepc\Reports\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use Twnepc\User\Models\PageView;
use Twnepc\Pamcompanies\Models\SearchLog;
use Twnepc\Pamcompanies\Models\Reviews;
use Twnepc\Pamcompanies\Models\CompanyTrackEvent;
use Twnepc\Pamcompanies\Models\CompanyViews;
use Twnepc\Roles\Models\Group;
use Rainlab\User\Models\Throttle;
use Session;
use DB;
/**
 * TODO: Registered stat
 */

class Registered extends Reports
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['pamonline.reports.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Reports','reports','registered');
    }

    public function index()
    {
        Session::forget('filters');
        $this->bodyClass = 'slim-container';
        $this->pageTitle = 'Registered User Overview';
        $this->vars['data'] = $this->prepareTableData();
    }

    public function listExtendQuery($query)
    {

    }

    public function onFilterGetOptions()
    {
        $scopeName = post('scopeName',false);
        $search = post('search',null);
        $options = [
            'available' => [],
            'active'    => []
        ];
        try{
            if(!$scopeName)
                throw new Exception("Filter exception");


            switch($scopeName){

                case "types":
                    $options['available'] = $this->prepareTypesAsOptions($search);
                    break;

            }

            return [
                'options' => $options,
                'scopeName' => $scopeName
            ];

        }catch(Exception $e){

        }
    }

    public function onFilterOptionsUpdate()
    {
        $this->setFilterSession();
        $data = $this->prepareTableData();

        return [
            '#Lists' => $this->makePartial('list',['data' => $data])
        ];
    }

    protected function prepareTypesAsOptions()
    {
        $options = [];
        $types = Group::all();
        foreach($types as $type){
            $options[] = [
                'id' => $type->id,
                'name' => $type->name
            ];
        }
        return $options;
    }

    private function prepareTableData()
    {
        $data = [];
        $data['success_logins'] = 0;
        $data['failed_loggins'] = 0;
        $data['page_views'] = 0;
        $data['page_views_excluding_homepage'] = 0;
        $data['basic_search'] = 0;
        $data['advanced_search'] = 0;
        $data['total_search'] = 0;
        $data['reviews'] = 0;
        $data['follow'] = 0;
        $data['unfollow'] = 0;
        $data['updates'] = 0;
        $data['uniq_page_views'] = 0;

        $filters = $this->getFilterSession();
        $typesFilters = [];
        $datesFilters = $this->prepareDateOptions();

        foreach($filters as $filterName => $options){
            switch($filterName){

                case "types":
                    foreach($options as $opt)
                        $typesFilters[] = $opt['id'];

                    break;

            }
        }

        $data['success_logins'] = Throttle::where('attempts',0)->count();

        $throttleData = Throttle::whereRaw("last_attempt_at < '".$datesFilters['to']."'")->whereRaw("last_attempt_at > '".$datesFilters['from']."'")
            ->select(Db::raw('count(attempts) as count'))->whereHas('user',function($user) use($typesFilters){
                $user->whereHas('roles',function($group) use ($typesFilters) {
                    if(count($typesFilters) > 0)
                        $group->whereIn('role_id', $typesFilters);
                });
            })->get();

        if(isset($throttleData->toArray()[0]))
                $data['failed_loggins'] = $throttleData->toArray()[0]['count'];

        $data['page_views'] = PageView::whereRaw("created_at < '".$datesFilters['to']."'")->whereRaw("created_at > '".$datesFilters['from']."'")->whereHas('user',function($user) use($typesFilters){
            $user->whereHas('roles',function($group) use ($typesFilters) {
                if(count($typesFilters) > 0)
                    $group->whereIn('role_id', $typesFilters);
            });
        })->count();

        $data['page_views_excluding_homepage'] = PageView::whereRaw("created_at < '".$datesFilters['to']."'")->whereRaw("created_at > '".$datesFilters['from']."'")->whereNotIn('page_url', ['http://pamonline.com'])->whereHas('user',function($user) use($typesFilters){
            $user->whereHas('roles',function($group) use ($typesFilters) {
                if(count($typesFilters) > 0)
                    $group->whereIn('role_id', $typesFilters);
            });
        })->count();

        $data['basic_search'] = SearchLog::whereRaw("SL_Date < '".$datesFilters['to']."'")->whereRaw("SL_Date > '".$datesFilters['from']."'")->whereNotIn('SL_SearchFormType',['a'])->whereHas('user',function($user) use($typesFilters){
            $user->whereHas('roles',function($group) use ($typesFilters) {
                if(count($typesFilters) > 0)
                    $group->whereIn('role_id', $typesFilters);
            });
        })->count();

        $data['advanced_search'] = SearchLog::whereRaw("SL_Date < '".$datesFilters['to']."'")->whereRaw("SL_Date > '".$datesFilters['from']."'")->where('SL_SearchFormType','a')->whereHas('user',function($user) use($typesFilters){
            $user->whereHas('roles',function($group) use ($typesFilters) {
                if(count($typesFilters) > 0)
                    $group->whereIn('role_id', $typesFilters);
            });
        })->count();

        $data['total_search'] = $data['basic_search']+$data['advanced_search'];
//        SearchLog::whereRaw("SL_Date < '".$datesFilters['to']."'")->whereRaw("SL_Date > '".$datesFilters['from']."'")->whereHas('user',function($user) use($typesFilters){
//            $user->whereHas('roles',function($group) use ($typesFilters) {
//                if(count($typesFilters) > 0)
//                    $group->whereIn('role_id', $typesFilters);
//            });
//        })->count();

        $data['reviews'] = Reviews::whereRaw("date < '".$datesFilters['to']."'")->whereRaw("date > '".$datesFilters['from']."'")->whereHas('user',function($user) use($typesFilters){
            $user->whereHas('roles',function($group) use ($typesFilters) {
                if(count($typesFilters) > 0)
                    $group->whereIn('role_id', $typesFilters);
            });
        })->count();

        $data['follow'] = CompanyTrackEvent::whereRaw("created_at < '".$datesFilters['to']."'")->whereRaw("created_at > '".$datesFilters['from']."'")->where('action','follow')->whereHas('user',function($user) use($typesFilters){
            $user->whereHas('roles',function($group) use ($typesFilters) {
                if(count($typesFilters) > 0)
                    $group->whereIn('role_id', $typesFilters);
            });
        })->count();

        $data['unfollow'] = CompanyTrackEvent::whereRaw("created_at < '".$datesFilters['to']."'")->whereRaw("created_at > '".$datesFilters['from']."'")->where('action','unfollow')->whereHas('user',function($user) use($typesFilters){
            $user->whereHas('roles',function($group) use ($typesFilters) {
                if(count($typesFilters) > 0)
                    $group->whereIn('role_id', $typesFilters);
            });
        })->count();

        $data['updates'] = PageView::whereRaw("created_at < '".$datesFilters['to']."'")
            ->whereRaw("created_at > '".$datesFilters['from']."'")->where('page_url', 'like', '%/my-account')->whereHas('user',function($user) use($typesFilters){
            $user->whereHas('roles',function($group) use ($typesFilters) {
                if(count($typesFilters) > 0)
                    $group->whereIn('role_id', $typesFilters);
            });
        })->count();

        $data['uniq_page_views'] = count(CompanyViews::whereRaw("pam_created < '".$datesFilters['to']."'")->whereRaw("pam_created > '".$datesFilters['from']."'")->groupBy('user_id')->get()->toArray());


        return $data;
    }

}