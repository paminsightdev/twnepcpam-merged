<?php namespace Twnepc\Reports\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use DB;
use Session;
use Twnepc\User\Models\PageView;

/**
 * TODO: Entry Statistic
 * 1. For Entry Pageviews table - Database table 'pamtbl_companyviews' should store extra data like
 * ['what kind of page viewed','frontend type of viewed company','for extra store $_SERVER var']
 *
 * 2. Need to log how many times Pam appeared In Search Results
 * 3. Log Pams Average Position in search results
 * 4. Log pams follow/unfollow activity.
 * 5. Contact requests made ??? - Looks that data not stored.
 * 6. Linked to website ??? - What kind of log data it is ?
 * 7. Entry Viewed ???
 */

class Entry extends Reports
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['pamonline.reports.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Reports','reports','entry');
    }

    public function index()
    {
        $this->bodyClass = 'slim-container';
        $this->pageTitle = 'Entry statistic';
        try{
            Session::forget('filters');
            $this->vars['data'] = [
                'views' => $this->prepareViewsTableData(),
                'general' => $this->prepareGeneralTableData(),
            ];
        }catch(Exception $e){

        }
    }


    public function onFilterOptions()
    {
        $scopeName = post('scopeName',false);
        $search = post('search',null);
        $options = [
            'available' => [],
            'active'    => []
        ];
        try{
            if(!$scopeName)
                throw new Exception("Filter exception");


            switch($scopeName){
                case "pams":
                    $options['available'] = $this->preparePamsAsOptions($search);
                    break;

                case "types":
                    $options['available'] = $this->prepareTypesAsOptions($search);
                    break;

            }

            return [
                'options' => $options,
                'scopeName' => $scopeName
            ];

        }catch(Exception $e){

        }
    }

    public function onFilterOptionsUpdate()
    {
        try{
            $this->setFilterSession();
            $data = [
                'views'     => $this->prepareViewsTableData(),
                'general'   => $this->prepareGeneralTableData()
            ];
            return [
                '#Lists' => $this->makePartial('list',['data' => $data])
            ];
        }catch(Exception $e){

        }

    }

    protected function preparePamsAsOptions($searchTerm = null)
    {
        $options = [];
        $companiesModel = Db::table('pamtbl_company');

        if($searchTerm != null){
            $companiesModel->whereRaw("c_companyname LIKE '%".$searchTerm."%'");
        }

        $companies = $companiesModel->get();
        foreach($companies as $company){
            $options[] = [
                'id' => $company->company_id,
                'name' => $company->c_companyname
            ];
        }
        return $options;
    }

    protected function prepareTypesAsOptions()
    {
        $options = [];
        $options[] = [
            'id'    => 'p',
            'name'  => 'Premier'
        ];
        $options[] = [
            'id'    =>'e',
            'name'  => 'Enhanced'
        ];
        $options[] = [
            'id'    => 'b',
            'name'  => 'Basic'
        ];
        return $options;
    }

    private function prepareViewsTableData()
    {
        $data = [];

        $filters        = $this->getFilterSession();
        $datesFilters   = $this->prepareDateOptions();

        $data['introduction']  = self::getEntryPageViews('',$filters,$datesFilters);
        $data['history']       = self::getEntryPageViews('history',$filters,$datesFilters);
        $data['philosophy']    = self::getEntryPageViews('investment',$filters,$datesFilters);
        $data['disclaimer']    = self::getEntryPageViews('disclaimeronline',$filters,$datesFilters);
        $data['corporate']     = self::getEntryPageViews('corporate',$filters,$datesFilters);
        $data['platforms']     = self::getEntryPageViews('investment-platforms',$filters,$datesFilters);
        $data['questions']     = self::getEntryPageViews('answers',$filters,$datesFilters);
        $data['products']      = self::getEntryPageViews('products',$filters,$datesFilters);
        $data['offices']       = self::getEntryPageViews('offices',$filters,$datesFilters);
        $data['shareholders']  = self::getEntryPageViews('shareholders',$filters,$datesFilters);
        $data['assets']        = self::getEntryPageViews('assets',$filters,$datesFilters);
        $data['personnel']     = self::getEntryPageViews('personnel',$filters,$datesFilters);
        $data['charges']       = self::getEntryPageViews('fees',$filters,$datesFilters);
        $data['articles']      = self::getEntryPageViews('articles',$filters,$datesFilters);
        $data['publications']  = self::getEntryPageViews('publications',$filters,$datesFilters);
        $data['news']          = self::getEntryPageViews('news',$filters,$datesFilters);
        $data['commentary']    = self::getEntryPageViews('investment-commentary',$filters,$datesFilters);
        $data['thresholds']    = self::getEntryPageViews('thresholds',$filters,$datesFilters);

        return $data;
    }

    static function getEntryPageViews($context = '',$filters,$dates)
    {
        $count = 0;
        if(isset($filters['pams'])){
            foreach($filters['pams'] as $filter){
                $query = DB::table('page_views');
                $query->whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'");
                if($context !== ''){
                    $count += $query->where('page_url','like',"%company/{$filter['id']}/{$context}%")->count();
                }else{
                    $count += $query->where('page_url','like',"%company/{$filter['id']}")->count();
                }
            }
        }

        if(isset($filters['types'])){
            foreach($filters['types'] as $filter){
                $companies = DB::table('pamtbl_company')->select('company_id')->where('c_typefront',$filter['id'])->get();
                foreach($companies as $company){
                    $query = DB::table('page_views');
                    $query->whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'");
                    if($context !== '')
                        $count += $query->where('page_url','like',"%company/{$company->company_id}/{$context}%")->count();
                    else
                        $count += $query->where('page_url','like',"%company/{$company->company_id}")->count();
                }
            }
        }

        if(!isset($filters['pams']) && !isset($filters['types'])){
            $query = DB::table('page_views');
            $query->whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'");
            if($context !== '')
                $count = $query->where('page_url','like',"%/{$context}%")->count();
        }
        return $count;
    }

    protected function prepareGeneralTableData()
    {
        $data = [];

        $companyTable = DB::table('pamtbl_company');
        $companyTrackEventTable = DB::table('pamtbl_companytrackevents');
        $favouriteTbale = DB::table('pamtbl_favourite_companies');
        $companyViewsTable = DB::table('pamtbl_companyviews');

        $filters = $this->getFilterSession();

        foreach($filters as $filterName => $options){
            switch($filterName){
                case "pams":
                    $vals = [];

                    foreach($options as $opt)
                        $vals[] = $opt['id'];

                    if(!empty($vals)){
                        $companyTable->whereIn('company_id', $vals);
                        $companyTrackEventTable->whereIn('company_id', $vals);
                        $favouriteTbale->whereIn("PamID",$vals);
                        $companyViewsTable->whereIn('company_id',$vals);
                    }

                    break;

                case "types":
                    $vals = [];

                    foreach($options as $opt)
                        $vals[] = $opt['id'];

//                    if(!empty($vals))
//                        $dbTable->whereIn('company_id', $vals);

                    break;

                case "dates":
                    $dates = $this->prepareDateOptions();
                    $companyTrackEventTable->whereRaw("created_at < '".$dates['to']."'")->whereRaw("created_at > '".$dates['from']."'");
                    break;

            }
        }

        $companyTrackEventTableWithQueryCopy1 = clone($companyTrackEventTable);
        $companyTrackEventTableWithQueryCopy2 = clone($companyTrackEventTable);

        $companies = $companyTable->get();

        $data['appeared_in_search']     = 0;
        $data['avg_position']           = 0;
        $data['added_favorites']        = $companyTrackEventTableWithQueryCopy1->where('action','follow')->count();
        $data['curr_favorites']         = $favouriteTbale->count();
        $data['contact_request']        = $companyTrackEventTableWithQueryCopy2->where('action','contact')->count();
        $data['link_to_web']            = 0;
        $data['entry_viewed']           = $companyViewsTable->count();
		
		$datesFilters   = $this->prepareDateOptions();
        $data['num_page_views']         = self::getEntryPageViews('%',$filters,$datesFilters);

        $countOfCompanies = 0;
        foreach($companies as $company){
            $data['appeared_in_search'] += $company->c_numsearches;
            $data['avg_position'] += $company->c_totalofposition;
            $countOfCompanies++;
        }

        
        $data['avg_position'] = number_format($data['avg_position']/$data['appeared_in_search'],0);
		$data['appeared_in_search'] = number_format($data['appeared_in_search']);
	
        if($data['avg_position'] < 1)
            $data['avg_position'] = 1;

        return $data;
    }

    public function listExtendQuery($query)
    {

    }
}