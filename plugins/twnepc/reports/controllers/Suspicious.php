<?php namespace Twnepc\Reports\Controllers;

use BackendMenu;
use Event;
use Request;
use BackendAuth;
use DB;

/**
 * TODO: Suspicious activity
 */

class Suspicious extends Reports
{
    public $implement = ['Backend\Behaviors\ListController'];

    public $listConfig = [
        'throttle'  => 'config_list.yaml',
        'logs'      => 'config_list2.yaml',
        'reviews'   => 'config_list3.yaml'
    ];

    public $requiredPermissions = ['pamonline.reports.access_main'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.Reports','reports','suspicious');
    }

    public function listExtendQuery($query,$def)
    {
        $dayAgo = date("Y-m-d H:m:s",strtotime("-1 days"));

        if($def == 'logs'){
            $query->select(Db::raw('pam_created,company_id,count(company_id) as count'));
//            $query->whereRaw("pam_created > '".$dayAgo."'");
            $query->groupBy('company_id');
            $query->orderBy('count','DESC');
        }

        if($def == 'reviews'){
            $query->select(Db::raw('date,company_id,count(company_id) as count'));
//            $query->whereRaw("date > '".$dayAgo."'");
            $query->groupBy('company_id');
            $query->orderBy('count','DESC');
        }
    }

}