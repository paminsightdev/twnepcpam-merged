<?php namespace Twnepc\Reports;

use System\Classes\PluginBase;
use Backend;

class Plugin extends PluginBase {

    public $require = [];


    public function pluginDetails() {
        return [
            'name'        => 'Reports plugin',
            'description' => 'Reports plugin description',
            'author'      => 'RT',
            'icon'        => 'icon-file-text-o',
            'homepage'    => 'http://pamonline.com'
        ];
    }

    /**
     * Registro do plugin, executado no primeiro registro do plugin.
     * Utilize este m�todo para extender outros plugins,por exemplo.
     *
     * @since   1.0.0
     */

    public function register () {

    }

    /**
     * M�todo de inicializa??o, executado logo antes de definir a rota.
     * Utilize este m�todo para extender eventos da aplica??o.
     *
     * @since   1.0.0
     */

    public function boot () {

    }

    /**
     * Registra tags de marca??o adicionais que poder?o ser utilizadas nas p?ginas.
     *
     * @return  array
     * @since   1.0.0
     * @example
     *
     *    return [
     *          'filters' => [
     *              '_'  => [$this, 'translateString'],
     *          ]
     *      ];
     *
     * @see   https://octobercms.com/docs/plugin/registration#extending-twig
     */

    public function registerMarkupTags () {

    }

    /**
     * Registers any front-end components used by this plugin.
     */

    public function registerComponents () {

    }

    /**
     * Register Navigation
     *
     * Registers back-end navigation menu items for this plugin.
     */

    public function registerNavigation () {

        return [
            'reports' => [
                'label'         => 'Reports',
                'url'           => Backend::url('twnepc/reports/overview'),
                'icon'          => 'icon-area-chart',
                'order'         => 500,
                'permissions'   => ['pamonline.reports.access_main'],
                'sideMenu' => [
                    'overview' => [
                        'label'       => 'Overview report',
                        'icon'        => 'icon-refresh',
                        'url'         => Backend::url('twnepc/reports/overview'),
                    ],
                    'general' => [
                        'label'       => 'General stats',
                        'icon'        => 'icon-question-circle',
                        'url'         => Backend::url('twnepc/reports/general'),
                    ],
                    'users' => [
                        'label'       => 'User session explorer',
                        'icon'        => 'icon-user',
                        'url'         => Backend::url('twnepc/reports/users'),
                    ],
                    'registered' => [
                        'label'       => 'Registered User Overview',
                        'icon'        => 'icon-child',
                        'url'         => Backend::url('twnepc/reports/registered'),
                    ],
                    'suspicious' => [
                        'label'       => 'Suspicious activity',
                        'icon'        => 'icon-user-secret',
                        'url'         => Backend::url('twnepc/reports/suspicious'),
                    ],
                    'entry' => [
                        'label'       => 'Entry statistic',
                        'icon'        => 'icon-list-ol',
                        'url'         => Backend::url('twnepc/reports/entry'),
                    ],
                    'portfolio' => [
                        'label'       => 'Portfolio range statistics',
                        'icon'        => 'icon-user',
                        'url'         => Backend::url('twnepc/reports/portfolio'),
                    ],
                    'search' => [
                        'label'       => 'Search research question report',
                        'icon'        => 'icon-search',
                        'url'         => Backend::url('twnepc/reports/search'),
                    ]
                ]
            ]
        ];
    }

    /**
     * Register Permissions
     *
     * Registers any back-end permissions used by this plugin.
     */

    public function registerPermissions () {
        return [
            'pamonline.reports.access_main' => ['tab' => 'Reports plugin', 'label' => 'Access Main Reports section.'],
        ];
	}

    /**
     * Register Settings
     *
     * Registers any back-end configuration links used by this plugin.
     */

    public function registerSettings () {
//        return [
//            'settings' => [
//                'label'       => 'woss.users::lang.settings.label',
//                'description' => 'woss.users::lang.settings.description',
//                'category'    => 'woss.users::lang.settings.users',
//                'icon'        => 'icon-cog',
//                'class'       => 'Woss\Users\Models\Settings',
//                'order'       => 500,
//                'permissions' => ['woss.users.settings.access']
//            ],
//        ];
    }

    /**
     * Register Form Widgets
     *
     * Registers any back-end form widgets used by this plugin.
     */

    public function registerFormWidgets () {

    }

    /**
     * Register Report Widgets
     *
     * Registers any back-end report widgets, including the dashboard widgets.
     */

    public function registerReportWidgets () {

    }

    /**
     * Register Mail Templates
     *
     * Registers any mail view templates supplied by this plugin.
     */

    public function registerMailTemplates () {

    }

    /**
     * Register Schedule
     *
     * Registers scheduled tasks that are executed on a regular basis.
     */

    public function registerSchedule ($schedule) {

    }
}
