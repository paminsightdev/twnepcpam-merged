<?php namespace Twnepc\SiteSearch\Components;

use Cms\Classes\ComponentBase;

class SiteSearchInclude extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.sitesearch::lang.siteSearchInclude.title',
            'description' => 'twnepc.sitesearch::lang.siteSearchInclude.description',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

}