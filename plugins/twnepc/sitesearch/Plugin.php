<?php namespace Twnepc\SiteSearch;

use Backend;
use System\Classes\PluginBase;

/**
 * SiteSearch Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'twnepc.sitesearch::lang.plugin.name',
            'description' => 'twnepc.sitesearch::lang.plugin.description',
            'author'      => 'twnepc.sitesearch::lang.plugin.author',
            'icon'        => 'icon-search',
        ];
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Twnepc\SiteSearch\Components\SearchResults'     => 'searchResults',
            'Twnepc\SiteSearch\Components\SiteSearchInclude' => 'siteSearchInclude',
        ];
    }

    /**
     * Registers any back-end permissions.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'pamonline.sitesearch.manage_settings' => [
                'tab'   => 'twnepc.sitesearch::lang.plugin.name',
                'label' => 'twnepc.sitesearch::lang.plugin.manage_settings_permission',
            ],
        ];
    }

    /**
     * Registers any back-end settings.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'twnepc.sitesearch::lang.plugin.name',
                'description' => 'twnepc.sitesearch::lang.plugin.manage_settings',
                'category'    => 'system::lang.system.categories.cms',
                'icon'        => 'icon-search',
                'class'       => 'Twnepc\SiteSearch\Models\Settings',
                'order'       => 500,
                'keywords'    => 'search',
                'permissions' => ['pamonline.sitesearch.manage_settings']
            ],
        ];
    }
}
