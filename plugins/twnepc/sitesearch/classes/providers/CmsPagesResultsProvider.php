<?php
namespace Twnepc\SiteSearch\Classes\Providers;

use Cms\Classes\Page;
use Cms\Classes\Theme;
use Cms\Classes\Content;
use Illuminate\Database\Eloquent\Collection;
use Twnepc\SiteSearch\Models\Settings;

/**
 * Searches the contents of native cms pages.
 *
 * @package Twnepc\SiteSearch\Classes\Providers
 */
class CmsPagesResultsProvider extends ResultsProvider
{
    /**
     * Runs the search for this provider.
     *
     * @return ResultsProvider
     */
    public function search()
    {
        if ( ! $this->isEnabled()) {
            return $this;
        }
		
	$inSearchArray = []; 
	
        foreach ($this->pages() as $page) {

            $contents = $this->removeTwigTags($page->markup);

            if ( ! $page->hasComponent('siteSearchInclude') || ! $this->containsQueryIn($contents, $page)) {
                continue;
            }

            $relevance = $this->containsQuery($page->settings['title']) ? 2 : 1;
			
	   if (!in_array($page->fileName, $inSearchArray)) {		
		$inSearchArray[] = $page->fileName;   
		$this->addResult($page->settings['title'], $contents, $page->settings['url'], $relevance);
	   }

        }

        foreach ($this->thecontents() as $content) {

            $contents = $this->removeTwigTags($content->markup);

            if ( ! $content->hasComponent('siteSearchInclude') || ! $this->containsContentQueryIn($contents, $content)) {
		continue;
            }

	  if (!in_array($content->fileName, $inSearchArray)) {	
		  $inSearchArray[] = $content->fileName;   
		$relevance = 1;		

		$theurlpath =  explode(".", $content->fileName); 

		$thepagetitle = "";

		foreach ($this->pages() as $page) {
		  if ($content->fileName == $page->fileName) {
			  $thepagetitle = $page->settings['title'];
		  }
		}

			  $this->addResult($thepagetitle, $contents, $theurlpath[0], $relevance);
	  }

        }		
		
        return $this;
    }

    /**
     * Checks if this provider is enabled
     * in the config.
     *
     * @return bool
     */
    protected function isEnabled()
    {
        return Settings::get('cms_pages_enabled', false);
    }

    /**
     * Removes {{ }} and {% %} markup blocks.
     *
     * @param $html
     *
     * @return mixed
     */
    private function removeTwigTags($html)
    {
        return preg_replace('/\{[%\{][^\}]*[%\}]\}/', '', $html);
    }

    /**
     * Get all cms pages.
     *
     * @return Collection
     */
    protected function pages()
    {
        return Page::listInTheme(Theme::getActiveTheme(), true);
    }

    /**
     * Get all cms content blocks.
     *
     * @return Collection
     */
    protected function thecontents()
    {
        return Content::listInTheme(Theme::getActiveTheme(), true);
    }	
	
    /**
     * Checks if $subjects contains the query string.
     *
     * @param $subject
     *
     * @return bool
     */
    protected function containsQuery($subject)
    {
        return mb_strpos(strtolower($subject), strtolower($this->query)) !== false;
    }

    /**
     * Display name for this provider.
     *
     * @return mixed
     */
    public function displayName()
    {
        return Settings::get('cms_pages_label', 'Page');
    }

    /**
     * Search the query in the title and contents of the page.
     *
     * @param $contents
     * @param $page
     *
     * @return bool
     */
    protected function containsQueryIn($contents, $page)
    {
        return $this->containsQuery($contents) || $this->containsQuery($page->settings['title']);
    }
	
    protected function containsContentQueryIn($contents, $page)
    {
        return $this->containsQuery($contents);
    }	
}

