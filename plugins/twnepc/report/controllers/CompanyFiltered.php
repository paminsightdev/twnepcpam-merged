<?php namespace Twnepc\Report\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use Backend\Widgets\ReportContainer;
use Twnepc\Company\Models\Company;
use Illuminate\Support\Facades\Response;

class CompanyFiltered extends Controller
{
    use \Backend\Traits\InspectableContainer;
    public $companyfilter;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContextOwner('Twnepc.report');
    }

    public function index() {
        $user = BackendAuth::getUser();
        $reportContainer = $this->initReportContainer('companyfilteredreportdashboard');
        $reportContainer->canAddAndDelete = false;


        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }
        $this->pageTitle = 'backend::lang.dashboard.menu_label';
        BackendMenu::setContext('Twnepc.Report','report','company-filtered-reports');
    }

    public function onPreFilter(){

        $filtered = Company::where('id',post('id'))->get();


        $data['results'] = [];
        foreach($filtered as $a) {
            $company = [
                'id' => $a->id,
                'text' => $a->name
            ];
            array_push($data['results'],$company);
        }


        return Response::json($data);
    }

    public function onFilter(){

        $filtered = Company::where(function($query){
            $query->where('name','like','%'.post('search').'%');
        });


        $filtered
            ->limit(100);

        $filtered = $filtered->get();

        $data['results'] = [];
        foreach($filtered as $a) {
            $company = [
                'id' => $a->id,
                'text' => $a->name
            ];
            array_push($data['results'],$company);
        }
        return Response::json($data);
    }

    public function onChangeCompany() {
        $reportContainer = $this->initReportContainer('companyfilteredreportdashboard');
        $user = BackendAuth::getUser();
        $reportContainer->canAddAndDelete = false;
        $this->companyfilter = post('companies');
       
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }

        return ['#reportReportContainer' => $this->widget->reportContainer->render()];
    }

    public function index_onInitReportContainer()
    {
        $user = BackendAuth::getUser();
        $reportContainer = $this->initReportContainer('companyfilteredreportdashboard');
        $reportContainer->canAddAndDelete = false;
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }
        return ['#reportReportContainer' => $this->widget->reportContainer->render()];
    }

    /**
     * Prepare the report widget used by the dashboard
     * @param Model $model
     * @return void
     */
    protected function initReportContainer($context)
    {
       return new ReportContainer($this,['context' => $context]);
    }
}