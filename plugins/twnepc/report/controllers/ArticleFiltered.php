<?php namespace Twnepc\Report\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use Backend\Widgets\ReportContainer;
use Twnepc\Company\Models\Company;
use Twnepc\News\Models\Article;
use Illuminate\Support\Facades\Response;

class ArticleFiltered extends Controller
{
    use \Backend\Traits\InspectableContainer;
    public $articlefilter;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContextOwner('Twnepc.report');
    }

    public function index() {
        $user = BackendAuth::getUser();
        $reportContainer = $this->initReportContainer('articlefilteredreportdashboard');
        $reportContainer->canAddAndDelete = false;


        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }
        $this->pageTitle = 'backend::lang.dashboard.menu_label';
        BackendMenu::setContext('Twnepc.Report','report','article-filtered-reports');
    }

    public function onPreFilter(){

        $filtered = Article::where('id',post('id'))->get();


        $data['results'] = [];
        foreach($filtered as $a) {
            $article = [
                'id' => $a->id,
                'text' => $a->title
            ];
            array_push($data['results'],$article);
        }


        return Response::json($data);
    }

    public function onFilter(){

        $filtered = Article::where(function($query){
            $query->where('title','like','%'.post('search').'%');
        });


        $filtered
            ->limit(100);

        $filtered = $filtered->get();

        $data['results'] = [];
        foreach($filtered as $a) {
            $article = [
                'id' => $a->id,
                'text' => $a->name
            ];
            array_push($data['results'],$article);
        }
        return Response::json($data);
    }

    public function onChangeArticle() {
        $reportContainer = $this->initReportContainer('articlefilteredreportdashboard');
        $user = BackendAuth::getUser();
        $reportContainer->canAddAndDelete = false;
        $this->articlefilter = post('articles');
       
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }

        return ['#reportReportContainer' => $this->widget->reportContainer->render()];
    }

    public function index_onInitReportContainer()
    {
        $user = BackendAuth::getUser();
        $reportContainer = $this->initReportContainer('articlefilteredreportdashboard');
        $reportContainer->canAddAndDelete = false;
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }
        return ['#reportReportContainer' => $this->widget->reportContainer->render()];
    }

    /**
     * Prepare the report widget used by the dashboard
     * @param Model $model
     * @return void
     */
    protected function initReportContainer($context)
    {
       return new ReportContainer($this,['context' => $context]);
    }
}