<?php namespace Twnepc\Report\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use Backend\Widgets\ReportContainer;
use RainLab\User\Models\User;
use Illuminate\Support\Facades\Response;


class UserFiltered extends Controller
{
    use \Backend\Traits\InspectableContainer;
    public $userfilter;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContextOwner('Twnepc.report');
    }

    public function index() {
        $user = BackendAuth::getUser();

        $reportContainer = $this->initReportContainer('userfilteredreportdashboard');
        $reportContainer->canAddAndDelete = false;
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }
        $this->pageTitle = 'backend::lang.dashboard.menu_label';
        BackendMenu::setContext('Twnepc.Report','report','user-filtered-reports');
    }

    public function onPreFilter(){

        $filtered = User::where('id',post('id'))->get();


        $data['results'] = [];
        foreach($filtered as $a) {
            $user = [
                'id' => $a->id,
                'text' => $a->name.' '.$a->surname.' - '.$a->email 
            ];
            array_push($data['results'],$user);
        }


        return Response::json($data);
    }

    public function onFilter(){

        $filtered = User::where(function($query){
            $query->where('name','like','%'.post('search').'%')
                ->orWhere('email','like','%'.post('search').'%')
                ->orWhere('email2','like','%'.post('search').'%')
                ->orWhere('surname','like','%'.post('search').'%');
        });


        $filtered
            ->limit(100);

        $filtered = $filtered->get();

        $data['results'] = [];
        foreach($filtered as $a) {
            $user = [
                'id' => $a->id,
                'text' => $a->name.' '.$a->surname.' - '.$a->email 
            ];
            array_push($data['results'],$user);
        }
        return Response::json($data);
    }

    public function onChangeUser() {
        $reportContainer = $this->initReportContainer('userfilteredreportdashboard');
        $user = BackendAuth::getUser();
        $reportContainer->canAddAndDelete = false;
        $this->userfilter = post('users');
       
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }

        return ['#reportReportContainer' => $this->widget->reportContainer->render()];
    }

    public function index_onInitReportContainer()
    {
        $user = BackendAuth::getUser();
        $reportContainer = $this->initReportContainer('userfilteredreportdashboard');
        $reportContainer->canAddAndDelete = false;
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }
        return ['#reportReportContainer' => $this->widget->reportContainer->render()];
    }

    /**
     * Prepare the report widget used by the dashboard
     * @param Model $model
     * @return void
     */
    protected function initReportContainer($context)
    {
       return new ReportContainer($this,['context' => $context]);
    }
}