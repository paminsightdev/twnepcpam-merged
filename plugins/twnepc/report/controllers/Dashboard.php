<?php namespace Twnepc\Report\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use Backend\Widgets\ReportContainer;

class Dashboard extends Controller
{
    use \Backend\Traits\InspectableContainer;

    public $sitefilter;

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContextOwner('Twnepc.report');
    }

    public function index() {

       
        
        $reportContainer = $this->initReportContainer('reportdashboard');

        $user = BackendAuth::getUser();

        $defaultSites = $this->vars['defaultSites'] =  \Keios\Multisite\Models\Setting::whereIn('id', $user->site->pluck('id'))->get()->pluck('theme','id');

        if($user->role->code == 'super_admin' || $user->is_superuser) {
            $defaultSites = $this->vars['defaultSites'] = \Keios\Multisite\Models\Setting::get()->pluck('theme','id');
        }
        $reportContainer->canAddAndDelete = false;
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }
        
        $this->pageTitle = 'backend::lang.dashboard.menu_label';
        BackendMenu::setContext('Twnepc.Report', 'report','report-dashboard');
    }

    public function index_onInitReportContainer()
    {
        $reportContainer = $this->initReportContainer('reportdashboard');
        $user = BackendAuth::getUser();
        $reportContainer->canAddAndDelete = false;
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }
        return ['#reportReportContainer' => $this->widget->reportContainer->render()];
    }

    public function onChangeSite() {
        $reportContainer = $this->initReportContainer('reportdashboard');
        $user = BackendAuth::getUser();
        $reportContainer->canAddAndDelete = false;
        $this->sitefilter = post('sitefilter');
       
        if($user->is_superuser || $user->role->code == 'super_admin') {
            $reportContainer->canAddAndDelete = true;  
        }

        return ['#reportReportContainer' => $this->widget->reportContainer->render()];
    }

    /**
     * Prepare the report widget used by the dashboard
     * @param Model $model
     * @return void
     */
    protected function initReportContainer($context)
    {
        return new ReportContainer($this,['context' => $context]);
    }
}