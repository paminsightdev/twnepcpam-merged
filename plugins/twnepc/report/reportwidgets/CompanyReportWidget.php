<?php
namespace Twnepc\Report\ReportWidgets;

use Backend\Facades\BackendAuth;
use Twnepc\Report\Models\Settings;
use Backend\Classes\ReportWidgetBase;
use Keios\Multisite\Models\Setting as Site;


class CompanyReportWidget extends ReportWidgetBase
{
   
    public function init() {
        
    }
    public function render()
    {
        
        $metabase_site_url = env('METABASE_URL');
        $metabase_secret_key = env('METABASE_SECRET');
        
        $params = [];

        $user = BackendAuth::getUser();

        $filter = ($this->controller->companyfilter ? implode(',',$this->controller->companyfilter) : NULL);

        $payload = [
            'resource' => [
                'question' => intval($this->property('question'))
            ],
            'params'=> ['param' => '','companies' => $filter],
        ];

        $jwt = $this->sign($payload,$metabase_secret_key);
        $this->vars['title'] = $this->property('title');
        $this->vars['url'] = $metabase_site_url."/embed/question/".$jwt."#bordered=true&titled=true";
        return $this->makePartial('widget');
    }

    private function sign($payload,$metabase_secret_key) {

        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        $payload = json_encode($payload);


        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        //token creation
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $metabase_secret_key, true);

        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));


        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }
    public function defineProperties()
    {

        $questions = Settings::get('questions');
        
        $options = [];
        foreach($questions as $q):;  
            $options[$q['question_id']] = $q['title'];
        endforeach;

        $sites = [];
        $sites_items = Site::all();
        
        foreach($sites_items as $s):;  
            $sites[$s->id] = $s->theme;
        endforeach;

        $props = [
            'title' => [
                'title'             => 'Widget title',
                'default'           => 'Author widgets',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'The Widget Title is required.'
            ],
            'question' => [
                'title'             => 'question',
                'default'           => '2',
                'type'              => 'dropdown',
                'options'           => $options,
                'context'           =>'reportdashboard'
            ]
        ];

        

       
        
       

        return $props;

        
    }
}
