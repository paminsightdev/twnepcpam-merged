<?php namespace Twnepc\Report\Models;

use Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'twnepc_report_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}