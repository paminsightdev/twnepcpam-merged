<?php namespace Twnepc\Report;

use Backend;
use System\Classes\PluginBase;

/**
 * Report Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Report',
            'description' => 'No description provided yet...',
            'author'      => 'Twnepc',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // Extend the navigation
        \Event::listen('backend.menu.extendItems', function($manager) {
            
            $manager->addSideMenuItems('Twnepc.Report', 'report', [
                'report-dashboard' => [
                    'label'       => 'Reports Dashboard',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'dashboard-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/dashboard'),
                ],
                'report-article' => [
                    'label'       => 'Article Reports',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'article-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/article'),
                ],
                'report-users'  => [
                    'label'       => 'User Reports',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'user-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/user'),
                ],
                'report-memberships' => [
                    'label'       => 'Membership Reports',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'membership-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/membership'),
                ],
                'detailed-report-article' => [
                    'label'       => 'Detailed Article Reports',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'detailed-article-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/detailedarticle'),
                ],
                'report-page-statistics' => [
                    'label'       => 'Page Statistics',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'page-statistics-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/pagestatistic'),
                ],
                'user-filtered-reports' => [
                    'label'       => 'User Filtered Reports',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'user-filtered-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/userfiltered'),
                ],
                'company-filtered-reports' => [
                    'label'       => 'Company Filtered Reports',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'company-filtered-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/companyfiltered'),
                ],
                'article-filtered-reports' => [
                    'label'       => 'Article Filtered Reports',
                    'icon'        => 'oc-icon-bar-chart-o',
                    'code'        => 'article-filtered-reports',
                    'owner'       => 'Twnepc.Report',
                    'url'         => Backend::url('twnepc/report/articlefiltered'),
                ],
                
            ]);
        });
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Twnepc\Report\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        //return []; // Remove this line to activate

        return [
            'twnepc.report.view_reports' => [
                'tab' => 'Report',
                'label' => 'View Reports'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        //return []; // Remove this line to activate

        return [
            'report' => [
                'label'       => 'Report',
                'url'         => Backend::url('twnepc/report/dashboard'),
                'icon'        => 'icon-leaf',
                'permissions' => ['twnepc.report.*'],
                'order'       => 500
            ],
        ];
    }

    public function registerReportWidgets(){
        return [
            'Twnepc\Report\ReportWidgets\BaseReportWidget' => [
                'label' => 'Report Widget',
            ],
            'Twnepc\Report\ReportWidgets\UserReportWidget' => [
                'label' => 'User Report Widget',
            ],
            'Twnepc\Report\ReportWidgets\CompanyReportWidget' => [
                'label' => 'Company Report Widget',
            ],
            'Twnepc\Report\ReportWidgets\ArticleReportWidget' => [
                'label' => 'Article Report Widget',
            ]

        ];
    }

    public function registerSettings()
    {
        return [
            'report' => [
                'label'       => 'Report Settings',
                'description' => 'Manage report settings',
                'category'    => 'Reports',
                'icon'        => 'icon-globe',
                'class'       => 'Twnepc\Report\Models\Settings',
                'order'       => 500,
                'keywords'    => 'reports settings',
                'permissions' => ['twnepc.report.*']
            ]
        ];
    }
}
