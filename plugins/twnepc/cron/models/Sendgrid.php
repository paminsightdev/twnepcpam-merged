<?php namespace Twnepc\Cron\Models;

use Model;

class Sendgrid extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'sendgrid_reports';
}