<?php namespace Twnepc\Cron;

use Event;
use Illuminate\Support\Facades\DB;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{

    public function registerSchedule($schedule)
    {
        $schedule->command('sendgrid:import')->hourly();
    }

    public function registerComponents()
    {
    }

    public function registerSettings()
    {

    }

    public function register() {
        $this->registerConsoleCommand('sendgrid.import', 'Twnepc\Cron\Console\SendgridImport');
    }

    public function boot(){  

    }
}
