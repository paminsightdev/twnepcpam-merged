<?php namespace Twnepc\Cron\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class CreateDb extends Migration
{
    public function up()
    {
        Schema::create('sendgrid_reports', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('created');
            $table->text('reason');
            $table->text('email');
            $table->text('type');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('sendgrid_reports');
    }
}
