<?php namespace Twnepc\Cron\Console;

use Illuminate\Console\Command;
use Twnepc\Cron\Models\Sendgrid;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SendgridImport extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'sendgrid:import';

    /**
     * @var string The console command description.
     */
    protected $description = 'Does Import';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        //MUST BE THE UNIX TIMESTAMPS FROM DB PER TYPE
        $inv_last_date = Sendgrid::where('type','invalid')->max('created');
        $bounce_last_date = Sendgrid::where('type','bounces')->max('created');
        $spam_last_date = Sendgrid::where('type','spam')->max('created');
        $invalid = $this->send('suppression/invalid_emails',$inv_last_date);
        $bounces = $this->send('suppression/bounces',$bounce_last_date);
        $spam = $this->send('suppression/spam_reports',$spam_last_date);

        $this->import($invalid,'invalid',$inv_last_date);
        $this->import($bounces,'bounces',$bounce_last_date);
        $this->import($spam,'spam',$spam_last_date);

       
    }

    private function import($data,$type,$lastfrom) {

        $d = \json_decode($data);

        foreach($d as $i) {

            if($i->created > $lastfrom) {
                
            $item = new Sendgrid;

            $item->created = $i->created;
            $item->reason = $i->reason;
            $item->email = $i->email;
            $item->type = $type;

            $item->save();
            
            }
        }
    }

    private function send($url,$from) {

        $curl = curl_init();
        if(boolval($this->option('nodate'))) {
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('SENDGRID_API_URL').$url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "{}",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer ".env('SENDGRID_IMPORT_API_KEY')
                ),
                ));
        } else {
            curl_setopt_array($curl, array(
                CURLOPT_URL => env('SENDGRID_API_URL').$url."?start_time=".$from,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "{}",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Bearer ".env('SENDGRID_IMPORT_API_KEY')
                ),
                ));
        }

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        return "cURL Error #:" . $err;
        } else {
        return $response;
        }

    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['nodate',null, InputOption::VALUE_OPTIONAL, 'Import all without date.',false],
        ];
    }

}