<?php namespace Twnepc\Logs\Components;

use DB;
use Auth;
use Keios\Multisite\Classes\SettingHelper;
use Lang;
use Request;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use Twnepc\Logs\Models\Visit as VisitModel;

class UserVisits extends ComponentBase {

    public function componentDetails()
    {
        return [
            'name'        => 'UserVisits',
            'description' => 'UserVisits recorded for all page load'
        ];
    }

    public function defineProperties(){
        
        return [];
    }

    
    public function onRun() {
        $user = $this->user = Auth::getUser();

        $visit = new VisitModel;
        if($user) {
            $visit->user_id = $user->id;
        } else {
            // a user id assigned to all non-members, this user record is Guest
            $visit->user_id = getenv('GUEST_USER_ID');
        }

        $visit->site_id = SettingHelper::getCurrentSite()->id;
        $visit->created_at = date('Y-m-d H:i:s');
        $visit->updated_at = date('Y-m-d H:i:s');
        $visit->session_id = Session::getId();
        $visit->device = Request::header('User-Agent');
        $visit->url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $visit->query_string = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);

        if(Request::get('message')) {
            $visit->message = Request::get('message');
        }

        if(Request::get('source')) {
            $visit->source = Request::get('source');
        }

        $visit->save();
    }
}