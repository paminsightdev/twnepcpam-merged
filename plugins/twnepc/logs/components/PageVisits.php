<?php namespace Twnepc\Logs\Components;

use DB;
use Auth;
use Keios\Multisite\Classes\SettingHelper;
use Lang;
use Request;
use Cms\Classes\ComponentBase;
use Twnepc\Company\Models\Company;
use Illuminate\Support\Facades\Session;
use Rainlab\User\Models\User as UserModel;
use Twnepc\Logs\Models\Article as VisitModel;
use Twnepc\News\Models\Article as ArticleModel;

class PageVisits extends ComponentBase {

    public $article;

    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.logs::lang.components.pagevisits.name',
            'description' => 'twnepc.logs::lang.components.pagevisits.description'
        ];
    }

    public function defineProperties(){
        
        return [
            'slug' => [
                'title'       => 'slug',
                'description' => 'Article slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }

    /*private function getSubscription($user) {
        $company = Company::find($user->company_id);
        if($company) {
            $branch = $user->thebranch;
            if($branch) {
                if(count($branch->membership) > 0) {
                    return $branch->membership->subscription_id;
                }
            }

            // check against company membership
            if(count($company->membership) > 0) {
                return $company->membership->subscription_id;
            }
        }

        // check against user membership
        $user = Auth::getUser();
        if(count($user->membership) > 0) {
            return $user->membership->subscription_id;
        }

        return 0;
    }*/

    /*private function getSubscriptionForTheArticle($user,$article) {

    }*/
    
    public function onRun() {
        $article = $this->article = $this->loadArticle();
        $user = $this->user = Auth::getUser();
        
        $membership_id = Session::get('article_membership');


        if(Request::get('source')) {
            Session::put('user.referer',Request::get('source'));
        }

        $visit = new VisitModel;
        if($user) {
            $visit->user_id = $user->id;
        } else {
            // a user id assigned to all non-members, this user record is Guest
            $visit->user_id = getenv('GUEST_USER_ID');
        }
        
        if($article){
            $visit->article_id = $article->id;
            $visit->created_at = date('Y-m-d H:i:s');
            $visit->updated_at = date('Y-m-d H:i:s');
            $visit->sessionid = Session::getId();
            $visit->device = Request::header('User-Agent');
            if($membership_id){
                $visit->membership_id = $membership_id;
            }

            if(Request::get('message')) {
                $visit->message_id = Request::get('message');
            }

            if(Request::get('source')) {
                $visit->source = Request::get('source');
            }

            $visit->save();
        }
        //
    }

    protected function loadArticle(){
        $site = SettingHelper::getCurrentSite();
        $article = ArticleModel::where('slug',$this->property('slug'))
            ->where('site_id', $site->id)
            ->first();
        return $article;
    }
}