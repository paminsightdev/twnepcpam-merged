<?php namespace Twnepc\Logs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UserVisits extends Migration
{
    public function up()
    {
        Schema::create('user_visits', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned(false);
            $table->integer('site_id')->unsigned(false);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->text('session_id')->nullable();
            $table->text('device')->nullable();
            $table->text('url');
            $table->text('query_string')->nullable();
            $table->integer('user_id')->nullable();
        });
    }
    
    public function down()
    {
    }
}
