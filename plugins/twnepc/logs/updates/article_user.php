<?php namespace Twnepc\Logs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ArticleUser extends Migration
{
    public function up()
    {
        Schema::create('article_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned(false);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('article_id');
            $table->integer('user_id');
            
        });
    }
    
    public function down()
    {
    }
}
