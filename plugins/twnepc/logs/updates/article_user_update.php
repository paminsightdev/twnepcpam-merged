<?php namespace Twnepc\Logs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ArticleUserUpdate extends Migration
{
    public function up()
    {
        Schema::table('article_user', function($table)
        {
            $table->integer('subscription_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('article_user', function($table)
        {
            $table->dropColumn('subscription_id');
        });
    }
}
