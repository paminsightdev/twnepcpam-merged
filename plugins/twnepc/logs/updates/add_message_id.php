<?php namespace Twnepc\Logs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddMessageId extends Migration
{
    public function up()
    {
        Schema::table('article_user', function($table)
        {
            $table->integer('message_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('article_user', function($table)
        {
            $table->dropColumn('message_id');
        });
    }
}
