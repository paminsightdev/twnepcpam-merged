<?php namespace Twnepc\Logs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class RenameSubid extends Migration
{
    public function up()
    {
        Schema::table('article_user', function($table)
        {
            $table->renameColumn('subscription_id', 'membership_id');
        });
    }

    public function down()
    {
        $table->renameColumn('membership_id', 'subscription_id');
    }
}
