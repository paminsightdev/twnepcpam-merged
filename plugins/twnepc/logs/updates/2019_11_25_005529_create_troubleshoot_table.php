<?php namespace Twnepc\Logs\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTroubleshootTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pam_splunk_troubleshooting', function ($table) {
            $table->bigIncrements('id');
            $table->string('host', 255);
            $table->string('source', 510);
            $table->string('sourcetype', 50);
            $table->integer('user_id')->index();
            $table->string('workflow_feature', 100);
            $table->string('workflow_class_function', 255);
            $table->string('event_name', 100);
            $table->text('event');
            $table->datetime('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pam_splunk_troubleshooting');
    }
}
