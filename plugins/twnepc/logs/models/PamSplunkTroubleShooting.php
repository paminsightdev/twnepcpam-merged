<?php

namespace Twnepc\Logs\Models;

use Model;
use Illuminate\Support\Facades\URL;
use Auth;
use Exception;

use October\Rain\Auth\Models\User;
use Illuminate\Support\Facades\DB;
use Responsiv\Campaign\Models\Subscriber;


class PamSplunkTroubleShooting extends Model {
    
    /**
     * The table associated with the model.
     */
    protected $table = 'pam_splunk_troubleshooting';
    
    /**
     * Indicates if the model should be timestamped.
     */
    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     */
    protected $fillable = [
        'host', 
        'source', 
        'sourcetype', 
        'user_id', 
        'workflow_feature', 
        'workflow_class_function', 
        'event_name', 
        'event', 
        'timestamp'
    ];
    
    /**
     * The file that holds subscription email addresses with no currently registered users.
     */
    public static $subscriptionNoUserCheck = 'check-subscription-email-addresses-exists.txt';
    
    /**
     * The file that holds registered users that currently have no subscription to newsletters.
     */
    public static $userWithNoSubscriptionCheck = 'check-why-current-users-not-subscribed.txt';
    
    
    
    /**
     * used to log variable states in functions and the functions in workflows, these
     * values are used by Splunk to monitor the website application indicating where errors
     * maybe or any intermittent issues.
     * @param string $pamLog partially populated log object with meta data
     * @param string $sourceFilePathFromApp e.g. '/plugins/rainlab/user/components/Account.php'
     * @param string $workflowClassFunction e.g. 'onUpdateMyInterests'
     * @param string $eventName e.g. 'data[category]'
     * @param mixed $eventData e.g. a single variable or array (which can be wrapped into a JSON string; for diagnostics of settings)
     * @return void
     */
    public static function saveLogData($pamLog, $sourceFilePathFromApp, $workflowClassFunction, $eventName, $eventData) {
    
        try {
            // use argument parameters to set data for logging 
            $pamLog->source=base_path().$sourceFilePathFromApp;
            $pamLog->workflow_class_function=$workflowClassFunction;
            $pamLog->event_name=$eventName;
            
            // check if array, object or single variable
            if(is_array($eventData) || is_object($eventData)) {
                $pamLog->event=json_encode((array)$eventData);
            } else {
                $pamLog->event=$eventData;
            }
            // generate time stamp
            $pamLog->timestamp=date('Y-m-d H:i:s', strtotime('now'));
            // send this to the database table for logging splunk insights
            //$pamLog->save();  uncomment when migration works
            
        } catch (Exception $ex) {
            // send this error to storage/logs/system.log | when the logger does not work
            try {
                tracelog("an exception was thrown from the PamSplunkTroubleShooting logger ".$pamLog->workflow_class_function);
                return false;
                
            } catch (Exception $ex) {
                // do nothing here... operation failed
                return false;
            }
        }
        // after log saved... operation success
        return true;
    }
    
    
    
    /**
     * used to log variable states in functions and the functions in workflows, these
     * values are used by Splunk to monitor the website application indicating where errors
     * maybe or any intermittent issues e.g. account newsletters.
     * @param string $sourceFilePathFromApp e.g. '/plugins/rainlab/user/components/Account.php'
     * @param string $workflowClassFunction e.g. 'onUpdateMyInterests'
     * @param string $eventName e.g. 'data[category]'
     * @param mixed $eventData e.g. a single variable or array (which can be wrapped into a JSON string; for diagnostics of settings)
     * @return void
     */
    public static function myAccountFeatureslog($sourceFilePathFromApp, $workflowClassFunction, $eventName, $eventData) {
        
        if(null === Auth::user()) {
            return;
        }
        
        // populate logging object with meta data to filter
        try {
            $pamLog = new PamSplunkTroubleShooting();
            $pamLog->host=URL::current();
            $pamLog->user_id=(null !== Auth::user()) ? Auth::user()->id : 9999999;
            $pamLog->sourcetype='my_account_features';
            $pamLog->workflow_feature='membership_subscription_change';
            
            // send this to the database
            $pamLog->saveLogData($pamLog, $sourceFilePathFromApp, $workflowClassFunction, $eventName, $eventData);
            
        } catch (Exception $ex) {
            // send this error to storage/logs/system.log | when the logger does not work
            try {
                tracelog("an exception was thrown from the PamSplunkTroubleShooting logger myAccountFeatureslog");
                return false;
                
            } catch (Exception $ex) {
                // do nothing here... operation failed
                return false;
            }
        }
        // after log saved... operation success
        return true;
    }
    
    
    /**
     * used to log variable states in functions and the functions in workflows, these
     * values are used by Splunk to monitor the website application indicating where errors
     * maybe or any intermittent issues specific to sending newsletters.
     * @param string $sourceFilePathFromApp e.g. '/plugins/rainlab/user/components/Account.php'
     * @param string $workflowClassFunction e.g. 'onUpdateMyInterests'
     * @param string $eventName e.g. 'data[category]'
     * @param mixed $eventData a single variable or array (which can be wrapped into a JSON string; for diagnostics of settings)
     * @return void
     */
    public static function sendingEmailFeatureslog($sourceFilePathFromApp, $workflowClassFunction, $eventName, $eventData) {
        
        // send this to the table for logging splunk insights
        try {
            $pamLog = new PamSplunkTroubleShooting;
            $pamLog->host=URL::current();
            $pamLog->user_id=(null !== Auth::user()) ? Auth::user()->id : 9999999;
            $pamLog->sourcetype='sending_email_features';
            $pamLog->workflow_feature='user_misconfigured_email';
            
            // send this to the database
            $pamLog->saveLogData($pamLog, $sourceFilePathFromApp, $workflowClassFunction, $eventName, $eventData);
            
        } catch (Exception $ex) {
            // send this error to storage/logs/system.log | when the logger does not work
            try {
                tracelog("an exception was thrown from the PamSplunkTroubleShooting logger sendingEmailFeatureslog");
                return false;
                
            } catch (Exception $ex) {
                // do nothing here... operation failed
                return false;
            }
        }
        // after log saved... operation success
        return true;
    }
    
    
    /**
     * used to log variable states in functions and the functions names in workflows, these
     * values are used by Splunk to monitor the website application indicating where errors
     * maybe or any intermittent issues specific to group emails and mailing lists
     * @param string $sourceFilePathFromApp e.g. '/plugins/rainlab/user/components/Account.php'
     * @param string $workflowClassFunction e.g. 'onUpdateMyInterests'
     * @param string $eventName e.g. 'data[category]'
     * @param mixed $eventData e.g. a single variable or array (which can be wrapped into a JSON string; for diagnostics of settings)
     * @return void
     */
    public static function groupEmailFeatureslog($sourceFilePathFromApp, $workflowClassFunction, $eventName, $eventData) {
        
        // send this to the table for logging splunk insights
        try {
            $pamLog = new PamSplunkTroubleShooting;
            $pamLog->host=URL::current();
            $pamLog->user_id=(null !== Auth::user()) ? Auth::user()->id : 9999999;
            $pamLog->sourcetype='sending_email_features';
            $pamLog->workflow_feature='user_misconfigured_group';
            
            // send this to the database
            $pamLog->saveLogData($pamLog, $sourceFilePathFromApp, $workflowClassFunction, $eventName, $eventData);
            
        } catch (Exception $ex) {
            // send this error to storage/logs/system.log | when the logger does not work
            try {
                tracelog("an exception was thrown from the PamSplunkTroubleShooting logger groupEmailFeatureslog");
                return false;
                
            } catch (Exception $ex) {
                // do nothing here... operation failed
                return false;
            }
        }
        // after log saved... operation success
        return true;
    }
    
    
    
    /**
     * check for any subscribers that are not registered as users.
     */
    public static function checkForSubscribersNotRegisteredUsers() {
        
        // mark the start time before executing
        $startTime = microtime(true);
        
        // get the users list - 8 seconds with X records
        $usersList = DB::table('users')->pluck('email')->toArray();
        // get the subscribers list - X seconds with X records
        $subsList = DB::table('responsiv_campaign_subscribers')->pluck('email')->toArray();
        
        // return the subscriber emails not in the user list
        $noUserResult = array_diff($subsList, $usersList);
        
        // mark the end time after execution
        $endTime = microtime(true);
        $timeTaken = '***It took ['.date("H:i:s", strtotime($endTime - $startTime)). '] to execute this function***\r';
        
        // write the data to a file in the /tests/audit/ directory list of users who subscribed but no longer exist as users
        //file_put_contents(base_path().'/tests/audit/'.static::$subscriptionNoUserCheck, $timeTaken);
        file_put_contents(base_path().'/tests/audit/'.static::$subscriptionNoUserCheck, implode(', ', $noUserResult), FILE_APPEND);
        static::checkForRegisteredUsersNotSubscribed();
    }
    
    
    /**
     * check for any registered users that are not subscribed.
     */
    public static function checkForRegisteredUsersNotSubscribed() {
        
        // mark the start time before executing
        $startTime = microtime(true);
        
        // get the users list - 8 seconds with X records
        $usersList = DB::table('users')->pluck('email')->toArray();
        // get the subscribers list - X seconds with X records
        $subsList = DB::table('responsiv_campaign_subscribers')->pluck('email')->toArray();
        
        // return the users not in the subscriber list
        $noSubscriptionResult = array_diff($usersList, $subsList);
        
        // mark the end time after execution
        $endTime = microtime(true);
        $timeTaken = '***It took ['.date("H:i:s", strtotime($endTime - $startTime)). '] to execute this function***\r';
        
        // write the data to a file in the /tests/audit/ directory list of users who have not subscribed to any newsletter
        //file_put_contents(base_path().'/tests/audit/'.static::$userWithNoSubscriptionCheck, $timeTaken);
        file_put_contents(base_path().'/tests/audit/'.static::$userWithNoSubscriptionCheck, implode(', ', $noSubscriptionResult), FILE_APPEND);
        
    }
    
}
