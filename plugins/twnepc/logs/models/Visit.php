<?php namespace Twnepc\Logs\Models;

use Model;

/**
 * Model
 */
class Visit extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsToMany = [
        'user' => 'Rainlab\User\Models\User'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'user_visits';
}
