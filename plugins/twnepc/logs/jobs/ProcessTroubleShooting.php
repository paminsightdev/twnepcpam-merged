<?php
namespace Twnepc\Logs\jobs;

use Twnepc\Logs\Models\PamSplunkTroubleShooting;

class ProcessTroubleShooting {

    /**
     * A job handler which receives a Job instance as well as the array of data 
     * that was pushed onto the queue.
     * @param Class $job
     * @param mixed $data
     */
    public function fire($job, $data) {
        
       // pass the data to a defined function name in the PamSplunkTroubleShooting instance
       PamSplunkTroubleShooting::{$data['functionName']}(
                $data['sourceFilePathFromApp'], 
                $data['workflowClassFunction'], 
                $data['eventName'], 
                $data['eventData']
            );
        
        // remove the job from the queue
        $job->release();
    }
}
