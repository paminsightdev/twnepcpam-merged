<?php namespace Twnepc\Logs;

use System\Classes\PluginBase;
use Twnepc\Logs\Components\PageVisits;
use Twnepc\Logs\Components\UserVisits;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            PageVisits::class => 'pagevisits',
            UserVisits::class => 'uservisits',
        ];
    } 

    public function registerSettings()
    {
    }
}
