<?php namespace Twnepc\News\Models;

use Model;

/**
 * Model
 */
class Rating extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    protected $fillable = ['user_id','rating','article_id','site_id'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_news_article_ratings';

    public $belongsTo = [
        'user' => 'Rainlab\User\Models\User',
        'article' => 'Twnepc\News\Models\Article'
    ];
}
