<?php namespace Twnepc\News\Models;

use Input;
use Model;
use Exception;
use BackendAuth;
use Backend\Models\User;
use Twnepc\News\Models\Rating;
use Twnepc\Author\Models\Author;
use Twnepc\News\Models\Category;
use Twnepc\Regions\Models\Region;
use Illuminate\Support\Facades\DB;
use Keios\Multisite\Models\Setting;
use October\Rain\Support\Facades\Flash;
use Twnepc\Widgets\Classes\ComponentHelper;
use Illuminate\Validation\ValidationException;
/**
 * Model
 */
class Article extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Revisionable;
    use \October\Rain\Database\Traits\Sortable;
    
    const SORT_ORDER = 'order';

    protected $revisionable = ['title','author_id', 'body','status_id','site_id','is_free','category_id','region_id','contributor','subeditor','author_checkedby','order','published_date','sponsored','slug','is_free','breaking','newsfeed','premium','not_include_in_email'];
    
    public $revisionableLimit = 25;

    /**
     * @var array Validation rules
     */
    public $rules = [
        'title' => 'required',
        'published_date' => 'required',
        'slug' => 'required|unique:twnepc_news_articles,slug',
        'site_id' => 'required',
    ];

    public $fillable = ['site_id'];


    public $jsonable = ['files'];
    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_news_articles';

    /**
     * @TODO: move to external model
     */
    public $belongsTo = [
        'site' => ['Keios\Multisite\Models\Setting',
                    'scope' => 'filterByBackendUser'],
        'status' => ['Twnepc\News\Models\Status'],
        'author' => ['Twnepc\Author\Models\Author'],
        'author' => ['Twnepc\Author\Models\Author',
                      'conditions' => 'status = 1'
                      ],
        'admin_author' => ['Backend\Models\User'],
    ];
    
    public $hasMany = [
        'rating' =>['Twnepc\News\Models\Rating'],
        
    ];

    public $belongsToMany = [
        'region' => ['Twnepc\Regions\Models\Region'],
        'related_article' => ['Twnepc\News\Models\Article','table' => 'article_article','key' => 'article_id','otherKey'=>'relation_id'],
        //'related_article' => ['Twnepc\News\Models\Article'],
        'category' => ['Twnepc\News\Models\Category'],
        'fund' => ['Twnepc\Fund\Models\Fund'],
        
    ];

    public $attachMany = [
        'attachment' => ['System\Models\File']
    ];
    
    public $attachOne = [
        'lead_image' => 'System\Models\File',
        
    ];


    public $morphMany = [
        'revision_history' => ['System\Models\Revision', 'name' => 'revisionable']
    ];
    
    public function relatedArticles() {
        return $this->related_article();
    }

    public function getRevisionableUser()
    {
        return BackendAuth::getUser()->id;
    }

    public function scopePublished($query)
    {
        return $query->where('status_id', '=', 3);
    }

    public function scopeSite($query) {
    
        
        return $query->where('site_id', '=', $site);
    
    }

    

    public function scopeOverallRating() {
        //$revisionSum = Rating::where('article_id',$this->id)->sum('rating');
        $revisionAvg = Rating::where('article_id',$this->id)->avg('rating');
        return $revisionAvg ? $revisionAvg : 0;
    }

    public function articleHitsCount(){

        $hits = DB::table('article_user')->select('id')->where('article_id',$this->id)->count();

        

        return $hits;
    }

    public function scopeSiteFilter($query,$value) {
        return $query->where('status_id',$value);
    }

    public function scopeFilterCategory($query, $categoryId = null) {
        if($categoryId) {
            $category = Category::find($categoryId);
            return $category ? $category->articles()->limit(4) : $query;
        
        } else {
        
            return $query;
        
        }
    }



    public function scopeRegionsToArray() {
        $regions = $this->region;
        $r = [];
        foreach($regions as $reg) {
            array_push($r,$reg->id);
        }

        return $r;
    }

    public function scopeGetMainCategories(){
        $categories = $this->category;

        $c = [];
        foreach($categories as $cat) {
            if(!$cat->parent_id) {
                array_push($c,$cat->id);
            }
        }

        return $c;
    }

    public function getCategoryOptions($value,$data)
    {
        $site = Input::get('site');
        $categories = [];
        $theme = $data->site;
        
        if($theme) {
            $categories = Category::where('site_id',$theme->id)->listsNested('name','id','&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
        }

        return $categories;
    }

    public function getRegionOptions($value,$data)
    {
        $site = Input::get('site');
        $regions = [];
        $theme = $data->site;
        
        if($theme) {
            $ids = DB::table('region_setting')->where('setting_id',$theme->id)->pluck('region_id');
            $regions = Region::whereIn('id',$ids)->pluck('name','id');
        }

        return $regions;
    }
    
    public function getAuthorOptions()
    {   
        return $author = Author::where('status',1);
    }
    
    public function beforeCreate()
    {
        $this->admin_author_id = BackendAuth::getUser()->id;
    }

    public function beforeSave() {
        
    }

    public function setSortableOrder($itemIds, $itemOrders = null)
    {
        if (!is_array($itemIds)) {
            $itemIds = [$itemIds];
        }

        if ($itemOrders === null) {
            $itemOrders = $itemIds;
        }

        if (count($itemIds) != count($itemOrders)) {
            throw new Exception('Invalid setSortableOrder call - count of itemIds do not match count of itemOrders');
        }
        $i = 10;
        foreach ($itemIds as $index => $id) {
            $order = $i;
            $this->newQuery()->where($this->getKeyName(), $id)->update([$this->getSortOrderColumn() => $order]);
            $i += 10;
        }
    }
}
