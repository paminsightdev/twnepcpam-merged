<?php namespace Twnepc\News\Models;

use Model;
/**
 * Model
 */
class Category extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Sortable;
    use \October\Rain\Database\Traits\SimpleTree;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_news_categories';
    
    /**
     * @TODO: move to external model
     */
    public $belongsTo = [
        'site' => ['Keios\Multisite\Models\Setting','scope' => 'filterByBackendUser'],
        'parent' => ['Twnepc\News\Models\Category']
    ];

    public function childs() {
        return $this->hasMany('Twnepc\News\Models\Category','parent_id','id') ;
    }

    public $belongsToMany = [
        'articles' => ['Twnepc\News\Models\Article']
    ];

    public function categoryArticles($region) {
        return $this->articles();
    }


}
