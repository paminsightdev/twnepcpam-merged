<?php namespace Twnepc\News\Models;

use Model;
/**
 * Model
 */
class Gift extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_gift';

    public $belongsTo = [
        'user' => ['Rainlab\User\Models\User'],
        'article' => ['Twnepc\News\Models\Article']
    ];

}
