<?php namespace Twnepc\News\Models;

use Model;

/**
 * Model
 */
class Group extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_news_groups';


    public $belongsTo = [
        'site' => ['Keios\Multisite\Models\Setting'],
    ];
}
