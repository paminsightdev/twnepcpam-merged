$(document).ready(function(){

    $('.rate-icon').on('click',function(){

        $('.hidden-rating').val($(this).data('number'));

        $('.rate-icon').each(function(index,item){
            if($(this).data('number') <= $('.hidden-rating').val()) {
                $(this).removeClass('fa-star-o');
                $(this).removeClass('fa-star');
                $(this).addClass('fa-star');
            }
            else {
                $(this).removeClass('fa-star-o');
                $(this).removeClass('fa-star');
                $(this).addClass('fa-star-o');
            }
        });

    });

})