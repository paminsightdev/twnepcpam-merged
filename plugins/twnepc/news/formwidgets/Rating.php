<?php namespace Twnepc\News\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Config;

use Twnepc\News\Models\Article;


class Rating extends FormWidgetBase {

    public function widgetDetails() {
        
        return NULL;


    }

    public function prepareVars(){
        $this->vars['id'] = $this->model->id;
        $this->vars['name'] = $this->formField->getName().'[]';
        $this->vars['value'] = $this->getLoadValue();


    }

    public function render() {

        $this->prepareVars();
        return $this->makePartial('widget');
    }

    public function loadAssets() {
        $this->addCss('css/rate.css');
        $this->addJs('js/rate.js');
    }

}
