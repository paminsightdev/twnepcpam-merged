<?php namespace Twnepc\News\Components;

use DB;
use Auth;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Carbon;
use Twnepc\Widgets\Classes\ComponentHelper;

class PopularFavourite extends ComponentBase {
    public $popular;

    public $favourite;
    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.news::lang.components.popular.name',
            'description' => 'twnepc.news::lang.components.popular.description'
        ];
    }

    public function defineProperties(){
        
        return [
            'pnum' => [
                'title'       => 'popular',
                'description' => 'Popular Number',
                'default'     => '5',
                'type'        => 'string'
            ],
            'fnum' => [
                'title'       => 'favourite',
                'description' => 'Popular Number',
                'default'     => '5',
                'type'        => 'string'
            ],
            'site' => [
                'title'             => 'Site',
                'type'              => 'dropdown',
                'showExternalParam' => false
            ],
        ];
    }

    public function onRun() {

        $this->popular = $this->page['popular'] = $this->collectPopular($this->property('pnum'));
        $this->favourite = $this->page['favourite'] = $this->collectFav($this->property('fnum'));

       
        
    }


    public function collectPopular($num) {
       //return DB::select(DB::raw('select article.title,article.slug, article_id, SUM(rating) as rating from twnepc_news_article_ratings left join twnepc_news_articles as article on article.id=twnepc_news_article_ratings.article_id  group by article_id order by rating DESC limit '.$num));
       return DB::table('article_user')
        ->select('article.title','article.slug','article.published_date','article.site_id','article_id',DB::raw('COUNT(article_user.id) as popular'))
        //->where('article.site_id',$this->property('site'))
        //->where('article.published_date','>',Carbon::today()->subMonths(24))
        ->leftJoin('twnepc_news_articles as article','article.id','=','article_user.article_id')
        ->where('article.published_date','>=',Carbon::today()->subDays(14))
        ->groupBy('article_id')
        ->having('article.site_id', $this->property('site'))
        ->orderBy('popular','DESC')
        ->limit($num)
        ->get();
   
    }

    public function collectFav($num) {
       //return DB::select(DB::raw('select article.title,article.slug, article_id, COUNT(user_id) as count from article_fav_user left join twnepc_news_articles as article on article.id=article_fav_user.article_id  group by article_id order by count DESC limit '.$num));
      
       if (Auth::getUser()) {
           $userId = Auth::getUser()->id;
           return DB::table('article_fav_user')
           ->where('article_fav_user.user_id',$userId)
           ->select('article.title','article.slug','article.site_id','article_id',DB::raw('COUNT(user_id) as count'))
           ->where('article.site_id',$this->property('site'))
           ->leftJoin('twnepc_news_articles as article','article.id','=','article_fav_user.article_id')
           ->groupBy('article_id')
           ->orderBy('article_fav_user.created_at','DESC')
           ->limit($num)
           ->get();
       }
    }

    public function getSiteOptions()
    {
        return ComponentHelper::instance()->listSites();
    }



}