<?php namespace Twnepc\News\Components;

use Keios\Multisite\Classes\SettingHelper;
use Lang;
use Cms\Classes\ComponentBase;
use Twnepc\News\Models\Article as ArticleModel;
use Auth;

class RelatedArticle extends ComponentBase {

    public $slug;

    public $user;

    public $articles;

    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.news::lang.components.related-article.name',
            'description' => 'twnepc.news::lang.components.related-article.description'
        ];
    }

    public function defineProperties(){
        
        return [
            'slug' => [
                'title'       => 'slug',
                'description' => 'Artist slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }

    public function onRun() {
        $this->articles = $this->page['articles'] = $this->loadArticles();
        
        $this->user = $this->page['user'] = Auth::getUser();
    }

    protected function loadArticles(){
        $site = SettingHelper::getCurrentSite();
        $article = ArticleModel::where('slug',$this->property('slug'))
            ->where('site_id', $site->id)
            ->first();
        if($article){
            $articles = ArticleModel::find($article->id)
                ->relatedArticles()
                ->where('status_id', 3)
                ->get();
        }else{
            $articles = ArticleModel::find(1)->relatedArticles()->get();
        }
        return $articles;
    }

}