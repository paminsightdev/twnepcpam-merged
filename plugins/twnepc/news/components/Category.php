<?php namespace Twnepc\News\Components;

use Keios\Multisite\Classes\SettingHelper;
use Lang;
use Auth;
use View;
use App;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Session;
use Twnepc\News\Models\Article as ArticleModel;
use Twnepc\news\Models\Category as CategoryModel;


class Category extends ComponentBase {

    public $slug;
    public $category;
    public $articles;
    public $site;

    public $sum;
    public $nextPage;
    public $prevPage;
    public $hasMore;
    public $current;
    public $pag;
    public $count;

    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.news::lang.components.category.name',
            'description' => 'twnepc.news::lang.components.category.description'
        ];
    }

    public function defineProperties(){
        
        return [
            'slug' => [
                'title'       => 'slug',
                'description' => 'Category slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
            'pag' => [
                'title'       => 'slug',
                'description' => 'Category slug',
                'default'     => '1',
                'type'        => 'string'
            ]
        ];
    }

    

    public function onRun() {
        $this->site = SettingHelper::getCurrentSite();
        $this->domain = $this->page['domain'] = $this->site->domain;
        $this->category = $this->page['category'] = CategoryModel::where('identifier',$this->property('slug'))
            ->where('site_id', $this->site->id)
            ->first();

        if(!$this->category) {
            return \Redirect::to('404');
        }

        $this->articles = $this->page['articles'] = $this->loadArticles();
        if($this->articles){
                $this->sum = $this->page['sum'] =  $this->articles->total();
                $this->nextPage = $this->page['nextPage'] =  $this->articles->nextPageUrl();
                $this->prevPage = $this->page['prevPage'] =  $this->articles->previousPageUrl();
                $this->hasMore = $this->page['hasMore'] =  $this->articles->hasMorePages();
                $this->current = $this->page['current'] =  $this->articles->currentPage();
                $this->pag = $this->page['pag'] = $this->property('pag');
                $this->count = $this->page['count'] = $this->articles->count();
        }
    }

    protected function loadArticles(){
        $region = Session::get('region','all');
        if($this->property('slug')) { 
            $model = CategoryModel::where('identifier',$this->property('slug'))->where('site_id', $this->site->id)->first();
   	        $articles = [];
            if($model){
                $articles = $model->articles()->with('region');

               if($region != 'all'){
                    $articles = $articles->whereHas('region',function($query) use ($region) {
                        $query->where('region_id',$region);
                    });
               }

                $articles = $articles->where('site_id',$this->site->id)
                    ->where('status_id',3)
                    ->orderBy('published_date','DESC')
                    ->paginate($this->property('pag'));
            }
        } else {
            $articles = ArticleModel::where('site_id',$this->site->id)
                ->where('status_id',3)
                ->with('region');

            if($region != 'all'){
                $articles = $articles->whereHas('region',function($query) use ($region) {
                    $query->where('region_id',$region);
                });
            }

            $articles = $articles->orderBy('published_date','DESC')
                ->paginate($this->property('pag'));
        }

        return $articles;
    }
}
