<?php namespace Twnepc\News\Components;

use DB;
use Auth;
use Lang;
use RainLab\User\Components\Session as userSession;
use Request;
use Cms\Classes\ComponentBase;
use Twnepc\News\Models\Rating;
use Illuminate\Support\Facades\Session;
use Keios\Multisite\Classes\SettingHelper;
use Twnepc\News\Models\Article as ArticleModel;
use Twnepc\User\Components\Subscriptioncheck;


class Article extends ComponentBase {

    public $slug;

    public $feedbackEmail = false;

    public $user;

    public $article;

    public $rating;

    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.news::lang.components.article.name',
            'description' => 'twnepc.news::lang.components.article.description'
        ];
    }

    public function defineProperties(){
        
        return [
            'slug' => [
                'title'       => 'slug',
                'description' => 'Article slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ]
        ];
    }

    public function onRun() {
        $site = SettingHelper::getCurrentSite();

        $this->addJs('assets/js/rate.js');
        $this->addJs('assets/js/fav.js');
        
        $this->article = $this->loadArticle();
                
        Session::put('article_url',$this->currentPageUrl());
        
        if($this->article){
            
            $user = Auth::getUser();
            
            // modified code to allow articles to be all free - set from the article backend admin site
            // when no articles found - load articles returns 'null' not '0'
            //checkSubscription();
            if(static::isArticlePreview($user, $this->article)) {
                
                // when not logged in and not free: get the article and decide whether to show full content or some of the content based on access/location
                $this->article = $this->page['article'] = $this->showSectionOfArticleContentForNonMember($this->article);
                
            } else {
                
                // when it's free load the whole article
                $this->page['article'] = $this->article;
            }
            
            $this->is_article_preview = $this->page['is_article_preview'] = static::isArticlePreview($user, $this->article);
            $this->user = $this->page['user'] = Auth::getUser();
            $this->rating = $this->page['rating'] = $this->getUserRating($this->user,$this->article);
            $this->rated_already = $this->page['rated_already'] = ($this->rating > 0);
            $this->fav = $this->page['fav'] = $this->isArticleFaved($this->user,$this->article);
            $this->page['is_free'] = $this->article->is_free;

            if ($site->id == 1) {
                $this->feedbackEmail = '&#110;&#101;&#119;&#115;&#064;&#116;&#104;&#101;&#119;&#101;&#097;&#108;&#116;&#104;&#110;&#101;&#116;&#046;&#099;&#111;&#109;';
            } elseif ($site->id == 2) {
                $this->feedbackEmail = '&#110;&#101;&#119;&#115;&#064;&#101;&#112;&#114;&#105;&#118;&#097;&#116;&#101;&#099;&#108;&#105;&#101;&#110;&#116;&#046;&#099;&#111;&#109;';
            } elseif ($site->id == 4) {
                $this->feedbackEmail = '&#115;&#117;&#112;&#112;&#111;&#114;&#116;&#064;&#112;&#097;&#109;&#105;&#110;&#115;&#105;&#103;&#104;&#116;&#046;&#099;&#111;&#109;';
            }

        }else{
            return \Redirect::to('404');
        }
        
    }
    
    
    /**
     * evaluate whether to show full article or preview article
     * @param object $user
     * @param object $article
     * @return boolean
     */
    public static function isArticlePreview($user, $article) {
        
        // when the article is free view full article
        if($article->is_free == 1) {
            return false;
        }
        
        // as a gifte view the full article
        if(Subscriptioncheck::isArticleAGift($article)) {
            return false;
        }
        
        // if you are not logged in view article preview
        if(null === $user) {
            return true;
        }
        
        // if you are logged in and subscribed then view full article
        if(userSession::isUserSubscribedToWebsite($user)) {
            return false;
        }
        
        // reurn preview by default
        return true;
    }
    
    
    
    /**
     * evaluate who the user is i.e. social media, non-registered or registered.
     * @param type $article - the full article from database
     * @return string $article - amount of article content
     */
    public function showSectionOfArticleContentForNonMember($article) {
        
        $user = Auth::getUser();
        
        if(!static::isArticlePreview($user, $article)) {
            // when there is a user logged in...
            return $article;
            
        } else {

            // when there is no user logged in...
            //#1.)$portionOfArticle = substr($article->body, 0, (strlen($article->body) / 2));
            
            $portionOfArticle = substr($article->body, 0, 650);
            
            // check where the last hyerlink start anchor is...
            $posLastAnchor = strripos($portionOfArticle, '<a href=');
            $posLastAnchorEnd = stripos($article->body, '</a>', $posLastAnchor);
            
            // there are hyperlinks in the content so lets be careful when we render this portion of article...
            if(isset($posLastAnchor) && !empty($posLastAnchor)) {
                
                // hyperlink overlaps content end causing following sections of the page to be active...
                if(strlen($portionOfArticle) < ($posLastAnchorEnd + 4)) {
                    
                    // has a hyperlink overlapping content end, include that link in the content...
                    $newArticleLength = stripos($article->body, '</a>', $posLastAnchor) + 4;
                    $article->body = substr($article->body, 0, $newArticleLength).'...';
                    return $article;
                    
                } elseif(strlen($portionOfArticle) > ($posLastAnchorEnd + 4)) {
                    
                    // has a hyperlink within content, but just return half the article...
                    $article->body = $portionOfArticle.'...';
                    return $article;
                }
            }
            
            // no hyperlink with content, again just return half the article...
            $article->body = $portionOfArticle.'...';
            return $article;
            
        }
    }
    
    
    public function isArticleFaved($user,$article) {
        $user = Auth::getUser();
	    $fav = 0;
	
        if($user){
            $fav = DB::table('article_fav_user')->select('*')->where('user_id',$user->id)->where('article_id',$article->id)->count();
        }
        
        return ($fav > 0);
    }

    public function getUserRating($user,$article) {
        $rating = 0;
        if($user){
            $result = Rating::where('user_id',$user->id)->where('article_id',$article->id)->first();
            if($result) {
                $rating = $result->rating;
            }
        }

        return $rating;
    }

    public function onRate() {
       $rating = Request::get('ratings');
       $article = Request::get('article_id');
       $user = Auth::getUser();

        $site = SettingHelper::getCurrentSite();

        $result = Rating::create([
            'rating' => $rating,
            'article_id' => $article,
            'site_id' => $site->id,
            'user_id' => $user ? $user->id : 0
        ]);

       $this->article = $this->page['article'] = $this->loadArticle();
       $this->page['rating'] = $this->article->overallRating();
       
       return [
            '#result' => $this->renderPartial('article::rateresult')
        ];
    }

    public function onFav(){
        
        $article = Request::get('article');
        $user = Request::get('user');
        $site = SettingHelper::getCurrentSite();
        $this->article = $this->page['article'] = $this->loadArticle();
        $this->user = $this->page['user'] = Auth::getUser();
        
        $faved = $this->isArticleFaved($this->user,$this->article);

        if(!$faved) {

            DB::table('article_fav_user')->insert(
                ['article_id' => $article,'user_id' =>$user, 'site_id' => $site->id]
            );

        }

       
        $this->fav = $this->page['fav'] = $this->isArticleFaved($this->user,$this->article);
        return [
            '#fav-result' => $this->renderPartial('article::favourite')
        ];
    }

   

    protected function loadArticle(){
        $site = SettingHelper::getCurrentSite();
        $article = ArticleModel::where('slug',$this->property('slug'))
            ->orderBy('published_date','desc')
            ->where('site_id', $site->id)
            ->first();
        return $article;
    }

}
