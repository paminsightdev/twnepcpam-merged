<?php namespace Twnepc\News\Components;

use App;
use Auth;
use Lang;
use View;

use Cms\Classes\ComponentBase;
use Illuminate\Support\Carbon;
use Keios\Multisite\Models\Setting;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Twnepc\News\Models\Gift as GiftModel;
use Twnepc\News\Models\Article as ArticleModel;
use Twnepc\news\Models\Category as CategoryModel;

class Gift extends ComponentBase {

    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.news::lang.components.gift.name',
            'description' => 'twnepc.news::lang.components.gift.description'
        ];
    }

    public function onRun() {
        $this->addCss('assets/css/gift.css');
    }


    public function onSendGift() {

        $data = post();
        
        //create random number
        $hash = bin2hex(random_bytes(16));

        $encrypted = Crypt::encrypt($hash);

        $user = Auth::getUser();

        $gift = new GiftModel();

        $gift->hash = $hash;
        $gift->email = array_get($data,'giftemail');
        $gift->message = array_get($data,'giftmessage');
        $gift->user_id = $user->id;
        $gift->article_id = array_get($data,'article');

        $gift->save();
        $article = ArticleModel::find(array_get($data, 'article'));

        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 

        $html = "<h3>Dear User,</h3><p>".$user->name." sent you an article as a gift.</p><p>".$actual_link."</p>";

        $params = [
            'username' => $user->name,
            'link' => $actual_link.'?gift='.$encrypted,
            'article' => $article,
            'mess' => array_get($data,'giftmessage')
        ];

        $mail = Mail::sendTo($gift->email, 'twnepc.news::mail.gift', $params, function($message) {
            $message->subject('An article was gifted to you!');
        });

        if($mail) {
            return [
                '#giftresult' => $this->renderPartial('gift::result')
            ];
        }

    }

}