$(document).ready(function(){
    var checkForInputTimer;
    var checkForInput = function () {
        if ($('input[name="Article[category]"').length > 0) {
            window.clearInterval(checkForInputTimer);
            
            $('input[name="Article[category][]"]').on('change', function(){

                var isChecked = $(this).prop('checked');
        
                $.request('onCategory', {
                    data: {
                        category: $(this).val()
                    },
                    success: function(response) {
                        console.log(response);
                        if(response[0].parent_id != null) {
                            
                            $('input[name="Article[category][]"]').each(function(index,item){
                                if($(item).val() == response[0].parent_id) {
                                    if(isChecked) {
                                        $(item).prop('checked',true);
                                    }
                                    //any other subcategory checked
                                    var otherSubChecked = false;
                                    $.each(response[1],function(index,value){
                                        console.log($('input[value='+value+']'));
                                        if($('input[value='+value+']').prop('checked')) {
                                            otherSubChecked = true;
                                        }
                                    })
                                    if(!otherSubChecked) {
                                        $(item).prop('checked',false);
                                    }
                                }
                            })
                        }
                    }});
            });
        }
    };

checkForInputTimer = window.setInterval(checkForInput, 200);
    
    

});