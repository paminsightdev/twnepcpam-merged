<?php 
use Twnepc\News\Models\Article;
use Twnepc\News\Models\Status;

class NewsHelper
{
    use Singleton;

    public function getReviewCount()
    {
        $status = Status::where('identifier','review');
        return Article::where('status_id',$status->id)->count();
    }
}
