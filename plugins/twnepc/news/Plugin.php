<?php namespace Twnepc\News;

use BackendMenu;
use Carbon\Carbon;

use Backend\Facades\Backend;
use Illuminate\Support\Facades\DB;
use System\Classes\PluginBase;
use Backend\Facades\BackendAuth;
use Twnepc\News\Components\Gift;
use Illuminate\Support\Facades\App;
use Twnepc\News\Components\Article;
use Twnepc\News\Components\Category;
use Keios\Multisite\Classes\SettingHelper;
use Twnepc\News\Components\RelatedArticle;
use Twnepc\News\Components\PopularFavourite;
use Twnepc\News\Models\Article as ArticleModel;

class Plugin extends PluginBase
{

    public function boot() {


        \Event::listen('backend.menu.extendItems', function($manager){
        
            
            $user = BackendAuth::getUser();

            if($user->role->code == 'siteadmin') {
                
                $sites = $user->site->pluck('theme')->toArray();
                if(in_array('twn',$sites)) {
                    $manager->addSideMenuItems('Twnepc.News', 'articles', [

                        'side-menu-reorder-twn' => [
                            'label' => 'twnepc.news::lang.reorder-twn',
                            'url' => Backend::url('twnepc/news/articleorder/reorder?site=twn'),
                            'icon'=>'icon-sitemap',
                            'permissions' => [
                                'view_all'
                            ]
                        ]
        
                    ]);
                }
    
                if(in_array('epc',$sites)) {
                    $manager->addSideMenuItems('Twnepc.News', 'articles', [
        
                        'side-menu-reorder-epc' => [
                            'label' => 'twnepc.news::lang.reorder-epc',
                            'url' => Backend::url('twnepc/news/articleorder/reorder?site=epc'),
                            'icon'=>'icon-sitemap',
                            'permissions' => [
                                'view_all'
                            ]
                        ]
        
                    ]);
                }
                
                if(in_array('fundeye',$sites)) {
                    $manager->addSideMenuItems('Twnepc.News', 'articles', [
        
                        'side-menu-reorder-fundeye' => [
                            'label' => 'twnepc.news::lang.reorder-fundeye',
                            'url' => Backend::url('twnepc/news/articleorder/reorder?site=fundeye'),
                            'icon'=>'icon-sitemap',
                            'permissions' => [
                                'view_all'
                            ]
                        ]
        
                    ]);
                }
            } else if($user->role->code == 'developer' || $user->is_superuser ) {
                $manager->addSideMenuItems('Twnepc.News', 'articles', [

                    'side-menu-reorder-twn' => [
                        'label' => 'twnepc.news::lang.reorder-twn',
                        'url' => Backend::url('twnepc/news/articleorder/reorder?site=twn'),
                        'icon'=>'icon-sitemap',
                        'permissions' => [
                            'view_all'
                        ]
                    ]
    
                ]);
                $manager->addSideMenuItems('Twnepc.News', 'articles', [
        
                    'side-menu-reorder-epc' => [
                        'label' => 'twnepc.news::lang.reorder-epc',
                        'url' => Backend::url('twnepc/news/articleorder/reorder?site=epc'),
                        'icon'=>'icon-sitemap',
                        'permissions' => [
                            'view_all'
                        ]
                    ]
    
                ]);
                $manager->addSideMenuItems('Twnepc.News', 'articles', [
        
                    'side-menu-reorder-fundeye' => [
                        'label' => 'twnepc.news::lang.reorder-fundeye',
                        'url' => Backend::url('twnepc/news/articleorder/reorder?site=fundeye'),
                        'icon'=>'icon-sitemap',
                        'permissions' => [
                            'view_all'
                        ]
                    ]
    
                ]);
            }



            
            


        
        });

        \Event::listen('offline.sitesearch.query', function ($query) {
            $site = SettingHelper::getCurrentSite();

            $queryPieces = explode(' ', trim($query));
            for($i=0; $i<count($queryPieces); $i++){
                $queryPieces[$i] = '+' . trim($queryPieces[$i]);
            }
            $queryString = implode(' ',$queryPieces);
            // Search your plugin's contents

            $items =  DB::table('twnepc_news_articles')
                //->leftJoin('system_files', 'twnepc_news_articles.id', '=', 'system_files.attachment_id')
                //->whereRaw('system_files.field = \'lead_image\'')
                ->select('twnepc_news_articles.title', 'twnepc_news_articles.summary', 'twnepc_news_articles.slug', 'published_date')
                ->where('site_id',$site->id)
                ->where('status_id',3)
                ->whereRaw("MATCH (twnepc_news_articles.title, body, summary) AGAINST (? IN BOOLEAN MODE)", [$queryString])
                ->orderBy('published_date','DESC')
                ->limit(1500);


            /*$items =  ArticleModel::where('site_id',$site->id)
                ->whereRaw("MATCH (title, body, summary) AGAINST (? IN BOOLEAN MODE)", [$queryString])
                ->where('status_id',3)
                ->orderBy('published_date','DESC')
                ->limit(1500);*/

            $items = $items->get();
            // Now build a results array
            $results = $items->map(function ($item) use ($query, $site) {
                return [
                    'meta'      => ['published_date' => $item->published_date],
                    'title'     => $item->title,
                    'text'      => $item->summary,
                    'url'       => $site->domain. '/article/' . $item->slug,
                    //'thumb'     => $item->lead_image, // Instance of System\Models\File
                    'relevance' => 1
                ];
            });
    
            return [
                'provider' => 'Article', // The badge to display for this result
                'results'  => $results,
            ];
        });



        if (!App::runningInBackend()) {
            return;
        }
    
        // Listen for `backend.page.beforeDisplay` event and inject js to current controller instance.
        \Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
            //$controller->addJs('/plugins/twnepc/news/assets/js/froala_editor.min.js');

            $controller->addCss('/plugins/twnepc/news/assets/css/spell_checker.min.css');
            //$controller->addJs('/plugins/twnepc/news/assets/js/spell_checker.min.js');
        });
        
        
    }

    public function registerSettings()
    {
    }

    public function registerComponents()
    {
        return [
            Article::class => 'article',
            RelatedArticle::class => 'related',
            Category::class => 'category',
            PopularFavourite::class => 'popularfavourite',
            Gift::class => 'gift',
        ];
    } 

    public function registerFormWidgets() {
        return [
            'Twnepc\News\FormWidgets\RelatedArticles' => [
                'label' => 'Related Articles',
                'code' => 'relatedarticles'
            ],
            'Twnepc\News\FormWidgets\Rating' => [
                'label' => 'Rating',
                'code' => 'rating'
            ],
            'Twnepc\News\FormWidgets\Upload' => [
                'label' => 'Customupload',
                'code' => 'customupload'
            ]
        ];
    }
}
