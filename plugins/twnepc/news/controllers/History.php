<?php namespace Twnepc\News\Controllers;

use View;
use BackendMenu;
use Backend\Models\User;
use System\Models\Revision;
use Twnepc\News\Models\Status;
use Backend\Classes\Controller;
use Twnepc\News\Models\Article;
use Twnepc\Author\Models\Author;
use Twnepc\News\Models\Category;
use Twnepc\Regions\Models\Region;
use Keios\Multisite\Models\Setting as Site;

class History extends Controller
{

    public $bodyClass = 'slim-container';

    public function __construct()
    {
        parent::__construct();
    }

    public function index() {
        
    }

    public function show($recordId = null) {
        $r = Article::find($recordId)->revision_history;

        //dump($r);

        $this->vars['revisions'] = $this->getRevisions($r);

        return $this->makePartial('show');
    }

    public function getRevisions($rev) {

        $ret = [];
        //Get relations
        $statuses = Status::all()->pluck('name','id');
        $sites = Site::all()->pluck('theme','id');
        $categories = Category::all()->pluck('name','id');
        $regions = Region::all()->pluck('name','id');
        $authors = Author::all()->pluck('author_name','id');
        foreach($rev as $r):;
            $ret["'".$r->created_at."'"] = [];
        endforeach;
        foreach($rev as $r):;
            $user = User::where('id',$r->user_id)->get();
            $item = [];
            $item['field']      = $r->field;
            $item['created_at'] = $r->created_at;
            $item['updated_at'] = $r->updated_at;
            
            switch ($item['field']) {
                case 'author_id':
                    $item['old_value']  = ($r->old_value ? $authors[$r->old_value] : null);
                    $item['new_value']  = ($r->new_value ? $authors[$r->new_value] : null);
                    break;
                case 'status_id':
                    $item['old_value']  = ($r->old_value ? $statuses[$r->old_value] : null);
                    $item['new_value']  = ($r->new_value ? $statuses[$r->new_value] : null);
                    break;
                case 'category_id':
                    $item['old_value']  = ($r->old_value ? $categories[$r->old_value] : null);
                    $item['new_value']  = ($r->new_value ? $categories[$r->new_value] : null);
                    break;
                case 'site_id':
                    $item['old_value']  = ($r->old_value ? $sites[$r->old_value] : null);
                    $item['new_value']  = ($r->new_value ? $sites[$r->new_value] : null);
                    break;
                case 'region_id':
                    $item['old_value']  = ($r->old_value ? $regions[$r->old_value] : null);
                    $item['new_value']  = ($r->new_value ? $regions[$r->new_value] : null);
                    break;
                case 'is_free':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                case 'breaking':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                case 'newsfeed':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                case 'premium':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                case 'not_include_in_email':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                
                default:
                    $item['old_value']  = $r->old_value;
                    $item['new_value']  = $r->new_value;
                    break;

            }

            //$item['old_value']  = $statuses;
            //$item['new_value']  = $r->new_value;
            
            $item['user']       = compact('user');
            array_push($ret["'".$r->created_at."'"],$item);
        endforeach;

        return array_reverse($ret);

    }
}
