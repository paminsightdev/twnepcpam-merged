<?php namespace Twnepc\News\Controllers;

use Illuminate\Support\Facades\DB;
use Request;
use BackendAuth;
use BackendMenu;
use Guzzle\Http\Url;
use Backend\Models\User;
use System\Models\Revision;
use Backend\Helpers\Backend;
use Twnepc\News\Models\Rating;
use Twnepc\News\Models\Status;
use Backend\Classes\Controller;
use Twnepc\News\Models\Article;
use Twnepc\Author\Models\Author;
use Twnepc\News\Models\Category;
use Twnepc\Regions\Models\Region;
use Illuminate\Support\Facades\Lang;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Keios\Multisite\Models\Setting as Site;

class Articles extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    protected $status;

    public function __construct()
    {
        parent::__construct();
        $this->addCss('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
        BackendMenu::setContext('Twnepc.News', 'articles');
    }

    public function index() {
        
       
        $this->status = Request::get('status');
        $this->asExtension('ListController')->index();

    }

    public function listExtendQuery($query)
    {
        $user = BackendAuth::getUser();
        if($this->status){

            $stat = Status::where('identifier',$this->status)->first();
            $query->where('status_id', $stat->id);
        
        }

        if($user->role->code == 'siteadmin') {
            if($user->site) {
                $query->whereIn('site_id', $user->site->pluck('id'));
            }
        }

        $query->addSelect(DB::raw('(SELECT count(article_user.id) FROM `article_user` WHERE `article_user`.`article_id` = `twnepc_news_articles`.`id`) AS hits'));
    }

    public function onDelete()
    {
        $selected = post('checked');
        Article::destroy($selected);

        return $this->listRefresh();
    }

    public function update($recordId = null, $context = null) {
       
        parent::update($recordId,$context);
        $this->addJs('/plugins/twnepc/news/assets/js/category.js');
        $this->addCss('/plugins/twnepc/news/assets/css/revisions.css');
        $this->addJs('/plugins/twnepc/news/assets/js/slug.js');
        $revisions = Article::find($recordId)->revision_history;
        $revisionSum = Rating::where('article_id',$recordId)->sum('rating');
        $revisionCount = Rating::where('article_id',$recordId)->count();
        $this->vars['ratings'] = $revisionCount ? $revisionSum/$revisionCount : 0;
        $this->vars['revisions'] = $this->getRevisions($revisions);
        $this->vars['recordId'] = $recordId;
    }

    public function create() {
        parent::create();
        $this->addJs('/plugins/twnepc/news/assets/js/category.js');
    }

    public function onCategory() {
        $data = post();
        
        $cat = Category::where('id',$data['category'])->first();

        $subcats = Category::where('parent_id',$cat->parent_id)->get();
        $sc = [];
        foreach($subcats as $s) {
            array_push($sc,$s->id);
        }

        return Response::json(array($cat,$sc));
    }
    
    public function onFilter(){

        $filtered = Article::where(function($query){
            $query->where('title','like','%'.post('search').'%')
                ->orWhere('summary','like','%'.post('search').'%')
                ->orWhere('body','like','%'.post('search').'%');
        });

        if(post('reason') == 'breaking'){
            $filtered->where('breaking',1);
        }

        if(post('site_id')){
            $filtered->where('site_id', post('site_id'));
        }

        $filtered
            ->with('site')
            ->orderBy('created_at', 'desc')
            ->limit(100);

        $filtered = $filtered->get();

        $data['results'] = [];
        foreach($filtered as $a) {
            $article = [
                'id' => $a->id,
                'text' => $a->title. ' - '.$a->published_date.' on '.$a->site->theme.' Site' 
            ];
            array_push($data['results'],$article);
        }
        return Response::json($data);
    }

    public function getRevisions($rev) {

        $ret = [];
        //Get relations
        $statuses = Status::all()->pluck('name','id');
        $sites = Site::all()->pluck('theme','id');
        $categories = Category::all()->pluck('name','id');
        $regions = Region::all()->pluck('name','id');
        $authors = Author::all()->pluck('author_name','id');
        foreach($rev as $r):;
            $ret["'".$r->created_at."'"] = [];
        endforeach;
        foreach($rev as $r):;
            $user = User::where('id',$r->user_id)->get();
            $item = [];
            $item['field']      = $r->field;
            $item['created_at'] = $r->created_at;
            $item['updated_at'] = $r->updated_at;
            switch ($item['field']) {
                case 'author_id':
                    $item['old_value']  = ($r->old_value ? $authors[$r->old_value] : null);
                    $item['new_value']  = ($r->new_value ? $authors[$r->new_value] : null);
                    break;
                case 'status_id':
                    $item['old_value']  = isset($statuses[$r->old_value]) ? $statuses[$r->old_value] : '';
                    $item['new_value']  = $statuses[$r->new_value];
                    break;
                case 'category_id':
                    $item['old_value']  = isset($categories[$r->old_value]) ? $categories[$r->old_value] : '';
                    $item['new_value']  = $categories[$r->new_value];
                    break;
                case 'site_id':
                    $item['old_value']  = isset($sites[$r->old_value]) ? $sites[$r->old_value] : '';
                    $item['new_value']  = $sites[$r->new_value];
                    break;
                case 'region_id':
                    $item['old_value']  = isset($regions[$r->old_value]) ? $regions[$r->old_value] : '';
                    $item['new_value']  = $regions[$r->new_value];
                    break;
                case 'is_free':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                case 'breaking':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                case 'newsfeed':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                case 'premium':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                case 'not_include_in_email':
                    $item['old_value']  = ($r->old_value ? 'On':'Off');
                    $item['new_value']  = ($r->new_value ? 'On':'Off');
                    break;
                
                default:
                    $item['old_value']  = $r->old_value;
                    $item['new_value']  = $r->new_value;
                    break;

            }

            //$item['old_value']  = $statuses;
            //$item['new_value']  = $r->new_value;
            
            $item['user']       = compact('user');
            array_push($ret["'".$r->created_at."'"],$item);
        endforeach;

        return array_reverse($ret);

    }

    public function onBulkAction(){
        $data = post();

        $articles = Article::whereIn('twnepc_news_articles.id',$data['checked']);

        $articles->update(['status_id' => 3]);

        $definition = post('definition', $this->primaryDefinition);
        Flash::success(Lang::get('twnepc.news::lang.list.publish_success'));
        $current = \Url::current();
        return Redirect::to($current.'?status=review');
    }

    public function formExtendFields($form) {

    }

    public function formAfterSave($model)
    {

        $site = $model->site->id;
        
        //ADDED MULTIPLE TIMES
        $autocategory = 'all-subjects';
        $category_to_add = Category::where('site_id',$site)->where('identifier',$autocategory)->first();

        

        if($category_to_add) {
            if(!$model->category->contains($category_to_add->id)) {
                $model->category()->attach($category_to_add->id);
            }
        }



        $data = post();
        $categories = $data['Article']['category'];
        
        if($categories) {
            foreach($categories as $cat) {
                $c = Category::where('id',$cat)->first();
                if($c->parent_id != NULL) {
                    $parent = Category::where('id',$c->parent_id)->first();
                    if(!$model->category->contains($parent->id)) {
                        $model->category()->attach($parent->id);
                    }
                }
            }
        }


        
    }

    public function update_onSave($recordId, $context = null) {
        $userid =  BackendAuth::getUser()->id;
        
        $article = Article::find($recordId);
        if($article->author_lockedby != $userid && $article->author_lockedby != null) {
             Flash::error('Locked article! You cannot save this article while it\'s locked.');
             $current = \Url::current();
             
             return Redirect::to($current);
        }else{
	    // Otherwise proceed, call the original update_onSave method
            return $this->asExtension('FormController')->update_onSave($recordId, $context);
	}
    }
    

    public function onLock($recordId = null) {
    
        $user = BackendAuth::getUser();
        
        $article = Article::find($recordId);
        $article->author_lockedby = $user->id;
        $article->save();
        Flash::success('The article is locked');
        $this->vars['recordId'] = $recordId;
        return [
            '#lock-result' => $this->makePartial('lock')
        ];
    }
    
    public function onUnlock($recordId = null) {
        
        $user = BackendAuth::getUser();
        
        $article = Article::find($recordId);
        $article->author_lockedby = null;
        $article->save();
        $this->vars['recordId'] = $recordId;
        Flash::success('The article is unlocked');

        return [
            '#lock-result' => $this->makePartial('lock')
        ];
    }

}
