<?php namespace Twnepc\News\Controllers;

use Backend\Classes\Controller;
use Backend\Facades\BackendAuth;
use BackendMenu;

class Categories extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'  ,'Backend\Behaviors\ReorderController'  ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.News', 'articles', 'side-menu-categories');
        $this->addCss('/plugins/twnepc/news/assets/css/mod.css');
    }

    public function listExtendQuery($query)
    {
        $user = BackendAuth::getUser();
        if($user->role->code == 'siteadmin') {
            if($user->site) {
                $query->whereIn('site_id', $user->site->pluck('id'));
            }
        }


    }
}
