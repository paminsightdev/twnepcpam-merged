<?php namespace Twnepc\News\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Ratings extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        $this->addCss('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');
        BackendMenu::setContext('Twnepc.News', 'rating-menu-item','ratings');
    }
}
