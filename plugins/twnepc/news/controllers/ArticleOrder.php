<?php namespace Twnepc\News\Controllers;

use BackendMenu;
use Illuminate\Http\Request;
use Backend\Classes\Controller;
use Twnepc\News\Models\Article;
use Backend\Facades\BackendAuth;
use Illuminate\Support\Facades\Input;
use Keios\Multisite\Models\Setting as Sites;

/**
 * Reorder Controller Back-end Controller
 */
class ArticleOrder extends Controller
{
    public $implement = [
        'Backend.Behaviors.ReorderController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        $this->addCss('/plugins/twnepc/news/assets/css/articleorder.css');
        BackendMenu::setContext('Twnepc.News', 'articles', 'side-menu-reorder');
       
         
    }


    public function reorderExtendQuery($query)
    {
        $user = BackendAuth::getUser();

        if($user->role->code == 'siteadmin') {
            if($user->site) {
                $query->whereIn('site_id', $user->site->pluck('id'));
            }
        }

        $siteFilter = Input::get('site');


        $latest = Article::where('status_id',3)
            ->orderBy('published_date', 'desc');

        if($siteFilter) {
            $site = Sites::where('theme',$siteFilter)->first();
            $query->where('site_id', $site->id);
            $latest->where('site_id', $site->id);
        }

        $latest = $latest->first();
        $date = $latest->published_date;
        $query->where('published_date',$date);
        $query->where('status_id',3);
    }

    public function reorderGetRecordName($record)
    {

        $config['ordertitle'] = $record->id.' - '.$record->title. ' - '.$record->site->theme;

        return $this->makePartial('reorder_title', $config);
    }
}
