<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcNewsGroups extends Migration
{
    public function up()
    {
        Schema::create('twnepc_news_groups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('identifier');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_news_groups');
    }
}
