<?php namespace Twnepc\News\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTwnepcGiftTable extends Migration
{
    public function up()
    {
        Schema::create('twnepc_gift', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('email');
            $table->text('hash');
            $table->integer('viewcount');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_gift');
    }
}
