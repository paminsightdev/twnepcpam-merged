<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ArticleArticle extends Migration
{
    public function up()
    {
        Schema::create('article_article', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('article_id');
            $table->integer('relation_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('article_article');
    }
}
