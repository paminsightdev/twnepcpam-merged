<?php  namespace Twnepc\News\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcAuthorAuthorTableDeleteContributorField extends Migration
{
    public function up()
    {
        Schema::table('twnepc_author_author', function($table)
        {
            $table->dropColumn(['contributor']);
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_author_author')) {
            Schema::table('twnepc_author_author', function ($table) {
                $table->integer('contributor')->default(-1);
            });
        }
    }
}