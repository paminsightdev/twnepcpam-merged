<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddGiftMessage extends Migration
{
    public function up()
    {
        Schema::table('twnepc_gift', function($table)
        {
            $table->string('message');
        });
    }
    
    public function down()
    {
    }
}
