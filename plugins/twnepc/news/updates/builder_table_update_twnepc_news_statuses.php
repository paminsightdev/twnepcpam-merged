<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsStatuses extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_statuses', function($table)
        {
            $table->increments('id')->unsigned(false)->change();
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_statuses', function($table)
        {
            $table->increments('id')->unsigned()->change();
            $table->timestamp('created_at')->nullable()->default('NULL');
            $table->timestamp('updated_at')->nullable()->default('NULL');
        });
    }
}
