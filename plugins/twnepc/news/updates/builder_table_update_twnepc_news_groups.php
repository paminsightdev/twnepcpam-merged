<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsGroups extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_groups', function($table)
        {
            $table->integer('site_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_groups', function($table)
        {
            $table->dropColumn('site_id');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
