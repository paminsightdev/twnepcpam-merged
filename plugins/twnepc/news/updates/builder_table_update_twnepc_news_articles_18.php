<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles18 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->string('slug');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
