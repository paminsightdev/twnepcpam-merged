<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles21 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->integer('oldtwn_article_id')->nullable()->default(-1);
            $table->integer('oldepc_article_id')->nullable()->default(-1);
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->dropColumn('oldtwn_article_id');
            $table->dropColumn('oldepc_article_id');
        });
    }
}
