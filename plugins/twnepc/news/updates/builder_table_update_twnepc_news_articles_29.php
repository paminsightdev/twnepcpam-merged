<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles29 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->boolean('premium')->nullable(false)->unsigned(false)->default(null)->change();
            $table->boolean('exclusive')->nullable(false)->unsigned(false)->default(null)->change();
            $table->boolean('front')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->integer('premium')->nullable(false)->unsigned(false)->default(null)->change();
            $table->integer('exclusive')->nullable(false)->unsigned(false)->default(null)->change();
            $table->integer('front')->nullable(false)->unsigned(false)->default(null)->change();
        });
    }
}
