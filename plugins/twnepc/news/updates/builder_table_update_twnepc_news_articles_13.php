<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles13 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->boolean('breaking')->default(-1)->change();
            $table->boolean('newsfeed')->default(-1)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->boolean('breaking')->default(null)->change();
            $table->boolean('newsfeed')->default(null)->change();
        });
    }
}
