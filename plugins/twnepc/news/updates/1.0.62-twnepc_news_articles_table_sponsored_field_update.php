<?php  namespace Twnepc\News\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcNewsArticlesTableSponsoredFieldUpdate extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->string('sponsored')->nullable()->change();
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_news_articles')) {
            Schema::table('twnepc_news_articles', function ($table) {
                $table->dropColumn(['sponsored']);
            });
        }
    }
}