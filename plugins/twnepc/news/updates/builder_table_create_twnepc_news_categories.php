<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcNewsCategories extends Migration
{
    public function up()
    {
        Schema::create('twnepc_news_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable()->default('NULL');
            $table->string('identifier')->nullable()->default('NULL');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_news_categories');
    }
}
