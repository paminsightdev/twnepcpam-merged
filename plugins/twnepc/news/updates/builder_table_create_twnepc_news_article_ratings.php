<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcNewsArticleRatings extends Migration
{
    public function up()
    {
        Schema::create('twnepc_news_article_ratings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('rating');
            $table->integer('article_id');
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_news_article_ratings');
    }
}
