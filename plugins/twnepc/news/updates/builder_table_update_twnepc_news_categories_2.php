<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsCategories2 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->integer('site_id')->nullable()->default(-1);
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->dropColumn('site_id');
        });
    }
}
