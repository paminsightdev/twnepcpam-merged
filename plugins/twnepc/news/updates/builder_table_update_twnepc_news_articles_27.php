<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles27 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            //$table->string('body', 65535)->nullable()->unsigned(false)->default('NULL')->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->text('body')->nullable()->unsigned(false)->default('\'NULL\'')->change();
        });
    }
}
