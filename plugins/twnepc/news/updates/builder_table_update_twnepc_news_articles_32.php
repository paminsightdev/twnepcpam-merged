<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles32 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->text('files');
            $table->integer('order')->default(0)->change();
            $table->integer('author_lockedby')->default(0)->change();
            $table->integer('author_checkedby')->default(0)->change();
            $table->string('subeditor', 191)->default('0')->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->dropColumn('files');
            $table->integer('order')->default(NULL)->change();
            $table->integer('author_lockedby')->default(NULL)->change();
            $table->integer('author_checkedby')->default(NULL)->change();
            $table->string('subeditor', 191)->default('NULL')->change();
        });
    }
}
