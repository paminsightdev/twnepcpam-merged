<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ArticleRegion extends Migration
{
    public function up()
    {
        Schema::create('article_region', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('article_id');
            $table->integer('region_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('article_region');
    }
}
