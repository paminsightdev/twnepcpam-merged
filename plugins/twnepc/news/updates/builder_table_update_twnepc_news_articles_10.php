<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles10 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->boolean('breaking');
            $table->boolean('newsfeed');
            $table->boolean('newsupdate');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->dropColumn('breaking');
            $table->dropColumn('newsfeed');
            $table->dropColumn('newsupdate');
        });
    }
}
