<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class AddIsSent extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->boolean('is_hidden')->nullable(false)->unsigned(false)->default(0);
        });
    }
    
    public function down()
    {
        
    }
}
