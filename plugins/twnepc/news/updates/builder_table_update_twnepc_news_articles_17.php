<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles17 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->date('published_date');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->dropColumn('published_date');
        });
    }
}
