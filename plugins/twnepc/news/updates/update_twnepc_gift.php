<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateTwnepcGift extends Migration
{
    public function up()
    {
        Schema::table('twnepc_gift', function($table)
        {
            $table->integer('user_id')->nullable()->default(NULL);
        });
    }
    
    public function down()
    {
    }
}
