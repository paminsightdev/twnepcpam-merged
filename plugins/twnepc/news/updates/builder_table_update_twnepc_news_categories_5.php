<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsCategories5 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->integer('parent_id')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->integer('parent_id')->default(NULL)->change();
        });
    }
}
