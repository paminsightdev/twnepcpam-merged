<?php  namespace Twnepc\News\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class AddSiteidRatings extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_article_ratings', function($table)
        {
            $table->integer('site_id')->nullable();
        });

        Schema::table('article_fav_user', function($table)
        {
            $table->integer('site_id')->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_news_article_ratings')) {
            Schema::table('twnepc_news_article_ratings', function ($table) {
                $table->dropColumn(['site_id']);
            });
        }

        if (Schema::hasTable('article_fav_user')) {
            Schema::table('article_fav_user', function ($table) {
                $table->dropColumn(['site_id']);
            });
        }
    }
}