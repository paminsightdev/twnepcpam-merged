<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsCategories12 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->integer('old_category_id')->nullable()->default(0);
            $table->integer('parent_id')->default(-1)->change();
            $table->integer('nest_left')->default(-1)->change();
            $table->integer('nest_right')->default(-1)->change();
            $table->integer('nest_depth')->default(-1)->change();
            $table->integer('sort_order')->default(-1)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->dropColumn('old_category_id');
            $table->integer('parent_id')->default(NULL)->change();
            $table->integer('nest_left')->default(NULL)->change();
            $table->integer('nest_right')->default(NULL)->change();
            $table->integer('nest_depth')->default(NULL)->change();
            $table->integer('sort_order')->default(NULL)->change();
        });
    }
}
