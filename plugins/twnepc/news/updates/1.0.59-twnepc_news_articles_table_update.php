<?php  namespace Twnepc\News\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcNewsArticlesTableUpdate extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->dropColumn(['region_id']);
            $table->integer('order')->nullable();
            $table->string('contributor')->nullable();
            $table->integer('admin_author_id');
            $table->integer('author_lockedby')->nullable();
            $table->integer('author_checkedby')->nullable();
            $table->string('subeditor')->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_news_articles')) {
            Schema::table('twnepc_news_articles', function ($table) {
                $table->integer('region_id');
                $table->dropColumn(['order']);
                $table->dropColumn(['contributor']);
                $table->dropColumn(['admin_author_id']);
                $table->dropColumn(['author_lockedby']);
                $table->dropColumn(['author_checkedby']);
                $table->dropColumn(['subeditor']);
            });
        }
    }
}