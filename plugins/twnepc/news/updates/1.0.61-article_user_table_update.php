<?php  namespace Twnepc\News\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class ArticleUserTableUpdate extends Migration
{
    public function up()
    {
        Schema::table('article_user', function($table)
        {
            $table->string('sessionid')->nullable();
            $table->string('device')->nullable();
            $table->string('source')->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('article_user')) {
            Schema::table('article_user', function ($table) {
                $table->dropColumn(['contributor']);
                $table->dropColumn(['device']);
                $table->dropColumn(['source']);
            });
        }
    }
}