<?php namespace Twnepc\News\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class ArticleFilesTable extends Migration
{
    public function up()
    {
        Schema::create('article_file', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('article_id');
            $table->integer('file_id');
            
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('article_file');
    }
}
