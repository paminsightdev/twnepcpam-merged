<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsCategories9 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->integer('parent_id')->default(null)->change();
            $table->integer('nest_left')->default(null)->change();
            $table->integer('nest_right')->default(null)->change();
            $table->integer('nest_depth')->default(null)->change();
            $table->integer('sort_order')->default(null)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_categories', function($table)
        {
            $table->integer('parent_id')->default(NULL)->change();
            $table->integer('nest_left')->default(NULL)->change();
            $table->integer('nest_right')->default(NULL)->change();
            $table->integer('nest_depth')->default(NULL)->change();
            $table->integer('sort_order')->default(NULL)->change();
        });
    }
}
