<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ArticleFavUser extends Migration
{
    public function up()
    {
        Schema::create('article_fav_user', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('article_id');
            $table->integer('user_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('article_fav_user');
    }
}
