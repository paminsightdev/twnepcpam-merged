<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles34 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            // pull a specific article record selected by the user
            $table->index(['slug', 'site_id'], 'twnepc_news_articles_idx_slug_site_id');
            $table->index('slug');
            // pull all active article records for a site
            $table->index(['status_id', 'site_id', 'newsfeed'], 'twnepc_news_articles_idx_status_id_site_id_newsfeed'); 
            
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->dropIndex('twnepc_news_articles_idx_slug_site_id');
            $table->dropIndex('slug');
            $table->dropIndex('twnepc_news_articles_idx_status_id_site_id_newsfeed');
        });
    }
}
