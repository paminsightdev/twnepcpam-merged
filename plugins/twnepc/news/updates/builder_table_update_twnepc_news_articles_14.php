<?php namespace Twnepc\News\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcNewsArticles14 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->boolean('is_free')->default(-1)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_news_articles', function($table)
        {
            $table->boolean('is_free')->default(null)->change();
        });
    }
}
