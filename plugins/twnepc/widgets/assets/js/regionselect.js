$(document).on('ready', function(){

	// Custom Region select
    $('.header-top .select-region select.regionselect').each(function() {

    	if (!$(this).hasClass('select-hidden')) {

		    var $this = $(this), numberOfOptions = $(this).children('option').length;
		  
		    $this.addClass('select-hidden'); 
		    $this.wrap('<div class="select"></div>');
		    $this.after('<div class="select-styled"><strong></strong> <span>(Change)</span></div>');

		    var $styledSelect = $this.next('div.select-styled');
		    $styledSelect.find('strong').text($this.children('option:selected').eq(0).text());
		  
		    var $list = $('<ul />', {
		        'class': 'select-options'
		    }).insertAfter($styledSelect);
		  
		    for (var i = 0; i < numberOfOptions; i++) {
		        $('<li />', {
		            text: $this.children('option').eq(i).text(),
		            rel: $this.children('option').eq(i).val()
		        }).appendTo($list);
		    }
		  
		    var $listItems = $list.children('li');
		  
		    $styledSelect.click(function(e) {
		        e.stopPropagation();
		        $('div.select-styled.active').not(this).each(function(){
		            $(this).removeClass('active').next('ul.select-options').hide();
		        });
		        $(this).toggleClass('active').next('ul.select-options').toggle();
		    });
		  
		    $listItems.click(function(e) {
		        e.stopPropagation();
		        $styledSelect.find('strong').text($(this).text()).removeClass('active');
		        $this.val($(this).attr('rel'));
		        console.log($(this).attr('rel'));
		        $list.hide();

		        location.href= $(this).attr('rel');
		    });
		  
		    $(document).click(function() {
		        $styledSelect.removeClass('active');
		        $list.hide();
		    });
		}
	});
});