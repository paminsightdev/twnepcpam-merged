<?php return [
    'plugin' => [
        'name' => 'Widgets',
        'description' => 'Multiple widgets',
    ],
    'components' => [
        'filtered_content' => [
            'name' => 'Filtered Content',
            'description' => 'Display Selected Model filtered by the given attributes'
        ],
        'articles' => [
            'name' => 'Articles',
            'description' => 'Display articles on selected parameters'
        ],
        'rating' => [
            'name' => 'Rating',
            'description' => 'Rating (create) Widget'
        ],
        'region' => [
            'name' => 'Region',
            'description' => 'Region selector'
        ]
    ]
];