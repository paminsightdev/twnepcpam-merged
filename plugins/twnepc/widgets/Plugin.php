<?php

namespace Twnepc\Widgets;

use Twnepc\Widgets\Components\Rating;
use Twnepc\Widgets\Components\Articles;
use Twnepc\Widgets\Components\Trending;
use Twnepc\Widgets\Components\Regionselect;
use Twnepc\Widgets\Components\Subcategories;
use Twnepc\Widgets\Components\Advancedsearch;
use Twnepc\Widgets\Components\FilteredContent;

class Plugin extends \System\Classes\PluginBase
{
    
    public $require = ['Twnepc.Regions'];

    public function pluginDetails()
    {
        return [
            'name' => 'twnepc.widgets::lang.plugin.name',
            'description' => 'twnepc.widgets::lang.plugin.description',
            'author' => 'Twnepc',
            'icon' => 'icon-leaf'
        ];
    }

    public function registerComponents()
    {
        return [
            FilteredContent::class => 'filteredcontent',
            Articles::class => 'articles',
            Rating::class => 'ratings',
            Regionselect::class => 'regionselect',
            Trending::class => 'trending',
            Subcategories::class => 'subcategories',
            Advancedsearch::class => 'advancedsearch',
        ];
    }


}