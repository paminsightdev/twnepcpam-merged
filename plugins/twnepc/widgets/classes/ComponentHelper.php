<?php namespace Twnepc\Widgets\Classes;

use Cache;
use Input;
use Exception;
use October\Rain\Support\Traits\Singleton;
use ApplicationException;
use Keios\Multisite\Models\Setting;
use Twnepc\Regions\Models\Region;
use Twnepc\News\Models\Category;
use File;

/**
 * Provides helper methods for Builder CMS components.
 *
 * @package Twnepc\widgets
 * @author Balint Gaspar, Bence Dragsits
 */
class ComponentHelper
{
    use Singleton;

    protected $modelListCache = null;

    public function listGlobalModels()
    {
        if ($this->modelListCache !== null) {
            return $this->modelListCache;
        }

        $key = 'builder-global-model-list';
        $cached = Cache::get($key, false);

        if ($cached !== false && ($cached = @unserialize($cached)) !== false) {
            return $this->modelListCache = $cached;
        }

        $plugins = \RainLab\Builder\Classes\PluginBaseModel::listAllPluginCodes();

        $result = [];
        foreach ($plugins as $pluginCode) {
            try {
                $pluginCodeObj = new \RainLab\Builder\Classes\PluginCode($pluginCode);

                $models = \RainLab\Builder\Classes\ModelModel::listPluginModels($pluginCodeObj);

                $pluginCodeStr = $pluginCodeObj->toCode();
                $pluginModelsNamespace = $pluginCodeObj->toPluginNamespace().'\\Models\\';
                foreach ($models as $model) {
                    $fullClassName = $pluginModelsNamespace.$model->className;

                    $result[$fullClassName] = $pluginCodeStr.' - '.$model->className;
                }
            }
            catch (Exception $ex) {
                // Ignore invalid plugins and models
            }
        }

        Cache::put($key, serialize($result), 1);

        return $this->modelListCache = $result;
    }

    public function getModelClassDesignTime()
    {
        $modelClass = trim(Input::get('modelClass'));
        
        if ($modelClass && !is_scalar($modelClass)) {
            throw new ApplicationException('Model class name should be a string.');
        }

        if (!strlen($modelClass)) {
            $models = $this->listGlobalModels();
            $modelClass = key($models);
        }

        if (!\RainLab\Builder\Classes\ModelModel::validateModelClassName($modelClass)) {
            throw new ApplicationException('Invalid model class name.');
        }

        return $modelClass;
    }

    public function listModelColumnNames()
    {
        $modelClass = $this->getModelClassDesignTime();

        $key = md5('builder-global-model-list-'.$modelClass);
        $cached = Cache::get($key, false);

        if ($cached !== false && ($cached = @unserialize($cached)) !== false) {
            return $cached;
        }

        $pluginCodeObj = \RainLab\Builder\Classes\PluginCode::createFromNamespace($modelClass);

        $modelClassParts = explode('\\', $modelClass);
        $modelClass = array_pop($modelClassParts);

        $columnNames = \RainLab\Builder\Classes\ModelModel::getModelFields($pluginCodeObj, $modelClass);
        
        $result = [];
        foreach ($columnNames as $columnName) {
            $result[$columnName] = $columnName;
        }

        Cache::put($key, serialize($result), 1);

        return $result;
    }

    public function listSites()
    {
        $sites = Setting::all();
        $result = [];

        $result['noselect'] = 'No Filter';

        foreach ($sites as $site) {
            $result[$site->id] = $site->theme;
        }

        return $result;
    }

    public function listColors()
    {
        return [
            'base' => 'Base',
            'orange' => 'Orange',
            'red' => 'Red',
            'turquoise' => 'Turquoise',
            'purple' => 'Purple',
            'yellow' => 'Yellow',
            'gray' => 'Gray',
        ];
    }

    public function listTemplates($theme,$component = null) {
       
        $site = Input::get('site');
        $files = [];
        $theme =  Setting::find($site);
        if($theme) {
            if(!$component) {
                $directory = base_path().'/themes/'.$theme->theme.'/partials/filteredcontent/';
                $files = File::allFiles($directory);
            }
            else 
            {
                $directory = plugins_path().'/twnepc/widgets/components/'.$component.'/';
                $files = File::allFiles($directory);
            }
        }
        $result = [];
        $result['noselect'] = 'No Template';
        foreach($files as $file) {
           $result[$file->getFilename()] = $file->getFilename();
        }

        return $result;
    }

    public function listRegions() {
        $regions = Region::all();
        $result = [];
        $result['noselect'] = 'No Region filter';
        foreach($regions as $region) {
            $result[$region->id] = $region->name;
        }
        return $result;
    }

    public function listCategories($theme) {

        $site = Input::get('site');

        $categories = [];
        $theme = Setting::find($site);
        if($theme) {
            $categories = Category::where('site_id',$theme['id'])->where('parent_id',NULL)->get();
            
        }
        $result = [];
        $result['noselect'] = 'No category filter';
        foreach($categories as $category) {
            $result[$category->id] = $category->name;
        }

        return $result;
    }
}