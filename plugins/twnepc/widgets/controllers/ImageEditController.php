<?php namespace Twnepc\Widgets\Controllers;

use Request;
use BackendMenu;
use GuzzleHttp\Psr7\Response;
use Twnepc\News\Models\Article;
use Illuminate\Routing\Controller;
use Keios\Multisite\Models\Setting;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class ImageEditController extends Controller
{

    public function store() {

        $orig = Input::get('slim');
        $f = json_decode($orig[0]);
       
        $uploadedFile = Input::file($f->output->field);

        $file = \System\Models\File::where('disk_name',$uploadedFile->getClientOriginalName())->first();
        $file->data = Input::file($f->output->field);
        $file->is_public = true;
        $file->save();        



    } 
}