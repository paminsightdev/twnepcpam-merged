<?php namespace Twnepc\Widgets\Components;

use Lang;
use Response;
use Exception;
use SystemException;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Carbon;
use Twnepc\News\Models\Article;
use Twnepc\Author\Models\Author;
use Twnepc\Widgets\Classes\ComponentHelper;
use DB;

class Advancedsearch extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.widgets::lang.components.advancedsearch.name',
            'description' => 'twnepc.widgets::lang.components.advancedsearch.description'
        ];
    }

    public function defineProperties() {
        return [
            
            'site' => [
                'title'             => 'Site',
                'type'              => 'dropdown',
                'showExternalParam' => false
            ]
        ];
    }

    public function onRun() {
        $authors = $this->page['authors'] = Author::where('status',1)->orderBy('author_name','ASC')->get();
    }

    public function getSiteOptions()
    {
        return ComponentHelper::instance()->listSites();
    }

    public function onAdvancedSearch() {
        $data = post();
        $search = array_get($data,'search');
        $result = null;
        $max = array_get($data,'maxresults');
        $date = array_get($data,'date');
        $author = array_get($data,'author');
        $condition = array_get($data,'condition');
        $sort = array_get($data,'sort');
        $from = array_get($data,'from-date');
        $to = array_get($data,'to-date');
        $from = date('Y-m-d',strtotime($from));
        $to = date('Y-m-d',strtotime($to));
        $result = Article::where('site_id',$this->property('site'));


        if($date) {
            switch($date) {
                case 'all':
                    break;
                case 'lastdays':
                    $now = Carbon::now();
                    $last = $now->subDays(30);
                    $result = $result->where('published_date','>=',$last);
                    break;
                case 'lastyear':
                    $now = Carbon::now();
                    $last = $now->subYear();
                    $result = $result->where('published_date','>=',$last);
                    break;
                case 'between': 
                    $result = $result->whereBetween('published_date',[$from,$to]);
                    break;

            }
        }

        

        if($search && $condition) {
            if($condition == 'title') {
                $result = $result->where('title','like','%'.$search.'%');
            } 
            else if ($condition == 'all-words') {
                $words = explode(' ',$search);

                $result = $result->where(function($query) use ($words) {

                    foreach($words as $word) {

                        $query->where(function($query2) use ($word){
                            
                                $query2->where('title','like','%'.$word.'%');
                                $query2->orWhere('body','like','%'.$word.'%');
                        });
    
                    }

                });

                

            } else if($condition == 'exact-phrase') {
                $result = $result->where('title','like','%'.$search.'%')->orWhere('body','like','%'.$search.'%');
            } else if($condition == 'one-of-the-words') {
                $words = explode(' ',$search);

                $result = $result->where(function($query) use($words) {
                    $query->where(function($query2) use ($words){
                    
                        foreach($words as $word) {
                            $query2->orWhere('title','like','%'.$word.'%');
                        }
                        
                    })->orWhere(function($query3) use ($words) {
                        foreach($words as $word) {
                            $query3->orWhere('body','like','%'.$word.'%');
                        }
                    });
                });
                 
            }
        }

        if($author) {
            $result = $result->where('author_id',$author);
        }

        if($sort) {
            if($sort == "published_date") {
                $result = $result->orderBy($sort,'DESC');
            } else {
                $result = $result->orderBy($sort,'ASC');
            }
        }

        $query = str_replace(array('?'), array('\'%s\''), $result->toSql());
        $result = $result->limit($max)->get();
        $this->page['result'] = $result;
        return [
            '#results' => $this->renderPartial('advancedsearch::result')
        ];
    }
}
