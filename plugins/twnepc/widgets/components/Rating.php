<?php namespace Twnepc\Widgets\Components;

use Lang;
use Cms\Classes\ComponentBase;
use Twnepc\Widgets\Classes\ComponentHelper;
use SystemException;
use Exception;
use Response;
use Twnepc\News\Models\Rating as RatingModel;

class Rating extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.widgets::lang.components.rating.name',
            'description' => 'twnepc.widgets::lang.components.rating.description'
        ];
    }

    public function onRun(){
        //$this->addCss('/vendor/kartik-v/bootstrap-star-rating/css/star-rating.css');
        //$this->addCss('/vendor/kartik-v/bootstrap-star-rating/js/star-rating.js');
        //$this->addCss('/vendor/kartik-v/bootstrap-star-rating/themes/krajee-svg/theme.css');
        //$this->addJs('/vendor/kartik-v/bootstrap-star-rating/themes/krajee-svg/theme.js');
        //$this->addJs('/vendor/kartik-v/bootstrap-star-rating/themes/krajee-svg/theme.js');

    }

    public function onSaveRating() {
        
    }
}