<?php namespace Twnepc\Widgets\Components;

use Keios\Multisite\Classes\SettingHelper;
use Lang;
use Response;
use Exception;
use SystemException;
use Cms\Classes\ComponentBase;
use Twnepc\News\Models\Category;
use Twnepc\Widgets\Classes\ComponentHelper;

class Subcategories extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.widgets::lang.components.subcategories.name',
            'description' => 'twnepc.widgets::lang.components.subcategories.description'
        ];
    }

    public function defineProperties(){
        
        return [
            'slug' => [
                'title'       => 'slug',
                'description' => 'Category slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string',
            ]
        ];
    }

    public function onRun() {
        $this->addCss('assets/css/subcategories.css');
        $site = SettingHelper::getCurrentSite();
        $main = Category::where('identifier',$this->property('slug'))
            ->where('site_id',$site->id)
            ->first();

        if($main){
            $this->subcategories = $this->page['subcategories'] = Category::where('parent_id',$main->id)
                ->where('site_id',$site->id)
                ->where('is_hidden',0)
                ->get();
        }
    }
}