<?php namespace Twnepc\Widgets\Components;

use Lang;
use Cms\Classes\ComponentBase;
use Twnepc\Widgets\Classes\ComponentHelper;
use SystemException;
use Exception;
use Response;

class Trending extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.widgets::lang.components.trending.name',
            'description' => 'twnepc.widgets::lang.components.trending.description'
        ];
    }

    public function onRun() {

    }
}