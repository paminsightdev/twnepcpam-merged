<?php namespace Twnepc\Widgets\Components;

use Lang;
use Cms\Classes\ComponentBase;
use Twnepc\Widgets\Classes\ComponentHelper;
use SystemException;
use Exception;
use Response;

class FilteredContent extends ComponentBase
{

    /**
     * A collection of records to display
     * @var \October\Rain\Database\Collection
     */
    public $records;

    /**
     * A String for the displayed field
     *
     */
    public $title;


    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.widgets::lang.components.filtered_content.name',
            'description' => 'twnepc.widgets::lang.components.filtered_content.description'
        ];
    }

    public function defineProperties()
    {
        return [

            'modelClass' => [
                'title'             => 'rainlab.builder::lang.components.list_model',
                'type'              => 'dropdown',
                'showExternalParam' => false
            ],

            'listTitle' => [
                'title'             => 'Display Title',
                'type'              => 'text',
                'showExternalParam' => false
            ],
            'site' => [
                'title'             => 'Site',
                'type'              => 'dropdown',
                'showExternalParam' => false
            ],
            'region' => [
                'title'             => 'Region',
                'type'              => 'dropdown',
                'showExternalParam' => true
            ],
            'topic' => [
                'title'             => 'Topic/Category',
                'type'              => 'dropdown',
                'depends'           => ['site'],
                'placeholder'       => 'select db-colum',
                'showExternalParam' => false
            ],
            'template' => [
                'title'             => 'Template',
                'type'              => 'dropdown',
                'depends'           => ['site'],
                'placeholder'       => 'select db-colum',
                'showExternalParam' => false
            ]

        ];
    }

    public function onRun()
    {
        $this->prepareVars();
        $this->records = $this->page['records'] = $this->listRecords();
        //Get selected template
        if($this->property('template') != 'noselect' && $this->property('template') !== null) {
            $content = $this->renderPartial('filteredcontent/'.$this->property('template'));
            //Output the template to Html
            return Response::make($content)->header('Content-Type', 'text/html');
        }
        
    }

    public function prepareVars()
    {
        $this->title = $this->page['title'] = $this->property('listTitle');
    }   


    protected function listRecords()
    {
        $modelClassName = $this->property('modelClass');
        if (!strlen($modelClassName) || !class_exists($modelClassName)) {
            throw new SystemException('Invalid model class name');
        }

        $model = new $modelClassName();
        $scope = $this->getScopeName($model);
        $scopeValue = $this->property('scopeValue');

        $site = $this->property('site');
        $template = $this->property('template');

        

        if ($scope !== null) {
            $model = $model->$scope($scopeValue);
        }

        if($site != 'noselect') {
            $model = $model->where('site_id',$site);
        }

        return $model->get();
    }

    protected function getScopeName($model)
    {
        $scopeMethod = trim($this->property('scope'));
        if (!strlen($scopeMethod) || $scopeMethod == '-') {
            return null;
        }

        if (!preg_match('/scope[A-Z].+/', $scopeMethod)) {
            throw new SystemException('Invalid scope method name.');
        }

        if (!method_exists($model, $scopeMethod)) {
            throw new SystemException('Scope method not found.');
        }

        return lcfirst(substr($scopeMethod, 5));
    }



    public function getModelClassOptions()
    {
        return ComponentHelper::instance()->listGlobalModels();
    }
    public function getSiteOptions()
    {
        return ComponentHelper::instance()->listSites();
    }

    public function getRegionOptions()
    {
        return ComponentHelper::instance()->listRegions();
    }

    public function getTopicOptions()
    {
        $theme = null;

        if($this->property('site') != null)
        {
            $theme = $this->property('site');
        }

        return ComponentHelper::instance()->listCategories($theme);
    }

    public function getTemplateOptions()
    {
        $theme = null;

        if($this->property('site') != null)
        {
            $theme = $this->property('site');
        }

        return ComponentHelper::instance()->listTemplates($theme);
    }
}