<?php namespace Twnepc\Widgets\Components;

use Lang;
use Response;
use Exception;
use SystemException;
use Cms\Classes\ComponentBase;
use Twnepc\News\Models\Article;
use Twnepc\News\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Twnepc\Widgets\Classes\ComponentHelper;

class Articles extends ComponentBase
{
    /**
     * A collection of records to display
     * @var \October\Rain\Database\Collection
     */
    public $records;

    /**
     * A String for the displayed field
     *
     */
    public $title;

    public $template;

    public $topic;



    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.widgets::lang.components.articles.name',
            'description' => 'twnepc.widgets::lang.components.articles.description'
        ];
    }

    public function defineProperties() {
        return [
            'title' => [
                'title'             => 'Display Title',
                'type'              => 'text',
                'showExternalParam' => false
            ],
            'site' => [
                'title'             => 'Site',
                'type'              => 'dropdown',
                'showExternalParam' => false
            ],
            'topic' => [
                'title'             => 'Topic/Category',
                'type'              => 'dropdown',
                'depends'           => ['site'],
                'placeholder'       => 'select db-colum',
                'showExternalParam' => false
            ],
            'exclusive' => [
                'title'             => 'Exclusive',
                'type'              => 'checkbox',
                'showExternalParam' => false
            ],
            'main_news' => [
                'title'             => 'Main News',
                'type'              => 'checkbox',
                'showExternalParam' => false
            ],
            'template' => [
                'title'             => 'Template',
                'type'              => 'dropdown',
                'depends'           => ['site'],
                'placeholder'       => 'select db-colum',
                'showExternalParam' => false
            ],
            'color' => [
                'title'             => 'Color',
                'type'              => 'dropdown',
                'placeholder'       => 'select color',
                'showExternalParam' => false
            ],
            'show_image' => [
                'title'             => 'Show Image',
                'type'              => 'checkbox',
                'showExternalParam' => false
            ]
        ];
    }

    public function onRun()
    {
        $this->prepareVars();
    
        $this->records = $this->page['records'] = $this->listRecords();
        $this->title = $this->page['title'] = $this->getTitle();
        $this->color = $this->page['color'] = $this->getColor();
        $this->template = $this->page['template'] = $this->property('template');

        if($this->property('topic'))
        {
            if(Category::find($this->property('topic'))) {
                $this->topic = $this->page['topic'] = Category::find($this->property('topic'));
            } else {
                $this->topic = $this->page['topic'] = '';
            }
            
        }
        else {
            $this->topic = $this->page['topic'] = ''; 
        }
    }

    public function getTitle() {
        return $this->property('title');
    }

    public function getColor() {
        return $this->property('color');
    }

    public function getShowImage() {
        return $this->property('show_image');
    }

    public function onRender() {
    }

    public function prepareVars() {
    }   

    protected function listRecords() {
        $model = new Article;
        $site = $this->property('site');
        $template = $this->property('template');
        $is_exclusive = $this->property('exclusive');
        $headline = $this->property('main_news');
        $category = $this->property('topic');
        $region = Session::get('region','all');
        $today = date('Y-m-d');

        $model = $model->published();

        if($site != 'noselect') {
            $model->where('site_id',$site);
        }

        if($is_exclusive) {
            $model = $model->where('newsfeed',1)
                //->where('published_date',$today)
                ->orderBy('published_date','DESC')
                ->orderBy('order','ASC')
                ->limit(1);
        }

        if($headline) {
            $model = $model->where('newsfeed',1)
                //->where('published_date',$today)
                ->orderBy('published_date','DESC')
                ->orderBy('order','ASC')
                ->limit(5);
        }

        if($region != 'all'){
            $model = $model->whereHas('region',function($query) use ($region) {
                $query->where('region_id',$region);
            });
        }

        if($category && $category != 'noselect') {
            $category = Category::find($category);
            if($category){
                $model = $category->articles()->with('region');

                if($region != 'all'){
                    $model = $model->whereHas('region',function($q) use($region){
                        $q->where('region_id',$region);
                    });
                }

                $model = $model->where('status_id',3)->where('site_id',$site)->limit(4)->groupBy('id')->orderBy('published_date','DESC')->orderBy('order','ASC');
            }
        }

        $model = $model->where('status_id',3)->get();
        return $model;
    }



    public function getModelClassOptions()
    {
        return ComponentHelper::instance()->listGlobalModels();
    }
    public function getSiteOptions()
    {
        return ComponentHelper::instance()->listSites();
    }

    public function getRegionOptions()
    {
        return ComponentHelper::instance()->listRegions();
    }

    public function getTopicOptions()
    {
        $theme = null;

        if($this->property('site') != null)
        {
            $theme = $this->property('site');
        }

        return ComponentHelper::instance()->listCategories($theme);
    }

    public function getTemplateOptions()
    {
        $theme = null;

        if($this->property('site') != null)
        {
            $theme = $this->property('site');
        }

        return ComponentHelper::instance()->listTemplates($theme,'articles');
    }

    protected function getScopeName($model)
    {
        $scopeMethod = trim($this->property('scope'));
        if (!strlen($scopeMethod) || $scopeMethod == '-') {
            return null;
        }

        if (!preg_match('/scope[A-Z].+/', $scopeMethod)) {
            throw new SystemException('Invalid scope method name.');
        }

        if (!method_exists($model, $scopeMethod)) {
            throw new SystemException('Scope method not found.');
        }

        return lcfirst(substr($scopeMethod, 5));
    }

    public function getColorOptions()
    {
        return ComponentHelper::instance()->listColors();
    }
}
