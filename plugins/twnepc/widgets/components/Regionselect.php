<?php namespace Twnepc\Widgets\Components;

use DB;
use App;
use Keios\Multisite\Classes\SettingHelper;
use Lang;
use Response;
use Exception;
use SystemException;
use Cms\Classes\ComponentBase;
use Twnepc\Regions\Models\Region;
use Keios\Multisite\Models\Setting;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Twnepc\Widgets\Classes\ComponentHelper;

class Regionselect extends ComponentBase
{
    public $regions;

    public $domain;
    public $selected;
    public function componentDetails()
    {
        return [
            'name'        => 'twnepc.widgets::lang.components.region.name',
            'description' => 'twnepc.widgets::lang.components.region.description'
        ];
    }

    public function onRun(){
        $this->addJs('assets/js/regionselect.js');

        $site = SettingHelper::getCurrentSite();
        if(Request::get('region')) {
            $region = DB::table('region_setting')
                ->select('regions.*')
                ->where('setting_id',$site->id)
                ->where('regions.identifier',Request::get('region'))
                ->leftJoin('twnepc_regions_region as regions','regions.id','=','region_setting.region_id')
                ->first();

            if($region){
                Session::forget('region');
                Session::put('region',$region->id);
            }else if(Request::get('region') == 'all'){
                Session::forget('region');
            }
        }

        $this->selected = $this->page['selected'] = Session::get('region','all');
        $this->domain = $this->page['domain'] = $site->domain;

        $this->regions = $this->page['regions'] = DB::table('region_setting')->select('*')
            ->leftJoin('twnepc_regions_region as region','region.id','=','region_setting.region_id')
            ->where('setting_id',$site->id)
            ->get();
        
    }
}