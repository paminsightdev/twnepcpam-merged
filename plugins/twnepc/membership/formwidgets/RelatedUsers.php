<?php namespace Twnepc\Membership\FormWidgets;

use Config;
use Rainlab\User\Models\User;
use Illuminate\Support\Facades\DB;
use Backend\Classes\FormWidgetBase;



class RelatedUsers extends FormWidgetBase {

    public function widgetDetails() {
        
        return [
            'name' => 'Membership Users',
            'description' => 'Listing Membership users'
        ];


    }

    public function prepareVars(){
        $this->vars['id'] = $this->model->id;
        $this->vars['users'] = $this->loadUsers();
    }

    public function render() {

        $this->prepareVars();



        return $this->makePartial('widget');
    }

    public function loadUsers(){
        $users = [];
        if($this->model->user_id) {
            $users = User::where('id',$this->model->user_id)->get();
        }

        if($this->model->branch_id) {
            $users = User::where('branch_id',$this->model->branch_id)->get();
        }

        if($this->model->company_id && $this->model->branch_id == null) {
            $users = User::where('company_id',$this->model->company_id)->get();
        }

        return $users;
    }
}