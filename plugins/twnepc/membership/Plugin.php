<?php namespace Twnepc\Membership;

use Event;
use Backend;
use Carbon\Carbon;
use System\Classes\PluginBase;
use Illuminate\Support\Facades\DB;
use System\Models\EventLog;
use Twnepc\Membership\Classes\BannedEmail;
use October\Rain\Support\Facades\Flash;
use Twnepc\Membership\Models\Membership;
use Keios\Multisite\Classes\SettingHelper;
use Twnepc\Membership\Classes\MembershipHelper;

/**
 * Membership Plugin Information File
 */
class Plugin extends PluginBase
{

    public $require = ['Twnepc.Regions'];
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'twnepc.membership::lang.plugin.name',
            'description' => 'twnepc.membership::lang.plugin.description',
            'author'      => 'Twnepc',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        Event::listen('rainlab.user.register', function($user, $data) {

            $bannedEmail = BannedEmail::validateBannedEmail($user->email);

            if(!$bannedEmail) {

                $site = SettingHelper::getCurrentSite();
                if(!MembershipHelper::hasSubscription($user, $site)){
                    $membership = MembershipHelper::createFreeTrial($user, $site);
                    if($membership){
                        Flash::success('You have received an Automatic Free Trial due to your registration!');
                    }
                }
            }
        });

        Event::listen('twnepc.user.preference', function($user, $site, $data) {
            $bannedEmail = BannedEmail::validateBannedEmail($user->email);
            if(!$bannedEmail) {
                if(!MembershipHelper::hasSubscription($user, $site)){
                    $membership = MembershipHelper::createFreeTrial($user, $site);
                    if($membership){
                        Flash::success('You have received an Automatic Free Trial due to your registration!');
                    }
                }
            }

        });

        Event::listen('rainlab.user.login', function ($user) {
            EventLog::add('Successful login', 'info', $user->email);
        });

        Event::listen('rainlab.user.logout', function ($user) {
            EventLog::add('Successful logout', 'info', $user->email);
        });

        Event::listen('backend.menu.extendItems', function ($manager) {

            $manager->addSideMenuItems('Twnepc.Membership', 'memberships', [
                'subscriptions' => [
                    'label' => 'Subscriptions',
                    'icon' => 'icon-leaf',
                    'code' => 'subscriptions',
                    'owner' => 'Twnepc.Membership',
                    'url' => Backend::url('twnepc/membership/subscriptions')
                ],
                'memberships' => [
                    'label' => 'Memberships',
                    'icon' => 'icon-life-ring',
                    'code' => 'subscriptions',
                    'owner' => 'Twnepc.Membership',
                    'url' => Backend::url('twnepc/membership/memberships')
                ]
            ]);  
        });

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Twnepc\Membership\Components\MyComponent' => 'myComponent',
        ];
    }

    public function registerFormWidgets() {
        return [
            'Twnepc\Membership\FormWidgets\RelatedUsers' => [
                'label' => 'Related Users',
                'code' => 'relatedusers'
            ]
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    /*public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'twnepc.membership.some_permission' => [
                'tab' => 'Membership',
                'label' => 'Some permission'
            ],
        ];
    }*/

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    /*public function registerNavigation()
    {

        return [
            'membership' => [
                'label'       => 'Membership',
                'url'         => Backend::url('twnepc/membership/subscriptions'),
                'icon'        => 'icon-leaf',
                'permissions' => ['twnepc.membership.*'],
                'order'       => 500,
            ],
        ];
    }*/
}
