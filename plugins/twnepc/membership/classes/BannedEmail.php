<?php namespace Twnepc\Membership\Classes;

use Twnepc\User\Models\TwnepcUserSettings as Settings;
use Illuminate\Support\Facades\DB;
use October\Rain\Support\Traits\Singleton;

class BannedEmail
{
    use Singleton;

    static function validateBannedEmail($email)
    {
        
        $bannedEmailDomains =  Settings::get('banned_emails');

        $bannedEmailDomains = explode(',',$bannedEmailDomains);

        $emailDomain = explode('@',$email)[1];

        if(in_array($emailDomain,$bannedEmailDomains)) {
            return true;
        }

        return false;
    }
}