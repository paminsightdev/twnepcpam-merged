<?php namespace Twnepc\Membership\Classes;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use October\Rain\Support\Traits\Singleton;
use Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Twnepc\Membership\Models\Membership;

/**
 * Provides helper methods for Builder CMS components.
 *
 * @package Keios\Multisite
 * @author Balint Gaspar, Bence Dragsits
 */
class MembershipHelper
{
    use Singleton;

    static function createFreeTrial($user, $site){
        $subscription = $site->subscription;
        if($subscription && $user){
            $start = Carbon::now();
            $end = Carbon::now()->addDays($subscription->interval);
            return DB::table('twnepc_membership_memberships')->insert(
                [
                    'subscription_id' => $subscription->id,
                    'user_id' => $user->id,
                    'start_date' => $start,
                    'end_date' => $end,
                    'created_at' => $start,
                    'updated_at' => $start,
                    'status' => 'Active',
                    'comments' => 'Automatic Free Trial due to Registration.'
                ]
            );
        }
    }

    static function hasSubscription($user, $site, $active = false){
        // check branch subscription
        if($branch = $user->thebranch){
            $membershipCount = Membership::whereHas('subscription.site', function($query) use ($site){
                $query->where('setting_id', $site->id);
            })->where('branch_id', $branch->id)->count();

            if($membershipCount > 0){
                return true;
            }
        }

        // check company subscription
        if($company = $user->thecompany){
            $membershipCount = Membership::whereHas('subscription.site', function($query) use ($site){
                $query->where('setting_id', $site->id);
            })->where('company_id', $company->id)->count();

            if($membershipCount > 0){
                return true;
            }
        }

        // check user subscription
        $membershipCount = Membership::whereHas('subscription.site', function($query) use ($site){
            $query->where('setting_id', $site->id);
        })->where('user_id', $user->id)->count();

        if($membershipCount > 0){
            return true;
        }


        return false;
    }
}
