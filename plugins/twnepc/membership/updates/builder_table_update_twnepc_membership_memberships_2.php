<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcMembershipMemberships2 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_memberships', function($table)
        {
            $table->integer('company_id')->nullable()->default(-1);
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_membership_memberships', function($table)
        {
            $table->dropColumn('company_id');
        });
    }
}
