<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class AddSettingsSubscription extends Migration
{
    public function up()
    {
        Schema::create('setting_subscription', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('setting_id');
            $table->integer('subscription_id');

        });
    }
    
    public function down()
    {
        Schema::dropIfExists('setting_subscription');
    }
}
