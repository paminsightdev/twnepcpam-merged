<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcMembershipMemberships3 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_memberships', function($table)
        {
            $table->string('status');
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_membership_memberships', function($table)
        {
            $table->dropColumn('status');
        });
    }
}
