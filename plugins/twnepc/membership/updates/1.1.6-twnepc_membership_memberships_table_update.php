<?php  namespace Twnepc\Membership\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcMembershipMembershipsTableUpdate extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_memberships', function($table)
        {
            $table->renameColumn('department_id', 'branch_id');
            $table->decimal('price')->default(0);
            $table->mediumText('comments')->nullable();
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_membership_memberships')) {
            Schema::table('twnepc_membership_memberships', function ($table) {
                $table->dropColumn(['branch_id']);
                $table->dropColumn(['price']);
                $table->dropColumn(['comments']);
            });
        }
    }
}