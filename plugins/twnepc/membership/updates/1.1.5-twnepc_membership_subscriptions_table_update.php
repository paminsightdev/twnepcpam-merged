<?php  namespace Twnepc\Membership\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcMembershipSubscriptionsTableUpdate extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_subscriptions', function($table)
        {
            $table->dropColumn(['site_id']);
            $table->dropColumn(['price']);
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_membership_subscriptions')) {
            Schema::table('twnepc_membership_subscriptions', function ($table) {
                $table->integer('site_id')->default(-1);
                $table->decimal('price')->default(0);
            });
        }
    }
}