<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcMembershipSubscriptions2 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_subscriptions', function($table)
        {
            $table->integer('interval')->default(0);
            $table->integer('site_id')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_membership_subscriptions', function($table)
        {
            $table->dropColumn('interval');
            $table->integer('site_id')->default(NULL)->change();
        });
    }
}
