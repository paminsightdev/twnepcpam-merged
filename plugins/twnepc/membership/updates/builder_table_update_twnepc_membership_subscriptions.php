<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcMembershipSubscriptions extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_subscriptions', function($table)
        {
            $table->decimal('price', 10, 0)->nullable()->default(0.00);
            $table->integer('site_id')->default(0)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_membership_subscriptions', function($table)
        {
            $table->dropColumn('price');
            $table->integer('site_id')->default(NULL)->change();
        });
    }
}
