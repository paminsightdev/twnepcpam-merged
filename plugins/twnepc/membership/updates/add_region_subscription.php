<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class AddRegionSubscription extends Migration
{
    public function up()
    {
        Schema::create('region_subscription', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->integer('region_id');
            $table->integer('subscription_id');

        });
    }
    
    public function down()
    {
        Schema::dropIfExists('region_subscription');
    }
}
