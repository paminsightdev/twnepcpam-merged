<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class AddCategorySubscription extends Migration
{
    public function up()
    {
        Schema::create('category_subscription', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->integer('category_id');
            $table->integer('subscription_id');

        });
    }
    
    public function down()
    {
        Schema::dropIfExists('category_subscription');
    }
}
