<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use October\Rain\Database\Schema\Blueprint;

class SiteidNullable extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_subscriptions', function($table)
        {
            $table->integer('site_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        
    }
}
