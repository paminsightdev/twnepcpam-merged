<?php  namespace Twnepc\Membership\Updates;
use Schema;
use October\Rain\Database\Updates\Migration;

class TwnepcMembershipEndDateUpdate extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_memberships', function($table)
        {
            $table->renameColumn('recurring_date', 'end_date');
        });
    }

    public function down()
    {
        if (Schema::hasTable('twnepc_membership_memberships')) {
            Schema::table('twnepc_membership_memberships', function ($table) {
                $table->renameColumn('end_date', 'recurring_date');
            });
        }
    }
}