<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTwnepcMembershipMemberships extends Migration
{
    public function up()
    {
        Schema::create('twnepc_membership_memberships', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('subscription_id')->nullable()->default(-1);
            $table->integer('user_id')->nullable()->default(-1);
            $table->integer('department_id')->nullable()->default(-1);
            $table->dateTime('start_date');
            $table->dateTime('recurring_date');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('twnepc_membership_memberships');
    }
}
