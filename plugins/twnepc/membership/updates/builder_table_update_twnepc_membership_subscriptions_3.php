<?php namespace Twnepc\Membership\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTwnepcMembershipSubscriptions3 extends Migration
{
    public function up()
    {
        Schema::table('twnepc_membership_subscriptions', function($table)
        {
            $table->integer('limit')->nullable()->default(-1);
            $table->integer('site_id')->default(-1)->change();
        });
    }
    
    public function down()
    {
        Schema::table('twnepc_membership_subscriptions', function($table)
        {
            $table->dropColumn('limit');
            $table->integer('site_id')->default(NULL)->change();
        });
    }
}
