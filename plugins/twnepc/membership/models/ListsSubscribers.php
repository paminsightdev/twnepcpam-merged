<?php
namespace Twnepc\Membership\Models;

use Model;
use DB;

/**
 * Description of ListsSubscribers
 * this class is able to communicate with the responsiv_campaign_lists_subscribers table, this dynamic lookup table 
 * links subscribers to a list via 'subscriber_id' and 'list_id' 
 * @author potchere
 */
class ListsSubscribers extends Model {
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'responsiv_campaign_lists_subscribers';
    
    
    /**
     * retrieve all the listIds using a subscriber_id
     * @param integer $subId subscriber id
     * @return array|mixed $listIds ids relating to the websites/portals
     */
    public static function getListIdsViaSubscriberId($subId) {
        
        // create a single dimensional array to hold ids
        $listIdArray = [];
        // create a raw query to retrieve all list id's associated with the subscriber's id
        $listIds = Db::select('select * from responsiv_campaign_lists_subscribers where `subscriber_id` = :subId', ['subId' => $subId]);
        // loop throught the array of row objects and add list ids to array
        foreach($listIds as $row) {
            $listIdArray[] = $row->list_id;
        }
        
        return $listIdArray;
    }
    
    
    /**
     * insert a row in the lookup table
     * @param integer $subId subscriber id
     * @return boolean $status ids relating to the websites/portals
     */
    public static function deleteListIdsViaSubscriberId($subId) {
        
        $status = Db::delete('delete from responsiv_campaign_lists_subscribers where `subscriber_id` = :subId', ['subId' => $subId]);
        
        return $status;
    }
    
    
    
    /**
     * insert a row in the lookup table
     * @param integer $subId subscriber id
     * @param array|mixed $listIds associative array of listIds
     * @return boolean $status ids relating to the websites/portals
     */
    public static function setListIdsViaSubscriberId($subId, $listIds) {
        
        // status of the operation
        $status = 0;
        // loop through all ids that need to be saved and save them one-by-one on each iteration... 
        foreach($listIds as $listId) {
            // save the list id with the subscriber id, constraint: list id must be unique for each subscriber
           $status = Db::insert('insert into responsiv_campaign_lists_subscribers (list_id, subscriber_id) values (?, ?)', [$listId, $subId]);
        }
        
        return $status;
    }
    
    
}
