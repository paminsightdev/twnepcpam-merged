<?php
namespace Twnepc\Membership\Models;

use Model;
use DB;

class UserAcceptSettings extends Model {

    /**
     * @var string The database table used by the model.
     */
    public $table = "twnepc_user_accept_settings";
    
    
    /**
     * retrieve all the site ids for a user
     * @param integer $usrId id of the user
     * @return array $userAcceptArray sites relating to the a user that has accepted Ts&Cs
     */
    public static function getUserAcceptViaUserId($usrId) {
        
        // create a single dimensional array to hold sites
        $userAcceptArray = [];
        // create a raw query to retrieve all site id's associated with the user's id
        $siteIds = Db::select('select site_id from twnepc_user_accept_settings where `user_id` = :usrId', ['usrId' => $usrId]);
        // loop throught the array of row objects and add site ids to array
        foreach($siteIds as $row) {
            $userAcceptArray[] = $row->site_id;
        }
        
        return $userAcceptArray;
    }
    
    
    /**
     * delete multiple rows in the lookup table
     * @param integer $usrId id of the user
     * @return boolean $status status of the delete process
     */
    public static function deleteUserAcceptViaUsrId($usrId) {
        
        $status = Db::delete('delete from twnepc_user_accept_settings where `user_id` = :usrId', ['usrId' => $usrId]);
        
        return $status;
    }
    
    
    /**
     * insert multiple rows in the lookup table
     * @param integer $siteId which site has had its settings accepted
     * @param integer $isAccepted value indicating acceptance
     * @param integer $usrId id of the user
     * @return boolean $status ids relating to the websites/portals
     */
    public static function setUserAccept($siteId, $isAccepted, $usrId) {
        
        // status of the operation
        $status = 0;
        $timestamp = date("Y-m-d H:i:s", strtotime('now'));
        
        // save the site id with the other data associated with the user, constraints on table so be careful, lastly not using AR extended model so use timestamps manually 
        $status = Db::insert('insert into twnepc_user_accept_settings (site_id, is_accepted, user_id, created_at, updated_at) values (?, ?, ?, ?, ?)', 
                [$siteId, $isAccepted, $usrId, $timestamp, $timestamp]);
        
        return $status;
    }
    
    
}
