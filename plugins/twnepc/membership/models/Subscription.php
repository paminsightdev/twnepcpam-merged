<?php namespace Twnepc\Membership\Models;

use Model;
use Twnepc\Regions\Models\Region;
use Twnepc\News\Models\Category;
use DB;
/**
 * Model
 */
class Subscription extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    public $timestamps = true;
    /**
     * @var array Validation rules
     */
    public $rules = [
        'name' => 'required',
        'description' => 'required',
        'interval' => 'required',
    ];

    /**
     * @var array Jsonable columns
     */
    protected $jsonable = ['options'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_membership_subscriptions';

    /**
     * @var integer The relation of site
     */
    public $belongsToMany = [
        'site' => ['Keios\Multisite\Models\Setting'],
        'regions' => ['Twnepc\Regions\Models\Region'],
        'categories' => ['Twnepc\News\Models\Category']
    ];

    /**
     * 
     * Options
     */


    public function getRegionsOptions($value,$data) {
        $ids = [];
        if($data->site) {
            foreach($data->site as $d){
                array_push($ids,$d->id);
            }
        }

        $regionIds = DB::table('region_setting')->select('*')->whereIn('setting_id',$ids)->pluck('region_id');


        $return = Region::whereIn('id',$regionIds)->pluck('name','id');

        return $return;
    }

    public function getCategoriesOptions($value,$data) {
        $ids = [];
        foreach($data->site as $d) {
            array_push($ids,$d->id);
        }
        $return = Category::whereIn('site_id',$ids)->where('parent_id',NULL)->pluck('name','id');

        return $return;
    }

    public function scopeRegionsToArray() {
        $regions = $this->regions;
        $r = [];
        foreach($regions as $reg) {
            array_push($r,$reg->id);
        }

        return $r;
    }

    public function scopeSitesToArray() {
        $sites = $this->site;
        $s = [];
        foreach($sites as $site) {
            array_push($s,$site->id);
        }

        return $s;
    }
    public function scopeCategoriesToArray() {
        $categories = $this->categories;
        $c = [];
        foreach($categories as $cat) {
            array_push($c,$cat->id);
        }

        return $c;
    }

}
