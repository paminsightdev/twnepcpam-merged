<?php namespace Twnepc\Membership\Models;

use Model;
use Twnepc\Company\Models\Branch;
use Twnepc\Company\Models\Company;
use Input;
use Twnepc\Membership\Models\Subscription;
use Carbon\Carbon;
use Twnepc\Logs\Models\PamSplunkTroubleShooting;

/**
 * Model
 */
class Membership extends Model
{
    use \October\Rain\Database\Traits\Validation;
    

    /**
     * @var array Validation rules
     */
    public $rules = [
        'subscription' => 'required',
        'status' => 'required',
        'start_date' => 'required',
        'end_date' => 'required',
        'price' => 'required'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'twnepc_membership_memberships';

    public $belongsTo = [
        'subscription' => 'Twnepc\Membership\Models\Subscription',
        'user' => 'Rainlab\User\Models\User',
        'company' => 'Twnepc\Company\Models\Company',
        'branch' => 'Twnepc\Company\Models\Branch'
    ];

    public function getBranchOptions($value,$data)
    {   
        $company = $data->company['id'];
        if($company) {
            $branches = Branch::where('company_id',$company)->lists('name','id');
        }
        else {
            $c = Company::all();
            $c = $c->first();
            $branches = Branch::where('company_id',$c ? $c->id : 0)->lists('name','id');
        }
        return $branches;
    }


    public function filterFields($fields, $context = null)
    {
        if (empty($this->subscription))
            return;

        $membership = Membership::find($this->id);
        if($membership){
            if(!empty($fields->end_date) && $membership->subscription_id == $fields->subscription->value)
                return;
        }

        // do something to get the name value based on the code

        $subscription = $this->subscription;
        
        $interval = $subscription->interval;

        $newValue = Carbon::parse($fields->start_date->value)->addDays($interval);

        $fields->end_date->value = $newValue;

    }


    public function isMembershipActive() {
        $ret = false;
        $startDate = strtotime(date('Y-m-d',strtotime($this->start_date)));
        $endDate = strtotime(date('Y-m-d',strtotime($this->end_date)));
        $now = strtotime(date('Y-m-d',strtotime('now')));
        
        /* troubleshooting log - test on test server first */
        // (null !== $this->start_date && !empty($this->start_date)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/twnepc/membership/models/Membership.php', 'isMembershipActive', 'startDate', date('Y-m-d H:i:s', strtotime($this->start_date))) : '';
        // (null !== $this->$this->end_date && !empty($this->$this->end_date)) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/twnepc/membership/models/Membership.php', 'isMembershipActive', 'endDate', date('Y-m-d H:i:s', strtotime($this->end_date))) : '';
        /* troubleshooting log - test on test server first */
        // (null !== $this->subscription() && !empty($this->subscription())) ? PamSplunkTroubleShooting::myAccountFeatureslog('/plugins/twnepc/membership/models/Membership.php', 'isMembershipActive', 'subscription', $this->subscription()) : '';
        
        if($now >= $startDate && $now <= $endDate) {
                $ret = true;
        }

        return $ret;

        
    }
}
