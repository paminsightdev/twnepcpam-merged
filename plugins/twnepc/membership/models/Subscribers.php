<?php
namespace Twnepc\Membership\Models;

use Model;
use DB;


/**
 * Description of Subscribers
 * this class is able to communicate with the responsiv_campaign_subscribers table, this table 
 * details subscribers and refers to the user_id
 * @author potchere
 */
class Subscribers extends Model {
    
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'responsiv_campaign_subscribers';
    
    
    /**
     * retrieve all the listIds using a subscriber_id
     * @param integer $subId subscriber id
     * @return array|mixed $listIds ids relating to the websites/portals
     */
    public static function getSubscriberViaUserId($usrId) {
        
        // create a single dimensional array to hold ids
        $subIdArray = [];
        // create a raw query to retrieve all list id's associated with the subscriber's id
        $subIds = Db::select('select * from responsiv_campaign_subscribers where `user_id` = :usrId', ['usrId' => $usrId]);
        // loop throught the array of row objects and add list ids to array
        foreach($subIds as $row) {
            $subIdArray[] = $row;
        }
        
        return $subIdArray;
    }
    
    
    /**
     * delete a row in the lookup table
     * @param integer $usrId the id of the user
     * @return boolean $status ids relating to the websites/portals
     * @todo this will delete all subscribers from the table with user id, use unsubscribed_at with timestamp instead
     */
    public static function deleteSubscriberViaUsrId($usrId) {
        
        $status = Db::delete('delete from responsiv_campaign_subscribers where `user_id` = :usrId', ['usrId' => $usrId]);
        
        return $status;
    }
    
    
    
    /**
     * insert a row in the lookup table
     * @param array|mixed $usrMemSub associative array of user membership and subscription details
     * @return boolean $status id of the last saved record
     */
    public static function setSubscriber($usrMemSub) {
        
        // status of the operation
        $status = 0;
        $timestamp = date('Y-m-d H:i:s', strtotime('now'));
        $ipOrDefaultAddr = isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '172.20.0.6';
        
        // loop through all ids that need to be saved and save them one-by-one on each iteration... 
        foreach($listIds as $listId) {
           
            // create query to save a record in the subscribers table
            $status = Db::insert('insert into responsiv_campaign_subscribers (first_name, last_name, email, created_at, updated_at, created_ip_address, confirmed_ip_address, confirmed_at, message_type, user_id) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', 
                [$usrMemSub['first_name'], $usrMemSub['last_name'], $usrMemSub['email'], $timestamp, $timestamp, $ipOrDefaultAddr, $ipOrDefaultAddr, $timestamp, 'html', $usrMemSub['user_id']]);
        }
        
        if($status) {
            return Db::select('select last_insert_id()');
        } else {
            return false;
        }
    }
    
}
