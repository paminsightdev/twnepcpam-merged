<?php
namespace Twnepc\Membership\Models;

use Model;
use DB;


/**
 * Description of Lists
 *
 * @author potchere
 */
class CategoryUser extends Model {
    
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'category_user';
    
    
    /**
     * retrieve all the categorys of a user
     * @param integer $usrId id of the user
     * @return array $catIdArray ids relating to the preferences/interest
     */
    public static function getCategorysViaUserId($usrId) {
        
        // create a single dimensional array to hold ids
        $catIdArray = [];
        // create a raw query to retrieve all list id's associated with the subscriber's id
        $subIds = Db::select('select * from category_user where `user_id` = :usrId', ['usrId' => $usrId]);
        // loop throught the array of row objects and add list ids to array
        foreach($subIds as $row) {
            $catIdArray[] = $row->category_id;
        }
        
        return $catIdArray;
    }
    
    
    /**
     * retrieve all the categorys of a user
     * @param integer $usrId id of the user
     * @return array $catIdArray ids relating to the preferences/interest
     */
    public static function getSiteIdsViaUserId($usrId) {
        
        $siteIdArray = [];
        // create a raw query to retrieve all list id's associated with the subscriber's id
        $resultSet = Db::select('SELECT site_id FROM twnepc_news_categories WHERE id IN (SELECT category_id FROM category_user WHERE user_id = :usrId) group by site_id order by id asc', ['usrId' => $usrId]);
        // loop throught the array of row objects and add list ids to array
        foreach($resultSet as $row) {
            $siteIdArray[] = $row->site_id;
        }
        
        return $siteIdArray;
    }
    
    
    
    
    /**
     * delete multiple rows in the lookup table
     * @param integer $usrId id of the user
     * @return boolean $status status of the delete process
     */
    public static function deleteCategorysViaUsrId($usrId) {
        
        $status = Db::delete('delete from category_user where `user_id` = :usrId', ['usrId' => $usrId]);
        
        return $status;
    }
    
    
    
    /**
     * insert multiple rows in the lookup table
     * @param integer $usrId id of the user
     * @param array $categorys array of category ids
     * @return boolean $status ids relating to the websites/portals
     */
    public static function setCategorys($usrId, $categorys) {
        
        // status of the operation
        $status = 0;
        
        // loop through all ids that need to be saved and save them one-by-one on each iteration... 
        foreach($categorys as $catId) {
            
            // save the list id with the subscriber id, constraint: list id must be unique for each subscriber
            $status = Db::insert('insert into category_user (user_id, category_id) values (?, ?)', [$usrId, $catId]);
        }
        
        return $status;
    }
    
}
