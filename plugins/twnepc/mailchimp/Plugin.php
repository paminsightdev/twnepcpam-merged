<?php namespace Twnepc\MailChimp;

use System\Classes\PluginBase;
use Backend\Facades\BackendAuth;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'PAM Mailchimp',
            'description' => 'Provides MailChimp integration services.',
            'author'      => 'Pam Online',
            'icon'        => 'icon-hand-peace-o'
        ];
    }

    public function registerComponents()
    {
        return [
            'Twnepc\MailChimp\Components\Signup' => 'mailSignup'
        ];
    }

    public function registerSettings()
    {
        $backendUser = BackendAuth::getUser();
        if($backendUser->is_superuser == 1) {
            
            // display configuration setting for the mailchimp 
            // only super user can change API key for mail chimp
            return [
                'settings' => [
                    'label'       => 'Mailchimp',
                    'category'    => 'Pamonline',
                    'icon'        => 'icon-hand-peace-o',
                    'description' => 'Configure MailChimp API access.',
                    'class'       => 'Twnepc\MailChimp\Models\Settings',
                    'order'       => 600
                ]
            ];
            
        } else {
            
            // hide configuration setting for the mailchimp 
            // when other role based users look at the backend settings
            return [];
            
        }

    }
}
