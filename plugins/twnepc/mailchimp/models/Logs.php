<?php
namespace Twnepc\MailChimp\Models;

use Model;

class Logs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pamonline_mailchimp_logs';
    protected $jsonable = ['data'];
}