<?php namespace Twnepc\PamCompanies\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use DB;
use Backend\Facades\BackendAuth;
use Backend\Models\User as BackendUser;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use Flash;
use Hash;
use Auth;
use Event;

class Admins extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController','Backend\Behaviors\ReorderController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = ['pamonline.pamcompanies.access_edit_pams'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.PamCompanies', 'companies','admins');
    }

    public function onActivate($recordId)
    {
        $model = $this->formFindModelObject($recordId);
        $model->activated = post('Admins[activated]',0);

        switch((int)$model->activated){
            case 0:
                if($this->deactivateAdmin($model->user)){
                    $model->save();
                    Event::fire('pamonline.pamsadmin.deactivate', [$model->user]);
                }else{
                    $model->save();
                }

                break;

            case 1:
                if($this->activateAdmin($model->user)){
                    $model->save();
                    Event::fire('pamonline.pamsadmin.activate', [$model->user]);
                }
                break;
        }
        Flash::success("Update successfully");

    }

    function deactivateAdmin($user)
    {
        if(!$user->backenduser_id)return false;
        $backenduser = BackendAuth::findUserById($user->backenduser_id);
        if($backenduser){
            $backenduser->delete();
            $backenduser->afterDelete();
        }
        $user->backenduser_id = null;
        $user->save();
        Flash::success("PAMs admin deactivated.");
        return true;
    }

    function activateAdmin($user)
    {
        if($user->backenduser)return false;

        $existingBackendUser = BackendUser::where('email',$user->email)->first();
        if($existingBackendUser){
            $backenduser = BackendAuth::findUserById($existingBackendUser->id);
            $backenduser->delete();
            $backenduser->afterDelete();
        }

        $password = Hash::make($user->email);
        $backendUser = BackendAuth::register(array(
            'login' => $user->email,
            'email' => $user->email,
            'password' => $password,
            'password_confirmation' => $password
        ),true);

        $user->backenduser_id = $backendUser->id;
        $user->save();
        $pamsAdminAdministratorGroup = CompanySettings::get('pams_admin_administrator_group');
        Db::table('backend_users_groups')->insert(
            ['user_id' => $backendUser->id, 'user_group_id' => $pamsAdminAdministratorGroup]
        );
        Flash::success("PAMs admin activated.");
        return true;
    }
}