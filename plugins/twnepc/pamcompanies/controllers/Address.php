<?php
namespace Twnepc\PamCompanies\Controllers;

use Illuminate\Routing\Controller;
use ApplicationException;
use Rainlab\User\Models\User;
use Twnepc\PamCompanies\Classes\CaptureInteractiveRetrieve;
use Twnepc\PamCompanies\Classes\CaptureInteractiveFind;

class Address extends Controller
{

    public function __construct()
    {

    }

    public function find()
    {
        $params         = get();
        $key            = 'MK73-NJ94-UB21-KN65';
        $country        = isset($params['country'])?$params['country']:"";
        $countryAsIso   = User::IsoCodeOfCountry($country);
        $text           = $params['query'];
        $container      = isset($params['id'])?$params['id']:"";

        try{
            $api = new CaptureInteractiveFind($key,$text,$countryAsIso,$container);
            $api->MakeRequest();
        }catch(Exception $e){

        }


        $data = (array) $api->HasData();

        $results = [];
        $results['suggestions'] = [];
        $results['highlights'] = [];

        foreach ($data as $dat) {
            $id = (string) $dat['Id'];
            $text = (string) $dat['Text'];
            $descr = (string) $dat['Description'];
            $highlight = (string) $dat['Highlight'];
            $results['suggestions'][] = [
                'label' => $text.", ".$descr,
                'value' => $text,
                'id'    => $id
            ];
            if($highlight !== "")
                $results['highlights'][] = $highlight;
        }

        die(json_encode($results));
    }

    public function retrieve()
    {
        $params         = get();
        $key            = 'MK73-NJ94-UB21-KN65';
        $id      = isset($params['id'])?$params['id']:"";

        try{
            $api = new CaptureInteractiveRetrieve($key,$id);
            $api->MakeRequest();
        }catch(Exception $e){

        }

        $data = [];
        $data['details'] = [];
        $apiData= (array) $api->HasData();

        foreach ($apiData as $item)
        {
           $data['details']['id'] = (string) $item["Id"];
           $data['details']['DomesticId'] = (string) $item["DomesticId"];
           $data['details']['Language'] = (string) $item["Language"];
           $data['details']['LanguageAlternatives'] = (string) $item["LanguageAlternatives"];
           $data['details']['Department'] = (string) $item["Department"];
           $data['details']['Company'] = (string) $item["Company"];
           $data['details']['SubBuilding'] = (string) $item["SubBuilding"];
           $data['details']['BuildingNumber'] = (string) $item["BuildingNumber"];
           $data['details']['BuildingName'] = (string) $item["BuildingName"];
           $data['details']['SecondaryStreet'] = (string) $item["SecondaryStreet"];
           $data['details']['Street'] = (string) $item["Street"];
           $data['details']['Block'] = (string) $item["Block"];
           $data['details']['Neighbourhood'] = (string) $item["Neighbourhood"];
           $data['details']['District'] = (string) $item["District"];
           $data['details']['City'] = (string) $item["City"];
           $data['details']['Line1'] = (string) $item["Line1"];
           $data['details']['Line2'] = (string) $item["Line2"];
           $data['details']['Line3'] = (string) $item["Line3"];
           $data['details']['Line4'] = (string) $item["Line4"];
           $data['details']['Line5'] = (string) $item["Line5"];
           $data['details']['AdminAreaName'] = (string) $item["AdminAreaName"];
           $data['details']['AdminAreaCode'] = (string) $item["AdminAreaCode"];
           $data['details']['Province'] = (string) $item["Province"];
           $data['details']['ProvinceName'] = (string) $item["ProvinceName"];
           $data['details']['ProvinceCode'] = (string) $item["ProvinceCode"];
           $data['details']['PostalCode'] = (string) $item["PostalCode"];
           $data['details']['CountryName'] = (string) $item["CountryName"];
           $data['details']['CountryIso2'] = (string) $item["CountryIso2"];
           $data['details']['CountryIso3'] = (string) $item["CountryIso3"];
           $data['details']['CountryIsoNumber'] = (string) $item["CountryIsoNumber"];
           $data['details']['SortingNumber1'] = (string) $item["SortingNumber1"];
           $data['details']['SortingNumber2'] = (string) $item["SortingNumber2"];
           $data['details']['Barcode'] = (string) $item["Barcode"];
           $data['details']['POBoxNumber'] = (string) $item["POBoxNumber"];
           $data['details']['Label'] = (string) $item["Label"];
           $data['details']['Type'] = (string) $item["Type"];
           $data['details']['DataLevel'] = (string) $item["DataLevel"];
        }

        die(json_encode($data));
    }

}