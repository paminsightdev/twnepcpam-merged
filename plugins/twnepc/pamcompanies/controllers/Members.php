<?php namespace Twnepc\PamCompanies\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Auth;
use Backend;
use Flash;
use Backend\Facades\BackendAuth;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use DB;
use Hash;
use Event;
use Twnepc\User\Models\Role;
use Crypt;
use Mail;
class Members extends Controller
{
    use \Twnepc\PamCompanies\Classes\Notificator;
    use \Twnepc\PamCompanies\Classes\PinSystemControllerTrait;

    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.pamcompanies.access_edit_members'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.PamCompanies', 'members');
        $this->addjs("/plugins/twnepc/pamcompanies/assets/js/DisplaySize.js", "1.0.0");
    }

    public function formBeforeCreate($model)
    {
        $model->password = $model->temporary_password = $model->generateStrongPassword();
        $model->is_activated = 1;
        $model->company_id = $this->user->frontenduser->pamadmin->company->company_id;
        $model->company = $this->user->frontenduser->pamadmin->company->c_companyname;
    }

    public function formAfterCreate($model)
    {
        $post = post('User',array());
        $pamsGroup = CompanySettings::get('pams_frontend_groups');
        $role_id = $pamsGroup;

        $post['company_id'] = $this->user->frontenduser->pamadmin->company->company_id;
        $post['role'] = $role_id;

        $userRole = new Role();
        $userRole->user_id = $model->id;
        $userRole->role_id = $role_id;
        $userRole->save();

        Event::fire('twnepc.user.register', [$model,$post,true]);

        $vars = [
            'name' => $model->name,
            'email' => $model->email,
            'password' => $model->temporary_password
        ];
        Mail::send('twnepc.user::mail.pams_welcome', $vars, function($message) use ($model) {
            $message->to($model->email);
        });
    }

    public function onActivate($recordId)
    {
        $model = $this->formFindModelObject($recordId);
        $model->activated = post('Members[activated]',0);
        $model->save();

        switch((int)$model->activated){
            case 0:
                $this->deactivateMember($model->user);
                Flash::success("Update successful");
                break;

            case 1:
                $this->activateMember($model->user);
                Flash::success("Update successful");
                break;
        }
    }

    public function onLoadInvitationEmail()
    {
        $this->user->frontenduser->checkPIN();
        return $this->makePartial('invitation_email');
    }

    public function onSendInvitationEmail($recordId)
    {
        if(post('invitation_target', false) == false)
                return;

        $model = $this->formFindModelObject($recordId);

        $url = url('');
        $link_text = '';
        switch(intval(post('invitation_target'))){
            case 1:
                $url .= "/?redirect_after_login=".url('backend/twnepc/pamcompanies/company');
                $link_text = 'Get access to edit company details.';
                break;

            case 2:
                $url .= "/?redirect_after_login=".url('backend/twnepc/awards/pamscategories');
                $link_text = 'Get access to entry Awards.';
                break;

            case 3:
                $url .= "/?redirect_after_login=".url('backend/twnepc/awards/pamspersonnels');
                $link_text = 'Get access to fill company Extended manager profile.';
                break;
        }
        $vars = [
            'name'  => $model->name,
            'access_url' => $url,
            'link_text' => $link_text,
            'company_name' => $model->member->company->c_companyname
        ];
        Mail::sendTo($model->email,'twnepc.user::mail.pams_activated', $vars);
        Flash::success("Email sent");
    }

    public function onLoadContactUs()
    {
        return $this->makePartial('contactus_modal');
    }


    public function listExtendQuery($query)
    {
        $query->where('company_id',$this->user->frontenduser->pamadmin->company->company_id);
    }

}