<?php namespace Twnepc\PamCompanies\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Event;
use Request;
use Twnepc\PamCompanies\Models\CompanyText as Text;
use Twnepc\PamCompanies\Models\CompanyTextType as TextTypes;
use Twnepc\PamCompanies\Models\Companies as CompaniesModel;
use Twnepc\PamCompanies\Models\CompanySignoff as CompanySignoff;
use BackendAuth;
use Carbon\Carbon;
use DB;
use Flash;
use Backend;
use Illuminate\Support\Facades\Log;

class Companies extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController',
        'Backend.Behaviors.RelationController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';
    public $relationConfig  = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.PamCompanies', 'companies','companies');
        $this->addCss("/plugins/twnepc/pamcompanies/assets/css/custom.css", "1.0.1");
        $this->addjs("/plugins/twnepc/pamcompanies/assets/js/custom.js", "1.0.1");
    }

    public function update($recordId = null, $context = null)
    {

        $model = $this->formFindModelObject($recordId);
        
        $this->pageTitle = $model->c_companyname;
        $cid = $model->attributes;
        $this->cid = $cid['company_id'];
    
        

        parent::update($recordId, $context);
    
    }


    public function preview($recordId = null, $context = null){
         $this->formConfig = 'config_preview.yaml';
         $model = CompaniesModel::find($recordId);

         switch($model->c_typeback){
            case "p":
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/print_premier.yaml");
                //$this->config->form = "$/twnepc/pamcompanies/models/companies/print_premier.yaml";
                break;
            case "e":
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/print_enhanced.yaml");
                //$this->config->form = "$/twnepc/pamcompanies/models/companies/print_enhanced.yaml";
                break;
            default:
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/print_basic.yaml");
                //$this->config->form = "$/twnepc/pamcompanies/models/companies/print_basic.yaml";
        }
         parent::update($recordId, $context);

         

         return $this->makePartial('print',['showHeader' => false]);
        
    }

    public function clearsignoffdata(){
        //DB::table('pamtbl_companysignoff')->truncate();
        DB::table('pamtbl_companysignoff')->update(array('archive' => 1));
        Flash::success("Signoff Data successfully cleared!");
        return Backend::redirect("twnepc/pamcompanies/companies");
    }

    public function formBeforeUpdate($model){
        
        
        $user = BackendAuth::getUser();
        $cdata = post('Companies');

        $referenceData = array();

        $referenceData['company_id'] = $model->company_id;
        foreach ($cdata as $key => $value) {
            if(strpos($key, '_signoff'))
            {
                $referenceData[$key] = intval($value);
            }
        }
        $signoff = CompanySignoff::where($referenceData)->first();

        

        if(!$signoff){
            $signoff = new CompanySignoff;
            $signoff->company_id = $model->company_id;
            $signoff->cs_name = $user->first_name." ".$user->last_name;
            $signoff->pam_userid = $user->id;
            $signoff->pam_modified = Carbon::now()->toDateTimeString();
            $signoff->c_general_signoff = $cdata['c_general_signoff'];
            $signoff->c_about_us_signoff = $cdata['c_about_us_signoff'];
            if(isset($cdata['c_history_signoff'])) 
            {
                $signoff->c_history_signoff = $cdata['c_history_signoff'];
            }
            $signoff->c_investment_philosophy_signoff = $cdata['c_investment_philosophy_signoff'];
            $signoff->c_disclaimer_signoff = $cdata['c_disclaimer_signoff'];
            $signoff->c_kcd_signoff = $cdata['c_kcd_signoff'];
            $signoff->c_offices_signoff = $cdata['c_offices_signoff'];
            $signoff->c_investment_platforms_signoff = $cdata['c_investment_platforms_signoff'];
            $signoff->c_products_and_services_signoff = $cdata['c_products_and_services_signoff'];
            $signoff->c_main_shareholders_signoff = $cdata['c_main_shareholders_signoff'];
            $signoff->c_asset_classes_signoff = $cdata['c_asset_classes_signoff'];
            $signoff->c_key_personnel_signoff = $cdata['c_key_personnel_signoff'];
            $signoff->c_fees_and_charges_signoff = $cdata['c_fees_and_charges_signoff'];
            $signoff->c_articles_and_press_cuttings_signoff = $cdata['c_articles_and_press_cuttings_signoff'];
            $signoff->c_publications_signoff = $cdata['c_publications_signoff'];
            $signoff->c_news_signoff = $cdata['c_news_signoff'];
            $signoff->c_investment_commentary_signoff = $cdata['c_investment_commentary_signoff'];
            $signoff->c_minimum_thresholds_signoff = $cdata['c_minimum_thresholds_signoff'];
            $signoff->c_questions_and_answers_signoff = $cdata['c_questions_and_answers_signoff'];
            $signoff->save();
        }

        $model->bindEvent('model.setAttribute', function($key, $value) use ($model) {
                
                unset($model->attributes['c_progress']);
                unset($model->attributes['c_general_signoff']);
                unset($model->attributes['c_about_us_signoff']);
                unset($model->attributes['c_history_signoff']);
                unset($model->attributes['c_investment_philosophy_signoff']);
                unset($model->attributes['c_disclaimer_signoff']);
                unset($model->attributes['c_kcd_signoff']);
                unset($model->attributes['c_investment_platforms_signoff']);
                unset($model->attributes['c_products_and_services_signoff']);
                unset($model->attributes['c_offices_signoff']);
                unset($model->attributes['c_main_shareholders_signoff']);
                unset($model->attributes['c_asset_classes_signoff']);
                unset($model->attributes['c_key_personnel_signoff']);
                unset($model->attributes['c_fees_and_charges_signoff']);
                unset($model->attributes['c_articles_and_press_cuttings_signoff']);
                unset($model->attributes['c_publications_signoff']);
                unset($model->attributes['c_news_signoff']);
                unset($model->attributes['c_investment_commentary_signoff']);
                unset($model->attributes['c_minimum_thresholds_signoff']);
                unset($model->attributes['c_questions_and_answers_signoff']);
        });

    }

    public function formAfterSave($model)
    {
//        $user = BackendAuth::getUser();
//        $allTextTypes = TextTypes::all();
//        foreach($allTextTypes as $textType){
//            $existedCompanyText = Text::where('company_id',$model->company_id)->where('companytexttype_id',$textType->companytexttype_id)->first();
//            if(!$existedCompanyText){
//                $newText = new Text();
//                $newText->companytexttype_id = $textType->companytexttype_id;
//                $newText->company_id = $model->company_id;
//                $newText->pam_userid = $user->id;
//                $newText->save();
//            }
//        }

        
    }

    private function updateCompaniesTexts()
    {
        $user = BackendAuth::getUser();
        $allTextTypes = TextTypes::all();
        $companies = CompaniesModel::all();
        foreach($companies as $company){
            foreach($allTextTypes as $textType){
                $existedCompanyText = Text::where('company_id',$company->company_id)->where('companytexttype_id',$textType->companytexttype_id)->first();
                if(!$existedCompanyText){
                    $newText = new Text();
                    $newText->companytexttype_id = $textType->companytexttype_id;
                    $newText->company_id = $company->company_id;
                    $newText->pam_userid = $user->id;
                    $newText->save();
                }
            }
        }
    }

}