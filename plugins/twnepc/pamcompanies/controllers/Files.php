<?php
namespace Twnepc\PamCompanies\Controllers;

use App;
use Illuminate\Routing\Controller;
use Auth;
use Twnepc\PamCompanies\Classes\File;
use ApplicationException;

class Files extends Controller
{

    public function __construct()
    {

    }

    public function get($id)
    {
        $user = Auth::getUser();
        if(!$user)
            return App::make('Cms\Classes\Controller')->setStatusCode(404)->run('/404');

        $roles = [];
        $user->roles();
        foreach ($user->roles as $role){
            $roles[] = $role->id;
        }

        try {
            echo $this->findFileObject($id,$roles)->output();
            exit;
        }
        catch (Exception $ex) {}

        return App::make('Cms\Classes\Controller')->setStatusCode(404)->run('/404');
    }

    protected function findFileObject($id,$roles)
    {
        if (!$file = File::where('disk_name', $id)->first()) {
            throw new ApplicationException('Unable to find file');
        }
        return $file;
    }

}