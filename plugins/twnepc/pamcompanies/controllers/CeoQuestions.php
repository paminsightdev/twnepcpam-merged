<?php namespace Twnepc\PamCompanies\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class CeoQuestions extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.pamcompanies.access_edit_reviews'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.PamCompanies', 'companies','ceoquestions');
    }
}