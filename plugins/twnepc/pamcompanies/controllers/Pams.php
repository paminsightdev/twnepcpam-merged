<?php namespace Twnepc\PamCompanies\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Auth;
use Backend;
use Flash;
use Backend\Facades\BackendAuth;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use Backend\Models\User as BackendUser;
use DB;
use Hash;
use Event;

class Pams extends Controller
{
    public $implement = ['Backend\Behaviors\ListController','Backend\Behaviors\FormController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.pamcompanies.access_edit_pams'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Twnepc.PamCompanies', 'companies','pams');
    }

    public function onActivate($recordId)
    {
        $model = $this->formFindModelObject($recordId);
        $model->activated = post('Members[activated]',0);

        switch((int)$model->activated){
            case 0:
                if($this->deactivateMember($model->user)){
                    $model->save();
                    Event::fire('pamonline.pams.deactivate', [$model->user]);
                }
                break;

            case 1:
                if($this->activateMember($model->user)){
                    $model->save();
                    Event::fire('pamonline.pams.activate', [$model->user]);
                }
                break;
        }
        Flash::success("Update successfully");
    }

    function deactivateMember($user)
    {
        if(!$user->backenduser)return false;
        $backenduser = BackendAuth::findUserById($user->backenduser->id);
        $backenduser->delete();
        $backenduser->afterDelete();
        $user->backenduser_id = null;
        $user->save();
        Flash::success("Member deactivated.");
        return true;
    }

    function activateMember($user)
    {
        if($user->backenduser)return false;

        $existingBackendUser = BackendUser::where('email',$user->email)->first();
        if($existingBackendUser){
            $backenduser = BackendAuth::findUserById($existingBackendUser->id);
            $backenduser->delete();
            $backenduser->afterDelete();
        }

        $password = Hash::make($user->email);
        $backendUser = BackendAuth::register(array(
            'login' => $user->email,
            'email' => $user->email,
            'password' => $password,
            'password_confirmation' => $password
        ),true);

        $user->backenduser_id = $backendUser->id;
        $user->save();
        $pamsAdministratorGroup = CompanySettings::get('pams_administrator_group');
        Db::table('backend_users_groups')->insert(
            ['user_id' => $backendUser->id, 'user_group_id' => $pamsAdministratorGroup]
        );
        Flash::success("User is now verified.");
        return true;
    }
}