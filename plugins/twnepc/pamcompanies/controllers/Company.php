<?php namespace Twnepc\PamCompanies\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Auth;
use Backend;
use Backend\Facades\BackendAuth;
use Flash;
use Validator;
use ValidationException;
use Session;
use Twnepc\PamCompanies\Models\CompanySignoff as CompanySignoff;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
class Company extends Controller
{
    use \Twnepc\PamCompanies\Classes\Notificator;
    use \Twnepc\PamCompanies\Classes\PinSystemControllerTrait;

    public $implement = ['Backend\Behaviors\FormController'];
    
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = ['pamonline.pamcompanies.access_edit_company'];

    public function __construct()
    {
        parent::__construct($this);
        BackendMenu::setContext('Twnepc.PamCompanies', 'company');
        $this->addCss("/plugins/twnepc/pamcompanies/assets/css/custom.css", "1.0.1");
        $this->addjs("/plugins/twnepc/pamcompanies/assets/js/custom.js", "1.0.1");
        $this->addjs("/plugins/twnepc/pamcompanies/assets/js/DisplaySize.js", "1.0.1");
    }

    public function index()
    {
        $user = BackendAuth::getUser();
        //if($user->frontenduser->pamadmin->exists()) {
        if($user->frontenduser->pamadmin) {
            $companyID = $user->frontenduser->pamadmin->company->company_id;
        //} elseif($user->frontenduser->member->exists()) {
        } elseif($user->frontenduser->member) {
            $companyID = $user->frontenduser->member->company->company_id;
        }
                
        return Backend::redirect("twnepc/pamcompanies/company/update/{$companyID}");
    }
    
    /**
     * render the correct form for the pam admin or the user wanting to view a list of companies
     * @param integer $company_id
     * @return void
     * @todo does this function allow for no company_id - production.ERROR: ErrorException: Missing argument 1 for Twnepc\PamCompanies\Controllers\Company::update() in /home/somethingbig/public_html/plugins/twnepc/pamcompanies/controllers/Company.php:45
     */
    public function update($company_id)
    {
        if(!isset($company_id) || empty($company_id))  {
            // we should return a value prompting the user that they should call the office because theyt are not linked to the company they are trying to edit
        }
        
        $companyID = 0;
        $model = $this->formFindModelObject($company_id);
        switch($model->c_typeback){
            case "p":
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/c_typeback_fields_premier.yaml");
                    //formWidget->form = "$/twnepc/pamcompanies/models/companies/c_typeback_fields_premier.yaml";
                break;
            case "e":
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/c_typeback_fields_enhanced.yaml");
                    //formWidget->form = "$/twnepc/pamcompanies/models/companies/c_typeback_fields_enhanced.yaml";
                break;
            default:
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/c_typeback_fields_basic.yaml");
                    //formWidget->form = "$/twnepc/pamcompanies/models/companies/c_typeback_fields_basic.yaml";
        }
        

        $user = BackendAuth::getUser();
//        var_dump($user);
//        var_dump($user->frontenduser);
//        die();
        
        //[2020-03-10 18:27:40] production.ERROR: exception 'ErrorException' with message 'Trying to get property of non-object' in /var/www/pamonline/plugins/twnepc/pamcompanies/controllers/Company.php:67
        // when the user is not a pam staff employee but a company admin - use this object
        //if($user->frontenduser->pamadmin->exists()) {
        if($user->frontenduser->pamadmin) {
            $companyID = $user->frontenduser->pamadmin->company->company_id;
        // when the user is not a pam staff employee or company admin - use this object
        //} elseif($user->frontenduser->member->exists()) {
        } elseif($user->frontenduser->member) {
            $companyID = $user->frontenduser->member->company->company_id;
        }
        
        // check the user accessing company data is assigned to same company
        if($companyID == $company_id){
           return parent::update($company_id);
        }

        // return an empty form
        parent::update();
        
    }

     public function preview($recordId = null, $context = null){
        $this->formConfig = 'config_preview.yaml';
        $model = $this->formFindModelObject($recordId);
        switch($model->c_typeback){
            case "p":
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/print_premier.yaml");
                    //config->form = "$/twnepc/pamcompanies/models/companies/print_premier.yaml";
                break;
            case "e":
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/print_enhanced.yaml");
                    //config->form = "$/twnepc/pamcompanies/models/companies/print_enhanced.yaml";
                break;
            default:
                $this->initForm($model, "$/twnepc/pamcompanies/models/companies/print_basic.yaml");
                    //config->form = "$/twnepc/pamcompanies/models/companies/print_basic.yaml";
        }
        parent::update($recordId, $context);

        
        return $this->makePartial('print',['showHeader' => false]);
        
    }

    public function formBeforeUpdate($model){
        
        $user = BackendAuth::getUser();
        $cdata = post('Companies');

        $referenceData = array();

        $referenceData['company_id'] = $model->company_id;
        

        $signoff = CompanySignoff::where($referenceData)->orderBy('companysignoff_id','DESC')->first();

        foreach ($cdata as $key => $value) {
            if(strpos($key, '_signoff'))
            {
                //$referenceData[$key] = intval($value);
                if($signoff[$key] != $cdata[$key])
                {
                    $signoff = false;
                }
            }
        }

        $cs_name = NULL;
        Log::debug($user->frontenduser->name);
        if($user->first_name != NULL && $user->last_name !== NULL)
        {
            $cs_name = $user->first_name." ".$user->last_name;
        }
        else{
            $cs_name = $user->frontenduser->name." ".$user->frontenduser->surname;
        }


        if(!$signoff){
            $signoff = new CompanySignoff;
            $signoff->company_id = $model->company_id;
            $signoff->cs_name = $cs_name;
            $signoff->pam_userid = $user->id;
            $signoff->pam_modified = Carbon::now()->toDateTimeString();
            $signoff->c_general_signoff = $cdata['c_general_signoff'];
            $signoff->c_about_us_signoff = $cdata['c_about_us_signoff'];
            if(isset($cdata['c_history_signoff']))
            {
                $signoff->c_history_signoff = $cdata['c_history_signoff'];
            }
            $signoff->c_investment_philosophy_signoff = $cdata['c_investment_philosophy_signoff'];
            $signoff->c_disclaimer_signoff = $cdata['c_disclaimer_signoff'];
            $signoff->c_kcd_signoff = $cdata['c_kcd_signoff'];
            $signoff->c_offices_signoff = $cdata['c_offices_signoff'];
            $signoff->c_investment_platforms_signoff = $cdata['c_investment_platforms_signoff'];
            $signoff->c_products_and_services_signoff = $cdata['c_products_and_services_signoff'];
            $signoff->c_main_shareholders_signoff = $cdata['c_main_shareholders_signoff'];
            $signoff->c_asset_classes_signoff = $cdata['c_asset_classes_signoff'];
            $signoff->c_key_personnel_signoff = $cdata['c_key_personnel_signoff'];
            $signoff->c_fees_and_charges_signoff = $cdata['c_fees_and_charges_signoff'];
            $signoff->c_articles_and_press_cuttings_signoff = $cdata['c_articles_and_press_cuttings_signoff'];
            $signoff->c_publications_signoff = $cdata['c_publications_signoff'];
            $signoff->c_news_signoff = $cdata['c_news_signoff'];
            $signoff->c_investment_commentary_signoff = $cdata['c_investment_commentary_signoff'];
            $signoff->c_minimum_thresholds_signoff = $cdata['c_minimum_thresholds_signoff'];
            $signoff->c_questions_and_answers_signoff = $cdata['c_questions_and_answers_signoff'];
            $signoff->save();
        }
        
        $model->bindEvent('model.setAttribute', function($key, $value) use ($model) {
                
                unset($model->attributes['c_progress']);
                unset($model->attributes['c_general_signoff']);
                unset($model->attributes['c_about_us_signoff']);
                unset($model->attributes['c_history_signoff']);
                unset($model->attributes['c_investment_philosophy_signoff']);
                unset($model->attributes['c_disclaimer_signoff']);
                unset($model->attributes['c_kcd_signoff']);
                unset($model->attributes['c_investment_platforms_signoff']);
                unset($model->attributes['c_products_and_services_signoff']);
                unset($model->attributes['c_offices_signoff']);
                unset($model->attributes['c_main_shareholders_signoff']);
                unset($model->attributes['c_asset_classes_signoff']);
                unset($model->attributes['c_key_personnel_signoff']);
                unset($model->attributes['c_fees_and_charges_signoff']);
                unset($model->attributes['c_articles_and_press_cuttings_signoff']);
                unset($model->attributes['c_publications_signoff']);
                unset($model->attributes['c_news_signoff']);
                unset($model->attributes['c_investment_commentary_signoff']);
                unset($model->attributes['c_minimum_thresholds_signoff']);
                unset($model->attributes['c_questions_and_answers_signoff']);
        });


    }

    public function onDisabledFieldClick()
    {
        return $this->makePartial('disabled_fields_modal');
    }

    public function onDisabledTabClick()
    {
        return $this->makePartial('disabled_tab_modal');
    }
}