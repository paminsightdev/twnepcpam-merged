<?php namespace Twnepc\PamCompanies\Models;

use Model;

/**
 * Model
 */
class CompanyAsset extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'assetcompany_id';

    /*
     * Validation
     */
    public $rules = [
        //'ac_contactnote'=>'required'
    ];

    public $attributeNames = [
        'ac_contactnote'         => 'Contact',
    ];

    protected $fillable = [
        'company_id','asset','ac_description','ac_orderno','ac_type','ac_contactnote'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_assetcompany';

    public $belongsTo = [
        'asset' => [
            'Twnepc\PamCompanies\Models\Asset',
            'key' => 'assetclass_id',
            'otherKey' => 'assetclass_id'
        ],
    ];

    /**
     *       in: In-house
     *       out: 3rd party
     *       Both: Both
     * @param $key
     */
    public function getType()
    {
        $types = [
            'in'    => 'In-house',
            'out'   => '3rd party',
            'Both'  => 'Both'
        ];
        return $types[$this->ac_type];
    }

    public function getAssetOptions()
    {
        $options = [];
        $assets = Asset::get();
        if($this->company_id){
            $companyAssets = $this->where('company_id',$this->company_id)->get();
            $existingCompanyAssets = [];
            foreach($companyAssets as $companyAsset){
                $existingCompanyAssets[] = $companyAsset->assetclass_id;
            }
            foreach($assets as $key => $value){
                if(!in_array($value->assetclass_id,$existingCompanyAssets))
                    $options[$value->assetclass_id] = $value->ac_name;
            }
        }else{
            foreach($assets as $key => $value){
                $options[$value->assetclass_id] = $value->ac_name;
            }
        }
        return $options;
    }
}