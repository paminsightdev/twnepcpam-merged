<?php namespace Twnepc\PamCompanies\Models;

use Model;
use BackendAuth;
/**
 * Model
 */
class PamsModel extends Model
{

    public $rules = [];

    public function beforeCreate()
    {
        $user = BackendAuth::getUser();
        $user->frontenduser;
        if($user->frontenduser){
            $this->pam_userid = $user->frontenduser->id;
        }else{
            $this->pam_userid = $user->id;
        }
    }

    public function beforeUpdate()
    {
        $user = BackendAuth::getUser();
        $user->frontenduser;
        if($user->frontenduser){
            $this->pam_userid = $user->frontenduser->id;
        }else{
            $this->pam_userid = $user->id;
        }
    }

    public function beforeDelete()
    {
        if(isset($this->company_id))
                $this->logCompanyChanges($this->company_id,$this->original,array());
    }

    public function afterUpdate()
    {
        if(isset($this->company_id))
                $this->logCompanyChanges($this->company_id,$this->original,$this->attributes);
    }

    public function resetValidationRules()
    {
        $this->rules = [];
    }

    private function logCompanyChanges($companyId,$beforeSave,$afterSave)
    {
        $user = BackendAuth::getUser();
        $diff = array_diff_assoc($beforeSave,$afterSave);
        if(count($diff) > 0){
            $companyLogsModel = new CompanyLogs;
            $companyLogsModel->user_id = $user->id;
            $companyLogsModel->company_id = $companyId;
            $companyLogsModel->before_save = $beforeSave;
            $companyLogsModel->after_save = $afterSave;
            $companyLogsModel->save();
        }
    }
}