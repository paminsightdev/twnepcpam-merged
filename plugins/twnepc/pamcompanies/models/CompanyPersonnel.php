<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanyPersonnel extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companypersonnel_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $fillable = [
        'cp_firstname','cp_lastname','cp_jobtitle','cp_order','cp_salutation','company_id','section'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    public $attachOne = [
        'avatar' => ['Twnepc\Pamcompanies\Classes\File','public' => false]
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companypersonnel';

    public $belongsTo = [
        'section' => [
            'Twnepc\PamCompanies\Models\CompanyPersonnelSection',
            'key' => 'cpsection_id'
        ],
    ];

    public function getSalutations()
    {
        $options = [
            '' => '',
            'Mr.' => 'Mr.',
            'Mrs.' => 'Mrs.',
            'Miss' => 'Miss',
            'Dr.' => 'Dr.',
            'Ms.' => 'Ms.',
            'Prof.' => 'Prof.',
            'Rev.' => 'Rev.',
        ];
        return $options;
    }
}