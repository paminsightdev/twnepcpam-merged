<?php namespace Twnepc\PamCompanies\Models;

use Mail;
use Event;
use October\Rain\Auth\Models\User as UserBase;
use Twnepc\User\Models\Settings as UserSettings;

class User extends UserBase
{
    use \October\Rain\Database\Traits\SoftDeleting;

    /**
     * @var string The database table used by the model.
     */
    protected $table = 'users';
    /**
     * Validation rules
     */
    public $rules = [
        'email'    => 'required|between:6,255|email|unique:users|unique:backend_users',
        'phone' => 'regex:/^\+*\d( ?\d){10,14}$/', //'regex:/^\+[\d]{2}\s\(\d\)[\d]{3}\s[\d]{3}\s[\d]{4}$/',
    ];

    public $attributeNames = [
        'phone' => 'Telephone'
    ];

    public $customMessages = [
        'email.unique' => 'The email address is already in use.',
    ];

    /**
     * @var array Relations
     */
    public $belongsToMany = [
        'groups' => ['Rainlab\User\Models\UserGroup', 'table' => 'users_groups'],
        'roles' => ['Twnepc\Roles\Models\UserGroup', 'table' => 'pamonline_roles_assigned_roles', 'key'=>'user_id', 'otherKey' => 'role_id']
    ];

    public $belongsTo = [
        'backenduser' => [
            'Backend\Models\User',
            'table' => 'users_backendusers',
            'key' => 'backenduser_id',
        ]
    ];

    public $hasOne = [
        'member' => [
            'Twnepc\PamCompanies\Models\Members',
            'table' => 'pamonline_pamcompanies_members'
        ],
        'pamadmin' => [
            'Twnepc\PamCompanies\Models\Admins',
            'table' => 'pamonline_pamcompanies_admins'
        ]
    ];

    public $attachOne = [
        'avatar' => ['System\Models\File']
    ];

    /**
     * @var array The attributes that are mass assignable.
     */
    protected $fillable = [
        'name',
        'surname',
        'login',
        'username',
        'email',
        'salutation',
        'phone',
        'zip',
        'country_id',
        'contactby_other',
        'contactby_pam',
        'iu_contactbusinesses',
        'company',
        'company_id',
        'password',
        'password_confirmation',
    ];

    /**
     * Purge attributes from data set.
     */
    protected $purgeable = ['password_confirmation','activated'];

    public static $loginAttribute = null;

    public $temporary_password;

    /**
     * @return string Returns the name for the user's login.
     */
    public function getLoginName()
    {
        if (static::$loginAttribute !== null) {
            return static::$loginAttribute;
        }

        return static::$loginAttribute = UserSettings::get('login_attribute', UserSettings::LOGIN_EMAIL);
    }

    /**
     * Before validation event
     * @return void
     */
    public function beforeValidate()
    {
        /*
         * When the username is not used, the email is substituted.
         */
        if (
            (!$this->username) ||
            ($this->isDirty('email') && $this->getOriginal('email') == $this->username)
        ) {
            $this->username = $this->email;
        }
    }

    public function afterLogin()
    {
        if ($this->trashed()) {
            $this->last_login = $this->freshTimestamp();
            $this->restore();

            Mail::sendTo($this, 'twnepc.user::mail.reactivate', [
                'name' => $this->name
            ]);

            Event::fire('pamonline.user.reactivate', [$this]);
        }
        else {
            parent::afterLogin();
        }

        Event::fire('pamonline.user.login', [$this]);
    }

    /**
     * After delete event
     * @return void
     */
    public function afterDelete()
    {
        if ($this->isSoftDelete()) {
            Event::fire('twnepc.user.deactivate', [$this]);
            return;
        }
        Event::fire('twnepc.user.delete', [$this]);
        $this->avatar && $this->avatar->delete();

        parent::afterDelete();
    }

    public function scopeIsActivated($query)
    {
        return $query->where('is_activated', 1);
    }

    public function scopeFilterByGroup($query, $filter)
    {
        return $query->whereHas('roles', function($group) use ($filter) {
            $group->whereIn('role_id', $filter);
        });
    }

    /**
     * Gets a code for when the user is persisted to a cookie or session which identifies the user.
     * @return string
     */
    public function getPersistCode()
    {
        if (!$this->persist_code) {
            return parent::getPersistCode();
        }

        return $this->persist_code;
    }

    /**
     * Returns the public image file path to this user's avatar.
     */
    public function getAvatarThumb($size = 25, $options = null)
    {
        if (is_string($options)) {
            $options = ['default' => $options];
        }
        elseif (!is_array($options)) {
            $options = [];
        }

        // Default is "mm" (Mystery man)
        $default = array_get($options, 'default', 'mm');

        if ($this->avatar) {
            return $this->avatar->getThumb($size, $size, $options);
        }
        else {
            return '//www.gravatar.com/avatar/'.
            md5(strtolower(trim($this->email))).
            '?s='.$size.
            '&d='.urlencode($default);
        }
    }

    /**
     * Sends the confirmation email to a user, after activating.
     * @param  string $code
     * @return void
     */
    public function attemptActivation($code)
    {
        $result = parent::attemptActivation($code);
        if ($result === false) {
            return false;
        }

        if ($mailTemplate = UserSettings::get('welcome_template')) {
            Mail::sendTo($this, $mailTemplate, [
                'name'  => $this->name,
                'email' => $this->email
            ]);
        }

        return true;
    }

    /**
     * Looks up a user by their email address.
     * @return self
     */
    public static function findByEmail($email)
    {
        if (!$email) {
            return;
        }

        return self::where('email', $email)->first();
    }

    //
    // Last Seen
    //

    /**
     * Checks if the user has been seen in the last 5 minutes, and if not,
     * updates the last_login timestamp to reflect their online status.
     * @return void
     */
    public function touchLastSeen()
    {
        if ($this->isOnline()) {
            return;
        }

        $oldTimestamps = $this->timestamps;
        $this->timestamps = false;

        $this
            ->newQuery()
            ->where('id', $this->id)
            ->update(['last_login' => $this->freshTimestamp()])
        ;

        $this->timestamps = $oldTimestamps;
    }

    /**
     * Returns true if the user has been active within the last 5 minutes.
     * @return bool
     */
    public function isOnline()
    {
        return $this->getLastSeen() > $this->freshTimestamp()->subMinutes(5);
    }

    /**
     * Returns the date this user was last seen.
     * @return Carbon\Carbon
     */
    public function getLastSeen()
    {
        return $this->last_login ?: $this->created_at;
    }

    public function resetValidationRules()
    {
        $this->rules = [];
    }

    public function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if(!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($password) > $dash_len)
        {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }
}
