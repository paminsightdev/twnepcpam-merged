<?php namespace Twnepc\PamCompanies\Models;

use Model;

/**
 * Model
 */
class CompanyLogs extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $jsonable = ['before_save','after_save'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companylogs';

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies',
            'key' => 'company_id',
            'otherKey' => 'company_id'
        ]
    ];
}