<?php namespace Twnepc\PamCompanies\Models;

use Input;
/**
 * Model
 */
class CompanyFee extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companyfee_id';

    protected $chargeTypes = [
        ''  => 'Select Charge type',
        's' => 'Schedule of Annual Fees & Charges',
        't' => 'Transaction & Handling Charges'];

    /*
     * Validation
     */
    public $rules = [
        'feetype' => 'required',
        'cf_charge' => 'required',
    ];

    public $customMessages = [
        'cf_text.required' => 'The Remarks or Attachment must be filled.',
        'cf_filename.required' => 'Attachement display name must be filled.',
    ];

    protected $fillable = [
        'company_id','cf_text','feetype','cf_charge','cf_filename'
    ];

    public $attachOne = [
        'attachment' => ['Twnepc\Pamcompanies\Classes\File','public' => false]
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companyfees';

    public $belongsTo = [
        'feetype' => [
            'Twnepc\PamCompanies\Models\CompanyFeeType',
            'key' => 'cfeetype_id',
        ]
    ];

    public function beforeValidate()
    {
        $sessionKey = \Input::get('_session_key');
        $attachment = $this->attachment()->withDeferred($sessionKey)
            ->orderBy('sort_order')
            ->get();

        $attachmentAsArray = $attachment->toArray();

        if(!$attachment && $this->cf_text == ""){
            $this->rules['cf_text'] = 'required';
        }

        if(count($attachmentAsArray) > 0){
            $this->rules['cf_filename'] = 'required';
        }
    }

    public function getChargeByIndex($index)
    {
        if(!isset($this->chargeTypes[$index]))
            return '';

        return $this->chargeTypes[$index];
    }

    public function getFeeTypeOptions($dependsValue = false)
    {
        $options = [];
        $options[] = 'Select Fee type';
        $types = CompanyFeeType::orderBy('cft_name','ASC')->get();
        foreach($types as $type){
            if ($type->cf_charge == $dependsValue) {
                $options[$type->cfeetype_id] = $type->cft_name;
            }
        }
        return $options;
    }

    public function getFeeTypeOptionsAsJson()
    {
        $options = [];
        $options[] = 'Select Fee type';
        $types = CompanyFeeType::orderBy('cft_name','ASC')->get();
        foreach($types as $type){
            if($this->cf_charge) {
                if ($type->cf_charge == $this->cf_charge) {
                    $options[$type->cfeetype_id] = $type->cft_name;
                }
            }
        }
        return $options;
    }


    public function getCfChargeOptions($keyValue = null)
    {
        return $this->chargeTypes;
    }
}