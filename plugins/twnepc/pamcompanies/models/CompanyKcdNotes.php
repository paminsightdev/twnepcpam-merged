<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanyKcdNotes extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companykcdn_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $fillable = [
        'company_id','ckcdn_explanatorynote','ckcdn_orderno','ckcdn_superscript'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companykcdnotes';
}