<?php
	use Twnepc\PamCompanies\Models\Settings as Settings;

$func = function($html, $args){
    foreach($args as $key => $val) $html = str_replace("%[$key]", $val, $html);
    return $html;
};

$data['year'] = 'Year being edited';

?>




<div class="intro">
	<p><?php echo $func(Settings::get('pams_admin_companykcd_intro_text'),$data); ?></p>
</div>