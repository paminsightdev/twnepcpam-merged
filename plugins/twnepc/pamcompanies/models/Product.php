<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class Product extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'product_id';

    /*
     * Validation
     */
    public $rules = [
        'p_name' => 'required'
    ];

    public $attributeNames = [
        'p_name'         => 'Name',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_product';
}