<?php namespace Twnepc\PamCompanies\Models;

use Model;

/**
 * Model
 */
class Admins extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamonline_pamcompanies_admins';

    public $belongsTo = [
        'user' => [
            'Rainlab\User\Models\User',
            'table' => 'users',
            'key' => 'user_id',
        ],
        'company' => [
            'Twnepc\PamCompanies\Models\Companies',
            'table' => 'pamonline_pamcompanies_companies',
            'key' => 'company_id'
        ]
    ];
}