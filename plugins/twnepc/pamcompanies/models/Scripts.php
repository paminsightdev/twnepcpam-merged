<?php namespace Twnepc\PamCompanies\Models;

use Model;
use Twnepc\PamCompanies\Models\Members;
use Twnepc\PamCompanies\Models\Settings;
use Event;
use Rainlab\User\Models\User;
use Twnepc\Awards\Models\AwardApplication;
use Twnepc\PamCompanies\Classes\File;

class Scripts extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'pamcompanies_scripts';
    public $settingsFields = 'fields.yaml';


    public function getScripts()
    {
        $options = [];
        $options[''] = "--Select script to execute it--";
        $options['members_check'] = "Check if all PAMS has right roles set.";
        $options['pams_check'] = "Check if all users/PAMs has linked to company";
        $options['awardapp_fix'] = "Award application fix";
        $options['files_fix'] = "PAMs/Awards files modify as protected";
        return $options;
    }

    public function beforeSave()
    {
        switch($this->value['scripts']){
            case "members_check":
                $this->membersCheck();
                break;
            case "pams_check":
                $this->pamsFix();
                break;
            case "awardapp_fix":
                $this->awardApplicationFix();
                break;

            case "files_fix":
                $this->modifyFilesAsProtected();
                break;
        }

        return;
    }

    protected function modifyFilesAsProtected()
    {
        $models = [
            'Twnepc\PamCompanies\Models\Company',
            'Twnepc\PamCompanies\Models\Companies',
            'Twnepc\PamCompanies\Models\CompanyPersonnel',
            'Twnepc\Awards\Models\AwardApplication',
            'Twnepc\Awards\Models\AwardPersonnel',
            'Twnepc\PamCompanies\Models\CompanyFee',
            'Twnepc\PamCompanies\Models\CompanyCommentary'
        ];
        $isPublic = 0;
        $files = File::all();
        foreach($files as $file){
            if(in_array($file->attachment_type,$models)){
                if($file->is_public != $isPublic){
                    $file->is_public = $isPublic;
                    $file->save();
                }
            }
        }
    }

    protected function pamsFix()
    {
        $users = User::all();
        $pamsFrontendGroups = Settings::get('pams_frontend_groups');
        foreach($users as $user){
            $user->roles();
            $userIsPam = false;
            foreach($user->roles as $role){
                if( (int)$role->id  ===  (int)$pamsFrontendGroups){
                    $userIsPam = true;
                }
            }
            if($userIsPam){
                $member = Members::where('user_id',$user->id)->first();
                if(!$member){
                    if($user->company_id){
                        $member = new Members();
                        $member->user_id = $user->id;
                        $member->company_id = $user->company_id;
                        $member->save();
                    }
                }
            }
        }
    }

    protected function membersCheck()
    {
        $pamsFrontendGroups = Settings::get('pams_frontend_groups');
        //$pamsAdminFrontendGroups = Settings::get('pams_admin_frontend_groups');
        $members = Members::all();
        foreach($members as $member){
            $member->user->roles();
            $userIsPam = false;
            foreach($member->user->roles as $role){
                if( (int)$role->id  ===  (int)$pamsFrontendGroups){
                    $userIsPam = true;
                }
            }
            if(!$userIsPam){
                $response = Event::fire('twnepc.user.deactivate', [$member->user]);
            }
        }
        exit();
    }

    protected function awardApplicationFix()
    {
        $awardApplications = AwardApplication::where('aa_applied',0)->where('aa_submitted',0)->get();
        foreach($awardApplications as $app){
            $app->aa_applied = 1;
            $app->resetValidationRules();
            $app->save();
        }
    }

}