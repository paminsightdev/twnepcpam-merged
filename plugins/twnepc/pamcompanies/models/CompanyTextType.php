<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanyTextType extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companytexttype_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companytexttypes';
}