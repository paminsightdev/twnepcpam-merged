<?php namespace Twnepc\Pamcompanies\Models;

/**
 * Model
 */
class CompanyCommentary extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companycomment_id';

    /*
     * Validation
     */
    public $rules = [
        'cc_name' => 'required'
    ];

    public $attributeNames = [
        'cc_name' => 'Name',
    ];

    protected $fillable = [
        'company_id','cc_name','cc_text','cc_date','cc_url','cc_contenttype'
    ];

    public $attachOne = [
        'attachment' => ['Twnepc\Pamcompanies\Classes\File','public' => false]
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companycommentary';
}