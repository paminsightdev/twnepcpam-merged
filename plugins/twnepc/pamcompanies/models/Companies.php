<?php namespace Twnepc\PamCompanies\Models;

use Twnepc\PamCompanies\Models\CompanyText;
use Twnepc\PamCompanies\Models\CompanySignoff;


/**
 * Model
 */
class Companies extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'company_id';
    public $email;

    /*
     * Validation
     */
    public $rules = [
        'c_companyname'=>'required',
        'c_discmin' =>'numeric',
        'c_advmin' =>'numeric',
        'c_averageportsize' => 'numeric'
    ];


    public $attributeNames = [
        'c_companyname'=>'Company Name',
        'c_discmin' =>'Discretionary Min. Portfolio Size',
        'c_advmin' =>'Advisory Min. Portfolio Size',
        'c_averageportsize' => 'Average Client Size'
    ];
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_company';

    protected $guarded = ['answers','c_general_signoff'];

    public $attachOne = [
        'logo' => ['System\Models\File']
    ];

    public $hasOne = [
        'companytextintro' => [
            'Twnepc\PamCompanies\Models\CompanyText',
            'key' => 'company_id',
            'conditions' => 'companytexttype_id = 1',
            'delete' => true
        ],
        'companytextinvestment' => [
            'Twnepc\PamCompanies\Models\CompanyText',
            'key' => 'company_id',
            'conditions' => 'companytexttype_id = 2',
            'delete' => true
        ],
        'companytexthistory' => [
            'Twnepc\PamCompanies\Models\CompanyText',
            'key' => 'company_id',
            'conditions' => 'companytexttype_id = 3',
            'delete' => true
        ],
        'companytextfeesnotes' => [
            'Twnepc\PamCompanies\Models\CompanyText',
            'key' => 'company_id',
            'conditions' => 'companytexttype_id = 4',
            'delete' => true
        ],
        'companytextdisclaimeronline' => [
            'Twnepc\PamCompanies\Models\CompanyText',
            'key' => 'company_id',
            'conditions' => 'companytexttype_id = 5',
            'delete' => true
        ],
        'companytextdisclaimerdirectory' => [
            'Twnepc\PamCompanies\Models\CompanyText',
            'key' => 'company_id',
            'conditions' => 'companytexttype_id = 6',
            'delete' => true
        ],
    ];

    public $hasMany = [
        'companytext' => [
            'Twnepc\PamCompanies\Models\CompanyText',
            'key' => 'company_id'
        ],
        'answers' => [
            'Twnepc\PamCompanies\Models\CompanyAnswer',
            'key' => 'company_id'
        ],
        'products' => [
            'Twnepc\PamCompanies\Models\CompanyProduct',
            'key' => 'company_id',
            'order'      => 'pc_orderno asc',
        ],
        'offices' => [
            'Twnepc\PamCompanies\Models\Office',
            'key' => 'company_id',
            'order'      => 'o_ordernouk asc',
        ],
        'shareholders' => [
            'Twnepc\PamCompanies\Models\CompanyShareholder',
            'key' => 'company_id',
            'order'      => 'csh_orderno asc',
        ],
        'assets' => [
            'Twnepc\PamCompanies\Models\CompanyAsset',
            'key' => 'company_id',
            'order'      => 'ac_orderno asc',
        ],
        'personnel' => [
            'Twnepc\PamCompanies\Models\CompanyPersonnel',
            'key'   => 'company_id',
            'order' => 'cp_order asc',
        ],
        'personnelsections' => [
            'Twnepc\PamCompanies\Models\CompanyPersonnelSection',
            'key'   => 'company_id',
            'order' => 'cps_order asc',
        ],
        'articles' => [
            'Twnepc\PamCompanies\Models\CompanyCommentary',
            'key'   => 'company_id',
            'conditions' => 'cc_contenttype = "a"'
        ],
        'publications' => [
            'Twnepc\PamCompanies\Models\CompanyCommentary',
            'key'   => 'company_id',
            'conditions' => 'cc_contenttype = "p"'
        ],
        'news' => [
            'Twnepc\PamCompanies\Models\CompanyCommentary',
            'key'   => 'company_id',
            'conditions' => 'cc_contenttype = "n"'
        ],
        'investments' => [
            'Twnepc\PamCompanies\Models\CompanyCommentary',
            'key'   => 'company_id',
            'conditions' => 'cc_contenttype = "i"'
        ],
        'fees' => [
            'Twnepc\PamCompanies\Models\CompanyFee',
            'key'   => 'company_id',
            'order' => 'cf_position asc',
        ],
        'kcdnotes' => [
            'Twnepc\PamCompanies\Models\CompanyKcdNotes',
            'key'   => 'company_id',
            'order' => 'ckcdn_orderno asc',
        ],
        'kcd' => [
            'Twnepc\PamCompanies\Models\CompanyKcd',
            'key'   => 'company_id',
            'order' => 'ckcd_year desc',
        ],
    ];


    public $belongsTo = [
        'companytype' => [
            'Twnepc\PamCompanies\Models\CompanyType',
            'table' => 'pamtbl_companytype',
            'key' => 'companytype_id',
        ]
    ];

    public $belongsToMany = [
        'registeredbodies' => [
            'Twnepc\PamCompanies\Models\RegisteredBody',
            'table' => 'pamtbl_registeredbodyhash',
            'key'   => 'company_id',
            'otherKey' => 'rbody_id',
            'order'      => 'rb_name asc',
        ],
        'platforms' => [
            'Twnepc\PamCompanies\Models\Platform',
            'table' => 'pamtbl_companyplatform',
            'key'   => 'company_id',
            'otherKey' => 'platform_id',
            'order'      => 'platform_name asc',
        ],
    ];
    
    public static function getAllUserCompaniesSubscriptions($user_id,$subscription='') {
        if ($subscription == '') {
            $favCompanies = \Db::table('pamtbl_favourite_companies')->where(["REGUSER_ID" => $user_id])->get();
        } else {
            $favCompanies = \Db::table('pamtbl_favourite_companies')->where(["REGUSER_ID" => $user_id, "subscription" => $subscription])->get();
        }
        $results = [];
        foreach ($favCompanies as $company) {
            $companyMsgs = self::getUpdatedSections($user_id, $company->PamID, $company->name);
            if (count($companyMsgs) > 0) {
                $results = array_merge($results,$companyMsgs);
            }
        }
        return $results;
    }
    
    public static function getUpdatedSections($user_id,$company_id,$company_title='') {
        $sectionUpdateRecord = \Db::table('company_sections_updates')->where(["company_id"=>$company_id])->first();
        $sectionUserRecord = \Db::table('company_sections_users')->where(["user_id"=>$user_id,"company_id"=>$company_id])->first();
        if (!$sectionUpdateRecord) {
            return [];
        }
        
        
        if ($sectionUserRecord) {
            $sectionUserRecord->investment_commentary = isset($sectionUserRecord->investment_commentary)?intval($sectionUserRecord->investment_commentary):0;
            $sectionUserRecord->publications = isset($sectionUserRecord->publications)?intval($sectionUserRecord->publications):0;
            $sectionUserRecord->news = isset($sectionUserRecord->news)?intval($sectionUserRecord->news):0;
            $sectionUserRecord->articles = isset($sectionUserRecord->articles)?intval($sectionUserRecord->articles):0;
        } else {
            $sectionUserRecord = new \stdClass();
            $sectionUserRecord->investment_commentary = 0;
            $sectionUserRecord->publications = 0;
            $sectionUserRecord->news = 0;
            $sectionUserRecord->articles = 0;            
        }
        
        if ($sectionUpdateRecord) {
            $sectionUpdateRecord->investment_commentary = isset($sectionUpdateRecord->investment_commentary)?intval($sectionUpdateRecord->investment_commentary):0;
            $sectionUpdateRecord->publications = isset($sectionUpdateRecord->publications)?intval($sectionUpdateRecord->publications):0;
            $sectionUpdateRecord->news = isset($sectionUpdateRecord->news)?intval($sectionUpdateRecord->news):0;
            $sectionUpdateRecord->articles = isset($sectionUpdateRecord->articles)?intval($sectionUpdateRecord->articles):0;
        } else {
            $sectionUpdateRecord = new \stdClass();
            $sectionUpdateRecord->investment_commentary = 0;
            $sectionUpdateRecord->publications = 0;
            $sectionUpdateRecord->news = 0;
            $sectionUpdateRecord->articles = 0;      
        }
        $result = [];
        if ($sectionUpdateRecord->investment_commentary > $sectionUserRecord->investment_commentary) {
            $result[] =  ['company_id'=>$company_id, 'type'=>'investment_commentary', 'text' => "\"{$company_title}\" company: Investment Commentary added or edited.",'url'=>"company/{$company_id}/investment-commentary"];
        } 
        if ($sectionUpdateRecord->publications > $sectionUserRecord->publications) {
            $result[] = ['company_id'=>$company_id, 'type'=>'publications', 'text' => "\"{$company_title}\" company: Publications added or edited.", 'url'=>"company/{$company_id}/publications"];
        } 
        if ($sectionUpdateRecord->news > $sectionUserRecord->news) {
            $result[] = ['company_id'=>$company_id, 'type'=>'news', 'text' => "\"{$company_title}\" company: News and Announcements added or edited.", 'url'=>"company/{$company_id}/news"];
        } 
        if ($sectionUpdateRecord->articles > $sectionUserRecord->articles) {
            $result[] = ['company_id'=>$company_id, 'type'=>'articles', 'text' => "\"{$company_title}\" company:  Articles and Press Cuttings added or edited.", 'url'=>"company/{$company_id}/articles"];
        }
        return $result;                
    }
    
    
    public static function userSectionUpdate($user_id,$company_id,$section) {
        $user_id = intval($user_id);
        $company_id = intval($company_id);
        if ($user_id <= 0 or $company_id <= 0) return false;
        $sectionUpdateRecord = \Db::table('company_sections_users')->where(["user_id"=>$user_id,"company_id"=>$company_id])->first();
        if (!$sectionUpdateRecord) {
            \Db::table('company_sections_users')->insert([
                'user_id' => $user_id,
                'company_id' => $company_id,
                $section => time()
            ]);
        } else {
            \Db::table('company_sections_users')->where(["user_id"=>$user_id,"company_id"=>$company_id])->update([$section => time()]);
        }
        return true;
    }
    

    public static function sectionUpdate($company_id,$section) {
        $sectionUpdateRecord = \Db::table('company_sections_updates')->where(["company_id"=>$company_id])->first();
        if (!$sectionUpdateRecord) {
            \Db::table('company_sections_updates')->insert([
                'company_id' => $company_id,
                $section => time()
            ]);
        } else {
            \Db::table('company_sections_updates')->where(["company_id"=>$company_id])->update([$section => time()]);
        }
        return true;
    }
    
    public function getCompanyQuestions()
    {
        return CompanyQuestion::all();
    }

    public function getEmail()
    {
        $email = false;
        $offices = $this->offices;
        foreach($offices as $office){
            if($office->o_headoffice == 1){
                $email = $office->o_emailaddress;
                break;
            }
        }
        if(!$email){
            foreach($offices as $office){
                if($office->o_website != ""){
                    $email = $office->o_emailaddress;
                    break;
                }
            }
        }

        if($email == 'janderson@tru-est.com')
                $email = false;

        $this->email = $email;
        return $email;
    }

    public function getLinkToWebsite()
    {
        $linkToWebsite = false;
        $offices = $this->offices;
        foreach($offices as $office){
            if($office->o_headoffice == 1){
                $linkToWebsite = $office->o_website;
                break;
            }
        }
        if(!$linkToWebsite){
            foreach($offices as $office){
                if($office->o_website != ""){
                    $linkToWebsite = $office->o_website;
                    break;
                }
            }
        }

        if ($linkToWebsite && strpos($linkToWebsite, 'http://') == false) {
            $linkToWebsite = "http://".$linkToWebsite;
        }
        return $linkToWebsite;
    }

    public function afterFetch()
    {
        if(!$this->companytextintro){
            $this->companytextintro = new CompanyText;
            $this->companytextintro->companytexttype_id = 1;
        }
        if(!$this->companytextinvestment){
            $this->companytextinvestment = new CompanyText;
            $this->companytextinvestment->companytexttype_id = 2;
        }
        if(!$this->companytexthistory){
            $this->companytexthistory = new CompanyText;
            $this->companytexthistory->companytexttype_id = 3;
        }
        if(!$this->companytextfeesnotes){
            $this->companytextfeesnotes = new CompanyText;
            $this->companytextfeesnotes->companytexttype_id = 4;
        }
        if(!$this->companytextdisclaimeronline){
            $this->companytextdisclaimeronline = new CompanyText;
            $this->companytextdisclaimeronline->companytexttype_id = 5;
        }
        if(!$this->companytextdisclaimerdirectory){
            $this->companytextdisclaimerdirectory = new CompanyText;
            $this->companytextdisclaimerdirectory->companytexttype_id = 6;
        }

	
        $signoff = CompanySignoff::where(['company_id'=>$this->company_id])->orderBy('companysignoff_id','DESC')->first();
        if($signoff){
            $data = $signoff->toArray();

            if(isset($data))
            {

            foreach ($data as $key => $value) {
                if($value == 1  && strpos($key, '_signoff'))
                {
                    $this[$key] = 1;
                }
            }
            

            }
        }
                 
    }

    public function beforeSave()
    {
	 foreach ($this->attributes as $key => $value) {
                if(strpos($key, '_signoff'))
                {
                    unset($this->attributes[$key]);
                }
            }
    }



}
