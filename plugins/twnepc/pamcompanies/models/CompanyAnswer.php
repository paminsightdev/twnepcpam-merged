<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanyAnswer extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companyanswer_id';

    /*
     * Validation
     */
    public $rules = [
        'ca_text' =>'required'
    ];

    protected $fillable = [
        'ca_text','personnel','companyquestion_id','company_id'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companyanswer';

    public $hasOne = [
        'question' => [
            'Twnepc\PamCompanies\Models\CompanyQuestion',
            'key' => 'companyquestion_id',
            'otherKey' => 'companyquestion_id'
        ],
    ];

    public $belongsTo = [
        'personnel' => [
            'Twnepc\PamCompanies\Models\CompanyPersonnel',
            'key' => 'companypersonnel_id',
            'otherKey' => 'companypersonnel_id'
        ],
    ];
}