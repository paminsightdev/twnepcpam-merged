<?php namespace Twnepc\PamCompanies\Models;
use Exception;

/**
 * Model
 */
class CompanyKcd extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companykcd_id';

    
    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies',
            'key' => 'company_id',
            'otherKey' => 'company_id'
        ]
    ];
    
    /*
     * Validation
     */
    public $rules = [
        'ckcd_year'         => 'required|numeric',
        'ckcd_clientassets' => 'required|numeric',
        'ckcd_clientadvice' => 'required|numeric',
        'ckcd_staffemployed'=> 'required|numeric',
        'ckcd_numstaff'     => 'required|numeric',
        'ckcd_discsplit'    => 'required|numeric',
        'ckcd_revenue'      => 'required|numeric',
        'ckcd_numclients'   => 'required|numeric',
        'ckcd_profit'       => 'required|numeric'
    ];

    public $attributeNames = [
        'ckcd_year'         => 'Year',
        'ckcd_clientassets' => 'Client Assets',
        'ckcd_clientadvice' => 'Client Advice',
        'ckcd_staffemployed'=> 'Staff Employed',
        'ckcd_numstaff'     => 'Staff Number',
        'ckcd_discsplit'    => 'Discretionary and Advisory Business Splits',
        'ckcd_numclients'   => 'Total Number Of Private Client Accounts',
        'ckcd_revenue'      => 'Revenue',
        'ckcd_profit'       => 'Profit',
    ];

    protected $fillable = [
        'company_id','ckcd_year','ckcd_clientassets','ckcd_clientadvice','ckcd_staffemployed',
        'ckcd_numstaff','ckcd_numclients','ckcd_discsplit','ckcd_revenue','ckcd_profit',
        'ckcd_clientassets_prefix','ckcd_clientadvice_prefix','ckcd_staffemployed_prefix',
        'ckcd_numstaff_prefix','ckcd_numclients_prefix','ckcd_discsplit_prefix','ckcd_revenue_prefix','ckcd_profit_prefix',
        'ckcd_clientassets_suffix','ckcd_clientadvice_suffix','ckcd_staffemployed_suffix','ckcd_numstaff_suffix','ckcd_numclients_suffix',
        'ckcd_discsplit_suffix','ckcd_revenue_suffix','ckcd_profit_suffix'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companykcd';

    public function getYearOptions()
    {
        $currentYear = intval(date('Y'));
        $prevYear = $currentYear-1;
        $options = [];
        $options[''] = "Select available year";
        if($this->company_id){
            $kcdData = $this->where('company_id',$this->company_id)->get();
            $alreadyExist = false;
            foreach($kcdData as $year){
                if($year->ckcd_year == $prevYear)
                    $alreadyExist = true;
            }
            if(!$alreadyExist){
                $options[$prevYear] = $prevYear;
            }
        }
        return $options;
    }

    public function getAvailableYear()
    {
        if(!$this->company_id)
                throw new Exception("Company id must be set.");
        $availableYear = 0;
        $options = $this->getYearOptions();
        foreach($options as $key => $value){
            if($key != ""){
                $keyAsInt = (int) $key;
                if($availableYear < $keyAsInt){
                    $availableYear  = $keyAsInt;
                }
            }
        }
        return $availableYear;
    }

//    public function getYearOptions()
//    {
//        $currentYear = intval(date('Y'));
//        $options = [];
//        $options[''] = "Select available year";
//        if($this->company_id){
//            $kcdData = $this->where('company_id',$this->company_id)->get();
//            $lastKcdDataYear = 1993;
//            foreach($kcdData as $year){
//                if($year->ckcd_year > $lastKcdDataYear)
//                        $lastKcdDataYear = $year->ckcd_year;
//            }
//            $lastKcdDataYear++;
//            while ($lastKcdDataYear < $currentYear) {
//                $options[$lastKcdDataYear] = $lastKcdDataYear;
//                $lastKcdDataYear++;
//            }
//        }
//        return $options;
//    }
}