<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class RegisteredBody extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'rbody_id';

    /*
     * Validation
     */
    public $rules = [
        'rb_name' => 'required',
        'rb_abbrev' => 'required'
    ];

    public $attributeNames = [
        'p_name'         => 'Name',
        'rb_abbrev' => 'Abbreviature',
    ];


    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_registeredbody';
}