<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class Platform extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'platform_id';

    /*
     * Validation
     */
    public $rules = [
        'platform_name' => 'required'
    ];

    public $attributeNames = [
        'platform_name'         => 'Name',
    ];

    protected $fillable = [];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_platform';
}