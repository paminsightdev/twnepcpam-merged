<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanySignoff extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companysignoff_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $fillable = [
        'cs_name',
        'company_id',
        'user_id',
        #'pam_userid',
        #'pam_modified',
        #'c_general_signoff',
        #'c_about_us_signoff',
        #'c_history_signoff',
        #'c_investment_philosophy_signoff',
        #'c_disclaimer_signoff',
        ##'c_kcd_signoff',
        #'c_investment_platforms_signoff',
        #'c_products_and_services_signoff',
        #'c_offices_signoff',
        #'c_main_shareholders_signoff',
        #'c_asset_classes_signoff',
        #'c_key_personnel_signoff',
        #'c_fees_and_charges_signoff',
        #'c_articles_and_press_cuttings_signoff',
        #'c_publications_signoff',
        #'c_news_signoff',
        #'c_investment_commentary_signoff',
        #'c_minimum_thresholds_signoff',
        #'c_questions_and_answers_signoff'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companysignoff';
}