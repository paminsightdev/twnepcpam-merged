<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanyQuestion extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companyquestion_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companyquestion';

    public $belongsTo = [
        'questionsection' => [
            'Twnepc\PamCompanies\Models\CompanyQuestionSection',
            'key' => 'companyqsection_id',
            'otherKey' => 'companyqsection_id'
        ],
    ];
}