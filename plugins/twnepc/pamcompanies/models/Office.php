<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class Office extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'office_id';

    /*
     * Validation
     */
    public $rules = [
        'o_address1' => 'required',
        'o_telephone' => 'required|regex:/^\+*\d( ?\d){10,14}$/', //regex:/^[0-9+]+$/',
    ];

    public $attributeNames = [
        'o_telephone' => 'Telephone'
    ];

    protected $fillable = [
        'o_contactname',
        'o_contactjobtitle',
        'o_address1',
        'o_address2',
        'o_address3',
        'o_town',
        'o_postcode',
        'o_telephone',
        'o_fax',
        'o_website',
        'o_headoffice',
        'o_otheroffice',
        'o_ordernouk',
        'company_id',
        'country',
        'o_point_x',
        'o_point_y',
        'o_directory',
        'timezone',
        'utc_offset',
        'o_emailaddress'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_office';

    public $belongsTo = [
        'country' => [
            'Twnepc\PamCompanies\Models\Country',
            'key'=>'country_id',
            'otherKey' => 'country_id',
            'order' => 'country_name ASC',
        ]
    ];
}