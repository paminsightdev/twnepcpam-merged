<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class RegisteredBodyHash extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'rbh_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $fillable = [
        'company_id','rbody_id'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_registeredbodyhash';

    public $hasMany = [
        'body' => [
            'Twnepc\PamCompanies\Models\RegisteredBody',
            'key' => 'rbody_id',
            'otherKey' => 'rbody_id'
        ],
    ];
}