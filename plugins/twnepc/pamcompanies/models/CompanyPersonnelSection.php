<?php namespace Twnepc\PamCompanies\Models;

use ValidationException;
/**
 * Model
 */
class CompanyPersonnelSection extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'cpsection_id';

    /*
     * Validation
     */
    public $rules = [
        'cps_name' => 'required'
    ];

    protected $fillable = [
        'cps_name','cps_order','company_id'
    ];

    public $options1 = [
        'Board'                     => 'Board',
        'UK Management Committee'   => 'UK Management Committee',
        'Senior Management'         => 'Senior Management',
        'Executive Committee'       => 'Executive Committee',
        'Executive Partners'        => 'Executive Partners',
        'Managing Partners'         => 'Managing Partners',
    ];

    public $options2 = [
        'Investment Committee'          => 'Investment Committee',
        'UK Management Committee'       => 'UK Management Committee',
        'Investment Management Staff'   => 'Investment Management Staff',
        'Asset Allocation Committee'    => 'Asset Allocation Committee',
        'Investment Oversight Committee'=> 'Investment Oversight Committee',
    ];

    public $options3 = [
        'Key Personnel'       => 'Key Personnel',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companypersonnelsection';

    public function getSectionOptionsOnCreate()
    {
        $options = [];
        if($this->company_id){
            $personnelSections = $this->where('company_id',$this->company_id)->get();
            $opt1 = false;
            $opt2 = false;
            $opt3 = false;
            foreach($personnelSections as $section){
                if(array_key_exists($section->cps_name,$this->options1)){
                    $opt1 = true;
                }
                if(array_key_exists($section->cps_name,$this->options2)){
                    $opt2 = true;
                }
                if(array_key_exists($section->cps_name,$this->options3)){
                    $opt3 = true;
                }
            }
            if(!$opt1)$options['Board'] = "Board";
            if(!$opt2)$options['Investment Committee'] = "Investment Committee";
            if(!$opt3)$options['Key Personnel'] = "Key Personnel";
        }
        return $options;
    }

    public function getSectionOptionsOnUpdate($keyValue)
    {
        if(array_key_exists($keyValue,$this->options1))
            return $this->options1;

        if(array_key_exists($keyValue,$this->options2))
            return $this->options2;

        if(array_key_exists($keyValue,$this->options3))
            return $this->options3;
        return [];
    }

    public function beforeCreate()
    {
        $section = $this->where("cps_name",$this->cps_name)->where('company_id',$this->company_id)->first();
        if($section)
            throw new ValidationException(['cps_name' => 'Sorry that section already exist!']);

        $count = $this->where('company_id',$this->company_id)->count();
        if($count >= 3)
            throw new ValidationException(['cps_name' => 'You can have maximum 3 section!']);
    }

}