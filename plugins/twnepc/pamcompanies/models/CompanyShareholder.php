<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanyShareholder extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companysh_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $fillable = [
        'csh_share',
        'csh_name',
        'csh_orderno',
        'company_id',
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companyshareholders';
}