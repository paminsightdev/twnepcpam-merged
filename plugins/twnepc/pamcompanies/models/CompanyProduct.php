<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanyProduct extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'productcompany_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    protected $fillable = [
        'company_id','pc_description','product','pc_orderno'
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_productcompany';

    public $belongsTo = [
        'product' => [
            'Twnepc\PamCompanies\Models\Product',
            'key' => 'product_id',
            'otherKey' => 'product_id',
            'order'   => 'p_name asc'
        ],
    ];
}