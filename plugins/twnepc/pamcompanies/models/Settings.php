<?php namespace Twnepc\PamCompanies\Models;

use Model;
use Twnepc\Roles\Models\Group;
use Backend\Models\UserGroup;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'pamcompanies_settings';
    public $settingsFields = 'fields.yaml';

    public function getPamsAdminFrontendGroupsOptions()
    {
        $settings = [];
        $roles = Group::all();
        foreach($roles as $role){
            $settings[$role->id] = $role->name;
        }
        return $settings;
    }

    public function getPamsFrontendGroupsOptions()
    {
        $settings = [];
        $roles = Group::all();
        foreach($roles as $role){
            $settings[$role->id] = $role->name;
        }
        return $settings;
    }

    public function getPamsAdminAdministratorGroupOptions()
    {
        $options = [];
        $groups = UserGroup::all();
        foreach($groups as $group){
            $options[$group->id] = $group->name;
        }
        return $options;
    }

    public function getPamsAdministratorGroupOptions()
    {
        $options = [];
        $groups = UserGroup::all();
        foreach($groups as $group){
            $options[$group->id] = $group->name;
        }
        return $options;
    }

}