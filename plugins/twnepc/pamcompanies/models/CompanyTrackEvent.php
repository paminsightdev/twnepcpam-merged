<?php namespace Twnepc\PamCompanies\Models;

use Model;

/**
 * Model
 */
class CompanyTrackEvent extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companytrackevents';

    public $belongsTo  = [
        'user' => ['Rainlab\User\Models\User', 'key' => 'user_id', 'otherKey' => 'id']
    ];
}