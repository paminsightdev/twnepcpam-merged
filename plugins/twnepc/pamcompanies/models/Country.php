<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class Country extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'country_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_country';
}