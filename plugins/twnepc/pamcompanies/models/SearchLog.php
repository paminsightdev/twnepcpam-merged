<?php namespace Twnepc\PamCompanies\Models;

use Model;

/**
 * Model
 */
class SearchLog extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_search_log';

    public $belongsTo  = [
        'user' => ['Rainlab\User\Models\User', 'key' => 'SL_UserID', 'otherKey' => 'id']
    ];

    /**
     * TODO: Read about relationship queries
     */
    public function scopeFilterByGroup($query,$filter)
    {
        $query->whereHas('user',function($user) use ($filter) {
            $user->whereHas('roles',function($group) use ($filter) {
                $group->whereIn('role_id', $filter);
            });
        });
    }

}