<?php namespace Twnepc\PamCompanies\Models;

/**
 * Model
 */
class CompanyText extends PamsModel
{
    use \October\Rain\Database\Traits\Validation;

    protected $primaryKey = 'companytext_id';

    /*
     * Validation
     */
    public $rules = [
    ];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamtbl_companytext';

    public $belongsTo = [
        'companytexttype' => [
            'Twnepc\PamCompanies\Models\CompanyTextType',
            'table' => 'pamtbl_companytexttype',
            'key' => 'companytexttype_id',
        ]
    ];
}