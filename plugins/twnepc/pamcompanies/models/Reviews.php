<?php namespace Twnepc\PamCompanies\Models;

use Model;
use Rainlab\User\Models\User;

/**
 * Model
 */
class Reviews extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /*
     * Validation
     */
    public $rules = [];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    protected $jsonable = ['rate'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'pamonline_pamcompanies_reviews';

    public $belongsTo = [
        'company' => [
            'Twnepc\Pamcompanies\Models\Companies',
            'key' => 'company_id',
            'otherKey' => 'company_id'
        ],
        'user' => [
            'Rainlab\User\Models\User',
            'key' => 'user_id',
            'otherKey' => 'id'
        ]
    ];

    public function getCompanyDetails()
    {
        return Companies::where('company_id',$this->company_id)->first();
    }

    public function getAuthor()
    {
        return User::where('id',$this->user_id)->first();
    }
}