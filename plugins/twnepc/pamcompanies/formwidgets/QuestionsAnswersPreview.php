<?php namespace Twnepc\PamCompanies\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyAnswer as Answer;
use Twnepc\PamCompanies\Models\CompanyQuestion as Question;
use Twnepc\PamCompanies\Models\CompanyQuestionSection as QuestionSections;
use Exception;

/**
 * Repeater Form Widget
 */
class QuestionsAnswersPreview extends FormWidgetBase
{
    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsAnswers";

     /**
      * @var bool Stops nested repeaters populating from previous sibling.
      */
    protected static $onAddItemCalled = false;

    public function init()
    {
        $this->getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('view');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $id = $this->model->company_id;
        $this->vars['sections'] = QuestionSections::all();
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
        $this->vars['sortColumnBy'] = "desc";
    }

    public function getQuestions()
    {
        $fields = [];
        $questions = $this->model->getCompanyQuestions();
        foreach($questions as $key => $question){
            $fields[$question->companyquestion_id] = $question->cq_text;
        }
        return $fields;
    }

    public function getAnswerByQuestionId($questionId)
    {
        $answerToReturn = null;
        foreach($this->model->answers as $answer){
            if($answer->companyquestion_id == $questionId){
                $answerToReturn = $answer;
                break;
            }
        }
        return $answerToReturn;
    }

    public function getAnswerId($qid)
    {
        $answers = $this->model->answers;
        foreach($answers as $key => $value){
            if(isset($value->companyanswer_id) && $value->companyquestion_id == $qid){
                return $value->companyanswer_id;
            }
        }
        return $this->addNewAnswer($qid);
    }

    public function getAnswerText($qid)
    {
        $answers = $this->model->answers;
        foreach($answers as $key => $value){
            if(isset($value->ca_text) && $value->companyquestion_id == $qid){
                return $value->ca_text;
            }
        }
        return '';
    }
}
