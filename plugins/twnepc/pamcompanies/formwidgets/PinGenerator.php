<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Twnepc\User\Models\Pins;
/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class PinGenerator extends FormWidgetBase
{

    protected $userId;

    public function init()
    {

    }

    public function prepareVars()
    {
        $this->userId = $this->model->user_id;
        $activePIN = Pins::where('user_id',$this->userId)->first();
        if($activePIN)
                $this->vars['pin'] = $activePIN->code;
        else
            $this->vars['pin'] = "------";
    }

    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('index');
    }

    public function onGeneratePIN()
    {
        $this->userId = $this->model->user_id;
        $activePIN = Pins::where('user_id',$this->userId)->first();
        if($activePIN)
                $activePIN->delete();

        $activePIN = new Pins;
        $activePIN->code = $activePIN->generatePIN();
        $activePIN->user_id = $this->userId;
        $activePIN->save();

        $this->vars['pin'] = $activePIN->code;

        return [
            '#pin-generator' => $this->makePartial('index', $this->vars)
        ];
    }
}