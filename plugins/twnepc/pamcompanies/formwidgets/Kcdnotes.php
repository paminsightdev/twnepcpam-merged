<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyKcdNotes as Note;
use Exception;
use Input;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Kcdnotes extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsKcdnotes";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    public function init()
    {
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = $this->model->kcdnotes;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    public function onLoadAddKcdnote() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companykcdnotes/add_fields.yaml');
            $config->model = new Note;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

	public function onLoadUpdateKcdnote() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $kcdnote_id = post('kcdnote_id', 0);
            if (!$item = Note::find($kcdnote_id))
                throw new Exception('Record not found.');

            $this->vars['kcdnote_id'] = $kcdnote_id;
            $this->vars['type'] = $type = $item->master_object_class;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companykcdnotes/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdateKcdnote() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        if (!isset($post['kcdnote_id'])) {
               throw new Exception('ID must be specified.');
        }
        $kcdnote_id = intval($post['kcdnote_id']);

        $note = Note::where('companykcdn_id',$kcdnote_id)->first();

        if (!$note)
                throw new Exception('Record not found.');
        $note->fill($post);
        $note->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Note::where('company_id', post('company_id'))->orderBy('ckcdn_orderno', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }
    
    
    /**
     * we will save the KCDNotes data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }

    
    public function onAddKcdnote() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $note = new Note;

        $order = Note::where('company_id',post('company_id'))->count();
        $order++;
        $post['ckcdn_orderno'] = $order;
        $note->fill($post);
        if($note->validate())
            $note->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Note::where('company_id', post('company_id'))->orderBy('ckcdn_orderno', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemoveKcdnote() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Note::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Note::where('company_id', post('company_id'))->orderBy('ckcdn_orderno', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onKcdnotesSorted()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $newOrder = post('ckcdn_orderno');
        foreach($newOrder as $id => $val){
            $item = Note::where('companykcdn_id',$id)->first();
            $item->ckcdn_orderno = $val;
            $item->save();
        }
        return  true;
    }
}
