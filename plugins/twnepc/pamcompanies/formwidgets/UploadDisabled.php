<?php

namespace Twnepc\Pamcompanies\FormWidgets;

use Backend\Classes\FormWidgetBase;
use BackendAuth;

/**
 *
 *
 */
class UploadDisabled extends FormWidgetBase
{
    protected $defaultAlias = 'disabled';
    protected $key = null;

    public function init()
    {

    }

    public function prepareVars()
    {
        $value = $this->getLoadValue();
        if($value && $value != ""){
            $this->vars['value'] = $value;
        }else{
            if(isset($this->formField->config['no_image'])){
                $this->vars['value'] = $this->formField->config['no_image'];
            }
        }
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial('field');
    }

}
