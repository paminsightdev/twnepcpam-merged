<?php
namespace Twnepc\Pamcompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Exception;
use Input;
use ApplicationException;
use Hash;
use BackendAuth;
use Flash;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use DB;
use Event;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class PamsBackendAccess extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    public $alias = 'index';

    public function init()
    {
        $this->getUser();
    }

    public function prepareVars()
    {
        if(CompanySettings::get('pams_frontend_groups',false) == false){
            throw new ApplicationException("Settings not set.");
        }

        $this->vars['isPams'] = false;
        $this->vars['hasBackendAccess'] = false;
        foreach($this->model->roles as $role){
            if($role->id == CompanySettings::get('pams_frontend_groups')){
                $this->vars['isPams'] = true;
                break;
            }
        }
        if($this->vars['isPams']){
            if($this->model->backenduser){
                $this->vars['hasBackendAccess'] = true;
            }
        }
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial($this->alias);
    }

    public function onActivateBackendUser()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();

        if(CompanySettings::get('pams_administrator_group',false) == false)
            throw new ApplicationException("Settings not set.");

        $user = $this->model;

        if(!$user->member)
            throw new ApplicationException("User don't have member relation.");

        $password = Hash::make($user->email);

        $backendUser = BackendAuth::findUserByLogin($user->email);

        if(!$backendUser){
            $backendUser = BackendAuth::register(array(
                'login' => $user->email,
                'email' => $user->email,
                'password' => $password,
                'password_confirmation' => $password
            ),true);
            $backendGroupId = CompanySettings::get('pams_administrator_group');
        }

        try{
            Db::table('backend_users_groups')->insert(
                ['user_id' => $backendUser->id, 'user_group_id' => $backendGroupId]
            );
        }catch(Exception $e){

        }

        $user->resetValidationRules();
        $user->backenduser_id = $backendUser->id;
        $user->save();
        $user->member->activated = 1;
        $user->member->save();

        Event::fire('pamonline.pams.activate', [$user]);

        Flash::success("Update successful");
        $this->prepareVars();
        return [
            '#pamsBackendAccess' => $this->makePartial($this->alias, $this->vars)
        ];
    }

    public function onDisableBackendUser()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $user = $this->model;

        if(!$user->member)
            throw new ApplicationException("User don't have member relation.");

        $user->member->activated = 0;
        $user->member->save();
        $backenduser = BackendAuth::findUserById($user->backenduser->id);
        if($backenduser){
            $backenduser->delete();
            $backenduser->afterDelete();
        }
        $user->backenduser = null;
        $user->resetValidationRules();
        $user->backenduser_id = null;
        $user->save();

        Event::fire('pamonline.pams.deactivate', [$user]);

        Flash::success("Update successful");
        $this->prepareVars();
        return [
            '#pamsBackendAccess' => $this->makePartial($this->alias, $this->vars)
        ];
    }
}