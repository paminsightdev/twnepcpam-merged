<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyPersonnelSection as Section;
use Twnepc\PamCompanies\Models\CompanyPersonnel as Personnel;
use Exception;
use Input;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class PersonnelSections extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsPersonnelSections";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'PersonnelSections';

    public function init()
    {
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = $this->model->personnelsections;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        $script = (count($this->vars['records'])>= 3)?"<script>$(document).ready(function() {\$('#addPersonnelSectionBtn').hide();});</script>":"";
        return $script.$this->makePartial('options');
    }

    public function onLoadAddSection() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $this->prepareVars();
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnelsection/add_fields.yaml');
            $config->model = new Section;
            $config->model->company_id = post('company_id', 0);
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

	public function onLoadUpdateSection() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $id = post('cpsection_id', 0);
            if (!$item = Section::find($id))
                throw new Exception('Record not found.');

            $this->vars['cpsection_id'] = $id;
            $this->vars['type'] = $type = $item->master_object_class;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnelsection/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdateSection() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        if (!isset($post['cpsection_id'])) {
               throw new Exception('ID must be specified.');
        }
        $id = intval($post['cpsection_id']);

        $product = Section::where('cpsection_id',$id)->first();

        if (!$product)
                throw new Exception('Record not found.');

        $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnelsection/update_fields.yaml');
        $config->model = $product;
        $config->context = 'update';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $product->fill($post);
        $product->save(null, $formWidget->getSessionKey());

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Section::where('company_id', post('company_id'))->orderBy('cps_order', 'asc')->get();
        $this->vars['records2'] = Personnel::where('company_id', post('company_id'))->orderBy('cp_order', 'asc')->get();
        $this->vars['sortColumnBy'] = "desc";
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']]),
            '#reorderRecordsPersonnel' => $this->makePartial('item_records2', ['records' => $this->vars['records2']])
        ];
    }

    
    /**
     * we will save the Personnel Section data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
    
    
    public function onAddSection() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $item = new Section;

        $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnelsection/add_fields.yaml');
        $config->model = $item;
        $config->context = 'create';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $order = Section::where('company_id',post('company_id'))->count();
        $order++;
        $post['cps_order'] = $order;
        $item->fill($post);
        if($item->validate())
            $item->save(null, $formWidget->getSessionKey());

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Section::where('company_id', post('company_id'))->orderBy('cps_order', 'asc')->get();
        $script = (count($this->vars['records'])>= 3)?"<script>$('#addPersonnelSectionBtn').hide();</script>":"<script>$('#addPersonnelSectionBtn').show();</script>";
        
        return [
            '#'.$this->listRecordsElementId => $script.$this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemoveSection() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Section::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Section::where('company_id', post('company_id'))->orderBy('cps_order', 'asc')->get();
        $script = (count($this->vars['records'])>= 5)?"<script>$('#addPersonnelSectionBtn').hide();</script>":"<script>$('#addPersonnelSectionBtn').show();</script>";
         
        return [
            '#'.$this->listRecordsElementId => $script.$this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }


    public function onSectionSorted()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $newOrder = post('cps_order');
        foreach($newOrder as $id => $val){
            $item = Section::where('cpsection_id',$id)->first();
            $item->cps_order = $val;
            $item->save();
        }
        return  true;
    }

}
