<?php

namespace Twnepc\Pamcompanies\FormWidgets;

use Backend\Classes\FormWidgetBase;
use BackendAuth;

/**
 *
 *
 */
class DisabledDropdown extends FormWidgetBase
{
    protected $defaultAlias = 'disabled';
    protected $key = null;

    public function init()
    {

    }

    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['label'] = function(){
            $options = $this->model->{$this->formField->options}();
            foreach($options as $key => $value){
                if($this->vars['value'] == $key){
                    return $value;
                }
            }
        };
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial('field');
    }

}
