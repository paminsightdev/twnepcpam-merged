<?php

namespace Twnepc\Pamcompanies\FormWidgets;

use Backend\Classes\FormWidgetBase;
use BackendAuth;

/**
 *
 *
 */
class DropdownDepends extends FormWidgetBase
{
    public $alias  = 'dropdownDepends';

    /**
     * {@inheritDoc}
     */
    public function init()
    {
        parent::init();
    }

    public function prepareVars()
    {
        $this->vars['dependsOnField'] = $this->formField->config['dependsOnField'];
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $dependFieldOptions = $this->model->{$this->formField->config['dependsOnOptions']}();
        $this->vars['options'] = [];
        $this->vars['id'] = $this->formField->getId();

        foreach($dependFieldOptions as $key => $value){
            $this->vars['options'][$key] = array();
        }

        foreach($this->vars['options'] as $key => $value){
            $this->vars['options'][$key] = $this->model->{$this->formField->options}($key);
        }
    }

    public function render() {
        $this->prepareVars();
        return $this->makePartial('field');
    }
}
