<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyCommentary as Commentary;
use Twnepc\PamCompanies\Models\Companies as CompaniesModel;
use Exception;
use Input;
use BackendAuth;
/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Articles extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsArticles";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';
    protected $contenttype;
    private $formWidget;

    public function init()
    {
        $this->fillFromConfig([
            'contenttype',
        ]);
        $this->listRecordsElementId = $this->listRecordsElementId."_".$this->contenttype;
        if(Request::header('X-OCTOBER-FILEUPLOAD')){
            if($articleID = post('article_id',false)){
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companycommentary/update_fields.yaml');
                $config->model = Commentary::find($articleID);
                $config->context = "update";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
            else if(post("cc_name",false) !== false){
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companycommentary/add_fields.yaml');
                $config->model = new Commentary();
                $config->context = "create";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
        }
        if(Request::header('X-OCTOBER-REQUEST-HANDLER')){
            if($articleID = post('article_id',false)){
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companycommentary/update_fields.yaml');
                $config->model = Commentary::find($articleID);
                $config->context = "update";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
        }
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = Commentary::where('company_id',$id)->where('cc_contenttype',$this->contenttype)->orderBy('cc_date','desc')->get();
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
        $this->vars['contenttype'] = $this->contenttype;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    public function onLoadAddArticle() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companycommentary/add_fields.yaml');
            $config->model = new Commentary;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

	public function onLoadUpdateArticle() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $article_id = post('article_id', 0);
            if (!$item = Commentary::find($article_id))
                throw new Exception('Record not found.');
            $this->vars['article_id'] = $article_id;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companycommentary/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdateArticle() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();

        $post = post();
        if (!isset($post['article_id'])) {
               throw new Exception('ID must be specified.');
        }
        $article_id = intval($post['article_id']);

        $article = Commentary::where('companycomment_id',$article_id)->first();

        if (!$article)
                throw new Exception('Record not found.');

        $config = $this->makeConfig('$/twnepc/pamcompanies/models/companycommentary/update_fields.yaml');
        $config->model = $article;
        $config->context = 'update';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $article->fill($post);
        $article->save([], $formWidget->getSessionKey());

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Commentary::where('company_id', post('company_id'))->where('cc_contenttype',$this->contenttype)->get();
        if ($this->contenttype == 'p') {        
            CompaniesModel::sectionUpdate(post('company_id'), 'publications');
        }  else if ($this->contenttype == 'i') {        
            CompaniesModel::sectionUpdate(post('company_id'), 'investment_commentary');
        } else if ($this->contenttype == 'a') {
            CompaniesModel::sectionUpdate(post('company_id'), 'articles');
        } else if ($this->contenttype == 'n') {
            CompaniesModel::sectionUpdate(post('company_id'), 'news');
        }   
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', $this->vars['records'])
        ];
    }

    
    /**
     * we will save the Commentary data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
    
    
    public function onAddArticle() {
        if($this->isPAMUser())
                $this->user->frontenduser->checkPIN();

        $post = post();
        $article = new Commentary;

        $config = $this->makeConfig('$/twnepc/pamcompanies/models/companycommentary/add_fields.yaml');
        $config->model = $article;
        $config->context = 'create';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $post['cc_contenttype'] = $this->contenttype;
        $article->fill($post);
        if($article->validate())
            $article->save([], $formWidget->getSessionKey());
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Commentary::where('company_id', post('company_id'))->where('cc_contenttype',$this->contenttype)->get();
        if ($this->contenttype == 'p') {        
            CompaniesModel::sectionUpdate(post('company_id'), 'publications');
        }  else if ($this->contenttype == 'i') {        
            CompaniesModel::sectionUpdate(post('company_id'), 'investment_commentary');
        } else if ($this->contenttype == 'a') {
            CompaniesModel::sectionUpdate(post('company_id'), 'articles');
        } else if ($this->contenttype == 'n') {
            CompaniesModel::sectionUpdate(post('company_id'), 'news');
        }
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records',  $this->vars['records'])
        ];
    }

    public function onRemoveArticle() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();

        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Commentary::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Commentary::where('company_id', post('company_id'))->where('cc_contenttype',$this->contenttype)->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', $this->vars['records'])
        ];
    }
}
