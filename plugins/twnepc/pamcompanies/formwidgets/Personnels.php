<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyPersonnel as Personnel;
use Exception;
use Input;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Personnels extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsPersonnel";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'Personnels';

    public function init()
    {
        if(Request::header('X-OCTOBER-FILEUPLOAD')){
            if($personnelID = post('personnel_id',false)){
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnel/update_fields.yaml');
                $config->model = Personnel::find($personnelID);
                $config->context = "update";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
        }
        if(Request::header('X-OCTOBER-REQUEST-HANDLER')){
            if($personnelID = post('personnel_id',false)){
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnel/update_fields.yaml');
                $config->model = Personnel::find($personnelID);
                $config->context = "update";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
        }
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = $this->model->personnel;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['sortColumnBy'] = "desc";
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    public function onLoadAddPersonnel() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $this->prepareVars();
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnel/add_fields.yaml');
            $config->model = new Personnel;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

	public function onLoadUpdatePersonnel() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $id = post('personnel_id', 0);
            if (!$item = Personnel::find($id))
                throw new Exception('Record not found.');

            $this->vars['personnel_id'] = $id;
            $this->vars['type'] = $type = $item->master_object_class;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnel/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdatePersonnel() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        if (!isset($post['personnel_id'])) {
               throw new Exception('ID must be specified.');
        }
        $id = intval($post['personnel_id']);

        $product = Personnel::where('companypersonnel_id',$id)->first();

        if (!$product)
                throw new Exception('Record not found.');

        $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnel/update_fields.yaml');
        $config->model = $product;
        $config->context = 'update';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $product->fill($post);
        $product->save(null, $formWidget->getSessionKey());

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Personnel::where('company_id', post('company_id'))->orderBy('cp_order', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    
    /**
     * we will save the Personnel data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
    
    
    public function onAddPersonnel() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $item = new Personnel;

        $config = $this->makeConfig('$/twnepc/pamcompanies/models/companypersonnel/add_fields.yaml');
        $config->model = $item;
        $config->context = 'create';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $order = Personnel::where('company_id',post('company_id'))->count();
        $order++;
        $post['cp_order'] = $order;
        $item->fill($post);
        if($item->validate())
            $item->save(null, $formWidget->getSessionKey());

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Personnel::where('company_id', post('company_id'))->orderBy('cp_order', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemovePersonnel() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Personnel::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Personnel::where('company_id', post('company_id'))->orderBy('cp_order', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onSortingByColumn()
    {
        $column = post("sortColumn");
        $sortedBy = post("sortColumnBy",'desc');
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['sortColumnBy'] = $sortedBy == "desc"?"asc":"desc";
        $this->vars['records'] = Personnel::where('company_id', post('company_id'))->orderBy($column, $sortedBy)->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onPersonnelSorted()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $newOrder = post('cp_order');
        foreach($newOrder as $id => $val){
            $item = Personnel::where('companypersonnel_id',$id)->first();
            $item->cp_order = $val;
            $item->save();
        }
        return  true;
    }

}
