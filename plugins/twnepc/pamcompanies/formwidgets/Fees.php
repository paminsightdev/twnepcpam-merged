<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyFee as Fee;
use Exception;
use Input;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Fees extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsFees";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    public function init()
    {
        if(Request::header('X-OCTOBER-FILEUPLOAD')){
            if($feeID = post('fee_id',false)){
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyfee/update_fields.yaml');
                $config->model = Fee::find($feeID);
                $config->context = "update";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
            else if(post("cf_charge",false) !== false){
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyfee/add_fields.yaml');
                $config->model = new Fee();
                $config->context = "create";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
        }
        if(Request::header('X-OCTOBER-REQUEST-HANDLER')){
            if($feeID = post('fee_id',false)){
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyfee/update_fields.yaml');
                $config->model = Fee::find($feeID);
                $config->context = "update";
                $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);
                $formWidget->render();
            }
        }
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = $this->model->fees;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    public function onFormRefresh()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyfee/add_fields.yaml');
            $config->model = new Fee;

            $config->model->fill(post());

            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

    public function onLoadAddFee() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyfee/add_fields.yaml');
            $config->model = new Fee;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

    public function onLoadUpdateFee() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $fee_id = post('fee_id', 0);
            if (!$item = Fee::find($fee_id))
                throw new Exception('Record not found.');

            $this->vars['fee_id'] = $fee_id;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyfee/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdateFee() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        if (!isset($post['fee_id'])) {
            throw new Exception('ID must be specified.');
        }
        $fee_id = intval($post['fee_id']);

        $fee = Fee::where('companyfee_id',$fee_id)->first();

        if (!$fee)
            throw new Exception('Record not found.');

        $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyfee/update_fields.yaml');
        $config->model = $fee;
        $config->context = 'update';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $fee->fill($post);
        $sessionKey = $formWidget->getSessionKey();
        if($fee->validate())
            $fee->save([], $sessionKey);

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Fee::where('company_id', post('company_id'))->orderBy('cf_position',"asc")->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }
    
    
    /**
     * we will save the Fee data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
    
    
    public function onAddFee() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $fee = new Fee;

        $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyfee/add_fields.yaml');
        $config->model = $fee;
        $config->context = 'create';
        $formWidget = $this->makeWidget('Backend\Widgets\Form', $config);

        $fee->fill($post);
        if($fee->validate())
            $fee->save(null, $formWidget->getSessionKey());

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Fee::where('company_id', post('company_id'))->orderBy('cf_position',"asc")->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onFeesSorted()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $newOrder = post('cf_position');
        foreach($newOrder as $id => $val){
            $item = Fee::where('companyfee_id',$id)->first();
            $item->cf_position = $val;
            $item->save();
        }
        return  true;
    }


    public function onRemoveFee() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Fee::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Fee::where('company_id', post('company_id'))->orderBy('cf_position',"asc")->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

}
