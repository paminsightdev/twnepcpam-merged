<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyKcdNotes as Note;
use Exception;
use Input;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class KcdNotesPreview extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsKcdnotes";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    public function init()
    {
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['notes'] = $this->model->kcdnotes;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('view');
    }

   

}
