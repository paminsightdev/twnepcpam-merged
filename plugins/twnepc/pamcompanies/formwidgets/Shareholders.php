<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Twnepc\PamCompanies\Controllers\Companies;
use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyShareholder as Shareholder;
use Exception;
use Input;
use ValidationException;
use Twnepc\PamCompanies\Models\Settings;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Shareholders extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsShareholders";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    public function init()
    {
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = $this->model->shareholders;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    public function onLoadAddShareholder() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyshareholder/add_fields.yaml');
            $config->model = new Shareholder;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

	public function onLoadUpdateShareholder() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $id = post('id', 0);
            if (!$item = Shareholder::find($id))
                throw new Exception('Record not found.');

            $this->vars['id'] = $id;
            $this->vars['type'] = $type = $item->master_object_class;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyshareholder/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdateShareholder() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        if (!isset($post['id'])) {
               throw new Exception('ID must be specified.');
        }
        $id = intval($post['id']);

        $item = Shareholder::where('companysh_id',$id)->first();

        $shareholders = Shareholder::where('company_id',post('company_id'))->get();

        $currentPercent = 0;

        foreach ($shareholders as $s) {
            $currentPercent += $s->csh_share;
        }

        $currentPercent += floatval(post('csh_share'));

        if(floatval(post('csh_share')) < 1.0)
        {
            throw new ValidationException(['csh_share'     => Settings::get('main_shareholders_five_text')]);
        }

        if($currentPercent > 100.0)
        {
            throw new ValidationException(['csh_share'     => Settings::get('main_shareholders_hundred_text')]);
        }

        if (!$item)
                throw new Exception('Record not found.');
        $item->fill($post);
        if($item->validate())
                $item->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Shareholder::where('company_id', post('company_id'))->orderBy('csh_orderno','asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }
    
    
    /**
     * we will save the Shareholder data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
    
    
    public function onAddShareholder() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $item = new Shareholder;

        $order = Shareholder::where('company_id',post('company_id'))->count();
        
        $shareholders = Shareholder::where('company_id',post('company_id'))->get();

        $currentPercent = 0;

        foreach ($shareholders as $s) {
            $currentPercent += $s->csh_share;
        }

        $currentPercent += floatval(post('csh_share'));

        if(floatval(post('csh_share')) < 5.0)
        {
            throw new ValidationException(['csh_share'     => Settings::get('main_shareholders_five_text')]);
        }

        if($currentPercent > 100.0)
        {
            throw new ValidationException(['csh_share'     => Settings::get('main_shareholders_hundred_text')]);
        }

        //

        $order++;
        $post['csh_orderno'] = $order;



        $item->fill($post);
        if($item->validate())
                $item->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Shareholder::where('company_id', post('company_id'))->orderBy('csh_orderno','asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemoveShareholder() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Shareholder::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['id_quest'] = post('id');
        $this->vars['records'] = Shareholder::where('company_id', post('company_id'))->orderBy('csh_orderno','asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onShareholderSorted()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $newOrder = post('csh_orderno');
        foreach($newOrder as $id => $val){
            $item = Shareholder::where('companysh_id',$id)->first();
            $item->csh_orderno = $val;
            $item->save();
        }
        return  true;
    }

    protected function loadAssets()
    {
        $this->addCss('css/sortable.css', 'core');
    }
}
