<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Twnepc\PamCompanies\Controllers\Companies;
use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\Office;
use Twnepc\PamCompanies\Models\Companies as Company;
use Rainlab\User\Models\User as UserModel;
use Exception;
use Input;
use Validator;
use ValidationException;
use Twnepc\PamCompanies\Models\Settings;
use DB;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Offices extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsOffices";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    public function init()
    {
        $this->getUser();
    }


    function get_timezone($latitude,$longitude,$username) {
    

        //error checking
        if (!is_numeric($latitude)) { custom_die('A numeric latitude is required.'); }
        if (!is_numeric($longitude)) { custom_die('A numeric longitude is required.'); }
        if (!$username) { custom_die('A GeoNames user account is required. You can get one here: http://www.geonames.org/login'); }

        //connect to web service
        $url = 'http://ws.geonames.org/timezone?lat='.$latitude.'&lng='.$longitude.'&style=full&username='.urlencode($username);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $xml = curl_exec($ch);
        curl_close($ch);

        if (!$xml) { $GLOBALS['error'] = 'The GeoNames service did not return any data: '.$url; return false; }

        //parse XML response
        $data = new \SimpleXMLElement($xml);
        //var_dump($data);
        //echo '<pre>'.print_r($data,true).'</pre>'; die();
        $timezone = trim(strip_tags($data->timezone->timezoneId));
        if ($timezone) { return $timezone; }
        else { $GLOBALS['error'] = 'The GeoNames service did not return a time zone: '.$url; return false; }

    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = $this->model->offices;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    public function onLoadAddOffice() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/office/add_fields.yaml');
            $config->model = new Office;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

	public function onLoadUpdateOffice() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $id = post('id', 0);
            if (!$item = Office::find($id))
                throw new Exception('Record not found.');

            $this->vars['id'] = $id;
            $this->vars['type'] = $type = $item->master_object_class;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/office/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdateOffice() {

        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        
       if (!empty($post['o_postcode'])) {

           if (!empty($post['country'])) {
               $country = \Db::table('pamtbl_country')->where(["country_id"=>intval($post['country'])])->first();
               if ($country) {
                   $country = $country->country_name;
               }
           } else {
               $country = "UK";
           }
           $point = UserModel::checkPostcode($post['o_postcode'],$country);

           if ($point === false) {
               throw new ValidationException(
                   ['o_postcode'     =>  'Incorrect postcode!']
               );
           }

           $post['o_point_x'] = $point["lat"];
           $post['o_point_y'] = $point["long"];
       } else {
           throw new ValidationException(['o_postcode'     =>  'Postcode must be specified.']);
       }

        if(isset($post['o_point_x']) && isset($post['o_point_y']))
        {
            $timezone = NULL;
            $utc_offset = NULL;

            $lat = number_format($post['o_point_x'],4);
            $lon = number_format($post['o_point_y'],4);
            
            $timezone = $this->get_timezone($lat,$lon,'okami');
            

            if($timezone){
                date_default_timezone_set($timezone);
                $utc_offset =  date('Z') / 3600;
                //var_dump('update pamtbl_office set timezone="'.$timezone.'", utc_offset='.$utc_offset.' where office_id='.$o->office_id);
                
                DB::table('pamtbl_office')
                ->where('office_id', $post['id'])
                ->update(['utc_offset' => $utc_offset,'timezone'=>$timezone]);
            }else{
               
            }
        }
        
        if (!isset($post['id'])) {
               throw new Exception('ID must be specified.');
        }


        $id = intval($post['id']);

        $item = Office::where('office_id',$id)->first();

        $officeCounter = null;

        $company = Company::find(post('company_id'));

         $type = $company->c_typefront;

        if($type == 'b')
        {
            $officeCounter = Settings::get('pam_basic_office');
        }
        else if($type == 'e')
        {
            $officeCounter = Settings::get('pam_enchanced_office');
        }
        else if($type == 'p')
        {
            $officeCounter = Settings::get('pam_premier_office');
        }

        
        $order_with_directory_yes = Office::where('company_id',post('company_id'))->where('o_directory',1)->count();

       /* if(intval($order_with_directory_yes) >= intval($officeCounter) && $post['o_directory'] == 1)
        {
            throw new ValidationException(['office'     =>  Settings::get('pam_exceeded_message').'&nbsp;'.$officeCounter]);
        }*/

        if (!$item)
                throw new Exception('Record not found.');
        $item->fill($post);
        if($item->validate())
                $item->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Office::where('company_id', post('company_id'))->orderBy('o_ordernouk','asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }
    
    
    /**
     * we will save the Office data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
    
    
    public function onAddOffice() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $item = new Office;

        $order = Office::where('company_id',post('company_id'))->count();

        $company = Company::find(post('company_id'));

        $type = $company->c_typefront;

        $officeCounter = null;

        

        if($type == 'b')
        {
            $officeCounter = Settings::get('pam_basic_office');
        }
        else if($type == 'e')
        {
            $officeCounter = Settings::get('pam_enchanced_office');
        }
        else if($type == 'p')
        {
            $officeCounter = Settings::get('pam_premier_office');
        }

        
        $order_with_directory_yes = Office::where('company_id',post('company_id'))->where('o_directory',1)->count();
        

        if(intval($order_with_directory_yes) >= intval($officeCounter) && $post['o_directory'] == 1)
        {
            
            throw new ValidationException(['office'     =>  Settings::get('pam_exceeded_message').'&nbsp;'.$officeCounter]);
        }

        $order++;
        $post['o_ordernouk'] = $order;
       
       //LAT LON 

       if (!empty($post['o_postcode'])) {

           if (!empty($post['country'])) {
               $country = \Db::table('pamtbl_country')->where(["country_id"=>intval($post['country'])])->first();
               if ($country) {
                   $country = $country->country_name;
               }
           } else {
               $country = "UK";
           }

           $point = UserModel::checkPostcode($post['o_postcode'],$country);

           if ($point === false) {
               throw new ValidationException(
                   ['o_postcode'     =>  'Incorrect postcode!']
               );
           }

           $post['o_point_x'] = $point["lat"];
           $post['o_point_y'] = $point["long"];
       } else {
           throw new ValidationException(['o_postcode'     =>  'Postcode must be specified.']);
       }



        if(isset($post['o_point_x']) && isset($post['o_point_y']))
        {
            $timezone = NULL;
            $utc_offset = NULL;

            $lat = number_format($post['o_point_x'],4);
            $lon = number_format($post['o_point_y'],4);
            
            $timezone = $this->get_timezone($lat,$lon,'okami');
            


            if($timezone){
                date_default_timezone_set($timezone);
                $utc_offset =  date('Z') / 3600;
                

                $post['utc_offset'] = $utc_offset;
                $post['timezone'] = $timezone;
            }else{
               
            }
        }


       //LAT LON
        
        $item->fill($post);
        if($item->validate())
                $item->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Office::where('company_id', post('company_id'))->orderBy('o_ordernouk','asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemoveOffice() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Office::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['id_quest'] = post('id');
        $this->vars['records'] = Office::where('company_id', post('company_id'))->orderBy('o_ordernouk','asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onOfficeSorted()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $newOrder = post('o_ordernouk');
        foreach($newOrder as $id => $val){
            $item = Office::where('office_id',$id)->first();
            $item->o_ordernouk = $val;
            $item->save();
        }
        return  true;
    }

    protected function loadAssets()
    {
        $this->addCss('css/sortable.css', 'core');
        $this->addCss('//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css', 'jquery-css');
        $this->addJs('https://code.jquery.com/ui/1.12.0/jquery-ui.js', 'jquery-js');
        $this->addJs('js/jquery.maskedinput.min.js');
    }
}
