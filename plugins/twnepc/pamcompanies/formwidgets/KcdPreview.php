<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyKcd;
use Exception;
use Input;
use Validator;
use ValidationException;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class KcdPreview extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsKcd";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    public function init()
    {
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $collection = $this->model->kcd;
        $this->vars['records'] = $collection->sortBy('ckcd_year');
        $this->vars['bodies'] = $this->model->registeredbodies;
        $this->vars['auditor'] = $this->model->c_auditor;
        $this->vars['fin_year'] = $this->model->c_auditor;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }
    
}
