<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Twnepc\PamCompanies\Controllers\Companies;
use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyProduct as Product;
use Exception;
use Input;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Products extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsProducts";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    public function init()
    {
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = $this->model->products;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    public function onLoadAddProduct() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyproduct/add_fields.yaml');
            $config->model = new Product;
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }

	public function onLoadUpdateProduct() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $id = post('id', 0);
            if (!$item = Product::find($id))
                throw new Exception('Record not found.');

            $this->vars['id'] = $id;
            $this->vars['type'] = $type = $item->master_object_class;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyproduct/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }

    public function onUpdateProduct() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        if (!isset($post['id'])) {
               throw new Exception('ID must be specified.');
        }
        $id = intval($post['id']);

        $product = Product::where('productcompany_id',$id)->first();

        if (!$product)
                throw new Exception('Record not found.');
        $product->fill($post);
        $product->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Product::where('company_id', post('company_id'))->orderBy('pc_orderno', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    
    /**
     * we will save the Product data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
    
    
    public function onAddProduct() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $item = new Product;

        $order = Product::where('company_id',post('company_id'))->count();
        $order++;
        $post['pc_orderno'] = $order;
        $item->fill($post);
        if($item->validate())
            $item->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Product::where('company_id', post('company_id'))->orderBy('pc_orderno', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemoveProduct() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            Product::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = Product::where('company_id', post('company_id'))->orderBy('pc_orderno', 'asc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onProductsSorted()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $newOrder = post('pc_orderno');
        foreach($newOrder as $id => $val){
            $item = Product::where('productcompany_id',$id)->first();
            $item->pc_orderno = $val;
            $item->save();
        }
        return  true;
    }
}
