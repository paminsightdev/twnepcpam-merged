<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Twnepc\PamCompanies\Controllers\Companies;
use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanySignoff as Signoff;
use Exception;
use Input;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Progress extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsProgress";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    public function init()
    {
        $this->getUser();
    }

    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;


         $disabled = array();

            switch($this->model->c_typeback){
                case "p":
                    $disabled = array();
                    break;
                case "e":
                    $disabled = array('history','questions_and_answers','articles_and_press_cuttings','publications','news');
                    break;
                default:
                    $disabled = array('about_us','history','investment_philosophy','questions_and_answers','products_and_services','main_shareholders','articles_and_press_cuttings','publications','news','investment_commentary');
        }
        $this->vars['disabled'] = $disabled;
        $this->vars['records'] = Signoff::where('company_id',$id)->orderBy('pam_modified','DESC')->get();
        $this->vars['company_id'] = $id;
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }

    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }

}