<?php

namespace Twnepc\PamCompanies\FormWidgets;

use Request;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyKcd;
use Exception;
use Input;
use Validator;
use ValidationException;

/**
 * Rich Editor
 * Renders a rich content editor field.
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
class Kcd extends FormWidgetBase {

    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsKcd";

    /**
     * {@inheritDoc}
     */
    public $defaultAlias = 'options';

    
    public function init()
    {
        $this->getUser();
    }
    
    
    /**
     * Prepares the list data
     */
    public function prepareVars() {
        $id = $this->model->company_id;
        $this->vars['records'] = $this->model->kcd;
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
    }
    
    
    /**
     * {@inheritDoc}
     */
    public function render() {
        $this->prepareVars();
        return $this->makePartial('options');
    }
    
    
    public function canEdit($recordYear)
    {
        if($this->isPAMUser()==false)return true;
        $currentYear =  (int) date("Y");
        $prevYear = $currentYear-1;
        $recordYear = (int) $recordYear;

        if($prevYear == $recordYear)
            return true;

//        foreach($this->vars['records'] as $record){
//            if($recordYear == (int) $record->ckcd_year)
//                return true;
//            break;
//        }
//      used to block all other conditions unless previous year
//      return false;
        
        // bypass logic to verify previous year condition, then and deny all other scenarios
        // now, as long as the user is logged in they can edit any year... without having to be a pam user/staff
        return true;
    }
    
    
    public function onLoadAddKcd() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            if($this->isPAMUser())
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companykcd/pam_add_fields.yaml');
            else
                $config = $this->makeConfig('$/twnepc/pamcompanies/models/companykcd/add_fields.yaml');

            $config->model = new CompanyKcd;
            $config->model->company_id = intval(post('company_id'));
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('create_item');
    }
    
    
    public function onLoadUpdateKcd() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $kcd_id = post('kcd_id', 0);
            if (!$item = CompanyKcd::find($kcd_id))
                throw new Exception('Record not found.');

            $this->vars['kcd_id'] = $kcd_id;
            $this->vars['type'] = $type = $item->master_object_class;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companykcd/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->vars['form'] = $form;
            $this->vars['company_id'] = intval(post('company_id'));
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }

        return $this->makePartial('update_item');
    }
    
    
    public function onUpdateKcd() {
        
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        
        $post = post();
        if (!isset($post['kcd_id'])) {
            throw new Exception('ID must be specified.');
        }
        
        $kcd_id = intval($post['kcd_id']);
        $kcd = CompanyKcd::where('companykcd_id',$kcd_id)->first();

        if (!$kcd)
            throw new Exception('Record not found.');
        
        $kcd->fill($post);
        $kcd->save();

        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = CompanyKcd::where('company_id', post('company_id'))->orderBy('ckcd_year', 'desc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }
    
    
    /**
     * we will save the KCD data separately to the main form
     * @param mixed $value The existing value for this widget.
     * @return string NO_SAVE_DATA - class constant to have the value ignored
     */
    public function getSaveValue($value) {
        return \Backend\Classes\FormField::NO_SAVE_DATA;
    }
    
    
    public function onAddKcd() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $kcd = new CompanyKcd;
        $this->prepareVars();

        foreach($this->vars['records'] as $record){
            if($record->ckcd_year == $post['ckcd_year'])
                throw new ValidationException(['ckcd_year'     =>  'Year already added.']);
        }

        $kcd->fill($post);
        if($kcd->validate())
            $kcd->save();

        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = CompanyKcd::where('company_id', post('company_id'))->orderBy('ckcd_year', 'desc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }

    public function onRemoveKcd() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $ids = post('ids');
        $arr = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $arr[] = intval($id);
            }
            CompanyKcd::destroy($arr);
        }
        $this->prepareVars();
        $this->vars['company_id'] = post('company_id');
        $this->vars['records'] = CompanyKcd::where('company_id', post('company_id'))->orderBy('ckcd_year', 'desc')->get();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('item_records', ['records' => $this->vars['records']])
        ];
    }
}
