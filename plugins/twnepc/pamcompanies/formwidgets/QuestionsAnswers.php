<?php namespace Twnepc\PamCompanies\FormWidgets;

use Backend\Classes\FormField;
use Backend\Classes\FormWidgetBase;
use Twnepc\PamCompanies\Models\CompanyAnswer as Answer;
use Twnepc\PamCompanies\Models\CompanyQuestion as Question;
use Twnepc\PamCompanies\Models\CompanyQuestionSection as QuestionSections;
use Exception;

/**
 * Repeater Form Widget
 */
class QuestionsAnswers extends FormWidgetBase
{
    use \Twnepc\PamCompanies\Classes\PinSystemWidgetTrait;

    private $listRecordsElementId = "reorderRecordsAnswers";

     /**
      * @var bool Stops nested repeaters populating from previous sibling.
      */
    protected static $onAddItemCalled = false;

    public function init()
    {
        $this->getUser();
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        $this->prepareVars();
        return $this->makePartial('options');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $id = $this->model->company_id;
        $this->vars['sections'] = QuestionSections::all();
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;
        $this->vars['sortColumnBy'] = "desc";
    }

    public function getAnswerByQuestionId($questionId)
    {
        $answerToReturn = null;
        foreach($this->model->answers as $answer){
            if($answer->companyquestion_id == $questionId){
                $answerToReturn = $answer;
                break;
            }
        }
        return $answerToReturn;
    }

    public function onLoadUpdateAnswer()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $aid = post('aid', 0);
            if (!$item = Answer::find($aid))
                throw new Exception('Record not found.');

            $this->vars['aid'] = $aid;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyanswer/update_fields.yaml');
            $config->model = $item;
            $config->context = 'update';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->prepareVars();
            $this->vars['form'] = $form;
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }
        return $this->makePartial('update_answer');
    }

    public function onLoadAddQuestion()
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        try {
            $qid = post('qid', 0);
            if (!Question::find($qid))
                throw new Exception('Record not found.');

            $this->vars['qid'] = $qid;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyanswer/add_fields.yaml');
            $config->model = new Answer();
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->prepareVars();
            $this->vars['form'] = $form;
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }
        return $this->makePartial('add_answer');
    }

    public function onLoadAddAnswer()
    {
        try {
            $qid = post('qid', 0);
            if (!Question::find($qid))
                throw new Exception('Record not found.');

            $this->vars['qid'] = $qid;
            $config = $this->makeConfig('$/twnepc/pamcompanies/models/companyanswer/add_fields.yaml');
            $config->model = new Answer();
            $config->context = 'create';
            $form = $this->makeWidget('Backend\Widgets\Form', $config);
            $this->prepareVars();
            $this->vars['form'] = $form;
        } catch (Exception $ex) {
            $this->vars['fatalError'] = $ex->getMessage();
        }
        return $this->makePartial('add_answer');
    }

    public function onAddAnswer() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        $answer = new Answer;

        $answer->fill($post);
        if($answer->validate())
            $answer->save();

        $this->prepareVars();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('options', ['sections' => $this->vars['sections']])
        ];
    }

    public function onUpdateAnswer() {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $post = post();
        if (!isset($post['aid'])) {
            throw new Exception('ID must be specified.');
        }
        $id = intval($post['aid']);

        $item = Answer::where('companyanswer_id',$id)->first();

        if (!$item)
            throw new Exception('Record not found.');
        $item->fill($post);
        if($item->validate())
            $item->save();

        $this->prepareVars();
        return [
            '#'.$this->listRecordsElementId => $this->makePartial('options', ['sections' => $this->vars['sections']])
        ];
    }

    public function getQuestions()
    {
        $fields = [];
        $questions = $this->model->getCompanyQuestions();
        foreach($questions as $key => $question){
            $fields[$question->companyquestion_id] = $question->cq_text;
        }
        return $fields;
    }

    public function getAnswerId($qid)
    {
        $answers = $this->model->answers;
        foreach($answers as $key => $value){
            if(isset($value->companyanswer_id) && $value->companyquestion_id == $qid){
                return $value->companyanswer_id;
            }
        }
        return $this->addNewAnswer($qid);
    }

    public function getAnswerText($qid)
    {
        $answers = $this->model->answers;
        foreach($answers as $key => $value){
            if(isset($value->ca_text) && $value->companyquestion_id == $qid){
                return $value->ca_text;
            }
        }
        return '';
    }

    private function addNewAnswer($qid)
    {
        if($this->isPAMUser())
            $this->user->frontenduser->checkPIN();
        $answer = new CompanyAnswer();
        $answer->company_id = $this->model->company_id;
        $answer->companyquestion_id = $qid;
        $answer->companypersonnel_id = 1;
        $answer->pam_userid = 0;
        $answer->save();
        return $answer->companyanswer_id;
    }

    public function onSortingByColumn()
    {
        $column = post("sortColumn");
        $sortedBy = post("sortColumnBy",'desc');

        $this->vars['sortColumnBy'] = $sortedBy == "desc"?"asc":"desc";

        $id = $this->model->company_id;
        $this->vars['sections'] = QuestionSections::orderBy($column, $sortedBy)->get();
        $this->vars['company_id'] = $id;
        $this->vars['name'] = $this->formField->getName();
        $this->vars['listRecordsElementId'] = $this->listRecordsElementId;

        return [
            '#'.$this->listRecordsElementId => $this->makePartial('options', ['sections' => $this->vars['sections']])
        ];
    }

    protected function loadAssets()
    {
        $this->addCss('css/questionsanswers.css', 'core');
    }
}
