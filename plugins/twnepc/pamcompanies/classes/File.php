<?php namespace Twnepc\Pamcompanies\Classes;

use Config;
use Request;
use System\Models\File as SystemFile;

/**
 * File attachment model
 *
 * @package october\system
 * @author Alexey Bobkov, Samuel Georges
 */
class File extends SystemFile
{
    /**
     * @var string The database table used by the model.
     */
    protected $table = 'system_files';

    /**
     * If working with local storage, determine the absolute local path.
     */
    protected function getLocalRootPath()
    {
        return Config::get('filesystems.disks.local.root', storage_path().'/app');
    }

    /**
     * Define the public address for the storage path.
     */
    public function getPublicPath()
    {
        $uploadsPath = Config::get('cms.storage.uploads.path', '/storage/app/uploads');

        if (!preg_match("/(\/\/|http|https)/", $uploadsPath)) {
            $uploadsPath = Request::getBasePath() . $uploadsPath;
        }

        if ($this->isPublic()) {
            return $uploadsPath . '/public/';
        }
        else {
            return $uploadsPath . '/protected/';
        }
    }

    public function getPath()
    {
        if(!$this->isPublic())
            return url()  . "/files/get/" . $this->disk_name;

        return $this->getPublicPath() . $this->getPartitionDirectory() . $this->disk_name;
    }
    
    /**
     * Define the internal storage path.
     */
    public function getStorageDirectory()
    {
        $uploadsFolder = Config::get('cms.storage.uploads.folder');

        if ($this->isPublic()) {
            return $uploadsFolder . '/public/';
        }
        else {
            return $uploadsFolder . '/protected/';
        }
    }
}
