<?php
namespace Twnepc\Pamcompanies\Classes;


/**
 * Form Model Saver Trait
 *
 * Special logic for applying form data (usually from postback) and
 * applying it to a model and its relationships. This is a customized,
 * safer and simplified version of $model->push().
 *
 * Usage:
 *
 *    $modelsToSave = $this->prepareModelsToSave($model, [...]);
 *
 *    foreach ($modelsToSave as $modelToSave) {
 *        $modelToSave->save();
 *    }
 *
 * @package october\backend
 * @author Alexey Bobkov, Samuel Georges
 */
use Flash;
use Mail;
use Log;
use Twnepc\User\Models\Pins;
trait PinSystemControllerTrait
{
    public function onRequestPIN()
    {
        $activePIN = Pins::where('user_id',$this->user->frontenduser->id)->first();
        if($activePIN){

            $now = time();
            $pinCreated = strtotime($activePIN->pam_created);
            $endOfActivePIN = strtotime('+1 day', $pinCreated);

            if($now > $endOfActivePIN){
                //PIN expires
                $activePIN->delete();

                $activePIN = new Pins;
                $activePIN->code = $activePIN->generatePIN();
                $activePIN->user_id = $this->user->frontenduser->id;
                $activePIN->save();
            }
        }else{
            $activePIN = new Pins;
            $activePIN->code = $activePIN->generatePIN();
            $activePIN->user_id = $this->user->frontenduser->id;
            $activePIN->save();
        }

        $vars = [
            'pin' => $activePIN->code,
            'name' => $this->user->frontenduser->name,
        ];
        Mail::sendTo($this->user->frontenduser->email,'twnepc.user::mail.pams_pin', $vars);
        Flash::success("Your PIN has been sent to your email address.");
    }

    public function onEnterPINSubmitted()
    {
        return $this->user->frontenduser->enterPIN();
    }

    public function onLoadEnterPIN()
    {
        return $this->makePartial('$/twnepc/pamcompanies/views/layouts/_pin_modal.htm');
		
    }

    public function update_onSave($recordId = null, $context = null)
    {
        $this->user->frontenduser->checkPIN();
        return parent::update_onSave($recordId, $context);
    }

    public function create_onSave($context = null)
    {
        $this->user->frontenduser->checkPIN();
        return parent::create_onSave($context);
    }
}
