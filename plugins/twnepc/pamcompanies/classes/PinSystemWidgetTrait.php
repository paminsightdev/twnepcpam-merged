<?php

namespace Twnepc\Pamcompanies\Classes;

use BackendAuth;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use ApplicationException;

trait PinSystemWidgetTrait
{
    public $user;
    public function getUser()
    {
        $this->user = BackendAuth::getUser();
    }

    public function isPAMUser()
    {
        $pamsAdminBackendGroup  = CompanySettings::get('pams_admin_administrator_group');
        $pamsBackendGroup       = CompanySettings::get('pams_administrator_group');
        if(!$pamsAdminBackendGroup || !$pamsBackendGroup)
                throw new ApplicationException('Must set Company settings.');

        foreach($this->user->groups as $group){
            if(($group->id == $pamsAdminBackendGroup) || ($group->id == $pamsBackendGroup))
                return true;
        }
        return false;
    }

    public function getWidgetPINButton()
    {
        return $this->makePartial('$/twnepc/pamcompanies/views/layouts/_widget_pin_button.htm');
    }

    public function onLoadWidgetEnterPIN()
    {
        return $this->makePartial('$/twnepc/pamcompanies/views/layouts/_widget_pin_modal.htm');

    }
}
