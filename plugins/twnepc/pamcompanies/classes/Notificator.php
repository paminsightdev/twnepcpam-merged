<?php
namespace Twnepc\Pamcompanies\Classes;

use Flash;
use Mail;
use Log;

trait Notificator
{

    public function onLoadDisplaySizeNotification()
    {
        return $this->makePartial('$/twnepc/pamcompanies/views/layouts/_display_size_modal.htm');
    }

}