<?php

namespace Twnepc\PamCompanies\Components;



use Cms\Classes\ComponentBase;

use Twnepc\PamCompanies\Models\Companies as CompanyModel;

use Twnepc\PamCompanies\Models\Reviews;

use Twnepc\PamCompanies\Models\CompanyViews;

use Twnepc\PamCompanies\Models\RateQuestions;

use Backend;

use Response;

use Session;

use DateTime;

use Session as AppSession;

use Auth;



class Company extends ComponentBase{



    private $sectionPartial;

    private $titles = [

        'history'       => 'History',

        'disclaimer'    => 'Disclaimer',

        'reviews'       => 'Company Reviews',

        'full-review'   => 'Review - '

    ];



    private $sections = [];



    public function componentDetails()

    {

        return [

            'name' => 'Company Details Edit',

            'description' => 'Here you can edit PAMs company details.'

        ];

    }



    public function onRun()

    {

        $company = CompanyModel::where('company_id',$this->param('id'))->where('c_active',1)->first();

        $this->initBackendCompaniesSections($company->c_typefront);



        if(!$company){

            return $this->controller->run('404');

        }

        

        $userId = (isset($this->page['user']))?$this->page['user']->attributes['id']:'';

        if ($userId > 0) {

            $record = \Db::table("pamtbl_favourite_companies")->where(["REGUSER_ID"=>$userId,"PamID"=>intval($this->param('id'))])->first();

            if ($record) {

                $this->page['favCompany'] = $record;

            }

        }

        $lastSearchUrl = Session::get('lastSearchURL');

        if (!empty($lastSearchUrl)) {

            $this->page['returnButtonTitle'] = "Return to search";

            $this->page['returnButtonURL'] = $lastSearchUrl;

        } else {

            $this->page['returnButtonTitle'] = "Return to home";

            $this->page['returnButtonURL'] = "/";

        }



        $this->page['activeSection'] =  $this->param('section')?$this->param('section'):'introduction';

        $section = $this->param('section')?$this->param('section'):'introduction';

        $section = $this->getAvailableSection($company->c_typefront,$section);

        $this->page['activeSection'] = $section;



        switch($section){

            case 'reviews':

                $this->page['post']             = $company;

                $this->page['title']            = "Company Reviews";

                $this->page['countOfReviews']   = $this->countReviews();

                $this->page['commonRating']     = $this->getCommonRating();

                $this->page['generalRating']    = $this->getGeneralRating($this->page['commonRating']);

                $this->sectionPartial           = $this->renderPartial('@reviews');

                break;



            case 'introduction':

                $this->page['post']             = $company;

                $this->page['title']            = "Introduction to ".$company->c_companyname;

                $this->page['content']          = $company->companytextintro?$company->companytextintro->ct_text:"";

                $this->sectionPartial           = $this->renderPartial('@simple');

                break;



            case 'history':

                $this->page['post']             = $company;

                $this->page['title']            = "History";

                $this->page['content']          = $company->companytexthistory?$company->companytexthistory->ct_text:"";

                $this->sectionPartial           = $this->renderPartial('@simple');

                break;



            case 'answersceo':

                $this->page['post']             = $company;

                $this->page['title']            = "CEO Questions and Answers";

                $this->page['answers']          = $this->filterAnswersBySection($company->answers,"CEO");

                $this->page['author']           = $this->getAuthorByAnswers($this->page['answers']);

                $this->sectionPartial           = $this->renderPartial('@answers');

                break;



            case 'answerscio':

                $this->page['post']             = $company;

                $this->page['title']            = "CIO Questions and Answers";

                $this->page['answers']          = $this->filterAnswersBySection($company->answers,"CIO");

                $this->page['author']           = $this->getAuthorByAnswers($this->page['answers']);

                $this->sectionPartial           = $this->renderPartial('@answers');

                break;



            case 'investment':

                $this->page['post']             = $company;

                $this->page['title']            = "Investment Philosophy";

                $this->page['content']          = $company->companytextinvestment?$company->companytextinvestment->ct_text:"";

                $this->sectionPartial           = $this->renderPartial('@simple');

                break;



            case 'disclaimeronline':

                $this->page['post']             = $company;

                $this->page['title']            = "Online Disclaimer";

                $this->page['content']          = $company->companytextdisclaimeronline?$company->companytextdisclaimeronline->ct_text:"";

                $this->sectionPartial           = $this->renderPartial('@simple');

                break;



            case 'disclaimerdirectory':

                $this->page['post']             = $company;

                $this->page['title']            = "Directory Disclaimer";

                $this->page['content']          = $company->companytextdisclaimerdirectory?$company->companytextdisclaimerdirectory->ct_text:"";

                $this->sectionPartial           = $this->renderPartial('@simple');

                break;



            case 'products':

                $this->page['post']             = $company;

                $this->page['title']            = "Products and Services";

                $this->sectionPartial           = $this->renderPartial('@products');

                break;



            case 'offices':

                $this->page['post']             = $company;

                $this->page['title']            = "Main Offices";

                $this->page['title2']           = "Other Offices";

                $this->page['hasOtherOffices']  = $this->hasOtherOffices($company);

                $this->sectionPartial           = $this->renderPartial('@offices');

                break;



            case 'shareholders':

                $this->page['post']             = $company;

                $this->page['title']            = "Main Shareholders";

                $this->sectionPartial           = $this->renderPartial('@shareholders');

                break;



            case 'assets':

                $this->page['post']             = $company;

                $this->page['title']            = "Asset Classes";

                $this->sectionPartial           = $this->renderPartial('@assets');

                break;



            case 'personnel':

                $this->page['post']             = $company;

                $this->page['title']            = "Key Personnel";

                $this->sectionPartial           = $this->renderPartial('@personnel');

                break;



            case 'publications':

                $this->page['post']             = $company;

                $this->page['title']            = "Publications";

                $this->page['articles']         = $company->publications;

                $this->sectionPartial           = $this->renderPartial('@articles');

                CompanyModel::userSectionUpdate($userId, $this->param('id'), 'publications');

                break;



            case 'articles':

                $this->page['post']             = $company;

                $this->page['title']            = "Articles and Press Cuttings";

                $this->page['articles']         = $company->articles;

                $this->sectionPartial           = $this->renderPartial('@articles');

                CompanyModel::userSectionUpdate($userId, $this->param('id'), 'articles');

                break;



            case 'news':

                $this->page['post']             = $company;

                $this->page['title']            = "News and Announcements";

                $this->page['articles']         = $company->news;

                $this->sectionPartial           = $this->renderPartial('@articles');

                CompanyModel::userSectionUpdate($userId, $this->param('id'), 'news');

                break;



            case 'investment-commentary':

                $this->page['post']             = $company;

                $this->page['title']            = "Investment Commentary";

                $this->page['articles']         = $company->investments;

                $this->sectionPartial           = $this->renderPartial('@articles');

                CompanyModel::userSectionUpdate($userId, $this->param('id'), 'investment_commentary');

                break;



            case 'thresholds':

                $company->attributes['c_discmin'] = number_format(intval(str_replace(',', '', $company->attributes['c_discmin'])));

                $company->attributes['c_advmin'] =  number_format(intval(str_replace(',', '', $company->attributes['c_advmin'])));

                $this->page['post']             = $company;

                $this->page['title']            = "Minimum Thresholds";

                $this->sectionPartial           = $this->renderPartial('@thresholds');

                break;



            case 'fees':

                $this->page['post']             = $company;

                $this->page['title']            = "Fees and Charges";

                $this->sectionPartial           = $this->renderPartial('@fees');

                break;



            case 'corporate':

                $this->page['post']             = $company;

                $this->page['title']            = "Key Corporate Data";

                $this->sectionPartial           = $this->renderPartial('@corporate');

                break;



            case 'investment-platforms':

                $this->page['post']             = $company;

                $this->page['title']            = "Investment Platforms";

                $this->sectionPartial           = $this->renderPartial('@platforms');

                break;



            default:

                //

        }



        $this->setCompanyAsViewed($company);



    }



    public function getSectionPartial()

    {

        echo $this->sectionPartial;

    }



    public function onRequestReviews()

    {

        $alreadyLoadedReviews = post('alreadyLoadedReviews');

        $reviews = Reviews::where('company_id',$this->param('id'))

            ->where('activated',1)

            ->orderBy('id', 'desc')

            ->skip($alreadyLoadedReviews)

            ->take(5)

            ->get();

        $content = $this->renderPartial('@ajax-list-reviews',[

            'companyReviews'=>$reviews

        ]);

        $alreadyLoadedReviews += (count($reviews));

        return ['html' => $content,'alreadyLoadedReviews'=>$alreadyLoadedReviews];

    }



    public function backendUrl($forSection = false)

    {

        $section = $this->param('section')?$this->param('section'):'introduction';

        $backendTab = isset($this->sections[$section])?$this->sections[$section]:"";

        if($forSection){

            $backendTab = isset($this->sections[$forSection])?$this->sections[$forSection]:"";

        }

        return Backend::url('twnepc/pamcompanies/company/update/'.$this->param('id')."".$backendTab);

    }



    public function countReviews()

    {

        return Reviews::where('company_id',$this->param('id'))

            ->where('activated',1)

            ->count();

    }



    public function getGeneralRating($common)

    {

        $rate = 0;

        foreach($common as $rating){

            $rate += $rating['avg'];

        }

        return $rate/count($common);

    }



    public function getCommonRating()

    {

        $common = array();



        $questions = RateQuestions::all();



        foreach($questions as $question){

            $common[$question->name] = array(

                'title' => $question->title,

                'values' => array(),

                'avg' => 0

            );

        }



        $reviews = Reviews::where('company_id',$this->param('id'))

            ->where('activated',1)

            ->orderBy('id', 'desc')

            ->get();



        foreach($reviews as $review){

            $rate = $review->rate;

            foreach($rate as $key => $row){

                $common[$key]['values'][] = $row["rate"];

            }

        }



        foreach($common as $key => $value){

            if(count($common[$key]['values']) > 0)

                    $common[$key]['avg'] = array_sum($common[$key]['values']) / count($common[$key]['values']);

        }

        return $common;

    }



    public function isEditable($user)

    {

        $userCompany = null;

        if(!$user)return false;

        if($user->member && $user->member->activated){

            $userCompany = $user->member->company;

        }else if($user->pamadmin && $user->pamadmin->activated){

            $userCompany = $user->pamadmin->company;

        }

        if($userCompany){

            if($this->param('id') == $userCompany->company_id)

                    return true;

        }

        return false;

    }



    private function getAuthorByAnswers($answers)

    {

        $personnel = false;

        foreach($answers as $answer){

            if( $answer->personnel){

                $personnel = $answer->personnel;

            }

        }

        return $personnel;

    }



    private function filterAnswersBySection($answers,$filter)

    {

        $filteredData = [];

        foreach($answers as $answer){

            if( $answer->question->questionsection->cqs_text == $filter){

                $filteredData[] = $answer;

            }

        }

        return $filteredData;

    }



    public function stripTimeFromDate($date)

    {

        $createDate = new DateTime($date);

        $strip = $createDate->format('Y-m-d');

        return $strip;

    }



    private function initBackendCompaniesSections($accessType)

    {

        $backendTabs = [];

        $frontendTabs = [];





        $configPath = '$/twnepc/pamcompanies/models/companies/c_typeback_fields_basic.yaml';

        switch($accessType){

            case "p":

                $configPath = '$/twnepc/pamcompanies/models/companies/c_typeback_fields_premier.yaml';

                break;



            case "e":

                $configPath = '$/twnepc/pamcompanies/models/companies/c_typeback_fields_enhanced.yaml';

                break;

        }



        $backendController = new \Twnepc\PamCompanies\Controllers\Companies();

        $backendFormConfig = $backendController->makeConfig($configPath);



        foreach($backendFormConfig->tabs as $key => $tab){

            foreach($tab as $field => $options){

                if(isset($options['tab'])){

                    if(!isset($backendTabs[$options['tab']])){

                        if(isset($options['frontendtabs']) && isset($options['tabKey'])){

                            foreach($options['frontendtabs'] as $section => $title){

                                $frontendTabs[$section] = "#primarytab-".$options['tabKey'];

                            }

                        }

                    }

                }

            }

        }

        $this->sections = $frontendTabs;

    }



    public function IsOddAddClass($index,$className)

    {

        return $index % 2 == 0 ? "":$className;

    }



    public function modifyKcdNumberData($string,$placeholder)

    {
        if($placeholder != "") return $placeholder;

       
        /*if(!$string)return $placeholder;

        if($string == "")return $placeholder;

        if($string == 0)return $placeholder;*/



        return number_format($string);

    }



	

    public function modifyKcdDiscAdv($string,$placeholder,$before="",$after="")

    {

        if($placeholder != "") return $placeholder;
        //if(!$string) return $placeholder;

        //if($string == "") return $placeholder;

   //     if($string == 0) return $placeholder;

	       $string2 = 100 - $string;

		

        return html_entity_decode ($before,ENT_COMPAT).number_format($string,1) . "/" . number_format($string2,1) .$after;

    }	

	

    public function getSortedRegulatoryBodies($bodies)

    {

        $string = "";

        foreach($bodies as $body){

            $string .= $body->rb_name.", ";

        }

        return trim($string,", ");

    }



    public function hasOtherOffices($company)

    {

        foreach($company->offices as $office){

            if($office->o_otheroffice == 1)

                    return true;

        }

        return false;

    }



    public function getSortedKcdData($kcdData,$limit)

    {

        $dataByYears = [];

        $counter = 0;

        foreach($kcdData as $row){

            if(!isset($dataByYears[$row->ckcd_year])){

                $dataByYears[$row->ckcd_year] = $row;

                $counter++;

            }

            if($limit && $limit <= $counter)break;

        }

        return array_reverse($dataByYears);

    }



    public function setCompanyAsViewed($company)

    {

        $viewedCompanies = AppSession::get('viewed_companies',array());

        if(!isset($viewedCompanies[$company->c_companyname])){

            $user = Auth::getUser();

            if($user){

                //TODO Need to add extra fields ['what kind of page it is or just current url','company frontend type']

                $companyViewModel = new CompanyViews;

                $companyViewModel->company_id = $company->company_id;

                $companyViewModel->url = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:"";

                $companyViewModel->user_id = $user->id;

                $companyViewModel->save();

                $viewedCompanies[$company->c_companyname] = array(

                    'user' => $user->id,

                    'timestamp' => time()

                );

                AppSession::put('viewed_companies',$viewedCompanies);

            }

        }

    }



    protected function getAvailableSection($frontAccessType,$section)

    {

        $firstAvalableForntendSection = null;

        $fields = [];

        $isSectionAvailable = false;

        $backendController = new \Twnepc\PamCompanies\Controllers\Companies();

        $configPath = '$/twnepc/pamcompanies/models/companies/c_typeback_fields_basic.yaml';

        switch($frontAccessType){

            case "p":

                $configPath = '$/twnepc/pamcompanies/models/companies/c_typeback_fields_premier.yaml';

                break;



            case "e":

                $configPath = '$/twnepc/pamcompanies/models/companies/c_typeback_fields_enhanced.yaml';

                break;

        }

        $backendFormConfig = $backendController->makeConfig($configPath);

        foreach($backendFormConfig->tabs as $key => $value){

            foreach ($value as $name => $field) {

                $fields[$name] = $field;

            }

        }

        foreach($fields as $key => $value){

            if($firstAvalableForntendSection == null && isset($value['frontendtabs'])){

                $arrayKeys = array_keys($value['frontendtabs']);

                $firstAvalableForntendSection = $arrayKeys[0];

            }



            if(isset($value['frontendtabs'])){

                foreach($value['frontendtabs'] as $key => $value){

                    if($key == $section){

                        $isSectionAvailable = true;

                        break;

                    }

                }

            }



        }

        if(!$isSectionAvailable){

            return $firstAvalableForntendSection;

        }

        return $section;

    }



}

?>