<?php
namespace Twnepc\PamCompanies\components;

use Cms\Classes\ComponentBase;
use Twnepc\PamCompanies\Models\Companies;
use Backend;
use Validator;
use ValidationException;
use Request;
use Twnepc\PamCompanies\Models\RateQuestions;
use Twnepc\PamCompanies\Models\Reviews;
use Twnepc\PamCompanies\Models\Settings;
use Url;
use Mail;
use Event;
use Auth;

class Review extends ComponentBase{

    private $company;
    private $badwords = [];

    public function componentDetails()
    {
        return [
            'name' => 'Company Review',
            'description' => ''
        ];
    }

    public function onRun()
    {
        $companyID = $this->property('companyID');
        $company = Companies::where('company_id',$companyID)->first();
        if(!$company){
            return $this->controller->run('404');
        }
        $this->page['company'] = $company;
        $this->page['questions'] = $this->getCustomReviewQuestions();
    }

    public function defineProperties()
    {
        return [
            'companyID' => [
                'title'             => 'Company ID',
                'description'       => 'Company ID reviews',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The Max Items property can contain only numeric symbols'
            ]
        ];
    }

    public function user()
    {
        if (!Auth::check()) {
            return null;
        }

        return Auth::getUser();
    }

    public function onCompanyReviewAdd()
    {
        try{
            $data = post();
            $questions = $this->getCustomReviewQuestions();

            Validator::extend('swearwords', function($attribute, $value, $parameters) {
                if(count($this->badwords) < 1){
                    $swearWordsString = Settings::get('swearwords');
                    if(strlen($swearWordsString) > 0){
                        $array = explode("|",$swearWordsString);
                        foreach($array as $line){
                            $this->badwords[] = $line;
                        }
                    }
                }
                foreach($this->badwords as $badWord){
                    if (stripos($value,$badWord,0) !== false) {
                        return false;
                    }
                }
                return true;
            });

            $rules = [
                'title'         => 'required|swearwords',
                'description'   => 'required|swearwords'
            ];

            foreach($questions as $question){
                $rules[$question->name."_rate"] = 'required|min:1';
                $rules[$question->name."_description"] = 'required|swearwords';
            }

            $messages = [
                'swearwords' => 'The :attribute field contain swearwords.',
            ];

            $validation = Validator::make($data, $rules, $messages);
            if ($validation->fails()) {
                throw new ValidationException($validation);
            }


            $rate = array();
            $general = 0;
            foreach($questions as $question){
                $general += (int) $data[$question->name."_rate"];
                $rate[$question->name] = array(
                    "rate" => $data[$question->name."_rate"],
                    "description" => $data[$question->name."_description"],
                    "title" => $question->title,
                );
            }

            $user = $this->user();

            $review = new Reviews();
            $review->company_id = $this->property('companyID');
            $review->user_id = $user->id;
            $review->title = $data['title'];
            $review->description = $data['description'];
            $review->rate = $rate;
            $review->general = $general/count($rate);
            $review->save();

            $notificationEmailAddress = Settings::get('notification_email');

            if($notificationEmailAddress){
                $companyID = $this->property('companyID');
                $company = Companies::where('company_id',$companyID)->first();

                $data = [
                    'company_name' => $company->c_companyname,
                ];

                Event::fire('pamonline.pamcompanies.review.added', [$company]);
            }

            return ['X_OCTOBER_REDIRECT'=>Url::to('company/'.$this->property('companyID')."/reviews")];

        }catch(\Exception $ex){
            if (Request::ajax()) throw $ex;
            else Flash::error($ex->getMessage());
        }
    }

    private function getCustomReviewQuestions()
    {
        return RateQuestions::all();
    }

}
?>