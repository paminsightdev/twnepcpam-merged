<?php namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcompaniesMembers extends Migration
{
    public function up()
    {
        // check if table exists...
        if(!Schema::hasTable('pamonline_pamcompanies_members')) {
            // create the table...
            Schema::create('pamonline_pamcompanies_members', function($table)
            {
                $table->engine = 'InnoDB';
                $table->integer('company_id');
                $table->integer('user_id');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcompanies_members');
    }
}
