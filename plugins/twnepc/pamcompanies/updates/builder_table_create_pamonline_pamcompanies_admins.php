<?php namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcompaniesAdmins extends Migration
{
    public function up()
    {
        // check if table exists...
        if(!Schema::hasTable('pamonline_pamcompanies_admins')) {
            // create the table...
            Schema::create('pamonline_pamcompanies_admins', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('company_id');
                $table->integer('user_id');
                $table->integer('activated')->default(0);
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcompanies_admins');
    }
}
