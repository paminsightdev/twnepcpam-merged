<?php namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcompaniesReviews extends Migration
{
    public function up()
    {
        // check if table exists...
        if(!Schema::hasTable('pamonline_pamcompanies_reviews')) {
            // create the table...
            Schema::create('pamonline_pamcompanies_reviews', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('company_id');
                $table->dateTime('date');
                $table->string('title', 255);
                $table->text('description');
                $table->text('rate');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcompanies_reviews');
    }
}
