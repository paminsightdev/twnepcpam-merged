<?php namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcompaniesRateQuestions extends Migration
{
    public function up()
    {
        // check if table exists...
        if(!Schema::hasTable('pamonline_pamcompanies_rate_questions')) {
            // create the table...
            Schema::create('pamonline_pamcompanies_rate_questions', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name', 255);
                $table->text('title');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcompanies_rate_questions');
    }
}
