<?php namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\PamCompanies\Models\Company;

class BuilderTableUpdatePamonlinePamcompaniesCompanies extends Migration
{
    public function up()
    {
        // check if table exists...
        if(Schema::hasTable('pamonline_pamcompanies_companies')) {
            // when there is no data put dummy one in
            if(Company::all()->count() == 0) {
                // alter the table column
                Schema::table('pamonline_pamcompanies_companies', function($table)
                {
                    $table->text('intro');
                    $table->text('history');
                    $table->text('disclaimer');
                });
            }
        }
    }
    
    public function down()
    {
        Schema::table('pamonline_pamcompanies_companies', function($table)
        {
            $table->dropColumn('intro');
            $table->dropColumn('history');
            $table->dropColumn('disclaimer');
        });
    }
}
