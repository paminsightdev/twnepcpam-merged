<?php namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\PamCompanies\Models\Members;

class BuilderTableUpdatePamonlinePamcompaniesMembers extends Migration
{
    public function up()
    {
        // check if table exists...
        if(Schema::hasTable('pamonline_pamcompanies_members')) {
            // when there is no data put dummy one in
            if(Members::all()->count() == 0) {
                // alter the table column
                Schema::table('pamonline_pamcompanies_members', function($table)
                {
                    $table->integer('activated')->default(0);
                });
            }
        }
    }
    
    public function down()
    {
        Schema::table('pamonline_pamcompanies_members', function($table)
        {
            $table->dropColumn('activated');
        });
    }
}
