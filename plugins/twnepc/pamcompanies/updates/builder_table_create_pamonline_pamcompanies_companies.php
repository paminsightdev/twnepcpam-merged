<?php namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcompaniesCompanies extends Migration
{
    public function up()
    {
        // check if table exists...
        if(!Schema::hasTable('pamonline_pamcompanies_companies')) {
            // create the table...
            Schema::create('pamonline_pamcompanies_companies', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name', 255);
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcompanies_companies');
    }
}
