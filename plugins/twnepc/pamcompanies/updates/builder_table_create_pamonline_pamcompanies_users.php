<?php namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePamonlinePamcompaniesUsers extends Migration
{
    public function up()
    {
        // check if table exists...
        if(!Schema::hasTable('pamonline_pamcompanies_users')) {
            // create the table...
            Schema::create('pamonline_pamcompanies_users', function($table)
            {
                $table->engine = 'InnoDB';
                $table->integer('user_id');
                $table->integer('pam_id');
            });
        }
    }
    
    public function down()
    {
        Schema::dropIfExists('pamonline_pamcompanies_users');
    }
}
