<?php
namespace Twnepc\PamCompanies\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use Twnepc\PamCompanies\Models\Companies;

class BuilderTableUpdatePamtblCompany extends Migration {
    
    public function up() {
        
        // check if table exists...
        if(Schema::hasTable('pamtbl_company')) {
            // when there is no data put dummy one in
            if(Companies::all()->count() == 0) {
                // alter the table column...
                Schema::table('pamtbl_company', function($table) {
                    $table->tinyInteger('c_custodian')->default('0');
                });
            }
        }
    }
    
    public function down() {
        
        Schema::table('pamtbl_company', function($table) {
            $table->dropColumn('c_custodian');
        });
    }
}
