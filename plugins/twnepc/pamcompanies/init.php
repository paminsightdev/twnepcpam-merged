<?php

/**
 * On user register
 */
Event::listen('twnepc.user.register', function($user,$post,$automaticActivation) {

    $params = [
        'id' => $user->id,
        'name' => $user->name." ".$user->surname,
        'email' => $user->email,
    ];

    $notificationEmailAddress = Twnepc\Pamcompanies\Models\Settings::get('notification_email');
    $emails = explode('|',$notificationEmailAddress);
    foreach($emails as $email){
        $email = trim($email);
        Mail::sendTo($email, 'twnepc.user::mail.new_user', $params);
    }
    return true;
});


/**
 * On delete user.
 */
Event::listen('twnepc.user.delete', function($user) {
    $backenduser = BackendAuth::findUserById($user->backenduser_id);
    if($backenduser){
        $backenduser->delete();
        $backenduser->afterDelete();
    }

    $member = Twnepc\Pamcompanies\Models\Members::where('user_id',$user->id)->first();
    if($member){
        $member->delete();
    }

    $admin = Twnepc\Pamcompanies\Models\Admins::where('user_id',$user->id)->first();
    if($admin){
        $admin->delete();
    }
});

/**
 * On deactivate user.
 */
Event::listen('twnepc.user.deactivate', function($user) {
    $backenduser = BackendAuth::findUserById($user->backenduser_id);
    if($backenduser){
        $backenduser->delete();
        $backenduser->afterDelete();
    }

    $member = Twnepc\Pamcompanies\Models\Members::where('user_id',$user->id)->first();
    if($member){
        $member->delete();
    }

    $admin = Twnepc\Pamcompanies\Models\Admins::where('user_id',$user->id)->first();
    if($admin){
        $admin->delete();
    }
});

/**
 * no longer works with later version of october 
 */
Event::listen('backend.my_account', function() {
    $user = BackendAuth::getUser();
    if($user && $user->frontenduser){
        header("Location:".url('/my-account'));die();
    }
});


Event::listen('pamonline.pamsadmin.deactivate', function($user) {
    $vars = [
        'name' => $user->name,
        'company_name' => $user->pamadmin->company->c_companyname,
    ];
    Mail::sendTo($user->email, 'twnepc.user::mail.pams_deactivated', $vars);
    return false;
});

Event::listen('pamonline.pamsadmin.activate', function($user) {
    $vars = [
        'name' => $user->name,
        'company_name' => $user->pamadmin->company->c_companyname,
        'access_url' =>  url('')."/?redirect_after_login=".url('backend/twnepc/pamcompanies/company'),
        'link_text' => 'Get access to edit company details.',
    ];
    Mail::sendTo($user->email, 'twnepc.user::mail.pams_activated', $vars);
    return false;
});

Event::listen('pamonline.pams.deactivate', function($user) {
    $vars = [
        'name' => $user->name,
        'company_name' => $user->member->company->c_companyname,
    ];
    Mail::sendTo($user->email, 'twnepc.user::mail.pams_deactivated', $vars);
    return false;
});

Event::listen('pamonline.pams.activate', function($user) {
    $vars = [
        'name' => $user->name,
        'company_name' => $user->member->company->c_companyname,
        'access_url' =>  url('')."/?redirect_after_login=".url('backend/twnepc/pamcompanies/company'),
        'link_text' => 'Get access to edit company details.',
    ];
    Mail::sendTo($user->email, 'twnepc.user::mail.pams_activated', $vars);
    return false;
});

Event::listen('twnepc.user.company.changed', function($user) {
    $member = Twnepc\Pamcompanies\Models\Members::where('user_id',$user->id)->first();
    if($member){
        $member->company_id = $user->company_id;
        $member->activated = 0;
        $member->save();
    }

    $admin = Twnepc\Pamcompanies\Models\Admins::where('user_id',$user->id)->first();
    if($admin){
        $admin->company_id = $user->company_id;
        $admin->activated = 0;
        $admin->save();
    }

    $backenduser = BackendAuth::findUserById($user->backenduser_id);

    if($backenduser){
        $user->backenduser_id=null;
        $user->save();
        $backenduser->delete();
        $backenduser->afterDelete();
    }
});

Event::listen('twnepc.user.group.changed', function($user,$oldGroup,$newGroup) {

    $pamsFrontendGroups = Twnepc\PamCompanies\Models\Settings::get('pams_frontend_groups');
    $pamsAdminFrontendGroups = Twnepc\PamCompanies\Models\Settings::get('pams_admin_frontend_groups');

    if($newGroup == $pamsFrontendGroups || $newGroup == $pamsAdminFrontendGroups){
        if($user->company_id == "" or $user->company_id == 0){
            throw new ValidationException(['company_id' => 'If you want set user as PAMs or PAMs admin you must specify existing company.']);
        }
        $company = Twnepc\PamCompanies\Models\Companies::where('company_id',$user->company_id )->first();
    }

    $backenduser = BackendAuth::findUserById($user->backenduser_id);

    if($backenduser){
        $backenduser->delete();
        $backenduser->afterDelete();
    }

    $user->backenduser_id = null;

    if($oldGroup == $pamsFrontendGroups || $oldGroup == $pamsAdminFrontendGroups){

        $member = Twnepc\Pamcompanies\Models\Members::where('user_id',$user->id)->first();
        if($member){
            $member->delete();
        }

        $admin = Twnepc\Pamcompanies\Models\Admins::where('user_id',$user->id)->first();
        if($admin){
            $admin->delete();
        }
    }

    if($newGroup == $pamsFrontendGroups){
        $member = new Twnepc\Pamcompanies\Models\Members();
        $member->user_id = $user->id;
        $member->company_id = $company->company_id;
        $member->save();
    }

    if($newGroup == $pamsAdminFrontendGroups){
        $member = new Twnepc\Pamcompanies\Models\Admins();
        $member->user_id = $user->id;
        $member->company_id = $company->company_id;
        $member->save();
    }

});

Event::listen('pamonline.pamcompanies.review.added', function($company) {

    $params = [
        'company_name' => $company->c_companyname,
    ];

    $notificationEmailAddress = Twnepc\PamCompanies\Models\Settings::get('notification_email');
    $emails = explode('|',$notificationEmailAddress);
    foreach($emails as $email){
        $email = trim($email);
        Mail::sendTo($email, 'pamonline.pamcompanies::mail.new_review', $params);
    }

    return true;
});

Event::listen('pamonline.pamcompanies.pams.added', function($user,$company) {

    /**
     * Send notification to CMS admins
     */
    $params = [
        'url' => Backend::url()."/user/users/update/".$user->id,
    ];

    $notificationEmailAddress = Twnepc\PamCompanies\Models\Settings::get('notification_email');
    $emails = explode('|',$notificationEmailAddress);
    foreach($emails as $email){
        $email = trim($email);
        Mail::sendTo($email, 'pamonline.pamcompanies::mail.new_pam', $params);
    }

    /**
     * Send notification to PAMs admins
     */

    $pamsAdmins = Twnepc\Pamcompanies\Models\Admins::where('company_id',$company->company_id)->get();

    $params = [
        'url' => Backend::url()."/twnepc/pamcompanies/members/update/".$user->id,
    ];

    foreach($pamsAdmins as $pamsAdmin){
        if($pamsAdmin->user)
            Mail::sendTo($pamsAdmin->user->email, 'pamonline.pamcompanies::mail.new_pam', $params);
    }

    return true;
});

Event::listen('pamonline.pamcompanies.pamsadmin.added', function($user,$company) {

    /**
     * Send notification to CMS admins
     */
    $params = [
        'url' => Backend::url()."/user/users/update/".$user->id,
    ];

    $notificationEmailAddress = Twnepc\PamCompanies\Models\Settings::get('notification_email');
    $emails = explode('|',$notificationEmailAddress);
    foreach($emails as $email){
        $email = trim($email);
        Mail::sendTo($email, 'pamonline.pamcompanies::mail.new_pamadmin', $params);
    }
    return true;
});