<?php namespace Twnepc\PamCompanies;

use System\Classes\PluginBase;
use Backend\Facades\BackendAuth;
use Backend;
use Auth;
use Event;
use Twnepc\PamCompanies\Models\Companies as Company;
use Twnepc\PamCompanies\Models\Settings as CompanySettings;
use Twnepc\PamCompanies\Models\CompanySignoff;
use Backend\Facades\BackendMenu;
use Illuminate\Support\Facades\DB;
use Twnepc\PamCompanies\Models\Members;
use Twnepc\PamCompanies\Models\Admins;
use Illuminate\Support\Facades\Log;
use Backend\Classes\Controller;
use Keios\Multisite\Classes\SettingHelper;
use Session;
use Backend\Controllers\Users as BackendUsersController;
use Backend\Models\User as BackendUserModel;
use Illuminate\Support\Facades\URL;


class Plugin extends PluginBase
{
    // important - page.beforeDisplay event
    public $elevated = true; 
    
    public function registerComponents()
    {
        return [
            'Twnepc\PamCompanies\Components\Company' => 'company',
            'Twnepc\PamCompanies\Components\Review' => 'review',
        ];
    }

    public function boot()
    {
        
        // when the user's backend role is not for paminsight twn/epc staff then hide the settings tab
        Event::listen('backend.menu.extendItems', function($navigationManager) {
            // CAN BE REMOVED... stops the settings button being displayed for PAM backend users
            // however the the menu items all have permissions now so the settings butoon should not be displayed!
            $backendUser = BackendAuth::getUser();
            if(!in_array($backendUser->role_id, [3, 4, 5, 6, 7]) || $backendUser->is_superuser !== 1) {
                $navigationManager->removeMainMenuItem('October.System', 'system');
            }
        });
        
        
        // dont let the user get to the backend account page, from front-end, whilst in the controller change the route
        Event::listen('backend.page.beforeDisplay', function ($backendController, $action, $params) {
            // current url at pam/backend/backend/users/myaccount
            if(stripos($backendController->actionUrl(), 'backend/users/myaccount')) {
                // only when its a PAM user...
                $backendUser = BackendAuth::getUser();
                if($backendUser->frontenduser) {
                    // forward the user to their my account page...
                    header("Location: ".SettingHelper::getCurrentSite()->domain.'/my-account');
                    die();
                }
            }
        });
        
        
        Event::listen('twnepc.user.register', function($user,$post,$automaticActivation) {

            $pamsGroup = CompanySettings::get('pams_frontend_groups');
            $pamsAdminGroup = CompanySettings::get('pams_admin_frontend_groups');

            if($post['role'] == $pamsGroup || $post['role'] == $pamsAdminGroup){

                $company = Company::where('company_id',$post['company_id'])->first();
                if($post['role'] == $pamsGroup){
                    $newMember = new Members();
                    $newMember->user_id = $user->id;
                    $newMember->company_id = $company->company_id;
                    $newMember->save();
                    Event::fire('pamonline.pamcompanies.pams.added', [$user,$company]);
                }
                if($post['role'] == $pamsAdminGroup){
                    $newMember = new Admins();
                    $newMember->user_id = $user->id;
                    $newMember->company_id = $company->company_id;
                    $newMember->save();
                    Event::fire('pamonline.pamcompanies.pamsadmin.added', [$user,$company]);
                }
            }

            return true;
        });
        
        
        // this code is used to extend the framework model without changing it, 
        // so the user object can be accessed as a relation via it for PAMOnline legacy code to work.
        \Backend\Models\User::extend(function($model) {
            $model->hasOne['frontenduser'] = ['Rainlab\User\Models\User', 'table' => 'users', 'key' => 'backenduser_id'];
        });
        
        BackendUsersController::extendFormFields(function($form, $model, $context) {
            
            if(!$model instanceof BackendUserModel) {
                return;
            }
            
            if(stripos(URL::full(), 'backend/backend/users/create')) {
                if(!empty($context) && 'create' === $context) { 
                    \Backend\Models\User::extend(function($model) {
                        $model->hasOne['frontenduser'] = null;
                        //$model->frontenduser->detach();
                    });
                }
            }
        });
        
    }

    public function registerSettings()
    {
        
        return [
            'settings' => [
                'label'       => 'Companies settings',
                'description' => 'Configure companies plugin.',
                'category'    => 'Pamonline',
                'icon'        => 'icon-university',
                'class'       => 'Twnepc\PamCompanies\Models\Settings',
                'order'       => 500,
                'permissions' => ['pamonline.pamcompanies.settings']
            ],
            'scripts' => [
                'label'       => 'Scripts',
                'description' => 'Fix scripts.',
                'category'    => 'Scripts',
                'icon'        => 'icon-university',
                'class'       => 'Twnepc\PamCompanies\Models\Scripts',
                'order'       => 500,
                'permissions' => ['pamonline.pamcompanies.settings']
            ]
        ];
        
         
    }

    public function registerNavigation()
    {
        $backendUser = BackendAuth::getUser();

        if($backendUser->is_superuser == 0){
            return [
                'company' => [
                    'label'       => 'Company Details',
                    'url'         => Backend::url('twnepc/pamcompanies/company'),
                    'icon'        => 'icon-university',
                    'permissions' => ['pamonline.pamcompanies.access_edit_company'],
                    'order'       => 500,
                ],
                'members' => [
                    'label'       => 'Authorised PAMs',
                    'url'         => Backend::url('twnepc/pamcompanies/members'),
                    'icon'        => 'icon-users',
                    'permissions' => ['pamonline.pamcompanies.access_edit_members'],
                    'order'       => 500,
                ],
                'companies' => [
                    'label'       => 'Companies',
                    'url'         => Backend::url('twnepc/pamcompanies/companies'),
                    'icon'        => 'icon-university',
                    'permissions' => ['pamonline.pamcompanies.access_companies'],
                    'order'       => 500,
                    'sideMenu' => [
                        'admins' => [
                            'label'       => 'PAM Admins',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/admins'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_pams']
                        ],
                        'pams' => [
                            'label'       => 'PAMs',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/pams'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_pams']
                        ],
                        'questions' => [
                            'label'       => 'Rate questions',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/questions'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_questions']
                        ],
                        'reviews' => [
                            'label'       => 'Reviews',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/reviews'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_reviews']
                        ],
                        'ceoquestions' => [
                            'label'       => 'Questions',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/ceoquestions'),
                            'permissions' => ['pamonline.pamcompanies.access_companies']
                        ],
                        'products' => [
                            'label'       => 'Products',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/products'),
                            'permissions' => ['pamonline.pamcompanies.access_companies']
                        ],
                        'registeredbodies' => [
                            'label'       => 'Registered bodies',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/registeredbodies'),
                            'permissions' => ['pamonline.pamcompanies.access_companies']
                        ],
                        'platforms' => [
                            'label'       => 'Platforms',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/platforms'),
                            'permissions' => ['pamonline.pamcompanies.access_companies']
                        ],
                    ]
                ]
            ];
        }

        if($backendUser->is_superuser == 1){
            return [
                'companies' => [
                    'label'       => 'Companies',
                    'url'         => Backend::url('twnepc/pamcompanies/companies'),
                    'icon'        => 'icon-university',
                    'permissions' => ['pamonline.pamcompanies.access_companies'],
                    'order'       => 500,
                    'sideMenu' => [
                        'companies' => [
                            'label'       => 'Companies',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/companies'),
                            'permissions' => ['pamonline.pamcompanies.access_companies']
                        ],
                        'admins' => [
                            'label'       => 'PAM Admins',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/admins'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_members']
                        ],
                        'pams' => [
                            'label'       => 'PAMs',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/pams'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_pams']
                        ],
                        'questions' => [
                            'label'       => 'Rate questions',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/questions'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_questions']
                        ],
                        'reviews' => [
                            'label'       => 'Reviews',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/reviews'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_reviews']
                        ],
                        'ceoquestions' => [
                            'label'       => 'Questions',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/ceoquestions'),
                            'permissions' => ['pamonline.pamcompanies.access_edit_reviews']
                        ],
                        'products' => [
                            'label'       => 'Products',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/products'),
                            'permissions' => ['pamonline.pamcompanies.access_companies']
                        ],
                        'registeredbodies' => [
                            'label'       => 'Registered bodies',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/registeredbodies'),
                            'permissions' => ['pamonline.pamcompanies.access_companies']
                        ],
                        'platforms' => [
                            'label'       => 'Platforms',
                            'icon'        => 'icon-copy',
                            'url'         => Backend::url('twnepc/pamcompanies/platforms'),
                            'permissions' => ['pamonline.pamcompanies.access_companies']
                        ],
                    ]
                ]
            ];
        }
    }

    public function registerPermissions()
    {
        return [
            'pamonline.pamcompanies.access_edit_company' => ['tab' => 'pamonline.pamcompanies::lang.plugin.name', 'label' => 'Access to manage PAMs company details (PAMs admin & PAMs)'],
            'pamonline.pamcompanies.access_edit_members' => ['tab' => 'pamonline.pamcompanies::lang.plugin.name', 'label' => 'Access to manage company members (PAMs admin)'],
            'pamonline.pamcompanies.access_edit_pams' => ['tab' => 'pamonline.pamcompanies::lang.plugin.name', 'label' => 'Access to manage PAMs users '],
            'pamonline.pamcompanies.access_edit_admins' => ['tab' => 'pamonline.pamcompanies::lang.plugin.name', 'label' => 'Access to manage PAMs admins'],
            'pamonline.pamcompanies.access_companies' => ['tab' => 'pamonline.pamcompanies::lang.plugin.name', 'label' => 'Access to manage companies'],
            'pamonline.pamcompanies.access_edit_questions' => ['tab' => 'pamonline.pamcompanies::lang.plugin.name', 'label' => 'Access to manage  companies review questions'],
            'pamonline.pamcompanies.access_edit_reviews' => ['tab' => 'pamonline.pamcompanies::lang.plugin.name', 'label' => 'Access to manage companies reviews']
        ];
    }

    public function registerSchedule($schedule)
    {
//        $schedule->call(function () {
//            \Log::info('Shedule is running');
//        })->everyMinute();
    }

    public function registerReportWidgets()
    {
        return [
            'Twnepc\PamCompanies\ReportWidgets\Reviews' => [
                'label'   => 'Reviews to approve',
                'context' => 'dashboard'
            ]
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Twnepc\PamCompanies\FormWidgets\DropdownDepends' => [
                'label' => 'Dropdown depends',
                'code' => 'dropdownDepends'
            ],
            'Twnepc\PamCompanies\FormWidgets\Progress' => [
                'label' => 'Progress',
                'code' => 'progress'
            ],
            'Twnepc\PamCompanies\FormWidgets\Kcd' => [
                'label' => 'Kcd',
                'code' => 'options'
            ]
        ];
    }
}
