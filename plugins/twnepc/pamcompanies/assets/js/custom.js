$(function(){

    var hashTag             = window.location.hash;
    var formTabsContainer   = $("#Form-primaryTabs");
    var tabsNavigation      = formTabsContainer.find(".nav-tabs");
    var tabsContent         = formTabsContainer.find(".tab-content");

    if(hashTag != ""){
        tabsNavigation.find("a").each(function(){
            var currentTab = $(this);
            if(currentTab.attr("href") === hashTag){
                tabsNavigation.find("a").each(function(){
                    $(this).parent().removeClass("active");
                });
                currentTab.parent().addClass("active");
                tabsContent.find(".tab-pane").each(function(){
                    $(this).removeClass("active");
                });
                tabsContent.find(currentTab.data('target')).addClass("active");
            }
        });
    }



    $(document).ready(function(){
        var inputFieldDiv = $('.form-control:disabled').closest("div");
        var clickableDiv = $("<div />");
        clickableDiv.addClass('disabledFieldClickableDiv');
        inputFieldDiv.append(clickableDiv);
        clickableDiv.click(function(){
            $.popup({ handler: 'onDisabledFieldClick' });
        });

        $('.disabledWidget').click(function(){
            $.popup({ handler: 'onDisabledFieldClick' });
        });

        $('.nav-tabs .disabled_tab').click(function(){
            $.popup({ handler: 'onDisabledTabClick' });
        });

        $('.checkboxAll').each(function(){
            var checkboxAll = $(this);
            checkboxAll.change(function(){
                var table = $(this).closest('table');
                if($(this).is(":checked")) {
                    table.find('td input[type="checkbox"]').prop('checked', true);
                }else{
                    table.find('td input[type="checkbox"]').prop('checked', false);
                }
            });
        });
    });

   $.each( $('.company-summary-print').find('.layout-row').find('.layout-cell'),function(index,item){
        //console.log(item);
        var title = $(item).find('.section-field').find('h4');
        var signoff = $(item).find('.checkbox-field').find('input[type="checkbox"]');

        console.log();
        if($(signoff).attr('checked') == "checked")
        {
            $(title).append('&nbsp;<small>Signed Off: Yes</small>');
        }
    });
    $.each($('#Form-primaryTabs').find('.nav-tabs').find('li.disabled'),function(index,item){

        var target = $(item);
        var id = $(target).find('a').attr('data-target');
        $(id).addClass('print-invisible');

    });
   
});