<?php namespace Twnepc\PamCompanies\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Exception;
use Twnepc\Pamcompanies\Models\Reviews as CompanyReviews;
use DB;

class Reviews extends ReportWidgetBase
{
    public function render()
    {
        try {
            $this->loadData();
        }
        catch (Exception $ex) {
            $this->vars['error'] = $ex->getMessage();
        }

        return $this->makePartial('widget');
    }

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'backend::lang.dashboard.widget_title_label',
                'default'           => 'indikator.user::lang.plugin.name',
                'type'              => 'string',
                'validationPattern' => '^.+$',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error'
            ],
            'total' => [
                'title'             => 'indikator.user::lang.widget.show_total',
                'default'           => true,
                'type'              => 'checkbox'
            ],
            'active' => [
                'title'             => 'indikator.user::lang.widget.show_active',
                'default'           => true,
                'type'              => 'checkbox'
            ],
            'inactive' => [
                'title'             => 'indikator.user::lang.widget.show_inactive',
                'default'           => true,
                'type'              => 'checkbox'
            ],
            'deleted' => [
                'title'             => 'indikator.user::lang.widget.show_deleted',
                'default'           => true,
                'type'              => 'checkbox'
            ]
        ];
    }

    protected function loadData()
    {
        $this->vars['total'] = CompanyReviews::where('activated',0)->count();
        $this->vars['reviews'] = CompanyReviews::where('activated',0)->get();
    }
}
