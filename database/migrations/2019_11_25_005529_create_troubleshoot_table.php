<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/*
create table pam_splunk_troubleshooting (
`id` int not null auto_increment,
`host` varchar(255),
`source` varchar(510),
`sourcetype` varchar(50),
`user_id` int not null,
`workflow_feature` varchar(100),
`workflow_class_function` varchar(255),
`event_name` varchar(100),
`event` text,
`timestamp` datetime,
primary key(id));
*/

class CreateTroubleshootTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pam_splunk_troubleshooting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('host', 255);
            $table->string('source', 510);
            $table->string('sourcetype', 50);
            $table->integer('user_id')->index();
            $table->string('workflow_feature', 100);
            $table->string('workflow_class_function', 255);
            $table->string('event_name', 100);
            $table->text('event');
            $table->datetime('timestamp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pam_splunk_troubleshooting');
    }
}
